unit UCanhoto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, sMaskEdit, sCustomComboEdit, sTooledit,
  ExtCtrls, sPanel, Buttons, sBitBtn, sRadioButton, QuickRpt, QRCtrls, DB,
  DBClient, SimpleDS;

type
  TFCanhoto = class(TForm)
    P1: TsPanel;
    edData1: TsDateEdit;
    edData2: TsDateEdit;
    sPanel1: TsPanel;
    Rb2: TsRadioButton;
    Rb3: TsRadioButton;
    Rb1: TsRadioButton;
    btVisualizar: TsBitBtn;
    btFechar: TsBitBtn;
    qrCertidoes: TQuickRep;
    QRBand1: TQRBand;
    lbCartorio1: TQRLabel;
    lbEndereco1: TQRLabel;
    QRLabel2: TQRLabel;
    lbPagina1: TQRSysData;
    QRSysData1: TQRSysData;
    lbMovimento1: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel3: TQRLabel;
    Detalhe: TQRBand;
    QRDBText1: TQRDBText;
    Certidao: TSimpleDataSet;
    CertidaoCODIGO: TIntegerField;
    CertidaoREQUERIDO: TStringField;
    CertidaoRECIBO: TIntegerField;
    CertidaoRESULTADO: TStringField;
    CertidaoSELO: TStringField;
    CertidaoTipo: TStringField;
    CertidaoANOS: TIntegerField;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRBand2: TQRBand;
    lbCidade1: TQRLabel;
    QRShape1: TQRShape;
    lbFuncionario1: TQRLabel;
    QRSysData2: TQRSysData;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    qrPagos: TQuickRep;
    QRBand3: TQRBand;
    lbCartorio2: TQRLabel;
    lbEndereco2: TQRLabel;
    QRLabel10: TQRLabel;
    QRSysData3: TQRSysData;
    QRSysData4: TQRSysData;
    lbMovimento2: TQRLabel;
    QRChildBand1: TQRChildBand;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRBand4: TQRBand;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRBand5: TQRBand;
    lbCidade2: TQRLabel;
    QRShape2: TQRShape;
    lbFuncionario2: TQRLabel;
    QRSysData5: TQRSysData;
    Pagos: TSimpleDataSet;
    PagosPROTOCOLO: TIntegerField;
    PagosSELO_PAGAMENTO: TStringField;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    qrCancelados: TQuickRep;
    QRBand6: TQRBand;
    lbCartorio3: TQRLabel;
    lbEndereco3: TQRLabel;
    QRLabel15: TQRLabel;
    QRSysData6: TQRSysData;
    QRSysData7: TQRSysData;
    lbMovimento3: TQRLabel;
    QRChildBand2: TQRChildBand;
    QRLabel19: TQRLabel;
    QRLabel22: TQRLabel;
    QRBand7: TQRBand;
    QRBand8: TQRBand;
    lbCidade3: TQRLabel;
    QRShape3: TQRShape;
    lbFuncionario3: TQRLabel;
    QRSysData8: TQRSysData;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel23: TQRLabel;
    Cancelados: TSimpleDataSet;
    CanceladosPROTOCOLO: TIntegerField;
    CanceladosSELO_PAGAMENTO: TStringField;
    CanceladosSELO_REGISTRO: TStringField;
    CanceladosDT_REGISTRO: TDateField;
    CanceladosDT_PAGAMENTO: TDateField;
    CanceladosLIVRO_PROTOCOLO: TIntegerField;
    CanceladosFOLHA_PROTOCOLO: TStringField;
    CanceladosLIVRO_REGISTRO: TIntegerField;
    CanceladosFOLHA_REGISTRO: TStringField;
    QRDBText6: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText14: TQRDBText;
    CanceladosRECIBO: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure CertidaoCalcFields(DataSet: TDataSet);
    procedure btVisualizarClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qrCanceladosEndPage(Sender: TCustomQuickRep);
    procedure qrCertidoesEndPage(Sender: TCustomQuickRep);
    procedure qrPagosEndPage(Sender: TCustomQuickRep);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCanhoto: TFCanhoto;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFCanhoto.FormCreate(Sender: TObject);
begin
  edData1.Date:=Now;
  edData2.Date:=Now;
end;

procedure TFCanhoto.CertidaoCalcFields(DataSet: TDataSet);
begin
  if CertidaoRESULTADO.AsString='N' then
    CertidaoTipo.AsString:='NEGATIVA DE '+CertidaoANOS.AsString+' ANOS'
      else CertidaoTipo.AsString:='POSITIVA DE '+CertidaoANOS.AsString+' ANOS';
end;

procedure TFCanhoto.btVisualizarClick(Sender: TObject);
begin
  if Rb1.Checked then
  begin
      PF.Aguarde(True);
      Pagos.Close;
      Pagos.DataSet.Params[0].AsDate:=edData1.Date;
      Pagos.DataSet.Params[1].AsDate:=edData2.Date;
      Pagos.Open;
      lbCartorio2.Caption     :=dm.ServentiaDESCRICAO.AsString;
      lbEndereco2.Caption     :=dm.ServentiaENDERECO.AsString+' - '+dm.ServentiaBAIRRO.AsString+' - RJ';
      lbMovimento2.Caption    :='Movimento referente aos pagamentos de: '+edData1.Text+' a '+edData2.Text;
      lbCidade2.Caption       :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',Now);
      lbFuncionario2.Caption  :=dm.vNome;
      qrPagos.Preview;
  end;

  if Rb2.Checked then
  begin
      PF.Aguarde(True);
      Cancelados.Close;
      Cancelados.DataSet.Params[0].AsDate:=edData1.Date;
      Cancelados.DataSet.Params[1].AsDate:=edData2.Date;
      Cancelados.Open;
      lbCartorio3.Caption     :=dm.ServentiaDESCRICAO.AsString;
      lbEndereco3.Caption     :=dm.ServentiaENDERECO.AsString+' - '+dm.ServentiaBAIRRO.AsString+' - RJ';
      lbMovimento3.Caption    :='Movimento referente aos cancelamentos de: '+edData1.Text+' a '+edData2.Text;
      lbCidade3.Caption       :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',Now);
      lbFuncionario3.Caption  :=dm.vNome;
      qrCancelados.Preview;
  end;

  if Rb3.Checked then
  begin
      PF.Aguarde(True);
      Certidao.Close;
      Certidao.DataSet.Params[0].AsDate:=edData1.Date;
      Certidao.DataSet.Params[1].AsDate:=edData2.Date;
      Certidao.Open;
      lbCartorio1.Caption     :=dm.ServentiaDESCRICAO.AsString;
      lbEndereco1.Caption     :=dm.ServentiaENDERECO.AsString+' - '+dm.ServentiaBAIRRO.AsString+' - RJ';
      lbMovimento1.Caption    :='Movimento referente �s certid�es de: '+edData1.Text+' a '+edData2.Text;
      lbCidade1.Caption       :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',Now);
      lbFuncionario1.Caption  :=dm.vNome;
      qrCertidoes.Preview;
  end;
end;

procedure TFCanhoto.btFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TFCanhoto.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFCanhoto.qrCanceladosEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFCanhoto.qrCertidoesEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFCanhoto.qrPagosEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

end.
