unit UCancelado2014;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Buttons, sBitBtn, sCurrEdit, sCurrencyEdit,
  DBCtrls, sDBLookupComboBox, sEdit, Mask, sMaskEdit, sCustomComboEdit,
  sTooledit, ExtCtrls, sPanel, sCheckBox, sComboBox, sLabel, acPNG, Grids,
  DBGrids, acDBGrid, DBClient, Menus;

type
  TFCancelado2014 = class(TForm)
    P1: TsPanel;
    dsBaixas: TDataSource;
    Grid: TsDBGrid;
    btConfirmar: TsBitBtn;
    btCancelar: TsBitBtn;
    ckImprimir: TsCheckBox;
    PM: TPopupMenu;
    N11: TMenuItem;
    N21: TMenuItem;
    N31: TMenuItem;
    NIHIL1: TMenuItem;
    btCustas: TsBitBtn;
    F1: TMenuItem;
    procedure btConfirmarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Custas(Codigo: Integer; Cobranca: String);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure N11Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure N31Click(Sender: TObject);
    procedure NIHIL1Click(Sender: TObject);
    procedure btCustasClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure F1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Saldo: Double;
    vPeriodo: Integer;
  end;

var
  FCancelado2014: TFCancelado2014;

implementation

uses UDM, UPF, UCustas, UGeral, UGDM, StdConvs;

{$R *.dfm}

procedure TFCancelado2014.FormCreate(Sender: TObject);
begin
  dm.Escreventes.Close;
  dm.Escreventes.Open;

  dm.RxCustas.Close;
  dm.RxCustas.Open;

  dm.Codigos.Close;
  dm.Codigos.Params.ParamByName('OCULTO1').AsString:='N';
  dm.Codigos.Params.ParamByName('OCULTO2').AsString:='N';
  dm.Codigos.Open;

  dm.cBaixas.DisableControls;
  dm.cBaixas.First;
  while not dm.cBaixas.Eof do
  begin
      dm.vIdAto:=dm.cBaixasID_ATO.AsInteger;
      dm.RxCustas.Filtered:=False;
      dm.RxCustas.Filtered:=True;

      if dm.cBaixasCONVENIO.AsString='S' then
        Custas(4038,'CC')
          else Custas(4010,'CC');
      dm.cBaixas.Next;
  end;
  dm.cBaixas.First;
  dm.cBaixas.EnableControls;
end;

procedure TFCancelado2014.Custas(Codigo: Integer; Cobranca: String);
var
  vSoma,vComuns: Double;
begin
  dm.cBaixas.Edit;
  if (Cobranca='JG') or (Cobranca='SC') then
  begin
      dm.cBaixasEMOLUMENTOS.AsFloat :=0;
      dm.cBaixasFETJ.AsFloat        :=0;
      dm.cBaixasFUNDPERJ.AsFloat    :=0;
      dm.cBaixasFUNPERJ.AsFloat     :=0;
      dm.cBaixasFUNARPEN.AsFloat    :=0;
      dm.cBaixasPMCMV.AsFloat       :=0;
      dm.cBaixasISS.AsFloat         :=0;
      dm.cBaixasMUTUA.AsFloat       :=0;
      dm.cBaixasACOTERJ.AsFloat     :=0;
      dm.cBaixasDISTRIBUICAO.AsFloat:=0;
      dm.cBaixasTOTAL.AsFloat       :=0;
  end
  else
  begin
      vSoma:=0;
      vComuns:=0;

      dm.Codigos.Locate('COD',Codigo,[]);

      dm.Itens.Close;
      dm.Itens.Params[0].AsInteger:=Codigo;
      dm.Itens.Open;
      dm.Itens.First;
      while not dm.Itens.Eof do
      begin
          dm.RxCustas.Append;
          dm.RxCustasID_ATO.AsInteger   :=dm.cBaixasID_ATO.AsInteger;
          dm.RxCustasTABELA.AsString    :=dm.ItensTAB.AsString;
          dm.RxCustasITEM.AsString      :=dm.ItensITEM.AsString;
          dm.RxCustasSUBITEM.AsString   :=dm.ItensSUB.AsString;
          dm.RxCustasVALOR.AsFloat      :=dm.ItensVALOR.AsFloat;
          dm.RxCustasQTD.AsString       :=dm.ItensQTD.AsString;
          dm.RxCustasTOTAL.AsFloat      :=dm.ItensTOTAL.AsFloat;
          dm.RxCustasDESCRICAO.AsString :=dm.ItensDESCR.AsString;
          dm.RxCustas.Post;

          if dm.RxCustasTABELA.AsString='16' then
          vComuns:=vComuns+dm.RxCustasTOTAL.AsFloat;
          vSoma:=vSoma+dm.RxCustasTOTAL.AsFloat;

          dm.Itens.Next;
      end;

      if Cobranca='NH' then
        dm.cBaixasEMOLUMENTOS.AsFloat:=0
          else dm.cBaixasEMOLUMENTOS.AsFloat:=vSoma;

      dm.cBaixasFETJ.AsFloat        :=GR.NoRound(vSoma*0.2,2);
      dm.cBaixasFUNDPERJ.AsFloat    :=GR.NoRound(vSoma*0.05,2);
      dm.cBaixasFUNPERJ.AsFloat     :=GR.NoRound(vSoma*0.05,2);
      dm.cBaixasFUNARPEN.AsFloat    :=GR.NoRound(vSoma*0.04,2);
      dm.cBaixasPMCMV.AsFloat       :=GR.NoRound((vSoma-vComuns)*0.02,2);
      dm.cBaixasISS.AsFloat         :=GR.NoRound(vSoma*Gdm.vAliquotaISS,2);
      dm.cBaixasMUTUA.AsFloat       :=dm.CodigosMUTUA.AsFloat;
      dm.cBaixasACOTERJ.AsFloat     :=dm.CodigosACOTERJ.AsFloat;
      dm.cBaixasDISTRIBUICAO.AsFloat:=dm.CodigosDISTRIB.AsFloat;

      if dm.cBaixasCONVENIO.AsString='S' then
        dm.cBaixasAPONTAMENTO.AsFloat:=dm.MontarTabelaConvenio(dm.cBaixasCODIGO.AsInteger,
                                                               dm.cBaixasID_ATO.AsInteger,
                                                               GR.iif(dm.cBaixasSALDO_TITULO.AsFloat<>0,
                                                                      dm.cBaixasSALDO_TITULO.AsFloat,
                                                                      dm.cBaixasVALOR_TITULO.AsFloat))
          else
            dm.cBaixasAPONTAMENTO.AsFloat:=0;

      dm.cBaixasTOTAL.AsFloat:=dm.cBaixasEMOLUMENTOS.AsFloat+
                               dm.cBaixasFETJ.AsFloat+
                               dm.cBaixasFUNDPERJ.AsFloat+
                               dm.cBaixasFUNPERJ.AsFloat+
                               dm.cBaixasFUNARPEN.AsFloat+
                               dm.cBaixasPMCMV.AsFloat+
                               dm.cBaixasISS.AsFloat+
                               dm.cBaixasMUTUA.AsFloat+
                               dm.cBaixasACOTERJ.AsFloat+
                               dm.cBaixasDISTRIBUICAO.AsFloat+
                               dm.cBaixasAPONTAMENTO.AsFloat;
  end;
  dm.cBaixas.Post;
end;

procedure TFCancelado2014.btConfirmarClick(Sender: TObject);
begin
  dm.vOkGeral:=True;
  Close;
end;

procedure TFCancelado2014.btCancelarClick(Sender: TObject);
begin
  dm.vOkGeral:=False;
  Gdm.LiberarSelo('PROTESTO','',False,-1,dm.cBaixasID_RESERVADO.AsInteger);
  Close;
end;

procedure TFCancelado2014.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if not dm.vOkGeral then
  btCancelar.Click;
end;

procedure TFCancelado2014.N11Click(Sender: TObject);
begin
  dm.ApagarRXCustas(dm.cBaixasID_ATO.AsInteger);
  dm.cBaixas.Edit;
  dm.cBaixasTIPO_COBRANCA.AsString:='JUSTI�A GRATUITA';
  dm.cBaixasEMOLUMENTOS.AsFloat   :=0;
  dm.cBaixasFETJ.AsFloat          :=0;
  dm.cBaixasFUNDPERJ.AsFloat      :=0;
  dm.cBaixasFUNPERJ.AsFloat       :=0;
  dm.cBaixasFUNARPEN.AsFloat      :=0;
  dm.cBaixasPMCMV.AsFloat         :=0;
  dm.cBaixasISS.AsFloat           :=0;
  dm.cBaixasMUTUA.AsFloat         :=0;
  dm.cBaixasACOTERJ.AsFloat       :=0;
  dm.cBaixasDISTRIBUICAO.AsFloat  :=0;
  dm.cBaixasAPONTAMENTO.AsFloat   :=0;
  dm.cBaixasTOTAL.AsFloat         :=0;
  dm.cBaixas.Post;
end;

procedure TFCancelado2014.N21Click(Sender: TObject);
begin
  dm.ApagarRXCustas(dm.cBaixasID_ATO.AsInteger);
  dm.cBaixas.Edit;
  dm.cBaixasTIPO_COBRANCA.AsString:='COM COBRAN�A';
  dm.cBaixas.Post;
  if dm.cBaixasCONVENIO.AsString='S' then
    Custas(4038,'CC')
      else Custas(4010,'CC');
end;

procedure TFCancelado2014.N31Click(Sender: TObject);
begin
  dm.ApagarRXCustas(dm.cBaixasID_ATO.AsInteger);
  dm.cBaixas.Edit;
  dm.cBaixasTIPO_COBRANCA.AsString:='SEM COBRAN�A';
  dm.cBaixasEMOLUMENTOS.AsFloat   :=0;
  dm.cBaixasFETJ.AsFloat          :=0;
  dm.cBaixasFUNDPERJ.AsFloat      :=0;
  dm.cBaixasFUNPERJ.AsFloat       :=0;
  dm.cBaixasFUNARPEN.AsFloat      :=0;
  dm.cBaixasPMCMV.AsFloat         :=0;
  dm.cBaixasISS.AsFloat           :=0;
  dm.cBaixasMUTUA.AsFloat         :=0;
  dm.cBaixasACOTERJ.AsFloat       :=0;
  dm.cBaixasDISTRIBUICAO.AsFloat  :=0;
  dm.cBaixasAPONTAMENTO.AsFloat   :=0;
  dm.cBaixasTOTAL.AsFloat         :=0;
  dm.cBaixas.Post;
end;

procedure TFCancelado2014.NIHIL1Click(Sender: TObject);
begin
  dm.ApagarRXCustas(dm.cBaixasID_ATO.AsInteger);
  dm.cBaixas.Edit;
  dm.cBaixasTIPO_COBRANCA.AsString:='NIHIL';
  dm.cBaixas.Post;
  if dm.cBaixasCONVENIO.AsString='S' then
    Custas(4038,'NH')
      else Custas(4010,'NH');
end;

procedure TFCancelado2014.btCustasClick(Sender: TObject);
begin
  dm.vIdAto:=dm.cBaixasID_ATO.AsInteger;
  dm.RxCustas.Filtered:=False;
  dm.RxCustas.Filtered:=True;

  dm.vEmolumentos   :=dm.cBaixasEMOLUMENTOS.AsFloat;
  dm.vFetj          :=dm.cBaixasFETJ.AsFloat;
  dm.vFundperj      :=dm.cBaixasFUNDPERJ.AsFloat;
  dm.vFunperj       :=dm.cBaixasFUNPERJ.AsFloat;
  dm.vFunarpen      :=dm.cBaixasFUNARPEN.AsFloat;
  dm.vPmcmv         :=dm.cBaixasPMCMV.AsFloat;
  dm.vIss           :=dm.cBaixasISS.AsFloat;
  dm.vMutua         :=dm.cBaixasMUTUA.AsFloat;
  dm.vAcoterj       :=dm.cBaixasACOTERJ.AsFloat;
  dm.vDistribuicao  :=dm.cBaixasDISTRIBUICAO.AsFloat;
  dm.vAAR           :=0;
  dm.vApontamento   :=dm.cBaixasAPONTAMENTO.AsFloat;
  dm.vTarifa        :=0;
  dm.vTotal         :=dm.cBaixasTOTAL.AsFloat;

  GR.CriarForm(TFCustas,FCustas);

  if dm.vOkGeral then
  begin
      dm.cBaixas.Edit;
      dm.cBaixasEMOLUMENTOS.AsFloat :=dm.vEmolumentos;
      dm.cBaixasFETJ.AsFloat        :=dm.vFetj;
      dm.cBaixasFUNDPERJ.AsFloat    :=dm.vFundperj;
      dm.cBaixasFUNPERJ.AsFloat     :=dm.vFunperj;
      dm.cBaixasFUNARPEN.AsFloat    :=dm.vFunarpen;
      dm.cBaixasPMCMV.AsFloat       :=dm.vPmcmv;
      dm.cBaixasISS.AsFloat         :=dm.vIss;
      dm.cBaixasMUTUA.AsFloat       :=dm.vMutua;
      dm.cBaixasACOTERJ.AsFloat     :=dm.vAcoterj;
      dm.cBaixasDISTRIBUICAO.AsFloat:=dm.vDistribuicao;
      dm.cBaixasAPONTAMENTO.AsFloat :=dm.vApontamento;
      dm.cBaixasTOTAL.AsFloat       :=dm.vTotal;
      dm.cBaixas.Post;
  end;
  dm.vOkGeral:=False;
end;

procedure TFCancelado2014.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then btCancelar.Click;
end;

procedure TFCancelado2014.F1Click(Sender: TObject);
begin
  dm.ApagarRXCustas(dm.cBaixasID_ATO.AsInteger);
  dm.cBaixas.Edit;
  dm.cBaixasTIPO_COBRANCA.AsString:='FOR�A DA LEI';
  dm.cBaixasEMOLUMENTOS.AsFloat   :=0;
  dm.cBaixasFETJ.AsFloat          :=0;
  dm.cBaixasFUNDPERJ.AsFloat      :=0;
  dm.cBaixasFUNPERJ.AsFloat       :=0;
  dm.cBaixasFUNARPEN.AsFloat      :=0;
  dm.cBaixasPMCMV.AsFloat         :=0;
  dm.cBaixasISS.AsFloat           :=0;
  dm.cBaixasMUTUA.AsFloat         :=0;
  dm.cBaixasACOTERJ.AsFloat       :=0;
  dm.cBaixasDISTRIBUICAO.AsFloat  :=0;
  dm.cBaixasAPONTAMENTO.AsFloat   :=0;
  dm.cBaixasTOTAL.AsFloat         :=0;
  dm.cBaixas.Post;
end;

end.
