object FQuantidadeSelo: TFQuantidadeSelo
  Left = 760
  Top = 280
  Cursor = crHandPoint
  BorderIcons = [biSystemMenu]
  BorderStyle = bsNone
  Caption = 'QUANTIDADE DISPON'#205'VEL'
  ClientHeight = 49
  ClientWidth = 166
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDefault
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 1
    Top = 1
    Width = 165
    Height = 48
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Color = clWindow
    TabOrder = 0
    object Bevel1: TBevel
      Left = 0
      Top = 22
      Width = 163
      Height = 1
      Cursor = crSizeAll
    end
    object lbSerie: TLabel
      Left = 2
      Top = 2
      Width = 161
      Height = 21
      Cursor = crSizeAll
      Align = alTop
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'SELO: 00100 '
      Color = 8421631
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = False
      Layout = tlCenter
      OnMouseMove = lbSerieMouseMove
    end
    object lbCCT: TLabel
      Left = 2
      Top = 24
      Width = 161
      Height = 22
      Cursor = crSizeAll
      Align = alBottom
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CCT: 00100 '
      Color = 8454016
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = False
      Layout = tlCenter
      OnMouseMove = lbSerieMouseMove
    end
  end
  object Timer: TTimer
    OnTimer = TimerTimer
    Left = 32
    Top = 112
  end
end
