object FRelSustacao: TFRelSustacao
  Left = 375
  Top = 286
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Ordem de Protesto'
  ClientHeight = 211
  ClientWidth = 273
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 15
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 273
    Height = 161
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sGroupBox1: TsGroupBox
      Left = 24
      Top = 16
      Width = 225
      Height = 121
      Caption = 'Protocolo'
      TabOrder = 0
      SkinData.SkinSection = 'GROUPBOX'
      object edInicial: TsEdit
        Left = 70
        Top = 32
        Width = 121
        Height = 23
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnExit = edInicialExit
        OnKeyPress = edInicialKeyPress
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Inicial'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
      end
      object edFinal: TsEdit
        Left = 70
        Top = 80
        Width = 121
        Height = 23
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnKeyPress = edFinalKeyPress
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Final'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
      end
    end
  end
  object btVisualizar: TsBitBtn
    Left = 80
    Top = 174
    Width = 91
    Height = 25
    Cursor = crHandPoint
    Caption = 'Visualizar'
    TabOrder = 1
    OnClick = btVisualizarClick
    ImageIndex = 4
    Images = dm.Imagens
    SkinData.SkinSection = 'BUTTON'
  end
  object btSair: TsBitBtn
    Left = 176
    Top = 174
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Caption = 'Sair'
    TabOrder = 2
    OnClick = btSairClick
    ImageIndex = 7
    Images = dm.Imagens
    SkinData.SkinSection = 'BUTTON'
  end
  object Protocolos: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      ''
      'DT_SUSTADO,TIPO_SUSTACAO,'
      'RETORNO_PROTESTO,DT_RETORNO_PROTESTO,'
      'DT_DEFINITIVA,SELO_PAGAMENTO,ALEATORIO_SOLUCAO'
      ''
      'from titulos where PROTOCOLO between :P1 and :P2'
      ''
      'and not TIPO_SUSTACAO is null')
    Left = 24
    Top = 168
    ParamData = <
      item
        Name = 'P1'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'P2'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object ProtocolosDT_SUSTADO: TDateField
      FieldName = 'DT_SUSTADO'
      Origin = 'DT_SUSTADO'
    end
    object ProtocolosTIPO_SUSTACAO: TStringField
      FieldName = 'TIPO_SUSTACAO'
      Origin = 'TIPO_SUSTACAO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosRETORNO_PROTESTO: TStringField
      FieldName = 'RETORNO_PROTESTO'
      Origin = 'RETORNO_PROTESTO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosDT_RETORNO_PROTESTO: TDateField
      FieldName = 'DT_RETORNO_PROTESTO'
      Origin = 'DT_RETORNO_PROTESTO'
    end
    object ProtocolosDT_DEFINITIVA: TDateField
      FieldName = 'DT_DEFINITIVA'
      Origin = 'DT_DEFINITIVA'
    end
    object ProtocolosSELO_PAGAMENTO: TStringField
      FieldName = 'SELO_PAGAMENTO'
      Origin = 'SELO_PAGAMENTO'
      Size = 9
    end
    object ProtocolosALEATORIO_SOLUCAO: TStringField
      FieldName = 'ALEATORIO_SOLUCAO'
      Origin = 'ALEATORIO_SOLUCAO'
      Size = 3
    end
  end
end
