unit UQuickCertidaoCancelamento1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QuickRpt, QRCtrls, ExtCtrls, StdCtrls, ComCtrls;

type
  TFQuickCertidaoCancelamento1 = class(TForm)
    Cancelado: TQuickRep;
    QRBand1: TQRBand;
    RE: TRichEdit;
    QRRichText1: TQRRichText;
    ChildBand1: TQRChildBand;
    lbData: TQRLabel;
    QRShape2: TQRShape;
    lbTabeliao: TQRLabel;
    lbMatricula: TQRLabel;
    QRLabel3: TQRLabel;
    M: TQRMemo;
    QRLabel1: TQRLabel;
    lbCGJ1: TQRLabel;
    lbCGJ2: TQRLabel;
    lbCGJ3: TQRLabel;
    lbSeloEletronico: TQRLabel;
    lbCGJ4: TQRLabel;
    lbCGJ5: TQRLabel;
    lbCidade: TQRLabel;
    lbTitular: TQRLabel;
    lbCartorio: TQRLabel;
    QRLabel2: TQRLabel;
    lbRecibo: TQRLabel;
    QRShape3: TQRShape;
    lbProcolo: TQRLabel;
    lbTitulo: TQRLabel;
    lbValor: TQRLabel;
    lbVencimento: TQRLabel;
    lbCedente: TQRLabel;
    lbPortador: TQRLabel;
    QRShape1: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    lb2Via: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure CanceladoEndPage(Sender: TCustomQuickRep);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickCertidaoCancelamento1: TFQuickCertidaoCancelamento1;

implementation

uses UDM, UPF, DB, UGeral;

{$R *.dfm}

procedure TFQuickCertidaoCancelamento1.FormCreate(Sender: TObject);
var
  Documento: String;
begin
  {PEDRO - 29/03/2016}
  if GR.Pergunta('DESEJA IMPRIMIR NA FOLHA DE SEGURAN�A') then
  begin
      if dm.vFlTiracabecalho = 'S' then
      begin
        QRBand1.Enabled := False;
      end
      else
      begin
        QRBand1.Enabled := True;
      end;

      QRBand1.Frame.DrawTop    := False;
      QRBand1.Frame.DrawBottom := False;
      QRBand1.Frame.DrawLeft   := False;
      QRBand1.Frame.DrawRight  := False;

      ChildBand1.Frame.DrawTop    := False;
      ChildBand1.Frame.DrawBottom := False;
      ChildBand1.Frame.DrawLeft   := False;
      ChildBand1.Frame.DrawRight  := False;


      Cancelado.Page.BottomMargin := StrToFloat(dm.vFlBottom);
      Cancelado.Page.TopMargin    := StrToFloat(dm.vFlTop);
      Cancelado.Page.LeftMargin   := StrToFloat(dm.vFlLeft);
      Cancelado.Page.RightMargin  := StrToFloat(dm.vFlRight);

      if dm.vFlFolha ='A4' then
      begin
        Cancelado.Page.Width := 210.0;
        Cancelado.Page.Length:= 297.0;
      end;

      if dm.vFlFolha ='Legal' then
      begin
        Cancelado.Page.Width := 215.9;
        Cancelado.Page.Length:= 355.6;
      end;
  end;

  lb2Via.Enabled            :=dm.v2Via;
  lbCartorio.Caption        :=dm.ServentiaDESCRICAO.AsString;
  lbCidade.Caption          :=dm.ServentiaCIDADE.AsString+' - Rio de Janeiro';
  lbTitular.Caption         :=dm.ServentiaTABELIAO.AsString+' - '+dm.ServentiaTITULO.AsString;
  lbData.Caption            :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',Now);
  lbTabeliao.Caption        :=dm.vAssinatura;
  lbMatricula.Caption       :=dm.vMatricula;
  lbRecibo.Caption          :='Recibo: '+dm.CertidoesRECIBO.AsString;
  lbSeloEletronico.Caption  :=GR.SeloFormatado(dm.CertidoesSELO.AsString,dm.CertidoesALEATORIO.AsString);

  if dm.TitulosTIPO_DEVEDOR.AsString='F' then
    Documento:='CPF '+PF.FormatarCPF(dm.TitulosCPF_CNPJ_DEVEDOR.AsString)
      else Documento:='CNPJ '+PF.FormatarCNPJ(dm.TitulosCPF_CNPJ_DEVEDOR.AsString);

  RE.Lines.Add('CERTIFICO E DOU F�, QUE O PRESENTE REGISTRO DE PROTESTO FOI CANCELADO EM '+FormatDateTime('dd/mm/yyyy',dm.TitulosDT_PAGAMENTO.AsDateTime)+
               ', NO LIVRO '+dm.TitulosLIVRO_REGISTRO.AsString+
               ', FLS. '+dm.TitulosFOLHA_REGISTRO.AsString+', REGISTRO '+dm.TitulosREGISTRO.AsString+', PROTOCOLO '+dm.TitulosPROTOCOLO.AsString+
               ', CONSTANDO COMO DEVEDOR '+dm.TitulosDEVEDOR.AsString+', '+Documento+'.');

  RE.Text:=PF.JstParagrafo(RE.Text,85);

  PF.CarregarCustas(dm.CertidoesID_CERTIDAO.AsInteger);
  dm.RxCustas.First;
  while not dm.RxCustas.Eof do
  begin
      M.Lines.Add(dm.RxCustasTABELA.AsString+'.'+dm.RxCustasITEM.AsString+'.'+dm.RxCustasSUBITEM.AsString+'   '+dm.RxCustasDESCRICAO.AsString+' x '+dm.RxCustasQTD.AsString+Format('%-1s %7.2f',[' =',dm.RxCustasTOTAL.AsFloat]));
      dm.RxCustas.Next;
  end;

  M.Lines.Add(Format('%-1s %7.2f',['Emolumentos =',dm.CertidoesEMOLUMENTOS.AsFloat]));
  M.Lines.Add(Format('%-1s %7.2f',['Fetj =',dm.CertidoesFETJ.AsFloat]));
  M.Lines.Add(Format('%-1s %7.2f',['Fundperj =',dm.CertidoesFUNDPERJ.AsFloat]));
  M.Lines.Add(Format('%-1s %7.2f',['Funperj =',dm.CertidoesFUNPERJ.AsFloat]));
  M.Lines.Add(Format('%-1s %7.2f',['Funarpen =',dm.CertidoesFUNARPEN.AsFloat]));
  M.Lines.Add(Format('%-1s %7.2f',['Pmcmv =',dm.CertidoesPMCMV.AsFloat]));
  M.Lines.Add(Format('%-1s %7.2f',['Iss =',dm.CertidoesISS.AsFloat]));
  M.Lines.Add(Format('%-1s %7.2f',['Total =',dm.CertidoesTOTAL.AsFloat]));

  lbProcolo.Caption    :='Protocolo: '+dm.TitulosPROTOCOLO.AsString+'   Data do Protesto: '+FormatDateTime('dd/mm/yyyy',dm.TitulosDT_REGISTRO.AsDateTime);
  lbTitulo.Caption     :='N� T�tulo: '+dm.TitulosNUMERO_TITULO.AsString;
  lbCedente.Caption    :='Cedente: '+dm.TitulosCEDENTE.AsString;
  lbPortador.Caption   :='Portador: '+dm.TitulosAPRESENTANTE.AsString;
  lbValor.Caption      :='Valor R$: '+FloatToStrF(dm.TitulosVALOR_TITULO.AsFloat,ffNumber,7,2);

  if dm.TitulosDT_VENCIMENTO.AsDateTime<>0 then
    lbVencimento.Caption:='Vencimento: '+FormatDateTime('dd/mm/yyyy',dm.TitulosDT_VENCIMENTO.AsDateTime)
      else lbVencimento.Caption:='Vencimento: ';

  if dm.ServentiaCODIGO.AsInteger=1554 then
  begin
      lbMatricula.Enabled:=False;
      lbTabeliao.Caption:='Tabeli�o do Protesto';
  end;

  Cancelado.Preview;
end;

procedure TFQuickCertidaoCancelamento1.CanceladoEndPage(
  Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

end.
