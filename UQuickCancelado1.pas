unit UQuickCancelado1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls;

type
  TFQuickCancelado1 = class(TForm)
    Carimbo: TQuickRep;
    Titulo1: TQRBand;
    QRLabel1: TQRLabel;
    lbTexto: TQRLabel;
    lbTalao: TQRLabel;
    lbEspecie: TQRLabel;
    lbTitulo: TQRLabel;
    lbSelo: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel2: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel14: TQRLabel;
    Titulo2: TQRBand;
    QRShape3: TQRShape;
    QRLabel15: TQRLabel;
    lbTexto2: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    lbTalao2: TQRLabel;
    lbEspecie2: TQRLabel;
    lbTitulo2: TQRLabel;
    lbSelo2: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRShape4: TQRShape;
    QRLabel32: TQRLabel;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRLabel33: TQRLabel;
    lbCGJ1: TQRLabel;
    lbCGJ2: TQRLabel;
    lbCGJ3: TQRLabel;
    lbSeloEletronico: TQRLabel;
    lbCGJ4: TQRLabel;
    lbCGJ5: TQRLabel;
    lbNome: TQRLabel;
    QRShape5: TQRShape;
    QRLabel3: TQRLabel;
    QRDBText19: TQRDBText;
    lbProtocolo: TQRLabel;
    QRLabel5: TQRLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickCancelado1: TFQuickCancelado1;

implementation

uses UDM, UPF, DB, UGeral, StrUtils;

{$R *.dfm}

procedure TFQuickCancelado1.FormCreate(Sender: TObject);
begin
  lbTexto.Caption           :='O protesto referente a este documento foi cancelado em '+FormatDateTime('dd/mm/yyyy.',dm.TitulosDT_PAGAMENTO.AsDateTime);
  lbTalao.Caption           :='Tal�o: '+IfThen(dm.CertidoesRECIBO.AsInteger<>0,dm.CertidoesRECIBO.AsString,'');
  lbEspecie.Caption         :='Esp�cie: '+PF.RetornarSigla(dm.TitulosTIPO_TITULO.AsInteger);
  lbTitulo.Caption          :='N� T�tulo: '+dm.TitulosNUMERO_TITULO.AsString;
  lbSelo.Caption            :='Selo: '+GR.SeloFormatado(dm.CertidoesSELO.AsString,dm.CertidoesALEATORIO.AsString);

  lbTexto2.Caption          :=lbTexto.Caption;
  lbTalao2.Caption          :=IfThen(dm.CertidoesRECIBO.AsInteger<>0,dm.CertidoesRECIBO.AsString,'');
  lbEspecie2.Caption        :=PF.RetornarSigla(dm.TitulosTIPO_TITULO.AsInteger);
  lbTitulo2.Caption         :=dm.TitulosNUMERO_TITULO.AsString;
  lbSelo2.Caption           :=GR.SeloFormatado(dm.CertidoesSELO.AsString,dm.CertidoesALEATORIO.AsString);
  lbSeloEletronico.Caption  :=lbSelo2.Caption;
  lbProtocolo.Caption       :=dm.fProtocolo(dm.CertidoesID_ATO.AsString);
  lbNome.Caption            :=PF.NomeEscrevente(dm.CertidoesESCREVENTE.AsString);

  if dm.ServentiaCODIGO.AsInteger=1726 then
  begin
      Titulo1.Enabled:=False;
      Titulo2.Enabled:=True;
  end;

  Carimbo.Preview;
end;

end.
