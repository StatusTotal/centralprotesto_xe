�
 TFQUICKCERTIDAOCANCELAMENTO1 0�;  TPF0TFQuickCertidaoCancelamento1FQuickCertidaoCancelamento1LeftTop� WidthoHeight�VertScrollBar.Position�Caption
Cancelado2Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderScaledOnCreate
FormCreatePixelsPerInch`
TextHeight 	TQuickRep	CanceladoLeftTop5WidthHeightcFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' 	OnEndPageCanceladoEndPageOptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeCustomPage.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCode	PrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPrevInitialZoomqrZoomToWidthPreviewDefaultSaveTypestQRP TQRBandQRBand1Left9Top9Width�Height� Frame.ColorclBlackFrame.DrawTop	Frame.DrawBottomFrame.DrawLeft	Frame.DrawRight	AlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.Values      �@      :�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbTitle TQRRichTextQRRichText1LeftTop� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUU�@VUUUUU��@UUUUUU��	@ 	AlignmenttaLeftJustifyAutoStretch	ColorclWindowFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style ParentRichEditRE  TQRLabellbCidadeLeft TopEWidthiHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@      ��@      ��@      �@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaptionCidade - EstadoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsItalic 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabel	lbTitularLeftTop.WidthkHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@������ֽ@������j�@UUUUUU��@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaptionNome do TitularColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsItalic 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabel
lbCartorioLeft� TopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������j�@UUUUUUm�@      ��@UUUUUU��@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaption   NOME DO CARTÓRIOColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel2Left� ToppWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������J�@������Z�@������*�@UUUUUU�@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaption   CERTIDÃO DE CANCELAMENTOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabellbReciboLeft?Top� Width+HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@VUUUUU�@VUUUUU�@��������@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaptionRecibo:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize	  TQRLabellb2ViaLeftATop� Width&HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      T�@      ��@UUUUUU�@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaption	   (2ª Via)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize	   TQRChildBand
ChildBand1Left9TopWidth�HeightOFrame.ColorclBlackFrame.DrawTopFrame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.Values��������@      :�	@ PreCaluculateBandHeightKeepOnOnePage
ParentBandQRBand1
PrintOrdercboAfterParent TQRLabellbDataLeft� TopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values ������@ XUUUU��@ ������@ ������	@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaptionCidade, DataColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize	  TQRShapeQRShape2Left� Top� WidthRHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values XUUUUU� @ �����
�@ �������@ �������@ Brush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabel
lbTabeliaoLeft�Top� Width!HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������J�	@UUUUUU��@      ��@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption	lbTitularColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabellbMatriculaLeft�Top� Width&HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU��	@������Z�@UUUUUU�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption	MatriculaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel3LeftTopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������
�@UUUUUUU�@UUUUUUQ�@��������	@ 	AlignmenttaCenterAlignToBand	AutoSizeAutoStretch	Caption�   Esta Certidão é gerada de forma eletrônica, qualquer forma de rasura ou complementação a tornará como inválida, ou como princípio de fraude. (Art. 559 - Resolução 01/2000- CGJERJ) - Válida somente se aposta de selo de fiscalização.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRMemoMLeftTop;Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUU�@     \�@��������	@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretch	ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel1LeftTop+Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��	@��������@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionTaxas e Emolumentos:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabellbCGJ1Left(Top� WidthhHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������@UUUUUUU�@UUUUUU��@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption   Poder Judiciário - TJERJColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRLabellbCGJ2LeftTop� Width}HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUu�@�������@UUUUUU]�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption   Corregedoria Geral da JustiçaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRLabellbCGJ3LeftTop� Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��@      p�@��������@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption"   Selo de Fiscalização EletrônicoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRLabellbSeloEletronicoLeft1Top� WidthVHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU��@��������@��������@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaptionLLLL 00000 XXXColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRLabellbCGJ4LeftTop� Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@       �@VUUUUU��@�������@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaptionConsulte a validade do selo em:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRLabellbCGJ5LeftTop� Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@VUUUUU��@������b�@UUUUUU5�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption$https://www3.tjrj.jus.br/sitepublicoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRShapeQRShape3LeftTopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU� @UUUUUUU�@UUUUUU��@��������	@ Brush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabel	lbProcoloLeftTopWidth;HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@UUUUUU�@�������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
Protocolo:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize	  TQRLabellbTituloLeftfTopWidth2HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU�	@UUUUUU�@������J�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Nº Título:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize	  TQRLabellbValorLeftdTop@Width4HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������g�	@UUUUUUU�@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption	Valor R$:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize	  TQRLabellbVencimentoLeftPTop*WidthHHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������	@     @�@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionVencimento:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize	  TQRLabel	lbCedenteLeftTop*Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@     @�@UUUUUU_�	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionCedente:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize	  TQRLabel
lbPortadorLeftTop@Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@UUUUUUU�@UUUUUU_�	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption	Portador:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize	  TQRShapeQRShape1LeftTopUWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU� @UUUUUUU�@UUUUUU��@��������	@ Brush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShapeQRShape4LeftTopWidthHeightIFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU%�@UUUUUUU�@UUUUUU��@UUUUUUU� @ Brush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShapeQRShape5Left�TopWidthHeightIFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU%�@VUUUUU��	@UUUUUU��@UUUUUUU� @ Brush.ColorclBlackShapeqrsRectangle
VertAdjust     	TRichEditRELeftTop5Width�HeightFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTabOrder   