unit UReciboApontamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, Buttons, sBitBtn, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit;

type
  TFReciboApontamento = class(TForm)
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    P1: TsPanel;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    procedure btSairClick(Sender: TObject);
    procedure btVisualizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FReciboApontamento: TFReciboApontamento;

implementation

uses UQuickReciboApontamento, DB, UPF, UGeral, UDM, Math;

{$R *.dfm}

procedure TFReciboApontamento.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFReciboApontamento.btVisualizarClick(Sender: TObject);
var
  CodigoApresentante: String;
begin
  Application.CreateForm(TFQuickReciboApontamento,FQuickReciboApontamento);
  with FQuickReciboApontamento do
  begin
      qryProtocolos.Close;
      qryProtocolos.Params[0].AsDate:=edInicio.Date;
      qryProtocolos.Params[1].AsDate:=edFim.Date;
      qryProtocolos.Open;

      vReciboInicial:=-1;

      RX.Close;
      RX.Open;
      qryProtocolos.First;
      CodigoApresentante:=qryProtocolosCODIGO_APRESENTANTE.AsString;
      while not qryProtocolos.Eof do
      begin
          PF.Aguarde(True);

          if qryProtocolosRECIBO.AsInteger=0 then
          begin
              qryProtocolos.Edit;
              qryProtocolosRECIBO.AsInteger:=StrToInt(dm.ValorParametro(11));
              dm.AtualizarParametro(11,IntToStr(qryProtocolosRECIBO.AsInteger+1));
              qryProtocolos.Post;
              qryProtocolos.ApplyUpdates(0);
          end;

          if vReciboInicial=-1 then vReciboInicial:=qryProtocolosRECIBO.AsInteger;

          if qryProtocolosCODIGO_APRESENTANTE.AsString=CodigoApresentante then
          begin
              if RX.Locate('FAIXA',qryProtocolosFaixa.AsString,[]) then
                RX.Edit
                  else RX.Append;

              RXDT_PROTOCOLO.AsDateTime       :=qryProtocolosDT_PROTOCOLO.AsDateTime;
              RXAPRESENTANTE.AsString         :=qryProtocolosAPRESENTANTE.AsString;
              RXCODIGO_APRESENTANTE.AsString  :=qryProtocolosCODIGO_APRESENTANTE.AsString;
              RXFAIXA.AsString                :=qryProtocolosFaixa.AsString;

              if RXPROTOCOLOS.AsString='' then
                RXPROTOCOLOS.AsString:='Protocolos faixa '+qryProtocolosFaixa.AsString+': '+qryProtocolosPROTOCOLO.AsString
                  else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+','+qryProtocolosPROTOCOLO.AsString;

              RXEMOLUMENTOS.AsFloat     :=RXEMOLUMENTOS.AsFloat+qryProtocolosEMOLUMENTOS.AsFloat;
              RXFETJ.AsFloat            :=RXFETJ.AsFloat+qryProtocolosFETJ.AsFloat;
              RXFUNDPERJ.AsFloat        :=RXFUNDPERJ.AsFloat+qryProtocolosFUNDPERJ.AsFloat;
              RXFUNPERJ.AsFloat         :=RXFUNPERJ.AsFloat+qryProtocolosFUNPERJ.AsFloat;
              RXFUNARPEN.AsFloat        :=RXFUNARPEN.AsFloat+qryProtocolosFUNARPEN.AsFloat;
              RXPMCMV.AsFloat           :=RXPMCMV.AsFloat+qryProtocolosPMCMV.AsFloat;
              RXISS.AsFloat             :=RXISS.AsFloat+qryProtocolosISS.AsFloat;
              RXMUTUA.AsFloat           :=RXMUTUA.AsFloat+qryProtocolosMUTUA.AsFloat;
              RXACOTERJ.AsFloat         :=RXACOTERJ.AsFloat+qryProtocolosACOTERJ.AsFloat;
              RXDISTRIBUICAO.AsFloat    :=RXDISTRIBUICAO.AsFloat+qryProtocolosDISTRIBUICAO.AsFloat;
              RXTOTAL.AsFloat           :=RXTOTAL.AsFloat+qryProtocolosTOTAL.AsFloat;
              RXQTD.AsInteger           :=RXQTD.AsInteger+1;

              vReciboFinal:=qryProtocolosRECIBO.AsInteger;

              qryProtocolos.Next;
              if qryProtocolos.Eof then
              begin
                  vReciboFinal:=qryProtocolosRECIBO.AsInteger;
                  Relatorio.Preview;
                  vReciboInicial:=-1;
              end;
          end
          else
          begin
              Relatorio.Preview;
              vReciboInicial:=qryProtocolosRECIBO.AsInteger;
              RX.Close;
              RX.Open;
              CodigoApresentante:=qryProtocolosCODIGO_APRESENTANTE.AsString;
          end;
      end;

      Free;
  end;
end;

procedure TFReciboApontamento.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

end.
