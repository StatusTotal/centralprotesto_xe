unit USustados2014;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, Mask, sMaskEdit, sCustomComboEdit,
  sTooledit, ExtCtrls, sPanel, sEdit, sGroupBox, sCheckBox, sMemo, sLabel;

type
  TFSustado2014 = class(TForm)
    P1: TsPanel;
    edData: TsDateEdit;
    btOk: TsBitBtn;
    btCancelar: TsBitBtn;
    edSelo: TsEdit;
    RgTipo: TsRadioGroup;
    ckImprimir: TsCheckBox;
    MJudicial: TsMemo;
    lbAleatorio: TsLabel;
    procedure btOkClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RgTipoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSustado2014: TFSustado2014;

implementation

uses UDM, UPF, DB, UGeral, UGDM;

{$R *.dfm}

procedure TFSustado2014.btOkClick(Sender: TObject);
begin
  if edData.Date=0 then
  begin
      GR.Aviso('INFORME A DATA DA SUSTA��O!');
      edData.SetFocus;
      Exit;
  end;

  dm.vOkGeral:=True;
  Close;
end;

procedure TFSustado2014.btCancelarClick(Sender: TObject);
begin
  dm.vOkGeral:=False;
  Gdm.LiberarSelo('PROTESTO','',False,-1,dm.vIdReservado);
  Close;
end;

procedure TFSustado2014.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if not dm.vOkGeral then
  btCancelar.Click;
end;

procedure TFSustado2014.RgTipoClick(Sender: TObject);
begin
  {MJudicial.Enabled:=RgTipo.ItemIndex=1;
  if RgTipo.ItemIndex<>1 then MJudicial.Clear;

  if RgTipo.ItemIndex=1 then
  begin
      dm.vIdReservado     :=GR.ValorGeneratorDBX('ID_RESERVADO',Gdm.BDGerencial);
      edSelo.Text         :=Gdm.ProximoSelo(dm.SerieAtual,'PROTESTO',Self.Name,'N','',0,0,0,'SUSTA��O',dm.vNome,Now,dm.vIdReservado);
      lbAleatorio.Caption :=Gdm.vAleatorio;
      edSelo.Enabled      :=True;
  end
  else
  begin
      if dm.vIdReservado<>0 then
      Gdm.LiberarSelo('PROTESTO','',False,-1,dm.vIdReservado);
      edSelo.Clear;
      edSelo.Enabled:=False;
      lbAleatorio.Caption:='';
  end; }
end;

end.
