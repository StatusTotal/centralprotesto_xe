unit UAR;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, Buttons, sBitBtn, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit;

type
  TFAR = class(TForm)
    P1: TsPanel;
    edData: TsDateEdit;
    btOk: TsBitBtn;
    btCancelar: TsBitBtn;
    procedure btOkClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAR: TFAR;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFAR.btOkClick(Sender: TObject);
begin
  dm.vOkGeral:=True;
  Close;
end;

procedure TFAR.btCancelarClick(Sender: TObject);
begin
  dm.vOkGeral:=False;
  Close;
end;

end.
