�
 TFELETRONICO 0�(  TPF0TFEletronicoFEletronicoLeft TopBorderIconsbiSystemMenu BorderStylebsDialogCaptionExportarClientHeight�ClientWidthColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
KeyPreview	OldCreateOrderPositionpoMainFormCenterOnClose	FormCloseOnCreate
FormCreate	OnKeyDownFormKeyDownPixelsPerInch`
TextHeight TsMemoMLeftTopWidth�Height� BorderStylebsNoneColor��� Font.CharsetANSI_CHARSET
Font.Color+5M Font.Height�	Font.NameCourier New
Font.Style 
ParentFontReadOnly	
ScrollBarsssBothTabOrderVisibleBoundLabel.ParentFontBoundLabel.Font.CharsetDEFAULT_CHARSETBoundLabel.Font.ColorclWindowTextBoundLabel.Font.Height�BoundLabel.Font.NameMS Sans SerifBoundLabel.Font.Style SkinData.SkinSectionHINT  TsPanelP1Left Top WidthHeight?AlignalTopTabOrder SkinData.SkinSectionPANEL TsLabelsLabel1Left�TopWidthVHeightSkinSectionSCROLLBTNRIGHTCaptionCancelamento
ParentFontFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.StylefsBold   TsLabelsLabel2Left�TopWidth=HeightSkinSectionSCROLLBTNRIGHTCaption   Eletrônico
ParentFontFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.StylefsBold   TsLabelsLabel3Left�Top,Width<HeightSkinSectionSCROLLBTNRIGHTCaption:::::::::::::::
ParentFontFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.StylefsBold   TsBitBtnbtVisualizarLeft	TopWidthEHeight0CursorcrHandPointCaptionArquivoLayout
blGlyphTopTabOrder OnClickbtVisualizarClick
ImageIndexImages
dm.Imagens	Reflected	SkinData.SkinSectionBUTTON  TsBitBtn
btExportarLeftSTopWidthEHeight0CursorcrHandPointCaption	ConfirmarLayout
blGlyphTopTabOrderOnClickbtExportarClick
ImageIndexImages
dm.Imagens	Reflected	SkinData.SkinSectionBUTTON   	TwwDBGridGridAtosLeft Top?WidthHeightlControlType.StringsCheck;CheckBox;S;N Selected.StringsCheck	4	*	FProtocolo	8	Protocolo	F   NUMERO_TITULO	11	Nº Título	TAPRESENTANTE	22	Apresentante	F   STATUS	13	Situação	TSELO_PAGAMENTO	8	Selo	FDT_PAGAMENTO	10	Cancelamento	FDT_RETIRADO	10	Retirada	FRECIBO_PAGAMENTO	6	Recibo	FVALOR_PAGAMENTO	8	Pago	F IniAttributes.Delimiter;;IniAttributes.UnicodeIniFile
TitleColor	clBtnFace	FixedColsShowHorzScrollBar	EditControlOptionsecoCheckboxSingleClickecoSearchOwnerForm AlignalClientBorderStylebsNone
DataSource	dsTitulosEditCalculated	Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
KeyOptions Options	dgEditingdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit
dgWordWrapdgProportionalColResize 
ParentFont	PopupMenuPMTabOrderTitleAlignmenttaCenterTitleFont.CharsetANSI_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameTahomaTitleFont.Style 
TitleLinesTitleButtons	
UseTFields
OnColEnterGridAtosColEnter  TsStatusBarBarraLeft Top�WidthHeightPanelsTextEncontradosWidthP Width2  SkinData.SkinSection	STATUSBAR  TDataSourcedsPortadoresDataSetdm.PortadoresLeft`Top  TSQLDataSet
dtsTitulosCommandTextIselect * from TITULOS

where PROTOCOLO=:PROTOCOLO

order by PROTOCOLOMaxBlobSize�ParamsDataType	ftIntegerName	PROTOCOLO	ParamTypeptInput  Left� Top�   TDataSetProvider
dspTitulosDataSet
dtsTitulosOptionspoAllowCommandText Left� Top  TClientDataSetTitulos
Aggregates ParamsDataType	ftIntegerName	PROTOCOLO	ParamTypeptInput  ProviderName
dspTitulos	AfterPostTitulosAfterPostOnCalcFieldsTitulosCalcFieldsLeft� Top8 TIntegerFieldTitulosID_ATO	FieldNameID_ATOProviderFlags
pfInUpdate	pfInWherepfInKey Required	  TIntegerFieldTitulosCODIGO	FieldNameCODIGO  TIntegerFieldTitulosRECIBO	AlignmenttaLeftJustify	FieldNameRECIBO  
TDateFieldTitulosDT_PROTOCOLO	FieldNameDT_PROTOCOLO  TIntegerFieldTitulosPROTOCOLO	AlignmenttaLeftJustify	FieldName	PROTOCOLO  TIntegerFieldTitulosLIVRO_PROTOCOLO	AlignmenttaLeftJustify	FieldNameLIVRO_PROTOCOLO  TStringFieldTitulosFOLHA_PROTOCOLO	FieldNameFOLHA_PROTOCOLOSize
  
TDateFieldTitulosDT_PRAZO	FieldNameDT_PRAZO  
TDateFieldTitulosDT_REGISTRO	FieldNameDT_REGISTRO  TIntegerFieldTitulosREGISTRO	FieldNameREGISTRO  TIntegerFieldTitulosLIVRO_REGISTRO	FieldNameLIVRO_REGISTRO  TStringFieldTitulosFOLHA_REGISTRO	FieldNameFOLHA_REGISTROSize
  TStringFieldTitulosSELO_REGISTRO	FieldNameSELO_REGISTROSize  
TDateFieldTitulosDT_PAGAMENTO	AlignmenttaCenter	FieldNameDT_PAGAMENTODisplayFormat
DD/MM/YYYY  TStringFieldTitulosSELO_PAGAMENTO	AlignmenttaCenter	FieldNameSELO_PAGAMENTOSize  TIntegerFieldTitulosRECIBO_PAGAMENTO	FieldNameRECIBO_PAGAMENTO  TStringFieldTitulosCOBRANCA	FieldNameCOBRANCA	FixedChar	Size  TFloatFieldTitulosEMOLUMENTOS	FieldNameEMOLUMENTOSDisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosFETJ	FieldNameFETJDisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosFUNDPERJ	FieldNameFUNDPERJDisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosFUNPERJ	FieldNameFUNPERJDisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosMUTUA	FieldNameMUTUADisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosDISTRIBUICAO	FieldNameDISTRIBUICAODisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosACOTERJ	FieldNameACOTERJDisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosTOTAL	FieldNameTOTALDisplayFormat	#####0.00
EditFormat	#####0.00  TIntegerFieldTitulosTIPO_PROTESTO	FieldNameTIPO_PROTESTO  TIntegerFieldTitulosTIPO_TITULO	FieldNameTIPO_TITULO  TStringFieldTitulosNUMERO_TITULO	FieldNameNUMERO_TITULO  
TDateFieldTitulosDT_TITULO	FieldName	DT_TITULO  TStringFieldTitulosBANCO	FieldNameBANCO  TFloatFieldTitulosVALOR_TITULO	FieldNameVALOR_TITULODisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosSALDO_PROTESTO	FieldNameSALDO_PROTESTODisplayFormat	#####0.00
EditFormat	#####0.00  
TDateFieldTitulosDT_VENCIMENTO	FieldNameDT_VENCIMENTO  TStringFieldTitulosCONVENIO	FieldNameCONVENIO	FixedChar	Size  TStringFieldTitulosTIPO_APRESENTACAO	FieldNameTIPO_APRESENTACAO	FixedChar	Size  
TDateFieldTitulosDT_ENVIO	FieldNameDT_ENVIO  TStringFieldTitulosTIPO_INTIMACAO	FieldNameTIPO_INTIMACAO	FixedChar	Size  
TMemoFieldTitulosMOTIVO_INTIMACAO	FieldNameMOTIVO_INTIMACAOBlobTypeftMemo  
TDateFieldTitulosDT_INTIMACAO	FieldNameDT_INTIMACAO  
TDateFieldTitulosDT_PUBLICACAO	FieldNameDT_PUBLICACAO  TFloatFieldTitulosVALOR_PAGAMENTO	FieldNameVALOR_PAGAMENTODisplayFormat	#####0.00
EditFormat	#####0.00  TStringFieldTitulosAPRESENTANTE	FieldNameAPRESENTANTESized  TStringFieldTitulosCPF_CNPJ_APRESENTANTE	FieldNameCPF_CNPJ_APRESENTANTESize  TStringFieldTitulosTIPO_APRESENTANTE	FieldNameTIPO_APRESENTANTE	FixedChar	Size  TStringFieldTitulosCEDENTE	FieldNameCEDENTESized  TStringFieldTitulosNOSSO_NUMERO	FieldNameNOSSO_NUMEROSize  TStringFieldTitulosSACADOR	FieldNameSACADORSized  TStringFieldTitulosDEVEDOR	FieldNameDEVEDORSized  TStringFieldTitulosCPF_CNPJ_DEVEDOR	FieldNameCPF_CNPJ_DEVEDORSize  TStringFieldTitulosTIPO_DEVEDOR	FieldNameTIPO_DEVEDOR	FixedChar	Size  TStringFieldTitulosAGENCIA_CEDENTE	FieldNameAGENCIA_CEDENTESize  TStringFieldTitulosPRACA_PROTESTO	FieldNamePRACA_PROTESTO  TStringFieldTitulosTIPO_ENDOSSO	FieldNameTIPO_ENDOSSO	FixedChar	Size  TStringFieldTitulosACEITE	FieldNameACEITE	FixedChar	Size  TStringFieldTitulosCPF_ESCREVENTE	FieldNameCPF_ESCREVENTESize  TStringFieldTitulosCPF_ESCREVENTE_PG	FieldNameCPF_ESCREVENTE_PGSize  
TMemoFieldTitulosOBSERVACAO	FieldName
OBSERVACAOBlobTypeftMemo  
TDateFieldTitulosDT_SUSTADO	FieldName
DT_SUSTADO  
TDateFieldTitulosDT_RETIRADO	FieldNameDT_RETIRADO  TStringFieldTitulosSTATUS	AlignmenttaCenter	FieldNameSTATUS  TStringFieldTitulosPROTESTADO	FieldName
PROTESTADO	FixedChar	Size  TStringFieldTitulosENVIADO_PROTESTO	FieldNameENVIADO_PROTESTO	FixedChar	Size  TStringFieldTitulosENVIADO_PAGAMENTO	FieldNameENVIADO_PAGAMENTO	FixedChar	Size  TStringFieldTitulosEXPORTADO	AlignmenttaCenter	FieldName	EXPORTADO	FixedChar	Size  TStringFieldTitulosDocDevedor	FieldKindfkCalculated	FieldName
DocDevedor
Calculated	  TStringFieldTitulosDocApresentante	FieldKindfkCalculated	FieldNameDocApresentanteSize
Calculated	  TStringFieldTitulosCheck	FieldKindfkInternalCalc	FieldNameCheckSize  
TDateFieldTitulosDT_ENTRADA	FieldName
DT_ENTRADA  TStringFieldTitulosAGENCIA	FieldNameAGENCIASize
  TStringFieldTitulosCONTA	FieldNameCONTASize
  TStringFieldTitulosAVALISTA_DEVEDOR	FieldNameAVALISTA_DEVEDOR	FixedChar	Size  TStringFieldTitulosFINS_FALIMENTARES	FieldNameFINS_FALIMENTARES	FixedChar	Size  TFloatFieldTitulosTARIFA_BANCARIA	FieldNameTARIFA_BANCARIA  TStringFieldTitulosFORMA_PAGAMENTO	FieldNameFORMA_PAGAMENTO	FixedChar	Size  TStringFieldTitulosNUMERO_PAGAMENTO	FieldNameNUMERO_PAGAMENTOSize(  TStringFieldTitulosARQUIVO	FieldNameARQUIVO  TStringFieldTitulosRETORNO	FieldNameRETORNO  
TDateFieldTitulosDT_DEVOLVIDO	FieldNameDT_DEVOLVIDO  TStringFieldTitulosCPF_CNPJ_SACADOR	FieldNameCPF_CNPJ_SACADORSize  TIntegerFieldTitulosID_MSG	FieldNameID_MSG  TFloatFieldTitulosVALOR_AR	FieldNameVALOR_AR  TStringFieldTitulosELETRONICO	FieldName
ELETRONICO	FixedChar	Size  TStringFieldTitulosCODIGO_APRESENTANTE	FieldNameCODIGO_APRESENTANTESize
   TDataSource	dsTitulosDataSetTitulosLeft� Toph  TDataSourcedsParametrosDataSetdm.ParametrosLeft`Top8  
TPopupMenuPMLeft`Toph 	TMenuItemMarcarTodos1CaptionMarcar TodosOnClickMarcarTodos1Click  	TMenuItemDesmarcarTodos1CaptionDesmarcar TodosOnClickDesmarcarTodos1Click  	TMenuItemN1Caption-  	TMenuItem
Parmetros1Caption   ParâmetrosOnClickParmetros1Click   TDataSource
dsRemessasDataSetdm.RemessasLeft`Top�   TsOpenDialogopdLeft`Top�    