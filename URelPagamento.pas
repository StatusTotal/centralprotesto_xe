unit URelPagamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FMTBcd, StdCtrls, Buttons, sBitBtn, sEdit, sGroupBox, ExtCtrls,
  sPanel, DB, SqlExpr, sRadioButton, Mask, sMaskEdit, sCustomComboEdit,
  sTooledit, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFRelPagamento = class(TForm)
    P1: TsPanel;
    Gb: TsGroupBox;
    edInicial: TsEdit;
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    edFinal: TsEdit;
    RbProtocolo: TsRadioButton;
    RbData: TsRadioButton;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    qryData: TFDQuery;
    qryProtocolo: TFDQuery;
    qryDataID_ATO: TIntegerField;
    qryProtocoloID_ATO: TIntegerField;
    procedure btVisualizarClick(Sender: TObject);
    procedure edInicialKeyPress(Sender: TObject; var Key: Char);
    procedure btSairClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edFinalKeyPress(Sender: TObject; var Key: Char);
    procedure RbProtocoloClick(Sender: TObject);
    procedure RbDataClick(Sender: TObject);
    procedure edInicialChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelPagamento: TFRelPagamento;

implementation

uses UQuickPagamento1, UDM, UPF, UAssinatura, UGeral;

{$R *.dfm}

procedure TFRelPagamento.btVisualizarClick(Sender: TObject);
var
  I: Integer;
  Lista: WideString;
begin
  if RbProtocolo.Checked then
  begin
      if edInicial.Text='' then
      begin
          edInicial.SetFocus;
          Exit;
      end;

      if edFinal.Text='' then
      edFinal.Text:=edInicial.Text;
  end;

  GR.CriarForm(TFAssinatura,FAssinatura);

  dm.v2Via:=False;
  Application.CreateForm(TFQuickPagamento1,FQuickPagamento1);

  if RbProtocolo.Checked then
  begin
      qryProtocolo.Close;
      qryProtocolo.Params[0].AsInteger:=StrToInt(edInicial.Text);
      qryProtocolo.Params[1].AsInteger:=StrToInt(edFinal.Text);
      qryProtocolo.Open;

      if qryProtocolo.IsEmpty then
        GR.Aviso('Protocolo n�o encontrado.')
      else
      begin
          qryProtocolo.First;
          while not qryProtocolo.Eof do
          begin
              Lista:=Lista+qryProtocoloID_ATO.AsString+',';
              qryProtocolo.Next;
          end;
          Lista[Length(Lista)]:=' ';
          Lista:=Trim(Lista);

          with FQuickPagamento1 do
          begin
              qryImprime.Close;
              qryImprime.SQL.Text:='SELECT * FROM TITULOS WHERE ID_ATO IN ('+Lista+') ORDER BY PROTOCOLO';
              qryImprime.Open;
              Certidao.Preview;
              if dm.vPergunta then
                Certidao2.Preview ;
              Free;
          end;
      end;
  end
  else
  begin
      qryData.Close;
      qryData.Params[0].AsDate:=edInicio.Date;
      qryData.Params[1].AsDate:=edFim.Date;
      qryData.Open;
      if qryData.IsEmpty then
        GR.Aviso('Protocolo '+IntToStr(I)+' n�o encontrado.')
      else
      begin
          qryData.First;
          while not qryData.Eof do
          begin
              Lista:=Lista+qryDataID_ATO.AsString+',';
              qryData.Next;
          end;
          Lista[Length(Lista)]:=' ';
          Lista:=Trim(Lista);

          with FQuickPagamento1 do
          begin
              qryImprime.Close;
              qryImprime.SQL.Text:='SELECT * FROM TITULOS WHERE ID_ATO IN ('+Lista+') ORDER BY PROTOCOLO';
              qryImprime.Open;
              Certidao.Preview;
              if dm.vPergunta then
                Certidao2.Preview ;
              Free;
          end;
      end;
  end;
  dm.Titulos.Close;
end;

procedure TFRelPagamento.edInicialKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in ['0'..'9',#8]) then key:=#0;
end;

procedure TFRelPagamento.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFRelPagamento.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFRelPagamento.edFinalKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8]) then key:=#0;
end;

procedure TFRelPagamento.RbProtocoloClick(Sender: TObject);
begin
  if RbProtocolo.Checked then
  begin
      edInicial.Visible :=True;
      edFinal.Visible   :=True;
      edInicio.Visible  :=False;
      edFim.Visible     :=False;
      edInicial.SetFocus;
  end
  else
  begin
      edInicial.Visible :=False;
      edFinal.Visible   :=False;
      edInicio.Visible  :=True;
      edFim.Visible     :=True;
      edInicial.SetFocus;
  end;
end;

procedure TFRelPagamento.RbDataClick(Sender: TObject);
begin
  if RbData.Checked then
  begin
      edInicial.Visible :=False;
      edFinal.Visible   :=False;
      edInicio.Visible  :=True;
      edFim.Visible     :=True;
      edInicio.SetFocus;
  end
  else
  begin
      edInicial.Visible :=True;
      edFinal.Visible   :=True;
      edInicio.Visible  :=False;
      edFim.Visible     :=False;
      edInicial.SetFocus;
  end;
end;

procedure TFRelPagamento.edInicialChange(Sender: TObject);
begin
  edFinal.Text:=edInicial.Text;
end;

procedure TFRelPagamento.FormShow(Sender: TObject);
begin
  RbProtocoloClick(Sender);
end;

end.
