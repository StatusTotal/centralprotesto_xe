object FDadosEdital: TFDadosEdital
  Left = 458
  Top = 256
  BorderStyle = bsDialog
  Caption = 'Informa'#231#245'es do Edital'
  ClientHeight = 177
  ClientWidth = 424
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object PDados: TsPanel
    Left = 0
    Top = 0
    Width = 424
    Height = 177
    TabOrder = 0
    SkinData.SkinSection = 'BARTITLE'
    object lbProtocolo: TsLabel
      Left = 307
      Top = 20
      Width = 103
      Height = 34
      Alignment = taCenter
      AutoSize = False
      SkinSection = 'EXTRALINE'
      Caption = 'Protocolo'
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object lkMotivo: TsDBLookupComboBox
      Left = 14
      Top = 88
      Width = 396
      Height = 22
      Hint = 'MOTIVO DA INTIMA'#199#195'O POR EDITAL'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyField = 'DESCRICAO'
      ListField = 'DESCRICAO'
      ListSource = dsMotivos
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      BoundLabel.Active = True
      BoundLabel.Caption = 'Motivo do Edital'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
    end
    object btContinuar: TsBitBtn
      Left = 307
      Top = 128
      Width = 103
      Height = 30
      Cursor = crHandPoint
      Caption = 'Continuar...'
      TabOrder = 1
      OnClick = btContinuarClick
      Layout = blGlyphRight
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 24
      Images = dm.Im24
    end
    object edPublicacao: TsDateEdit
      Left = 14
      Top = 32
      Width = 106
      Height = 22
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.Caption = 'Data da Publica'#231#227'o'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
    end
  end
  object dsMotivos: TDataSource
    DataSet = dm.Motivos
    Left = 40
    Top = 120
  end
  object dsProtocolo: TDataSource
    DataSet = FRelEdital.Protocolo
    Left = 96
    Top = 120
  end
end
