unit UQuickEdital1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, DB, RxMemDS;

type
  TFQuickEdital1 = class(TForm)
    Edital1: TQuickRep;
    Cabecalho: TQRBand;
    QRLabel1: TQRLabel;
    Detalhe: TQRBand;
    qrTitular: TQRLabel;
    RX: TRxMemoryData;
    RXTitulo: TStringField;
    RXApresentante: TStringField;
    RXDevedor: TStringField;
    RXValor: TFloatField;
    RXVencimento: TDateField;
    RXProtocolo: TStringField;
    RXSaldo: TFloatField;
    RXCustas: TFloatField;
    RXEndereco: TStringField;
    RXDt_Titulo: TDateField;
    RXDt_Prazo: TDateField;
    RXDt_Protocolo: TDateField;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRLabel7: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    RXEspecie: TStringField;
    QRLabel8: TQRLabel;
    QRDBText6: TQRDBText;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText7: TQRDBText;
    QRLabel15: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRBand1: TQRBand;
    QRShape4: TQRShape;
    lbCidade: TQRLabel;
    lbFuncionario: TQRLabel;
    lbAviso: TQRLabel;
    lbTabeliao: TQRLabel;
    lbCartorio: TQRLabel;
    QRLabel16: TQRLabel;
    lbEndereco: TQRLabel;
    lbTelefone: TQRLabel;
    lbSite: TQRLabel;
    QRLabel3: TQRLabel;
    QRDBText14: TQRDBText;
    RXMotivo: TStringField;
    lbHorario: TQRLabel;
    ChildBand1: TQRChildBand;
    QRSysData1: TQRSysData;
    procedure CabecalhoBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure Edital1EndPage(Sender: TCustomQuickRep);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickEdital1: TFQuickEdital1;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFQuickEdital1.CabecalhoBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  qrTitular.Caption:=dm.ServentiaTABELIAO.AsString+', '+dm.ValorParametro(7)+' do '+dm.ServentiaDESCRICAO.AsString+' '+
                    'na forma da Lei etc.'+#13+
                    'FAZ SABER a quem interessar possa que foi(ram) apresentado(s) a este '+
                    'Tabelionato de Protesto de T�tulos para ser(em) apontado(s) '+
                    'e posteriormente protestado(s) o(s) seguinte(s) t�tulo(s):';

  lbTabeliao.Caption:=dm.ServentiaTABELIAO.AsString+' - '+dm.ServentiaTITULO.AsString;
  lbEndereco.Caption:=dm.ServentiaENDERECO.AsString+' - '+dm.ServentiaBAIRRO.AsString+' - Cep: '+dm.ServentiaCEP.AsString;
  lbTelefone.Caption:='E-mail: '+dm.ServentiaEMAIL.AsString+' - '+'('+Copy(dm.ServentiaTELEFONE.AsString,1,2)+')'+Copy(dm.ServentiaTELEFONE.AsString,3,4)+'-'+Copy(dm.ServentiaTELEFONE.AsString,7,4);
  lbSite.Caption    :=dm.ServentiaSITE.AsString;
end;

procedure TFQuickEdital1.Edital1EndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickEdital1.FormCreate(Sender: TObject);
begin
  lbCartorio.Caption:=dm.ServentiaDESCRICAO.AsString;
  lbHorario.Caption :='Hor�rio de Funcionamento: '+dm.ValorParametro(25);
  lbAviso.Enabled   :=dm.ServentiaCODIGO.AsInteger<>1336;

  {if dm.ServentiaCODIGO.AsInteger=1430 then
    if GR.Pergunta('Utilizar informa��es do 4� OFicio') then
      lbCartorio.Caption:='VASSOURAS - 4� OF�CIO';}
end;

end.



