object FQuickReciboApontamento: TFQuickReciboApontamento
  Left = 249
  Top = 186
  Caption = 'Recibo de Apontamento'
  ClientHeight = 626
  ClientWidth = 929
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 15
  object Relatorio: TQuickRep
    Left = 80
    Top = 48
    Width = 794
    Height = 1123
    ShowingPreview = False
    DataSet = RX
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnEndPage = RelatorioEndPage
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Continuous = False
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = False
    PrinterSettings.CustomBinCode = 0
    PrinterSettings.ExtendedDuplex = 0
    PrinterSettings.UseCustomPaperCode = False
    PrinterSettings.CustomPaperCode = 0
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.MemoryLimit = 1000000
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 0
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 100
    PrevFormStyle = fsNormal
    PreviewInitialState = wsMaximized
    PreviewWidth = 500
    PreviewHeight = 500
    PrevInitialZoom = qrZoomToWidth
    PreviewDefaultSaveType = stQRP
    PreviewLeft = 0
    PreviewTop = 0
    object Header: TQRBand
      Left = 38
      Top = 38
      Width = 718
      Height = 101
      AlignToBottom = False
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        267.229166666666700000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbPageHeader
      object txNomeServentia: TQRDBText
        Left = 320
        Top = 3
        Width = 78
        Height = 17
        Size.Values = (
          44.979166666666670000
          846.666666666666700000
          7.937500000000000000
          206.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Color = clWhite
        DataSet = dm.Serventia
        DataField = 'DESCRICAO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object txEndereco: TQRDBText
        Left = 331
        Top = 19
        Width = 55
        Height = 17
        Size.Values = (
          44.979166666666670000
          875.770833333333300000
          50.270833333333330000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Color = clWhite
        DataSet = dm.Serventia
        DataField = 'ENDERECO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object txEmail: TQRDBText
        Left = 343
        Top = 50
        Width = 31
        Height = 17
        Size.Values = (
          44.979166666666670000
          907.520833333333300000
          132.291666666666700000
          82.020833333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Color = clWhite
        DataSet = dm.Serventia
        DataField = 'EMAIL'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object txCNPJ: TQRDBText
        Left = 346
        Top = 66
        Width = 26
        Height = 17
        Size.Values = (
          44.979166666666670000
          915.458333333333300000
          174.625000000000000000
          68.791666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Color = clWhite
        DataSet = dm.Serventia
        DataField = 'CNPJ'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object txSite: TQRDBText
        Left = 348
        Top = 82
        Width = 22
        Height = 17
        Size.Values = (
          44.979166666666670000
          920.750000000000000000
          216.958333333333300000
          58.208333333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Color = clWhite
        DataSet = dm.Serventia
        DataField = 'SITE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbTelefone: TQRLabel
        Left = 333
        Top = 34
        Width = 52
        Height = 17
        Size.Values = (
          44.979166666666670000
          881.062500000000000000
          89.958333333333330000
          137.583333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'TELEFONE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    object GrupoHeader: TQRGroup
      Left = 38
      Top = 165
      Width = 718
      Height = 20
      Frame.DrawTop = True
      Frame.DrawBottom = True
      Frame.DrawLeft = True
      Frame.DrawRight = True
      AlignToBottom = False
      BeforePrint = GrupoHeaderBeforePrint
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        52.916666666666670000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      Expression = 'RX.CODIGO_APRESENTANTE'
      FooterBand = GrupoFooter
      Master = Relatorio
      ReprintOnNewPage = False
      object lbPortador: TQRLabel
        Left = 4
        Top = 3
        Width = 42
        Height = 15
        Size.Values = (
          39.687500000000000000
          10.583333333333330000
          7.937500000000000000
          111.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Portador'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    object Detail: TQRBand
      Left = 38
      Top = 185
      Width = 718
      Height = 41
      Frame.DrawLeft = True
      Frame.DrawRight = True
      AlignToBottom = False
      TransparentBand = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ForceNewColumn = False
      ForceNewPage = False
      ParentFont = False
      Size.Values = (
        108.479166666666700000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object QRDBRichText1: TQRDBRichText
        Left = 4
        Top = 2
        Width = 707
        Height = 37
        Size.Values = (
          97.895833333333330000
          10.583333333333330000
          5.291666666666667000
          1870.604166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AutoStretch = False
        Color = clWindow
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        HiresExport = False
        Transparent = False
        YIncrement = 50
        DataField = 'PROTOCOLOS'
        DataSet = RX
      end
    end
    object ChildBand1: TQRChildBand
      Left = 38
      Top = 226
      Width = 718
      Height = 52
      Frame.DrawBottom = True
      Frame.DrawLeft = True
      Frame.DrawRight = True
      AlignToBottom = False
      BeforePrint = ChildBand1BeforePrint
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      LinkBand = GrupoFooter
      Size.Values = (
        137.583333333333300000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      ParentBand = Detail
      PrintOrder = cboAfterParent
      object qrFundos1: TQRLabel
        Left = 4
        Top = 2
        Width = 41
        Height = 15
        Size.Values = (
          39.687500000000000000
          10.583333333333330000
          5.291666666666667000
          108.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Fundos1'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrFundos2: TQRLabel
        Left = 4
        Top = 18
        Width = 41
        Height = 15
        Size.Values = (
          39.687500000000000000
          10.583333333333330000
          47.625000000000000000
          108.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Fundos2'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrQtd: TQRLabel
        Left = 4
        Top = 34
        Width = 48
        Height = 15
        Size.Values = (
          39.687500000000000000
          10.583333333333330000
          89.958333333333330000
          127.000000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Quantidade'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
    object GrupoFooter: TQRBand
      Left = 38
      Top = 278
      Width = 718
      Height = 6
      AlignToBottom = False
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      LinkBand = GrupoHeader
      Size.Values = (
        15.875000000000000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbGroupFooter
    end
    object QRBand1: TQRBand
      Left = 38
      Top = 284
      Width = 718
      Height = 166
      AlignToBottom = False
      BeforePrint = QRBand1BeforePrint
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        439.208333333333300000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbSummary
      object QRExpr1: TQRExpr
        Left = 1
        Top = 31
        Width = 71
        Height = 16
        Size.Values = (
          42.333333333333330000
          2.645833333333333000
          82.020833333333330000
          187.854166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Color = clWhite
        ParentFont = False
        ResetAfterPrint = False
        Transparent = False
        Expression = 'Sum(RX.TOTAL)'
        Mask = 'TOTAL: R$###,##0.00'
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRExpr2: TQRExpr
        Left = 1
        Top = 14
        Width = 62
        Height = 16
        Size.Values = (
          42.333333333333330000
          2.645833333333333000
          37.041666666666670000
          164.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Color = clWhite
        ParentFont = False
        ResetAfterPrint = False
        Transparent = False
        Expression = 'Sum(RX.QTD)'
        Mask = 'QUANTIDADE: #####0'
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRLabel1: TQRLabel
        Left = 1
        Top = 58
        Width = 276
        Height = 15
        Size.Values = (
          39.687500000000000000
          2.645833333333333000
          153.458333333333300000
          730.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Data de entrega de acordo com o art. 20 da Lei 94 92/97.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel2: TQRLabel
        Left = 1
        Top = 74
        Width = 371
        Height = 15
        Size.Values = (
          39.687500000000000000
          2.645833333333333000
          195.791666666666700000
          981.604166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 
          'Realizado na forma do art. 6'#186', al'#237'nea '#39'D'#39', do Ato Normativo Conj' +
          'unto n'#186' 27/99.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape1: TQRShape
        Left = 218
        Top = 144
        Width = 281
        Height = 1
        Size.Values = (
          2.645833333333333000
          576.791666666666700000
          381.000000000000000000
          743.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrCidade: TQRLabel
        Left = 342
        Top = 106
        Width = 34
        Height = 15
        Size.Values = (
          39.687500000000000000
          904.875000000000000000
          280.458333333333300000
          89.958333333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'Cidade'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrAssinatura: TQRLabel
        Left = 332
        Top = 146
        Width = 54
        Height = 15
        Size.Values = (
          39.687500000000000000
          878.416666666666700000
          386.291666666666700000
          142.875000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'Assinatura'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    object ChildBand2: TQRChildBand
      Left = 38
      Top = 139
      Width = 718
      Height = 26
      AlignToBottom = False
      BeforePrint = ChildBand2BeforePrint
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        68.791666666666670000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      ParentBand = Header
      PrintOrder = cboAfterParent
      object lbRecibo: TQRLabel
        Left = 4
        Top = 5
        Width = 131
        Height = 15
        Size.Values = (
          39.687500000000000000
          10.583333333333330000
          13.229166666666670000
          346.604166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Recibo(s) N'#186'(s): 000 a 000'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
  end
  object RX: TRxMemoryData
    FieldDefs = <>
    Left = 32
    Top = 136
    object RXDT_PROTOCOLO: TDateField
      FieldName = 'DT_PROTOCOLO'
    end
    object RXAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Size = 100
    end
    object RXCODIGO_APRESENTANTE: TStringField
      FieldName = 'CODIGO_APRESENTANTE'
      Size = 10
    end
    object RXFAIXA: TStringField
      FieldName = 'FAIXA'
      Size = 1
    end
    object RXPROTOCOLOS: TMemoField
      FieldName = 'PROTOCOLOS'
      BlobType = ftMemo
    end
    object RXEMOLUMENTOS: TFloatField
      FieldName = 'EMOLUMENTOS'
    end
    object RXFETJ: TFloatField
      FieldName = 'FETJ'
    end
    object RXFUNDPERJ: TFloatField
      FieldName = 'FUNDPERJ'
    end
    object RXFUNPERJ: TFloatField
      FieldName = 'FUNPERJ'
    end
    object RXFUNARPEN: TFloatField
      FieldName = 'FUNARPEN'
    end
    object RXPMCMV: TFloatField
      FieldName = 'PMCMV'
    end
    object RXISS: TFloatField
      FieldName = 'ISS'
    end
    object RXMUTUA: TFloatField
      FieldName = 'MUTUA'
    end
    object RXACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
    end
    object RXDISTRIBUICAO: TFloatField
      FieldName = 'DISTRIBUICAO'
    end
    object RXTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
    object RXQTD: TIntegerField
      FieldName = 'QTD'
    end
  end
  object qrySoma: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      ''
      'SUM(EMOLUMENTOS+FETJ+FUNDPERJ+FUNPERJ+FUNARPEN+PMCMV+'
      'MUTUA+ACOTERJ+DISTRIBUICAO) as TOTAL '
      ''
      'from TITULOS '
      ''
      'where DT_PROTOCOLO=:DATA and CODIGO_APRESENTANTE=:CODIGO and'
      'CONVENIO='#39'N'#39)
    Left = 32
    Top = 224
    ParamData = <
      item
        Name = 'DATA'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'CODIGO'
        DataType = ftString
        ParamType = ptInput
      end>
    object qrySomaTOTAL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object qryProtocolos: TFDQuery
    OnCalcFields = qryProtocolosCalcFields
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      ''
      'ID_ATO,'
      'CODIGO,'
      'RECIBO,'
      'DT_PROTOCOLO,'
      'PROTOCOLO,'
      'COBRANCA,'
      'EMOLUMENTOS,'
      'FETJ,'
      'FUNDPERJ, FUNPERJ,'
      'FUNARPEN,'
      'PMCMV,'
      'ISS,'
      'MUTUA,'
      'ACOTERJ,'
      'DISTRIBUICAO,'
      'TOTAL, '
      'CONVENIO,'
      'CODIGO_APRESENTANTE,'
      'APRESENTANTE '
      ''
      'from '
      ''
      'TITULOS'
      ''
      'where'
      ''
      'DT_PROTOCOLO between :D1 and :D2'
      ''
      'order by CODIGO_APRESENTANTE,CODIGO,PROTOCOLO')
    Left = 32
    Top = 296
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end>
    object qryProtocolosID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryProtocolosCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
    end
    object qryProtocolosRECIBO: TIntegerField
      FieldName = 'RECIBO'
      Origin = 'RECIBO'
    end
    object qryProtocolosDT_PROTOCOLO: TDateField
      FieldName = 'DT_PROTOCOLO'
      Origin = 'DT_PROTOCOLO'
    end
    object qryProtocolosPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object qryProtocolosCOBRANCA: TStringField
      FieldName = 'COBRANCA'
      Origin = 'COBRANCA'
      FixedChar = True
      Size = 2
    end
    object qryProtocolosEMOLUMENTOS: TFloatField
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
    end
    object qryProtocolosFETJ: TFloatField
      FieldName = 'FETJ'
      Origin = 'FETJ'
    end
    object qryProtocolosFUNDPERJ: TFloatField
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
    end
    object qryProtocolosFUNPERJ: TFloatField
      FieldName = 'FUNPERJ'
      Origin = 'FUNPERJ'
    end
    object qryProtocolosFUNARPEN: TFloatField
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
    end
    object qryProtocolosPMCMV: TFloatField
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
    end
    object qryProtocolosISS: TFloatField
      FieldName = 'ISS'
      Origin = 'ISS'
    end
    object qryProtocolosMUTUA: TFloatField
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
    end
    object qryProtocolosACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
    end
    object qryProtocolosDISTRIBUICAO: TFloatField
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
    end
    object qryProtocolosTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
    end
    object qryProtocolosCONVENIO: TStringField
      FieldName = 'CONVENIO'
      Origin = 'CONVENIO'
      FixedChar = True
      Size = 1
    end
    object qryProtocolosCODIGO_APRESENTANTE: TStringField
      FieldName = 'CODIGO_APRESENTANTE'
      Origin = 'CODIGO_APRESENTANTE'
      Size = 10
    end
    object qryProtocolosAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Origin = 'APRESENTANTE'
      Size = 100
    end
    object qryProtocolosFaixa: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'Faixa'
      Size = 1
    end
  end
end
