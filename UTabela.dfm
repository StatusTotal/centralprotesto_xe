object FTabela: TFTabela
  Left = 288
  Top = 204
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Tabela de Atos - Livro Adicional Eletr'#244'nico'
  ClientHeight = 542
  ClientWidth = 884
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TwwDBGrid
    Left = 0
    Top = 46
    Width = 884
    Height = 470
    Selected.Strings = (
      'COD'#9'7'#9'C'#243'digo'#9'F'
      'TITULO'#9'31'#9'Descri'#231#227'o'#9'F'
      'EMOL'#9'8'#9'Emol.'#9'F'
      'FETJ'#9'8'#9'20%'#9'F'
      'FUND'#9'8'#9'5%'#9'F'
      'FUNP'#9'7'#9'5%'#9'F'
      'FUNA'#9'5'#9'4%'#9'F'
      'PMCMV'#9'6'#9'2%'#9'F'
      'ISS'#9'5'#9'Iss'#9'F'
      'MUTUA'#9'6'#9'M'#250'tua'#9'F'
      'ACOTERJ'#9'6'#9'Acoterj'#9'F'
      'DISTRIB'#9'7'#9'Dist.'#9'F'
      'TOT'#9'8'#9'Total'#9'F')
    IniAttributes.Delimiter = ';;'
    IniAttributes.UnicodeIniFile = False
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    BorderStyle = bsNone
    DataSource = dsCodigos
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
    ParentFont = False
    PopupMenu = PM
    TabOrder = 0
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = False
    UseTFields = False
    OnCalcCellColors = GridCalcCellColors
    OnDblClick = GridDblClick
  end
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 884
    Height = 46
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'PANEL'
    object btItens: TsBitBtn
      Left = 11
      Top = 9
      Width = 118
      Height = 28
      Cursor = crHandPoint
      Caption = 'Tabela de Itens'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = btItensClick
      ImageIndex = 38
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object btDetalhes: TsBitBtn
      Left = 136
      Top = 9
      Width = 79
      Height = 28
      Cursor = crHandPoint
      Caption = 'Detalhes'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btDetalhesClick
      ImageIndex = 4
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object P2: TsPanel
    Left = 0
    Top = 516
    Width = 884
    Height = 26
    Align = alBottom
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object Shape2: TShape
      Left = 7
      Top = 6
      Width = 14
      Height = 14
      Brush.Color = clRed
    end
    object Label1: TLabel
      Left = 25
      Top = 6
      Width = 50
      Height = 13
      Caption = 'Conv'#234'nios'
      Transparent = True
    end
  end
  object dsCodigos: TDataSource
    DataSet = dm.Codigos
    Left = 88
    Top = 136
  end
  object PM: TPopupMenu
    Left = 40
    Top = 136
    object Ajustar1: TMenuItem
      Caption = 'Ajustar'
      OnClick = Ajustar1Click
    end
  end
  object Opd: TOpenDialog
    Left = 144
    Top = 136
  end
end
