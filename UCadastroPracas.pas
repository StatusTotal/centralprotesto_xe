unit UCadastroPracas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, sPanel, Grids, Wwdbigrd, Wwdbgrid, StdCtrls,
  Buttons, sBitBtn;

type
  TFCadastroPracas = class(TForm)
    Grid: TwwDBGrid;
    dsPracas: TDataSource;
    sPanel1: TsPanel;
    btIncluir: TsBitBtn;
    btAlterar: TsBitBtn;
    btExcluir: TsBitBtn;
    sBitBtn1: TsBitBtn;
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroPracas: TFCadastroPracas;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFCadastroPracas.sBitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TFCadastroPracas.FormCreate(Sender: TObject);
begin
  dm.Pracas.Close;
  dm.Pracas.Open;
end;

procedure TFCadastroPracas.btExcluirClick(Sender: TObject);
begin
  if dm.Pracas.IsEmpty then Exit;

  dm.Pracas.Delete;
  dm.Pracas.ApplyUpdates(0);
end;

procedure TFCadastroPracas.btIncluirClick(Sender: TObject);
var
  Descricao: String;
begin
  dm.Pracas.Append;

  if InputQuery('Inclus�o','Descri��o',Descricao) then
  begin
      dm.PracasID_PRACA.AsInteger :=dm.IdAtual('ID_PRACA','S');
      dm.PracasNOME.AsString      :=PF.TextoMaiusculo(Descricao);
      dm.Pracas.Post;
      dm.Pracas.ApplyUpdates(0);
  end
  else
    dm.Pracas.Cancel;
end;

procedure TFCadastroPracas.btAlterarClick(Sender: TObject);
var
  Descricao: String;
begin
  if dm.Pracas.IsEmpty then Exit;
  
  dm.Pracas.Edit;

  Descricao:=dm.PracasNOME.AsString;
  if InputQuery('Altera��o','Descri��o',Descricao) then
  begin
    dm.PracasNOME.AsString:=PF.TextoMaiusculo(Descricao);
    dm.Pracas.Post;
    dm.Pracas.ApplyUpdates(0);
  end
  else
    dm.Pracas.Cancel;
end;

procedure TFCadastroPracas.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFCadastroPracas.GridDblClick(Sender: TObject);
begin
  btAlterarClick(Sender);
end;

end.
