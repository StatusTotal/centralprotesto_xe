unit UQuickCertidao1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, DB, RxMemDS, StdCtrls, ComCtrls;

type
  TFQuickCertidao1 = class(TForm)
    RX: TRxMemoryData;
    RXTitulo: TStringField;
    RXApresentante: TStringField;
    RXDevedor: TStringField;
    RXValor: TFloatField;
    RXVencimento: TDateField;
    RXProtocolo: TStringField;
    RXDt_Titulo: TDateField;
    RXEspecie: TStringField;
    qkCertidao: TQuickRep;
    Cabecalho: TQRBand;
    txNomeServentia: TQRDBText;
    txEndereco: TQRDBText;
    lbCertidao: TQRLabel;
    lbTitular: TQRLabel;
    Detalhe: TQRBand;
    QRShape1: TQRShape;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRLabel7: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText6: TQRDBText;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRLabel2: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel3: TQRLabel;
    QRDBText2: TQRDBText;
    RXSacador: TStringField;
    RXCedente: TStringField;
    RXDt_Protesto: TDateField;
    QRLabel4: TQRLabel;
    QRDBText12: TQRDBText;
    RXEndosso: TStringField;
    RXLivroFolha: TStringField;
    Summary: TQRBand;
    lbEscrevente: TQRLabel;
    QRLabel17: TQRLabel;
    lbCidade: TQRLabel;
    lbAssinatura: TQRLabel;
    QRShape5: TQRShape;
    txEmail: TQRDBText;
    txCNPJ: TQRDBText;
    RE2: TRichEdit;
    RT2: TQRRichText;
    txSite: TQRDBText;
    lbTelefone: TQRLabel;
    lbMatricula: TQRLabel;
    lbPedido: TQRLabel;
    lbOrdem: TQRLabel;
    lbTexto4: TQRLabel;
    CBRodape: TQRBand;
    QRLabel18: TQRLabel;
    qrAviso: TQRLabel;
    sysPagina: TQRSysData;
    M: TQRMemo;
    RE1: TRichEdit;
    RT1: TQRRichText;
    lbCGJ1: TQRLabel;
    lbCGJ2: TQRLabel;
    lbCGJ3: TQRLabel;
    lbSeloEletronico: TQRLabel;
    lbCGJ4: TQRLabel;
    lbCGJ5: TQRLabel;
    QRLabel1: TQRLabel;
    QRDBText13: TQRDBText;
    RXTotal: TFloatField;
    lbOutrossim: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure DetalheAfterPrint(Sender: TQRCustomBand; BandPrinted: Boolean);
    procedure CabecalhoBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qkCertidaoEndPage(Sender: TCustomQuickRep);
    procedure SummaryBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure DetalheBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure JustifyText;
  private
    { Private declarations }
    vSoma: Double;
  public
    { Public declarations }
    xRegistro: Integer;
  end;

var
  FQuickCertidao1: TFQuickCertidao1;

implementation

uses UDM, UPF, UAssinatura, UGeral, RichEdit, DateUtils, UGDM;

{$R *.dfm}

procedure TFQuickCertidao1.JustifyText;
var
  Format: TParaFormat2; // declared in RichEdit.pas
begin
  FillChar(Format,SizeOf(TParaFormat2),0);
  Format.cbSize     :=SizeOf(TParaFormat2);
  Format.dwMask     :=PFM_ALIGNMENT; {The wAlignment member is valid}
  Format.wAlignment :=PFA_JUSTIFY;
  {Some words about PFA_JUSTIFY from MSDN help:
  Rich Edit 2.0: Paragraphs are justified. This value is included for compatibility with TOM interfaces;rich edit controls earlier than version 3.0 display the text aligned with the left margin.}
  SendMessage(RE2.Handle,EM_SETPARAFORMAT,0,LPARAM(@Format));
end;

procedure TFQuickCertidao1.FormCreate(Sender: TObject);
var
  vData1,vData2,vDocumento,vMatricula,vTipo: String;
begin
  {PEDRO - 29/03/2016}
  if GR.Pergunta('DESEJA IMPRIMIR NA FOLHA DE SEGURAN�A') then
  begin
      if dm.vFlTiracabecalho = 'S' then
      begin
        txNomeServentia.Enabled := False;
        txEndereco.Enabled      := False;
        lbTelefone.Enabled      := False;
        txEmail.Enabled         := False;
        txCNPJ.Enabled          := False;
        txSite.Enabled          := False;
      end
      else
      begin
        txNomeServentia.Enabled := True;
        txEndereco.Enabled      := True;
        lbTelefone.Enabled      := True;
        txEmail.Enabled         := True;
        txCNPJ.Enabled          := True;
        txSite.Enabled          := True;
      end;

      qkCertidao.Frame.DrawTop    :=False;
      qkCertidao.Frame.DrawBottom :=False;
      qkCertidao.Frame.DrawLeft   :=False;
      qkCertidao.Frame.DrawRight  :=False;

      qkCertidao.Page.BottomMargin := StrToFloat(dm.vFlBottom);
      qkCertidao.Page.TopMargin    := StrToFloat(dm.vFlTop);
      qkCertidao.Page.LeftMargin   := StrToFloat(dm.vFlLeft);
      qkCertidao.Page.RightMargin  := StrToFloat(dm.vFlRight);

      if dm.vFlFolha ='A4' then
      begin
        qkCertidao.Page.Width := 210.0;
        qkCertidao.Page.Length:= 297.0;
      end;

      if dm.vFlFolha ='Legal' then
      begin
        qkCertidao.Page.Width := 215.9;
        qkCertidao.Page.Length:= 355.6;
      end;
      {FIM}
  end;

  if (dm.ServentiaCODIGO.AsInteger=1726) or (dm.ServentiaCODIGO.AsInteger=1622) then
  begin
      qkCertidao.Frame.DrawTop    :=False;
      qkCertidao.Frame.DrawBottom :=False;
      qkCertidao.Frame.DrawLeft   :=False;
      qkCertidao.Frame.DrawRight  :=False;
      txNomeServentia.Enabled     :=False;
      txEndereco.Enabled          :=False;
      lbTelefone.Enabled          :=False;
      txEmail.Enabled             :=False;
      txCNPJ.Enabled              :=False;
      txSite.Enabled              :=False;
  end;

     {1� OF�CIO DE VASSOURAS}
  if (dm.ServentiaCODIGO.AsInteger=1430) then
  begin
      txNomeServentia.Enabled:=False;
      txEndereco.Enabled:=False;
      lbTelefone.Enabled:=False;
      txEmail.Enabled:=False;
  end;

  vSoma:=0;
  lbTitular.Caption   :=dm.ServentiaTABELIAO.AsString+',';
  lbTelefone.Caption  :=PF.FormatarTelefone(dm.ServentiaTELEFONE.AsString);
  if dm.CertidoesTIPO_REQUERIDO.AsString='F' then
    vDocumento:=', CPF: '+PF.FormatarCPF(dm.CertidoesCPF_CNPJ_REQUERIDO.AsString)
      else vDocumento:=', CNPJ: '+PF.FormatarCNPJ(dm.CertidoesCPF_CNPJ_REQUERIDO.AsString);

  RE1.Lines.Add(dm.ValorParametro(7)+' do '+dm.ServentiaDESCRICAO.AsString+', Estado do Rio de Janeiro, por nomea��o na forma da Lei.');

  if dm.ServentiaCODIGO.AsInteger=1554 then
  RE1.Text:='Titular do Tabelionato do 1� Of�cio de Itabora�/RJ, no exerc�cio de suas atribui��es, em conformidade com o '+
            'disposto no Art. 6�, item II, da Lei 8.935/94.';

  if dm.ServentiaCODIGO.AsInteger<>1415 then 
  GR.JustifyRichEdit(RE1,True);

  vTipo:=GR.iif(dm.CertidoesNUMERO_TITULO.AsString='',' em nome de: '+dm.CertidoesREQUERIDO.AsString+vDocumento+'.',
                                                      ' relacionado ao N� de T�tulo: '+dm.CertidoesNUMERO_TITULO.AsString+'.');

  {ARRAIAL DO CABO}
  if dm.ServentiaCODIGO.AsInteger=2353 then
    RE2.Lines.Add('CERTIFICA, que em virtude de pedido por escrito, feito pela parte interessada '+
                  'que, revendo em Cart�rio os livros de  registros  de  protestos  no  per�odo  de 27 de Mar�o de 2000'+
                  ' at� a presente data, neles verifiquei que,'+vTipo)
    else
      if dm.ServentiaCODIGO.AsInteger=4448 then
        RE2.Lines.Add('CERTIFICA, que em virtude de pedido por escrito, feito pela parte interessada '+
                      'que, revendo em Cart�rio os livros de  registros  de  protestos  � partir de 15 de abril de 2009'+
                      ' at� a presente data, neles verifiquei que,'+vTipo)
          else
            if (dm.ServentiaCODIGO.AsInteger=1336) or (dm.ServentiaCODIGO.AsInteger=1622) then
            begin
                vData1:=GR.DataExtenso(IncYear(IncDay(dm.CertidoesDT_CERTIDAO.AsDateTime,-1),{-5}dm.CertidoesANOS.AsInteger*-1),2);
                vData2:=GR.DataExtenso(IncDay(dm.CertidoesDT_CERTIDAO.AsDateTime,-1),2);
                RE2.Lines.Add('CERTIFICA, em virtude de pedido por escrito, feito pela parte interessada '+
                              ', revendo em Cart�rio os livros de registros de protestos no per�odo de '+vData1+
                              ' ('+FormatDateTime('dd/mm/yyyy',IncYear(IncDay(dm.CertidoesDT_CERTIDAO.AsDateTime,-1),{-5}dm.CertidoesANOS.AsInteger*-1))+
                              ') a '+vData2+' ('+FormatDateTime('dd/mm/yyyy',IncDay(dm.CertidoesDT_CERTIDAO.AsDateTime,-1))+'), '+
                              ' neles verifiquei que,'+vTipo);
            end
            else {JAPERI}
              if (dm.ServentiaCODIGO.AsInteger=6926) then
              begin
                  RE2.Lines.Add('CERTIFICA, que em virtude de pedido por escrito, feito pela parte interessada '+
                                'que, revendo em Cart�rio os livros de registros de protestos no per�odo compreendido '+
                                'entre primeiro (01) de julho (07) de dois mil e onze (2011) at� a presente data, '+
                                'neles verifiquei que,'+vTipo);
                  Cabecalho.Height:=370;
                  lbOutrossim.Enabled:=True;
              end
              else
                RE2.Lines.Add('CERTIFICA, que em virtude de pedido por escrito, feito pela parte interessada '+
                              'que, revendo em Cart�rio os livros de registros de protestos no per�odo de '+dm.CertidoesANOS.AsString+
                              ' ANOS anteriores � presente data, neles verifiquei que,'+vTipo);

  {if dm.ServentiaCODIGO.AsInteger=2357 then
    RE2.Text:=PF.JstParagrafo(RE2.Text,100)
      else GR.JustifyRichEdit(RE2,True);}

  //GR.JustifyRichEdit(RE2,True);
  //JustifyText;

  if dm.CertidoesTIPO_CERTIDAO.AsString='I' then
    lbTexto4.Caption:='CONSTA O T�TULO PROTESTADO:'
  else
  if dm.CertidoesTIPO_CERTIDAO.AsString='N' then
    if dm.CertidoesRESULTADO.AsString='P' then
    begin
        if dm.CertidoesREGISTROS.AsInteger>1 then
          lbTexto4.Caption:='CONSTAM: '+dm.CertidoesREGISTROS.AsString+' T�TULOS'
            else lbTexto4.Caption:='CONSTA: '+dm.CertidoesREGISTROS.AsString+' T�TULO';
    end
    else
    begin
        lbCertidao.Caption:='CERTID�O NEGATIVA DE PROTESTO DE T�TULOS E OUTROS DOCUMENTOS DE D�VIDA';
        lbTexto4.Caption  :='N�O CONSTAM PROTESTOS';
    end;

  vMatricula:=dm.MatriculaEscrevente(dm.EscreventesLOGIN.AsString);
  if vMatricula<>'' then
    vMatricula:=', Mat.: '+vMatricula
      else vMatricula:='';

  lbEscrevente.Caption  :='Eu,__________________, '+dm.EscreventesNOME.AsString+
                          vMatricula+
                          ', '+dm.EscreventesQUALIFICACAO.AsString+', efetuei a busca.';

  lbCidade.Caption      :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',dm.CertidoesDT_CERTIDAO.AsDateTime);
  lbAssinatura.Caption  :=dm.vAssinatura;
  lbMatricula.Caption   :=dm.vMatricula;

  if dm.ServentiaCODIGO.AsInteger=1554 then
  begin
      lbAssinatura.Caption:='Tabeli�o de Protesto';
      lbMatricula.Caption:='';
  end;

  {ARRAIAL DO CABO}
  if dm.ServentiaCODIGO.AsInteger=2353 then
  begin
      qkCertidao.Frame.DrawTop     :=False;
      qkCertidao.Frame.DrawBottom  :=False;
      qkCertidao.Frame.DrawLeft    :=False;
      qkCertidao.Frame.DrawRight   :=False;
      qkCertidao.Page.LeftMargin   :=10;
      qkCertidao.Page.RightMargin  :=5;
      qkCertidao.Page.TopMargin    :=10;
      //M.Font.Size                  :=9;
      lbTelefone.Caption           :=lbTelefone.Caption+' / (22)2622-4168';
  end;

  {PINHEIRAL}
  if dm.ServentiaCODIGO.AsInteger=2369 then
  begin
      qrAviso.Caption             :='"V�LIDO SOMENTE COM O SELO DE FISCALIZA��O"';
      qkCertidao.Frame.DrawTop    :=False;
      qkCertidao.Frame.DrawBottom :=False;
      qkCertidao.Frame.DrawLeft   :=False;
      qkCertidao.Frame.DrawRight  :=False;
  end
  else
    qrAviso.Caption             :='';
end;

procedure TFQuickCertidao1.DetalheAfterPrint(Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  if dm.ValorParametro(22)='N' then Exit;

  if Detalhe.Enabled then
  begin
      Inc(xRegistro);

      if xRegistro=StrToInt(dm.ValorParametro(18)) then
      begin
          qkCertidao.NewPage;
          xRegistro:=0;
      end;
  end;
end;

procedure TFQuickCertidao1.CabecalhoBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  if dm.ServentiaCODIGO.AsInteger=1823 then
    lbPedido.Caption:='Data do Pedido: '+FormatDateTime('dd/mm/yyyy',dm.CertidoesDT_PEDIDO.AsDateTime)+' / N�: '+dm.CertidoesRECIBO.AsString
      else lbPedido.Caption:='';
  xRegistro:=0;
end;

procedure TFQuickCertidao1.qkCertidaoEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickCertidao1.SummaryBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
var
  vQtd1,vQtd2,vCobranca: String;
  vComuns,vEmolumentos: Double;
begin
  if dm.CertidoesORDEM.AsInteger<>0 then
    lbOrdem.Caption:='N� de Ordem: '+dm.CertidoesORDEM.AsString
      else lbOrdem.Caption:='';

  lbSeloEletronico.Caption:=GR.PegarLetra(dm.CertidoesSELO.AsString)+' '+
                            GR.Zeros(GR.PegarNumeroTexto(dm.CertidoesSELO.AsString),'D',5)+' '+
                            dm.CertidoesALEATORIO.AsString;

  vComuns:=0;
  vEmolumentos:=0;
  PF.CarregarCustas(dm.CertidoesID_CERTIDAO.AsInteger);
  dm.RxCustas.First;
  while not dm.RxCustas.Eof do
  begin
      if dm.RxCustasTABELA.AsString+dm.RxCustasITEM.AsString+dm.RxCustasSUBITEM.AsString='162*' then
      begin
          vQtd1:='1';
          vQtd2:=dm.RxCustasQTD.AsString;
      end
      else
      begin
          vQtd1:=dm.RxCustasQTD.AsString;
          vQtd2:=dm.RxCustasQTD.AsString;
      end;

      if dm.RxCustasTABELA.AsString='16' then
      vComuns:=vComuns+(dm.RxCustasVALOR.AsFloat*StrToInt(vQtd1));

      vEmolumentos:=vEmolumentos+(dm.RxCustasVALOR.AsFloat*StrToInt(vQtd1));
      M.Lines.Add(dm.RxCustasTABELA.AsString+'.'+dm.RxCustasITEM.AsString+'.'+dm.RxCustasSUBITEM.AsString+'   '+dm.RxCustasDESCRICAO.AsString+' x '+vQtd2+Format('%-1s %7.2f',[' =',dm.RxCustasVALOR.AsFloat*StrToInt(vQtd2)]));
      dm.RxCustas.Next;
  end;

  {RECALCULO PARA FOR�AR A SAIR SEMPRE UMA EXTRA��O DE CERTID�O E N�O APARECER CASO TENHA FOLHAS EXCEDENTES}
  //M.Lines.Clear;
  M.Lines.Add('Emol.: R$'+FloatToStrF(vEmolumentos+dm.CertidoesEXEMOLUMENTOS.AsFloat,ffNumber,7,2)+'|'+
              'Fetj: R$'+FloatToStrF(GR.FETJ(vEmolumentos)+dm.CertidoesEXFETJ.AsFloat,ffNumber,7,2)+'|'+
              'Fundperj: R$'+FloatToStrF(GR.FUNDPERJ(vEmolumentos)+dm.CertidoesEXFUNDPERJ.AsFloat,ffNumber,7,2)+'|'+
              'Funperj: R$'+FloatToStrF(GR.FUNPERJ(vEmolumentos)+dm.CertidoesEXFUNPERJ.AsFloat,ffNumber,7,2)+'|'+
              'Funarpen: R$'+FloatToStrF(GR.FUNARPEN(vEmolumentos)+dm.CertidoesEXFUNARPEN.AsFloat,ffNumber,7,2)+'|'+
              'Pmcmv: R$'+FloatToStrF(GR.PMCMV(vEmolumentos-vComuns),ffNumber,7,2)+'|'+
              'Iss: R$'+FloatToStrF(GR.ISS(vEmolumentos,Gdm.vAliquotaISS)+dm.CertidoesISS.AsFloat,ffNumber,7,2)+'|'+
              'Total: R$'+FloatToStrF(vEmolumentos+dm.CertidoesEXEMOLUMENTOS.AsFloat+
                                      GR.FETJ(vEmolumentos)+dm.CertidoesEXFETJ.AsFloat+
                                      GR.FUNDPERJ(vEmolumentos)+dm.CertidoesEXFUNDPERJ.AsFloat+
                                      GR.FUNPERJ(vEmolumentos)+dm.CertidoesEXFUNPERJ.AsFloat+
                                      GR.FUNARPEN(vEmolumentos)+dm.CertidoesEXFUNARPEN.AsFloat+
                                      GR.PMCMV(vEmolumentos-vComuns)+
                                      GR.ISS(vEmolumentos,Gdm.vAliquotaISS)+dm.CertidoesEXISS.AsFloat,
                                      ffNumber,7,2));

  if dm.CertidoesCOBRANCA.AsString='CC' then vCobranca:='';
  if dm.CertidoesCOBRANCA.AsString='SC' then vCobranca:='SEM COBRAN�A';
  if dm.CertidoesCOBRANCA.AsString='JG' then vCobranca:='JUSTI�A GRATUITA';
  if dm.CertidoesCOBRANCA.AsString='NH' then vCobranca:='NIHIL';

  M.Lines.Add('');
  M.Lines.Add(vCobranca);

  if dm.ValorParametro(85)='S' then
  begin
      M.Lines.Add('Total de custas atualizado de acordo com o ano atual ('+dm.ValorParametro(77)+
                  ') dos t�tulos discriminados acima: R$ '+FloatToStrF(vSoma,ffNumber,7,2));
      vSoma:=0;
  end;
end;

procedure TFQuickCertidao1.DetalheBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  vSoma:=vSoma+RXTotal.AsFloat;
end;

end.
