unit UEtiquetaRetificacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sMemo, sSkinProvider, Buttons, sBitBtn, ExtCtrls,
  sPanel, Menus, IniFiles;

type
  TFEtiquetaRetificacao = class(TForm)
    P1: TsPanel;
    btImprimir: TsBitBtn;
    sSkinProvider: TsSkinProvider;
    MRetificacao: TsMemo;
    MEtiqueta: TsMemo;
    PM: TPopupMenu;
    D1: TMenuItem;
    N1: TMenuItem;
    D2: TMenuItem;
    N2: TMenuItem;
    T1: TMenuItem;
    M1: TMenuItem;
    C1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btImprimirClick(Sender: TObject);
    procedure MRetificacaoChange(Sender: TObject);
    procedure D1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure D2Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure T1Click(Sender: TObject);
    procedure M1Click(Sender: TObject);
    procedure C1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEtiquetaRetificacao: TFEtiquetaRetificacao;

implementation

uses UGeral, UDM;

{$R *.dfm}

procedure TFEtiquetaRetificacao.FormCreate(Sender: TObject);
begin
  MRetificacao.Text:=GR.LerArquivo(GR.DirExe+'Retificacao.txt');
end;

procedure TFEtiquetaRetificacao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  GR.EscreverNoArquivo(GR.DirExe+'Retificacao.txt',MRetificacao.Text);
end;

procedure TFEtiquetaRetificacao.btImprimirClick(Sender: TObject);
var
  I: TIniFile;
  F: TextFile;
  X: Integer;
  P: String;
begin
  try
    I:=TIniFile.Create(ExtractFilePath(Application.ExeName)+'Par.ini');
    P:=I.ReadString('IMPRESSORAS','ETIQUETA','');
    AssignFile(F,P);
    I.Free;
    Rewrite(F);
    for X := 0 to MEtiqueta.Lines.Count-1 do
    Writeln(F,MEtiqueta.Lines[X]);
    CloseFile(F);
    GR.Aviso('IMPRESS�O EFETUADA COM SUCESSO!');
  except
    on E: Exception do
    GR.Aviso('ERRO: '+E.Message);
  end;
end;

procedure TFEtiquetaRetificacao.MRetificacaoChange(Sender: TObject);
begin
  MEtiqueta.Text:=MRetificacao.Text;
  MEtiqueta.Text:=StringReplace(MEtiqueta.Text,'[DATAPROTOCOLO]',dm.vDataProtocolo,[rfReplaceAll, rfIgnoreCase]);
  MEtiqueta.Text:=StringReplace(MEtiqueta.Text,'[PROTOCOLO]',dm.vProtocolo,[rfReplaceAll, rfIgnoreCase]);
  MEtiqueta.Text:=StringReplace(MEtiqueta.Text,'[CIDADE]',GR.PrimeiraLetraMaiscula(GR.RemoverAcento(dm.ServentiaCIDADE.AsString)),[rfReplaceAll, rfIgnoreCase]);
  MEtiqueta.Text:=StringReplace(MEtiqueta.Text,'[DATAATUAL]',FormatDateTime('dd "de" mmmm "de" yyyy',Now),[rfReplaceAll, rfIgnoreCase]);
  MEtiqueta.Text:=StringReplace(MEtiqueta.Text,'[NOMETABELIAO]',GR.PrimeiraLetraMaiscula(GR.RemoverAcento(dm.ServentiaTABELIAO.AsString)),[rfReplaceAll, rfIgnoreCase]);
  MEtiqueta.Text:=StringReplace(MEtiqueta.Text,'[TITULOTABELIAO]',GR.PrimeiraLetraMaiscula(dm.ServentiaTITULO.AsString),[rfReplaceAll, rfIgnoreCase]);
  MEtiqueta.Text:=StringReplace(MEtiqueta.Text,'[MATRICULATABELIAO]',dm.ServentiaMATRICULA.AsString,[rfReplaceAll, rfIgnoreCase]);
end;

procedure TFEtiquetaRetificacao.D1Click(Sender: TObject);
begin
  MRetificacao.SelText:='[DATAPROTOCOLO]';
end;

procedure TFEtiquetaRetificacao.N1Click(Sender: TObject);
begin
  MRetificacao.SelText:='[PROTOCOLO]';
end;

procedure TFEtiquetaRetificacao.D2Click(Sender: TObject);
begin
  MRetificacao.SelText:='[DATAATUAL]';
end;

procedure TFEtiquetaRetificacao.N2Click(Sender: TObject);
begin
  MRetificacao.SelText:='[NOMETABELIAO]';
end;

procedure TFEtiquetaRetificacao.T1Click(Sender: TObject);
begin
  MRetificacao.SelText:='[TITULOTABELIAO]';
end;

procedure TFEtiquetaRetificacao.M1Click(Sender: TObject);
begin
  MRetificacao.SelText:='[MATRICULATABELIAO]';
end;

procedure TFEtiquetaRetificacao.C1Click(Sender: TObject);
begin
  MRetificacao.SelText:='[CIDADE]';
end;

end.
