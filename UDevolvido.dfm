�
 TFDEVOLVIDO 0�  TPF0TFDevolvido
FDevolvidoLeft[Top� BorderIconsbiSystemMenu BorderStylebsDialogCaption	DevolvidoClientHeight�ClientWidthLColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
KeyPreview	OldCreateOrderPositionpoMainFormCenterOnClose	FormCloseOnCreate
FormCreateOnMouseMoveFormMouseMovePixelsPerInch`
TextHeight TsPanelP1Left Top WidthLHeightAAlignalTopTabOrder OnMouseMoveP1MouseMoveSkinData.SkinSectionPANEL TsBevelsBevel1Left� TopWidthIHeight  TsLabellbMsgLeftTopWidth� Height	AlignmenttaCenterCaption1   Informe abaixo o motivo da devolução do título
ParentFontFont.CharsetANSI_CHARSET
Font.Color+5M Font.Height�	Font.NameArial
Font.Style   
TsDateEditedDataLeftxTopWidthYHeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	MaxLength

ParentFontTabOrder BoundLabel.Active	BoundLabel.Caption   Data da DevoluçãoBoundLabel.Indent BoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameArialBoundLabel.Font.StylefsBold BoundLabel.LayoutsclLeftBoundLabel.MaxWidth BoundLabel.UseSkinColor	SkinData.SkinSectionEDITGlyphMode.Blend GlyphMode.GrayedDefaultToday	   TsBitBtnbtOkLeft�Top�WidthKHeightCursorcrHandPointCaptionOkTabOrderOnClick	btOkClickSkinData.SkinSectionBUTTON
ImageIndexImages
dm.Imagens  TsBitBtn
btCancelarLeft�Top�WidthYHeightCursorcrHandPointCaptionCancelarTabOrderOnClickbtCancelarClickSkinData.SkinSectionBUTTON
ImageIndexImages
dm.Imagens  	TwwDBGridGridLeft TopAWidthLHeightXControlType.StringsCheck;CheckBox;True;False Selected.StringsCheck	4	#	FMOTIVO	74	Irregularidade	F IniAttributes.Delimiter;;
TitleColor	clBtnFace	FixedColsShowHorzScrollBar	AlignalTopBorderStylebsNone
DataSourcedsIrregularidadesOptions	dgEditingdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit
dgWordWrapdgProportionalColResize TabOrderTitleAlignmenttaCenterTitleFont.CharsetANSI_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameArialTitleFont.Style 
TitleLinesTitleButtons
UseTFieldsOnMouseMoveGridMouseMove	OnMouseUpGridMouseUp  
TsCheckBox
ckRetiradoLeftTop�Width� HeightCursorcrHandPointCaptionBaixar como RetiradoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTabOrderVisibleOnClickckRetiradoClickSkinData.SkinSectionCHECKBOX
ImgChecked ImgUnchecked   
TsCheckBoxckDevolvidoLeft� Top�Width� HeightCursorcrHandPointCaptionBaixar como DevolvidoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTabOrderVisibleOnClickckDevolvidoClickSkinData.SkinSectionCHECKBOX
ImgChecked ImgUnchecked   TDataSourcedsIrregularidadesDataSetdm.IrregularidadesLeft�Toph   