object FEtiquetaRetificacao: TFEtiquetaRetificacao
  Left = 298
  Top = 230
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'AVERBA'#199#195'O DE RETIFICA'#199#195'O'
  ClientHeight = 432
  ClientWidth = 577
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 15
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 577
    Height = 432
    Align = alClient
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object btImprimir: TsBitBtn
      Left = 488
      Top = 388
      Width = 81
      Height = 33
      Cursor = crHandPoint
      Caption = 'Imprimir'
      TabOrder = 0
      OnClick = btImprimirClick
    end
    object MRetificacao: TsMemo
      Left = 8
      Top = 20
      Width = 561
      Height = 169
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      PopupMenu = PM
      TabOrder = 1
      OnChange = MRetificacaoChange
      BoundLabel.Active = True
      BoundLabel.Caption = 'Modelo'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object MEtiqueta: TsMemo
      Left = 8
      Top = 209
      Width = 561
      Height = 169
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      BoundLabel.Active = True
      BoundLabel.Caption = 'Etiqueta'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
  end
  object sSkinProvider: TsSkinProvider
    AddedTitle.Font.Charset = DEFAULT_CHARSET
    AddedTitle.Font.Color = clNone
    AddedTitle.Font.Height = -11
    AddedTitle.Font.Name = 'MS Sans Serif'
    AddedTitle.Font.Style = []
    FormHeader.AdditionalHeight = 0
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 88
    Top = 280
  end
  object PM: TPopupMenu
    Left = 176
    Top = 112
    object D1: TMenuItem
      Caption = 'Data do Protocolo'
      OnClick = D1Click
    end
    object N1: TMenuItem
      Caption = 'N'#186' do Protocolo'
      OnClick = N1Click
    end
    object D2: TMenuItem
      Caption = 'Data do Dia Atual'
      OnClick = D2Click
    end
    object N2: TMenuItem
      Caption = 'Nome do Tabeli'#227'o'
      OnClick = N2Click
    end
    object T1: TMenuItem
      Caption = 'T'#237'tulo do Tabeli'#227'o'
      OnClick = T1Click
    end
    object M1: TMenuItem
      Caption = 'Matr'#237'cula do Tabeli'#227'o'
      OnClick = M1Click
    end
    object C1: TMenuItem
      Caption = 'Cidade do Cart'#243'rio'
      OnClick = C1Click
    end
  end
end
