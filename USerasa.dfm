�
 TFSERASA 0�> TPF0TFSerasaFSerasaLeft� Top� BorderIconsbiSystemMenu BorderStylebsDialogCaption!   Exportação - SERASA / BOA VISTAClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
KeyPreview	OldCreateOrderPositionpoMainFormCenterScaledOnClick	FormClickOnClose	FormCloseOnCreate
FormCreate	OnKeyDownFormKeyDownOnShowFormShowPixelsPerInch`
TextHeight TsMemoMLeftTop Width�Height� BorderStylebsNoneColor��� Font.CharsetANSI_CHARSET
Font.Color+5M Font.Height�	Font.NameCourier New
Font.Style 
ParentFont
ScrollBarsssBothTabOrderVisibleSkinData.SkinSectionHINT  TsPanelP1Left Top Width�Height� AlignalTopTabOrder SkinData.SkinSectionPANEL TsSpeedButton
btExportarLeftTop5WidthRHeightCursorcrHandPointCaptionExportarEnabledButtonStyletbsDropDownSkinData.SkinSectionMAINMENUDropdownMenuPM2  TsSpeedButton
btImprimirLeftsTopWidthRHeightCursorcrHandPointCaptionImprimir	PopupMenuPM3ButtonStyletbsDropDownSkinData.SkinSectionMAINMENUDropdownMenuPM3  TsSpeedButtonsSpeedButton1LeftzTopPWidthHeightCursorcrHandPointOnClicksSpeedButton1ClickSkinData.SkinSectionTRANSPARENTImagesGdm.Im16
ImageIndex  
TsCheckBoxckFiltroLeftTopgWidth�HeightCursorcrHandPointCaptionk   Gerar somente a relação de Letra de Câmbio e Cheque no período de 5 anos (Comunicado 002/2012 - SERASA)TabOrderOnClickckFiltroClick
ImgChecked ImgUnchecked SkinData.SkinSectionCHECKBOX  TsBitBtn	btFiltrarLeftTopWidthRHeightCursorcrHandPointCaptionFiltrarTabOrderOnClickbtFiltrarClick
ImageIndex Images
dm.ImagensSkinData.SkinSectionMAINMENU  
TsGroupBoxGbProtocoloLeftTopWidth� Height=Caption	ProtocoloTabOrder SkinData.SkinSectionMAINMENUCaptionSkinCOMBOBOX TsEditedProtFinalLeft� TopWidthBHeightColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderSkinData.SkinSectionEDITBoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionFinalBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style   TsEditedProtInicialLeft(TopWidthBHeightColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrder OnExitedProtInicialExitSkinData.SkinSectionEDITBoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionInicialBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style    TsRadioButtonRb1LeftTopWidthHeightCursorcrHandPointChecked	TabOrderTabStop	OnClickRb1ClickSkinData.SkinSectionTRANSPARENT  
TsGroupBoxGbDataLeft� TopWidthHeight=CaptionDataEnabledTabOrderSkinData.SkinSectionMAINMENUCaptionSkinCOMBOBOX 
TsDateEditedDataInicialLeft(TopWidth]HeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontTabOrder BoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionInicialBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style SkinData.SkinSectionEDITGlyphMode.GrayedGlyphMode.Blend DefaultToday	  
TsDateEditedDataFinalLeft� TopWidthaHeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontTabOrderBoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionFinalBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style SkinData.SkinSectionEDITGlyphMode.GrayedGlyphMode.Blend DefaultToday	   TsRadioButtonRb2Left� TopWidthHeightCursorcrHandPointTabOrderOnClickRb2ClickSkinData.SkinSectionTRANSPARENT  TsBitBtn
btCertidaoLeftsTop5WidthRHeightCursorcrHandPointCaption	   Certidão
Glyph.Data
�   �   BM�       v   (               x                    �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ����������     ����������������������������������������������������������������������  ������������ ����   ����������TabOrderOnClickbtCertidaoClickSkinData.SkinSectionMAINMENU  
TsCheckBoxckProtestadosLeftTop|Width� HeightCursorcrHandPointCaption"   Gerar somente títulos protestadosTabOrder
ImgChecked ImgUnchecked SkinData.SkinSectionCHECKBOX  
TsCheckBoxckAviso042015LeftTopRWidthkHeightCursorcrHandPointCaptionl   Gerar novamente a relação, no período de 5 anos, de títulos solicitados pelo SERASA (Comunicado 05/2015)AutoSizeFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTabOrderOnClickckAviso042015Click
ImgChecked ImgUnchecked SkinData.SkinSectionCHECKBOX   	TwwDBGridGridAtosLeft Top� Width�Height'ControlType.StringsCheck;CheckBox;True;False Selected.StringsCheck	5	*	FPROTOCOLO	10	Protocolo	TStatus	16	Status	FData	12	Data	FDEVEDOR	43	Devedor	TSituacao	19	Tipo	FACEITE	5	Aceite	F IniAttributes.Delimiter;;IniAttributes.UnicodeIniFile
TitleColor	clBtnFace	FixedColsShowHorzScrollBar	EditControlOptionsecoCheckboxSingleClickecoSearchOwnerForm AlignalClientBorderStylebsNone
DataSourcedsRXFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
KeyOptions Options	dgEditingdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit
dgWordWrapdgProportionalColResize 
ParentFont	PopupMenuPMTabOrderTitleAlignmenttaCenterTitleFont.CharsetANSI_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameTahomaTitleFont.Style 
TitleLinesTitleButtons	
UseTFieldsOnMouseMoveGridAtosMouseMove  	TQuickRep	qkModelo1Left^Top@WidthcHeightShowingPreviewDataSetRXFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' 	OnEndPageqkModelo1EndPageOptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.OrientationpoLandscapePage.PaperSizeA4Page.ContinuousPage.Values       �@      @�
@       �@      ��
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPreviewWidth�PreviewHeight�PrevInitialZoomqrZoomToFitPreviewDefaultSaveTypestQRPPreviewLeft 
PreviewTop  TQRBandTituloLeftqTop&Width�HeightzAlignToBottomBeforePrintTituloBeforePrintTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUUe�@      ��
@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageHeader TQRLabel
lbTabeliaoLeft�TopWidth*HeightSize.Values      ��@      ʙ	@      @�@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionTabeliaoColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbCartorioLeft�TopWidth%HeightSize.Values      ��@UUUUUUs�	@ XUUUUU�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionCartorioColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel1Left�Top"WidthHeightSize.Values      ��@UUUUUU��@�������@UUUUUU=�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption,   PROTESTO DE TÍTULOS E DOCUMENTOS DE DÍVIDAColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbEnderecoLeft�Top0Width/HeightSize.Values      ��@      ̘	@       �@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption	   EndereçoColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbTelefoneLeft�Top<Width+HeightSize.Values      ��@UUUUUUu�	@      ��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionTelefoneColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	lbRelacaoLeft�Top]Width� HeightSize.Values������*�@UUUUUU��@      �@UUUUUUu�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption!   RELAÇÃO DE TÍTULOS PROTESTADOSColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	lbPeriodoLeft�TopiWidth'HeightSize.Values������*�@�������	@      �@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   PeríodoColorclWhiteTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbSiteLeft�TopIWidthHeightSize.Values������*�@UUUUUUm�	@UUUUUU%�@UUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionSiteColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  
TQRSysData
QRSysData1Left�TopWidthCHeightSize.Values������*�@��������
@UUUUUUU�@UUUUUUE�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandColorclWhiteDataqrsPageNumberText   Pág.: TransparentExportAsexptTextFontSize   TQRBandDetalheLeftqTop� Width�HeightFrame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomBeforePrintDetalheBeforePrintTransparentBandForceNewColumnForceNewPageSize.Values     @�@      ��
@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail 	TQRDBText	QRDBText1LeftTopWidth� HeightSize.Values������*�@       �@UUUUUUU�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldDevedorFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText2LeftTopWidth[HeightSize.Values������*�@UUUUUU=�@UUUUUUU�@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataSetRX	DataFieldCPF_CNPJ_DevedorFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBTextqrDataLeftdTopWidth7HeightSize.Values������*�@������z�@UUUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldDt_PagamentoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText5Left�TopWidth7HeightSize.Values������*�@      ��	@UUUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldValor_TituloFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText6LeftETopWidthBHeightSize.Values������*�@UUUUUU'�	@UUUUUUU�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldDt_RegistroFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText7Left�TopWidth7HeightSize.Values������*�@��������	@UUUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataField	ProtocoloFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText3Left�TopWidthHeightSize.Values������*�@     �	@UUUUUUU�@������*�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandColorclWhiteDataSetRX	DataFieldAceiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText8LeftTopWidthHeightSize.Values������*�@      y�
@UUUUUUU�@������
�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandColorclWhiteDataSetRX	DataFieldMotivoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText9Left_TopWidthBHeightSize.Values������*�@��������
@UUUUUUU�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandColorclWhiteDataSetRX	DataFieldDt_VencimentoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabel
lbNaturezaLeft�TopWidth,HeightSize.Values������*�@UUUUUU��	@UUUUUUU�@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaption
lbNaturezaColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   TQRChildBand
ChildBand1LeftqTop� Width�HeightFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUU�@      ��
@ PreCaluculateBandHeightKeepOnOnePage
ParentBandTitulo
PrintOrdercboAfterParent TQRLabelQRLabel2LeftTopWidth*HeightSize.Values XUUUU��@ �����J�@ �������@ XUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionDevedorColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel3LeftTopWidth4HeightSize.Values������*�@UUUUUU=�@UUUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaptionCPF/CNPJColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbDataLeftdTopWidth7HeightSize.Values������*�@������z�@UUUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaption
Data Canc.ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel4Left�TopWidth,HeightSize.Values������*�@UUUUUU��	@UUUUUUU�@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaptionNaturezaColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel5Left�TopWidth/HeightSize.Values������*�@UUUUUUW�	@UUUUUUU�@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaptionVlr. Docto.ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel6LeftETopWidthBHeightSize.Values������*�@UUUUUU'�	@UUUUUUU�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaptionData ProtestoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel7Left�TopWidth.HeightSize.Values������*�@��������	@UUUUUUU�@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaption	ProtocoloColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel8Left�TopWidthHeightSize.Values������*�@TUUUUU��	@UUUUUUU�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaptionAc.ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel9LeftTopWidthHeightSize.Values������*�@      y�
@UUUUUUU�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaptionMotiv.ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbDataVencimentoLeft:TopWidth� HeightSize.Values������*�@UUUUUU��
@UUUUUUU�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaption   Data Venc./Emissão (Se cheque)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   TQRBandRodapeLeftqTop� Width�Height� AlignToBottomBeforePrintRodapeBeforePrintTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUU5�@      ��
@ PreCaluculateBandHeightKeepOnOnePageBandType	rbSummary TQRLabellbCidadeLeft�TopRWidth*HeightSize.Values������*�@      ʙ	@UUUUUU��@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionlbCidadeColorclWhiteTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel10LeftTopWidth`HeightSize.Values������*�@       �@UUUUUUU�@       �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   PFP = Não PagamentoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel11LeftTopWidthQHeightSize.Values������*�@       �@       �@      P�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   PFA = Não AceiteColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel12LeftTopWidth`HeightSize.Values������*�@       �@VUUUUU��@       �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   PFD = Não DevoluçãoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShapeqrAssinaturaLeft]TopxWidthHeightEnabledSize.ValuesUUUUUUU� @UUUUUU��@      ��@      ��@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabellbAssinaturaLeft�TopzWidth*HeightEnabledSize.Values������*�@      ʙ	@UUUUUUe�@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption	   TabeliãoColorclWhiteTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbQtdLeft�Top:Width� HeightSize.Values������*�@������@�	@UUUUUUu�@������ڻ@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   QUANTIDADE DE TÍTULOS:ColorclWhiteTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize    TsStatusBarBLeft Top�Width�HeightCursorcrHandPointPanelsTextLavradosWidthA Text0Width< Text
CanceladosWidthK Text0Width< Text   Próxima Remessa SerasaWidth�  Width< Text   Próxima Remessa BoaVistaWidth�  Width2  SkinData.SkinSection	STATUSBAR TsCurrencyEditedSerasaLeft�TopWidth7Height	AlignmenttaLeftJustifyAutoSizeColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrder SkinData.SkinSectionEDITGlyphMode.GrayedGlyphMode.Blend DisplayFormat0  TsCurrencyEdit
edBoaVistaLeftbTopWidth7Height	AlignmenttaLeftJustifyAutoSizeColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderSkinData.SkinSectionEDITGlyphMode.GrayedGlyphMode.Blend DisplayFormat0   	TQuickRep	qkModelo2Left^Top�WidthcHeightShowingPreviewDataSetRXFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' 	OnEndPageqkModelo2EndPageOptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.OrientationpoLandscapePage.PaperSizeA4Page.ContinuousPage.Values       �@      @�
@       �@      ��
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPreviewWidth�PreviewHeight�PrevInitialZoomqrZoomToFitPreviewDefaultSaveTypestQRPPreviewLeft 
PreviewTop  TQRBandQRBand1LeftqTop&Width�Height� AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values      ��@      ��
@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageHeader TQRLabel	QRLabel18Left^TopWidthHeightSize.Values������*�@��������@UUUUUU��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption%   CERTIDÃO PLURINOMINAL DE PROTESTOS EColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel13Left^TopWidth� HeightSize.Values������*�@��������@UUUUUUu�@      ܐ@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   OUTROS DOCUMENTOS DE DÍVIDASColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbProtestoLeft^TopHWidth� HeightSize.ValuesUUUUUUU�@��������@      ��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption&   |  | Protesto Lavrado (para Inclusão)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabellbCanceladoLeftXTopHWidth� HeightSize.ValuesUUUUUUU�@      p�	@      ��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption(   |  | Protesto Cancelado (para Exclusão)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRImageQRImage1Left TopWidth� Height.Size.Values������j�@UUUUUUU�@       �@������J�@ XLColumn XLNumFormat	nfGeneralAutoSize	Center	Picture.Data
�P  
TJPEGImage�P  ���� JFIF  x x  ��	VExif  MM *                  b       j(       1       r2       ��i       �   �   x      x   Adobe Photoshop 7.0 2014:01:30 12:06:31    �    ��  �       Ƞ       .                          (             &      (       H      H   ���� JFIF  H H  �� Adobe_CM �� Adobe d�   �� � 			
��   �" ��  ��?          	
         	
 3 !1AQa"q�2���B#$R�b34r��C%�S���cs5���&D�TdE£t6�U�e���u��F'���������������Vfv��������7GWgw�������� 5 !1AQaq"2����B#�R��3$b�r��CScs4�%���&5��D�T�dEU6te����u��F���������������Vfv��������'7GWgw�������   ? �B����x�C'
��Xs]��]���R�J*���~��uz�=/U�+���^mշ3�ڧ�f:!�=�ՑE�Z��}��u���=^������R�d�n�IJ���=���f�����<k�mN��e�[^V�۳ue��ʿ��w�;1����ǩ���M��}�}�]�m�ރ-~�R��g�VP��=��G���w%"aa��n��v;1۰:�8�o��\�ٸN�����l}� ��_O�S�XGQ�v#꩏�~��Q{r�w��������c�����$q�]��)`k]�X}K##���p��ȋ0�-�]����v���ed[[/����7!�X�mc�v���O��K��du�ћ�qh�*�eUk�C�[��_��o��U�Q���O��މ�����+ǯ�"��m5���o������(z"dx��q~��Ke��IG� �L�8I��o�?+���V>�K)��ݼnw�w�9[��Q���>���EU��Vq������� ���mp���w��S�[iŧ*���Nc1�e]`ƺ���en��S��;u�M�N�xi&|&1$K�q\M��>�ҋ�JK	�ZE�W���m��,��t�:ǹ�u�5ٷӯ�ݏg�]�ۍ�ZM-�/�e@dg�m�m-�7+����è������_�z({S�z� ~R�������Ư�nG��U��z׍�������/�C���#��zuW��#�~Q3"�l"�^�J��Q���~�����E�{S�����{������Ӯ~�6zW_�����ŧ:�:��E75��k}��+eN����v�Yu,���Ƈ��4p��%	Gp�3������~����K��b�c?��Ȥ��t;��������Y]�TOզ����̻'2����mcB�������zM��,�=OW�'�n&O��ѿ�Y!�WԹ�/&�2s,��r�kkd��*mU�������c� �{Э��]u��6ʨ�)�ހc���X�R��Ki����?��I	d������ ��f_x~f]�5V-m!�`���>�����h�����?��z��/O�V��ӕfk�N9����za�}[Vݏf���� �[	"e�������}.��y�^�ާ�*��8�꥙b��.{�Υչ����;\Ϧ�� ��98�{:�zOV���8,���c�����ϩ�l�O����w���<|�/f�ۣ��$��(=�V� ���s2��ټ�*��1�����oJ�vwK��:�1룩[U^�;���pvݯk[��?�&��=Ka��_�2�B���k�ݾ��Y[l�*o�v�ݻ�����I7�`��.##�J��7���O7��h�jzc�f0ͦ� I�����mv�S�}?Yޏ�K�]��_��c�m�y�{����f9�!�շ��=?�}�����~�S�[�'e���� I�C�5�� ���􁍓FK�,ȶ�m���ѻֱ���Ƴc��MU�R�Ϫ������K�;���k�U�ٗ��9���2��g��[�&����\c����������[��<�S��}�
��h�Ym����6���P��A.1��v����t$f~kߪb >Z�����Photoshop 3.0 8BIM         8BIM%     F�&�Vڰ�����w8BIM�      x     x    8BIM&               ?�  8BIM        8BIM        8BIM�     	         8BIM
       8BIM'     
        8BIM�     H /ff  lff       /ff  ���       2    Z         5    -        8BIM�     p  �����������������������    �����������������������    �����������������������    �����������������������  8BIM          @  @    8BIM         8BIM    ]              .   �    s e r a s a - e x p e r i a n - l o g o                                 �   .                                            null      boundsObjc         Rct1       Top long        Leftlong        Btomlong   .    Rghtlong   �   slicesVlLs   Objc        slice      sliceIDlong       groupIDlong       originenum   ESliceOrigin   autoGenerated    Typeenum   
ESliceType    Img    boundsObjc         Rct1       Top long        Leftlong        Btomlong   .    Rghtlong   �   urlTEXT         nullTEXT         MsgeTEXT        altTagTEXT        cellTextIsHTMLbool   cellTextTEXT        	horzAlignenum   ESliceHorzAlign   default   	vertAlignenum   ESliceVertAlign   default   bgColorTypeenum   ESliceBGColorType    None   	topOutsetlong       
leftOutsetlong       bottomOutsetlong       rightOutsetlong     8BIM      8BIM        8BIM    D      �     �  +�  (  ���� JFIF  H H  �� Adobe_CM �� Adobe d�   �� � 			
��   �" ��  ��?          	
         	
 3 !1AQa"q�2���B#$R�b34r��C%�S���cs5���&D�TdE£t6�U�e���u��F'���������������Vfv��������7GWgw�������� 5 !1AQaq"2����B#�R��3$b�r��CScs4�%���&5��D�T�dEU6te����u��F���������������Vfv��������'7GWgw�������   ? �B����x�C'
��Xs]��]���R�J*���~��uz�=/U�+���^mշ3�ڧ�f:!�=�ՑE�Z��}��u���=^������R�d�n�IJ���=���f�����<k�mN��e�[^V�۳ue��ʿ��w�;1����ǩ���M��}�}�]�m�ރ-~�R��g�VP��=��G���w%"aa��n��v;1۰:�8�o��\�ٸN�����l}� ��_O�S�XGQ�v#꩏�~��Q{r�w��������c�����$q�]��)`k]�X}K##���p��ȋ0�-�]����v���ed[[/����7!�X�mc�v���O��K��du�ћ�qh�*�eUk�C�[��_��o��U�Q���O��މ�����+ǯ�"��m5���o������(z"dx��q~��Ke��IG� �L�8I��o�?+���V>�K)��ݼnw�w�9[��Q���>���EU��Vq������� ���mp���w��S�[iŧ*���Nc1�e]`ƺ���en��S��;u�M�N�xi&|&1$K�q\M��>�ҋ�JK	�ZE�W���m��,��t�:ǹ�u�5ٷӯ�ݏg�]�ۍ�ZM-�/�e@dg�m�m-�7+����è������_�z({S�z� ~R�������Ư�nG��U��z׍�������/�C���#��zuW��#�~Q3"�l"�^�J��Q���~�����E�{S�����{������Ӯ~�6zW_�����ŧ:�:��E75��k}��+eN����v�Yu,���Ƈ��4p��%	Gp�3������~����K��b�c?��Ȥ��t;��������Y]�TOզ����̻'2����mcB�������zM��,�=OW�'�n&O��ѿ�Y!�WԹ�/&�2s,��r�kkd��*mU�������c� �{Э��]u��6ʨ�)�ހc���X�R��Ki����?��I	d������ ��f_x~f]�5V-m!�`���>�����h�����?��z��/O�V��ӕfk�N9����za�}[Vݏf���� �[	"e�������}.��y�^�ާ�*��8�꥙b��.{�Υչ����;\Ϧ�� ��98�{:�zOV���8,���c�����ϩ�l�O����w���<|�/f�ۣ��$��(=�V� ���s2��ټ�*��1�����oJ�vwK��:�1룩[U^�;���pvݯk[��?�&��=Ka��_�2�B���k�ݾ��Y[l�*o�v�ݻ�����I7�`��.##�J��7���O7��h�jzc�f0ͦ� I�����mv�S�}?Yޏ�K�]��_��c�m�y�{����f9�!�շ��=?�}�����~�S�[�'e���� I�C�5�� ���􁍓FK�,ȶ�m���ѻֱ���Ƴc��MU�R�Ϫ������K�;���k�U�ٗ��9���2��g��[�&����\c����������[��<�S��}�
��h�Ym����6���P��A.1��v����t$f~kߪb >Z���8BIM!     U       A d o b e   P h o t o s h o p    A d o b e   P h o t o s h o p   7 . 0    8BIM        ��Hhttp://ns.adobe.com/xap/1.0/ <?xpacket begin='﻿' id='W5M0MpCehiHzreSzNTczkc9d'?>
<?adobe-xap-filters esc="CR"?>
<x:xapmeta xmlns:x='adobe:ns:meta/' x:xaptk='XMP toolkit 2.8.2-33, framework 1.5'>
<rdf:RDF xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#' xmlns:iX='http://ns.adobe.com/iX/1.0/'>

 <rdf:Description about='uuid:b13586d1-89b7-11e3-870a-a852d09ddec1'
  xmlns:xapMM='http://ns.adobe.com/xap/1.0/mm/'>
  <xapMM:DocumentID>adobe:docid:photoshop:b13586cf-89b7-11e3-870a-a852d09ddec1</xapMM:DocumentID>
 </rdf:Description>

</rdf:RDF>
</x:xapmeta>
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                                                                    
                                                       
<?xpacket end='w'?>�� !Adobe d@                �� � ��  . � �� �              	
            	        	 !0"2#       !1A"Qaq�2���B�	 ��#3����     !1AQaq"�2 ��0��BR#���b3r�C$��    ��ehWV�Vd�  	���u�16�\��L����9��9�@ F2e� kj�aY5ԆNv�	���EƵ�mD�x����?θ9�չ�\v|�V�*��8a�����M�.p�d��58($,[9�7�|���j6���s����r����I[)(�;��ӹ�{�󇦾~=Iٗ���V^���k���Ӹ�����vr��/{v���4>8�6G][�_S}-��]�B��W	����LL���?���+��y���g�~IY�ck���g}q�me[����� d�Y�C9l6lCft��-JV�k��o�Z�_r$���*�U����� R8��l��+�s���/�nN}��I_�r%�B����U�Җ�����v�  P  A!>n�=�N�n�{IN�z0` d   ��   �G�?��6B�:tJ�*\S�-��WgrD��O}����|��p!��󛿩����	#�MqDD��o��L o%ɓ���KI��tD��A'����J�<z�TO��-���8Q(p �*>.��ׁ;�e���Fm5nP�Ӵ�,6���
��G#0(ԕc]zɍ�ْ5�@�f4v8x@�m%,����K�rZ�aUX�R9$|��/����:�~5�3��<*��_G�V�	?�g���QY>DF�E�1-C�2e�"1G����hl����D���b�nH�ʚ�r"1�qo�r�O��F4���9$�'�\�3d�U?2��FG4�ã��<%ɾ�S�S:j;D&�1_%ȶD�DR<�=f��X�m!��"UY��&B��� �L$��&/�.C�E�@g�2���M3e��5��(}{\� �`l�����8���˜�&�\���L�8|����8����A�G������   �3�� ����"P1)3�R����gL
V�F����0�� =��cG���݋b��2�Z���!�z0p���l4
G{�����.�k�e��x�E��9-������B~~���H~��_7�pL@�tHu��j_��j���"X,�JfKm"�J��\ﶂ����%g��,'�ɦ$���jTN�X|o!��"݂��*q�ڪ�E�D��"Hnu���4Q�`���ɒ-��m�1��n��ʕD��/����D���Rg���5��P��}��[��]`��4
�+�;5�kbhܼ��tj�O��4�iZ�f�t��8�NTI#�K47Z��L��љ��4�!���_��ZHRX���(e��5�l�m����]�giX���u߇�&6\x@Pɀ�2��6 f!��RHS���fl���N-���F��:�Y�&)�����!$1�Ñ>8��a<� 8@q��8��SeJu����q�C	���   7�~�}l|�Nn�Ev�QIĢe�G�l�����!e�4�,��ԏ��� �>Hr�S�����4��F�A%�]�Tl�~A�e(��|�@)�C9�Kmh���+v�z�m����փ�V��)J{���Z�����l�˘���	��$1@ �z��4�4����c  @y+i�οO6_�L
�۽�uI�u��������݃�v{�7^�q�5�������Ċ����`I���`�7#�TF��vMj�̩�K� �_u�w� �˹� UiM�x�"�����MKh������ vm���/��-����yo���-uGkoe̟f��m-�~���"}��ۣ���y���wV��TcZ�;W�����v�]�����񶁤͍�Ϻw?�ڒ�۝=ٽI��7�z{_�J�R.}�4�+�e��i}jf��b��u�f�[*駖#ձ�K��6��V��>��?c+e� ��z\�B�(]���+[ky��~f/�?�!�$�'?�)�8��bQ>3�L��n9@b/ڕ_ߓ�ܚ���V]^���z��m���C��C	v+Y�{$ى9���&2�?B8��8�D	�&��|	��#u�ny�&�C�?�s�8�!Ĕ�<�x7~C�#pLn����~�?�� ? �X ���U�#t���!����\��kB�~����^?�����q@S���F�*�P��|(`?q�t��ǣ��js�b�aQKPѱ��$��׽��p�e�·q Zsծ�;H=0�ii�������醆5��4�9�m��<��|�j��]SP?q���*�ͻ}A������?ݎOz��R�UGq��U�fG\�˷LXy�ޞ���iت�?�P<�Gr�{O�Ich�ZjS����v@5�@@PSw�L>�q����Ҋ?�Q�08�����ޥ]{�u�e/�¦'�e+���ЭϹz��U(q�Nﲞ������P�������b�����u=Q-k^֢1�p /bs��#�넲{*ɘ��L��j�ޥ@���x�M\�kk�������{������L�-w+�!t��ԭ���������*�&}qAi����L�l��C�0���S��Eꩋ����o�4��;	#iM�~`��e�F-p�ދ���^��3 �R�ܪ3oýÒXMp��@�?S��8d�zت� ���8�o�O`������W<p[�����0�� �k�v�:��0�Mx�cj^���y� z;>����m��
���p�P�Q��<�ǩ�й��	VmpF�|��.�[���R��q��>�ѐLmP xE�<�u��?"��v��*9�s���Ӹ���n�H�(w���^ߘ S�D*��}��d��Z�,r4��ۺ0���{�,v�It���V�ec`��ǹ�q(�5)�9�W$����e��yCUcz�B�AD�1�/<j�3���Ƃ���72K2�#��\k� \���0��j�����E�b���S ��4�f��G��]Tر�+=i��Z�n��k����e�R�VDJ�stV�cu֎YGIM9h������25�S�<q�}���ECK%0�V���n�ސ��Fc<�z}o:�������E��KdF�@4=Nx�~EH4����әO-A9�ش��9�0�T�E�=KSc�4J��.������4�ܜ�S�Za�'8��P�����^h��$�<IRַ�d�7Ҩv���9�]�|Uw�fk�ּ�t^� r�M=@"��Faw���"�q�������6!c\��2�;��]`7�Am/Uq�ïn��c�UW�@�����@;@h1$w9.�1`�]`�ɡ�F�n�Iϩ��M�/���vLƒǜ�`��r�,\y,W�/w��7&�QG��8啔�'�m���b���]�g�SӠ�M]' ������FC��o��D��A�x�_�9�5=3)�H߰J�l L`���1��}J/����2����-R���F@&/z��f�BLM@v����5�tņ�S�)a����ָ$a�� �s.q����OU�~t� �Z��$��p�<oZ�j���@w�o�}��
î=��I�W�����t��7�Έ�s���Z�}�i���ַ�����ѷi��=���,����n�y��"F4���� �J�!���ㆲ�t��:@�#�P�&g=����|��U�v��cZ�z�iEϮA;`PT�d��2vF�H@��U��8N��1�H��8B�n��`�H�c��LJu�pds�~���ѐ����L�@d�v�ۘ�{���a�0�	���_����\����ө�aw��:/S�?@�`{c׃�����x�7?�������� �� ? �߼�|1�i��tn)��8�^��5Y�@t�1���@W�Hx������G󸜛�w�+�?��\,4��g�������2�J����K��,�|�]k&��������O�����>8�eED�Ѹ����B��zdt\���:��s�sB5	��`hZ��눚�s�H���Á'p���� <{��lk�z��3���>S�>������<���� ��pNLq���k�m$��9]����:��1�:OVӚ���� N8M����]�Y,n�HJ��7SR?v.\��S.�L��9��r��u�6���K� D#���,Iw؋cU'r�Ԍ�8��oq}�W�(,8�AD���������a��T��GG5[Q���+�q����{�G|"���+h��|/�'4=�3T"�(��ʆ��o�] ����$�I�pp?����θ�`�2��{���HU�pq 8B5�b�')����]CD/�@��6����Fz.G6�G�;uO�g{���� �2PS㋘����;w4=��8*۴��D��^�5Uʅ�"i�Ѹ(b<�� S���ŕ��S���rE!�#^JmB	G"�x���-��X�O �;�=o ��uŮ+V�����>�@�Q�7\�$)�_E�@�I��Dؙ�2�����v(�Ij�p����i��.2}E��oequLp�1+bk�D8��� ��n?A^O	��{�]� #a{\�Q]�W$�U=O��kn�{��7�w��<r�Ž�vG�%����ɔN�N���-Wۥ�8��T��0���l.9�k��-no`m5��$�ԍq(��q%�e���r���q�7�s��DnG�p�@;c�C�-���!��=�C]�N�@ْ��F��mP2�i��$n=�d8�����F&lrN��Q@�$�~X��V�&�����FB�A �sM�u��=�(D7Fo |��ŭ�"�ett���F����Ka�jzڈ�.����=~����WmdZ��9� � � Ѵ����P�?��\R��q�0a#����S<}F�{I���/u1q$�C�C�!�L]x�<
����IQ2� *K\�e�|�ˇY�W����-i�0D�-nH��;�9t�����2�G�N�F[���4	��k��Y k�A���9j28��S).��KK�G$�b��p\:�9����U�UQ:F����$JZQ��� �[��,5-3�ӯF�N9�4qWS�Q���
8�2  �u��4���o�=O%��C�fF��-n�u��g��b1]69��Ơ�IAl���{��@�/n��Z�����n��yi>�Z�� �"��<��-��SS�G�G�C7mF��%��_���6gF��{Oq������K*!�e �&@�R'�~�;W�ž�KH6�s��&h���i��TUrY*D�L�� s�S$Dw�q��u�AA�D�d�Hִ{ =�ͮ�5�zHC�o%Z��3
1��d��S�����=S��� LF�����A��\jdk@���+��*4>G_�0J����a���toC爛�t��=b{\Z�m��D�i��b��Pq�����iXZn0�n��yB _w��q�!�\B�Kd���#L��J�CO��SP�Y+B8D�8v�$�珞�r��&�Hs*G�n:�۾!!:�(-t�!�e�&MS$"Ĩ��I��1��j�ؓs���ۯ�X�1��#�#�fD=�On�k�iǸ�d��1�0��� � ��\�_ِ� ����SB���X\{d	#�%
� �ßY$dא �W _���m�b崂?,�i�� ? �}���r=�0w���=<�K�����u��%��q`c�޳�l�\¸yJ��'��٬X0h���#ǅ�' @D�ߩ[e��W��53ə���ED�3����I�t|��c 	��z�����7�gQ-n�[�7���'%b�++��*d�0$#���}��I��%B�C3q#--(�&Q�Y�+�x�ۃ͛�Q2�1�� ��kv���/uwk8j�~�.�r!g-Lr܏��Y���D ��K��:�� ����!�C���G�� ��9\`~���C���G�8�
�C R��v�A�0�@>� ��QN��.�.�cn�����k�2.��,���QM�1��T��{���/f鋤U�*e�JV%Q�@@���-��2I��*��)�=��*�0�|@1����� M��Ð���  o]��^��{���>}� 2a���;� }=�r#���~����"����=��x3�֪��}���h�N�����X/���)'��.�b���CyG �Oiq�d��kh%��6��EG�ܛ���tD�(/Jx������c	D��_ۡ��-�-(R&�G��M��� Sj���x� }������/�xd� 81+�n�5���p�l�[ �����b����NF�&��Bv�gP�$.��}��Z2U�g_^�x�wc�w4�%!�󒯤'�2Mٱt� '�r=D�c�̂�)�X|T)�%Q3�H ,�껱������kbY*݁�nSi-eUH7+��|#x��\�n����F��s��!�" 뜇�9���w�!���:Ϙ� !���:��;�C6�J4u#_w&��H֧*n�0�Ub�tѩ� ��LJA�{���aǷ����Ƀ���A�`D��#����A��L?:�����F�x�$Z*�\�?�8@� �Ԟ��|1��+Z�X櫛������a�e\H�{�U�'*ȑe>`��"E 0�0=n�_�w�/�T�ٵ�bN�Y�B\�aJ���1�S^=�Q0?�dh�?p�����b�M��l���V�'w������e�W�[����YEK����E7�`�F��Z���_#�������|�$�J�U"7oO�LEy]��|˲�H�2`Ln׽�t�<Q��4	�k;�ηs����k��)2Z�կ����F$`f�.P0��z�q��0�N�h��[�N���4�x ��,5n�	^UI��Lr��8�&;��e�|f���n��-P��8��6��I���S���2�p�$U�b���H�=����k nH{������R��*9)y:����]���Y��I�B�E@�8�4�A�! � �h�+�����I	x��P�
�����> ��q�9�Y�dF����S+rvz�kM/42	^�N x��Eg�H��Q�� p#� ��� �Gj:?S���TO����X3zj�rW�R��GxC���(x�.P8��p'�%�Ve���Z�񔖡"L�H��|gK��+C&m>!�M*�R��(� 8u�d�{c�\6�ԍ�D���nz�Y:ĢP�w-QUH'R�NU��{��E��� z�+$�2�zW�����.�5�m%�t���rTN%�gHSÎ�
�#_EԸ��������?��ֻ2&lt}m��9��,_2�'����4�&��~�BK|oz�.��"�e�gg��
ʚ�(�`?�%�1IS51Sl�� �zӚ��zS[Q(����j������%*�x�f�cD��4l�ě�"Q�V���K�B�u�7������RY-�-��/�������ʬ��Z��9/�+�Rp�d��*.���`��N4i���`�kŞ�3g-]�V�R*h-dvd�`�
L����(D���1�:�;��V��y�9m[ݛR��ɡ�N�Y6�
���!&�QE��(C��鎭�3`��&C�L��I�r��BF_�l�n��A��9�Qr��A"��@�>�ҕ�����m�Y��펫Q���$�n�m�3��VdQ�˙4�(dL� �\���*���e��u����"�U������*�2�D���$�|�9�`�\�j6T�R����ݜ�S|5�ױ붊��5}�%�z�z�	�i�UpӐ��jӅj�.�ұ��~�B<�v)�$#T�},Q \�"�!D<z�˴kH
��� �r2�ƢH&-Bk�&	Qg�1U�s[]�!Â.d�#���M��̴����z�v�w�Z�]ǐ���5c:��~�T�\�����T�4Ȣ�L�J�
8��� ��/D�^�Q���R�V�ɶ��lJ��� <�R@
r-�	1�[�D��״sq�L��ޛ��X�O��ZRJ�B�����&�"
$���"���0���mʮ�=���ǭ~�fj�e��� �W��dgq�_�$��pl�]4��劲1Nup&�� ��D�θ�Q8�{��q�/����5T]�$y��r_﷋3��-�`Ʉ�{{Hp?bh�;�ּk�
Z��V���m�#�J�W�󞍤A ��1,�WXU�����og�_$�[��F�	�z|!�]<f�9�r���YV��J��� z�w �u}�f���Z��O?�&��K>ኅ�Mۆ��ΐ�=@PUȘ��_|�Ǘ�h�e7s����Jf�Q�R54c�
�|Y%�(|�&b�Q <��uoh�C]nʹ�̾�5M�b�jf7'K�#0�w�74p�Rȩ��6O������Wk˱qh]�����_zE3M�fl��u�D۷I"��H�C��O�����{^Cʱ�������I�Y���=�I�J�2!���E�Zg������ƈ����[��y��c9r�3����T[�Vj>x��ۘ3<M�x�O�����Z/��s[��u���{m�E��w*��i���wx�R"��5ra�����ͣ�\a��m��u��M+�O�_q	G�:A�,k:t����	��~��ښ_y�Czﭲ��|��;W�ry���V�$�Z��A/o����w�+(W�J�D����9���{Z��c{��Mr�Zo\kmىe���컷o�$*R
��U�`�h���1�,G��d��01����*(II�Sj�W��������4dw ��������wf�����u&����6Ɠ]���F���4T1J�	��ᔹ6nuu��]%���*R�x'E����Q� ��Ep���M��v�0 ��s�c��g�h��V�eK��b(A����)��/3��X���7o�`�3�^�����@�&0 	�0Q��9���yG���=�{� `0�q��� ��ߢ��� $�)C�w���L�� >�������.T!��c�o���C�?À���~��C#�>��c�g�Ө�t�VJ�^�
�����T���Y�����⑋�N��2+ܗᶌ��k��4k\f������ī�Ǳ� w�m�G�oQ�(�or Q�뇵�+p�A�]G_����+9��Nͬ��=ӕ+�;i��b�#���*?!@{�մ�ե��\���X_B�g\����Z��1�,2�ҏw G�e����=C�����k�Ԭ����&M������S��Q�kG�-��$c�*�!@}{t�{`@��0�zw���<N!� d�۰�>ε�e�^����u�z��*��K;��2�3�!����� AA����(	�D��J)��8��C"|�v�q��B/����3⠉@ M����! (G.}<��?gY*e��ro"��"p}]C"n���>���%�Q({`r!��;��� ���`����� � 1_�~����z� ������������A�Q���|� �{��{~��*x��7� ���0���?WE�g����{{G�����0���D<�CM��{z~� C �� �w!������Yq� �]�8�K��?��b�/���?�_��{�!�"���#߮����z�Q �J?�?�����  TQRLabellbInfo1LeftTopXWidthGHeightSize.ValuesUUUUUUU�@UUUUUUU� @VUUUUU��@������ڻ@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   InformaçõesColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbInfo3LeftTopxWidthGHeightSize.ValuesUUUUUUU�@UUUUUUU� @      ��@������ڻ@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   InformaçõesColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbInfo2LeftTophWidthGHeightSize.ValuesUUUUUUU�@UUUUUUU� @UUUUUU��@������ڻ@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   InformaçõesColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   TQRBandQRBand2LeftqTop� Width�Height� Frame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomBeforePrintQRBand2BeforePrintTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUUM�@      ��
@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail 	TQRDBText	QRDBText4LeftnTopWidth�HeightSize.Values������*�@UUUUUU��@UUUUUU�@      ڑ	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldDevedorFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText10Left�TopWidth[HeightSize.Values������*�@UUUUUU��	@UUUUUU�@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataSetRX	DataFieldCPF_CNPJ_DevedorFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText11LeftTopsWidthaHeightSize.Values������*�@       �@������"�@������R�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldDt_PagamentoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText12LeftTopSWidthaHeightSize.Values������*�@       �@��������@������R�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldValor_TituloFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText13LeftTop3WidthaHeightSize.Values������*�@       �@      ��@������R�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldDt_RegistroFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText14LeftTopWidthaHeightSize.Values������*�@       �@UUUUUU�@������R�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataField	ProtocoloFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText17Left$Top3WidthBHeightSize.Values������*�@      �
@      ��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataSetRX	DataFieldDt_VencimentoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabellbNatureza2Left$TopWidthKHeightSize.Values������*�@      �
@UUUUUU�@      p�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption
lbNaturezaColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel28LeftTopWidth*HeightSize.Values������*�@       �@UUUUUUU�@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	ProtocoloColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel27LeftTop$Width>HeightSize.Values������*�@       �@      ��@������
�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionData ProtestoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel26LeftTopDWidth/HeightSize.Values������*�@       �@�������@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption
Valor - R$ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel24LeftTopdWidth`HeightSize.Values������*�@       �@������J�@       �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionData do CancelamentoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShapeQRShape2LeftTop"WidthfHeightSize.ValuesUUUUUUU� @       �@�������@      ��@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShapeQRShape3LeftTopBWidthfHeightSize.ValuesUUUUUUU� @       �@      ��@      ��@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShapeQRShape4LeftTopbWidthfHeightSize.ValuesUUUUUUU� @       �@UUUUUU��@      ��@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShapeQRShape5LeftjTopWidthHeight� Size.ValuesUUUUUUU�@������:�@UUUUUUU� @UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabel	QRLabel22LeftnTopWidthTHeightSize.Values������*�@UUUUUU��@UUUUUUU�@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionSacado ou DevedorColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShapeQRShape6LeftlTop"Width[HeightSize.ValuesUUUUUUU� @      ��@�������@UUUUUU�
@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShapeQRShape7Left*Top WidthHeight"Size.Values�������@UUUUUU9�	@          UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShapeQRShape8LeftlTopBWidth[HeightSize.ValuesUUUUUUU� @      ��@      ��@UUUUUU�
@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShapeQRShape9LeftlTopbWidth[HeightSize.ValuesUUUUUUU� @      ��@UUUUUU��@UUUUUU�
@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShape	QRShape10LeftZTop WidthHeight"Size.Values�������@UUUUUU�	@          UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShape	QRShape11Left�Top WidthHeight"Size.Values�������@��������	@          UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabel	QRLabel38Left/TopWidthHeightSize.Values������*�@�������	@UUUUUUU�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionF/JColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel39Left_TopWidth2HeightSize.Values������*�@��������	@UUUUUUU�@������J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionTipo Docto.ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShape	QRShape12Left Top WidthHeight"Size.Values�������@������J�
@          UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShape	QRShape13LeftsTop WidthHeight"Size.Values�������@UUUUUU�
@          UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabel	QRLabel23Left�TopWidthRHeightSize.Values������*�@UUUUUU��	@UUUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption   Nº do DocumentoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel29Left$TopWidth!HeightSize.Values������*�@      �
@UUUUUUU�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionTDDPColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel30LeftxTopWidth(HeightSize.Values������*�@      ؒ
@UUUUUUU�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption   EspécieColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	lbEspecieLeftxTopWidth(HeightSize.Values������*�@      ؒ
@UUUUUU�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption   EspécieColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	lbTipoDocLeft_TopWidth2HeightSize.Values������*�@��������	@UUUUUU�@������J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	lbTipoDocColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText15Left/TopWidth)HeightSize.Values������*�@�������	@UUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldTipo_DevedorFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabel	QRLabel21LeftnTop$Width+HeightSize.Values������*�@UUUUUU��@      ��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	   EndereçoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShape	QRShape14Left�Top#WidthHeight Size.ValuesUUUUUUU�@�������	@UUUUUU5�@UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShape	QRShape15Left�Top#WidthHeight Size.ValuesUUUUUUU�@��������	@UUUUUU5�@UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShape	QRShape16Left�Top#WidthHeight Size.ValuesUUUUUUU�@������z�	@UUUUUU5�@UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShape	QRShape17Left Top#WidthHeight Size.ValuesUUUUUUU�@������J�
@UUUUUU5�@UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShape	QRShape18LeftsTop#WidthHeight Size.ValuesUUUUUUU�@UUUUUU�
@UUUUUU5�@UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabel	QRLabel25Left�Top$Width!HeightSize.Values������*�@UUUUUUa�	@      ��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionCidadeColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel40Left�Top$WidthHeightSize.Values������*�@UUUUUU��	@      ��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionUFColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel41Left�Top$WidthHeightSize.Values������*�@������v�	@      ��@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionCEPColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel42Left$Top$WidthKHeightSize.Values������*�@      �
@      ��@      p�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionData VencimentoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel43LeftxTop$Width?HeightSize.Values������*�@      ؒ
@      ��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption   Data EmissãoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText16LeftxTop3Width&HeightSize.Values������*�@      ؒ
@      ��@UUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataSetRX	DataField	Dt_TituloFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabel	QRLabel44LeftnTopDWidthdHeightSize.Values������*�@UUUUUU��@�������@������J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionCedente ou FavorecidoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel45LeftnTopdWidth+HeightSize.Values������*�@UUUUUU��@������J�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	   EndereçoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShape	QRShape19Left�TopcWidthHeight Size.ValuesUUUUUUU�@�������	@      ��@UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShape	QRShape20Left�TopcWidthHeight Size.ValuesUUUUUUU�@��������	@      ��@UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShape	QRShape21Left�TopcWidthHeight Size.ValuesUUUUUUU�@������z�	@      ��@UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabel	QRLabel46Left�TopdWidth!HeightSize.Values������*�@UUUUUUa�	@������J�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionCidadeColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel47Left�TopdWidthHeightSize.Values������*�@UUUUUU��	@������J�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionUFColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel48Left�TopdWidthHeightSize.Values������*�@������v�	@������J�@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionCEPColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShape	QRShape22Left*TopBWidthHeight!Size.Values      ��@UUUUUU9�	@      ��@UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShape	QRShape23LeftZTopBWidthHeight!Size.Values      ��@UUUUUU�	@      ��@UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShape	QRShape24Left�TopBWidthHeight!Size.Values      ��@��������	@      ��@UUUUUUU� @ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabel	QRLabel49Left/TopDWidthHeightSize.Values������*�@�������	@�������@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionF/JColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel50LeftaTopDWidth4HeightSize.Values������*�@      j�	@�������@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionTipo Docto.ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel51Left�TopDWidthRHeightSize.Values������*�@UUUUUU��	@�������@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption   Nº do DocumentoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText18LeftnTop3WidthxHeightSize.Values������*�@UUUUUU��@      ��@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldEndereco_DevedorFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText19Left�Top3Width� HeightSize.Values������*�@UUUUUUa�	@      ��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldCidade_DevedorFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText20Left�Top3Width"HeightSize.Values������*�@UUUUUU��	@      ��@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataField
UF_DevedorFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText21Left�Top3Width=HeightSize.Values������*�@������v�	@      ��@UUUUUUe�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataSetRX	DataFieldCEP_DevedorFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText22LeftnTopSWidth�HeightSize.Values������*�@UUUUUU��@��������@      ڑ	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldApresentanteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText23Left/TopSWidth)HeightSize.Values������*�@�������	@��������@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldTipo_ApresentanteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabel
lbTipoDoc2LeftaTopSWidth2HeightSize.Values������*�@      j�	@��������@������J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption
lbTipoDoc2ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText24Left�TopSWidthoHeightSize.Values������*�@UUUUUU��	@��������@      ؒ@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataSetRX	DataFieldCPF_CNPJ_ApresentanteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMicrosoft Sans Serif
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize   TQRChildBand
ChildBand2LeftqTop3Width�Height
AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values��������@      ��
@ PreCaluculateBandHeightKeepOnOnePage
ParentBandQRBand2
PrintOrdercboAfterParent   TDataSourcedsRXDataSetRXLeftXTop  
TPopupMenuPMLeft Top�  	TMenuItemMarcarTodosCaptionMarcar todosOnClickMarcarTodosClick  	TMenuItemDesmarcarTodosCaptionDesmarcar todosOnClickDesmarcarTodosClick  	TMenuItemN1Caption-  	TMenuItemMarcarQuantidadeCaptionMarcar quantidade abaixoOnClickMarcarQuantidadeClick   TSaveDialogSvdOnShowSvdShowFilter	txt|*.txtLeft Top�   
TPopupMenuPM2LeftXTop�  	TMenuItemSerasa1CaptionSerasaOnClickSerasa1Click  	TMenuItem
PopEquifaxCaption	Boa VistaOnClickPopEquifaxClick   TRxMemoryDataRX	FieldDefsNameCheckDataType	ftBoolean NameID_ATODataType	ftInteger Name	ProtocoloDataType	ftInteger NameDevedorDataTypeftStringSized NameTipo_DevedorDataTypeftStringSize NameCPF_CNPJ_DevedorDataTypeftStringSize NameTipo_TituloDataType	ftInteger NameValor_TituloDataTypeftFloat NameDt_RegistroDataTypeftDate NameStatusDataTypeftStringSize NameAceiteDataTypeftStringSize NameDt_VencimentoDataTypeftDate Name	Dt_TituloDataTypeftDate NameMotivoDataTypeftStringSize NameDt_PagamentoDataTypeftDate NameFins_FalimentaresDataTypeftStringSize NameLivro_ProtocoloDataType	ftInteger NameFolha_ProtocoloDataTypeftStringSize
 NameDataDataTypeftDate NameSituacaoDataTypeftStringSize  OnFilterRecordRXFilterRecordLeftXTop�  TBooleanFieldRXCheck	FieldNameCheck  TIntegerFieldRXID_ATO	FieldNameID_ATO  TIntegerFieldRXProtocolo	AlignmenttaCenter	FieldName	Protocolo  TStringField	RXDevedor	FieldNameDevedorSized  TStringFieldRXTipo_Devedor	FieldNameTipo_DevedorSize  TStringFieldRXCPF_CNPJ_Devedor	FieldNameCPF_CNPJ_DevedorSize  TIntegerFieldRXTipo_Titulo	FieldNameTipo_Titulo  TFloatFieldRXValor_Titulo	FieldNameValor_TituloDisplayFormat
###,##0.00
EditFormat
###,##0.00  TFloatFieldRXSaldo_Titulo	FieldNameSaldo_TituloDisplayFormat
###,##0.00
EditFormat
###,##0.00  
TDateFieldRXDt_Registro	FieldNameDt_Registro  TStringFieldRXStatus	AlignmenttaCenter	FieldNameStatus  TStringFieldRXAceite	AlignmenttaCenter	FieldNameAceiteSize  
TDateFieldRXDt_Vencimento	FieldNameDt_Vencimento  
TDateFieldRXDt_Titulo	FieldName	Dt_Titulo  TStringFieldRXMotivo	AlignmenttaCenter	FieldNameMotivoSize  
TDateFieldRXDt_Pagamento	FieldNameDt_Pagamento  TStringFieldRXFins_Falimentares	FieldNameFins_FalimentaresSize  TIntegerFieldRXLivro_Protocolo	FieldNameLivro_Protocolo  TStringFieldRXFolha_Protocolo	FieldNameFolha_ProtocoloSize
  
TDateFieldRXData	AlignmenttaCenter	FieldNameDataDisplayFormat
DD/MM/YYYY  TStringField
RXSituacao	AlignmenttaCenter	FieldNameSituacao  TIntegerFieldRXTipo_Protesto	FieldNameTipo_Protesto  TIntegerFieldRXLivro_Registro	FieldNameLivro_Registro  TStringFieldRXFolha_Registro	FieldNameFolha_RegistroSize
  TStringFieldRXEndereco_Devedor	FieldNameEndereco_DevedorSized  TStringFieldRXCidade_Devedor	FieldNameCidade_DevedorSized  TStringFieldRXUF_Devedor	FieldName
UF_DevedorSize  TStringFieldRXCEP_Devedor	FieldNameCEP_DevedorEditMask99.999-999;0;_Size  TStringFieldRXApresentante	FieldNameApresentanteSized  TStringFieldRXCPF_CNPJ_Apresentante	FieldNameCPF_CNPJ_ApresentanteSize  TStringFieldRXTipo_Apresentante	FieldNameTipo_ApresentanteSize   
TPopupMenuPM3Left� Top�  	TMenuItemM1CaptionModelo 1OnClickM1Click  	TMenuItemM2CaptionModelo 2OnClickM2Click   TsSkinProvidersSkinProviderAddedTitle.Font.CharsetDEFAULT_CHARSETAddedTitle.Font.ColorclNoneAddedTitle.Font.Height�AddedTitle.Font.NameMS Sans SerifAddedTitle.Font.Style SkinData.SkinSectionFORMTitleButtons Left� Top�   TFDQueryqryConsulta
Connectiondm.conSISTEMASQL.Stringsselect     ID_ATO,
PROTOCOLO,DT_PROTOCOLO,DEVEDOR,TIPO_DEVEDOR,CPF_CNPJ_DEVEDOR,TIPO_APRESENTANTE,CPF_CNPJ_APRESENTANTE,TIPO_TITULO,VALOR_TITULO,SALDO_TITULO,DT_REGISTRO,DT_PAGAMENTO,STATUS,TIPO_PROTESTO,ACEITE,DT_VENCIMENTO,
DT_TITULO,FINS_FALIMENTARES,LIVRO_PROTOCOLO,FOLHA_PROTOCOLO,LIVRO_REGISTRO,FOLHA_REGISTRO,CEDENTE,APRESENTANTE    from TITULOS where    PROTOCOLO BETWEEN :P1 AND :P2 Left Top	ParamDataNameP1DataType	ftInteger	ParamTypeptInput NameP2DataType	ftInteger	ParamTypeptInput   TBooleanFieldqryConsultaCheck	FieldKindfkInternalCalc	FieldNameCheck  TIntegerFieldqryConsultaID_ATO	FieldNameID_ATOOriginID_ATOProviderFlags
pfInUpdate	pfInWherepfInKey Required	  TIntegerFieldqryConsultaPROTOCOLO	FieldName	PROTOCOLOOrigin	PROTOCOLO  
TDateFieldqryConsultaDT_PROTOCOLO	FieldNameDT_PROTOCOLOOriginDT_PROTOCOLO  TStringFieldqryConsultaDEVEDOR	FieldNameDEVEDOROriginDEVEDORSized  TStringFieldqryConsultaTIPO_DEVEDOR	FieldNameTIPO_DEVEDOROriginTIPO_DEVEDOR	FixedChar	Size  TStringFieldqryConsultaCPF_CNPJ_DEVEDOR	FieldNameCPF_CNPJ_DEVEDOROriginCPF_CNPJ_DEVEDORSize  TStringFieldqryConsultaTIPO_APRESENTANTE	FieldNameTIPO_APRESENTANTEOriginTIPO_APRESENTANTE	FixedChar	Size  TStringField qryConsultaCPF_CNPJ_APRESENTANTE	FieldNameCPF_CNPJ_APRESENTANTEOriginCPF_CNPJ_APRESENTANTESize  TIntegerFieldqryConsultaTIPO_TITULO	FieldNameTIPO_TITULOOriginTIPO_TITULO  TFloatFieldqryConsultaVALOR_TITULO	FieldNameVALOR_TITULOOriginVALOR_TITULO  TFloatFieldqryConsultaSALDO_TITULO	FieldNameSALDO_TITULOOriginSALDO_TITULO  
TDateFieldqryConsultaDT_REGISTRO	FieldNameDT_REGISTROOriginDT_REGISTRO  
TDateFieldqryConsultaDT_PAGAMENTO	FieldNameDT_PAGAMENTOOriginDT_PAGAMENTO  TStringFieldqryConsultaSTATUS	FieldNameSTATUSOriginSTATUS  TIntegerFieldqryConsultaTIPO_PROTESTO	FieldNameTIPO_PROTESTOOriginTIPO_PROTESTO  TStringFieldqryConsultaACEITE	FieldNameACEITEOriginACEITE	FixedChar	Size  
TDateFieldqryConsultaDT_VENCIMENTO	FieldNameDT_VENCIMENTOOriginDT_VENCIMENTO  
TDateFieldqryConsultaDT_TITULO	FieldName	DT_TITULOOrigin	DT_TITULO  TStringFieldqryConsultaFINS_FALIMENTARES	FieldNameFINS_FALIMENTARESOriginFINS_FALIMENTARES	FixedChar	Size  TIntegerFieldqryConsultaLIVRO_PROTOCOLO	FieldNameLIVRO_PROTOCOLOOriginLIVRO_PROTOCOLO  TStringFieldqryConsultaFOLHA_PROTOCOLO	FieldNameFOLHA_PROTOCOLOOriginFOLHA_PROTOCOLOSize
  TIntegerFieldqryConsultaLIVRO_REGISTRO	FieldNameLIVRO_REGISTROOriginLIVRO_REGISTRO  TStringFieldqryConsultaFOLHA_REGISTRO	FieldNameFOLHA_REGISTROOriginFOLHA_REGISTROSize
  TStringFieldqryConsultaCEDENTE	FieldNameCEDENTEOriginCEDENTESized  TStringFieldqryConsultaAPRESENTANTE	FieldNameAPRESENTANTEOriginAPRESENTANTESized    