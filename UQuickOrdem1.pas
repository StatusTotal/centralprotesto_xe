{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UQuickOrdem1.pas
  Descricao:   Impressao de Ordem de Protesto
  Author:      -
  Date:        -
  Last Update: 28-ago-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UQuickOrdem1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QuickRpt, StdCtrls, ComCtrls, QRCtrls, ExtCtrls, DB, RxMemDS,
  Grids, DBGrids;

type
  TFQuickOrdem1 = class(TForm)
    Ordem: TQuickRep;
    RE: TRichEdit;
    RXOrdem: TRxMemoryData;
    RXOrdemDevedor: TStringField;
    RXOrdemCPF_CNPJ_Devedor: TStringField;
    RXOrdemPraca: TStringField;
    RXOrdemEnderecoDevedor: TStringField;
    RXOrdemSacador: TStringField;
    RXOrdemCedente: TStringField;
    RXOrdemApresentante: TStringField;
    RXOrdemEmissao: TDateField;
    RXOrdemValor: TFloatField;
    RXOrdemAgencia: TStringField;
    RXOrdemEspecie: TStringField;
    RXOrdemNumeroTitulo: TStringField;
    RXOrdemEndosso: TStringField;
    RXOrdemAceite: TStringField;
    RXOrdemNossoNumero: TStringField;
    RXOrdemProtocolo: TIntegerField;
    RXOrdemDataProtocolo: TDateField;
    RXOrdemCustas: TFloatField;
    QRBand2: TQRBand;
    lbSacado1: TQRLabel;
    lbDocumento1: TQRLabel;
    lbEndereco1: TQRLabel;
    lbSacador1: TQRLabel;
    lbCedente1: TQRLabel;
    lbPortador1: TQRLabel;
    lbVencimento1: TQRLabel;
    lbValor1: TQRLabel;
    lbEspecie1: TQRLabel;
    lbEndosso1: TQRLabel;
    lbNosso1: TQRLabel;
    lbProtocolo1: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    txtApresentante1: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText18: TQRDBText;
    lbCustas1: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel38: TQRLabel;
    QRDBText37: TQRDBText;
    RXOrdemTipoDevedor: TStringField;
    QRLabel41: TQRLabel;
    RXOrdemConvenio: TBooleanField;
    lbConvenio1: TQRLabel;
    RXOrdemVencimento: TStringField;
    lbAviso1: TQRLabel;
    lbAviso2: TQRLabel;
    RXOrdemLetra: TStringField;
    lbTitulo: TQRLabel;
    qRE: TQRRichText;
    QRLabel1: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel11: TQRLabel;
    lbDocumento2: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    txtApresentante2: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText31: TQRDBText;
    QRDBText32: TQRDBText;
    QRDBText33: TQRDBText;
    QRDBText34: TQRDBText;
    QRDBText35: TQRDBText;
    QRLabel33: TQRLabel;
    QRDBText36: TQRDBText;
    lbCustas2: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    lbTitulo2: TQRLabel;
    QRRichText1: TQRRichText;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel15: TQRLabel;
    QRDBText38: TQRDBText;
    QRLabel42: TQRLabel;
    lbConvenio2: TQRLabel;
    lbAviso4: TQRLabel;
    lbAviso3: TQRLabel;
    RXOrdemCartorio: TStringField;
    QRDBText39: TQRDBText;
    QRDBText40: TQRDBText;
    QRDBText41: TQRDBText;
    lblSaldo1: TQRLabel;
    lblSaldo2: TQRLabel;
    QRDBText42: TQRDBText;
    RXOrdemSaldo: TFloatField;
    RXOrdemCodApresentante: TStringField;
    RXOrdemNomeApresentante: TStringField;
    RXOrdemPOSTECIPADO: TStringField;
    QRLabel7: TQRLabel;
    QRDBText6: TQRDBText;
    procedure FormCreate(Sender: TObject);
    procedure OrdemEndPage(Sender: TCustomQuickRep);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand2AfterPrint(Sender: TQRCustomBand; BandPrinted: Boolean);
    procedure ChildBand1AfterPrint(Sender: TQRCustomBand; BandPrinted: Boolean);
  private
    { Private declarations }

    iTotImpressos: Integer;
  public
    { Public declarations }

    lSegundaVia,
    lTrocarPortadorCedente: Boolean;

    iTotOrdems: Integer;
  end;

var
  FQuickOrdem1: TFQuickOrdem1;

implementation

uses UPF, UDM;

{$R *.dfm}

procedure TFQuickOrdem1.ChildBand1AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  Inc(iTotImpressos);
end;

procedure TFQuickOrdem1.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if not lSegundaVia then
  begin
    if iTotOrdems > iTotImpressos then
      RXOrdem.Next
    else
      ChildBand1.Enabled := False;
  end;
end;

procedure TFQuickOrdem1.FormCreate(Sender: TObject);
var
  vDistribuicao: String;
begin
  lbTitulo.Caption:='Ilmo Sr. Oficial do '+dm.ServentiaDESCRICAO.AsString;
  lbTitulo2.Caption:=lbTitulo.Caption;

  if dm.ServentiaCODIGO.AsInteger=1622 then
    vDistribuicao:=' o '
      else vDistribuicao:=' a distribui��o e ';

  RE.Lines.Add('Na qualidade de portador, solicitamos'+vDistribuicao+'protesto por falta de pagamento, ou quando '+
               'for o caso, por falta de aceite, do t�tulo abaixo caracterizado, de acordo com '+
               'a Lei 9.492 de 10 de setembro de 1997.');
  PF.JustificarRichEdit(RE,True);

  if dm.ServentiaCODIGO.AsInteger=2323 then
      QRBand2.HasChild := False
      else QRBand2.HasChild := True;

end;

procedure TFQuickOrdem1.OrdemEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickOrdem1.QRBand2AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  Inc(iTotImpressos);
end;

procedure TFQuickOrdem1.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if RXOrdemTipoDevedor.AsString='J' then
    lbDocumento1.Caption:='CNPJ..........:'
  else
    lbDocumento1.Caption:='CPF...........:';

  lbConvenio1.Enabled:=RXOrdemConvenio.AsBoolean;

  if (RXOrdemLetra.AsString='G') or (RXOrdemLetra.AsString='D') or (RXOrdemLetra.AsString='T') then
  begin
    lbAviso1.Enabled:=True;
    lbAviso2.Enabled:=True;

    if RXOrdemLetra.AsString='G' then
    begin
      lbAviso1.Caption:='"O Apresentante declara que a d�vida foi regularmente inscrita';
      lbAviso2.Caption:='e que o "TERMO DE INSCRI��O" cont�m todos os requisitos legais"';
    end
    else
    begin
      lbAviso1.Caption:='"Foi feito declara��o substitutiva, nos termos do Art. 978�13,14 e 15 da Consolida��o';
      lbAviso2.Caption:='Normativa da Corregedoria Geral de Justi�a do Estado do Rio de Janeiro."';
    end;
  end
  else
  begin
    lbAviso1.Enabled:=False;
    lbAviso2.Enabled:=False;
  end;

  if QRBand2.HasChild = True then
  begin
    lbDocumento2.Caption:=lbDocumento1.Caption;
    lbConvenio2.Enabled:=RXOrdemConvenio.AsBoolean;
    lbAviso3.Caption:=lbAviso1.Caption;
    lbAviso4.Caption:=lbAviso2.Caption;

    if (RXOrdemLetra.AsString='G') or (RXOrdemLetra.AsString='D') or (RXOrdemLetra.AsString='T') then
    begin
      lbAviso3.Enabled:=True;
      lbAviso4.Enabled:=True;
    end
    else
    begin
      lbAviso3.Enabled:=False;
      lbAviso4.Enabled:=False;
    end;
  end;
  if dm.ValorParametro(102)='S' then
  begin
    lbCustas1.Caption:='Custas: '+FloatToStrF(RXOrdemCustas.AsFloat,ffnumber,18,2);
    lbCustas2.Caption:=lbCustas1.Caption;
  end
  else
  begin
    lbCustas1.Caption:='Custas: '+FloatToStrF(0,ffnumber,18,2);
    lbCustas2.Caption:=lbCustas1.Caption;
  end;

end;

end.
