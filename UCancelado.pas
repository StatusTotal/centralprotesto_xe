unit UCancelado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Buttons, sBitBtn, sCurrEdit, sCurrencyEdit,
  DBCtrls, sDBLookupComboBox, sEdit, Mask, sMaskEdit, sCustomComboEdit,
  sTooledit, ExtCtrls, sPanel, sCheckBox, sComboBox, sLabel, acPNG;

type
  TFCancelado = class(TForm)
    P1: TsPanel;
    edData: TsDateEdit;
    edSelo: TsEdit;
    lkEscreventes: TsDBLookupComboBox;
    btConfirmar: TsBitBtn;
    btCancelar: TsBitBtn;
    dsEscreventes: TDataSource;
    edRecibo: TsEdit;
    lbTotal: TsLabel;
    dsConsulta: TDataSource;
    ckImprimir: TsCheckBox;
    cbCobranca: TsComboBox;
    edAleatorio: TsEdit;
    procedure btConfirmarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lbTotalClick(Sender: TObject);
    procedure Custas(Codigo: Integer);
    procedure cbCobrancaChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    Saldo: Double;
    vPeriodo: Integer;
  end;

var
  FCancelado: TFCancelado;

implementation

uses UDM, UPF, UCustas, UGeral, UGDM;

{$R *.dfm}

procedure TFCancelado.Custas(Codigo: Integer);
var
  Soma: Double;
begin
  dm.RxCustas.Close;
  dm.RxCustas.Open;

  Soma:=0;

  dm.Codigos.Close;
  dm.Codigos.Params.ParamByName('OCULTO1').AsString:='N';
  dm.Codigos.Params.ParamByName('OCULTO2').AsString:='N';
  dm.Codigos.Open;
  dm.Codigos.Locate('COD',Codigo,[]);

  dm.Itens.Close;
  dm.Itens.Params[0].AsInteger:=Codigo;
  dm.Itens.Open;
  dm.Itens.First;
  while not dm.Itens.Eof do
  begin
      dm.RxCustas.Append;
      dm.RxCustasTABELA.AsString    :=dm.ItensTAB.AsString;
      dm.RxCustasITEM.AsString      :=dm.ItensITEM.AsString;
      dm.RxCustasSUBITEM.AsString   :=dm.ItensSUB.AsString;
      dm.RxCustasVALOR.AsFloat      :=dm.ItensVALOR.AsFloat;
      dm.RxCustasQTD.AsString       :=dm.ItensQTD.AsString;
      dm.RxCustasTOTAL.AsFloat      :=dm.ItensTOTAL.AsFloat;
      dm.RxCustasDESCRICAO.AsString :=dm.ItensDESCR.AsString;
      dm.RxCustas.Post;
      dm.Itens.Next;
  end;
  dm.Tabela.Close;
  dm.Tabela.Open;

  if vPeriodo>5 then
    dm.Tabela.Locate('TAB;ITEM;SUB',VarArrayOf(['1','4','B']),[loCaseInsensitive, loPartialKey])
      else dm.Tabela.Locate('TAB;ITEM;SUB',VarArrayOf(['1','4','A']),[loCaseInsensitive, loPartialKey]);

  dm.RxCustas.Append;
  dm.RxCustasTABELA.AsString    :=dm.TabelaTAB.AsString;
  dm.RxCustasITEM.AsString      :=dm.TabelaITEM.AsString;
  dm.RxCustasSUBITEM.AsString   :=dm.TabelaSUB.AsString;
  dm.RxCustasVALOR.AsFloat      :=dm.TabelaVALOR.AsFloat;
  dm.RxCustasQTD.AsInteger      :=vPeriodo Div 5;
  dm.RxCustasTOTAL.AsFloat      :=dm.TabelaVALOR.AsFloat;
  dm.RxCustasDESCRICAO.AsString :=dm.TabelaDESCR.AsString;
  dm.RxCustas.Post;
  dm.RxCustas.First;
  while not dm.RxCustas.Eof do
  begin
      Soma:=Soma+dm.RxCustasTOTAL.AsFloat;
      dm.RxCustas.Next;
  end;

  if cbCobranca.ItemIndex=3 then
    dm.vEmolumentos:=0
      else dm.vEmolumentos:=Soma;

  dm.vFetj          :=GR.NoRound(Soma*0.2,2);
  dm.vFundperj      :=GR.NoRound(Soma*0.05,2);
  dm.vFunperj       :=GR.NoRound(Soma*0.05,2);
  dm.vFunarpen      :=GR.NoRound(Soma*0.04,2);
  dm.vMutua         :=dm.CodigosMUTUA.AsFloat;
  dm.vAcoterj       :=dm.CodigosACOTERJ.AsFloat;
  dm.vDistribuicao  :=dm.CodigosDISTRIB.AsFloat;
  dm.vTotal         :=dm.vEmolumentos+dm.vFetj+dm.vFundperj+dm.vFunperj+dm.vFunarpen+dm.vMutua+dm.vAcoterj+dm.vDistribuicao;

  lbTotal.Caption:='Total: R$ '+FloatToStrF(dm.vTotal,ffNumber,7,2);
  dm.RxCustas.First;
end;

procedure TFCancelado.btConfirmarClick(Sender: TObject);
begin
  if edData.Date=0 then
  begin
      GR.Aviso('Informe a Data do Cancelamento.');
      edData.SetFocus;
      Exit;
  end;

  if edSelo.Text='' then
  begin
      GR.Aviso('Informe o Selo do Registro.');
      edSelo.SetFocus;
      Exit;
  end;

  if dm.ServentiaCODIGO.AsInteger<>1554 then
    if edRecibo.Text='' then
    begin
        GR.Aviso('Informe o N�mero do Recibo.');
        edRecibo.SetFocus;
        Exit;
    end;

  case cbCobranca.ItemIndex of
    0: dm.vCobranca:='JG';
    1: dm.vCobranca:='CC';
    2: dm.vCobranca:='SC';
    3: dm.vCobranca:='NH';
    4: dm.vCobranca:='FL';
  end;

  dm.vOkGeral:=True;

  Close;
end;

procedure TFCancelado.btCancelarClick(Sender: TObject);
begin
  dm.vOkGeral:=False;
  Close;
end;

procedure TFCancelado.FormCreate(Sender: TObject);
begin
  {dm.Escreventes.Close;
  dm.Escreventes.Open;
  lkEscreventes.KeyValue:=dm.vCPF;

  dm.vIdReservado:=GR.ValorGeneratorDBX('ID_RESERVADO',Gdm.BDGerencial);

  edSelo.Text:=Gdm.ProximoSelo(dm.SerieAtual,'PROTESTO',Self.Name,'N',dm.TitulosLIVRO_REGISTRO.AsString,
                               GR.PegarNumero(dm.TitulosFOLHA_REGISTRO.AsString),
                               GR.PegarNumero(dm.TitulosFOLHA_REGISTRO.AsString),
                               0,
                               'CANCELAMENTO',
                               dm.vNome,
                               Now,
                               dm.vIdReservado);
  edAleatorio.Text:=Gdm.vAleatorio;

  if dm.CertidoesCOBRANCA.AsString='JG' then cbCobranca.ItemIndex:=0;
  if dm.CertidoesCOBRANCA.AsString='CC' then cbCobranca.ItemIndex:=1;
  if dm.CertidoesCOBRANCA.AsString='SC' then cbCobranca.ItemIndex:=2;
  if dm.CertidoesCOBRANCA.AsString='NH' then cbCobranca.ItemIndex:=3;
  if dm.CertidoesCOBRANCA.AsString='FL' then cbCobranca.ItemIndex:=4;   }
end;

procedure TFCancelado.lbTotalClick(Sender: TObject);
begin
  dm.Tabela.Close;
  dm.Tabela.Open;
  GR.CriarForm(TFCustas,FCustas);
  dm.Tabela.Close;
  lbTotal.Caption:='Total: R$ '+FloatToStrF(dm.vTotal,ffNumber,7,2);
end;

procedure TFCancelado.cbCobrancaChange(Sender: TObject);
begin
  if (cbCobranca.ItemIndex=0) or (cbCobranca.ItemIndex=2) or (cbCobranca.ItemIndex=4) then
  begin
      dm.RxCustas.Close;
      dm.RxCustas.Open;
      dm.vEmolumentos   :=0;
      dm.vFetj          :=0;
      dm.vFundperj      :=0;
      dm.vFunperj       :=0;
      dm.vMutua         :=0;
      dm.vAcoterj       :=0;
      dm.vDistribuicao  :=0;
      dm.vAAR           :=0;
      dm.vTotal         :=0;
      lbTotal.Caption   :='Total: R$ 0,00';
  end
  else
  begin
      Custas(4010);
  end;
end;

procedure TFCancelado.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if not dm.vOkGeral then
  Gdm.LiberarSelo('PROTESTO',edSelo.Text,False,-1,dm.vIdReservado);
end;

end.
