object FImportarTxt: TFImportarTxt
  Left = 282
  Top = 149
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Gera'#231#227'o de Arquivos'
  ClientHeight = 546
  ClientWidth = 1004
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  Icon.Data = {
    0000010001002020020001000100300100001600000028000000200000004000
    0000010001000000000088000000000000000000000000000000000000000000
    0000FFFFFF000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
    FFFFFFFFFFFF}
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 15
  object GridAtos: TwwDBGrid
    Left = 0
    Top = 89
    Width = 1004
    Height = 186
    ControlType.Strings = (
      'Check;CheckBox;True;False')
    Selected.Strings = (
      'Check'#9'2'#9'*'#9'F'
      'NUMERO_TITULO'#9'12'#9'N'#186' T'#237'tulo'#9'T'
      'CODIGO_APRESENTANTE'#9'8'#9'C'#243'digo'#9'F'
      'APRESENTANTE'#9'26'#9'Apresentante'#9'F'
      'CEDENTE'#9'27'#9'Cedente'#9'T'
      'STATUS'#9'8'#9'Situa'#231#227'o'#9'T'
      'MotivoRejeicao'#9'28'#9'Motivo (Rejei'#231#227'o)'#9'T'
      'FLG_COOBRIGADO'#9'8'#9'Coob.'#9'T'
      'Protocolo'#9'9'#9'Protocolo'#9'F'
      'SERVENTIA_AGREG'#9'27'#9'Serventia Agregada'#9'T')
    IniAttributes.Delimiter = ';;'
    IniAttributes.UnicodeIniFile = False
    TitleColor = clBtnFace
    FixedCols = 1
    ShowHorzScrollBar = True
    EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
    Align = alClient
    BorderStyle = bsNone
    DataSource = dsRX
    EditCalculated = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
    ParentFont = False
    PopupMenu = PM
    ReadOnly = True
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = False
    UseTFields = False
    OnExit = GridAtosExit
    OnMouseMove = GridAtosMouseMove
    OnMouseUp = GridAtosMouseUp
  end
  object P2: TsPanel
    Left = 0
    Top = 302
    Width = 1004
    Height = 164
    Align = alBottom
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object edCep: TsDBEdit
      Left = 880
      Top = 135
      Width = 116
      Height = 22
      Color = clWhite
      DataField = 'CEP_DEVEDOR'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 15
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Pra'#231'a'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edSaldo: TsDBEdit
      Left = 210
      Top = 19
      Width = 90
      Height = 22
      Color = clWhite
      DataField = 'SALDO_TITULO'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Saldo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edDevedor: TsDBEdit
      Left = 8
      Top = 58
      Width = 398
      Height = 22
      Color = clWhite
      DataField = 'DEVEDOR'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Devedor'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edDataTitulo: TsDBDateEdit
      Left = 8
      Top = 19
      Width = 100
      Height = 22
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 0
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data do T'#237'tulo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Blend = 0
      DataField = 'DT_TITULO'
      DataSource = dsRX
    end
    object edValor: TsDBEdit
      Left = 114
      Top = 19
      Width = 90
      Height = 22
      Color = clWhite
      DataField = 'VALOR_TITULO'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Valor'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edEspecie: TsDBEdit
      Left = 423
      Top = 19
      Width = 573
      Height = 22
      Color = clWhite
      DataField = 'Especie'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Esp'#233'cie'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edDataVencimento: TsDBDateEdit
      Left = 306
      Top = 19
      Width = 100
      Height = 22
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 3
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data do Venc.'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Blend = 0
      DataField = 'DT_VENCIMENTO'
      DataSource = dsRX
    end
    object edDocumento: TsDBEdit
      Left = 640
      Top = 99
      Width = 356
      Height = 22
      Color = clWhite
      DataField = 'CPF_CNPJ_DEVEDOR'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 12
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Documento'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edEndereco: TsDBEdit
      Left = 423
      Top = 58
      Width = 211
      Height = 22
      Color = clWhite
      DataField = 'END_DEVEDOR'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Endere'#231'o (Devedor)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edBairro: TsDBEdit
      Left = 640
      Top = 57
      Width = 127
      Height = 22
      Color = clWhite
      DataField = 'BAIRRO_DEVEDOR'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Bairro (Devedor)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edUF: TsDBEdit
      Left = 773
      Top = 57
      Width = 43
      Height = 22
      Color = clWhite
      DataField = 'UF'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'UF (Dev.)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edCidade: TsDBEdit
      Left = 822
      Top = 57
      Width = 174
      Height = 22
      Color = clWhite
      DataField = 'CID_DEVEDOR'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Cidade (Devedor)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edPraca: TsDBEdit
      Left = 423
      Top = 135
      Width = 451
      Height = 22
      Color = clWhite
      DataField = 'PRACA'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 14
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Pra'#231'a'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edtNossoNumero: TsDBEdit
      Left = 423
      Top = 99
      Width = 211
      Height = 22
      Color = clWhite
      DataField = 'NOSSO_NUMERO'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Nosso N'#250'mero'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edCedente: TsDBEdit
      Left = 8
      Top = 97
      Width = 398
      Height = 22
      Color = clWhite
      DataField = 'CEDENTE'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Cedente'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edSacador: TsDBEdit
      Left = 8
      Top = 135
      Width = 398
      Height = 22
      Color = clWhite
      DataField = 'SACADOR'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 13
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Sacador'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object PB: TsProgressBar
      Left = 15
      Top = 73
      Width = 1002
      Height = 162
      Align = alCustom
      TabOrder = 16
      Visible = False
      SkinData.SkinSection = 'MAINMENU'
    end
    object sDBCheckBox1: TsDBCheckBox
      Left = 912
      Top = 1
      Width = 84
      Height = 18
      Caption = 'Postecipado'
      Alignment = taLeftJustify
      TabOrder = 17
      Visible = False
      ImgChecked = 0
      ImgUnchecked = 0
      DataField = 'POSTECIPADO'
      DataSource = dsRX
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
  end
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 1004
    Height = 89
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    SkinData.SkinSection = 'SCROLLSLIDERH'
    object sImage1: TsImage
      Left = 968
      Top = 20
      Width = 25
      Height = 25
      Hint = 
        'A PGFN est'#225' enviando algumas CDAs com mais de um devedor e todos' +
        ' '#13#10'com endere'#231'os distintos. Pe'#231'o a gentileza de que n'#227'o devolvam' +
        ' os t'#237'tulos '#13#10'cujo endere'#231'o de um devedor seja fora da sua comar' +
        'ca. O tratamento deve '#13#10'ser o mesmo de qualquer t'#237'tulo com mais ' +
        'de um devedor. O que for da sua '#13#10'comarca deve ser intimado pess' +
        'oalmente e o outro intimado por edital. Vale '#13#10'lembrar que aquel' +
        'es que n'#227'o possu'#237'rem endere'#231'o na comarca, poder'#227'o ser '#13#10'informad' +
        'os do t'#237'tulo atrav'#233's de comunicado emitido pela Serventia, de ac' +
        'ordo '#13#10'com o art. 991 '#167'5'#186' da Consolida'#231#227'o Normativa da Corregedo' +
        'ria Geral de '#13#10'Justi'#231'a do Rio de Janeiro.'
      ParentShowHint = False
      Picture.Data = {07544269746D617000000000}
      ShowHint = True
      ImageIndex = 22
      Images = Gdm.Im24
      SkinData.SkinSection = 'CHECKBOX'
    end
    object btArquivo: TsBitBtn
      Left = 232
      Top = 7
      Width = 67
      Height = 52
      Cursor = crHandPoint
      Caption = 'Arquivo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      Layout = blGlyphTop
      ParentFont = False
      TabOrder = 2
      OnClick = btArquivoClick
      Reflected = True
      ImageIndex = 4
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object sDBNavigator1: TsDBNavigator
      Left = 1164
      Top = 5
      Width = 94
      Height = 52
      FullRepaint = False
      TabOrder = 6
      SkinData.SkinSection = 'ALPHACOMBOBOX'
      DataSource = dsRX
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
    end
    object btProcessar: TsBitBtn
      Left = 378
      Top = 7
      Width = 79
      Height = 52
      Cursor = crHandPoint
      Hint = 'Apontar / Rejeitar T'#237'tulos'
      Caption = 'Processar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      Layout = blGlyphTop
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = btProcessarClick
      Reflected = True
      ImageIndex = 8
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object btRejeitar: TsBitBtn
      Left = 463
      Top = 7
      Width = 107
      Height = 52
      Cursor = crHandPoint
      Caption = 'Irregularidades'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      Layout = blGlyphTop
      ParentFont = False
      TabOrder = 5
      OnClick = btRejeitarClick
      Reflected = True
      ImageIndex = 9
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object edProtocolo: TsDBEdit
      Left = 131
      Top = 7
      Width = 94
      Height = 24
      Color = clWhite
      DataField = 'VALOR'
      DataSource = dsControle
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnExit = edProtocoloExit
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Protocolo Inicial'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
    end
    object btCriticas: TsBitBtn
      Left = 305
      Top = 7
      Width = 67
      Height = 52
      Cursor = crHandPoint
      Caption = 'Cr'#237'ticas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      Layout = blGlyphTop
      ParentFont = False
      TabOrder = 3
      OnClick = btCriticasClick
      Reflected = True
      ImageIndex = 5
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object edData: TsDateEdit
      Left = 131
      Top = 36
      Width = 94
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data do Apontamento'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Blend = 0
      DefaultToday = True
    end
    object sPanel2: TsPanel
      Left = 0
      Top = 65
      Width = 1004
      Height = 24
      Align = alBottom
      TabOrder = 7
      OnMouseMove = sPanel2MouseMove
      SkinData.SkinSection = 'SELECTION'
      object lbAviso: TLabel
        Left = 4
        Top = 5
        Width = 610
        Height = 14
        Cursor = crHandPoint
        AutoSize = False
        Caption = 'Seu arquivo de confirma'#231#227'o ser'#225' gerado na seguinte pasta:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        OnClick = lbAvisoClick
      end
      object ckVGF: TsCheckBox
        Left = 841
        Top = 3
        Width = 157
        Height = 18
        Cursor = crHandPoint
        Caption = 'Via Gerenciador Financeiro'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        ImgChecked = 0
        ImgUnchecked = 0
        SkinData.SkinSection = 'CHECKBOX'
      end
    end
  end
  object wwDBGrid1: TwwDBGrid
    Left = 1108
    Top = 285
    Width = 981
    Height = 135
    Selected.Strings = (
      'Linha'#9'10000'#9'Linha'#9#9)
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    IniAttributes.UnicodeIniFile = False
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsEntrada
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    KeyOptions = []
    ParentFont = False
    TabOrder = 3
    TitleAlignment = taLeftJustify
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Arial'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = False
    Visible = False
  end
  object PA: TsPanel
    Left = 0
    Top = 466
    Width = 1004
    Height = 80
    Align = alBottom
    TabOrder = 4
    SkinData.SkinSection = 'COMBOBOX'
    object Memo1: TMemo
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 996
      Height = 72
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = 8249599
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      Lines.Strings = (
        'Ol'#225'!'
        ''
        
          'Nessa tela voc'#234' apontar'#225' os t'#237'tulos recebidos atrav'#233's de arquivo' +
          ' eletr'#244'nico no formato ".txt". O seu pr'#243'ximo n'#250'mero de protocolo' +
          ' aparece na parte superior da tela junto a data do apontamento. ' +
          'Para '
        
          'localizar o arquivo a ser importado basta clicar no bot'#227'o "Arqui' +
          'vo", e ap'#243's carregar o arquivo, as informa'#231#245'es dos t'#237'tulos ser'#227'o' +
          ' mostradas no painel acima para uma pr'#233'via visualiza'#231#227'o dos dado' +
          's. O '
        
          'bot'#227'o "Cr'#237'ticas" ir'#225' verificar se possui alguma irregularidade n' +
          'os t'#237'tulos listados. Para aceitar ou rejeitar um t'#237'tulo basta ma' +
          'rcar ou desmarcar o campo * na grade acima. Para efetuar o apont' +
          'amento e(ou) '
        'a rejei'#231#227'o basta clicar em "Processar".'
        ''
        
          'Logo ap'#243's o apontamento ser'#225' gerado o arquivo de confirma'#231#227'o no ' +
          'local indicado acima.'
        ''
        
          'Dica: Para definir o diret'#243'rio dos arquivos basta acessar Menu P' +
          'rincipal > Configura'#231#245'es > Par'#226'metros.')
      ParentFont = False
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object PMotivo: TsPanel
    Left = 0
    Top = 275
    Width = 1004
    Height = 27
    Align = alBottom
    TabOrder = 5
    OnMouseMove = PMotivoMouseMove
    SkinData.SkinSection = 'HINT'
    object lbIrreguralidade: TsLabel
      Left = 6
      Top = 6
      Width = 83
      Height = 15
      Caption = 'Irregularidade:'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object txMotivo: TsDBText
      Left = 95
      Top = 6
      Width = 671
      Height = 15
      Caption = 'txMotivo'
      ParentFont = False
      ShowAccelChar = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      DataField = 'MotivoRejeicao'
      DataSource = dsRX
    end
  end
  object wwDBGrid2: TwwDBGrid
    Left = 1108
    Top = 445
    Width = 981
    Height = 135
    Selected.Strings = (
      'Linha'#9'10'#9'Linha'#9#9)
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    IniAttributes.UnicodeIniFile = False
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsLinhas
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    KeyOptions = []
    ParentFont = False
    TabOrder = 6
    TitleAlignment = taLeftJustify
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Arial'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = False
    Visible = False
  end
  object wwDBGrid3: TwwDBGrid
    Left = 1108
    Top = 602
    Width = 981
    Height = 135
    Selected.Strings = (
      'Codigo'#9'10'#9'Codigo'#9#9
      'Linha'#9'10000'#9'Linha'#9#9)
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    IniAttributes.UnicodeIniFile = False
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsRXEN
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    KeyOptions = []
    ParentFont = False
    TabOrder = 7
    TitleAlignment = taLeftJustify
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Arial'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = False
    Visible = False
  end
  object RE: TRichEdit
    Left = 1112
    Top = 112
    Width = 981
    Height = 97
    BorderStyle = bsNone
    Color = 9699327
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssHorizontal
    TabOrder = 8
    WordWrap = False
    Zoom = 100
  end
  object MM: TMemo
    Left = 1112
    Top = 214
    Width = 981
    Height = 124
    BorderStyle = bsNone
    Color = 8454016
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 9
  end
  object RX1: TRxMemoryData
    FieldDefs = <
      item
        Name = 'CEDENTE'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'SACADOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DEVEDOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'TIPO_DEVEDOR'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CPF_CNPJ_DEVEDOR'
        DataType = ftString
        Size = 14
      end
      item
        Name = 'RG_DEVEDOR'
        DataType = ftString
        Size = 25
      end
      item
        Name = 'END_DEVEDOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CEP_DEVEDOR'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'UF'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'CID_DEVEDOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'BAIRRO_DEVEDOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DT_TITULO'
        DataType = ftDate
      end
      item
        Name = 'DT_VENCIMENTO'
        DataType = ftDate
      end
      item
        Name = 'VALOR_TITULO'
        DataType = ftFloat
      end
      item
        Name = 'APRESENTANTE'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CODIGO_APRESENTANTE'
        DataType = ftInteger
      end
      item
        Name = 'TIPO_TITULO'
        DataType = ftInteger
      end
      item
        Name = 'NUMERO_TITULO'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SALDO_TITULO'
        DataType = ftFloat
      end
      item
        Name = 'PRACA'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ACEITE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ENDOSSO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'STATUS'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NOSSO_NUMERO'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'AGENCIA'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'CUSTAS'
        DataType = ftFloat
      end
      item
        Name = 'CPF_CNPJ_SACADOR'
        DataType = ftString
        Size = 14
      end
      item
        Name = 'END_SACADOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CEP_SACADOR'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'UF_SACADOR'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'BAIRRO_SACADOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CID_SACADOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Check'
        DataType = ftBoolean
      end
      item
        Name = 'Protocolo'
        DataType = ftInteger
      end
      item
        Name = 'Irregularidade'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'MotivoRejeicao'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CONVENIO'
        DataType = ftBoolean
      end
      item
        Name = 'AVISTA'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LETRA'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FINS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NUMDEVEDOR'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SERVENTIA_AGREG'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'SEQUENCIAL_ARQ'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'SEQ_BANCO'
        DataType = ftInteger
      end
      item
        Name = 'FLG_COOBRIGADO'
        DataType = ftString
        Size = 1
      end>
    Left = 32
    Top = 200
    object RX1CEDENTE: TStringField
      DisplayLabel = 'Cedente'
      DisplayWidth = 25
      FieldName = 'CEDENTE'
      Size = 100
    end
    object RX1SACADOR: TStringField
      DisplayLabel = 'Sacador'
      DisplayWidth = 22
      FieldName = 'SACADOR'
      Size = 100
    end
    object RX1DEVEDOR: TStringField
      DisplayLabel = 'Devedor'
      DisplayWidth = 28
      FieldName = 'DEVEDOR'
      Visible = False
      Size = 100
    end
    object RX1TIPO_DEVEDOR: TStringField
      DisplayLabel = 'Tipo'
      DisplayWidth = 4
      FieldName = 'TIPO_DEVEDOR'
      Visible = False
      Size = 1
    end
    object RX1CPF_CNPJ_DEVEDOR: TStringField
      DisplayLabel = 'Documento'
      DisplayWidth = 15
      FieldName = 'CPF_CNPJ_DEVEDOR'
      Visible = False
      Size = 14
    end
    object RX1RG_DEVEDOR: TStringField
      DisplayLabel = 'RG'
      DisplayWidth = 12
      FieldName = 'RG_DEVEDOR'
      Visible = False
      Size = 25
    end
    object RX1END_DEVEDOR: TStringField
      DisplayLabel = 'Endere'#231'o'
      DisplayWidth = 28
      FieldName = 'END_DEVEDOR'
      Visible = False
      Size = 100
    end
    object RX1CEP_DEVEDOR: TStringField
      DisplayLabel = 'CEP'
      DisplayWidth = 10
      FieldName = 'CEP_DEVEDOR'
      Visible = False
      EditMask = '99.999-999;0;_'
      Size = 8
    end
    object RX1UF: TStringField
      DisplayWidth = 3
      FieldName = 'UF'
      Visible = False
      Size = 2
    end
    object RX1CID_DEVEDOR: TStringField
      DisplayLabel = 'Cidade'
      DisplayWidth = 19
      FieldName = 'CID_DEVEDOR'
      Visible = False
      Size = 100
    end
    object RX1BAIRRO_DEVEDOR: TStringField
      DisplayLabel = 'Bairro'
      DisplayWidth = 21
      FieldName = 'BAIRRO_DEVEDOR'
      Visible = False
      Size = 100
    end
    object RX1DT_TITULO: TDateField
      DisplayLabel = 'Data'
      DisplayWidth = 10
      FieldName = 'DT_TITULO'
      Visible = False
    end
    object RX1DT_VENCIMENTO: TDateField
      DisplayLabel = 'Vencimento'
      DisplayWidth = 10
      FieldName = 'DT_VENCIMENTO'
      Visible = False
    end
    object RX1VALOR_TITULO: TFloatField
      DisplayLabel = 'Valor'
      DisplayWidth = 10
      FieldName = 'VALOR_TITULO'
      Visible = False
      DisplayFormat = '###,##0.00'
    end
    object RX1APRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Visible = False
      Size = 100
    end
    object RX1CODIGO_APRESENTANTE: TStringField
      FieldName = 'CODIGO_APRESENTANTE'
      Size = 10
    end
    object RX1TIPO_TITULO: TIntegerField
      FieldName = 'TIPO_TITULO'
      Visible = False
    end
    object RX1NUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
      Visible = False
    end
    object RX1SALDO_TITULO: TFloatField
      FieldName = 'SALDO_TITULO'
      Visible = False
      DisplayFormat = '###,##0.00'
      EditFormat = '#####0.00'
    end
    object RX1PRACA: TStringField
      FieldName = 'PRACA'
      Visible = False
    end
    object RX1ACEITE: TStringField
      FieldName = 'ACEITE'
      Size = 1
    end
    object RX1ENDOSSO: TStringField
      FieldName = 'ENDOSSO'
      Size = 1
    end
    object RX1STATUS: TStringField
      Alignment = taCenter
      FieldName = 'STATUS'
    end
    object RX1NOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Size = 15
    end
    object RX1AGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 15
    end
    object RX1CUSTAS: TFloatField
      FieldName = 'CUSTAS'
    end
    object RX1CPF_CNPJ_SACADOR: TStringField
      FieldName = 'CPF_CNPJ_SACADOR'
      Size = 14
    end
    object RX1END_SACADOR: TStringField
      FieldName = 'END_SACADOR'
      Size = 100
    end
    object RX1CEP_SACADOR: TStringField
      FieldName = 'CEP_SACADOR'
      Size = 8
    end
    object RX1UF_SACADOR: TStringField
      FieldName = 'UF_SACADOR'
      Size = 2
    end
    object RX1BAIRRO_SACADOR: TStringField
      FieldName = 'BAIRRO_SACADOR'
      Size = 100
    end
    object RX1CID_SACADOR: TStringField
      FieldName = 'CID_SACADOR'
      Size = 100
    end
    object RX1ESPECIE: TStringField
      DisplayLabel = 'Esp'#233'cie'
      DisplayWidth = 18
      FieldName = 'ESPECIE'
      Visible = False
      Size = 100
    end
    object RX1Documento: TStringField
      FieldKind = fkCalculated
      FieldName = 'Documento'
      Size = 18
      Calculated = True
    end
    object RX1Check: TBooleanField
      FieldName = 'Check'
    end
    object RX1Protocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object RX1Irregularidade: TStringField
      FieldName = 'Irregularidade'
      Size = 2
    end
    object RX1MotivoRejeicao: TStringField
      FieldName = 'MotivoRejeicao'
      Size = 100
    end
    object RX1CONVENIO: TBooleanField
      FieldName = 'CONVENIO'
    end
    object RX1AVISTA: TStringField
      FieldName = 'AVISTA'
      Size = 1
    end
    object RX1LETRA: TStringField
      FieldName = 'LETRA'
      Size = 1
    end
    object RX1FINS: TStringField
      FieldName = 'FINS'
      Size = 1
    end
    object RX1NUMDEVEDOR: TStringField
      FieldName = 'NUMDEVEDOR'
    end
    object RX1SERVENTIA_AGREG: TStringField
      DisplayWidth = 100
      FieldName = 'SERVENTIA_AGREG'
      Size = 100
    end
    object RX1SEQUENCIAL_ARQ: TStringField
      FieldName = 'SEQUENCIAL_ARQ'
      Size = 4
    end
    object RX1SEQ_BANCO: TIntegerField
      FieldName = 'SEQ_BANCO'
    end
    object RX1FLG_COOBRIGADO: TStringField
      Alignment = taCenter
      FieldName = 'FLG_COOBRIGADO'
      Size = 1
    end
    object RX1DIAS_AVISTA: TIntegerField
      FieldName = 'DIAS_AVISTA'
    end
    object RX1CARTORIO: TIntegerField
      FieldName = 'CARTORIO'
    end
    object RX1POSTECIPADO: TStringField
      FieldName = 'POSTECIPADO'
    end
  end
  object dsRX: TDataSource
    DataSet = RX
    OnDataChange = dsRXDataChange
    Left = 64
    Top = 200
  end
  object Opd: TsOpenDialog
    Left = 96
    Top = 200
  end
  object PM: TPopupMenu
    Left = 128
    Top = 200
    object MarcarTodos: TMenuItem
      Caption = 'Marcar todos'
      OnClick = MarcarTodosClick
    end
    object DesmarcarTodos: TMenuItem
      Caption = 'Desmarcar todos'
      OnClick = DesmarcarTodosClick
    end
  end
  object Entrada: TRxMemoryData
    FieldDefs = <
      item
        Name = 'Linha'
        DataType = ftMemo
      end>
    Left = 56
    Top = 120
    object EntradaLinha: TMemoField
      DisplayWidth = 10000
      FieldName = 'Linha'
      BlobType = ftMemo
    end
  end
  object dsEntrada: TDataSource
    DataSet = Entrada
    Left = 672
    Top = 152
  end
  object Saida: TRxMemoryData
    FieldDefs = <
      item
        Name = 'Linha'
        DataType = ftMemo
      end>
    Left = 108
    Top = 120
    object SaidaLinha: TMemoField
      DisplayWidth = 10
      FieldName = 'Linha'
      BlobType = ftMemo
    end
  end
  object dsSaida: TDataSource
    DataSet = Saida
    Left = 720
    Top = 152
  end
  object dsControle: TDataSource
    DataSet = dm.Controle
    Left = 768
    Top = 152
  end
  object RXBC: TRxMemoryData
    FieldDefs = <>
    Left = 672
    Top = 200
    object RXBCCodigo: TStringField
      FieldName = 'Codigo'
      Size = 10
    end
    object RXBCImportar: TMemoField
      FieldName = 'Importar'
      BlobType = ftMemo
    end
    object RXBCExportar: TMemoField
      FieldName = 'Exportar'
      BlobType = ftMemo
    end
  end
  object dsRXBC: TDataSource
    DataSet = RXBC
    Left = 720
    Top = 200
  end
  object RXEN: TRxMemoryData
    FieldDefs = <
      item
        Name = 'Linha'
        DataType = ftMemo
      end
      item
        Name = 'Codigo'
        DataType = ftInteger
      end>
    OnFilterRecord = RXENFilterRecord
    Left = 768
    Top = 200
    object RXENCodigo: TStringField
      FieldName = 'Codigo'
      Size = 3
    end
    object RXENLinha: TMemoField
      DisplayWidth = 10000
      FieldName = 'Linha'
      BlobType = ftMemo
    end
  end
  object dsRXEN: TDataSource
    DataSet = RXEN
    Left = 814
    Top = 200
  end
  object Linhas: TRxMemoryData
    FieldDefs = <>
    Left = 440
    Top = 128
    object LinhasLinha: TMemoField
      DisplayWidth = 10
      FieldName = 'Linha'
      BlobType = ftMemo
    end
  end
  object Portadores: TRxMemoryData
    FieldDefs = <>
    Left = 496
    Top = 128
    object PortadoresCodigo: TStringField
      FieldName = 'Codigo'
      Size = 10
    end
    object PortadoresNome: TStringField
      FieldName = 'Nome'
      Size = 100
    end
    object PortadoresAgencia: TStringField
      FieldName = 'Agencia'
      Size = 6
    end
    object PortadoresPraca: TStringField
      FieldName = 'Praca'
      Size = 7
    end
  end
  object dsLinhas: TDataSource
    DataSet = Linhas
    Left = 440
    Top = 176
  end
  object qryDuplicado: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select ID_ATO from titulos where '
      ''
      'NUMERO_TITULO=:NUMERO and '
      'CODIGO_APRESENTANTE=:CODIGO and'
      'VALOR_TITULO=:VALOR and'
      'DT_TITULO=:DATA and'
      'DT_VENCIMENTO=:VENCIMENTO'
      ''
      'order by ID_ATO'
      '')
    Left = 312
    Top = 120
    ParamData = <
      item
        Name = 'NUMERO'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CODIGO'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'VALOR'
        DataType = ftFloat
        ParamType = ptInput
      end
      item
        Name = 'DATA'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'VENCIMENTO'
        DataType = ftDate
        ParamType = ptInput
      end>
    object qryDuplicadoID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
  object qryConsultaProtocolo: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select ID_ATO from titulos where '
      ''
      'PROTOCOLO=:PROTOCOLO and DT_PROTOCOLO=:DATA')
    Left = 312
    Top = 168
    ParamData = <
      item
        Name = 'PROTOCOLO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'DATA'
        DataType = ftDate
        ParamType = ptInput
      end>
    object qryConsultaProtocoloID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
  object qryFaixa: TFDQuery
    Connection = dm.conCENTRAL
    SQL.Strings = (
      'select COD,EMOL,FETJ,FUND,FUNP,MUTUA,ACOTERJ,'
      'DISTRIB,TOT, TITULO from COD'
      ''
      'where '
      ''
      '(ATRIB=4) AND'
      ''
      '(CONVENIO=:C) AND (OCULTO=:OCULTO1 OR :OCULTO2 = '#39'T'#39') AND'
      ''
      '(MINIMO <= :VALOR1 AND MAXIMO >= :VALOR2)'
      ''
      'group by COD,EMOL,FETJ,FUND,FUNP,MUTUA,ACOTERJ,'
      'DISTRIB,TOT, TITULO')
    Left = 161
    Top = 199
    ParamData = <
      item
        Name = 'C'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'OCULTO1'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'OCULTO2'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'VALOR1'
        DataType = ftFloat
        ParamType = ptInput
      end
      item
        Name = 'VALOR2'
        DataType = ftFloat
        ParamType = ptInput
      end>
    object qryFaixaCOD: TIntegerField
      FieldName = 'COD'
      Origin = 'COD'
    end
    object qryFaixaEMOL: TFloatField
      FieldName = 'EMOL'
      Origin = 'EMOL'
    end
    object qryFaixaFETJ: TFloatField
      FieldName = 'FETJ'
      Origin = 'FETJ'
    end
    object qryFaixaFUND: TFloatField
      FieldName = 'FUND'
      Origin = 'FUND'
    end
    object qryFaixaFUNP: TFloatField
      FieldName = 'FUNP'
      Origin = 'FUNP'
    end
    object qryFaixaMUTUA: TFloatField
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
    end
    object qryFaixaACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
    end
    object qryFaixaDISTRIB: TFloatField
      FieldName = 'DISTRIB'
      Origin = 'DISTRIB'
    end
    object qryFaixaTOT: TFloatField
      FieldName = 'TOT'
      Origin = 'TOT'
    end
    object qryFaixaTITULO: TStringField
      FieldName = 'TITULO'
      Origin = 'TITULO'
      Size = 50
    end
  end
  object qryTabela: TFDQuery
    Connection = dm.conCENTRAL
    SQL.Strings = (
      'select * from CONCUS'
      ''
      'where TAB='#39'24'#39' and ITEM='#39'1'#39' and SUB=:S')
    Left = 824
    Top = 151
    ParamData = <
      item
        Name = 'S'
        DataType = ftString
        ParamType = ptInput
      end>
    object qryTabelaORDEM: TIntegerField
      FieldName = 'ORDEM'
      Origin = 'ORDEM'
    end
    object qryTabelaANO: TIntegerField
      FieldName = 'ANO'
      Origin = 'ANO'
    end
    object qryTabelaVAI: TStringField
      FieldName = 'VAI'
      Origin = 'VAI'
      FixedChar = True
      Size = 1
    end
    object qryTabelaTAB: TStringField
      FieldName = 'TAB'
      Origin = 'TAB'
    end
    object qryTabelaITEM: TStringField
      FieldName = 'ITEM'
      Origin = 'ITEM'
    end
    object qryTabelaSUB: TStringField
      FieldName = 'SUB'
      Origin = 'SUB'
    end
    object qryTabelaDESCR: TStringField
      FieldName = 'DESCR'
      Origin = 'DESCR'
      Size = 100
    end
    object qryTabelaVALOR: TFloatField
      FieldName = 'VALOR'
      Origin = 'VALOR'
    end
    object qryTabelaTEXTO: TStringField
      FieldName = 'TEXTO'
      Origin = 'TEXTO'
      Size = 250
    end
  end
  object RXFAKE: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 160
    Top = 120
    object RXFAKECEDENTE: TStringField
      DisplayLabel = 'Cedente'
      DisplayWidth = 25
      FieldName = 'CEDENTE'
      Size = 100
    end
    object RXFAKESACADOR: TStringField
      DisplayLabel = 'Sacador'
      DisplayWidth = 22
      FieldName = 'SACADOR'
      Size = 100
    end
    object RXFAKEDEVEDOR: TStringField
      DisplayLabel = 'Devedor'
      DisplayWidth = 28
      FieldName = 'DEVEDOR'
      Visible = False
      Size = 100
    end
    object RXFAKETIPO_DEVEDOR: TStringField
      DisplayLabel = 'Tipo'
      DisplayWidth = 4
      FieldName = 'TIPO_DEVEDOR'
      Visible = False
      Size = 1
    end
    object RXFAKECPF_CNPJ_DEVEDOR: TStringField
      DisplayLabel = 'Documento'
      DisplayWidth = 15
      FieldName = 'CPF_CNPJ_DEVEDOR'
      Visible = False
      Size = 14
    end
    object RXFAKERG_DEVEDOR: TStringField
      DisplayLabel = 'RG'
      DisplayWidth = 12
      FieldName = 'RG_DEVEDOR'
      Visible = False
      Size = 25
    end
    object RXFAKEEND_DEVEDOR: TStringField
      DisplayLabel = 'Endere'#231'o'
      DisplayWidth = 28
      FieldName = 'END_DEVEDOR'
      Visible = False
      Size = 100
    end
    object RXFAKECEP_DEVEDOR: TStringField
      DisplayLabel = 'CEP'
      DisplayWidth = 10
      FieldName = 'CEP_DEVEDOR'
      Visible = False
      EditMask = '99.999-999;0;_'
      Size = 8
    end
    object RXFAKEUF: TStringField
      DisplayWidth = 3
      FieldName = 'UF'
      Visible = False
      Size = 2
    end
    object RXFAKECID_DEVEDOR: TStringField
      DisplayLabel = 'Cidade'
      DisplayWidth = 19
      FieldName = 'CID_DEVEDOR'
      Visible = False
      Size = 100
    end
    object RXFAKEBAIRRO_DEVEDOR: TStringField
      DisplayLabel = 'Bairro'
      DisplayWidth = 21
      FieldName = 'BAIRRO_DEVEDOR'
      Visible = False
      Size = 100
    end
    object RXFAKEDT_TITULO: TDateField
      DisplayLabel = 'Data'
      DisplayWidth = 10
      FieldName = 'DT_TITULO'
      Visible = False
    end
    object RXFAKEDT_VENCIMENTO: TDateField
      DisplayLabel = 'Vencimento'
      DisplayWidth = 10
      FieldName = 'DT_VENCIMENTO'
      Visible = False
    end
    object RXFAKEVALOR_TITULO: TFloatField
      DisplayLabel = 'Valor'
      DisplayWidth = 10
      FieldName = 'VALOR_TITULO'
      Visible = False
      DisplayFormat = '###,##0.00'
    end
    object RXFAKEAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Visible = False
      Size = 100
    end
    object RXFAKECODIGO_APRESENTANTE: TStringField
      FieldName = 'CODIGO_APRESENTANTE'
      Size = 10
    end
    object RXFAKETIPO_TITULO: TIntegerField
      FieldName = 'TIPO_TITULO'
      Visible = False
    end
    object RXFAKENUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
      Visible = False
    end
    object RXFAKESALDO_TITULO: TFloatField
      FieldName = 'SALDO_TITULO'
      Visible = False
      DisplayFormat = '###,##0.00'
      EditFormat = '#####0.00'
    end
    object RXFAKEPRACA: TStringField
      FieldName = 'PRACA'
      Visible = False
    end
    object RXFAKEACEITE: TStringField
      FieldName = 'ACEITE'
      Size = 1
    end
    object RXFAKEENDOSSO: TStringField
      FieldName = 'ENDOSSO'
      Size = 1
    end
    object RXFAKESTATUS: TStringField
      Alignment = taCenter
      FieldName = 'STATUS'
    end
    object RXFAKENOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Size = 15
    end
    object RXFAKEAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 15
    end
    object RXFAKECUSTAS: TFloatField
      FieldName = 'CUSTAS'
    end
    object RXFAKECPF_CNPJ_SACADOR: TStringField
      FieldName = 'CPF_CNPJ_SACADOR'
      Size = 14
    end
    object RXFAKEEND_SACADOR: TStringField
      FieldName = 'END_SACADOR'
      Size = 100
    end
    object RXFAKECEP_SACADOR: TStringField
      FieldName = 'CEP_SACADOR'
      Size = 8
    end
    object RXFAKEUF_SACADOR: TStringField
      FieldName = 'UF_SACADOR'
      Size = 2
    end
    object RXFAKEBAIRRO_SACADOR: TStringField
      FieldName = 'BAIRRO_SACADOR'
      Size = 100
    end
    object RXFAKECID_SACADOR: TStringField
      FieldName = 'CID_SACADOR'
      Size = 100
    end
    object RXFAKEESPECIE: TStringField
      DisplayLabel = 'Esp'#233'cie'
      DisplayWidth = 18
      FieldName = 'ESPECIE'
      Visible = False
      Size = 100
    end
    object RXFAKEDocumento: TStringField
      FieldKind = fkCalculated
      FieldName = 'Documento'
      Size = 18
      Calculated = True
    end
    object RXFAKECheck: TBooleanField
      FieldName = 'Check'
    end
    object RXFAKEProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object RXFAKEIrregularidade: TStringField
      FieldName = 'Irregularidade'
      Size = 2
    end
    object RXFAKEMotivoRejeicao: TStringField
      FieldName = 'MotivoRejeicao'
      Size = 100
    end
    object RXFAKECONVENIO: TBooleanField
      FieldName = 'CONVENIO'
    end
    object RXFAKEAVISTA: TStringField
      FieldName = 'AVISTA'
      Size = 1
    end
    object RXFAKELETRA: TStringField
      FieldName = 'LETRA'
      Size = 1
    end
    object RXFAKEFINS: TStringField
      FieldName = 'FINS'
      Size = 1
    end
    object RXFAKENUMDEVEDOR: TStringField
      FieldName = 'NUMDEVEDOR'
    end
    object RXFAKESERVENTIA_AGREG: TStringField
      DisplayWidth = 100
      FieldName = 'SERVENTIA_AGREG'
      Size = 100
    end
    object RXFAKESEQUENCIAL_ARQ: TStringField
      FieldName = 'SEQUENCIAL_ARQ'
      Size = 4
    end
    object RXFAKESEQ_BANCO: TIntegerField
      FieldName = 'SEQ_BANCO'
    end
    object RXFAKEFLG_COOBRIGADO: TStringField
      Alignment = taCenter
      FieldName = 'FLG_COOBRIGADO'
      Size = 1
    end
    object RXFAKEDIAS_AVISTA: TIntegerField
      FieldName = 'DIAS_AVISTA'
    end
    object RXFAKECARTORIO: TIntegerField
      FieldName = 'CARTORIO'
    end
    object RXFAKEPOSTECIPADO: TStringField
      FieldName = 'POSTECIPADO'
    end
  end
  object RX: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 16
    Top = 152
    object RXCEDENTE: TStringField
      DisplayLabel = 'Cedente'
      DisplayWidth = 25
      FieldName = 'CEDENTE'
      Size = 100
    end
    object RXSACADOR: TStringField
      DisplayLabel = 'Sacador'
      DisplayWidth = 22
      FieldName = 'SACADOR'
      Size = 100
    end
    object RXDEVEDOR: TStringField
      DisplayLabel = 'Devedor'
      DisplayWidth = 28
      FieldName = 'DEVEDOR'
      Visible = False
      Size = 100
    end
    object RXTIPO_DEVEDOR: TStringField
      DisplayLabel = 'Tipo'
      DisplayWidth = 4
      FieldName = 'TIPO_DEVEDOR'
      Visible = False
      Size = 1
    end
    object RXCPF_CNPJ_DEVEDOR: TStringField
      DisplayLabel = 'Documento'
      DisplayWidth = 15
      FieldName = 'CPF_CNPJ_DEVEDOR'
      Visible = False
      Size = 14
    end
    object RXRG_DEVEDOR: TStringField
      DisplayLabel = 'RG'
      DisplayWidth = 12
      FieldName = 'RG_DEVEDOR'
      Visible = False
      Size = 25
    end
    object RXEND_DEVEDOR: TStringField
      DisplayLabel = 'Endere'#231'o'
      DisplayWidth = 28
      FieldName = 'END_DEVEDOR'
      Visible = False
      Size = 100
    end
    object RXCEP_DEVEDOR: TStringField
      DisplayLabel = 'CEP'
      DisplayWidth = 10
      FieldName = 'CEP_DEVEDOR'
      Visible = False
      EditMask = '99.999-999;0;_'
      Size = 8
    end
    object RXUF: TStringField
      DisplayWidth = 3
      FieldName = 'UF'
      Visible = False
      Size = 2
    end
    object RXCID_DEVEDOR: TStringField
      DisplayLabel = 'Cidade'
      DisplayWidth = 19
      FieldName = 'CID_DEVEDOR'
      Visible = False
      Size = 100
    end
    object RXBAIRRO_DEVEDOR: TStringField
      DisplayLabel = 'Bairro'
      DisplayWidth = 21
      FieldName = 'BAIRRO_DEVEDOR'
      Visible = False
      Size = 100
    end
    object RXDT_TITULO: TDateField
      DisplayLabel = 'Data'
      DisplayWidth = 10
      FieldName = 'DT_TITULO'
      Visible = False
    end
    object RXDT_VENCIMENTO: TDateField
      DisplayLabel = 'Vencimento'
      DisplayWidth = 10
      FieldName = 'DT_VENCIMENTO'
      Visible = False
    end
    object RXVALOR_TITULO: TFloatField
      DisplayLabel = 'Valor'
      DisplayWidth = 10
      FieldName = 'VALOR_TITULO'
      Visible = False
      DisplayFormat = '###,##0.00'
    end
    object RXAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Visible = False
      Size = 100
    end
    object RXCODIGO_APRESENTANTE: TStringField
      FieldName = 'CODIGO_APRESENTANTE'
      Size = 10
    end
    object RXTIPO_TITULO: TIntegerField
      FieldName = 'TIPO_TITULO'
      Visible = False
    end
    object RXNUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
      Visible = False
    end
    object RXSALDO_TITULO: TFloatField
      FieldName = 'SALDO_TITULO'
      Visible = False
      DisplayFormat = '###,##0.00'
      EditFormat = '#####0.00'
    end
    object RXPRACA: TStringField
      FieldName = 'PRACA'
      Visible = False
    end
    object RXACEITE: TStringField
      FieldName = 'ACEITE'
      Size = 1
    end
    object RXENDOSSO: TStringField
      FieldName = 'ENDOSSO'
      Size = 1
    end
    object RXSTATUS: TStringField
      Alignment = taCenter
      FieldName = 'STATUS'
    end
    object RXNOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Size = 15
    end
    object RXAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 15
    end
    object RXCUSTAS: TFloatField
      FieldName = 'CUSTAS'
    end
    object RXCPF_CNPJ_SACADOR: TStringField
      FieldName = 'CPF_CNPJ_SACADOR'
      Size = 14
    end
    object RXEND_SACADOR: TStringField
      FieldName = 'END_SACADOR'
      Size = 100
    end
    object RXCEP_SACADOR: TStringField
      FieldName = 'CEP_SACADOR'
      Size = 8
    end
    object RXUF_SACADOR: TStringField
      FieldName = 'UF_SACADOR'
      Size = 2
    end
    object RXBAIRRO_SACADOR: TStringField
      FieldName = 'BAIRRO_SACADOR'
      Size = 100
    end
    object RXCID_SACADOR: TStringField
      FieldName = 'CID_SACADOR'
      Size = 100
    end
    object RXESPECIE: TStringField
      DisplayLabel = 'Esp'#233'cie'
      DisplayWidth = 18
      FieldName = 'ESPECIE'
      Visible = False
      Size = 100
    end
    object RXDocumento: TStringField
      FieldKind = fkCalculated
      FieldName = 'Documento'
      Size = 18
      Calculated = True
    end
    object RXCheck: TBooleanField
      FieldName = 'Check'
    end
    object RXProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object RXIrregularidade: TStringField
      FieldName = 'Irregularidade'
      Size = 2
    end
    object RXMotivoRejeicao: TStringField
      FieldName = 'MotivoRejeicao'
      Size = 100
    end
    object RXCONVENIO: TBooleanField
      FieldName = 'CONVENIO'
    end
    object RXAVISTA: TStringField
      FieldName = 'AVISTA'
      Size = 1
    end
    object RXLETRA: TStringField
      FieldName = 'LETRA'
      Size = 1
    end
    object RXFINS: TStringField
      FieldName = 'FINS'
      Size = 1
    end
    object RXNUMDEVEDOR: TStringField
      FieldName = 'NUMDEVEDOR'
    end
    object RXSERVENTIA_AGREG: TStringField
      DisplayWidth = 100
      FieldName = 'SERVENTIA_AGREG'
      Size = 100
    end
    object RXSEQUENCIAL_ARQ: TStringField
      FieldName = 'SEQUENCIAL_ARQ'
      Size = 4
    end
    object RXSEQ_BANCO: TIntegerField
      FieldName = 'SEQ_BANCO'
    end
    object RXFLG_COOBRIGADO: TStringField
      Alignment = taCenter
      FieldName = 'FLG_COOBRIGADO'
      Size = 1
    end
    object RXDIAS_AVISTA: TIntegerField
      FieldName = 'DIAS_AVISTA'
    end
    object RXCARTORIO: TIntegerField
      FieldName = 'CARTORIO'
    end
    object RXPOSTECIPADO: TStringField
      FieldName = 'POSTECIPADO'
    end
  end
end
