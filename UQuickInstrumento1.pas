unit UQuickInstrumento1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, QuickRpt, QRCtrls, DB, Clipbrd, RxMemDS;

type
  TFQuickInstrumento1 = class(TForm)
    qkInstrumento: TQuickRep;
    Titulo: TQRBand;
    dsServentia: TDataSource;
    qrEnderecoServentia: TQRDBText;
    Detalhes: TQRBand;
    RX: TRxMemoryData;
    RXTitulo: TStringField;
    RXApresentante: TStringField;
    RXCredor: TStringField;
    RXSacador: TStringField;
    RXDevedor: TStringField;
    RXValor: TFloatField;
    RXVencimento: TDateField;
    RXProtocolo: TStringField;
    RXIntimacao: TDateField;
    RXSaldoProtesto: TFloatField;
    RXCustas: TFloatField;
    RXEndereco: TStringField;
    qrTitulo: TQRLabel;
    RXLivro: TStringField;
    RXFolha: TStringField;
    qrApresentante: TQRLabel;
    qrNatureza: TQRLabel;
    qrNumero: TQRLabel;
    qrEndosso: TQRLabel;
    qrDevedor: TQRLabel;
    qrDocumento: TQRLabel;
    qrIntimado: TQRLabel;
    RXNumero: TStringField;
    RXEndosso: TStringField;
    RXTipo: TStringField;
    RXDocumento: TStringField;
    RXIntimado: TStringField;
    RXMotivo: TStringField;
    qrSacador: TQRLabel;
    qrCedente: TQRLabel;
    RXCedente: TStringField;
    qrValor: TQRLabel;
    qrEmissao: TQRLabel;
    RXDt_Titulo: TDateField;
    RE1: TRichEdit;
    RT2: TQRRichText;
    RE2: TRichEdit;
    RXEmolumentos: TFloatField;
    RXFetj: TFloatField;
    RXFundperj: TFloatField;
    RXFunperj: TFloatField;
    RXMutua: TFloatField;
    RXAcoterj: TFloatField;
    RXTotal: TFloatField;
    RXSelo: TStringField;
    RXEnderecoAp: TStringField;
    RXDt_Protocolo: TDateField;
    qrMotivo: TQRLabel;
    RXDt_Registro: TDateField;
    RXTipoProtesto: TIntegerField;
    RXObservacao: TMemoField;
    RE3: TRichEdit;
    RXPraca: TStringField;
    qrSite: TQRDBText;
    RXTermo: TIntegerField;
    qrEnderecoSacador: TQRLabel;
    RXEnderecoSa: TStringField;
    RXDt_Publicacao: TDateField;
    RXDt_Intimacao: TDateField;
    RXMensagem: TStringField;
    Child: TQRChildBand;
    ChildBand: TQRChildBand;
    shEscrevente: TQRShape;
    qrFuncionario: TQRLabel;
    qrQualificacao: TQRLabel;
    qrTestemunho: TQRLabel;
    qrEM: TQRLabel;
    qrFE: TQRLabel;
    qrFD: TQRLabel;
    qrFP: TQRLabel;
    qrMU: TQRLabel;
    qrTT: TQRLabel;
    qrEmolumentos: TQRLabel;
    qrFetj: TQRLabel;
    qrFundperj: TQRLabel;
    qrFunperj: TQRLabel;
    qrMutua: TQRLabel;
    qrTotal: TQRLabel;
    RT3: TQRRichText;
    qrDI: TQRLabel;
    qrDistribuicao: TQRLabel;
    qrEndereco: TQRLabel;
    qrTB: TQRLabel;
    qrTarifa: TQRLabel;
    qrCO: TQRLabel;
    qrCorreio: TQRLabel;
    RXTarifa: TFloatField;
    RXAR: TFloatField;
    qrEnderecoAp: TQRLabel;
    RT1: TQRRichText;
    RXDistribuicao: TFloatField;
    qrServentia: TQRLabel;
    lbConvenio: TQRLabel;
    RXConvenio: TStringField;
    qrFN: TQRLabel;
    qrFunarpen: TQRLabel;
    RXFunarpen: TFloatField;
    QRShape1: TQRShape;
    qrPM: TQRLabel;
    qrPmcmv: TQRLabel;
    RXPmcmv: TFloatField;
    qrAC: TQRLabel;
    qrAcoterj: TQRLabel;
    RXSaldoTitulo: TFloatField;
    lbCGJ1: TQRLabel;
    lbCGJ2: TQRLabel;
    lbCGJ3: TQRLabel;
    lbSeloEletronico: TQRLabel;
    lbCGJ4: TQRLabel;
    lbCGJ5: TQRLabel;
    RXAleatorio: TStringField;
    RXFins: TStringField;
    RXNossoNumero: TStringField;
    RXDt_Certidao: TDateField;
    ChildBand2: TQRChildBand;
    qrInstrumento: TQRLabel;
    qrLivroFolha: TQRLabel;
    qrProtocolo: TQRLabel;
    qrDataProtocolo: TQRLabel;
    qrDataProtesto: TQRLabel;
    lbCoDevedor: TQRLabel;
    lbDocumento: TQRLabel;
    lbEndereco: TQRLabel;
    RXIdAto: TIntegerField;
    RXStatus: TStringField;
    RXSeloCancelamento: TStringField;
    RXDt_Cancelamento: TDateField;
    qrAviso1: TQRLabel;
    qrAviso2: TQRLabel;
    RE4: TRichEdit;
    RT4: TQRRichText;
    RXSeloProtesto: TStringField;
    lbCoDevedor2: TQRLabel;
    lbDocumento2: TQRLabel;
    lbEndereco2: TQRLabel;
    RXIss: TFloatField;
    QRShape2: TQRShape;
    QRLabel1: TQRLabel;
    qrIss: TQRLabel;
    Shape: TQRShape;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    procedure TituloBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure DetalhesBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qkInstrumentoEndPage(Sender: TCustomQuickRep);
    procedure FormCreate(Sender: TObject);
    procedure ChildBandBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickInstrumento1: TFQuickInstrumento1;

implementation

uses UDM, URelIntimacao1, UPF, UGeral, StrUtils;

{$R *.dfm}

procedure TFQuickInstrumento1.TituloBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if dm.ServentiaSEXO.AsString='M' then
    qrTitulo.Caption:='Tabeli�o: '+dm.ServentiaTABELIAO.AsString
      else qrTitulo.Caption:='Tabeli�: '+dm.ServentiaTABELIAO.AsString;
end;

procedure TFQuickInstrumento1.DetalhesBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Motivo,DataVencimento,Tipo,vDataProtesto: String;
begin
  if (RXVencimento.AsDateTime=30/12/1899) or (RXVencimento.AsDateTime=0) or (RXVencimento.AsDateTime=RXDt_Intimacao.AsDateTime) then
    DataVencimento:='� VISTA'
      else DataVencimento:=PF.FormatarData(RXVencimento.AsDateTime,'N');

  case RXTipoProtesto.AsInteger of
    1: Motivo:='Falta de Pagamento';
    2: Motivo:='N�o Aceite';
    3: Motivo:='N�o Devolu��o';
  end;

  if dm.ValorParametro(53)='S' then
    vDataProtesto:='   DATA DO PROTESTO: '+FormatDateTime('dd/mm/yyyy',RXDt_Registro.AsDateTime)+' - '+FormatDateTime('HH:MM',dm.vHora)+'hs'
      else vDataProtesto:='   DATA DO PROTESTO: '+FormatDateTime('dd/mm/yyyy',RXDt_Registro.AsDateTime);

  qrMotivo.Caption          :='�Motivo do Protesto: '+Motivo;
  qrProtocolo.Caption       :='PROTOCOLO N�: '+RXProtocolo.AsString;
  qrDataProtocolo.Caption   :='DATA DO PROTOCOLO: '+FormatDateTime('dd/mm/yyyy',RXDt_Protocolo.AsDateTime);
  qrDataProtesto.Caption    :=vDataProtesto;

  if dm.ValorParametro(33)='S' then
  qrLivroFolha.Caption      :='LIVRO: '+RXLivro.AsString+'   FOLHA: '+RXFolha.AsString+GR.iif(RXTermo.AsInteger<>0,'   TERMO: '+RXTermo.AsString,'')
  else qrLivroFolha.Caption :='LIVRO: '+RXLivro.AsString+'   FOLHA: '+RXFolha.AsString;
  if dm.ServentiaCODIGO.AsInteger=1430 then
  qrLivroFolha.Caption      :='LIVRO: '+RXLivro.AsString+'   FOLHA: '+RXFolha.AsString;

  qrApresentante.Caption    :='�Apresentante: '+RXApresentante.AsString;
  qrEnderecoAp.Caption      :='�Endere�o: '+RXEnderecoAp.AsString;
  qrNatureza.Caption        :='�Natureza: '+RXTitulo.AsString;
  qrNumero.Caption          :='�N� do T�tulo: '+RXNumero.AsString+'     �Pra�a de Pagamento: '+RXPraca.AsString;
  qrEndosso.Caption         :='�Endosso: '+RXEndosso.AsString+GR.iif(RXNossoNumero.AsString<>'','  �Nosso N�mero: '+RXNossoNumero.AsString,'');
  qrDevedor.Caption         :='�Devedor: '+RXDevedor.AsString;
  qrEnderecoSacador.Caption :='�Endere�o: '+RXEnderecoSa.AsString;
  lbConvenio.Enabled        :=RXConvenio.AsString='S';

  PF.CarregarDevedor(RXIdAto.AsInteger);
  if dm.RxDevedor.RecordCount>1 then
  begin
      dm.RxDevedor.Next;
      lbCoDevedor.Caption :='�Co-Devedor: '+dm.RxDevedorNOME.AsString;
      lbDocumento.Caption :=IfThen(dm.RxDevedorTIPO.AsString='F','�CPF: ','�CNPJ: ')+GR.FormatarCPFCNPJ(dm.RxDevedorDOCUMENTO.AsString,'N');
      lbEndereco.Caption  :='�Endere�o: '+dm.RxDevedorENDERECO.AsString+' '+dm.RxDevedorBAIRRO.AsString+' '+
                                          dm.RxDevedorMUNICIPIO.AsString+' '+dm.RxDevedorUF.AsString;
      if dm.RxDevedorCEP.AsString<>'' then
      lbEndereco.Caption:=lbEndereco.Caption+' CEP:'+PF.FormatarCEP(dm.RxDevedorCEP.AsString);

      lbCoDevedor.Enabled :=True;
      lbDocumento.Enabled :=True;
      lbEndereco.Enabled  :=True;

      if dm.RxDevedor.RecordCount>2 then
      begin
          dm.RxDevedor.Next;
          lbCoDevedor2.Caption :='�Co-Devedor: '+dm.RxDevedorNOME.AsString;
          lbDocumento2.Caption :=IfThen(dm.RxDevedorTIPO.AsString='F','�CPF: ','�CNPJ: ')+GR.FormatarCPFCNPJ(dm.RxDevedorDOCUMENTO.AsString,'N');
          lbEndereco2.Caption  :='�Endere�o: '+dm.RxDevedorENDERECO.AsString+' '+dm.RxDevedorBAIRRO.AsString+' '+
                                              dm.RxDevedorMUNICIPIO.AsString+' '+dm.RxDevedorUF.AsString;
          if dm.RxDevedorCEP.AsString<>'' then
          lbEndereco2.Caption  :=lbEndereco2.Caption+' CEP:'+PF.FormatarCEP(dm.RxDevedorCEP.AsString);
          lbCoDevedor2.Enabled :=True;
          lbDocumento2.Enabled :=True;
          lbEndereco2.Enabled  :=True;
      end;
  end
  else
  begin
      lbCoDevedor.Enabled :=False;
      lbDocumento.Enabled :=False;
      lbEndereco.Enabled  :=False;
  end;

  dm.RxDevedor.Close;

  if RXEnderecoAp.AsString='' then qrEnderecoAp.Caption:='';

  if RXTipo.AsString='J' then
    qrDocumento.Caption:='�CNPJ: '+RXDocumento.AsString
      else qrDocumento.Caption:='�CPF: '+RXDocumento.AsString;

  qrEndereco.Caption:='�Endere�o: '+RXEndereco.AsString;

  RE4.Lines.Clear;
  if RXIntimado.AsString='EDITAL' then
  begin
      qrIntimado.Caption:='�Intima��o: O devedor foi intimado por EDITAL em '+
                          FormatDateTime('dd/mm/yyyy',RXDt_Publicacao.AsDateTime)+
                          ', nos termos do aviso 199/07, da CGJ/RJ.';
      if RXMotivo.AsString<>'' then
      begin
          RE4.Lines.Add('Motivo: '+RXMotivo.AsString);
          RE4.Text:=PF.JstParagrafo(RE4.Text,122);
      end;
  end;

  if RXIntimado.AsString='PESSOAL' then
  qrIntimado.Caption:='�Intima��o: O devedor foi intimado por INTIMA��O PESSOAL em '+FormatDateTime('dd/mm/yyyy',RXDt_Intimacao.AsDateTime)+'.';

  if RXIntimado.AsString='CARTA' then
  qrIntimado.Caption:='�Intima��o: O devedor foi intimado por CARTA em '+FormatDateTime('dd/mm/yyyy',RXDt_Intimacao.AsDateTime)+'.';

  qrSacador.Caption       :='�Sacador: '+RXSacador.AsString;
  qrCedente.Caption       :='�Cedente: '+RXCedente.AsString;
  qrValor.Caption         :='�Valor do T�tulo: R$ '+FloatToStrF(RXValor.AsFloat,ffNumber,15,2)+'     �Saldo do T�tulo: R$ '+FloatToStrF(RXSaldoTitulo.AsFloat,ffNumber,15,2);;
  qrEmissao.Caption       :='�Emiss�o: '+PF.FormatarData(RXDt_Titulo.AsDateTime,'N')+'     �Vencimento: '+DataVencimento;
  qrEmolumentos.Caption   :=IfThen(RXConvenio.AsString='S','0,00',FloatToStrF(RXEmolumentos.AsFloat,ffNumber,7,2));
  qrFetj.Caption          :=IfThen(RXConvenio.AsString='S','0,00',FloatToStrF(RXFetj.AsFloat,ffNumber,7,2));
  qrFundperj.Caption      :=IfThen(RXConvenio.AsString='S','0,00',FloatToStrF(RXFundperj.AsFloat,ffNumber,7,2));
  qrFunperj.Caption       :=IfThen(RXConvenio.AsString='S','0,00',FloatToStrF(RXFunperj.AsFloat,ffNumber,7,2));
  qrFunarpen.Caption      :=IfThen(RXConvenio.AsString='S','0,00',FloatToStrF(RXFunarpen.AsFloat,ffNumber,7,2));
  qrPmcmv.Caption         :=IfThen(RXConvenio.AsString='S','0,00',FloatToStrF(RXPMCMV.AsFloat,ffNumber,7,2));
  qrIss.Caption           :=IfThen(RXConvenio.AsString='S','0,00',FloatToStrF(RXIss.AsFloat,ffNumber,7,2));
  qrMutua.Caption         :=IfThen(RXConvenio.AsString='S','0,00',FloatToStrF(RXMutua.AsFloat,ffNumber,7,2));
  qrAcoterj.Caption       :=IfThen(RXConvenio.AsString='S','0,00',FloatToStrF(RXAcoterj.AsFloat,ffNumber,7,2));
  qrDistribuicao.Caption  :=IfThen(RXConvenio.AsString='S','0,00',FloatToStrF(RXDistribuicao.AsFloat,ffNumber,7,2));
  qrTarifa.Caption        :=IfThen(RXConvenio.AsString='S','0,00',FloatToStrF(RXTarifa.AsFloat,ffNumber,7,2));
  qrCorreio.Caption       :=IfThen(RXConvenio.AsString='S','0,00',FloatToStrF(RXAR.AsFloat,ffNumber,7,2));
  qrTotal.Caption         :=IfThen(RXConvenio.AsString='S','0,00',FloatToStrF(RXTotal.AsFloat,ffNumber,7,2));

  if dm.ServentiaCODIGO.AsInteger=4473 then
  begin
      qrCorreio.Caption:='0,00';
      qrTotal.Caption:=IfThen(RXConvenio.AsString='S','0,00',FloatToStrF(RXTotal.AsFloat-RXAR.AsFloat,ffNumber,7,2));
  end;

  {FOI UM MEIO R�PIDO DE IDENTIFICAR QUE � CERTID�O DE INTEIRO TEOR}
  if RXDt_Certidao.AsDateTime<>0 then
  begin
      RE1.Lines.Text:='SAIBAM quantos esta p�blica certid�o de inteiro teor virem que em '+FormatDateTime('dd/mm/yyyy',RXDt_Certidao.AsDateTime)+
                      ', nesta cidade de '+dm.ServentiaCIDADE.AsString+', Estado do Rio de Janeiro, Republica federativa do Brasil, '+
                      dm.ServentiaDESCRICAO.AsString+', consta  protesto no teor seguinte:';

      qrAviso1.Enabled:=False;
      qrAviso2.Enabled:=False;

      qrMotivo.Caption:=qrMotivo.Caption+'  �Situa��o Atual: '+RXStatus.AsString;
  end
  else
  begin
      RE1.Lines.Text:='SAIBAM quantos este p�blico Instrumento de Protesto virem que '+PF.DataEscrita(RXDt_Protocolo.AsDateTime)+', '+
                      'nesta cidade de '+dm.ServentiaCIDADE.AsString+', Estado do Rio de Janeiro, Rep�blica Federativa do Brasil, '+
                      'na sede deste '+dm.ServentiaDESCRICAO.AsString+', foi-me indicado para ser  protestado o seguinte t�tulo:';
  end;

  RE1.Text:=PF.JstParagrafo(RE1.Text,74);

  if dm.ServentiaCODIGO.AsInteger=1554 then
  begin
      Tipo:='triduo';
      qrFuncionario.Caption:='TABELI�O DO PROTESTO';
      qrQualificacao.Enabled:=False;
  end
  else
    Tipo:='prazo';

  {FOI UM MEIO R�PIDO DE IDENTIFICAR QUE � CERTID�O DE INTEIRO TEOR}
  if RXDt_Certidao.AsDateTime<>0 then
  begin
      RE2.Lines.Text:='CERTIFICO que intimei o Devedor por INTIMA��O '+RXIntimado.AsString+' para vir dentro do '+Tipo+' legal pagar '+
                      'o t�tulo em apre�o, o qual n�o pagou.';

      RE2.Lines.Add('');
      if RXDt_Registro.AsDateTime<>0 then
      RE2.Lines.Add('Data do Protesto: '+FormatDateTime('dd/mm/yyyy',RXDt_Registro.AsDateTime)+' - Selo: '+RXSeloProtesto.AsString);

      if RXDt_Cancelamento.AsDateTime<>0 then
      RE2.Lines.Add('Data do Cancelamento: '+FormatDateTime('dd/mm/yyyy',RXDt_Cancelamento.AsDateTime)+' - Selo: '+RXSeloCancelamento.AsString);
      qrDataProtesto.Enabled:=False;
  end
  else
    RE2.Lines.Text:='CERTIFICO que intimei o Devedor por INTIMA��O '+RXIntimado.AsString+' para vir dentro do '+Tipo+' legal pagar '+
                    'o t�tulo em apre�o, o qual n�o pagou. Pelo que, para garantia do credor, lavro o presente instrumento '+
                    'de protesto que registrado no livro pr�prio desde Cart�rio, vai por mim assinado com meu sinal p�blico.';

  if RXFins.AsString='SIM' then
  begin
      RE2.Lines.Add('');
      RE2.Lines.Add('T�tulo apresentado para FINS FALIMENTARES (art. 10 L.F. e 23 � unico, Lei 9492/97).');
  end;

  PF.JustificarRichEdit(RE2,True);

  RE3.Enabled:=False;
  Child.Enabled:=False;
  RE3.Lines.Text:='';
  if RXObservacao.AsString<>'' then
  begin
      RE3.Enabled:=True;
      Child.Enabled:=True;
      RE3.Lines.Add('� '+Trim(RXObservacao.AsString));
  end;

  if RXMensagem.AsString<>'' then
  begin
      RE3.Enabled:=True;
      Child.Enabled:=True;
      RE3.Lines.Add('�'+Trim(RXMensagem.AsString));
  end;

  lbSeloEletronico.Caption:=GR.SeloFormatado(RXSelo.AsString,RXAleatorio.AsString);

  {FOI UM MEIO R�PIDO DE IDENTIFICAR QUE � CERTID�O DE INTEIRO TEOR}
  if RXDt_Certidao.AsDateTime<>0 then
  begin
      if dm.ServentiaCODIGO.AsInteger=1726 then
      begin
          qrServentia.Enabled             :=False;
          qrTitulo.Enabled                :=False;
          qrEnderecoServentia.Enabled     :=False;
          qrSite.Enabled                  :=False;
          qrAviso1.Enabled                :=False;
          qkInstrumento.Frame.DrawTop     :=False;
          qkInstrumento.Frame.DrawBottom  :=False;
          qkInstrumento.Frame.DrawLeft    :=False;
          qkInstrumento.Frame.DrawRight   :=False;
          Titulo.Height                   :=160;
      end;
  end;
end;

procedure TFQuickInstrumento1.qkInstrumentoEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickInstrumento1.FormCreate(Sender: TObject);
begin
  qrServentia.Caption :=dm.ServentiaDESCRICAO.AsString;
  qrAviso1.Enabled    :=dm.ServentiaCODIGO.AsInteger<>1622;
  
  {if dm.ServentiaCODIGO.AsInteger=1430 then
    if GR.Pergunta('Utilizar informa��es do 4� OFicio') then
      qrServentia.Caption:='VASSOURAS - 4� OF�CIO';}

  if (dm.ServentiaCODIGO.AsInteger=2369) then
  begin
      qkInstrumento.Frame.DrawTop     :=False;
      qkInstrumento.Frame.DrawBottom  :=False;
      qkInstrumento.Frame.DrawLeft    :=False;
      qkInstrumento.Frame.DrawRight   :=False;
      qrServentia.Font.Name           :='Arial';
      qrServentia.Font.Style          :=[fsBold];
      qrTitulo.Font.Name              :='Arial';
      qrEnderecoServentia.Font.Name   :='Arial';
      qrSite.Font.Name                :='Arial';
      qrInstrumento.Font.Name         :='Arial';
      qrProtocolo.Font.Name           :='Arial';
      qrDataProtocolo.Font.Name       :='Arial';
      qrDataProtesto.Font.Name        :='Arial';
      qrLivroFolha.Font.Name          :='Arial';
      RE1.Font.Name                   :='Arial';
      RE2.Font.Name                   :='Arial';
      RE3.Font.Name                   :='Arial';
      RT1.Font.Name                   :='Arial';
      RT2.Font.Name                   :='Arial';
      RT3.Font.Name                   :='Arial';
      qrMotivo.Font.Name              :='Arial';
      qrApresentante.Font.Name        :='Arial';
      qrEnderecoAp.Font.Name          :='Arial';
      qrEnderecoAp.Font.Style         :=[];
      qrEnderecoAp.Font.Size          :=10;
      qrNatureza.Font.Name            :='Arial';
      qrNumero.Font.Name              :='Arial';
      qrValor.Font.Name               :='Arial';
      qrEmissao.Font.Name             :='Arial';
      qrEndosso.Font.Name             :='Arial';
      qrDevedor.Font.Name             :='Arial';
      qrDocumento.Font.Name           :='Arial';
      qrEndereco.Font.Name            :='Arial';
      qrIntimado.Font.Name            :='Arial';
      qrSacador.Font.Name             :='Arial';
      qrEnderecoSacador.Font.Name     :='Arial';
      qrCedente.Font.Name             :='Arial';
      qrEM.Font.Name                  :='Arial';
      qrFE.Font.Name                  :='Arial';
      qrFD.Font.Name                  :='Arial';
      qrFP.Font.Name                  :='Arial';
      qrMU.Font.Name                  :='Arial';
      qrAC.Font.Name                  :='Arial';
      qrDI.Font.Name                  :='Arial';
      qrTB.Font.Name                  :='Arial';
      qrCO.Font.Name                  :='Arial';
      qrTT.Font.Name                  :='Arial';
      qrEmolumentos.Font.Name         :='Arial';
      qrFetj.Font.Name                :='Arial';
      qrFundperj.Font.Name            :='Arial';
      qrFunperj.Font.Name             :='Arial';
      qrMutua.Font.Name               :='Arial';
      qrDistribuicao.Font.Name        :='Arial';
      qrTarifa.Font.Name              :='Arial';
      qrCorreio.Font.Name             :='Arial';
      qrTotal.Font.Name               :='Arial';
      qrEM.Caption                    :='Emolumentos:';
      qrFE.Caption                    :='Fetj:';
      qrFD.Caption                    :='Fundperj:';
      qrFP.Caption                    :='Funperj:';
      qrMU.Caption                    :='M�tua:';
      qrAC.Caption                    :='Acoterj:';
      qrDI.Caption                    :='Distribui��o:';
      qrTB.Caption                    :='Tarifa Banc�ria:';
      qrCO.Caption                    :='Correios:';
      qrTT.Caption                    :='Total:';
  end;

  {if (dm.CertidoesTIPO_CERTIDAO.AsString='I') and (dm.ServentiaCODIGO.AsInteger=1823) then
  begin
      qrdata.Left           := 136;
      qrData.Top            := 220;
      qrEmTestemunho.Left   := 74;
      qrEmTestemunho.Top    := 235;
      shEscrevente.Left     := 18;
      shEscrevente.Top      := 269;
      qrFuncionario.Left    := 103;
      qrFuncionario.Top     := 276;
      qrQualificacao.Left   := 120;
      qrQualificacao.Top    := 291;
      qrAviso1.Left         := 43;
      qrAviso1.Top          := 144;
      qrAviso2.Left         := 70;
      qrAviso2.Top          := 192;
  end
  else
  begin
      qrdata.Left           := 296;
      qrData.Top            := 129;
      qrEmTestemunho.Left   := 234;
      qrEmTestemunho.Top    := 144;
      shEscrevente.Left     := 162;
      shEscrevente.Top      := 178;
      qrFuncionario.Left    := 247;
      qrFuncionario.Top     := 185;
      qrQualificacao.Left   := 264;
      qrQualificacao.Top    := 200;
      qrAviso1.Left         := 43;
      qrAviso1.Top          := 216;
      qrAviso2.Left         := 70;
      qrAviso2.Top          := 264;
  end;}

  if (dm.ServentiaCODIGO.AsInteger=2353) or (dm.ServentiaCODIGO.AsInteger=4473) then
  begin
      qrDI.Enabled          :=False;
      qrTB.Enabled          :=False;
      qrCO.Enabled          :=False;
      qrDistribuicao.Enabled:=False;
      qrTarifa.Enabled      :=False;
      qrCorreio.Enabled     :=False;
      qrTT.Top              :=21;
      qrTotal.Top           :=21;
      //Shape.Height          :=109;
  end;

  {PEDRO - 31/03/2016}
  if GR.Pergunta('DESEJA IMPRIMIR NA FOLHA DE SEGURAN�A')  then
  begin
      if dm.vFlTiracabecalho = 'S' then
      begin
        Titulo.Enabled := False;
      end
      else
      begin
        Titulo.Enabled := True;
      end;

      qkInstrumento.Frame.DrawTop   := False;
      qkInstrumento.Frame.DrawBottom:= False;
      qkInstrumento.Frame.DrawLeft  := False;
      qkInstrumento.Frame.DrawRight := False;


      qkInstrumento.Page.BottomMargin := StrToFloat(dm.vFlBottom);
      qkInstrumento.Page.TopMargin    := StrToFloat(dm.vFlTop);
      qkInstrumento.Page.LeftMargin   := StrToFloat(dm.vFlLeft);
      qkInstrumento.Page.RightMargin  := StrToFloat(dm.vFlRight);

      if dm.vFlFolha ='A4' then
      begin
        qkInstrumento.Page.Width := 210.0;
        qkInstrumento.Page.Length:= 297.0;
      end;

      if dm.vFlFolha ='Legal' then
      begin
        qkInstrumento.Page.Width := 215.9;
        qkInstrumento.Page.Length:= 355.6;
      end;
  end;    
end;

procedure TFQuickInstrumento1.ChildBandBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if RXDt_Certidao.AsDateTime<>0 then
    qrTestemunho.Caption:=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',RXDt_Certidao.AsDateTime)
      else qrTestemunho.Caption:=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',RXDt_Registro.AsDateTime);

  qrTestemunho.Caption:=qrTestemunho.Caption+' Em testemunho da verdade.';
end;

end.

