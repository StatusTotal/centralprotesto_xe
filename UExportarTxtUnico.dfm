�
 TFEXPORTARTXTUNICO 0�8  TPF0TFExportarTxtUnicoFExportarTxtUnicoLeftnTop� BorderIconsbiSystemMenu BorderStylebsDialogCaptionExportarClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
KeyPreview	OldCreateOrderPositionpoMainFormCenterOnClose	FormClose	OnKeyDownFormKeyDownPixelsPerInch`
TextHeight TsMemoMLeft TopWidth�Height� BorderStylebsNoneColor��� Font.CharsetANSI_CHARSET
Font.Color+5M Font.Height�	Font.NameCourier New
Font.Style 
ParentFontReadOnly	
ScrollBarsssBothTabOrderVisibleBoundLabel.Indent BoundLabel.Font.CharsetDEFAULT_CHARSETBoundLabel.Font.ColorclWindowTextBoundLabel.Font.Height�BoundLabel.Font.NameMS Sans SerifBoundLabel.Font.Style BoundLabel.LayoutsclLeftBoundLabel.MaxWidth BoundLabel.UseSkinColor	SkinData.SkinSectionHINT  	TwwDBGridGridAtosLeft Top?Width�HeightlControlType.StringsCheck;CheckBox;S;N Selected.StringsCheck	3	*	FProtocolo	13	Protocolo	F   NUMERO_TITULO	14	Nº Título	TAPRESENTANTE	30	Apresentante	FDEVEDOR	30	Devedor	F   STATUS	16	Situação	TEXPORTADO	11	Exportado	F IniAttributes.Delimiter;;
TitleColor	clBtnFace	FixedColsShowHorzScrollBar	EditControlOptionsecoCheckboxSingleClickecoSearchOwnerForm AlignalClientBorderStylebsNone
DataSource	dsTitulosEditCalculated	Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
KeyOptions OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit
dgWordWrapdgProportionalColResize 
ParentFont	PopupMenuPMTabOrderTitleAlignmenttaCenterTitleFont.CharsetANSI_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameTahomaTitleFont.Style 
TitleLinesTitleButtons	
UseTFieldsOnMouseDownGridAtosMouseDownOnMouseMoveGridAtosMouseMove  TsPanelP1Left Top Width�Height?AlignalTopTabOrder SkinData.SkinSectionPANEL TsBevelsBevel1LeftTopWidthHeight0  TsRadioGroupRgTipoLeftTop�Width� Height8Caption     Tipo de Arquivo      ParentBackgroundTabOrderVisibleCaptionLayoutclTopCenterSkinData.SkinSectionHINTCaptionSkinHINT	ItemIndex Items.StringsBancoDistribuidor   TsBitBtnbtVisualizarLeft9TopWidthMHeight0CursorcrHandPointCaption
VisualizarTabOrderOnClickbtVisualizarClickLayout
blGlyphTopSkinData.SkinSectionBUTTON
ImageIndexImages
dm.Imagens	Reflected	  TsBitBtnbtExportar2Left�Top�WidthMHeight0CursorcrHandPointCaptionExportarTabOrderVisibleOnClickbtExportar2ClickLayout
blGlyphTopSkinData.SkinSectionBUTTON
ImageIndexImages
dm.Imagens	Reflected	  
TsDateEditedInicioLeft3TopWidth_HeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontTabOrder BoundLabel.Active	BoundLabel.Caption   InícioBoundLabel.Indent BoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style BoundLabel.LayoutsclLeftBoundLabel.MaxWidth BoundLabel.UseSkinColor	SkinData.SkinSectionEDITGlyphMode.Blend GlyphMode.GrayedDefaultToday	  
TsDateEditedFimLeft� TopWidth_HeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontTabOrderBoundLabel.Active	BoundLabel.CaptionFimBoundLabel.Indent BoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style BoundLabel.LayoutsclLeftBoundLabel.MaxWidth BoundLabel.UseSkinColor	SkinData.SkinSectionEDITGlyphMode.Blend GlyphMode.GrayedDefaultToday	  TsBitBtn
btRejeitarLeft�TopWidthYHeight0CursorcrHandPointCaptionIrregularidadeFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderOnClickbtRejeitarClickLayout
blGlyphTopSkinData.SkinSectionBUTTON
ImageIndex	Images
dm.Imagens	Reflected	  
TsDateEditedDataLeftTopWidthaHeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontReadOnly	TabOrderBoundLabel.Active	BoundLabel.CaptionDataBoundLabel.Indent BoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style BoundLabel.LayoutsclLeftBoundLabel.MaxWidth BoundLabel.UseSkinColor	SkinData.SkinSectionEDITGlyphMode.Blend GlyphMode.GrayedDefaultToday	  TsBitBtn
btExportarLeft�TopWidthMHeight0CursorcrHandPointCaptionExportarTabOrderOnClickbtExportarClickLayout
blGlyphTopSkinData.SkinSectionBUTTON
ImageIndexImages
dm.Imagens	Reflected	   TsStatusBarBarraLeft Top�Width�HeightPanelsTextEncontradosWidthP Width2  SkinData.SkinSection	STATUSBAR  TDataSourcedsPortadoresDataSetdm.PortadoresLefthTop  TSQLDataSet
dtsTitulosCommandText[  select * from TITULOS

where 

(DT_REGISTRO BETWEEN :D1 AND :D2 OR
DT_PAGAMENTO BETWEEN :D1 AND :D2 OR
DT_RETIRADO BETWEEN :D1 AND :D2 OR
DT_SUSTADO BETWEEN :D1 AND :D2 OR
DT_DEVOLVIDO BETWEEN :D1 AND :D2) and

(STATUS='PROTESTADO' OR STATUS='PAGO' OR
STATUS='RETIRADO' OR STATUS='SUSTADO' OR 
STATUS='DEVOLVIDO')

order by PROTOCOLOMaxBlobSize�ParamsDataTypeftDateNameD1	ParamTypeptInput DataTypeftDateNameD2	ParamTypeptInput DataTypeftDateNameD1	ParamTypeptInput DataTypeftDateNameD2	ParamTypeptInput DataTypeftDateNameD1	ParamTypeptInput DataTypeftDateNameD2	ParamTypeptInput DataTypeftDateNameD1	ParamTypeptInput DataTypeftDateNameD2	ParamTypeptInput DataTypeftDateNameD1	ParamTypeptInput DataTypeftDateNameD2	ParamTypeptInput  SQLConnectiondm.ProtestoLeft� Top�   TDataSetProvider
dspTitulosDataSet
dtsTitulosLeft� Top  TClientDataSetTitulos
Aggregates ParamsDataTypeftDateNameD1	ParamTypeptInput DataTypeftDateNameD2	ParamTypeptInput DataTypeftDateNameD1	ParamTypeptInput DataTypeftDateNameD2	ParamTypeptInput DataTypeftDateNameD1	ParamTypeptInput DataTypeftDateNameD2	ParamTypeptInput DataTypeftDateNameD1	ParamTypeptInput DataTypeftDateNameD2	ParamTypeptInput DataTypeftDateNameD1	ParamTypeptInput DataTypeftDateNameD2	ParamTypeptInput  ProviderName
dspTitulosLeft� Top8 TStringFieldTitulosCheck	FieldKindfkInternalCalc	FieldNameCheckSize  TIntegerFieldTitulosID_ATO	FieldNameID_ATORequired	  TIntegerFieldTitulosCODIGO	FieldNameCODIGO  TIntegerFieldTitulosRECIBO	AlignmenttaLeftJustify	FieldNameRECIBO  
TDateFieldTitulosDT_PROTOCOLO	FieldNameDT_PROTOCOLO  TIntegerFieldTitulosPROTOCOLO	AlignmenttaLeftJustify	FieldName	PROTOCOLO  TIntegerFieldTitulosLIVRO_PROTOCOLO	AlignmenttaLeftJustify	FieldNameLIVRO_PROTOCOLO  TStringFieldTitulosFOLHA_PROTOCOLO	FieldNameFOLHA_PROTOCOLOSize
  
TDateFieldTitulosDT_PRAZO	FieldNameDT_PRAZO  
TDateFieldTitulosDT_REGISTRO	FieldNameDT_REGISTRO  TIntegerFieldTitulosREGISTRO	FieldNameREGISTRO  TIntegerFieldTitulosLIVRO_REGISTRO	FieldNameLIVRO_REGISTRO  TStringFieldTitulosFOLHA_REGISTRO	FieldNameFOLHA_REGISTROSize
  TStringFieldTitulosSELO_REGISTRO	FieldNameSELO_REGISTROSize  
TDateFieldTitulosDT_PAGAMENTO	FieldNameDT_PAGAMENTO  TStringFieldTitulosSELO_PAGAMENTO	FieldNameSELO_PAGAMENTOSize  TIntegerFieldTitulosRECIBO_PAGAMENTO	FieldNameRECIBO_PAGAMENTO  TStringFieldTitulosCOBRANCA	FieldNameCOBRANCA	FixedChar	Size  TFloatFieldTitulosEMOLUMENTOS	FieldNameEMOLUMENTOSDisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosFETJ	FieldNameFETJDisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosFUNDPERJ	FieldNameFUNDPERJDisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosFUNPERJ	FieldNameFUNPERJDisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosMUTUA	FieldNameMUTUADisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosDISTRIBUICAO	FieldNameDISTRIBUICAODisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosACOTERJ	FieldNameACOTERJDisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosTOTAL	FieldNameTOTALDisplayFormat	#####0.00
EditFormat	#####0.00  TIntegerFieldTitulosTIPO_PROTESTO	FieldNameTIPO_PROTESTO  TIntegerFieldTitulosTIPO_TITULO	FieldNameTIPO_TITULO  TStringFieldTitulosNUMERO_TITULO	FieldNameNUMERO_TITULO  
TDateFieldTitulosDT_TITULO	FieldName	DT_TITULO  TStringFieldTitulosBANCO	FieldNameBANCO  TFloatFieldTitulosVALOR_TITULO	FieldNameVALOR_TITULODisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosSALDO_PROTESTO	FieldNameSALDO_PROTESTODisplayFormat	#####0.00
EditFormat	#####0.00  
TDateFieldTitulosDT_VENCIMENTO	FieldNameDT_VENCIMENTO  TStringFieldTitulosCONVENIO	FieldNameCONVENIO	FixedChar	Size  TStringFieldTitulosTIPO_APRESENTACAO	FieldNameTIPO_APRESENTACAO	FixedChar	Size  
TDateFieldTitulosDT_ENVIO	FieldNameDT_ENVIO  TStringFieldTitulosTIPO_INTIMACAO	FieldNameTIPO_INTIMACAO	FixedChar	Size  
TMemoFieldTitulosMOTIVO_INTIMACAO	FieldNameMOTIVO_INTIMACAOBlobTypeftMemo  
TDateFieldTitulosDT_INTIMACAO	FieldNameDT_INTIMACAO  
TDateFieldTitulosDT_PUBLICACAO	FieldNameDT_PUBLICACAO  TFloatFieldTitulosVALOR_PAGAMENTO	FieldNameVALOR_PAGAMENTO  TStringFieldTitulosAPRESENTANTE	FieldNameAPRESENTANTESized  TStringFieldTitulosCPF_CNPJ_APRESENTANTE	FieldNameCPF_CNPJ_APRESENTANTESize  TStringFieldTitulosTIPO_APRESENTANTE	FieldNameTIPO_APRESENTANTE	FixedChar	Size  TStringFieldTitulosCEDENTE	FieldNameCEDENTESized  TStringFieldTitulosNOSSO_NUMERO	FieldNameNOSSO_NUMEROSize  TStringFieldTitulosSACADOR	FieldNameSACADORSized  TStringFieldTitulosDEVEDOR	FieldNameDEVEDORSized  TStringFieldTitulosCPF_CNPJ_DEVEDOR	FieldNameCPF_CNPJ_DEVEDORSize  TStringFieldTitulosTIPO_DEVEDOR	FieldNameTIPO_DEVEDOR	FixedChar	Size  TStringFieldTitulosAGENCIA_CEDENTE	FieldNameAGENCIA_CEDENTESize  TStringFieldTitulosPRACA_PROTESTO	FieldNamePRACA_PROTESTO  TStringFieldTitulosTIPO_ENDOSSO	FieldNameTIPO_ENDOSSO	FixedChar	Size  TStringFieldTitulosACEITE	FieldNameACEITE	FixedChar	Size  TStringFieldTitulosCPF_ESCREVENTE	FieldNameCPF_ESCREVENTESize  TStringFieldTitulosCPF_ESCREVENTE_PG	FieldNameCPF_ESCREVENTE_PGSize  
TMemoFieldTitulosOBSERVACAO	FieldName
OBSERVACAOBlobTypeftMemo  
TDateFieldTitulosDT_SUSTADO	FieldName
DT_SUSTADO  
TDateFieldTitulosDT_RETIRADO	FieldNameDT_RETIRADO  TStringFieldTitulosSTATUS	AlignmenttaCenter	FieldNameSTATUS  TStringFieldTitulosPROTESTADO	FieldName
PROTESTADO	FixedChar	Size  TStringFieldTitulosENVIADO_PROTESTO	FieldNameENVIADO_PROTESTO	FixedChar	Size  TStringFieldTitulosENVIADO_PAGAMENTO	FieldNameENVIADO_PAGAMENTO	FixedChar	Size  TStringFieldTitulosEXPORTADO	AlignmenttaCenter	FieldName	EXPORTADO	FixedChar	Size  
TDateFieldTitulosDT_ENTRADA	FieldName
DT_ENTRADA  TStringFieldTitulosAGENCIA	FieldNameAGENCIASize
  TStringFieldTitulosCONTA	FieldNameCONTASize
  TStringFieldTitulosAVALISTA_DEVEDOR	FieldNameAVALISTA_DEVEDOR	FixedChar	Size  TStringFieldTitulosFINS_FALIMENTARES	FieldNameFINS_FALIMENTARES	FixedChar	Size  TFloatFieldTitulosTARIFA_BANCARIA	FieldNameTARIFA_BANCARIA  TStringFieldTitulosFORMA_PAGAMENTO	FieldNameFORMA_PAGAMENTO	FixedChar	Size  TStringFieldTitulosNUMERO_PAGAMENTO	FieldNameNUMERO_PAGAMENTOSize(  TStringFieldTitulosARQUIVO	FieldNameARQUIVO  TStringFieldTitulosRETORNO	FieldNameRETORNO  
TDateFieldTitulosDT_DEVOLVIDO	FieldNameDT_DEVOLVIDO  TStringFieldTitulosCPF_CNPJ_SACADOR	FieldNameCPF_CNPJ_SACADORSize  TIntegerFieldTitulosID_MSG	FieldNameID_MSG  TFloatFieldTitulosVALOR_AR	FieldNameVALOR_AR  TStringFieldTitulosELETRONICO	FieldName
ELETRONICO	FixedChar	Size  TIntegerFieldTitulosIRREGULARIDADE	FieldNameIRREGULARIDADE  TStringFieldTitulosCODIGO_APRESENTANTE	FieldNameCODIGO_APRESENTANTESize
  TStringFieldTitulosENVIADO_APONTAMENTO	FieldNameENVIADO_APONTAMENTO	FixedChar	Size  TStringFieldTitulosENVIADO_RETIRADO	FieldNameENVIADO_RETIRADO	FixedChar	Size  TStringFieldTitulosENVIADO_SUSTADO	FieldNameENVIADO_SUSTADO	FixedChar	Size  TStringFieldTitulosENVIADO_DEVOLVIDO	FieldNameENVIADO_DEVOLVIDO	FixedChar	Size  TStringFieldTitulosAVISTA	FieldNameAVISTA	FixedChar	Size  TFloatFieldTitulosSALDO_TITULO	FieldNameSALDO_TITULO  TStringFieldTitulosTIPO_SUSTACAO	FieldNameTIPO_SUSTACAO	FixedChar	Size  TStringFieldTitulosRETORNO_PROTESTO	FieldNameRETORNO_PROTESTO	FixedChar	Size  
TDateFieldTitulosDT_RETORNO_PROTESTO	FieldNameDT_RETORNO_PROTESTO  
TDateFieldTitulosDT_DEFINITIVA	FieldNameDT_DEFINITIVA  TStringFieldTitulosJUDICIAL	FieldNameJUDICIAL	FixedChar	Size  TFloatFieldTitulosFUNARPEN	FieldNameFUNARPEN  TFloatFieldTitulosPMCMV	FieldNamePMCMV  TStringFieldTitulosALEATORIO_PROTESTO	FieldNameALEATORIO_PROTESTOSize  TStringFieldTitulosALEATORIO_SOLUCAO	FieldNameALEATORIO_SOLUCAOSize  TStringField
TitulosCCT	FieldNameCCTSize	  TStringFieldTitulosDETERMINACAO	FieldNameDETERMINACAOSized  TStringFieldTitulosANTIGO	FieldNameANTIGO	FixedChar	Size   TDataSource	dsTitulosDataSetTitulosLeft� Toph  TDataSourcedsParametrosDataSetdm.ParametrosLefthTop8  
TPopupMenuPMLefthToph 	TMenuItemMarcarTodos1CaptionMarcar TodosOnClickMarcarTodos1Click  	TMenuItemDesmarcarTodos1CaptionDesmarcar TodosOnClickDesmarcarTodos1Click   TDataSource
dsRemessasDataSetdm.RemessasLefthTop�   TRxMemoryDataRX	FieldDefs Left� Top�  TStringField
RXPortador	FieldNamePortadorSized  TStringFieldRXDocumento	FieldName	DocumentoSize  TStringFieldRXCodigo	FieldNameCodigoSize    