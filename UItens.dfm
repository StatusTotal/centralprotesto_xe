object FItens: TFItens
  Left = 251
  Top = 201
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Itens'
  ClientHeight = 365
  ClientWidth = 687
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object P2: TsPanel
    Left = 0
    Top = 198
    Width = 687
    Height = 167
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'SCROLLBAR2V'
    object Grid2: TwwDBGrid
      Left = 13
      Top = 10
      Width = 661
      Height = 107
      Selected.Strings = (
        'TAB'#9'6'#9'Tabela'#9'F'
        'ITEM'#9'7'#9'Item'#9'F'
        'SUB'#9'7'#9'Subitem'#9'F'
        'DESCR'#9'57'#9'Descri'#231#227'o'#9'F'
        'VALOR'#9'7'#9'Valor'#9'F'
        'QTD'#9'6'#9'Qtd.'#9'F'
        'TOTAL'#9'8'#9'Total'#9'F')
      IniAttributes.Delimiter = ';;'
      IniAttributes.UnicodeIniFile = False
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      DataSource = dsItens
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
      ReadOnly = True
      TabOrder = 10
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      TitleLines = 1
      TitleButtons = False
      UseTFields = False
      OnDblClick = Grid2DblClick
    end
    object edEmolumentos: TsDBEdit
      Left = 13
      Top = 137
      Width = 65
      Height = 21
      AutoSize = False
      Color = clWhite
      DataField = 'EMOL'
      DataSource = dsCodigos
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Emolumentos'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edFetj: TsDBEdit
      Left = 88
      Top = 137
      Width = 65
      Height = 21
      AutoSize = False
      Color = clWhite
      DataField = 'FETJ'
      DataSource = dsCodigos
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'FETJ'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edFundperj: TsDBEdit
      Left = 163
      Top = 137
      Width = 47
      Height = 21
      AutoSize = False
      Color = clWhite
      DataField = 'FUND'
      DataSource = dsCodigos
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Fundperj'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edFunperj: TsDBEdit
      Left = 220
      Top = 137
      Width = 47
      Height = 21
      AutoSize = False
      Color = clWhite
      DataField = 'FUNP'
      DataSource = dsCodigos
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Funperj'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edMutua: TsDBEdit
      Left = 451
      Top = 137
      Width = 42
      Height = 21
      AutoSize = False
      Color = clWhite
      DataField = 'MUTUA'
      DataSource = dsCodigos
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'M'#250'tua'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edAcoterj: TsDBEdit
      Left = 500
      Top = 137
      Width = 41
      Height = 21
      AutoSize = False
      Color = clWhite
      DataField = 'ACOTERJ'
      DataSource = dsCodigos
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Acoterj'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edDistribuicao: TsDBEdit
      Left = 548
      Top = 137
      Width = 55
      Height = 21
      AutoSize = False
      Color = clWhite
      DataField = 'DISTRIB'
      DataSource = dsCodigos
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Distribui'#231#227'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edFunarpen: TsDBEdit
      Left = 277
      Top = 137
      Width = 47
      Height = 21
      AutoSize = False
      Color = clWhite
      DataField = 'FUNA'
      DataSource = dsCodigos
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Funarpen'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edPMCMV: TsDBEdit
      Left = 334
      Top = 137
      Width = 47
      Height = 21
      AutoSize = False
      Color = clWhite
      DataField = 'PMCMV'
      DataSource = dsCodigos
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Pmcmv'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edTotal: TsDBEdit
      Left = 617
      Top = 137
      Width = 57
      Height = 21
      AutoSize = False
      Color = clWhite
      DataField = 'TOT'
      DataSource = dsCodigos
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Total'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edIss: TsDBEdit
      Left = 391
      Top = 137
      Width = 47
      Height = 21
      AutoSize = False
      Color = clWhite
      DataField = 'ISS'
      DataSource = dsCodigos
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Iss'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
  end
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 687
    Height = 198
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sLabel1: TsLabel
      Left = 168
      Top = 24
      Width = 295
      Height = 13
      Caption = ' Informe o n'#250'mero da tabela e o '#237'tem desejado para localizar.'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object ImAviso: TImage
      Left = 150
      Top = 21
      Width = 15
      Height = 16
      Picture.Data = {
        07544269746D617076010000424D760100000000000076000000280000002000
        000010000000010004000000000000010000120B0000120B0000100000000000
        0000000000000000800000800000008080008000000080008000808000007F7F
        7F00BFBFBF000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
        FF00333333303333333333333337FF3333333333330003333333333333777F33
        33333333330803333333333F33777FF33F3333B33B000B33B3333373F777773F
        7333333BBB0B0BBB33333337737F7F77F333333BBB0F0BBB33333337337373F7
        3F3333BBB0F7F0BBB333337F3737F73F7F3333BB0FB7BF0BB3333F737F37F37F
        73FFBBBB0BF7FB0BBBB3773F7F37337F377333BB0FBFBF0BB333337F73F33373
        7F3333BBB0FBF0BBB3333373F73FF7337333333BBB000BBB33333337FF777337
        F333333BBBBBBBBB3333333773FF3F773F3333B33BBBBB33B333337337737733
        73333333333B3333333333333337F33333333333333B33333333333333373333
        3333}
      Transparent = True
    end
    object Grid1: TwwDBGrid
      Left = 13
      Top = 45
      Width = 661
      Height = 107
      Selected.Strings = (
        'TAB'#9'6'#9'Tabela'#9'F'
        'ITEM'#9'7'#9'Item'#9'F'
        'SUB'#9'7'#9'Subitem'#9'F'
        'DESCR'#9'72'#9'Descri'#231#227'o'#9'F'
        'VALOR'#9'8'#9'Valor'#9'F')
      IniAttributes.Delimiter = ';;'
      IniAttributes.UnicodeIniFile = False
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      DataSource = dsTabela
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
      ReadOnly = True
      TabOrder = 4
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      TitleLines = 1
      TitleButtons = False
      UseTFields = False
      OnDblClick = Grid1DblClick
    end
    object edTabela: TsEdit
      Left = 13
      Top = 19
      Width = 54
      Height = 21
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnChange = edTabelaChange
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Tabela'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edItem: TsEdit
      Left = 73
      Top = 19
      Width = 54
      Height = 21
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnChange = edItemChange
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Item'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edVlr: TsDBEdit
      Left = 85
      Top = 168
      Width = 65
      Height = 21
      Color = clWhite
      DataField = 'VALOR'
      DataSource = dsTabela
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Valor'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object spQtd: TsSpinEdit
      Left = 13
      Top = 168
      Width = 65
      Height = 21
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      Text = '1'
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Qtd.'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      MaxValue = 10000
      MinValue = 1
      Value = 1
    end
    object btAdicionar: TsBitBtn
      Left = 157
      Top = 161
      Width = 84
      Height = 28
      Cursor = crHandPoint
      Caption = 'Adicionar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        E6000000424DE60000000000000076000000280000000E0000000E0000000100
        04000000000070000000120B0000120B00001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7700777777707777770077777701077777007777701910777700777701919107
        7700777000191000770077777091907777007777701910777700777770919077
        7700777770191077770077777091907777007777700000777700777777777777
        77007777777777777700}
      ParentFont = False
      TabOrder = 5
      OnClick = btAdicionarClick
      SkinData.SkinSection = 'BUTTON'
    end
    object btRemover: TsBitBtn
      Left = 248
      Top = 161
      Width = 84
      Height = 28
      Cursor = crHandPoint
      Caption = 'Remover'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        E6000000424DE60000000000000076000000280000000E0000000E0000000100
        04000000000070000000120B0000120B00001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7700777777777777770077777777777777007777777707777700777777770077
        7700770000000107770077091919191077007701919191910700770919191910
        7700770000000107770077777777007777007777777707777700777777777777
        77007777777777777700}
      ParentFont = False
      TabOrder = 6
      OnClick = btRemoverClick
      SkinData.SkinSection = 'BUTTON'
    end
    object btConcluir: TsBitBtn
      Left = 590
      Top = 161
      Width = 84
      Height = 28
      Cursor = crHandPoint
      Caption = 'Concluir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      OnClick = btConcluirClick
      ImageIndex = 3
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object edCodigo: TsEdit
      Left = 486
      Top = 161
      Width = 58
      Height = 27
      AutoSize = False
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnKeyPress = edCodigoKeyPress
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Copiar de'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
    end
    object btOk: TsBitBtn
      Left = 550
      Top = 161
      Width = 33
      Height = 28
      Cursor = crHandPoint
      TabOrder = 9
      OnClick = btOkClick
      ImageIndex = 0
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object btCalcular: TsBitBtn
      Left = 339
      Top = 161
      Width = 92
      Height = 28
      Cursor = crHandPoint
      Hint = 'CALCULAR'
      Caption = 'F5 - Calcular'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 10
      OnClick = btCalcularClick
      ImageIndex = 8
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object P3: TsPanel
    Left = 278
    Top = 194
    Width = 126
    Height = 111
    ParentBackground = False
    TabOrder = 2
    Visible = False
    SkinData.SkinSection = 'MAINMENU'
    object sbOk: TsSpeedButton
      Left = 68
      Top = 83
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000939393F9939393F9939393F9939393F9939393F9939393F9939393F99393
        93F9939393F9939393F9939393F9939393F90000000000000000000000000000
        0000939393F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF939393F90000000000000000000000000000
        0000939393F900750CF500750CF5EEEEEEFFEDEDEDFFECECECFFEBEBEBFFEAEA
        EAFFE9E9E9FFE8E8E8FFFFFFFFFF939393F90000000000000000000000000000
        000000750CF5D3FFE9FF8EFDB4FF00750CF5EFEFEFFFEEEEEEFFEDEDEDFFECEC
        ECFFEBEBEBFFEAEAEAFFFFFFFFFF939393F90000000000000000000000000075
        0CF5D3FFE9FF41EF7DFF58EF8BFF8EF9B2FF00750CF5F0F0F0FFEFEFEFFFEEEE
        EEFFEDEDEDFFECECECFFFFFFFFFF939393F9000000000000000000750CF5D3FF
        E9FF7FFFAEFF76BD8CFF00750CF56FF79EFF84F2A9FF00750CF5F1F1F1FFF0F0
        F0FFEFEFEFFFEEEEEEFFFFFFFFFF939393F9000000000000000050795D350075
        0CF5A7DBB7FF00750CF5F6F6F6FF00750CF588FDB0FF8BF4AFFF00750CF5F1F1
        F1FFF1F1F1FFF0F0F0FFFFFFFFFF939393F90000000000000000000000000014
        003800750CF5FFFFFFFFF8F8F8FFF2F2F2FF00750CF5D3FFE9FF00750CF5E7EC
        E8FFF2F2F2FFF1F1F1FFFFFFFFFF939393F90000000000000000000000000000
        0000939393F9FFFFFFFFFAFAFAFFF9F9F9FFF2F2F2FF00750CF5C5C5C5FFF5F5
        F5FFF4F4F4FFF3F3F3FFF5F5F5FF939393F90000000000000000000000000000
        0000939393F9FFFFFFFFFBFBFBFFFAFAFAFFFAFAFAFFF9F9F9FFF8F8F8FF9393
        93F9939393F9939393F9939393F9939393F90000000000000000000000000000
        0000939393F9FFFFFFFFFDFDFDFFFCFCFCFFFBFBFBFFFAFAFAFFFAFAFAFF9393
        93F9E1E1E1FFE1E1E1FFB5B5B5F9939393F90000000000000000000000000000
        0000939393F9FFFFFFFFFFFFFFFFFEFEFEFFFDFDFDFFFCFCFCFFFBFBFBFF9393
        93F9E1E1E1FFB5B5B5F9939393F9000000000000000000000000000000000000
        0000939393F9FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFEFEFEFFFDFDFDFF9393
        93F9B5B5B5F9939393F900000000000000000000000000000000000000000000
        0000939393F9939393F9939393F9939393F9939393F9939393F9939393F99393
        93F9939393F90000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
      OnClick = sbOkClick
      SkinData.SkinSection = 'COLHEADER'
    end
    object sbCancelar: TsSpeedButton
      Left = 92
      Top = 83
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Glyph.Data = {
        42010000424D4201000000000000760000002800000011000000110000000100
        040000000000CC00000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777700000007777777777777777700000007777777777778187700000007000
        0000007711177000000070FFFFFFF07781877000000070F88888F07777777000
        000070F8FFF8F07771777000000070F88888F07781877000000070FFFFFFF077
        11177000000070F88777F07711177000000070FFFF00007711177000000070F8
        8707077711177000000070FFFF00777711177000000070000007777711177000
        0000777777777777717770000000777777777777777770000000777777777777
        777770000000}
      OnClick = sbCancelarClick
      SkinData.SkinSection = 'COLHEADER'
    end
    object edQuantidade: TsDBEdit
      Left = 12
      Top = 17
      Width = 102
      Height = 21
      Color = clWhite
      DataField = 'QTD'
      DataSource = dsItens
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Quantidade'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edValor: TsDBEdit
      Left = 12
      Top = 57
      Width = 102
      Height = 21
      Color = clWhite
      DataField = 'VALOR'
      DataSource = dsItens
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Valor'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
  end
  object dsTabela: TDataSource
    DataSet = dm.Tabela
    Left = 436
    Top = 88
  end
  object dsItens: TDataSource
    DataSet = dm.Itens
    Left = 483
    Top = 88
  end
  object dsCodigos: TDataSource
    DataSet = dm.Codigos
    Left = 532
    Top = 88
  end
  object qryCopiar: TFDQuery
    Connection = dm.conCENTRAL
    SQL.Strings = (
      'select * from TABCUS'
      ''
      'where COD=:COD')
    Left = 384
    Top = 88
    ParamData = <
      item
        Name = 'COD'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryCopiarID_CUS: TIntegerField
      FieldName = 'ID_CUS'
      Origin = 'ID_CUS'
    end
    object qryCopiarCOD: TIntegerField
      FieldName = 'COD'
      Origin = 'COD'
    end
    object qryCopiarTAB: TStringField
      FieldName = 'TAB'
      Origin = 'TAB'
    end
    object qryCopiarITEM: TStringField
      FieldName = 'ITEM'
      Origin = 'ITEM'
    end
    object qryCopiarSUB: TStringField
      FieldName = 'SUB'
      Origin = 'SUB'
    end
    object qryCopiarDESCR: TStringField
      FieldName = 'DESCR'
      Origin = 'DESCR'
      Size = 100
    end
    object qryCopiarVALOR: TFloatField
      FieldName = 'VALOR'
      Origin = 'VALOR'
    end
    object qryCopiarQTD: TStringField
      FieldName = 'QTD'
      Origin = 'QTD'
      Size = 10
    end
    object qryCopiarTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
    end
    object qryCopiarCOMP: TStringField
      FieldName = 'COMP'
      Origin = 'COMP'
      Size = 50
    end
    object qryCopiarEXC: TStringField
      FieldName = 'EXC'
      Origin = 'EXC'
      Size = 50
    end
  end
end
