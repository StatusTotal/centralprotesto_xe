unit UConsVerbal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Buttons, sBitBtn, Mask, ExtCtrls, Grids,
  DBGrids, sPanel, acDBGrid, sMaskEdit, sCustomComboEdit, sToolEdit,
  sDBDateEdit;

type
  TFConsVerbal = class(TForm)
    btIncluir: TsBitBtn;
    btAlterar: TsBitBtn;
    btExcluir: TsBitBtn;
    dsVerbal: TDataSource;
    P1: TsPanel;
    btLocalizar: TsBitBtn;
    Grid: TsDBGrid;
    edData: TsDateEdit;
    procedure btIncluirClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GridDblClick(Sender: TObject);
    procedure btLocalizarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FConsVerbal: TFConsVerbal;

implementation

uses UGDM,UDM,UGeral,UInformacaoVerbal;

{$R *.dfm}

procedure TFConsVerbal.btIncluirClick(Sender: TObject);
begin
  (**if not Gdm.QtdCCTDisponivel(1) then
  begin
      GR.Aviso('CCT N�O DISPON�VEL!');
      Exit;
  end;

  dm.qryVerbal.Append;
  dm.qryVerbalDATA.AsDateTime:=Now;

  dm.Codigos.Close;
  dm.Codigos.Params.ParamByName('OCULTO1').AsString:='N';
  dm.Codigos.Params.ParamByName('OCULTO2').AsString:='N';
  dm.Codigos.Open;
  if dm.Codigos.Locate('COD',4025,[]) then
  begin
      dm.qryVerbalEMOLUMENTOS.AsFloat  :=dm.CodigosEMOL.AsFloat;
      dm.qryVerbalFETJ.AsFloat         :=dm.CodigosFETJ.AsFloat;
      dm.qryVerbalFUNDPERJ.AsFloat     :=dm.CodigosFUND.AsFloat;
      dm.qryVerbalFUNPERJ.AsFloat      :=dm.CodigosFUNP.AsFloat;
      dm.qryVerbalFUNARPEN.AsFloat     :=dm.CodigosFUNA.AsFloat;
      dm.qryVerbalTOTAL.AsFloat        :=dm.CodigosTOT.AsFloat;
  end;
  GR.CriarForm(TFInformacaoVerbal,FInformacaoVerbal);  **)
end;

procedure TFConsVerbal.btAlterarClick(Sender: TObject);
begin
  if dm.qryVerbal.IsEmpty then Exit;
  dm.qryVerbal.Edit;
  GR.CriarForm(TFInformacaoVerbal,FInformacaoVerbal);
end;

procedure TFConsVerbal.btExcluirClick(Sender: TObject);
var
  vCCT: String;
begin
  if dm.qryVerbal.IsEmpty then Exit;
  if GR.Pergunta('CONFIRMA A EXCLUS�O') then
  begin
      vCCT:=dm.qryVerbalCCT.AsString;
      dm.qryVerbal.Delete;
      dm.qryVerbal.ApplyUpdates(0);
      Gdm.LiberarSelo('NOTAS',vCCT,False);
  end;
end;

procedure TFConsVerbal.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFConsVerbal.GridDblClick(Sender: TObject);
begin
  btAlterar.Click;
end;

procedure TFConsVerbal.btLocalizarClick(Sender: TObject);
begin
  dm.qryVerbal.Close;
  dm.qryVerbal.Params.ParamByName('DT').AsDate:=edData.Date;
  dm.qryVerbal.Params.ParamByName('EN').AsString:='*';
  dm.qryVerbal.Open;
end;

procedure TFConsVerbal.FormCreate(Sender: TObject);
begin
  edData.Date:=Now;
  btLocalizar.Click;
end;

end.
