�
 TFRELPAGAMENTO 01  TPF0TFRelPagamentoFRelPagamentoLeftpTop
BorderIconsbiSystemMenu BorderStylebsDialogCaption   Certidão de PagamentoClientHeight� ClientWidth'Color	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
KeyPreview	OldCreateOrderPositionpoMainFormCenter	OnKeyDownFormKeyDownOnShowFormShowPixelsPerInch`
TextHeight TsPanelP1Left Top Width'Height� AlignalTopTabOrder SkinData.SkinSectionPANEL 
TsGroupBoxGbLeftTop0WidthHeight� Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderSkinData.SkinSectionGROUPBOX TsEdit	edInicialLeftUTop WidthyHeightColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.StylefsBold 
ParentFontTabOrder OnChangeedInicialChange
OnKeyPressedInicialKeyPressSkinData.SkinSectionEDITBoundLabel.Active	BoundLabel.ParentFontBoundLabel.Caption   InícioBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style   TsEditedFinalLeftUTopXWidthyHeightColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.StylefsBold 
ParentFontTabOrder
OnKeyPressedFinalKeyPressSkinData.SkinSectionEDITBoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionFimBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style   
TsDateEditedInicioLeftUTop WidthyHeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontTabOrderText
  /  /    BoundLabel.Active	BoundLabel.ParentFontBoundLabel.Caption   InícioBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style SkinData.SkinSectionEDITGlyphMode.Blend GlyphMode.GrayedPopupHeight� 
PopupWidth�   
TsDateEditedFimLeftUTopXWidthyHeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontTabOrderText
  /  /    BoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionFimBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style SkinData.SkinSectionEDITGlyphMode.Blend GlyphMode.GrayedPopupHeight� 
PopupWidth�    TsRadioButtonRbProtocoloLeftTopWidthdHeightCaption	PROTOCOLOChecked	Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold 
ParentFontTabOrder TabStop	OnClickRbProtocoloClickSkinData.SkinSectionTRANSPARENT  TsRadioButtonRbDataLeft{TopWidth� HeightCaptionDATA DE PAGAMENTOFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold 
ParentFontTabOrderOnClickRbDataClickSkinData.SkinSectionTRANSPARENT   TsBitBtnbtVisualizarLeft5Top� Width[HeightCursorcrHandPointCaption
VisualizarFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderOnClickbtVisualizarClick
ImageIndexImages
dm.ImagensSkinData.SkinSectionBUTTON  TsBitBtnbtSairLeft� Top� Width[HeightCursorcrHandPointCaptionSairFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderOnClickbtSairClick
ImageIndexImages
dm.ImagensSkinData.SkinSectionBUTTON  TFDQueryqryData
Connectiondm.conSISTEMASQL.Strings select ID_ATO from titulos where    2DT_PAGAMENTO between :D1 and :D2 and STATUS='PAGO' Left� Top@	ParamDataNameD1DataTypeftDate	ParamTypeptInput NameD2DataTypeftDate	ParamTypeptInput   TIntegerFieldqryDataID_ATO	FieldNameID_ATOOriginID_ATOProviderFlags
pfInUpdate	pfInWherepfInKey Required	   TFDQueryqryProtocolo
Connectiondm.conSISTEMASQL.Strings!select ID_ATO from titulos where     /PROTOCOLO between :P1 and :P2 and STATUS='PAGO' Left� Top� 	ParamDataNameP1DataType	ftInteger	ParamTypeptInput NameP2DataType	ftInteger	ParamTypeptInput   TIntegerFieldqryProtocoloID_ATO	FieldNameID_ATOOriginID_ATOProviderFlags
pfInUpdate	pfInWherepfInKey Required	    