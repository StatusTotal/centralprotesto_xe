unit UCadastroMotivos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, sPanel, Grids, Wwdbigrd, Wwdbgrid, StdCtrls,
  Buttons, sBitBtn;

type
  TFCadastroMotivos = class(TForm)
    Grid: TwwDBGrid;
    dsMotivos: TDataSource;
    sPanel1: TsPanel;
    btIncluir: TsBitBtn;
    btAlterar: TsBitBtn;
    btExcluir: TsBitBtn;
    sBitBtn1: TsBitBtn;
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btExcluirClick(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroMotivos: TFCadastroMotivos;

implementation

uses UDM, UPF, UGeral, UMotivo;

{$R *.dfm}

procedure TFCadastroMotivos.sBitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TFCadastroMotivos.FormCreate(Sender: TObject);
begin
  dm.Motivos.Close;
  dm.Motivos.Open;
  dm.Motivos.Filtered:=False;
  dm.Motivos.Filter  :='ID_MOTIVO<>'+QuotedStr('0');
  dm.Motivos.Filtered:=True;
end;

procedure TFCadastroMotivos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm.Motivos.Filtered:=False;
end;

procedure TFCadastroMotivos.btExcluirClick(Sender: TObject);
begin
  if dm.Motivos.IsEmpty then Exit;

  dm.Motivos.Delete;
  dm.Motivos.ApplyUpdates(0);
end;

procedure TFCadastroMotivos.btIncluirClick(Sender: TObject);
begin
  dm.vMotivo:='';
  dm.vOkMotivo:=False;

  GR.CriarForm(TFMotivo,FMotivo);

  if dm.vOkMotivo then
  begin
      dm.Motivos.Append;
      dm.MotivosID_MOTIVO.AsInteger :=dm.IdAtual('ID_MOTIVO','S');
      dm.MotivosDESCRICAO.AsString  :=dm.vMotivo;
      dm.Motivos.Post;
      dm.Motivos.ApplyUpdates(0);
  end;
end;

procedure TFCadastroMotivos.btAlterarClick(Sender: TObject);
begin
  dm.vMotivo:=dm.MotivosDESCRICAO.AsString;
  dm.vOkMotivo:=False;

  GR.CriarForm(TFMotivo,FMotivo);

  if dm.vOkMotivo then
  begin
      dm.Motivos.Edit;
      dm.MotivosDESCRICAO.AsString  :=dm.vMotivo;
      dm.Motivos.Post;
      dm.Motivos.ApplyUpdates(0);
  end;
end;

procedure TFCadastroMotivos.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFCadastroMotivos.GridDblClick(Sender: TObject);
begin
  btAlterarClick(Sender);
end;

end.
