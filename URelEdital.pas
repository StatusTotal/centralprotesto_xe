unit URelEdital;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, ExtCtrls, sPanel, sEdit, sGroupBox,
  QuickRpt, QRCtrls, DB, RxMemDS, FMTBcd, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, sRadioButton, SqlExpr, Grids, Wwdbigrd,
  Wwdbgrid, DBClient, SimpleDS, Menus, DBCtrls, sDBLookupComboBox,
  sDBDateEdit, sLabel, acDBTextFX, Data.DBXFirebird, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFRelEdital = class(TForm)
    P: TsPanel;
    GbProtocolo: TsGroupBox;
    edProtFinal: TsEdit;
    edProtInicial: TsEdit;
    Rb1: TsRadioButton;
    GbData: TsGroupBox;
    Rb2: TsRadioButton;
    edDataInicial: TsDateEdit;
    edDataFinal: TsDateEdit;
    dsProtocolo: TDataSource;
    GridAtos: TwwDBGrid;
    btFiltrar: TsBitBtn;
    btVisualizar: TsBitBtn;
    PM: TPopupMenu;
    Marcartodos1: TMenuItem;
    Desmarcartodos1: TMenuItem;
    Protocolo: TFDQuery;
    ProtocoloCheck: TBooleanField;
    ProtocoloID_ATO: TIntegerField;
    ProtocoloPROTOCOLO: TIntegerField;
    ProtocoloDT_PROTOCOLO: TDateField;
    ProtocoloAPRESENTANTE: TStringField;
    ProtocoloDEVEDOR: TStringField;
    ProtocoloSTATUS: TStringField;
    ProtocoloMOTIVO_INTIMACAO: TMemoField;
    ProtocoloDT_PUBLICACAO: TDateField;
    ProtocoloTIPO_INTIMACAO: TStringField;
    procedure edProtInicialKeyPress(Sender: TObject; var Key: Char);
    procedure edProtFinalKeyPress(Sender: TObject; var Key: Char);
    procedure Rb1Click(Sender: TObject);
    procedure Rb2Click(Sender: TObject);
    procedure btFiltrarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridAtosTitleButtonClick(Sender: TObject;
      AFieldName: String);
    procedure btVisualizarClick(Sender: TObject);
    procedure edDataInicialExit(Sender: TObject);
    procedure Marcartodos1Click(Sender: TObject);
    procedure Desmarcartodos1Click(Sender: TObject);
    procedure GridAtosMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure GridAtosMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelEdital: TFRelEdital;

implementation

uses UDM, UPF, UQuickEdital2, Math, UDadosEdital, UAssinatura, UGeral;

{$R *.dfm}

procedure TFRelEdital.edProtInicialKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFRelEdital.edProtFinalKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFRelEdital.Rb1Click(Sender: TObject);
begin
  GbData.Enabled:=False;
  GbProtocolo.Enabled:=True;
  edDataInicial.Clear;
  edDataFinal.Clear;
  edProtInicial.SetFocus;
end;

procedure TFRelEdital.Rb2Click(Sender: TObject);
begin
  GbData.Enabled:=True;
  GbProtocolo.Enabled:=False;
  edProtInicial.Clear;
  edProtFinal.Clear;
  edDataInicial.SetFocus;
end;

procedure TFRelEdital.btFiltrarClick(Sender: TObject);
begin
  if Rb1.Checked then
  if edProtInicial.Text='' then
  begin
      edProtInicial.SetFocus;
      Exit;
  end;

  if edProtFinal.Text='' then edProtFinal.Text:=edProtInicial.Text;

  Protocolo.Close;
  if Rb1.Checked then
  begin
      Protocolo.SQL.Text:='SELECT ID_ATO,PROTOCOLO,DT_PROTOCOLO,APRESENTANTE,DEVEDOR,STATUS,'+
                             'MOTIVO_INTIMACAO,DT_PUBLICACAO,TIPO_INTIMACAO FROM TITULOS WHERE '+
                             'PROTOCOLO BETWEEN :P1 AND :P2';
      Protocolo.ParamByName('P1').AsInteger:=StrToInt(edProtInicial.Text);
      Protocolo.ParamByName('P2').AsInteger:=StrToInt(edProtFinal.Text);
  end
  else
  begin
      Protocolo.SQL.Text:='SELECT ID_ATO,PROTOCOLO,DT_PROTOCOLO,APRESENTANTE,DEVEDOR,STATUS,'+
                             'MOTIVO_INTIMACAO,DT_PUBLICACAO,TIPO_INTIMACAO FROM TITULOS WHERE '+
                             'DT_PROTOCOLO BETWEEN :D1 AND :D2';
      Protocolo.ParamByName('D1').AsDate:=edDataInicial.Date;
      Protocolo.ParamByName('D2').AsDate:=edDataFinal.Date;
  end;
  Protocolo.Open;
end;

procedure TFRelEdital.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFRelEdital.GridAtosTitleButtonClick(Sender: TObject;
  AFieldName: String);
begin
  Protocolo.IndexFieldNames:=AFieldName;
end;

procedure TFRelEdital.btVisualizarClick(Sender: TObject);
var
  Posicao: Integer;
begin
  if Protocolo.IsEmpty then Exit;

  try
    PF.Aguarde(True);
    Application.CreateForm(TFQuickEdital2,FQuickEdital2);

    with FQuickEdital2 do
    begin
        RX.Close;
        RX.Open;

        Posicao:=Protocolo.RecNo;
        Protocolo.DisableControls;
        Protocolo.First;
        while not Protocolo.Eof do
        begin
            if ProtocoloCheck.Value=True then
            begin
                Protocolo.Edit;
                ProtocoloTIPO_INTIMACAO.AsString:='E';
                ProtocoloSTATUS.AsString        :='INTIMADO EDITAL';
                Application.CreateForm(TFDadosEdital,FDadosEdital);
                with FDadosEdital do
                begin
                    if ProtocoloDT_PUBLICACAO.AsDateTime<>0 then
                    edPublicacao.Date:=ProtocoloDT_PUBLICACAO.AsDateTime;
                    lbProtocolo.Caption:=ProtocoloPROTOCOLO.AsString;
                    if ProtocoloMOTIVO_INTIMACAO.AsString<>'' then
                    lkMotivo.KeyValue:=ProtocoloMOTIVO_INTIMACAO.AsString;
                    ShowModal;
                    if edPublicacao.Date=0 then
                    begin
                        GR.Aviso('Data da publica��o do edital n�o informada!');
                        Protocolo.Cancel;
                        Free;
                        Exit;
                    end;
                    ProtocoloMOTIVO_INTIMACAO.AsString:=dm.MotivosDESCRICAO.AsString;
                    ProtocoloDT_PUBLICACAO.AsDateTime :=edPublicacao.Date;
                    Free;
                end;
                Protocolo.ApplyUpdates(0);
                dm.Motivos.Close;
                dm.Titulos.Close;
                dm.Titulos.Params[0].AsInteger:=ProtocoloID_ATO.AsInteger;
                dm.Titulos.Open;

                RX.Append;
                RXID_ATO.AsInteger          :=dm.TitulosID_ATO.AsInteger;
                RXTitulo.AsString           :=PF.RetornarTitulo(dm.TitulosTIPO_TITULO.AsInteger);
                RXApresentante.AsString     :=dm.TitulosAPRESENTANTE.AsString;
                RXSacador.AsString          :=dm.TitulosSACADOR.AsString;
                RXDevedor.AsString          :=dm.TitulosDEVEDOR.AsString;
                RXValor.AsFloat             :=dm.TitulosVALOR_TITULO.AsFloat;
                RXCustas.AsFloat            :=dm.TitulosTOTAL.AsFloat;
                RXEmol.AsFloat              :=dm.TitulosEMOLUMENTOS.AsFloat;
                RXFetj.AsFloat              :=dm.TitulosFETJ.AsFloat;
                RXFund.AsFloat              :=dm.TitulosFUNDPERJ.AsFloat;
                RXFunp.AsFloat              :=dm.TitulosFUNPERJ.AsFloat;
                RXMutua.AsFloat             :=dm.TitulosMUTUA.AsFloat;
                RXAcoterj.AsFloat           :=dm.TitulosACOTERJ.AsFloat;
                RXDist.AsFloat              :=dm.TitulosDISTRIBUICAO.AsFloat;
                RXTotal.AsFloat             :=dm.TitulosTOTAL.AsFloat;
                RXSaldo.AsFloat             :=dm.TitulosSALDO_PROTESTO.AsFloat;

                if dm.TitulosDT_VENCIMENTO.AsDateTime<>0 then
                RXVencimento.AsDateTime     :=dm.TitulosDT_VENCIMENTO.AsDateTime;
                
                RXProtocolo.AsInteger       :=dm.TitulosPROTOCOLO.AsInteger;
                RXEspecie.AsString          :=dm.TitulosEspecie.AsString;

                if dm.TitulosCPF_CNPJ_DEVEDOR.AsString<>'' then
                  if dm.TitulosTIPO_DEVEDOR.AsString='F' then
                    RXCPF_CNPJ_Devedor.AsString:=PF.FormatarCPF(dm.TitulosCPF_CNPJ_DEVEDOR.AsString)
                      else
                        if dm.TitulosTIPO_DEVEDOR.AsString='J' then
                          RXCPF_CNPJ_Devedor.AsString:=PF.FormatarCNPJ(dm.TitulosCPF_CNPJ_DEVEDOR.AsString);

                RXTitulo.AsString           :=dm.TitulosNUMERO_TITULO.AsString;
                RXDt_Protocolo.AsDateTime   :=dm.TitulosDT_PROTOCOLO.AsDateTime;
                RXDt_Titulo.AsDateTime      :=dm.TitulosDT_TITULO.AsDateTime;

                PF.CarregarDevedor(dm.TitulosID_ATO.AsInteger);
                RXTipo.AsString    :=dm.RxDevedorTIPO.AsString; 
                RXEndereco.AsString:=dm.RxDevedorENDERECO.AsString+' '+dm.RxDevedorCEP.AsString+' '+dm.RxDevedorBAIRRO.AsString+' '+
                                     dm.RxDevedorMUNICIPIO.AsString+' '+dm.RxDevedorUF.AsString;

                RXDt_Intimacao.AsDateTime  :=dm.TitulosDT_INTIMACAO.AsDateTime;
                RXDt_Edital.AsDateTime     :=dm.TitulosDT_PUBLICACAO.AsDateTime;
                RXMotivo.AsString          :=dm.TitulosMOTIVO_INTIMACAO.AsString;
                RX.Post;
            end;
            Protocolo.Next;
        end;
        Protocolo.RecNo:=Posicao;
        Protocolo.EnableControls;

        if not RX.IsEmpty then
        begin
            GR.CriarForm(TFAssinatura,FAssinatura);
            Edital.Preview;
        end
        else
          PF.Aguarde(False);

        Free;
    end;
  except
    on E:Exception do
    begin
        PF.Aguarde(False);
        GR.Aviso('Erro: '+E.Message);
    end;
  end;
end;

procedure TFRelEdital.edDataInicialExit(Sender: TObject);
begin
  if edDataFinal.Date<edDataInicial.Date then
    edDataFinal.Date:=edDataInicial.Date;
end;

procedure TFRelEdital.Marcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=Protocolo.RecNo;
  Protocolo.DisableControls;
  Protocolo.First;
  while not Protocolo.Eof do
  begin
      Protocolo.Edit;
      ProtocoloCheck.AsBoolean:=True;
      Protocolo.Post;
      Protocolo.Next;
  end;
  Protocolo.RecNo:=Posicao;
  Protocolo.EnableControls;
end;

procedure TFRelEdital.Desmarcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=Protocolo.RecNo;
  Protocolo.DisableControls;
  Protocolo.First;
  while not Protocolo.Eof do
  begin
      Protocolo.Edit;
      ProtocoloCheck.AsBoolean:=False;
      Protocolo.Post;
      Protocolo.Next;
  end;
  Protocolo.RecNo:=Posicao;
  Protocolo.EnableControls;
end;

procedure TFRelEdital.GridAtosMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if X in [12..51] then
    Screen.Cursor:=crHandPoint
      else Screen.Cursor:=crDefault;
end;

procedure TFRelEdital.GridAtosMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Screen.Cursor=crHandPoint then
  begin
      if Protocolo.IsEmpty then Exit;
      Protocolo.Edit;
      if ProtocoloCheck.AsBoolean then
        ProtocoloCheck.AsBoolean:=False
          else ProtocoloCheck.AsBoolean:=True;
      Protocolo.Post;
  end;
end;

end.
