�
 TFQUICKLIVROPROTOCOLO1 0��  TPF0TFQuickLivroProtocolo1FQuickLivroProtocolo1Left� TopvCaptionLivro Protocolo 1ClientHeight�ClientWidthColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderScaledOnCreate
FormCreatePixelsPerInch`
TextHeight 	TQuickRepqkEncerramentoLeftTop
WidthHeightcShowingPreviewBeforePrintqkLivroBeforePrintDataSetRXFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA20'' 	OnEndPageqkLivroEndPageOptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.ContinuousPage.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPreviewWidth�PreviewHeight�PrevInitialZoomqrZoomToWidthPreviewDefaultSaveTypestQRPPreviewLeft 
PreviewTop  TQRBand
Cabecalho2Left9Top9Width�HeightJFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomBeforePrintCabecalho2BeforePrintTransparentBandForceNewColumnForceNewPageSize.Values��������@     :�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageHeader TQRLabelqrFolha2Left{Top8Width%HeightSize.Values�������@TUUUUU�	@������*�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionFolha:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabelqrLivro2LeftTop8WidthHeightSize.Values�������@������*�@������*�@������
�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionLivro:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel
qrTitular2Left�TopWidth'HeightSize.Values      ��@      T�@      ��@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionTITULARColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText2Left�TopWidth6HeightSize.Values�������@UUUUUU	�@UUUUUUu�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataSetdm.Serventia	DataFieldENDERECOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabel	qrTitulo2Left�Top+Width� HeightSize.Values��������@������2�@��������@UUUUUUe�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption!L I V R O  D E  P R O T O C O L OColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrServentia2Left'TopWidth[HeightSize.Values XUUUUU�@UUUUUU!�@       �@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionNome ServentiaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	   TQRBandQRBand2Left9Top� Width�Height�AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values�������	@     :�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbTitle TQRLabelQRLabel6Left}�Top
WidthHeightSize.ValuesUUUUUU�@      �@�������@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   TERMO DE ENCERRAMENTO DIÁRIOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCertificoLeft)�TopRWidth�HeightSize.Values�������@�������@��������@������,�	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionQ   Certifico e dou fé, que no dia 00/00/0000 não houve movimentação cartorária.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRShapeQRShape5LeftlTop�Width�HeightSize.ValuesUUUUUUU� @      ��@UUUUUU��	@      ʙ	@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabellbAssinaturaLeft�Top�Width?HeightSize.Values�������@      d�@������>�	@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption
AssinaturaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
    	TQuickRepqkLivroLeft� TopWidthHeightcShowingPreviewBeforePrintqkLivroBeforePrintDataSetRXFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' 	OnEndPageqkLivroEndPageOptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.ContinuousPage.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPreviewWidth�PreviewHeight�PrevInitialZoomqrZoomToWidthPreviewDefaultSaveTypestQRPPreviewLeft 
PreviewTop  TQRBand
Cabecalho1Left9Top9Width�HeightJFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomBeforePrintCabecalho1BeforePrintTransparentBandForceNewColumnForceNewPageSize.Values��������@     :�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageHeader TQRLabel	qrTitularLeft�TopWidth'HeightSize.Values      ��@      T�@      ��@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionTITULARColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText
txEnderecoLeft�TopWidth6HeightSize.Values�������@UUUUUU	�@UUUUUUu�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataSetdm.Serventia	DataFieldENDERECOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabel	qrTitulo1Left�Top+Width� HeightSize.Values��������@������2�@��������@UUUUUUe�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption!L I V R O  D E  P R O T O C O L OColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrLivroLeftTop8WidthHeightSize.Values�������@������*�@������*�@������
�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionLivro:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabelqrFolhaLeft{Top8Width%HeightSize.Values�������@TUUUUU�	@������*�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionFolha:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabelqrServentiaLeft'TopWidth[HeightSize.Values XUUUUU�@UUUUUU!�@       �@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionNome ServentiaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	   TQRChildBandChildLeft9Top'Width�HeightTAlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values     @�@     :�	@ PreCaluculateBandHeightKeepOnOnePage
ParentBandDetalhe
PrintOrdercboAfterParent TQRLabel	QRLabel18Left�TopWidth� HeightSize.Values�������@UUUUUUY�@VUUUUU��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   TERMO DE ENCERRAMENTO DIÁRIOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShapeQRShape4Left� Top9WidthCHeightSize.ValuesUUUUUUU� @UUUUUU��@      Ж@��������@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabellbTermoLeft?�TopWidth�HeightSize.Values      ��@UUUUUU5�@       �@UUUUUU��	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionX   Certifico e dou fé, que a hora regulamentar encerrei a protocolização com X títulos.ColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText18Left�Top;Width#HeightSize.Values      ��@��������@�������@UUUUUU5�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataSetRX	DataFieldAssinaTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize   TQRBandDetalheLeft9Top� Width�Height� 
AfterPrintDetalheAfterPrintAlignToBottomBeforePrintDetalheBeforePrintTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUU��@     :�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail TQRShapeSh7LefthTopWidthAHeightSize.Values PUUUU�@ XUUUU��@       �@ �����Ծ	@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRShapeSh8Left�TopWidthHeightSize.ValuesUUUUUU��@UUUUUUu�	@       �@       �@ XLColumn XLNumFormat	nfGeneralBrush.StylebsClearShapeqrsRectangle
VertAdjust   TQRLabelQr6Left�TopWidth=HeightSize.Values      ��@      	@       �@UUUUUUe�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption	DEVOLVIDOColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShapeSh9Left TopWidthaHeight� Size.ValuesUUUUUU5�@                 �@������R�@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRLabel	qrDevedorLeft,TopWidth2HeightSize.Values      ��@VUUUUU��@������J�@������J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionDevedorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
qrEnderecoLeft&Top8Width8HeightSize.Values      ��@UUUUUU�@������*�@������*�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption	   EndereçoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
qrPortadorLeft*TopHWidth4HeightSize.Values      ��@      @�@      ��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionPortadorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	qrEspecieLeft/TopXWidth/HeightSize.Values      ��@TUUUUU��@VUUUUU��@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption   EspécieColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShapesh10LefthTopWidth�Height� Size.ValuesUUUUUU5�@UUUUUU��@       �@UUUUUUo�	@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRShapeSh11LeftHTopWidthaHeight� Size.ValuesUUUUUU5�@UUUUUU%�	@       �@������R�@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRLabelqrNumTituloLeftTophWidthBHeightSize.Values      ��@������*�@UUUUUU��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption   Nº do TítuloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText	lbDevedorLeftlTopWidth^HeightSize.Values      ��@      ��@������J�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldDevedorTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
lbEnderecoLeftlTop8Width^HeightSize.Values      ��@      ��@������*�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldEnderecoTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
lbPortadorLeftlTopHWidth^HeightSize.Values      ��@      ��@      ��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldApresentanteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	lbEspecieLeftlTopXWidth^HeightSize.Values      ��@      ��@VUUUUU��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldEspecieTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBTextlbNumTituloLeftlTophWidth^HeightSize.Values      ��@      ��@UUUUUU��@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldTituloTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabelqrDataTituloLeft� TophWidthNHeightSize.Values      ��@      �@UUUUUU��@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption   Data do Título:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBTextlbDataTituloLeft!TophWidth0HeightSize.Values      ��@UUUUUU)�@UUUUUU��@       �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataSetRX	DataField
DataTituloTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabelqrProtocoloLeftTopWidth9HeightSize.Values      ��@��������	@������J�@      Ж@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption	ProtocoloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrDataLeft#Top(WidthHeightSize.Values      ��@�������	@��������@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionDataColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrPrazoLeftTop8Width#HeightSize.Values      ��@UUUUUUC�	@������*�@UUUUUU5�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionPrazoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrValorLeft�TopHWidthTHeightSize.Values      ��@�������	@      ��@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption   Valor do TítuloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrCustasLeftTophWidth+HeightSize.Values      ��@      ��	@UUUUUU��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionCustasColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrVencimentoLefthTophWidthGHeightSize.Values      ��@       �@UUUUUU��@������ڻ@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionVencimento:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBTextlbVencimentoLeft�TophWidthPHeightSize.Values      ��@      ޏ	@UUUUUU��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataSetRX	DataFieldDataVencimentoTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabelqrSaldoLeft�TopxWidthcHeightSize.Values      ��@�������	@      ��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionSaldo do ProtestoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBTextlbProtocoloLeftLTopWidthYHeightSize.Values      ��@      x�	@      ��@ �����z�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataField	ProtocoloTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBTextlbDataLeftLTop+WidthYHeightSize.Values      ��@      x�	@ �������@ �����z�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldDataProtocoloTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBTextlbPrazoLeftLTop;WidthYHeightSize.Values      ��@      x�	@ ������@ �����z�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataField	DataPrazoTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBTextlbValorLeftLTopKWidthYHeightSize.Values      ��@      x�	@      p�@ �����z�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldValorTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBTextlbCustasLeftLTopkWidthYHeightSize.Values      ��@      x�	@UUUUUU��@������z�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldCustasTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBTextlbSaldoLeftLTop{WidthYHeightSize.Values      ��@      x�	@      ��@������z�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldSaldoTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabel	qrSacadorLeft,TopxWidth2HeightSize.Values      ��@VUUUUU��@      ��@������J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionSacadorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	qrCedenteLeft,Top� Width2HeightSize.Values      ��@VUUUUU��@      ��@������J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionCedenteColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrDocumentoLeftTop(Width@HeightSize.Values      ��@      ��@��������@UUUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption	DocumentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBTextlbDocumentoLeftlTop(Width^HeightSize.Values      ��@      ��@��������@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataField	DocumentoTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	lbSacadorLeftlTopxWidth^HeightSize.Values      ��@      ��@      ��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldSacadorTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	lbCedenteLeftlTop� Width^HeightSize.Values      ��@      ��@      ��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldCedenteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabel	qrEndossoLeftTop� Width1HeightSize.Values      ��@      ��	@      ��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionEndossoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText	lbEndossoLeftLTop� WidthYHeightSize.Values      ��@      x�	@UUUUUU=�@������z�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldEndossoTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRShapeSh1LeftlTopWidthHeightSize.Values XUUUU��@      ��@       �@       �@ XLColumn XLNumFormat	nfGeneralBrush.StylebsClearShapeqrsRectangle
VertAdjust   TQRLabelQr1LeftyTopWidthFHeightSize.Values      ��@�������@       �@UUUUUU5�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption
PROTESTADOColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQr2Left� TopWidthHeightSize.Values      ��@UUUUUU��@       �@������
�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionPAGOColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShapeSh2Left� TopWidthHeightSize.ValuesUUUUUU��@      ��@       �@       �@ XLColumn XLNumFormat	nfGeneralBrush.StylebsClearShapeqrsRectangle
VertAdjust   TQRLabelQr3LeftTopWidthAHeightSize.Values      ��@      ��@       �@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption	CANCELADOColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShapeSh3Left� TopWidthHeightSize.ValuesUUUUUU��@�������@       �@       �@ XLColumn XLNumFormat	nfGeneralBrush.StylebsClearShapeqrsRectangle
VertAdjust   TQRShapeSh4LeftGTopWidthHeightSize.ValuesUUUUUU��@������K�@       �@       �@ XLColumn XLNumFormat	nfGeneralBrush.StylebsClearShapeqrsRectangle
VertAdjust   TQRLabelQr4LeftTTopWidth4HeightSize.Values      ��@UUUUUU��@       �@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionRETIRADOColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShapeSh5Left�TopWidthHeightSize.ValuesUUUUUU��@      ��	@       �@       �@ XLColumn XLNumFormat	nfGeneralBrush.StylebsClearShapeqrsRectangle
VertAdjust   TQRLabelQr5Left�TopWidth3HeightSize.Values      ��@������D�	@       �@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionSUSTADOColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQrDLeft$TopWidth� HeightSize.Values      ��@UUUUUU=�	@       �@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionEM:   ____/____/________ColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShapeSh6Left TopWidthaHeightSize.Values PUUUU�@                 �@ �����R�@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRLabelQrSLeft,TopWidth2HeightSize.Values      ��@ `UUUU��@ �������@ �����J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption
   SituaçãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBTextlbSaldoTituloLeftLTop[WidthYHeightSize.Values      ��@      x�	@TUUUUU��@������z�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetRX	DataFieldSaldoTituloTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabelqrSaldoTituloLeft�TopXWidthTHeightSize.Values      ��@�������	@VUUUUU��@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption   Saldo do TítuloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrEncerramento1Left�Top"Width� HeightEnabledSize.ValuesUUUUUU�@UUUUUU}�@�������@UUUUUUy�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   TERMO DE ENCERRAMENTO DIÁRIOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabelqrEncerramento2Left)�TopBWidth�HeightEnabledSize.Values�������@�������@      ��@������,�	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionQ   Certifico e dou fé, que no dia 00/00/0000 não houve movimentação cartorária.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabelqrEncerramento3Left�Top� Width?HeightEnabledSize.Values�������@      d�@��������@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption
AssinaturaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRShapeSh12LeftlTop� Width�HeightEnabledSize.Values       �@      ��@UUUUUUU�@      ʙ	@ XLColumn XLNumFormat	nfGeneralBrush.ColorclRedShapeqrsRectangle
VertAdjust   TQRLabelQRLabel1LeftTop� Width*HeightSize.ValuesUUUUUU��@�������	@      p�@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandBiDiModebdLeftToRightParentBiDiModeCaptionQRLabel1ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize    TRxMemoryDataRX	FieldDefsNameTituloDataTypeftStringSize NameApresentanteDataTypeftStringSized NameDevedorDataTypeftStringSized NameValorDataTypeftFloat Name
VencimentoDataTypeftDate Name	ProtocoloDataTypeftStringSize NameSaldoDataTypeftFloat NameCustasDataTypeftFloat NameEnderecoDataTypeftStringSize�  Name	Dt_TituloDataTypeftDate NameDt_PrazoDataTypeftDate NameDt_ProtocoloDataTypeftDate NameEspecieDataTypeftStringSized Name	DocumentoDataTypeftStringSize NameSacadorDataTypeftStringSized NameCedenteDataTypeftStringSized NameEndossoDataTypeftStringSize NameAssinaDataTypeftStringSized NameEncerramentoDataType	ftBoolean NameStatusDataTypeftStringSize  LeftTop TIntegerFieldRXIdAto	FieldNameIdAto  TStringFieldRXTitulo	FieldNameTitulo  TStringFieldRXApresentante	FieldNameApresentanteSized  TStringField	RXDevedor	FieldNameDevedorSized  TFloatFieldRXValorDisplayLabel
###,##0.00	FieldNameValorDisplayFormat
###,##0.00  TStringFieldRXProtocolo	FieldName	Protocolo  TFloatFieldRXSaldoDisplayLabel
###,##0.00	FieldNameSaldoDisplayFormat
###,##0.00  TFloatFieldRXCustasDisplayLabel
###,##0.00	FieldNameCustasDisplayFormat
###,##0.00  TStringField
RXEndereco	FieldNameEnderecoSize�   
TDateFieldRXDataTitulo	FieldName
DataTituloDisplayFormat
DD/MM/YYYY  
TDateFieldRXDataPrazo	FieldName	DataPrazoDisplayFormat
DD/MM/YYYY  
TDateFieldRXDataProtocolo	FieldNameDataProtocoloDisplayFormat
DD/MM/YYYY  TStringField	RXEspecie	FieldNameEspecieSized  TStringFieldRXDocumento	FieldName	Documento  TStringField	RXSacador	FieldNameSacadorSized  TStringField	RXCedente	FieldNameCedenteSized  TStringField	RXEndosso	FieldNameEndosso  TStringFieldRXAssina	FieldNameAssinaSized  TBooleanFieldRXEncerramento	FieldNameEncerramento  TStringFieldRXStatus	FieldNameStatusSize  TStringFieldRXFolha	FieldNameFolhaSize
  
TDateFieldRXDataSolucao	FieldNameDataSolucao  TFloatFieldRXSaldoTituloDisplayLabel
###,##0.00	FieldNameSaldoTituloDisplayFormat
###,##0.00  
TDateFieldRXDataTermo	FieldName	DataTermo  TBooleanFieldRXVazio	FieldNameVazio  TStringField
RXConvenio	FieldNameConvenioSize  TStringFieldRXDataVencimento	FieldNameDataVencimentoSize
  
TTimeFieldRXHora	FieldNameHora    