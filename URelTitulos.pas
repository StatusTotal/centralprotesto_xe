unit URelTitulos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, sDBLookupComboBox, StdCtrls, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, sGroupBox, ExtCtrls, sPanel, Buttons,
  sBitBtn, DB, DBClient, SimpleDS, sCheckBox;

type
  TFRelTitulos = class(TForm)
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    P1: TsPanel;
    sGroupBox1: TsGroupBox;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    lkPortador: TsDBLookupComboBox;
    dsPortador: TDataSource;
    ckTodos: TsCheckBox;
    RgFiltro: TsRadioGroup;
    procedure btSairClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btVisualizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelTitulos: TFRelTitulos;

implementation

uses UDM, UPF, UQuickTitulos1;

{$R *.dfm}

procedure TFRelTitulos.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFRelTitulos.FormCreate(Sender: TObject);
begin
  dm.Portadores.Close;
  dm.Portadores.Open;
end;

procedure TFRelTitulos.btVisualizarClick(Sender: TObject);
begin
  if (not ckTodos.Checked) and (lkPortador.Text='') then Exit;
  PF.Aguarde(True);
  Application.CreateForm(TFQuickTitulos1,FQuickTitulos1);
  with FQuickTitulos1 do
  begin
      if ckTodos.Checked then
      begin
          qryTodosPortadores.Close;
          qryTodosPortadores.Params[0].AsDate:=edInicio.Date;
          qryTodosPortadores.Params[1].AsDate:=edFim.Date;
          qryTodosPortadores.Open;

          qryTodosPortadores.Filtered:=False;

          if RgFiltro.ItemIndex=0 then
          begin
              qryTodosPortadores.Filter:='CONVENIO='+QuotedStr('S');
              qryTodosPortadores.Filtered:=True;
          end;

          if RgFiltro.ItemIndex=1 then
          begin
              qryTodosPortadores.Filter:='CONVENIO='+QuotedStr('N');
              qryTodosPortadores.Filtered:=True;
          end;

          lbTabeliao.Caption   :=dm.ServentiaTABELIAO.AsString;
          lbCartorio2.Caption  :=dm.ServentiaDESCRICAO.AsString;
          lbEndereco2.Caption  :=dm.ServentiaENDERECO.AsString+' '+dm.ServentiaBAIRRO.AsString+' '+dm.ServentiaCIDADE.AsString+' - RJ ';
          lbTitulo2.Caption    :='Relat�rio de T�tulos - Per�odo: '+edInicio.Text+' at� '+edFim.Text;
          Todos.Preview;
      end
      else
      begin
          qryTitulos.Close;
          if dm.PortadoresCODIGO.AsInteger<>0 then
          begin
              qryTitulos.SQL.Text:='SELECT PROTOCOLO, DT_PROTOCOLO, TOTAL, NUMERO_TITULO, VALOR_TITULO, NOSSO_NUMERO, SACADOR, DEVEDOR,'+
                                           'STATUS FROM TITULOS WHERE CODIGO_APRESENTANTE=:CODIGO AND DT_PROTOCOLO BETWEEN :D1 AND :D2 ORDER BY 2,1';
              qryTitulos.Params.ParamByName('CODIGO').AsInteger:=dm.PortadoresCODIGO.AsInteger;
          end
          else
          begin
              qryTitulos.SQL.Text:='SELECT PROTOCOLO, DT_PROTOCOLO, TOTAL, NUMERO_TITULO, VALOR_TITULO, NOSSO_NUMERO, SACADOR, DEVEDOR,'+
                                           'STATUS from TITULOS WHERE CPF_CNPJ_APRESENTANTE=:CPF AND DT_PROTOCOLO BETWEEN :D1 AND :D2 ORDER BY 2,1';

              qryTitulos.Params.ParamByName('CPF').AsString:=dm.PortadoresDOCUMENTO.AsString;
          end;

          qryTitulos.Params.ParamByName('D1').AsDate:=edInicio.Date;
          qryTitulos.Params.ParamByName('D2').AsDate:=edFim.Date;

          qryTitulos.Open;

          qryTitulos.Filtered:=False;

          if RgFiltro.ItemIndex=0 then
          begin
              qryTitulos.Filter:='CONVENIO='+QuotedStr('S');
              qryTitulos.Filtered:=True;
          end;

          if RgFiltro.ItemIndex=1 then
          begin
              qryTitulos.Filter:='CONVENIO='+QuotedStr('N');
              qryTitulos.Filtered:=True;
          end;

          lbCartorio.Caption  :=dm.ServentiaDESCRICAO.AsString;
          lbEndereco.Caption  :=dm.ServentiaENDERECO.AsString+' '+dm.ServentiaBAIRRO.AsString+' '+dm.ServentiaCIDADE.AsString+' - RJ ';
          lbTitulo.Caption    :='Relat�rio de T�tulos - Per�odo: '+edInicio.Text+' at� '+edFim.Text;
          lbPortador.Caption  :=dm.PortadoresCODIGO.AsString+'  '+dm.PortadoresNOME.AsString;
          QuickRep1.Preview;
      end;
      Free;
  end;
end;

procedure TFRelTitulos.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

end.
