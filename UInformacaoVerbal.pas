unit UInformacaoVerbal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, sBitBtn, DBCtrls, DB, Mask,
  RXDBCtrl, RxDBComb, DBClient, SimpleDS, sComboBox, sDBComboBox,
  sDBEdit, sMaskEdit, sCustomComboEdit, sToolEdit, sDBDateEdit, sPanel,
  Data.DBXInterBase, Data.DBXFirebird, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFInformacaoVerbal = class(TForm)
    btSalvar: TsBitBtn;
    btCancelar: TsBitBtn;
    dsVerbal: TDataSource;
    P1: TsPanel;
    edData: TsDBDateEdit;
    edCCT: TsDBEdit;
    edRecibo: TsDBEdit;
    cbEscrevente: TsDBComboBox;
    edNome: TsDBEdit;
    cbCobranca: TsComboBox;
    edEmolumentos: TsDBEdit;
    edFetj: TsDBEdit;
    edFundperj: TsDBEdit;
    edFunperj: TsDBEdit;
    edFunarpen: TsDBEdit;
    edTotal: TsDBEdit;
    Custas: TFDQuery;
    CustasID_CUSTA: TIntegerField;
    CustasID_ATO: TIntegerField;
    CustasTABELA: TStringField;
    CustasITEM: TStringField;
    CustasSUBITEM: TStringField;
    CustasVALOR: TFloatField;
    CustasQTD: TIntegerField;
    CustasTOTAL: TFloatField;
    CustasDESCRICAO: TStringField;
    procedure btSalvarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CarregarEscreventes;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    vIdReservado: Integer;
  public
    { Public declarations }
  end;

var
  FInformacaoVerbal: TFInformacaoVerbal;

implementation

uses UDM, UGeral, UGDM;

{$R *.dfm}

procedure TFInformacaoVerbal.btSalvarClick(Sender: TObject);
begin
  if edNome.Text='' then
  begin
      GR.Aviso('INFORME O NOME!');
      edNome.SetFocus;
      Exit;
  end;

  if dm.qryVerbal.State=dsInsert then
  dm.qryVerbalID_VERBAL.AsInteger:=dm.IdAtual('ID_ATO','S');

  case cbCobranca.ItemIndex of
    0: dm.qryVerbalTIPO_COBRANCA.AsString:='JG';
    1: dm.qryVerbalTIPO_COBRANCA.AsString:='CC';
    2: dm.qryVerbalTIPO_COBRANCA.AsString:='SC';
    3: dm.qryVerbalTIPO_COBRANCA.AsString:='NH';
  end;

  dm.qryVerbalESCREVENTE.AsString:=cbEscrevente.Text;
  dm.qryVerbal.Post;
  dm.qryVerbal.ApplyUpdates(0);

  if (dm.qryVerbalTIPO_COBRANCA.AsString='CC') or (dm.qryVerbalTIPO_COBRANCA.AsString='NH') then
  begin
      Custas.Close;
      Custas.Params[0].AsInteger:=dm.qryVerbalID_VERBAL.AsInteger;
      Custas.Open;
      if Custas.IsEmpty then
      begin
          Custas.Append;
          CustasID_CUSTA.Value:=dm.IDAtual('ID_CUSTA','S');
      end
      else
        Custas.Edit;

      CustasID_ATO.AsInteger   :=dm.qryVerbalID_VERBAL.Value;
      CustasTABELA.AsString    :='16';
      CustasITEM.AsString      :='3';
      CustasSUBITEM.AsString   :='*';
      CustasDESCRICAO.AsString :='INFORMA��O VERBAL (50% DO VALOR DE UMA CERTID�O)';
      CustasQTD.AsInteger      :=1;
      CustasVALOR.AsFloat      :=dm.qryVerbalEMOLUMENTOS.AsFloat;
      CustasTOTAL.AsFloat      :=dm.qryVerbalEMOLUMENTOS.AsFloat;
      Custas.Post;
      Custas.ApplyUpdates(0);
  end
  else
    GR.ExecutarSQLDAC('DELETE FROM CUSTAS WHERE ID_ATO='+dm.qryVerbalID_VERBAL.AsString,dm.conSISTEMA);

  Gdm.BaixarSelo(dm.qryVerbalCCT.AsString,vIdReservado,'PROTESTO',Self.Name,dm.qryVerbalID_VERBAL.AsInteger,0,5,dm.qryVerbalDATA.AsDateTime,'',0,0,0,0,
                 GR.PegarNumero(dm.qryVerbalNUMERO.AsString),4025,'INFORMA��O VERBAL',dm.vNome,'N','CC',dm.qryVerbalEMOLUMENTOS.AsFloat,
                 dm.qryVerbalFETJ.AsFloat,dm.qryVerbalFUNDPERJ.AsFloat,dm.qryVerbalFUNPERJ.AsFloat,dm.qryVerbalFUNARPEN.AsFloat,
                 0,0,0,0,0,0,0,0,dm.qryVerbalTOTAL.AsFloat,0{ISS});
  vIdReservado:=0;
  Close;
end;

procedure TFInformacaoVerbal.btCancelarClick(Sender: TObject);
begin
  dm.qryVerbal.Cancel;
  Close;
end;

procedure TFInformacaoVerbal.FormCreate(Sender: TObject);
begin
  (**CarregarEscreventes;
  
  if dm.qryVerbal.State=dsInsert then
  begin
      vIdReservado:=GR.ValorGeneratorDBX('ID_RESERVADO',Gdm.BDGerencial);
      dm.qryVerbalCCT.AsString:=Gdm.ProximoSelo(dm.SerieAtual,'PROTESTO',Self.Name,'S','',0,0,0,'INFORMA��O VERBAL',dm.vNome,Now,vIdReservado);
      cbEscrevente.ItemIndex:=cbEscrevente.Items.IndexOf(dm.vNomeCompleto);
  end
  else
  begin
      if dm.qryVerbalTIPO_COBRANCA.AsString='JG' then cbCobranca.ItemIndex:=0;
      if dm.qryVerbalTIPO_COBRANCA.AsString='CC' then cbCobranca.ItemIndex:=1;
      if dm.qryVerbalTIPO_COBRANCA.AsString='SC' then cbCobranca.ItemIndex:=2;
      if dm.qryVerbalTIPO_COBRANCA.AsString='NH' then cbCobranca.ItemIndex:=3;

      cbEscrevente.ItemIndex:=cbEscrevente.Items.IndexOf(dm.qryVerbalESCREVENTE.AsString);
  end; **)
end;

procedure TFInformacaoVerbal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if dm.qryVerbal.State in [dsEdit,dsInsert] then dm.qryVerbal.Cancel;
  Gdm.LiberarSelo('PROTESTO',edCCT.Text,False,-1,vIdReservado);
end;

procedure TFInformacaoVerbal.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFInformacaoVerbal.CarregarEscreventes;
begin
  dm.Escreventes.Close;
  dm.Escreventes.Open;
  dm.Escreventes.First;
  while not dm.Escreventes.Eof do
  begin
      cbEscrevente.Items.Add(dm.EscreventesNOME.AsString);
      dm.Escreventes.Next;
  end;
end;

procedure TFInformacaoVerbal.FormShow(Sender: TObject);
begin
  edData.SetFocus;
end;

end.
