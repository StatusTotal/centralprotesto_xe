unit UQuickLivroProtocolo1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, RxMemDS, QRCtrls, QuickRpt, ExtCtrls, Grids, DBGrids;

type
  TFQuickLivroProtocolo1 = class(TForm)
    qkLivro: TQuickRep;
    Cabecalho1: TQRBand;
    txEndereco: TQRDBText;
    qrTitulo1: TQRLabel;
    qrTitular: TQRLabel;
    RX: TRxMemoryData;
    qrLivro: TQRLabel;
    qrFolha: TQRLabel;
    Child: TQRChildBand;
    QRLabel18: TQRLabel;
    QRShape4: TQRShape;
    lbTermo: TQRLabel;
    QRDBText18: TQRDBText;
    Detalhe: TQRBand;
    Sh9: TQRShape;
    qrDevedor: TQRLabel;
    qrEndereco: TQRLabel;
    qrPortador: TQRLabel;
    qrEspecie: TQRLabel;
    sh10: TQRShape;
    Sh11: TQRShape;
    qrNumTitulo: TQRLabel;
    lbDevedor: TQRDBText;
    lbEndereco: TQRDBText;
    lbPortador: TQRDBText;
    lbEspecie: TQRDBText;
    lbNumTitulo: TQRDBText;
    qrDataTitulo: TQRLabel;
    lbDataTitulo: TQRDBText;
    qrProtocolo: TQRLabel;
    qrData: TQRLabel;
    qrPrazo: TQRLabel;
    qrValor: TQRLabel;
    qrCustas: TQRLabel;
    qrVencimento: TQRLabel;
    lbVencimento: TQRDBText;
    qrSaldo: TQRLabel;
    lbProtocolo: TQRDBText;
    lbData: TQRDBText;
    lbPrazo: TQRDBText;
    lbValor: TQRDBText;
    lbCustas: TQRDBText;
    lbSaldo: TQRDBText;
    qrSacador: TQRLabel;
    qrCedente: TQRLabel;
    qrDocumento: TQRLabel;
    lbDocumento: TQRDBText;
    lbSacador: TQRDBText;
    lbCedente: TQRDBText;
    qrEndosso: TQRLabel;
    lbEndosso: TQRDBText;
    Sh7: TQRShape;
    Sh1: TQRShape;
    Qr1: TQRLabel;
    Qr2: TQRLabel;
    Sh2: TQRShape;
    Qr3: TQRLabel;
    Sh3: TQRShape;
    Sh4: TQRShape;
    Qr4: TQRLabel;
    Sh5: TQRShape;
    Qr5: TQRLabel;
    QrD: TQRLabel;
    Sh6: TQRShape;
    QrS: TQRLabel;
    Qr6: TQRLabel;
    Sh8: TQRShape;
    qrServentia: TQRLabel;
    RXIdAto: TIntegerField;
    RXTitulo: TStringField;
    RXApresentante: TStringField;
    RXDevedor: TStringField;
    RXValor: TFloatField;
    RXProtocolo: TStringField;
    RXSaldo: TFloatField;
    RXCustas: TFloatField;
    RXEndereco: TStringField;
    RXDataTitulo: TDateField;
    RXDataPrazo: TDateField;
    RXDataProtocolo: TDateField;
    RXEspecie: TStringField;
    RXDocumento: TStringField;
    RXSacador: TStringField;
    RXCedente: TStringField;
    RXEndosso: TStringField;
    RXAssina: TStringField;
    RXEncerramento: TBooleanField;
    RXStatus: TStringField;
    RXFolha: TStringField;
    RXDataSolucao: TDateField;
    qrSaldoTitulo: TQRLabel;
    RXSaldoTitulo: TFloatField;
    qkEncerramento: TQuickRep;
    Cabecalho2: TQRBand;
    qrTitular2: TQRLabel;
    QRDBText2: TQRDBText;
    qrTitulo2: TQRLabel;
    qrLivro2: TQRLabel;
    qrFolha2: TQRLabel;
    qrServentia2: TQRLabel;
    lbSaldoTitulo: TQRDBText;
    QRBand2: TQRBand;
    QRLabel6: TQRLabel;
    lbCertifico: TQRLabel;
    QRShape5: TQRShape;
    lbAssinatura: TQRLabel;
    RXDataTermo: TDateField;
    qrEncerramento1: TQRLabel;
    qrEncerramento2: TQRLabel;
    Sh12: TQRShape;
    qrEncerramento3: TQRLabel;
    RXVazio: TBooleanField;
    QRLabel1: TQRLabel;
    RXConvenio: TStringField;
    RXDataVencimento: TStringField;
    RXHora: TTimeField;
    procedure Cabecalho1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure DetalheBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qkLivroEndPage(Sender: TCustomQuickRep);
    procedure FormCreate(Sender: TObject);
    procedure qkLivroBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure Cabecalho2BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure DetalheAfterPrint(Sender: TQRCustomBand; BandPrinted: Boolean);
    procedure AtivarComponentes(Enable: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickLivroProtocolo1: TFQuickLivroProtocolo1;
  Qtd,vLivro,vFolha: Integer;

implementation

uses UDM, UPF, UGeral, StrUtils, UGDM;

{$R *.dfm}

procedure TFQuickLivroProtocolo1.Cabecalho1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  qrTitular.Caption   :=dm.ValorParametro(7)+' '+dm.ServentiaTABELIAO.AsString;
  Gdm.cQuickFolhas.Locate('PAGINA',qkLivro.QRPrinter.PageCount,[]);
  qrLivro.Caption     :='Livro: '+Gdm.cQuickFolhasLIVRO.AsString;
  qrFolha.Caption     :='Folha: '+Gdm.cQuickFolhasFOLHA.AsString;
  qrTitulo1.Caption   :=IfThen(dm.vLivroConvenio,'L I V R O  D E  C O N V � N I O','L I V R O  D E  P R O T O C O L O');
end;

procedure TFQuickLivroProtocolo1.Cabecalho2BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  qrTitular2.Caption  :=dm.ValorParametro(7)+' '+dm.ServentiaTABELIAO.AsString;
  qrLivro2.Caption    :='Livro: '+IntToStr(vLivro);
  Gdm.cQuickFolhas.Locate('PAGINA',qkEncerramento.QRPrinter.PageCount,[]);
  qrFolha2.Caption    :='Folha: '+Gdm.cQuickFolhasFOLHA.AsString;
  qrTitulo2.Caption   :=IfThen(dm.vLivroConvenio,'L I V R O  D E  C O N V � N I O','L I V R O  D E  P R O T O C O L O');
end;

procedure TFQuickLivroProtocolo1.qkLivroEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickLivroProtocolo1.DetalheBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Titulo: String;
begin
  if not RXVazio.AsBoolean then
  Inc(Qtd);

  Sh6.Enabled:=GR.iif(dm.ServentiaCODIGO.AsInteger<>1726,True,False);
  Sh7.Enabled:=GR.iif(dm.ServentiaCODIGO.AsInteger<>1726,True,False);

  Child.Enabled:=RXEncerramento.AsBoolean;
  if Child.Enabled then
  begin
      if Qtd>1 then
        Titulo:=' t�tulos'
          else Titulo:=' t�tulo';
      lbTermo.Caption:='Certifico e dou f�, que a hora regulamentar do dia '+
                       FormatDateTime('dd/mm/yyyy',RXDataTermo.AsDateTime)+
                       ', encerrei a movimenta��o com '+IntToStr(Qtd)+Titulo+'.';
      Qtd:=0;
  end;

  QrD.Caption:='';

  if RXStatus.AsString='PROTESTADO' then
  begin
      Sh1.Brush.Color:=clBlack;
      Sh1.Brush.Style:=bsSolid;
      QrD.Caption:='EM: '+FormatDateTime('dd/mm/yyyy',RXDataSolucao.AsDateTime);
  end
  else
    Sh1.Brush.Style:=bsClear;

  if RXStatus.AsString='PAGO' then
  begin
      Sh2.Brush.Color:=clBlack;
      Sh2.Brush.Style:=bsSolid;
      QrD.Caption:='EM: '+FormatDateTime('dd/mm/yyyy',RXDataSolucao.AsDateTime);
  end
  else
    Sh2.Brush.Style:=bsClear;

  if RXStatus.AsString='CANCELADO' then
  begin
      Sh3.Brush.Color:=clBlack;
      Sh3.Brush.Style:=bsSolid;
      QrD.Caption:='EM: '+FormatDateTime('dd/mm/yyyy',RXDataSolucao.AsDateTime);
  end
  else
    Sh3.Brush.Style:=bsClear;

  if RXStatus.AsString='RETIRADO' then
  begin
      Sh4.Brush.Color:=clBlack;
      Sh4.Brush.Style:=bsSolid;
      QrD.Caption:='EM: '+FormatDateTime('dd/mm/yyyy',RXDataSolucao.AsDateTime);
  end
  else
    Sh4.Brush.Style:=bsClear;

  if RXStatus.AsString='SUSTADO' then
  begin
      Sh5.Brush.Color:=clBlack;
      Sh5.Brush.Style:=bsSolid;
      QrD.Caption:='EM: '+FormatDateTime('dd/mm/yyyy',RXDataSolucao.AsDateTime);
  end
  else
    Sh5.Brush.Style:=bsClear;

  if RXStatus.AsString='DEVOLVIDO' then
  begin
      Sh8.Brush.Color:=clBlack;
      Sh8.Brush.Style:=bsSolid;
      QrD.Caption:='EM: '+FormatDateTime('dd/mm/yyyy',RXDataSolucao.AsDateTime);
  end
  else
    Sh8.Brush.Style:=bsClear;

  if RXVazio.AsBoolean then
    AtivarComponentes(False)
      else AtivarComponentes(True);

  if RXConvenio.AsString='S' then
    QRLabel1.Caption:='Realizado na forma do artigo 6o, inciso III, alinea b do Executivo Conjunto No 22/99'
      else QRLabel1.Caption:=' ';
end;

procedure TFQuickLivroProtocolo1.FormCreate(Sender: TObject);
begin
  if dm.ServentiaCODIGO.AsInteger=1726 then
  begin
      Sh1.Enabled :=False;
      Sh2.Enabled :=False;
      Sh3.Enabled :=False;
      Sh4.Enabled :=False;
      Sh5.Enabled :=False;
      Sh6.Enabled :=False;
      Sh7.Enabled :=False;
      Sh8.Enabled :=False;
      Qr1.Enabled :=False;
      Qr2.Enabled :=False;
      Qr3.Enabled :=False;
      Qr4.Enabled :=False;
      Qr5.Enabled :=False;
      Qr6.Enabled :=False;
      QrS.Enabled :=False;
      QrD.Enabled :=False;
  end;

  qrServentia.Caption:=dm.ServentiaDESCRICAO.AsString;
  qrServentia2.Caption:=dm.ServentiaDESCRICAO.AsString;

  {if dm.ServentiaCODIGO.AsInteger=1430 then
    if GR.Pergunta('Utilizar informa��es do 4� OFicio') then
      qrServentia.Caption:='VASSOURAS - 4� OF�CIO';}

  Qtd:=0;
  if dm.ServentiaCODIGO.AsInteger=2652 then
  QrD.Enabled:=False;
end;

procedure TFQuickLivroProtocolo1.qkLivroBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  if dm.ValorParametro(21)='N' then
  begin
      Cabecalho1.Frame.DrawTop      :=False;
      Cabecalho1.Frame.DrawBottom   :=False;
      Cabecalho1.Frame.DrawLeft     :=False;
      Cabecalho1.Frame.DrawRight    :=False;
      Cabecalho2.Frame.DrawTop      :=False;
      Cabecalho2.Frame.DrawBottom   :=False;
      Cabecalho2.Frame.DrawLeft     :=False;
      Cabecalho2.Frame.DrawRight    :=False;
      qrServentia.Enabled           :=False;
      qrTitular.Enabled             :=False;
      txEndereco.Enabled            :=False;
      qrTitulo1.Enabled             :=False;
      qrLivro.Enabled               :=False;
      qrFolha.Enabled               :=False;
  end;
end;

procedure TFQuickLivroProtocolo1.DetalheAfterPrint(Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  if (not RXVazio.AsBoolean) and (RXIdAto.AsInteger<>0) then
    GR.ExecutarSQLDAC('UPDATE TITULOS SET LIVRO_PROTOCOLO='+IntToStr(vLivro)+
                      ',FOLHA_PROTOCOLO='+IfThen(GR.PegarNumeroTexto(qrFolha.Caption)<>'',
                      GR.PegarNumeroTexto(qrFolha.Caption),IntToStr(vFolha))+
                      ' WHERE ID_ATO='+RXIdAto.AsString,dm.conSISTEMA);
end;

procedure TFQuickLivroProtocolo1.AtivarComponentes(Enable: Boolean);
begin
  Sh1.Enabled             :=Enable;
  Sh2.Enabled             :=Enable;
  Sh3.Enabled             :=Enable;
  Sh4.Enabled             :=Enable;
  Sh5.Enabled             :=Enable;
  Sh6.Enabled             :=Enable;
  Sh7.Enabled             :=Enable;
  Sh8.Enabled             :=Enable;
  Sh9.Enabled             :=Enable;
  sh10.Enabled            :=Enable;
  Sh11.Enabled            :=Enable;
  QrS.Enabled             :=Enable;
  QrD.Enabled             :=Enable;
  Qr1.Enabled             :=Enable;
  Qr2.Enabled             :=Enable;
  Qr3.Enabled             :=Enable;
  Qr4.Enabled             :=Enable;
  Qr5.Enabled             :=Enable;
  Qr6.Enabled             :=Enable;
  qrDevedor.Enabled       :=Enable;
  qrDocumento.Enabled     :=Enable;
  qrEndereco.Enabled      :=Enable;
  qrPortador.Enabled      :=Enable;
  qrEspecie.Enabled       :=Enable;
  qrNumTitulo.Enabled     :=Enable;
  qrSacador.Enabled       :=Enable;
  qrCedente.Enabled       :=Enable;
  lbDevedor.Enabled       :=Enable;
  lbDocumento.Enabled     :=Enable;
  lbEndereco.Enabled      :=Enable;
  lbPortador.Enabled      :=Enable;
  lbEspecie.Enabled       :=Enable;
  lbNumTitulo.Enabled     :=Enable;
  lbSacador.Enabled       :=Enable;
  lbCedente.Enabled       :=Enable;
  qrDataTitulo.Enabled    :=Enable;
  qrVencimento.Enabled    :=Enable;
  lbDataTitulo.Enabled    :=Enable;
  qrProtocolo.Enabled     :=Enable;
  qrData.Enabled          :=Enable;
  qrPrazo.Enabled         :=Enable;
  qrValor.Enabled         :=Enable;
  qrSaldoTitulo.Enabled   :=Enable;
  qrCustas.Enabled        :=Enable;
  qrSaldo.Enabled         :=Enable;
  qrEndosso.Enabled       :=Enable;
  lbProtocolo.Enabled     :=Enable;
  lbData.Enabled          :=Enable;
  lbVencimento.Enabled    :=Enable;
  lbPrazo.Enabled         :=Enable;
  lbValor.Enabled         :=Enable;
  lbSaldoTitulo.Enabled   :=Enable;
  lbCustas.Enabled        :=Enable;
  lbSaldo.Enabled         :=Enable;
  lbEndosso.Enabled       :=Enable;

  qrEncerramento1.Enabled :=False;
  qrEncerramento2.Enabled :=False;
  qrEncerramento3.Enabled :=False;
  Sh12.Enabled            :=False;

  if (RXVazio.AsBoolean) and (RXIdAto.AsInteger=-1) then
  begin
      qrEncerramento1.Font.Color:=clBlack;
      qrEncerramento2.Font.Color:=clBlack;
      qrEncerramento3.Font.Color:=clBlack;
      qrEncerramento1.Enabled   :=True;
      qrEncerramento2.Enabled   :=True;
      qrEncerramento3.Enabled   :=True;
      Sh12.Brush.Color          :=clBlack;
      Sh12.Enabled              :=True;
      Sh12.Height               :=1;
      qrEncerramento2.Caption   :='Certifico e dou f�, que no dia '+FormatDateTime('dd/mm/yyyy',RXDataTermo.AsDateTime)+' n�o houve movimenta��o cartor�ria.';
      qrEncerramento3.Caption   :=dm.vAssinatura;
  end;
end;

end.
