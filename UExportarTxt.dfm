�
 TFEXPORTARTXT 0�n  TPF0TFExportarTxtFExportarTxtLeft� Top� BorderIconsbiSystemMenu BorderStylebsDialogCaptionARQUIVO DE RETORNOClientHeight�ClientWidthColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
KeyPreview	OldCreateOrderPositionpoMainFormCenterScaledOnCreate
FormCreate	OnKeyDownFormKeyDownPixelsPerInch`
TextHeight TsMemoMLeft Top6Width�Height� BorderStylebsNoneColor��� Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameCourier New
Font.Style 
ParentFontReadOnly	
ScrollBarsssBothTabOrderVisibleSkinData.SkinSectionHINT  TsPanelP1LeftTopWidthHeightSTabOrder OnMouseMoveP1MouseMoveSkinData.SkinSectionSCROLLSLIDERH TsRadioGroupRgTipoLeftTop�Width� Height8Caption     Tipo de Arquivo      ParentBackgroundTabOrder	VisibleCaptionLayoutclTopCenterSkinData.SkinSectionHINTCaptionSkinHINT	ItemIndex Items.StringsBancoDistribuidor   TsBitBtnbtVisualizarLeft0TopWidth@Height0CursorcrHandPointCaption
VisualizarLayout
blGlyphTopTabOrderOnClickbtVisualizarClick
ImageIndexImagesGdm.Im16SkinData.SkinSectionPAGECONTROL  TsBitBtn
btExportarLeft�TopWidthAHeight0CursorcrHandPointCaptionExportarLayout
blGlyphTop	PopupMenu
pmExportarTabOrderOnClickbtExportarClick
ImageIndex ImagesGdm.Im16SkinData.SkinSectionPAGECONTROL  
TsDateEditedInicioLeftATop Width[HeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorDDD Font.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontTabOrder
OnKeyPressedInicioKeyPressBoundLabel.Active	BoundLabel.ParentFontBoundLabel.Caption   InícioBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameArialBoundLabel.Font.Style SkinData.SkinSectionEDITGlyphMode.GrayedGlyphMode.Blend DefaultToday	  
TsDateEditedFimLeft� Top Width[HeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorDDD Font.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontTabOrder
OnKeyPressedFimKeyPressBoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionFimBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameArialBoundLabel.Font.Style SkinData.SkinSectionEDITGlyphMode.GrayedGlyphMode.Blend DefaultToday	  
TsDateEditedDataLeft�TopWidth[HeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorDDD Font.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontReadOnly	TabOrderOnChangeedDataChangeBoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionData do ArquivoBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style SkinData.SkinSectionEDITGlyphMode.GrayedGlyphMode.Blend DefaultToday	  TsDBEdit	edRemessaLeft�Top9Width[HeightColorclWhite	DataFieldNUMERO
DataSource
dsRemessasFont.CharsetANSI_CHARSET
Font.ColorDDD Font.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderOnExitedRemessaExitSkinData.SkinSectionEDITBoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionRemessaBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style   TsBitBtn
btRejeitarLefttTopWidthWHeight0CursorcrHandPointCaptionIrregularidadeFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style Layout
blGlyphTop
ParentFontTabOrderOnClickbtRejeitarClick
ImageIndexImagesGdm.Im16SkinData.SkinSectionPAGECONTROL  TsDBLookupComboBoxlkOutrosLeftATopWidth� HeightCursorcrHandPointColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorDDD Font.Height�	Font.NameTahoma
Font.Style KeyFieldID_PORTADOR	ListFieldNOME
ListSourcedsOutros
ParentFontTabOrder OnClicklkOutrosClickBoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionPortadorBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.ColorclWindowTextBoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style SkinData.SkinSectionCOMBOBOX  TsDBLookupComboBoxlcbServentiaAgregadaLeftATop9Width� HeightCursorcrHandPointColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorDDD Font.Height�	Font.NameTahoma
Font.Style KeyFieldCODIGO_DISTRIBUICAO	ListField	DESCRICAO
ListSourcedsServentiaAgregada
ParentFontTabOrderOnClicklkOutrosClick
OnKeyPresslcbServentiaAgregadaKeyPressBoundLabel.Active	BoundLabel.ParentFontBoundLabel.Caption	Serv. Ag.BoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.ColorclWindowTextBoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style SkinData.SkinSectionCOMBOBOX   TsStatusBarBarraLeftTop�WidthHeightAlignalNonePanelsText
QuantidadeWidthI Width2  OnMouseMoveBarraMouseMoveSkinData.SkinSection	STATUSBAR  	TQuickRepqkRelatorioLeft� Top�WidthHeightcShowingPreviewDataSetTitulosFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.ContinuousPage.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPreviewWidth�PreviewHeight�PrevInitialZoomqrZoomToFitPreviewDefaultSaveTypestQRPPreviewLeft 
PreviewTop  TQRBandQRBand1Left&Top&Width�HeightuFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values      Ț@ �����v�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageHeader TQRLabellbCidadeLeft�Top%WidthNHeightSize.Values�������@��������@��������@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionCidade - EstadoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabel
lbCartorioLeft�TopWidth� HeightSize.Values��������@UUUUUU=�@      ��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   NOME DO CARTÓRIOColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbReciboLeft�Top7Width� HeightSize.Values�������@��������@UUUUUU��@������F�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption%   Relação de Títulos Enviados ao CRAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabel	lbPeriodoLeft�TopMWidthHeightSize.Values�������@��������@��������@       �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionDataColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  
TQRSysData
QRSysData1Left�TopcWidthxHeightSize.Values       �@��������@      ��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataqrsDetailCountFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTextQuantidade: TransparentExportAsexptTextFontSize   TQRBandQRBand2Left&Top� Width�HeightFrame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values      ��@ �����v�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail 	TQRDBText	QRDBText1LeftTop Width]HeightSize.ValuesUUUUUU��@UUUUUUU�@                �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetTitulos	DataField	PROTOCOLOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText2LefthTop WidthZHeightSize.ValuesUUUUUU��@UUUUUU��@          �������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetTitulos	DataFieldNUMERO_TITULOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText3Left� Top Width� HeightSize.ValuesUUUUUU��@UUUUUU��@          UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetTitulos	DataFieldCEDENTEFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText4Left�Top Width� HeightSize.ValuesUUUUUU��@������:�	@          UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetTitulos	DataFieldSACADORFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText5Left�Top Width=HeightSize.ValuesUUUUUU��@      N�	@          UUUUUUe�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetTitulos	DataFieldSTATUSFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize   TQRChildBand
ChildBand1Left&Top� Width�HeightFrame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values XUUUUU�@ �����v�	@ PreCaluculateBandHeightKeepOnOnePage
ParentBandQRBand1
PrintOrdercboAfterParent TQRLabelQRLabel1LeftTopWidth.HeightSize.Values       �@UUUUUUU�@UUUUUUU� @������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption	ProtocoloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel2LefthTopWidth(HeightSize.Values       �@UUUUUU��@UUUUUUU� @��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   Nº TítuloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel3Left� TopWidth&HeightSize.Values       �@UUUUUU��@UUUUUUU� @UUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionCedenteColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel4Left�TopWidth&HeightSize.Values       �@������:�	@UUUUUUU� @UUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionSacadorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel5Left�TopWidth(HeightSize.Values       �@      N�	@UUUUUUU� @��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption
   SituaçãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   TQRBandQRBand3Left&Top� Width�HeightAlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values������j�@ �����v�	@ PreCaluculateBandHeightKeepOnOnePageBandType	rbSummary   TPanelPanel1LeftTopWWidthHeight|AutoSize	
BevelInnerbvRaised
BevelOuter	bvLoweredCaptionPanel1TabOrder 	TwwDBGridG1LeftTopWidthHeightxControlType.StringsCheck;CheckBox;S;N Selected.StringsCheck	4	*	F   CODIGO	6	Código	FAPRESENTANTE	25	Portador	TProtocolo	11	Protocolo	T   NUMERO_TITULO	13	Nº Título	T   STATUS	13	Situação	T%   AGENCIA_CEDENTE	19	Agência Cedente	F   NOSSO_NUMERO	19	Nosso Número	FEXPORTADO	9	Exportado	F IniAttributes.Delimiter;;IniAttributes.UnicodeIniFile
TitleColor	clBtnFace	FixedColsShowHorzScrollBar	EditControlOptionsecoCheckboxSingleClickecoSearchOwnerForm BorderStylebsNone
DataSource	dsTitulosEditCalculated	Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
KeyOptions Options	dgEditingdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit
dgWordWrapdgProportionalColResize 
ParentFont	PopupMenuPMTabOrder TitleAlignmenttaCenterTitleFont.CharsetANSI_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameTahomaTitleFont.Style 
TitleLinesTitleButtons
UseTFields
OnColEnter
G1ColEnter
OnKeyPress
G1KeyPressOnMouseMoveG1MouseMove   TDataSetProvider
dspTitulosDataSet
qryTitulos
UpdateModeupWhereKeyOnlyLeft� Top  TClientDataSetTitulos
Aggregates ParamsDataTypeftStringNameCR1	ParamTypeptInput DataTypeftStringNameCR2	ParamTypeptInput DataTypeftStringNameBC1	ParamTypeptInput DataTypeftStringNameBC2	ParamTypeptInput DataTypeftStringNameCV1	ParamTypeptInput DataTypeftStringNameCV2	ParamTypeptInput DataType	ftIntegerNameID1	ParamTypeptInput DataType	ftIntegerNameID2	ParamTypeptInput DataTypeftDateNameD11	ParamTypeptInput DataTypeftDateNameD12	ParamTypeptInput DataTypeftDateNameD21	ParamTypeptInput DataTypeftDateNameD22	ParamTypeptInput DataTypeftDateNameD31	ParamTypeptInput DataTypeftDateNameD32	ParamTypeptInput DataTypeftDateNameD41	ParamTypeptInput DataTypeftDateNameD42	ParamTypeptInput DataTypeftDateNameD51	ParamTypeptInput DataTypeftDateNameD52	ParamTypeptInput DataType	ftIntegerNameCART01	ParamTypeptInput DataType	ftIntegerNameCART02	ParamTypeptInput  ProviderName
dspTitulos	AfterPostTitulosAfterPostOnCalcFieldsTitulosCalcFieldsLeft� TopO TIntegerFieldTitulosID_ATO	FieldNameID_ATOProviderFlags
pfInUpdate	pfInWherepfInKey Required	  
TDateFieldTitulosDT_PROTOCOLO	FieldNameDT_PROTOCOLO  TIntegerFieldTitulosPROTOCOLO	AlignmenttaCenter	FieldName	PROTOCOLO  
TDateFieldTitulosDT_REGISTRO	FieldNameDT_REGISTRO  
TDateFieldTitulosDT_PAGAMENTO	FieldNameDT_PAGAMENTO  TFloatFieldTitulosDISTRIBUICAO	FieldNameDISTRIBUICAODisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosTOTAL	FieldNameTOTALDisplayFormat	#####0.00
EditFormat	#####0.00  TStringFieldTitulosNUMERO_TITULO	FieldNameNUMERO_TITULO  TFloatFieldTitulosVALOR_TITULO	FieldNameVALOR_TITULODisplayFormat	#####0.00
EditFormat	#####0.00  TFloatFieldTitulosSALDO_PROTESTO	FieldNameSALDO_PROTESTODisplayFormat	#####0.00
EditFormat	#####0.00  TStringFieldTitulosCONVENIO	FieldNameCONVENIO	FixedChar	Size  TStringFieldTitulosAPRESENTANTE	FieldNameAPRESENTANTESized  TStringFieldTitulosNOSSO_NUMERO	AlignmenttaCenter	FieldNameNOSSO_NUMEROSize  TStringFieldTitulosAGENCIA_CEDENTE	AlignmenttaCenter	FieldNameAGENCIA_CEDENTESize  
TDateFieldTitulosDT_SUSTADO	FieldName
DT_SUSTADO  
TDateFieldTitulosDT_RETIRADO	FieldNameDT_RETIRADO  TStringFieldTitulosSTATUS	AlignmenttaCenter	FieldNameSTATUS  TStringFieldTitulosEXPORTADO	AlignmenttaCenter	FieldName	EXPORTADO	FixedChar	Size  TStringFieldTitulosCheck	FieldKindfkInternalCalc	FieldNameCheckSize  TFloatFieldTitulosTARIFA_BANCARIA	FieldNameTARIFA_BANCARIA  TStringFieldTitulosRETORNO	FieldNameRETORNO  
TDateFieldTitulosDT_DEVOLVIDO	FieldNameDT_DEVOLVIDO  TFloatFieldTitulosVALOR_AR	FieldNameVALOR_AR  TIntegerFieldTitulosIRREGULARIDADE	FieldNameIRREGULARIDADE  TFloatFieldTitulosSALDO_TITULO	FieldNameSALDO_TITULODisplayFormat	#####0.00
EditFormat	#####0.00  TStringFieldTitulosCODIGO	AlignmenttaCenter	FieldNameCODIGOProviderFlags Size
  TIntegerFieldTitulosSEQUENCIA	FieldName	SEQUENCIAProviderFlags   TStringFieldTitulosAGENCIA	FieldNameAGENCIAProviderFlags Size  TFloatFieldTitulosVALOR_TED	FieldName	VALOR_TEDProviderFlags   TIntegerFieldTitulosID_PORTADOR	FieldNameID_PORTADORProviderFlags Required	  TStringFieldTitulosELETRONICO	FieldName
ELETRONICO	FixedChar	Size  TStringFieldTitulosCODIGO_APRESENTANTE	FieldNameCODIGO_APRESENTANTESize
  TIntegerFieldTitulosCARTORIO	FieldNameCARTORIO  
TDateFieldTitulosDT_ENTRADA	FieldName
DT_ENTRADA  TIntegerFieldTitulosPROTOCOLO_CARTORIO	FieldNamePROTOCOLO_CARTORIO  TBooleanFieldTitulosxDIA28	FieldKindfkInternalCalc	FieldNamexDIA28  TStringFieldTitulosPOSTECIPADO	FieldNamePOSTECIPADO	FixedChar	Size   TDataSource	dsTitulosDataSetTitulosLeft� Top  
TPopupMenuPMOnPopupPMPopupLeftaTop 	TMenuItemMarcarTodos1CaptionMarcar TodosOnClickMarcarTodos1Click  	TMenuItemDesmarcarTodos1CaptionDesmarcar TodosOnClickDesmarcarTodos1Click  	TMenuItemN1Caption-  	TMenuItemMenuPortadorCaptionSelecionar PortadorOnClickMenuPortadorClick  	TMenuItemN2Caption-  	TMenuItem	Imprimir1CaptionImprimirOnClickImprimir1Click   TDataSource
dsRemessasDataSetdm.RemessasLeftaTopO  TDataSourcedsOutrosDataSetRXOutrosLeftaTop  TRxMemoryDataRXOutros	FieldDefs LeftaTop�  TIntegerFieldRXOutrosID_PORTADOR	FieldNameID_PORTADOR  TStringFieldRXOutrosNOME	FieldNameNOMESized  TStringFieldRXOutrosCODIGO	FieldNameCODIGOSize
   TFDQuery	qryOutros
Connectiondm.conSISTEMASQL.Stringsselect * from PORTADORES    where     (CRA='S') or(BANCO='S' or CONVENIO='S')    order by NOME LeftaTop�  TIntegerFieldqryOutrosID_PORTADOR	FieldNameID_PORTADOROriginID_PORTADORProviderFlags
pfInUpdate	pfInWherepfInKey Required	  TStringFieldqryOutrosCODIGO	FieldNameCODIGOOriginCODIGOSize
  TStringFieldqryOutrosNOME	FieldNameNOMEOriginNOMESized  TStringFieldqryOutrosTIPO	FieldNameTIPOOriginTIPO	FixedChar	Size  TStringFieldqryOutrosDOCUMENTO	FieldName	DOCUMENTOOrigin	DOCUMENTOSize  TStringFieldqryOutrosENDERECO	FieldNameENDERECOOriginENDERECOSized  TStringFieldqryOutrosBANCO	FieldNameBANCOOriginBANCO	FixedChar	Size  TStringFieldqryOutrosCONVENIO	FieldNameCONVENIOOriginCONVENIO	FixedChar	Size  TStringFieldqryOutrosCONTA	FieldNameCONTAOriginCONTA  
TMemoFieldqryOutrosOBSERVACAO	FieldName
OBSERVACAOOrigin
OBSERVACAOBlobTypeftMemo  TStringFieldqryOutrosAGENCIA	FieldNameAGENCIAOriginAGENCIASize  TStringFieldqryOutrosPRACA	FieldNamePRACAOriginPRACASize  TStringFieldqryOutrosCRA	FieldNameCRAOriginCRA	FixedChar	Size  TIntegerFieldqryOutrosSEQUENCIA	FieldName	SEQUENCIAOrigin	SEQUENCIA  TFloatFieldqryOutrosVALOR_DOC	FieldName	VALOR_DOCOrigin	VALOR_DOC  TFloatFieldqryOutrosVALOR_TED	FieldName	VALOR_TEDOrigin	VALOR_TED  TStringFieldqryOutrosNOMINAL	FieldNameNOMINALOriginNOMINAL	FixedChar	Size  TStringFieldqryOutrosFORCA_LEI	FieldName	FORCA_LEIOrigin	FORCA_LEI	FixedChar	Size  TStringFieldqryOutrosESPECIE	FieldNameESPECIEOriginESPECIE	FixedChar	Size   TFDQuery
qryTitulos
Connectiondm.conSISTEMASQL.StringsSELECT T.ID_ATO,       T.APRESENTANTE,       T.CODIGO_APRESENTANTE,       T.PROTOCOLO,       T.NUMERO_TITULO,       T.STATUS,       T.AGENCIA_CEDENTE,       T.NOSSO_NUMERO,       T.EXPORTADO,       T.RETORNO,       T.VALOR_TITULO,       T.SALDO_TITULO,       T.SALDO_PROTESTO,       T.DT_ENTRADA,       T.DT_DEVOLVIDO,/       T.DT_PROTOCOLO_CARTORIO AS DT_PROTOCOLO,       T.DT_PAGAMENTO,       T.DT_RETIRADO,       T.DT_SUSTADO,       T.DISTRIBUICAO,       T.TARIFA_BANCARIA,       T.VALOR_AR,       T.TOTAL,       T.DT_REGISTRO,       T.IRREGULARIDADE,       T.CONVENIO,       T.ELETRONICO,       T.CARTORIO,       P.ID_PORTADOR,       P.CODIGO,       P.SEQUENCIA,       P.AGENCIA,       P.VALOR_TED,       T.PROTOCOLO_CARTORIO,       T.POSTECIPADO  FROM TITULOS T INNER JOIN PORTADORES P'    ON T.CODIGO_APRESENTANTE = P.CODIGO WHERE (P.CRA = :CR1    OR  '*'   = :CR2)   AND ((P.BANCO = :BC1    OR   '*'     = :BC2)    OR  (P.CONVENIO = :CV1    OR  '*'         = :CV2))   AND (P.ID_PORTADOR = :ID1    OR  -1            = :ID2),   AND (T.DT_REGISTRO  BETWEEN :D11 AND :D12,    OR  T.DT_PAGAMENTO BETWEEN :D21 AND :D22,    OR  T.DT_RETIRADO  BETWEEN :D31 AND :D32,    OR  T.DT_SUSTADO   BETWEEN :D41 AND :D42-    OR  T.DT_DEVOLVIDO BETWEEN :D51 AND :D52)   AND (T.STATUS = 'PROTESTADO'    OR  T.STATUS = 'PAGO'    OR  T.STATUS = 'RETIRADO'    OR  T.STATUS = 'SUSTADO'    OR  T.STATUS = 'CANCELADO'    OR  T.STATUS = 'DEVOLVIDO')   AND (T.JUDICIAL <> 'S'    OR  T.JUDICIAL IS NULL)N   AND T.CARTORIO = (CASE WHEN (:CART01 = 0) THEN T.CARTORIO ELSE :CART02 END)    AND T.TIPO_APRESENTACAO = 'I'ORDER BY T.APRESENTANTE,         T.PROTOCOLO Left� Top� 	ParamDataPositionNameCR1DataTypeftString	ParamTypeptInput PositionNameCR2DataTypeftString	ParamTypeptInput PositionNameBC1DataTypeftString	ParamTypeptInput PositionNameBC2DataTypeftString	ParamTypeptInput PositionNameCV1DataTypeftString	ParamTypeptInput PositionNameCV2DataTypeftString	ParamTypeptInput PositionNameID1DataType	ftInteger	ParamTypeptInput PositionNameID2DataType	ftInteger	ParamTypeptInput Position	NameD11DataTypeftDate	ParamTypeptInput Position
NameD12DataTypeftDate	ParamTypeptInput PositionNameD21DataTypeftDate	ParamTypeptInput PositionNameD22DataTypeftDate	ParamTypeptInput PositionNameD31DataTypeftDate	ParamTypeptInput PositionNameD32DataTypeftDate	ParamTypeptInput PositionNameD41DataTypeftDate	ParamTypeptInput PositionNameD42DataTypeftDate	ParamTypeptInput PositionNameD51DataTypeftDate	ParamTypeptInput PositionNameD52DataTypeftDate	ParamTypeptInput PositionNameCART01DataType	ftInteger	ParamTypeptInput PositionNameCART02DataType	ftInteger	ParamTypeptInput   TIntegerFieldqryTitulosID_ATO	FieldNameID_ATOOriginID_ATOProviderFlags
pfInUpdate	pfInWherepfInKey Required	  TStringFieldqryTitulosAPRESENTANTE	FieldNameAPRESENTANTEOriginAPRESENTANTESized  TStringFieldqryTitulosCODIGO_APRESENTANTE	FieldNameCODIGO_APRESENTANTEOriginCODIGO_APRESENTANTESize
  TIntegerFieldqryTitulosPROTOCOLO	FieldName	PROTOCOLOOrigin	PROTOCOLO  TStringFieldqryTitulosNUMERO_TITULO	FieldNameNUMERO_TITULOOriginNUMERO_TITULO  TStringFieldqryTitulosSTATUS	FieldNameSTATUSOriginSTATUS  TStringFieldqryTitulosAGENCIA_CEDENTE	FieldNameAGENCIA_CEDENTEOriginAGENCIA_CEDENTESize  TStringFieldqryTitulosNOSSO_NUMERO	FieldNameNOSSO_NUMEROOriginNOSSO_NUMEROSize  TStringFieldqryTitulosEXPORTADO	FieldName	EXPORTADOOrigin	EXPORTADO	FixedChar	Size  TStringFieldqryTitulosRETORNO	FieldNameRETORNOOriginRETORNO  TFloatFieldqryTitulosVALOR_TITULO	FieldNameVALOR_TITULOOriginVALOR_TITULO  TFloatFieldqryTitulosSALDO_TITULO	FieldNameSALDO_TITULOOriginSALDO_TITULO  TFloatFieldqryTitulosSALDO_PROTESTO	FieldNameSALDO_PROTESTOOriginSALDO_PROTESTO  
TDateFieldqryTitulosDT_DEVOLVIDO	FieldNameDT_DEVOLVIDOOriginDT_DEVOLVIDO  
TDateFieldqryTitulosDT_PROTOCOLO	FieldNameDT_PROTOCOLOOriginDT_PROTOCOLO  
TDateFieldqryTitulosDT_PAGAMENTO	FieldNameDT_PAGAMENTOOriginDT_PAGAMENTO  
TDateFieldqryTitulosDT_RETIRADO	FieldNameDT_RETIRADOOriginDT_RETIRADO  
TDateFieldqryTitulosDT_SUSTADO	FieldName
DT_SUSTADOOrigin
DT_SUSTADO  TFloatFieldqryTitulosDISTRIBUICAO	FieldNameDISTRIBUICAOOriginDISTRIBUICAO  TFloatFieldqryTitulosTARIFA_BANCARIA	FieldNameTARIFA_BANCARIAOriginTARIFA_BANCARIA  TFloatFieldqryTitulosVALOR_AR	FieldNameVALOR_AROriginVALOR_AR  TFloatFieldqryTitulosTOTAL	FieldNameTOTALOriginTOTAL  
TDateFieldqryTitulosDT_REGISTRO	FieldNameDT_REGISTROOriginDT_REGISTRO  TIntegerFieldqryTitulosIRREGULARIDADE	FieldNameIRREGULARIDADEOriginIRREGULARIDADE  TStringFieldqryTitulosCONVENIO	FieldNameCONVENIOOriginCONVENIO	FixedChar	Size  TStringFieldqryTitulosELETRONICO	FieldName
ELETRONICOOrigin
ELETRONICO	FixedChar	Size  TIntegerFieldqryTitulosID_PORTADORAutoGenerateValue	arDefault	FieldNameID_PORTADOROriginID_PORTADORProviderFlags ReadOnly	  TStringFieldqryTitulosCODIGOAutoGenerateValue	arDefault	FieldNameCODIGOOriginCODIGOProviderFlags ReadOnly	Size
  TIntegerFieldqryTitulosSEQUENCIAAutoGenerateValue	arDefault	FieldName	SEQUENCIAOrigin	SEQUENCIAProviderFlags ReadOnly	  TStringFieldqryTitulosAGENCIAAutoGenerateValue	arDefault	FieldNameAGENCIAOriginAGENCIAProviderFlags ReadOnly	Size  TFloatFieldqryTitulosVALOR_TEDAutoGenerateValue	arDefault	FieldName	VALOR_TEDOrigin	VALOR_TEDProviderFlags ReadOnly	  TIntegerFieldqryTitulosCARTORIO	FieldNameCARTORIOOriginCARTORIO  
TDateFieldqryTitulosDT_ENTRADA	FieldName
DT_ENTRADAOrigin
DT_ENTRADA  TIntegerFieldqryTitulosPROTOCOLO_CARTORIO	FieldNamePROTOCOLO_CARTORIOOriginPROTOCOLO_CARTORIO  TStringFieldqryTitulosPOSTECIPADO	FieldNamePOSTECIPADOOriginPOSTECIPADO	FixedChar	Size   TDataSourcedsServentiaAgregadaDataSetdm.ServentiaAgregadaLeft	Top  
TPopupMenu
pmExportarLeft� Top�  	TMenuItem	MenuItem1CaptionRetorno InstrumentoOnClickMenuItem1Click    