object FServentiaAgregada: TFServentiaAgregada
  Left = 516
  Top = 152
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Serventia Agregada'
  ClientHeight = 561
  ClientWidth = 620
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 620
    Height = 513
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object edtCodigo: TsDBEdit
      Left = 12
      Top = 24
      Width = 72
      Height = 23
      Color = clWhite
      DataField = 'CODIGO'
      DataSource = dsServentiaAgregada
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'C'#243'digo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edtDescricao: TsDBEdit
      Left = 91
      Top = 24
      Width = 518
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'DESCRICAO'
      DataSource = dsServentiaAgregada
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Descri'#231#227'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edtCodigoDistribuicao: TsDBEdit
      Left = 12
      Top = 261
      Width = 72
      Height = 23
      Color = clWhite
      DataField = 'CODIGO_DISTRIBUICAO'
      DataSource = dsServentiaAgregada
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'C'#243'd. Distrib.'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object cedValorUltimoSaldo: TsDBCalcEdit
      Left = 91
      Top = 261
      Width = 85
      Height = 23
      AutoSize = False
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 12
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #218'ltimo Saldo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      DisplayFormat = '### ### ##0,00;-### ### ##0,00;0'
      DataSource = dsServentiaAgregada
      DataField = 'VALOR_ULTIMO_SALDO'
    end
    object dteDataUltimoSaldo: TsDBDateEdit
      Left = 266
      Top = 261
      Width = 100
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ReadOnly = True
      TabOrder = 14
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Dt. '#218'lt. Saldo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      DataField = 'DATA_ULTIMO_SALDO'
      DataSource = dsServentiaAgregada
    end
    object Grid: TwwDBGrid
      Left = 1
      Top = 326
      Width = 618
      Height = 186
      Selected.Strings = (
        'CODIGO_DISTRIBUICAO'#9'8'#9'C'#243'd. Distrib.'#9'F'
        'DESCRICAO'#9'45'#9'Serventia'#9'F'
        'QTD_ULTIMO_SALDO'#9'7'#9'Qtd. Total'#9'F'
        'VALOR_ULTIMO_SALDO'#9'10'#9#218'lt. Saldo'#9'F'
        'DATA_ULTIMO_SALDO'#9'10'#9'Dt. '#218'lt. Saldo'#9'T')
      IniAttributes.Delimiter = ';;'
      IniAttributes.UnicodeIniFile = False
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alBottom
      BorderStyle = bsNone
      DataSource = dsServentiaAgregada
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
      ReadOnly = True
      TabOrder = 19
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'Arial'
      TitleFont.Style = []
      TitleLines = 1
      TitleButtons = False
      UseTFields = False
      ExplicitTop = 327
    end
    object btnSalvarServ: TsBitBtn
      Left = 424
      Top = 292
      Width = 90
      Height = 25
      Cursor = crHandPoint
      Caption = 'Salvar'
      TabOrder = 17
      OnClick = btnSalvarServClick
      ImageIndex = 3
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object btnCancelarServ: TsBitBtn
      Left = 519
      Top = 292
      Width = 90
      Height = 25
      Cursor = crHandPoint
      Caption = 'Cancelar'
      TabOrder = 18
      OnClick = btnCancelarServClick
      ImageIndex = 34
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object edtQtdUltimoSaldo: TsDBEdit
      Left = 183
      Top = 261
      Width = 77
      Height = 23
      Color = clWhite
      DataField = 'QTD_ULTIMO_SALDO'
      DataSource = dsServentiaAgregada
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 13
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Qtd. '#218'lt. Saldo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object chbAtiva: TsDBCheckBox
      Left = 523
      Top = 261
      Width = 50
      Height = 19
      Caption = 'Ativa'
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 16
      OnExit = chbAtivaExit
      ImgChecked = 0
      ImgUnchecked = 0
      DataField = 'FLG_ATIVA'
      DataSource = dsServentiaAgregada
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object chbUsaProtesto: TsDBCheckBox
      Left = 372
      Top = 261
      Width = 145
      Height = 19
      Caption = 'Usa sistema Protesto'
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 15
      ImgChecked = 0
      ImgUnchecked = 0
      DataField = 'FLG_USAPROTESTO'
      DataSource = dsServentiaAgregada
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object edtEndereco: TsDBEdit
      Left = 12
      Top = 69
      Width = 597
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'LOGRADOURO'
      DataSource = dsServentiaAgregada
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Endere'#231'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object lcbCidade: TsDBLookupComboBox
      Left = 12
      Top = 113
      Width = 373
      Height = 23
      Color = clWhite
      DataField = 'CIDADE_LOGRADOURO'
      DataSource = dsServentiaAgregada
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      KeyField = 'CIDADE'
      ListField = 'CIDADE'
      ListSource = dsMunicipios
      ParentFont = False
      TabOrder = 3
      OnClick = lcbCidadeClick
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Cidade'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
    end
    object edtCep: TsDBEdit
      Left = 454
      Top = 113
      Width = 155
      Height = 23
      Color = clWhite
      DataField = 'CEP'
      DataSource = dsServentiaAgregada
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 5
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'CEP'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edtTelefone1: TsDBEdit
      Left = 357
      Top = 160
      Width = 123
      Height = 23
      Color = clWhite
      DataField = 'TELEFONE1'
      DataSource = dsServentiaAgregada
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 13
      ParentFont = False
      TabOrder = 7
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Telefone (Principal)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edtEmail1: TsDBEdit
      Left = 12
      Top = 209
      Width = 296
      Height = 23
      CharCase = ecLowerCase
      Color = clWhite
      DataField = 'EMAIL1'
      DataSource = dsServentiaAgregada
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'E-mail (Principal)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edtTitular: TsDBEdit
      Left = 12
      Top = 160
      Width = 339
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'NOME_TITULAR'
      DataSource = dsServentiaAgregada
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Titular do Cart'#243'rio'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edtTelefone2: TsDBEdit
      Left = 486
      Top = 160
      Width = 123
      Height = 23
      Color = clWhite
      DataField = 'TELEFONE2'
      DataSource = dsServentiaAgregada
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 13
      ParentFont = False
      TabOrder = 8
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Telefone (Alternativo)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edtEmail2: TsDBEdit
      Left = 314
      Top = 209
      Width = 295
      Height = 23
      CharCase = ecLowerCase
      Color = clWhite
      DataField = 'EMAIL2'
      DataSource = dsServentiaAgregada
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'E-mail (Alternativo)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object cbUF: TsDBComboBox
      Left = 391
      Top = 113
      Width = 57
      Height = 22
      Style = csOwnerDrawFixed
      Color = clWhite
      DataField = 'UF_LOGRADOURO'
      DataSource = dsServentiaAgregada
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Items.Strings = (
        'AC'
        'AL'
        'AM'
        'AP'
        'BA'
        'CE'
        'DF'
        'ES'
        'GO'
        'MA'
        'MG'
        'MS'
        'MT'
        'PA'
        'PB'
        'PE'
        'PI'
        'PR'
        'RJ'
        'RN'
        'RO'
        'RR'
        'RS'
        'SC'
        'SE'
        'SP'
        'TO')
      ParentFont = False
      TabOrder = 4
      BoundLabel.Active = True
      BoundLabel.Caption = 'UF'
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
  end
  object btOk: TsBitBtn
    Left = 454
    Top = 524
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Caption = 'Ok'
    TabOrder = 1
    OnClick = btOkClick
    SkinData.SkinSection = 'BUTTON'
  end
  object btCancelar: TsBitBtn
    Left = 534
    Top = 524
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Caption = 'Cancelar'
    TabOrder = 2
    OnClick = btCancelarClick
    SkinData.SkinSection = 'BUTTON'
  end
  object btIncluir: TsBitBtn
    Left = 12
    Top = 524
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Caption = 'Incluir'
    TabOrder = 3
    OnClick = btIncluirClick
    ImageIndex = 6
    Images = dm.Imagens
    SkinData.SkinSection = 'BUTTON'
  end
  object btAlterar: TsBitBtn
    Left = 92
    Top = 524
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Caption = 'Alterar'
    TabOrder = 4
    OnClick = btAlterarClick
    ImageIndex = 8
    Images = dm.Imagens
    SkinData.SkinSection = 'BUTTON'
  end
  object btExcluir: TsBitBtn
    Left = 172
    Top = 524
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Caption = 'Excluir'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333333333333333333333333FFF33FF333FFF339993370733
      999333777FF37FF377733339993000399933333777F777F77733333399970799
      93333333777F7377733333333999399933333333377737773333333333990993
      3333333333737F73333333333331013333333333333777FF3333333333910193
      333333333337773FF3333333399000993333333337377737FF33333399900099
      93333333773777377FF333399930003999333337773777F777FF339993370733
      9993337773337333777333333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333333333333}
    NumGlyphs = 2
    TabOrder = 5
    OnClick = btExcluirClick
    SkinData.SkinSection = 'BUTTON'
  end
  object dsServentiaAgregada: TDataSource
    DataSet = dm.ServentiaAgregada
    Left = 500
    Top = 384
  end
  object dsMunicipios: TDataSource
    DataSet = dm.Municipios
    Left = 500
    Top = 447
  end
end
