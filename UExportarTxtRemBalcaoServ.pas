{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UExportarTxtRemBalcaoServ.pas
  Descricao:   Tela de geracao e exportacao de Arquivos de Remessa para Serventias Agregadas
  Author   :   Cristina
  Date:        24-ago-2016
  Last Update: 01-mar-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UExportarTxtRemBalcaoServ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, sStatusBar, DBCtrls, sDBLookupComboBox, sDBEdit,
  StdCtrls, Mask, sMaskEdit, sCustomComboEdit, sToolEdit, Buttons, sBitBtn,
  sGroupBox, sPanel, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, FMTBcd, SqlExpr,
  Provider, DB, DBClient, sCheckBox, RxMemDS, StrUtils, Menus,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  System.DateUtils;

type
  TFExportarTxtRemBalcaoServ = class(TForm)
    Panel1: TPanel;
    dbgTitulos: TwwDBGrid;
    P1: TsPanel;
    RgTipo: TsRadioGroup;
    btnVisualizar: TsBitBtn;
    btnExportar: TsBitBtn;
    dteDataEnt: TsDateEdit;
    stbBarra: TsStatusBar;
    dsTitulos: TDataSource;
    cdsTitulos: TClientDataSet;
    dspTitulos: TDataSetProvider;
    cdsTitulosID_ATO: TIntegerField;
    cdsTitulosCODIGO: TIntegerField;
    cdsTitulosRECIBO: TIntegerField;
    cdsTitulosDT_ENTRADA: TDateField;
    cdsTitulosDT_PROTOCOLO: TDateField;
    cdsTitulosPROTOCOLO: TIntegerField;
    cdsTitulosLIVRO_PROTOCOLO: TIntegerField;
    cdsTitulosFOLHA_PROTOCOLO: TStringField;
    cdsTitulosDT_PRAZO: TDateField;
    cdsTitulosDT_REGISTRO: TDateField;
    cdsTitulosREGISTRO: TIntegerField;
    cdsTitulosLIVRO_REGISTRO: TIntegerField;
    cdsTitulosFOLHA_REGISTRO: TStringField;
    cdsTitulosSELO_REGISTRO: TStringField;
    cdsTitulosDT_PAGAMENTO: TDateField;
    cdsTitulosSELO_PAGAMENTO: TStringField;
    cdsTitulosRECIBO_PAGAMENTO: TIntegerField;
    cdsTitulosCOBRANCA: TStringField;
    cdsTitulosEMOLUMENTOS: TFloatField;
    cdsTitulosFETJ: TFloatField;
    cdsTitulosFUNDPERJ: TFloatField;
    cdsTitulosFUNPERJ: TFloatField;
    cdsTitulosFUNARPEN: TFloatField;
    cdsTitulosPMCMV: TFloatField;
    cdsTitulosISS: TFloatField;
    cdsTitulosMUTUA: TFloatField;
    cdsTitulosDISTRIBUICAO: TFloatField;
    cdsTitulosACOTERJ: TFloatField;
    cdsTitulosTOTAL: TFloatField;
    cdsTitulosTIPO_PROTESTO: TIntegerField;
    cdsTitulosTIPO_TITULO: TIntegerField;
    cdsTitulosNUMERO_TITULO: TStringField;
    cdsTitulosDT_TITULO: TDateField;
    cdsTitulosBANCO: TStringField;
    cdsTitulosAGENCIA: TStringField;
    cdsTitulosCONTA: TStringField;
    cdsTitulosVALOR_TITULO: TFloatField;
    cdsTitulosSALDO_PROTESTO: TFloatField;
    cdsTitulosDT_VENCIMENTO: TDateField;
    cdsTitulosCONVENIO: TStringField;
    cdsTitulosTIPO_APRESENTACAO: TStringField;
    cdsTitulosDT_ENVIO: TDateField;
    cdsTitulosCODIGO_APRESENTANTE: TStringField;
    cdsTitulosTIPO_INTIMACAO: TStringField;
    cdsTitulosMOTIVO_INTIMACAO: TMemoField;
    cdsTitulosDT_INTIMACAO: TDateField;
    cdsTitulosDT_PUBLICACAO: TDateField;
    cdsTitulosVALOR_PAGAMENTO: TFloatField;
    cdsTitulosAPRESENTANTE: TStringField;
    cdsTitulosCPF_CNPJ_APRESENTANTE: TStringField;
    cdsTitulosTIPO_APRESENTANTE: TStringField;
    cdsTitulosCEDENTE: TStringField;
    cdsTitulosNOSSO_NUMERO: TStringField;
    cdsTitulosSACADOR: TStringField;
    cdsTitulosDEVEDOR: TStringField;
    cdsTitulosCPF_CNPJ_DEVEDOR: TStringField;
    cdsTitulosTIPO_DEVEDOR: TStringField;
    cdsTitulosAGENCIA_CEDENTE: TStringField;
    cdsTitulosPRACA_PROTESTO: TStringField;
    cdsTitulosTIPO_ENDOSSO: TStringField;
    cdsTitulosACEITE: TStringField;
    cdsTitulosCPF_ESCREVENTE: TStringField;
    cdsTitulosCPF_ESCREVENTE_PG: TStringField;
    cdsTitulosOBSERVACAO: TMemoField;
    cdsTitulosDT_SUSTADO: TDateField;
    cdsTitulosDT_RETIRADO: TDateField;
    cdsTitulosSTATUS: TStringField;
    cdsTitulosPROTESTADO: TStringField;
    cdsTitulosENVIADO_APONTAMENTO: TStringField;
    cdsTitulosENVIADO_PROTESTO: TStringField;
    cdsTitulosENVIADO_PAGAMENTO: TStringField;
    cdsTitulosENVIADO_RETIRADO: TStringField;
    cdsTitulosENVIADO_SUSTADO: TStringField;
    cdsTitulosENVIADO_DEVOLVIDO: TStringField;
    cdsTitulosEXPORTADO: TStringField;
    cdsTitulosAVALISTA_DEVEDOR: TStringField;
    cdsTitulosFINS_FALIMENTARES: TStringField;
    cdsTitulosTARIFA_BANCARIA: TFloatField;
    cdsTitulosFORMA_PAGAMENTO: TStringField;
    cdsTitulosNUMERO_PAGAMENTO: TStringField;
    cdsTitulosARQUIVO: TStringField;
    cdsTitulosRETORNO: TStringField;
    cdsTitulosDT_DEVOLVIDO: TDateField;
    cdsTitulosCPF_CNPJ_SACADOR: TStringField;
    cdsTitulosID_MSG: TIntegerField;
    cdsTitulosVALOR_AR: TFloatField;
    cdsTitulosELETRONICO: TStringField;
    cdsTitulosIRREGULARIDADE: TIntegerField;
    cdsTitulosAVISTA: TStringField;
    cdsTitulosSALDO_TITULO: TFloatField;
    cdsTitulosTIPO_SUSTACAO: TStringField;
    cdsTitulosRETORNO_PROTESTO: TStringField;
    cdsTitulosDT_RETORNO_PROTESTO: TDateField;
    cdsTitulosDT_DEFINITIVA: TDateField;
    cdsTitulosJUDICIAL: TStringField;
    cdsTitulosALEATORIO_PROTESTO: TStringField;
    cdsTitulosALEATORIO_SOLUCAO: TStringField;
    cdsTitulosCCT: TStringField;
    cdsTitulosDETERMINACAO: TStringField;
    cdsTitulosANTIGO: TStringField;
    cdsTitulosLETRA: TStringField;
    cdsTitulosPROTOCOLO_CARTORIO: TIntegerField;
    cdsTitulosARQUIVO_CARTORIO: TStringField;
    cdsTitulosRETORNO_CARTORIO: TStringField;
    cdsTitulosDT_DEVOLVIDO_CARTORIO: TDateField;
    cdsTitulosSELECIONADO: TBooleanField;
    cdsTitulosCOD_IRREGULARIDADE: TIntegerField;
    cdsTitulosSERVENTIA_AGREG: TStringField;
    cdsDuplicado: TClientDataSet;
    dsDuplicado: TDataSource;
    dspDuplicado: TDataSetProvider;
    cdsTitulosSEQUENCIAL_ARQ: TIntegerField;
    cdsTitulosSEQ_BANCO: TIntegerField;
    PM: TPopupMenu;
    MarcarTodos: TMenuItem;
    DesmarcarTodos: TMenuItem;
    qryDuplicado: TFDQuery;
    qryTitulos: TFDQuery;
    cdsTitulosCARTORIO: TIntegerField;
    cdsTitulosDT_PROTOCOLO_CARTORIO: TDateField;
    cdsTitulosDIAS_AVISTA: TIntegerField;
    cdsTitulosFLG_SALDO: TStringField;
    cdsTitulosID_ATO_1: TIntegerField;
    cdsTitulosID_DEVEDOR: TIntegerField;
    cdsTitulosORDEM: TIntegerField;
    cdsTitulosNOME: TStringField;
    cdsTitulosTIPO: TStringField;
    cdsTitulosDOCUMENTO: TStringField;
    cdsTitulosIFP_DETRAN: TStringField;
    cdsTitulosIDENTIDADE: TStringField;
    cdsTitulosDT_EMISSAO: TDateField;
    cdsTitulosORGAO: TStringField;
    cdsTitulosCEP: TStringField;
    cdsTitulosENDERECO: TStringField;
    cdsTitulosBAIRRO: TStringField;
    cdsTitulosMUNICIPIO: TStringField;
    cdsTitulosUF: TStringField;
    cdsTitulosIGNORADO: TStringField;
    cdsTitulosJUSTIFICATIVA: TMemoField;
    cdsTitulosTELEFONE: TStringField;
    cdsTitulosPOSTECIPADO: TStringField;
    procedure stbBarraMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PortadorAux(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnVisualizarClick(Sender: TObject);
    procedure dbgTitulosMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure btnExportarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dteDataEntKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MarcarTodosClick(Sender: TObject);
    procedure DesmarcarTodosClick(Sender: TObject);
    procedure dbgTitulosAddSelectColumn(Sender: TObject; AField: TField;
      var NodeGroups: string; var canAdd: Boolean);
    procedure cdsTitulosENVIADO_APONTAMENTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsTitulosENVIADO_PROTESTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsTitulosENVIADO_PAGAMENTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsTitulosENVIADO_RETIRADOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsTitulosENVIADO_SUSTADOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure cdsTitulosENVIADO_DEVOLVIDOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cdsTitulosENVIADO_APONTAMENTOSetText(Sender: TField;
      const Text: string);
    procedure cdsTitulosENVIADO_PROTESTOSetText(Sender: TField;
      const Text: string);
    procedure cdsTitulosENVIADO_PAGAMENTOSetText(Sender: TField;
      const Text: string);
    procedure cdsTitulosENVIADO_RETIRADOSetText(Sender: TField;
      const Text: string);
    procedure cdsTitulosENVIADO_SUSTADOSetText(Sender: TField;
      const Text: string);
    procedure cdsTitulosENVIADO_DEVOLVIDOSetText(Sender: TField;
      const Text: string);
    procedure dbgTitulosExit(Sender: TObject);
    procedure dbgTitulosMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    LstArqD,
    LstArqB: TListBox;

    NomeArqVazio,
    LstTitulos: String;

    procedure AbrirListaTitulos;
    procedure MarcarDesmarcarTitulos(Marcar: Boolean);
    procedure VerificaMarcados(Check: Boolean; Num_Titulo, Nosso_Numero,
                               ArquivoB, ArquivoD, TpApresentacao: String);

    function VerificarIrregularidades: Boolean;
  public
    { Public declarations }
  end;

var
  FExportarTxtRemBalcaoServ: TFExportarTxtRemBalcaoServ;

implementation

uses UDM, UGDM, UGeral, UPF;

{$R *.dfm}

procedure TFExportarTxtRemBalcaoServ.AbrirListaTitulos;
var
  sQry: String;
begin
  PF.Aguarde(True);

  sQry := '';

  cdsTitulos.Close;

  sQry := 'SELECT T.*, ' +
          '       D.* ' +
          '  FROM TITULOS T ' +
          ' INNER JOIN DEVEDORES D ' +
          '    ON T.ID_ATO = D.ID_ATO ';

  if dm.ValorParametro(100) = 'S' then
    sQry := sQry + ' WHERE T.TIPO_APRESENTACAO = ' + QuotedStr('B') +
                   '   AND T.STATUS = ' + QuotedStr('Aceito') +  //APONTADO
                   '   AND (T.ARQUIVO_CARTORIO IS NULL ' +
                   '    OR  TRIM(T.ARQUIVO_CARTORIO) = ' + QuotedStr('') + ') ' +
                   '   AND (T.RETORNO_CARTORIO IS NULL ' +
                   '    OR  TRIM(T.RETORNO_CARTORIO) = ' + QuotedStr('') + ') ' +
                   '   AND T.DT_ENTRADA = :DATA_ENT'
  else
    sQry := sQry + ' WHERE T.STATUS = ' + QuotedStr('Aceito') +  //APONTADO
                   '   AND (T.ARQUIVO_CARTORIO IS NULL ' +
                   '    OR  TRIM(T.ARQUIVO_CARTORIO) = ' + QuotedStr('') + ') ' +
                   '   AND (T.RETORNO_CARTORIO IS NULL ' +
                   '    OR  TRIM(T.RETORNO_CARTORIO) = ' + QuotedStr('') + ') ' +
                   '   AND T.DT_ENTRADA = :DATA_ENT';

  qryTitulos.SQL.Text := sQry;

  qryTitulos.SQL.Add('ORDER BY T.CARTORIO, T.APRESENTANTE, T.CODIGO_APRESENTANTE, T.PROTOCOLO, D.ORDEM');

  cdsTitulos.Params.ParamByName('DATA_ENT').Value := dteDataEnt.Date;
  cdsTitulos.Open;

  cdsTitulos.DisableControls;

  cdsTitulos.First;

  while not cdsTitulos.Eof do
  begin
    if Trim(LstTitulos) = '' then
      LstTitulos := cdsTitulosID_ATO.AsString
    else
      LstTitulos := LstTitulos + ', ' + cdsTitulosID_ATO.AsString;

    cdsTitulos.Next;
  end;

  cdsTitulos.EnableControls;

  dm.PortadorAux.Close;
  dm.PortadorAux.Open;

  MarcarDesmarcarTitulos(True);

  PF.Aguarde(False);

  stbBarra.Panels[0].Text := IntToStr(cdsTitulos.RecordCount) +
                             ' T�tulos encontrados';

  if cdsTitulos.RecordCount > 0 then
    btnExportar.Enabled := True
  else
    btnExportar.Enabled := False;
end;

procedure TFExportarTxtRemBalcaoServ.stbBarraMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  Screen.Cursor := crDefault;
end;

procedure TFExportarTxtRemBalcaoServ.PortadorAux(Sender: TObject);
begin
  if dm.ValorParametro(100) = 'S' then
    Self.Caption := 'Exporta��o de Arquivos de Remessa para Serventias Agregadas - T�tulos Balc�o'
  else
    Self.Caption := 'Exporta��o de Arquivos de Remessa para Serventias Agregadas';

  dteDataEnt.Date := Date;

  btnExportar.Enabled := False;
end;

procedure TFExportarTxtRemBalcaoServ.MarcarDesmarcarTitulos(Marcar: Boolean);
begin
  if not cdsTitulos.Active then
    Exit;

  if cdsTitulos.RecordCount = 0 then
    Exit;

  cdsTitulos.DisableControls;
  cdsTitulos.First;

  while not cdsTitulos.Eof do
  begin
    cdsTitulos.Edit;

    if cdsTitulosCOD_IRREGULARIDADE.IsNull then
    begin
      if Marcar then
        cdsTitulosSELECIONADO.AsBoolean := True
      else
        cdsTitulosSELECIONADO.AsBoolean := False;
    end
    else
      cdsTitulosSELECIONADO.AsBoolean := False;

    cdsTitulos.Post;

    VerificaMarcados(cdsTitulosSELECIONADO.AsBoolean,
                     cdsTitulosNUMERO_TITULO.AsString,
                     cdsTitulosNOSSO_NUMERO.AsString,
                     cdsTitulosARQUIVO.AsString,
                     cdsTitulosARQUIVO_CARTORIO.AsString,
                     cdsTitulosTIPO_APRESENTACAO.AsString);

    cdsTitulos.Next;
  end;

  cdsTitulos.EnableControls;
end;

procedure TFExportarTxtRemBalcaoServ.MarcarTodosClick(Sender: TObject);
begin
  MarcarDesmarcarTitulos(True);
end;

procedure TFExportarTxtRemBalcaoServ.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Self.Close;
end;

procedure TFExportarTxtRemBalcaoServ.btnVisualizarClick(Sender: TObject);
begin
  LstTitulos := '';

  AbrirListaTitulos;

  btnExportar.Enabled := True;
end;

procedure TFExportarTxtRemBalcaoServ.dbgTitulosAddSelectColumn(Sender: TObject;
  AField: TField; var NodeGroups: string; var canAdd: Boolean);
begin
  if cdsTitulos.Active then
    VerificaMarcados(cdsTitulosSELECIONADO.AsBoolean,
                     cdsTitulosNUMERO_TITULO.AsString,
                     cdsTitulosNOSSO_NUMERO.AsString,
                     cdsTitulosARQUIVO.AsString,
                     cdsTitulosARQUIVO_CARTORIO.AsString,
                     cdsTitulosTIPO_APRESENTACAO.AsString);
end;

procedure TFExportarTxtRemBalcaoServ.dbgTitulosExit(Sender: TObject);
begin
  if cdsTitulos.State in [dsEdit,dsInsert] then
    cdsTitulos.Post;
end;

procedure TFExportarTxtRemBalcaoServ.dbgTitulosMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if X in [0..40] then
    Screen.Cursor := crHandPoint
  else
    Screen.Cursor := crDefault;
end;

procedure TFExportarTxtRemBalcaoServ.dbgTitulosMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  VerificaMarcados(cdsTitulosSELECIONADO.AsBoolean,
                   cdsTitulosNUMERO_TITULO.AsString,
                   cdsTitulosNOSSO_NUMERO.AsString,
                   cdsTitulosARQUIVO.AsString,
                   cdsTitulosARQUIVO_CARTORIO.AsString,
                   cdsTitulosTIPO_APRESENTACAO.AsString);
end;

function TFExportarTxtRemBalcaoServ.VerificarIrregularidades: Boolean;
var
  sMsg, sMsgErro, sTitulo: String;
  iPos: Integer;
  lDuplicidade: Boolean;
begin
  Result := False;

  lDuplicidade := False;

  sMsg     := '';
  sMsgErro := '';
  sTitulo  := '';

  iPos := 0;

  Screen.Cursor := crHourGlass;

  PF.Aguarde(True);

  dm.Criticas.Close;
  dm.Criticas.Open;

  cdsTitulos.DisableControls;

  iPos := cdsTitulos.RecNo;

  cdsTitulos.First;

  while not cdsTitulos.Eof do
  begin
    Application.ProcessMessages;

    if cdsTitulos.FieldByName('SELECIONADO').AsBoolean then
    begin
      //Data de Apresentacao
      if (cdsTitulosDT_ENTRADA.AsDateTime < cdsTitulosDT_VENCIMENTO.AsDateTime) and
        (cdsTitulosAVISTA.AsString = 'N') then
      begin
        if Trim(sMsgErro) = '' then
        begin
          sMsgErro := '- DATA DE APRESENTA��O inferior � DATA DE VENCIMENTO';
          sTitulo  := 'T�tulo: ' + cdsTitulosNUMERO_TITULO.AsString + ' (' +
                      'Protocolo: ' + cdsTitulosPROTOCOLO.AsString + ')';
        end
        else
          sMsgErro := (sMsgErro + #13#10 +
                       '- DATA DE APRESENTA��O inferior � DATA DE VENCIMENTO');

        cdsTitulos.Edit;
        cdsTitulosCOD_IRREGULARIDADE.AsInteger := 1;
        cdsTitulos.Post;
      end;

      //CPF/CNPJ do Sacado
      if cdsTitulosCPF_CNPJ_DEVEDOR.AsString = '' then
      begin
        if Trim(sMsgErro) = '' then
        begin
          sMsgErro := '- CPF/CNPJ do Sacado INV�LIDO/INCORRETO';
          sTitulo  := 'T�tulo: ' + cdsTitulosNUMERO_TITULO.AsString + ' (' +
                      'Protocolo: ' + cdsTitulosPROTOCOLO.AsString + ')';
        end
        else
          sMsgErro := (sMsgErro + #13#10 +
                       '- CPF/CNPJ do Sacado INV�LIDO/INCORRETO');

        cdsTitulos.Edit;
        cdsTitulosCOD_IRREGULARIDADE.AsInteger := 7;
        cdsTitulos.Post;
      end
      else if cdsTitulosTIPO_DEVEDOR.AsString = 'F' then
      begin
        if not PF.VerificarCPF(cdsTitulosCPF_CNPJ_DEVEDOR.AsString) then
        begin
          if Trim(sMsgErro) = '' then
          begin
            sMsgErro := '- CPF/CNPJ do Sacado INV�LIDO/INCORRETO';
            sTitulo  := 'T�tulo: ' + cdsTitulosNUMERO_TITULO.AsString + ' (' +
                        'Protocolo: ' + cdsTitulosPROTOCOLO.AsString + ')';
          end
          else
            sMsgErro := (sMsgErro + #13#10 +
                         '- CPF/CNPJ do Sacado INV�LIDO/INCORRETO');

          cdsTitulos.Edit;
          cdsTitulosCOD_IRREGULARIDADE.AsInteger := 7;
          cdsTitulos.Post;
        end;
      end
      else
      begin
        if not PF.VerificarCNPJ(cdsTitulosCPF_CNPJ_DEVEDOR.AsString) then
        begin
          if Trim(sMsgErro) = '' then
          begin
            sMsgErro := '- CPF/CNPJ do Sacado INV�LIDO/INCORRETO';
            sTitulo  := 'T�tulo: ' + cdsTitulosNUMERO_TITULO.AsString + ' (' +
                        'Protocolo: ' + cdsTitulosPROTOCOLO.AsString + ')';
          end
          else
            sMsgErro := (sMsgErro + #13#10 +
                         '- CPF/CNPJ do Sacado INV�LIDO/INCORRETO');

          cdsTitulos.Edit;
          cdsTitulosCOD_IRREGULARIDADE.AsInteger := 7;
          cdsTitulos.Post;
        end;
      end;

      //Praca de Protesto
      if (cdsTitulosPRACA_PROTESTO.AsString <> dm.ValorParametro(6)) and
       (not dm.Pracas.Locate('NOME', cdsTitulosPRACA_PROTESTO.AsString, [loPartialKey, loCaseInsensitive])) then
      begin
        if Trim(sMsgErro) = '' then
        begin
          sMsgErro := '- PRA�A de pagamento incompat�vel (' +
                      cdsTitulosPRACA_PROTESTO.AsString + ')';
          sTitulo  := 'T�tulo: ' + cdsTitulosNUMERO_TITULO.AsString + ' (' +
                      'Protocolo: ' + cdsTitulosPROTOCOLO.AsString + ')';
        end
        else
          sMsgErro := (sMsgErro + #13#10 +
                       '- PRA�A de pagamento incompat�vel (' +
                       cdsTitulosPRACA_PROTESTO.AsString + ')');

        cdsTitulos.Edit;
        cdsTitulosCOD_IRREGULARIDADE.AsInteger := 15;
        cdsTitulos.Post;
      end;

      //Numero do Titulo
      if cdsTitulosNUMERO_TITULO.AsString = '' then
      begin
        if Trim(sMsgErro) = '' then
        begin
          sMsgErro := '- Falta o N�MERO DO T�TULO';
          sTitulo  := 'T�tulo: * (' +
                      'Protocolo: ' + cdsTitulosPROTOCOLO.AsString + ')';
        end
        else
          sMsgErro := (sMsgErro + #13#10 +
                       '- Falta o N�MERO DO T�TULO');

        cdsTitulos.Edit;
        cdsTitulosCOD_IRREGULARIDADE.AsInteger := 16;
        cdsTitulos.Post;
      end;

      //Data do Titulo
      if cdsTitulosDT_TITULO.AsDateTime = 0 then
      begin
        if Trim(sMsgErro) = '' then
        begin
          sMsgErro := '- Falta a DATA DE EMISS�O do T�tulo';
          sTitulo  := 'T�tulo: ' + cdsTitulosNUMERO_TITULO.AsString + ' (' +
                      'Protocolo: ' + cdsTitulosPROTOCOLO.AsString + ')';
        end
        else
          sMsgErro := (sMsgErro + #13#10 +
                       '- Falta a DATA DE EMISS�O do T�tulo');

        cdsTitulos.Edit;
        cdsTitulosCOD_IRREGULARIDADE.AsInteger := 18;
        cdsTitulos.Post;
      end;

      //Data do Titulo x Data do Vencimento
      if (cdsTitulosDT_TITULO.AsDateTime > cdsTitulosDT_VENCIMENTO.AsDateTime) and
        (cdsTitulosAVISTA.AsString = 'N') then
      begin
        if Trim(sMsgErro) = '' then
        begin
          sMsgErro := '- DATA DE EMISS�O posterior � DATA DE VENCIMENTO';
          sTitulo  := 'T�tulo: ' + cdsTitulosNUMERO_TITULO.AsString + ' (' +
                      'Protocolo: ' + cdsTitulosPROTOCOLO.AsString + ')';
        end
        else
          sMsgErro := (sMsgErro + #13#10 +
                       '- DATA DE EMISS�O posterior � DATA DE VENCIMENTO');

        cdsTitulos.Edit;
        cdsTitulosCOD_IRREGULARIDADE.AsInteger := 20;
        cdsTitulos.Post;
      end;

      //Titulo x Sacador
      if cdsTitulosDEVEDOR.AsString = cdsTitulosSACADOR.AsString then
      begin
        if Trim(sMsgErro) = '' then
        begin
          sMsgErro := '- SACADO e SACADOR/AVALISTA s�o a mesma pessoa';
          sTitulo  := 'T�tulo: ' + cdsTitulosNUMERO_TITULO.AsString + ' (' +
                      'Protocolo: ' + cdsTitulosPROTOCOLO.AsString + ')';
        end
        else
          sMsgErro := (sMsgErro + #13#10 +
                       '- SACADO e SACADOR/AVALISTA s�o a mesma pessoa');

        cdsTitulos.Edit;
        cdsTitulosCOD_IRREGULARIDADE.AsInteger := 28;
        cdsTitulos.Post;
      end;

      //Data do Vencimento
      if (cdsTitulosDT_VENCIMENTO.AsDateTime = 0) and
        (cdsTitulosAVISTA.AsString = 'N') then
      begin
        if Trim(sMsgErro) = '' then
        begin
          sMsgErro := '- Falta a DATA DE VENCIMENTO do T�tulo';
          sTitulo  := 'T�tulo: ' + cdsTitulosNUMERO_TITULO.AsString + ' (' +
                      'Protocolo: ' + cdsTitulosPROTOCOLO.AsString + ')';
        end
        else
          sMsgErro := (sMsgErro + #13#10 +
                       '- Falta a DATA DE VENCIMENTO do T�tulo');

        cdsTitulos.Edit;
        cdsTitulosCOD_IRREGULARIDADE.AsInteger := 33;
        cdsTitulos.Post;
      end;

      //Duplicidade de Titulos
      if cdsTitulosCODIGO_APRESENTANTE.AsString <> '' then
      begin
        cdsDuplicado.Close;
        cdsDuplicado.Params.ParamByName('NUMERO').AsString   := cdsTitulosNUMERO_TITULO.AsString;
        cdsDuplicado.Params.ParamByName('CODIGO').AsString   := cdsTitulosCODIGO_APRESENTANTE.AsString;
        cdsDuplicado.Params.ParamByName('VALOR').AsFloat     := cdsTitulosVALOR_TITULO.AsFloat;
        cdsDuplicado.Params.ParamByName('DATA').AsDate       := cdsTitulosDT_TITULO.AsDateTime;
        cdsDuplicado.Params.ParamByName('VENCIMENTO').AsDate := cdsTitulosDT_VENCIMENTO.AsDateTime;
        cdsDuplicado.Open;

        if cdsDuplicado.RecordCount > 1 then
        begin
          if Trim(sMsgErro) = '' then
          begin
            sMsgErro := '- T�tulo apresentado em DUPLICIDADE';
            sTitulo  := 'T�tulo: ' + cdsTitulosNUMERO_TITULO.AsString + ' (' +
                        'Protocolo: ' + cdsTitulosPROTOCOLO.AsString + ')';
          end
          else
            sMsgErro := (sMsgErro + #13#10 +
                         '- T�tulo apresentado em DUPLICIDADE');

          cdsTitulos.Edit;
          cdsTitulosCOD_IRREGULARIDADE.AsInteger := 52;
          cdsTitulos.Post;

          lDuplicidade := True;
        end;
      end;

      if Trim(sTitulo) <> '' then
        sMsg := (sTitulo + #13#10 + sMsgErro);

      sTitulo  := '';
      sMsgErro := '';
    end;

    cdsTitulos.Next;
  end;

  cdsTitulos.RecNo := iPos;
  cdsTitulos.EnableControls;

  PF.Aguarde(False);

  Screen.Cursor := crDefault;

  if Trim(sMsg) <> '' then
  begin
    if lDuplicidade then
    begin
      if Application.MessageBox(PChar(sMsg + #13#10 + #13#10 + 'Deseja exportar mesmo assim?'),
                                'Aviso',
                                MB_YESNO + MB_ICONWARNING) = ID_YES then
        Result := False
      else
        Result := True;
    end
    else
    begin
      Result := True;
      Application.MessageBox(PChar(sMsg), 'Aviso', MB_OK + MB_ICONERROR);
    end;
  end;
end;

procedure TFExportarTxtRemBalcaoServ.btnExportarClick(Sender: TObject);
var
  sValor, sMsgRet: String;
  cSomatVlrSeg: Double;
  iSel, iTotGeral, iTotDev, iTotCmp12, iTotCmp12O, iSeqReg: Integer;
  lErro: Boolean;
begin
  { Essa rotina gera os arquivos D para que serao enviados para
    as Serventias Agregadas }
  if VerificarIrregularidades then
    Exit;

  dm.ServentiaAgregada.Close;
  dm.ServentiaAgregada.Open;

  if dm.ServentiaAgregada.IsEmpty then
  begin
    GR.Aviso('FALTA CADASTRAR AS SERVENTIAS AGREGADAS!');
    Exit;
  end;

  iSel := 0;

  PF.Aguarde(True);

  dm.mdLinhaArqD.Close;
  dm.mdLinhaArqD.Open;

  dm.mdArquivoD.Close;
  dm.mdArquivoD.Open;

  sValor  := '';
  sMsgRet := '';

  dm.ServentiaAgregada.IndexFieldNames := 'CODIGO_DISTRIBUICAO';

  dm.ServentiaAgregada.First;

  while not dm.ServentiaAgregada.Eof do
  begin
    iSeqReg := 1;

    dm.PortadorAux.SortOnFields('SERVENTIA_AGREGADA; CODIGO; NOME', True, True);

    dm.PortadorAux.First;

    while not dm.PortadorAux.Eof do
    begin
      if dm.PortadorAuxSERVENTIA_AGREGADA.AsInteger = dm.ServentiaAgregadaCODIGO_DISTRIBUICAO.AsInteger then
      begin
        iTotGeral  := 0;
        iTotDev    := 0;
        iTotCmp12  := 0;
        iTotCmp12O := 0;

        cSomatVlrSeg := 0;

        cdsTitulos.DisableControls;

        cdsTitulos.IndexFieldNames := 'CARTORIO; CODIGO_APRESENTANTE; APRESENTANTE; ARQUIVO; PROTOCOLO; ORDEM';

        cdsTitulos.First;

        while not cdsTitulos.Eof do
        begin
          if (cdsTitulosCARTORIO.AsInteger = dm.ServentiaAgregadaCODIGO_DISTRIBUICAO.AsInteger) and
            (cdsTitulosCARTORIO.AsInteger = dm.PortadorAuxSERVENTIA_AGREGADA.AsInteger) and
            (cdsTitulosCODIGO_APRESENTANTE.AsString = dm.PortadorAuxCODIGO.AsString) and
            (Trim(cdsTitulosAPRESENTANTE.AsString) = Trim(dm.PortadorAuxNOME.AsString)) then
          begin
            { TRANSACAO }
            Inc(iSel);

            Inc(iSeqReg);
            Inc(iTotGeral);

            cSomatVlrSeg := (cSomatVlrSeg + cdsTitulosSALDO_TITULO.AsFloat);

            dm.mdArquivoD.Append;
            dm.mdArquivoDID_REGISTRO.AsString          := '1';
            dm.mdArquivoDNUM_COD_PORTADOR.AsString     := GR.Zeros(cdsTitulosCODIGO_APRESENTANTE.AsString, 'D', 3);
            dm.mdArquivoDNOME_PORTADOR.AsString        := cdsTitulosAPRESENTANTE.AsString;
            dm.mdArquivoDCOD_CEDENTE.AsString          := GR.Zeros(cdsTitulosAGENCIA_CEDENTE.AsString, 'D', 15);
            dm.mdArquivoDNOME_CEDENTE.AsString         := PF.Espaco(cdsTitulosCEDENTE.AsString, 'E', 45);
            dm.mdArquivoDARQUIVO_CRA.AsString          := cdsTitulosARQUIVO.AsString;

            //Sacador - Inicio
            dm.Sacadores.Close;
            dm.Sacadores.Params.ParamByName('ID_ATO').AsInteger := cdsTitulosID_ATO.AsInteger;
            dm.Sacadores.Open;

            dm.mdArquivoDNOME_SACADOR.AsString         := PF.Espaco(dm.SacadoresNOME.AsString, 'E', 45);
            dm.mdArquivoDNUM_DOC_SACADOR.AsString      := GR.Zeros(dm.SacadoresDOCUMENTO.AsString, 'D', 14);
            dm.mdArquivoDEND_SACADOR.AsString          := PF.Espaco(dm.SacadoresENDERECO.AsString, 'E', 45);
            dm.mdArquivoDCEP_SACADOR.AsString          := GR.Zeros(dm.SacadoresCEP.AsString, 'D', 8);
            dm.mdArquivoDCID_SACADOR.AsString          := PF.Espaco(dm.SacadoresMUNICIPIO.AsString, 'E', 20);
            dm.mdArquivoDUF_SACADOR.AsString           := PF.Espaco(dm.SacadoresUF.AsString, 'E', 2);
            //Sacador - Fim

            dm.mdArquivoDNOSSO_NUMERO.AsString         := GR.Zeros(cdsTitulosNOSSO_NUMERO.AsString, 'D', 15);

            //Especie - Inicio
            dm.Tipos.Close;
            dm.Tipos.Open;

            if dm.Tipos.Locate('CODIGO', cdsTitulosTIPO_TITULO.AsInteger, []) then
              dm.mdArquivoDESPECIE.AsString            := PF.Espaco(dm.TiposSIGLA.AsString, 'E', 3)
            else
              dm.mdArquivoDESPECIE.AsString            := PF.Espaco('', 'E', 3);

            if (Trim(dm.TiposSIGLA.AsString) = 'DMI') or
              (Trim(dm.TiposSIGLA.AsString) = 'DRI') or
              (Trim(dm.TiposSIGLA.AsString) = 'CBI') then
              Inc(iTotCmp12)
            else
              Inc(iTotCmp12O);
            //Especie - Fim

            dm.mdArquivoDNUM_TITULO.AsString           := PF.Espaco(cdsTitulosNUMERO_TITULO.AsString, 'E', 11);
            dm.mdArquivoDDATA_EMISSAO.AsString         := GR.Zeros(GR.PegarNumeroTexto(cdsTitulosDT_TITULO.AsString), 'D', 8);

            if Trim(cdsTitulosAVISTA.AsString) = 'S' then
            begin
              case cdsTitulosDIAS_AVISTA.AsInteger of
                0, 99: dm.mdArquivoDDATA_VENCIMENTO.AsString := '99999999';
                1: dm.mdArquivoDDATA_VENCIMENTO.AsString := '99990001';
                30: dm.mdArquivoDDATA_VENCIMENTO.AsString := '99990030';
              end;
            end
            else
              dm.mdArquivoDDATA_VENCIMENTO.AsString    := GR.Zeros(GR.PegarNumeroTexto(cdsTitulosDT_VENCIMENTO.AsString), 'D', 8);

            dm.mdArquivoDTIPO_MOEDA.AsString           := '001';  //Real

            sValor := FloatToStrF(GR.NoRound(cdsTitulosVALOR_TITULO.AsFloat, 2), ffFixed, 12, 2);
            dm.mdArquivoDVALOR_TITULO.AsString         := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

            sValor := FloatToStrF(GR.NoRound(cdsTitulosSALDO_TITULO.AsFloat, 2), ffFixed, 12, 2);
            dm.mdArquivoDSALDO_TITULO.AsString         := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

            dm.mdArquivoDPRACA_PROTESTO.AsString       := PF.Espaco(cdsTitulosPRACA_PROTESTO.AsString, 'E', 20);

            if Trim(cdsTitulosTIPO_ENDOSSO.AsString) = 'S' then
              dm.mdArquivoDTIPO_ENDOSSO.AsString       := ' '
            else
              dm.mdArquivoDTIPO_ENDOSSO.AsString       := PF.Espaco(cdsTitulosTIPO_ENDOSSO.AsString, 'E', 1);

            dm.mdArquivoDINFO_ACEITE.AsString          := PF.Espaco(cdsTitulosACEITE.AsString, 'E', 1);

            //Devedor - Inicio
{            dm.Devedores.Close;
            dm.Devedores.Params.ParamByName('ID_ATO').AsInteger := cdsTitulosID_ATO.AsInteger;
            dm.Devedores.Open;  }

            dm.mdArquivoDNUM_CONTROLE_DEV.AsString     := GR.Zeros(cdsTitulosORDEM.AsString, 'D', 1);
            dm.mdArquivoDNOME_DEVEDOR.AsString         := PF.Espaco(cdsTitulosNOME.AsString, 'E', 45);

            case AnsiIndexStr(Trim(cdsTitulosTIPO.AsString), ['J', 'F']) of
              0:
              begin
                dm.mdArquivoDTIPO_ID_DEVEDOR.AsString  := '001';
                dm.mdArquivoDNUM_ID_DEVEDOR.AsString   := GR.Zeros(cdsTitulosDOCUMENTO.AsString, 'D', 14);
                dm.mdArquivoDNUM_DOC_DEVEDOR.AsString  := '00000000000';
              end;
              1:
              begin
                dm.mdArquivoDTIPO_ID_DEVEDOR.AsString  := '002';
                dm.mdArquivoDNUM_ID_DEVEDOR.AsString   := GR.Zeros(cdsTitulosDOCUMENTO.AsString, 'D', 14);
                dm.mdArquivoDNUM_DOC_DEVEDOR.AsString  := '00000000000';
              end
              else
              begin
                dm.mdArquivoDTIPO_ID_DEVEDOR.AsString  := '000';
                dm.mdArquivoDNUM_ID_DEVEDOR.AsString   := '00000000000000';
                dm.mdArquivoDNUM_DOC_DEVEDOR.AsString  := GR.Zeros(cdsTitulosDOCUMENTO.AsString, 'D', 11);
              end;
            end;

            dm.mdArquivoDEND_DEVEDOR.AsString          := PF.Espaco(cdsTitulosENDERECO.AsString, 'E', 45);
            dm.mdArquivoDBAIRRO_DEVEDOR.AsString       := PF.Espaco(cdsTitulosBAIRRO.AsString, 'E', 20);
            dm.mdArquivoDCEP_DEVEDOR.AsString          := GR.Zeros(cdsTitulosCEP.AsString, 'D', 8);
            dm.mdArquivoDCID_DEVEDOR.AsString          := PF.Espaco(cdsTitulosMUNICIPIO.AsString, 'E', 20);
            dm.mdArquivoDUF_DEVEDOR.AsString           := PF.Espaco(cdsTitulosUF.AsString, 'E', 2);

            if Trim(cdsTitulosORDEM.AsString) = '1' then
              Inc(iTotDev);
            //Devedor - Fim

            dm.mdArquivoDNUM_PROTOCOLO_DIST.AsString   := GR.Zeros(cdsTitulosPROTOCOLO.AsString, 'D', 10);
            dm.mdArquivoDTIPO_OCORRENCIA.AsString      := GR.Espaco(' ', 'E', 1);
            dm.mdArquivoDDATA_PROTOCOLO.AsString       := GR.Zeros('0', 'D', 8);
            dm.mdArquivoDVALOR_CUSTAS_CART.AsString    := GR.Zeros('0', 'D', 10);
            dm.mdArquivoDDECLARACAO_PORT.AsString      := PF.Espaco(cdsTitulosLETRA.AsString, 'E', 1);
            dm.mdArquivoDDATA_OCORRENCIA.AsString      := GR.Zeros('0', 'D', 8);
            dm.mdArquivoDCOD_IRREGULARIDADE.AsString   := PF.Espaco(' ', 'E', 2);
            dm.mdArquivoDVALOR_CUSTAS_DISTRIB.AsString := GR.Zeros('0', 'D', 10);
            dm.mdArquivoDREG_DISTRIBUICAO.AsString     := GR.Zeros('0', 'D', 6);
            dm.mdArquivoDVALOR_GRAVACAO.AsString       := GR.Zeros('0', 'D', 10);
            dm.mdArquivoDNUM_OPERACAO.AsString         := GR.Zeros('0', 'D', 5);
            dm.mdArquivoDNUM_CONTRATO.AsString         := GR.Zeros('0', 'D', 15);
            dm.mdArquivoDNUM_PARCELA.AsString          := GR.Zeros('0', 'D', 3);
            dm.mdArquivoDTIPO_LETRA_CAMBIO.AsString    := PF.Espaco(' ', 'E', 1);
            dm.mdArquivoDCOMPL_COD_IRREG.AsString      := PF.Espaco(' ', 'E', 8);
            dm.mdArquivoDPROTESTO_FALENCIA.AsString    := PF.Espaco(IfThen(cdsTitulosFINS_FALIMENTARES.AsString = 'S', 'F', ' '), 'E', 1);
            dm.mdArquivoDPOSTECIPADO.AsString          := PF.Espaco(IfThen(cdsTitulosPOSTECIPADO.AsString = 'P', 'P', ' '), 'E', 1);
            dm.mdArquivoDINSTRUMENTO_PROT.AsString     := PF.Espaco(' ', 'E', 1);
            dm.mdArquivoDVALOR_DDESPESAS.AsString      := GR.Zeros(' ', 'D', 10);
            dm.mdArquivoDCOMPL_REGISTRO.AsString       := PF.Espaco(' ', 'E', 19);

            sValor := FloatToStrF(GR.NoRound(cdsTitulosEMOLUMENTOS.AsFloat, 2), ffFixed, 12, 2);
            dm.mdArquivoDEMOLUMENTOS.AsString        := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

            sValor := FloatToStrF(GR.NoRound(cdsTitulosFETJ.AsFloat, 2), ffFixed, 12, 2);
            dm.mdArquivoDFETJ.AsString               := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

            sValor := FloatToStrF(GR.NoRound(cdsTitulosFUNDPERJ.AsFloat, 2), ffFixed, 12, 2);
            dm.mdArquivoDFUNDPERJ.AsString           := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

            sValor := FloatToStrF(GR.NoRound(cdsTitulosFUNPERJ.AsFloat, 2), ffFixed, 12, 2);
            dm.mdArquivoDFUNPERJ.AsString            := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

            sValor := FloatToStrF(GR.NoRound(cdsTitulosFUNARPEN.AsFloat, 2), ffFixed, 12, 2);
            dm.mdArquivoDFUNARPEN.AsString           := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

            sValor := FloatToStrF(GR.NoRound(cdsTitulosPMCMV.AsFloat, 2), ffFixed, 12, 2);
            dm.mdArquivoDPMCMV.AsString              := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

            sValor := FloatToStrF(GR.NoRound(cdsTitulosISS.AsFloat, 2), ffFixed, 12, 2);
            dm.mdArquivoDISS.AsString                := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

            sValor := FloatToStrF(GR.NoRound(cdsTitulosMUTUA.AsFloat, 2), ffFixed, 12, 2);
            dm.mdArquivoDMUTUA.AsString              := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

            sValor := FloatToStrF(GR.NoRound(cdsTitulosACOTERJ.AsFloat, 2), ffFixed, 12, 2);
            dm.mdArquivoDACOTERJ.AsString            := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

            sValor := FloatToStrF(GR.NoRound(cdsTitulosDISTRIBUICAO.AsFloat, 2), ffFixed, 12, 2);
            dm.mdArquivoDDISTRIBUICAO.AsString       := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

            sValor := FloatToStrF(GR.NoRound(cdsTitulosTOTAL.AsFloat, 2), ffFixed, 12, 2);
            dm.mdArquivoDTOTAL_CUSTAS.AsString       := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

            dm.mdArquivoDSEQUENCIAL_REG.AsInteger      := iSeqReg;
            dm.mdArquivoDID_ATO.AsInteger              := cdsTitulosID_ATO.AsInteger;
            dm.mdArquivoDCOD_CARTORIO.AsString         := cdsTitulosCARTORIO.AsString;
            dm.mdArquivoD.Post;
          end;

          cdsTitulos.Next;
        end;

        cdsTitulos.EnableControls;

        if iTotGeral > 0 then
        begin
          { TRAILLER }
          Inc(iSeqReg);

          dm.mdLinhaArqD.Append;
          dm.mdLinhaArqDID_REGISTRO.AsInteger         := 9;

          if Trim(dm.PortadorAuxCODIGO.AsString) = '' then
            dm.mdLinhaArqDNUM_COD_PORTADOR.AsString   := GR.Zeros(' ', 'D', 3)
          else
            dm.mdLinhaArqDNUM_COD_PORTADOR.AsString   := GR.Zeros(Trim(dm.PortadorAuxCODIGO.AsString), 'D', 3);

          if cdsTitulosCODIGO_APRESENTANTE.AsString<>'TAB' then
            dm.mdLinhaArqDNOME_PORTADOR.AsString       := PF.Espaco(dm.PortadorAuxNOME.AsString, 'E', 40)
          else
            dm.mdLinhaArqDNOME_PORTADOR.AsString       := PF.Espaco(cdsTitulosCEDENTE.AsString, 'E', 40);

          //dm.mdLinhaArqDNOME_PORTADOR.AsString       := PF.Espaco(dm.PortadorAuxNOME.AsStrinG, 'E', 40);
          dm.mdLinhaArqDDATA_MOVIMENTO.AsString      := GR.Zeros(GR.PegarNumeroTexto(DateToStr(Date)), 'D', 8);
          dm.mdLinhaArqDCOMPL_REGISTRO.AsString      := PF.Espaco(' ', 'E', 521);
          dm.mdLinhaArqDSOMAT_QTD_REMESSA.AsString   := GR.Zeros(IntToStr(iTotGeral +
                                                                          iTotDev +
                                                                          iTotCmp12 +
                                                                          iTotCmp12O), 'D', 5);

          sValor := FloatToStrF(GR.NoRound(cSomatVlrSeg, 2), ffFixed, 16, 2);
          dm.mdLinhaArqDSOMAT_VAL_REMESSA.AsString   := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 18);

          dm.mdLinhaArqDNUM_SEQ_REGISTRO.AsString    := PF.Espaco(IntToStr(iSeqReg), 'E', 4);
          dm.mdLinhaArqDSERVENTIA_AGREGADA.AsInteger := dm.ServentiaAgregadaCODIGO_DISTRIBUICAO.AsInteger;
          dm.mdLinhaArqD.Post;

          { HEADER }
          dm.mdLinhaArqD.Append;
          dm.mdLinhaArqDID_REGISTRO.AsInteger        := 0;

          if Trim(dm.PortadorAuxCODIGO.AsString) = '' then
            dm.mdLinhaArqDNUM_COD_PORTADOR.AsString  := GR.Zeros('', 'D', 3)
          else
            dm.mdLinhaArqDNUM_COD_PORTADOR.AsString  := GR.Zeros(dm.PortadorAuxCODIGO.AsString, 'D', 3);

          if cdsTitulosCODIGO_APRESENTANTE.AsString<>'TAB' then
            dm.mdLinhaArqDNOME_PORTADOR.AsString       := PF.Espaco(dm.PortadorAuxNOME.AsString, 'E', 40)
          else
            dm.mdLinhaArqDNOME_PORTADOR.AsString       := PF.Espaco(cdsTitulosCEDENTE.AsString, 'E', 40);

          if Trim(dm.PortadorAuxAGENCIA.AsString) = '' then
            dm.mdLinhaArqDID_AGENCIA.AsString        := GR.Zeros('0', 'D', 6)
          else
            dm.mdLinhaArqDID_AGENCIA.AsString        := GR.Zeros(dm.PortadorAuxAGENCIA.AsString, 'D', 6);

          if Trim(dm.PortadorAuxPRACA.AsString) = '' then
            dm.mdLinhaArqDCOD_MUNIC_PRACA.AsString   := GR.Zeros('0', 'D', 7)
          else
            dm.mdLinhaArqDCOD_MUNIC_PRACA.AsString   := GR.Zeros(dm.PortadorAuxPRACA.AsString, 'D', 7);

          dm.mdLinhaArqDDATA_MOVIMENTO.AsString      := GR.Zeros(GR.PegarNumeroTexto(DateToStr(Date)), 'D', 8);
          dm.mdLinhaArqDID_TRANS_REMET.AsString      := 'BFO';
          dm.mdLinhaArqDID_TRANS_DESTIN.AsString     := 'SDT';
          dm.mdLinhaArqDID_TRANS_TIPO.AsString       := 'TPR';
          dm.mdLinhaArqDNUM_SEQ_REMESSA.AsString     := '0';  //Esse valor sera atualizado na geracao do arquivo D
          dm.mdLinhaArqDQTD_REG_REMESSA.AsString     := IntToStr(iTotGeral);
          dm.mdLinhaArqDQTD_TIT_REMESSA.AsString     := IntToStr(iTotDev);
          dm.mdLinhaArqDQTD_IND_REMESSA.AsString     := IntToStr(iTotCmp12);
          dm.mdLinhaArqDQTD_ORIG_REMESSA.AsString    := IntToStr(iTotCmp12O);
          dm.mdLinhaArqDVERSAO_LAYOUT.AsString       := '043';
          dm.mdLinhaArqDCOMPL_REGISTRO.AsString      := PF.Espaco(' ', 'E', 497);
          dm.mdLinhaArqDNUM_SEQ_REGISTRO.AsString    := PF.Espaco(IntToStr(iSeqReg - iTotGeral - 1), 'E', 4);
          dm.mdLinhaArqDSERVENTIA_AGREGADA.AsInteger := dm.ServentiaAgregadaCODIGO_DISTRIBUICAO.AsInteger;
          dm.mdLinhaArqD.Post;

          Inc(iSeqReg);
        end;
      end;

      dm.PortadorAux.Next;
    end;

    //Atualiza Serventia
    dm.ServentiaAgregada.Edit;

    if iSeqReg > 1 then
      dm.ServentiaAgregadaQTD_MAX_PREV.AsInteger := (iSeqReg - 1)
    else
      dm.ServentiaAgregadaQTD_MAX_PREV.AsInteger := 0;

    dm.ServentiaAgregada.Post;

    dm.ServentiaAgregada.Next;
  end;

  PF.Aguarde(False);

  if iSel = 0 then
  begin
    Application.MessageBox('N�o h� T�tulos selecionados para serem exportados.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
  end
  else
  begin
    { GERANDO OS ARQUIVOS DE REMESSA D }
    lErro := False;

    dm.ExportarCartorios(sMsgRet, lErro, False, LstArqB);

    if lErro then
    begin
      Application.MessageBox(PChar(sMsgRet), 'Erro', MB_OK + MB_ICONERROR);
      btnExportar.Enabled := False;
    end
    else
    begin
      //dm.EnviarRemessasCartorios;
      Application.MessageBox(PChar(sMsgRet), 'Sucesso!', MB_OK);

      PF.Aguarde(True);

      cdsTitulos.Close;
      qryTitulos.SQL.Text := 'SELECT T.*, ' +
                             '       D.* ' +
                             '  FROM TITULOS T ' +
                             ' INNER JOIN DEVEDORES D ' +
                             '    ON T.ID_ATO = D.ID_ATO ' +
                             ' WHERE T.ID_ATO IN (' + LstTitulos + ') '+
                             'ORDER BY T.CARTORIO, T.APRESENTANTE, T.CODIGO_APRESENTANTE, T.PROTOCOLO, D.ORDEM';
      cdsTitulos.Open;

      PF.Aguarde(False);

      btnExportar.Enabled := False;
    end;
  end;
end;

procedure TFExportarTxtRemBalcaoServ.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FreeAndNil(LstArqD);
  FreeAndNil(LstArqB);
end;

procedure TFExportarTxtRemBalcaoServ.FormCreate(Sender: TObject);
var
  Dia, Mes, Ano: Word;
begin
  Dia := 0;
  Mes := 0;
  Ano := 0;

  DecodeDate(Date, Ano, Mes, Dia);

  dm.Pracas.Close;
  dm.Pracas.Open;

  LstArqD := TListBox.Create(Self);
  LstArqD.Parent  := Self;
  LstArqD.Visible := False;

  LstArqB := TListBox.Create(Self);
  LstArqB.Parent  := Self;
  LstArqB.Visible := False;

  NomeArqVazio := 'D000' +
                  GR.Zeros(IntToStr(Dia), 'E', 2) +
                  GR.Zeros(IntToStr(Mes), 'E', 2) +
                  '.' +
                  Copy(IntToStr(Ano), 3, 2) +
                  '0';
end;

procedure TFExportarTxtRemBalcaoServ.dteDataEntKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    AbrirListaTitulos;
end;

procedure TFExportarTxtRemBalcaoServ.VerificaMarcados(Check: Boolean;
  Num_Titulo, Nosso_Numero, ArquivoB, ArquivoD,
  TpApresentacao: String);
var
  P: TBookmark;
  j, iVazio: Integer;
  NomeArquivo: String;
begin
//  if Screen.Cursor = crHandPoint then
//  begin
    if cdsTitulos.IsEmpty then
      Exit;

    P := cdsTitulos.GetBookmark;

    NomeArquivo := '';

    j      := 0;
    iVazio := 0;

    cdsTitulos.DisableControls;
    cdsTitulos.First;

    while not cdsTitulos.Eof do
    begin
      NomeArquivo := IfThen(Trim(cdsTitulosARQUIVO.AsString) = '', NomeArqVazio, cdsTitulosARQUIVO.AsString);

      if (TpApresentacao = 'I') and
        (ArquivoD = '') then
      begin
        if cdsTitulosARQUIVO.AsString = ArquivoB then
        begin
          cdsTitulos.Edit;
          cdsTitulosSELECIONADO.AsBoolean := Check;

          if cdsTitulosSELECIONADO.AsBoolean = False then
          begin
            if LstArqD.Items.IndexOf(NomeArquivo) > -1 then
              LstArqD.Items.Delete(LstArqD.Items.IndexOf(NomeArquivo));

            if LstArqB.Items.IndexOf(cdsTitulosARQUIVO.AsString) > -1 then
              LstArqB.Items.Delete(LstArqB.Items.IndexOf(cdsTitulosARQUIVO.AsString));
          end
          else
          begin
            cdsTitulosIRREGULARIDADE.Clear;

            if LstArqD.Items.IndexOf(NomeArquivo) < 0 then
              LstArqD.Items.Add(NomeArquivo);

            if LstArqB.Items.IndexOf(cdsTitulosARQUIVO.AsString) < 0 then
              LstArqB.Items.Add(cdsTitulosARQUIVO.AsString);
          end;

          cdsTitulos.Post;
        end;
      end
      else
      begin
        if (cdsTitulosNUMERO_TITULO.AsString = Num_Titulo) and
          (cdsTitulosNOSSO_NUMERO.AsString = Nosso_Numero) then
        begin
          cdsTitulos.Edit;
          cdsTitulosSELECIONADO.AsBoolean := Check;

          if cdsTitulosSELECIONADO.AsBoolean = True then
          begin
            cdsTitulosIRREGULARIDADE.Clear;

            if TpApresentacao = 'B' then
              Inc(iVazio);
          end;

          cdsTitulos.Post;
        end;
      end;

      if Trim(ArquivoB) = '' then
      begin
        if iVazio = 0 then
        begin
          if LstArqD.Items.IndexOf(NomeArqVazio) > -1 then
            LstArqD.Items.Delete(LstArqD.Items.IndexOf(NomeArqVazio));
        end
        else
        begin
          if LstArqD.Items.IndexOf(NomeArqVazio) < 0 then
            LstArqD.Items.Add(NomeArqVazio);
        end;
      end;

      if cdsTitulosSELECIONADO.AsBoolean = True then
      begin
        if not dm.PortadorAux.Locate('SERVENTIA_AGREGADA;NOME;CODIGO', VarArrayOf([cdsTitulosCARTORIO.AsInteger,
                                                                                   Trim(cdsTitulosAPRESENTANTE.AsString),
                                                                                   cdsTitulosCODIGO_APRESENTANTE.AsString]), [loCaseInsensitive]) then
        begin
          dm.Portadores.Close;
          dm.Portadores.Open;

          if dm.Portadores.Locate('NOME;', VarArrayOf([Trim(cdsTitulosAPRESENTANTE.AsString)]), []) then
          begin
            if dm.Portadores.Locate('CODIGO;', VarArrayOf([Trim(cdsTitulosCODIGO_APRESENTANTE.AsString)]), []) then
            begin
              dm.PortadorAux.Append;
              dm.PortadorAuxCODIGO.AsString              := dm.PortadoresCODIGO.AsString;
              dm.PortadorAuxNOME.AsString                := Trim(dm.PortadoresNOME.AsString);
              dm.PortadorAuxAGENCIA.AsString             := dm.PortadoresAGENCIA.AsString;
              dm.PortadorAuxPRACA.AsString               := dm.PortadoresPRACA.AsString;
              dm.PortadorAuxSERVENTIA_AGREGADA.AsInteger := cdsTitulosCARTORIO.AsInteger;
              dm.PortadorAux.Post;
            end
            else
            begin
              dm.PortadorAux.Append;
              dm.PortadorAuxCODIGO.AsString              := cdsTitulosCODIGO_APRESENTANTE.AsString;
              dm.PortadorAuxNOME.AsString                := Trim(cdsTitulosAPRESENTANTE.AsString);
              dm.PortadorAuxSERVENTIA_AGREGADA.AsInteger := cdsTitulosCARTORIO.AsInteger;
              dm.PortadorAux.Post;
            end;
          end
          else
          begin
            dm.PortadorAux.Append;
            dm.PortadorAuxCODIGO.AsString              := cdsTitulosCODIGO_APRESENTANTE.AsString;
            dm.PortadorAuxNOME.AsString                := cdsTitulosAPRESENTANTE.AsString;
            dm.PortadorAuxSERVENTIA_AGREGADA.AsInteger := cdsTitulosCARTORIO.AsInteger;
            dm.PortadorAux.Post;
          end;
        end;
      end;

      cdsTitulos.Next;
    end;

    cdsTitulos.EnableControls;
    cdsTitulos.GotoBookmark(P);
//  end;
end;

procedure TFExportarTxtRemBalcaoServ.DesmarcarTodosClick(Sender: TObject);
begin
  MarcarDesmarcarTitulos(False);
end;

procedure TFExportarTxtRemBalcaoServ.cdsTitulosENVIADO_APONTAMENTOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString = '' then
    Text := 'N';
end;

procedure TFExportarTxtRemBalcaoServ.cdsTitulosENVIADO_APONTAMENTOSetText(
  Sender: TField; const Text: string);
begin
  if Text = 'N' then
    Sender.AsString := 'N';
end;

procedure TFExportarTxtRemBalcaoServ.cdsTitulosENVIADO_DEVOLVIDOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString = '' then
    Text := 'N';
end;

procedure TFExportarTxtRemBalcaoServ.cdsTitulosENVIADO_DEVOLVIDOSetText(
  Sender: TField; const Text: string);
begin
  if Text = 'N' then
    Sender.AsString := 'N';
end;

procedure TFExportarTxtRemBalcaoServ.cdsTitulosENVIADO_PAGAMENTOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString = '' then
    Text := 'N';
end;

procedure TFExportarTxtRemBalcaoServ.cdsTitulosENVIADO_PAGAMENTOSetText(
  Sender: TField; const Text: string);
begin
  if Text = 'N' then
    Sender.AsString := 'N';
end;

procedure TFExportarTxtRemBalcaoServ.cdsTitulosENVIADO_PROTESTOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString = '' then
    Text := 'N';
end;

procedure TFExportarTxtRemBalcaoServ.cdsTitulosENVIADO_PROTESTOSetText(
  Sender: TField; const Text: string);
begin
  if Text = 'N' then
    Sender.AsString := 'N';
end;

procedure TFExportarTxtRemBalcaoServ.cdsTitulosENVIADO_RETIRADOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString = '' then
    Text := 'N';
end;

procedure TFExportarTxtRemBalcaoServ.cdsTitulosENVIADO_RETIRADOSetText(
  Sender: TField; const Text: string);
begin
  if Text = 'N' then
    Sender.AsString := 'N';
end;

procedure TFExportarTxtRemBalcaoServ.cdsTitulosENVIADO_SUSTADOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString = '' then
    Text := 'N';
end;

procedure TFExportarTxtRemBalcaoServ.cdsTitulosENVIADO_SUSTADOSetText(
  Sender: TField; const Text: string);
begin
  if Text = 'N' then
    Sender.AsString := 'N';
end;

end.
