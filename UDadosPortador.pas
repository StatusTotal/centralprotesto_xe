unit UDadosPortador;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Buttons, sBitBtn, DBCtrls, sDBMemo, sDBComboBox,
  Mask, sDBEdit, sCheckBox, sDBCheckBox, ExtCtrls, sPanel, acPNG;

type
  TFDadosPortador = class(TForm)
    P2: TsPanel;
    chbConvenio: TsDBCheckBox;
    edCodigo: TsDBEdit;
    edNome: TsDBEdit;
    cbTipo: TsDBComboBox;
    edDocumento: TsDBEdit;
    edEndereco: TsDBEdit;
    ckBanco: TsDBCheckBox;
    edConta: TsDBEdit;
    MObs: TsDBMemo;
    btOk: TsBitBtn;
    dsRxPortador: TDataSource;
    ImAviso1: TImage;
    ImOk1: TImage;
    chbPagaAntecipado: TsDBCheckBox;
    procedure cbTipoChange(Sender: TObject);
    procedure edDocumentoChange(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbTipoClick(Sender: TObject);
    procedure ckBancoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chbConvenioClick(Sender: TObject);
    procedure chbConvenioExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDadosPortador: TFDadosPortador;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFDadosPortador.cbTipoChange(Sender: TObject);
begin
  if cbTipo.ItemIndex=0 then
  begin
      edDocumento.BoundLabel.Caption   :='CPF';
      dm.RxPortadorDOCUMENTO.EditMask  :='999.999.999-99;0;_';
      dm.RxPortadorTIPO.AsString       :='F';
  end
  else
  begin
      edDocumento.BoundLabel.Caption   :='CNPJ';
      dm.RxPortadorDOCUMENTO.EditMask  :='99.999.999/9999-99;0;_';
      dm.RxPortadorTIPO.AsString       :='J';
  end;
end;

procedure TFDadosPortador.edDocumentoChange(Sender: TObject);
begin
  if GR.PegarNumeroTexto(edDocumento.Text)='' then
  begin
      ImAviso1.Visible:=True;
      ImOk1.Visible   :=False;
      Exit;
  end;

  ImAviso1.Visible:= not PF.VerificarCPF(GR.PegarNumeroTexto(edDocumento.Text));

  ImOk1.Visible:=not ImAviso1.Visible;

  if ActiveControl<>edDocumento then
    if dm.RxPortadorTIPO.AsString='F' then
    begin
        edDocumento.BoundLabel.Caption   :='CPF';
        dm.RxPortadorDOCUMENTO.EditMask  :='999.999.999-99;0;_';
    end
    else
    begin
        edDocumento.BoundLabel.Caption   :='CNPJ';
        dm.RxPortadorDOCUMENTO.EditMask  :='99.999.999/9999-99;0;_';
    end;
end;

procedure TFDadosPortador.btOkClick(Sender: TObject);
begin
  Close;
end;

procedure TFDadosPortador.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFDadosPortador.FormShow(Sender: TObject);
begin
  chbPagaAntecipado.Enabled := chbConvenio.Checked;
end;

procedure TFDadosPortador.cbTipoClick(Sender: TObject);
begin
  if cbTipo.ItemIndex=0 then
  begin
      edDocumento.BoundLabel.Caption   :='CPF';
      dm.RxPortadorDOCUMENTO.EditMask  :='999.999.999-99;0;_';
      dm.RxPortadorTIPO.AsString       :='F';
  end
  else
  begin
      edDocumento.BoundLabel.Caption   :='CNPJ';
      dm.RxPortadorDOCUMENTO.EditMask  :='99.999.999/9999-99;0;_';
      dm.RxPortadorTIPO.AsString       :='J';
  end;
end;

procedure TFDadosPortador.chbConvenioClick(Sender: TObject);
begin
  chbPagaAntecipado.Enabled := chbConvenio.Checked;

  if not chbConvenio.Checked then
    chbPagaAntecipado.Checked := False;
end;

procedure TFDadosPortador.chbConvenioExit(Sender: TObject);
begin
  chbPagaAntecipado.Enabled := chbConvenio.Checked;

  if not chbConvenio.Checked then
    chbPagaAntecipado.Checked := False;
end;

procedure TFDadosPortador.ckBancoClick(Sender: TObject);
begin
  if ckBanco.Checked then
    edCodigo.Enabled:=True
  else
  begin
      edCodigo.Enabled:=False;
      dm.RxPortadorCODIGO.Clear;
  end;
end;

procedure TFDadosPortador.FormCreate(Sender: TObject);
begin
  ckBancoClick(Sender);
end;

end.
