object FConsultaCertidao: TFConsultaCertidao
  Left = 287
  Top = 177
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'CONSULTA DE CERTID'#195'O'
  ClientHeight = 532
  ClientWidth = 867
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 15
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 867
    Height = 53
    Align = alTop
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object cbBuscar: TsComboBox
      Left = 11
      Top = 22
      Width = 177
      Height = 23
      Alignment = taLeftJustify
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Tipo de Busca'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ItemIndex = -1
      ParentFont = False
      TabOrder = 0
      Items.Strings = (
        'REQUERENTE'
        'REQUERIDO'
        'DOCUMENTO'
        'RECIBO'
        'SELO')
    end
    object edPesquisar: TsEdit
      Left = 201
      Top = 22
      Width = 330
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnKeyPress = edPesquisarKeyPress
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Localizar'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edData: TsDateEdit
      Left = 648
      Top = 22
      Width = 96
      Height = 23
      Hint = 'DATA QUE A CERTID'#195'O FOI LAVRADA'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = '  /  /    '
      OnKeyPress = edDataKeyPress
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data da Certid'#227'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA QUE A CERTID'#195'O FOI LAVRADA'
    end
    object edPedido: TsDateEdit
      Left = 544
      Top = 22
      Width = 96
      Height = 23
      Hint = 'DATA QUE FOI FEITO O PEDIDO DA CERTID'#195'O'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Text = '  /  /    '
      OnKeyPress = edPedidoKeyPress
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data do Pedido'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA QUE FOI FEITO O PEDIDO DA CERTID'#195'O'
    end
    object edEntrega: TsDateEdit
      Left = 752
      Top = 22
      Width = 96
      Height = 23
      Hint = 'DATA QUE A CERTID'#195'O FOI ENTREGUE'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      Text = '  /  /    '
      OnKeyPress = edEntregaKeyPress
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data de Entrega'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA QUE A CERTID'#195'O FOI ENTREGUE'
    end
  end
  object P2: TsPanel
    Left = 0
    Top = 491
    Width = 867
    Height = 41
    Align = alBottom
    TabOrder = 1
    SkinData.SkinSection = 'SELECTION'
    object sLabel1: TsLabel
      Left = 4
      Top = 13
      Width = 559
      Height = 15
      Caption = 
        '* Pressione [ENTER] no campo "Localizar" ou nos campos "Data" pa' +
        'ra efetuar a busca das Certid'#245'es.'
    end
    object btImprimir: TsBitBtn
      Left = 774
      Top = 6
      Width = 87
      Height = 31
      Cursor = crHandPoint
      Caption = 'Imprimir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Visible = False
      OnClick = btImprimirClick
      ImageIndex = 19
      Images = Gdm.Im16
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object GridAtos: TwwDBGrid
    Left = 0
    Top = 53
    Width = 867
    Height = 438
    Cursor = crHandPoint
    Selected.Strings = (
      'NomeCertidao'#9'15'#9'Tipo'#9'F'
      'DT_PEDIDO'#9'13'#9'Data do Pedido'#9'F'
      'DT_CERTIDAO'#9'14'#9'Data da Certid'#227'o'#9'F'
      'RECIBO'#9'9'#9'Recibo'#9'F'
      'SELO'#9'11'#9'Selo'#9'F'
      'ALEATORIO'#9'8'#9'Aleat'#243'rio'#9'F'
      'REQUERIDO'#9'27'#9'Requerido'#9'F'
      'FOLHAS'#9'4'#9'Fls.'#9'F'
      'Excedentes'#9'4'#9'Exc.'#9'F'
      'EXTOTAL'#9'7'#9'Total Exc.'#9'F')
    IniAttributes.Delimiter = ';;'
    IniAttributes.UnicodeIniFile = False
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    BorderStyle = bsNone
    DataSource = dsConsulta
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
    ParentFont = False
    PopupMenu = PM
    ReadOnly = True
    TabOrder = 2
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Arial'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
  end
  object PM: TPopupMenu
    Images = Gdm.Im16
    Left = 272
    Top = 241
    object Excluir: TMenuItem
      Caption = 'Excluir'
      ImageIndex = 14
      OnClick = ExcluirClick
    end
  end
  object dsConsulta: TDataSource
    DataSet = qryConsulta
    Left = 216
    Top = 241
  end
  object qryConsulta: TFDQuery
    AfterPost = qryConsultaAfterPost
    OnCalcFields = qryConsultaCalcFields
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      ''
      '*'
      ''
      'from CERTIDOES'
      ''
      'order by DT_CERTIDAO')
    Left = 216
    Top = 192
    object qryConsultaID_CERTIDAO: TIntegerField
      FieldName = 'ID_CERTIDAO'
      Origin = 'ID_CERTIDAO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryConsultaID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
    end
    object qryConsultaORDEM: TIntegerField
      FieldName = 'ORDEM'
      Origin = 'ORDEM'
    end
    object qryConsultaDT_PEDIDO: TDateField
      FieldName = 'DT_PEDIDO'
      Origin = 'DT_PEDIDO'
    end
    object qryConsultaDT_ENTREGA: TDateField
      FieldName = 'DT_ENTREGA'
      Origin = 'DT_ENTREGA'
    end
    object qryConsultaDT_CERTIDAO: TDateField
      FieldName = 'DT_CERTIDAO'
      Origin = 'DT_CERTIDAO'
    end
    object qryConsultaTIPO_CERTIDAO: TStringField
      FieldName = 'TIPO_CERTIDAO'
      Origin = 'TIPO_CERTIDAO'
      FixedChar = True
      Size = 1
    end
    object qryConsultaNUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
      Origin = 'NUMERO_TITULO'
    end
    object qryConsultaANOS: TIntegerField
      FieldName = 'ANOS'
      Origin = 'ANOS'
    end
    object qryConsultaCONVENIO: TStringField
      FieldName = 'CONVENIO'
      Origin = 'CONVENIO'
      FixedChar = True
      Size = 1
    end
    object qryConsultaCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
    end
    object qryConsultaSELO: TStringField
      FieldName = 'SELO'
      Origin = 'SELO'
      Size = 9
    end
    object qryConsultaALEATORIO: TStringField
      FieldName = 'ALEATORIO'
      Origin = 'ALEATORIO'
      Size = 3
    end
    object qryConsultaRECIBO: TIntegerField
      FieldName = 'RECIBO'
      Origin = 'RECIBO'
    end
    object qryConsultaREQUERIDO: TStringField
      FieldName = 'REQUERIDO'
      Origin = 'REQUERIDO'
      Size = 100
    end
    object qryConsultaTIPO_REQUERIDO: TStringField
      FieldName = 'TIPO_REQUERIDO'
      Origin = 'TIPO_REQUERIDO'
      FixedChar = True
      Size = 1
    end
    object qryConsultaCPF_CNPJ_REQUERIDO: TStringField
      FieldName = 'CPF_CNPJ_REQUERIDO'
      Origin = 'CPF_CNPJ_REQUERIDO'
      Size = 15
    end
    object qryConsultaREQUERENTE: TStringField
      FieldName = 'REQUERENTE'
      Origin = 'REQUERENTE'
      Size = 100
    end
    object qryConsultaCPF_CNPJ_REQUERENTE: TStringField
      FieldName = 'CPF_CNPJ_REQUERENTE'
      Origin = 'CPF_CNPJ_REQUERENTE'
      Size = 14
    end
    object qryConsultaTELEFONE_REQUERENTE: TStringField
      FieldName = 'TELEFONE_REQUERENTE'
      Origin = 'TELEFONE_REQUERENTE'
      Size = 10
    end
    object qryConsultaRESULTADO: TStringField
      FieldName = 'RESULTADO'
      Origin = 'RESULTADO'
      FixedChar = True
      Size = 1
    end
    object qryConsultaFOLHAS: TIntegerField
      FieldName = 'FOLHAS'
      Origin = 'FOLHAS'
    end
    object qryConsultaREGISTROS: TIntegerField
      FieldName = 'REGISTROS'
      Origin = 'REGISTROS'
    end
    object qryConsultaCOBRANCA: TStringField
      FieldName = 'COBRANCA'
      Origin = 'COBRANCA'
      FixedChar = True
      Size = 2
    end
    object qryConsultaEMOLUMENTOS: TFloatField
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
    end
    object qryConsultaFETJ: TFloatField
      FieldName = 'FETJ'
      Origin = 'FETJ'
    end
    object qryConsultaFUNDPERJ: TFloatField
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
    end
    object qryConsultaFUNPERJ: TFloatField
      FieldName = 'FUNPERJ'
      Origin = 'FUNPERJ'
    end
    object qryConsultaFUNARPEN: TFloatField
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
    end
    object qryConsultaPMCMV: TFloatField
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
    end
    object qryConsultaISS: TFloatField
      FieldName = 'ISS'
      Origin = 'ISS'
    end
    object qryConsultaMUTUA: TFloatField
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
    end
    object qryConsultaACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
    end
    object qryConsultaDISTRIBUICAO: TFloatField
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
    end
    object qryConsultaAPONTAMENTO: TFloatField
      FieldName = 'APONTAMENTO'
      Origin = 'APONTAMENTO'
    end
    object qryConsultaCAPA: TFloatField
      FieldName = 'CAPA'
      Origin = 'CAPA'
    end
    object qryConsultaTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
    end
    object qryConsultaEXRECIBO: TIntegerField
      FieldName = 'EXRECIBO'
      Origin = 'EXRECIBO'
    end
    object qryConsultaEXCOBRANCA: TStringField
      FieldName = 'EXCOBRANCA'
      Origin = 'EXCOBRANCA'
      FixedChar = True
      Size = 2
    end
    object qryConsultaEXEMOLUMENTOS: TFloatField
      FieldName = 'EXEMOLUMENTOS'
      Origin = 'EXEMOLUMENTOS'
    end
    object qryConsultaEXFETJ: TFloatField
      FieldName = 'EXFETJ'
      Origin = 'EXFETJ'
    end
    object qryConsultaEXFUNDPERJ: TFloatField
      FieldName = 'EXFUNDPERJ'
      Origin = 'EXFUNDPERJ'
    end
    object qryConsultaEXFUNPERJ: TFloatField
      FieldName = 'EXFUNPERJ'
      Origin = 'EXFUNPERJ'
    end
    object qryConsultaEXISS: TFloatField
      FieldName = 'EXISS'
      Origin = 'EXISS'
    end
    object qryConsultaEXFUNARPEN: TFloatField
      FieldName = 'EXFUNARPEN'
      Origin = 'EXFUNARPEN'
    end
    object qryConsultaEXTOTAL: TFloatField
      FieldName = 'EXTOTAL'
      Origin = 'EXTOTAL'
    end
    object qryConsultaCCT: TStringField
      FieldName = 'CCT'
      Origin = 'CCT'
      Size = 9
    end
    object qryConsultaESCREVENTE: TStringField
      FieldName = 'ESCREVENTE'
      Origin = 'ESCREVENTE'
      Size = 11
    end
    object qryConsultaRESPONSAVEL: TStringField
      FieldName = 'RESPONSAVEL'
      Origin = 'RESPONSAVEL'
      Size = 11
    end
    object qryConsultaOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      BlobType = ftMemo
    end
    object qryConsultaENVIADO: TStringField
      FieldName = 'ENVIADO'
      Origin = 'ENVIADO'
      FixedChar = True
      Size = 1
    end
    object qryConsultaENVIADO_FLS: TStringField
      FieldName = 'ENVIADO_FLS'
      Origin = 'ENVIADO_FLS'
      FixedChar = True
      Size = 1
    end
    object qryConsultaQTD: TIntegerField
      FieldName = 'QTD'
      Origin = 'QTD'
    end
    object qryConsultaNomeCertidao: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'NomeCertidao'
      Size = 40
    end
    object qryConsultaExcedentes: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'Excedentes'
    end
  end
end
