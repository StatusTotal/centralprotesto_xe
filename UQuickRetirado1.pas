unit UQuickRetirado1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QuickRpt, QRCtrls, ExtCtrls, StdCtrls, ComCtrls, DB, DBClient,
  SimpleDS, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFQuickRetirado1 = class(TForm)
    Desistencia: TQuickRep;
    QRBand1: TQRBand;
    lbCidade: TQRLabel;
    lbTitular: TQRLabel;
    lbCartorio: TQRLabel;
    QRLabel2: TQRLabel;
    RE: TRichEdit;
    QRRichText1: TQRRichText;
    ChildBand1: TQRChildBand;
    lbData: TQRLabel;
    QRShape2: TQRShape;
    lbAssinatura: TQRLabel;
    lbMatricula: TQRLabel;
    QRLabel3: TQRLabel;
    M: TQRMemo;
    QRLabel1: TQRLabel;
    QRBand2: TQRBand;
    QRLabel4: TQRLabel;
    qrProtocolo: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel33: TQRLabel;
    lbProtocolo: TQRLabel;
    lbDevedor: TQRLabel;
    lbDocumento: TQRLabel;
    lbTitulo: TQRLabel;
    lbSacador: TQRLabel;
    lbCedente: TQRLabel;
    lbPortador: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    lbProtocolo2: TQRLabel;
    QRLabel13: TQRLabel;
    lbDevedor2: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    lbDocumento2: TQRLabel;
    lbTitulo2: TQRLabel;
    lbSacador2: TQRLabel;
    lbCedente2: TQRLabel;
    lbData2: TQRLabel;
    QRShape1: TQRShape;
    QRLabel5: TQRLabel;
    lbValor: TQRLabel;
    QRLabel12: TQRLabel;
    lbVencimento: TQRLabel;
    QRLabel19: TQRLabel;
    lbApresentante: TQRLabel;
    lbRecibo: TQRLabel;
    qrRecibo: TQRLabel;
    lbRecibo2: TQRLabel;
    qrRecibo2: TQRLabel;
    lbCGJ1: TQRLabel;
    lbCGJ2: TQRLabel;
    lbCGJ3: TQRLabel;
    lbSeloEletronico: TQRLabel;
    lbCGJ4: TQRLabel;
    lbCGJ5: TQRLabel;
    QRLabel14: TQRLabel;
    lbMotivo: TQRLabel;
    QRLabel20: TQRLabel;
    lbValor2: TQRLabel;
    QRLabel21: TQRLabel;
    lbSelo: TQRLabel;
    qryProtocolo: TFDQuery;
    qryProtocoloID_ATO: TIntegerField;
    qryProtocoloPROTOCOLO: TIntegerField;
    qryProtocoloTIPO_DEVEDOR: TStringField;
    qryProtocoloDEVEDOR: TStringField;
    qryProtocoloCPF_CNPJ_DEVEDOR: TStringField;
    qryProtocoloNUMERO_TITULO: TStringField;
    qryProtocoloSACADOR: TStringField;
    qryProtocoloCEDENTE: TStringField;
    qryProtocoloVALOR_TITULO: TFloatField;
    qryProtocoloDT_VENCIMENTO: TDateField;
    qryProtocoloAPRESENTANTE: TStringField;
    qryProtocoloRECIBO_PAGAMENTO: TIntegerField;
    qryProtocoloSELO_PAGAMENTO: TStringField;
    qryProtocoloALEATORIO_SOLUCAO: TStringField;
    qryProtocoloEMOLUMENTOS: TFloatField;
    qryProtocoloFETJ: TFloatField;
    qryProtocoloFUNDPERJ: TFloatField;
    qryProtocoloFUNPERJ: TFloatField;
    qryProtocoloFUNARPEN: TFloatField;
    qryProtocoloPMCMV: TFloatField;
    qryProtocoloISS: TFloatField;
    qryProtocoloMUTUA: TFloatField;
    qryProtocoloACOTERJ: TFloatField;
    qryProtocoloTOTAL: TFloatField;
    qryProtocoloIRREGULARIDADE: TIntegerField;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickRetirado1: TFQuickRetirado1;

implementation

uses UDM, UPF, UAssinatura, UGeral;

{$R *.dfm}

procedure TFQuickRetirado1.FormCreate(Sender: TObject);
begin
  lbCartorio.Caption  :=dm.ServentiaDESCRICAO.AsString;
  lbCidade.Caption    :=dm.ServentiaCIDADE.AsString+' - Rio de Janeiro';
  lbTitular.Caption   :=dm.ServentiaTABELIAO.AsString+' - '+dm.ServentiaTITULO.AsString;
  lbData.Caption      :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',Now);

  GR.CriarForm(TFAssinatura,FAssinatura);
  lbAssinatura.Caption:=dm.vAssinatura;
  lbMatricula.Caption :=dm.vMatricula;

  if dm.ServentiaCODIGO.AsInteger=1554 then
  begin
      lbAssinatura.Caption:='';
      lbMatricula.Caption:='';
  end;
end;

end.
