unit UDadosBaixa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, ExtCtrls, sPanel, DBCtrls,
  sDBLookupComboBox, sEdit, Mask, sMaskEdit, sCustomComboEdit, sTooledit,
  DB, sCurrEdit, sCurrencyEdit, sComboBox, sCheckBox;

type
  TFDadosBaixa = class(TForm)
    P1: TsPanel;
    btConfirmar: TsBitBtn;
    btCancelar: TsBitBtn;
    edData: TsDateEdit;
    edSelo: TsEdit;
    edRegistro: TsEdit;
    edLivro: TsEdit;
    edLetra: TsEdit;
    edFolha: TsEdit;
    lkEscreventes: TsDBLookupComboBox;
    dsEscreventes: TDataSource;
    edValor: TsCurrencyEdit;
    cbTipoIntimacao: TsComboBox;
    edDataPublicacao: TsDateEdit;
    dsMotivos: TDataSource;
    lkMotivo: TsDBLookupComboBox;
    ckImprime: TsCheckBox;
    edRecibo: TsEdit;
    procedure btConfirmarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure edSeloExit(Sender: TObject);
    procedure edRegistroKeyPress(Sender: TObject; var Key: Char);
    procedure edLivroKeyPress(Sender: TObject; var Key: Char);
    procedure edFolhaKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure cbTipoIntimacaoChange(Sender: TObject);
    procedure edReciboKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDadosBaixa: TFDadosBaixa;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFDadosBaixa.btConfirmarClick(Sender: TObject);
begin
  if (edSelo.Enabled) and (edSelo.Text='') then
  begin
      GR.Aviso('Informe o Selo.');
      edSelo.SetFocus;
      Exit;
  end;

  if (edRecibo.Enabled) and (edRecibo.Text='') then
  begin
      GR.Aviso('Informe o N� do Recibo.');
      edRecibo.SetFocus;
      Exit;
  end;

  if (edRegistro.Enabled) and (edRegistro.Text='') then
  begin
      GR.Aviso('Informe o N� do Registro.');
      edRegistro.SetFocus;
      Exit;
  end;

  if (edLivro.Enabled) and (edLivro.Text='') then
  begin
      GR.Aviso('Informe o Livro.');
      edLivro.SetFocus;
      Exit;
  end;

  if (edFolha.Enabled) and (edFolha.Text='') then
  begin
      GR.Aviso('Informe a Folha.');
      edFolha.SetFocus;
      Exit;
  end;

  if (edDataPublicacao.Enabled) and (edDataPublicacao.Date=0) then
  begin
      GR.Aviso('Informe a Data de Publica��o.');
      edDataPublicacao.SetFocus;
      Exit;
  end;

  if (lkMotivo.Enabled) and (lkMotivo.KeyValue=0) then
  begin
      GR.Aviso('Informe o Motivo da Intima��o por Edital.');
      lkMotivo.SetFocus;
      Exit;
  end;

  if (lkEscreventes.Enabled) and (lkEscreventes.Text='') then
  begin
      GR.Aviso('Informe o Escrevente.');
      lkEscreventes.SetFocus;
      Exit;
  end;

  dm.vOkGeral:=True;
  Close;
end;

procedure TFDadosBaixa.btCancelarClick(Sender: TObject);
begin
  dm.vOkGeral:=False;
  Close;
end;

procedure TFDadosBaixa.edSeloExit(Sender: TObject);
begin
  edSelo.Text:=PF.FormatarSelo(edSelo.Text);
end;

procedure TFDadosBaixa.edRegistroKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFDadosBaixa.edLivroKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFDadosBaixa.edFolhaKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFDadosBaixa.FormCreate(Sender: TObject);
begin
  dm.Escreventes.Close;
  dm.Escreventes.Open;
  lkEscreventes.KeyValue:=dm.vNome;

  dm.Motivos.Close;
  dm.Motivos.Open;
end;

procedure TFDadosBaixa.cbTipoIntimacaoChange(Sender: TObject);
begin
  if cbTipoIntimacao.ItemIndex<>1 then
  begin
      edDataPublicacao.Clear;
      lkMotivo.KeyValue:=0;
      edDataPublicacao.Enabled:=False;
      lkMotivo.Enabled        :=False;
  end
  else
  begin
      edDataPublicacao.Enabled:=True;
      lkMotivo.Enabled        :=True;
  end;
end;

procedure TFDadosBaixa.edReciboKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

end.
