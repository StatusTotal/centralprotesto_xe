object FImportarDistribuidor: TFImportarDistribuidor
  Left = 362
  Top = 199
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Importar Arquivo'
  ClientHeight = 502
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 16
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 502
    Align = alClient
    TabOrder = 0
    OnMouseMove = P1MouseMove
    SkinData.SkinSection = 'SELECTION'
    object M: TMemo
      Left = 7
      Top = 119
      Width = 770
      Height = 130
      BorderStyle = bsNone
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 4
      Visible = False
    end
    object P4: TsPanel
      Left = 6
      Top = 7
      Width = 771
      Height = 54
      TabOrder = 0
      SkinData.SkinSection = 'SCROLLSLIDERV'
      object edData: TsDateEdit
        Left = 461
        Top = 16
        Width = 97
        Height = 24
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
        Text = '  /  /    '
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Data'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -13
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
      end
      object btArquivo: TsBitBtn
        Left = 8
        Top = 8
        Width = 78
        Height = 39
        Cursor = crHandPoint
        Caption = 'Arquivo'
        TabOrder = 1
        OnClick = btArquivoClick
        ImageIndex = 27
        Images = dm.Imagens
        Reflected = True
        SkinData.SkinSection = 'BUTTON'
      end
      object btProcessar: TsBitBtn
        Left = 91
        Top = 8
        Width = 97
        Height = 39
        Cursor = crHandPoint
        Caption = 'Processar'
        TabOrder = 2
        OnClick = btProcessarClick
        ImageIndex = 38
        Images = dm.Imagens
        SkinData.SkinSection = 'BUTTON'
      end
      object edQtd: TsCurrencyEdit
        Left = 699
        Top = 16
        Width = 57
        Height = 24
        AutoSize = False
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Quantidade de T'#237'tulos'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -13
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        DecimalPlaces = 0
        DisplayFormat = '0'
      end
      object btIrregularidades: TsBitBtn
        Left = 193
        Top = 8
        Width = 122
        Height = 39
        Cursor = crHandPoint
        Caption = 'Irregularidades'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnClick = btIrregularidadesClick
        ImageIndex = 34
        Images = dm.Imagens
        Reflected = True
        SkinData.SkinSection = 'BUTTON'
      end
      object btSelo: TsBitBtn
        Left = 320
        Top = 8
        Width = 92
        Height = 39
        Cursor = crHandPoint
        Caption = 'Selo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnClick = btSeloClick
        ImageIndex = 24
        Images = dm.Imagens
        Reflected = True
        SkinData.SkinSection = 'BUTTON'
      end
    end
    object Grid: TwwDBGrid
      Left = 6
      Top = 70
      Width = 771
      Height = 196
      ControlType.Strings = (
        'Devolvido;CheckBox;True;False'
        'Retirado;CheckBox;True;False')
      Selected.Strings = (
        'Retirado'#9'8'#9'Retirado'#9'F'
        'Protocolo'#9'13'#9'Protocolo'#9'F'
        'CodigoPortador'#9'7'#9'C'#243'digo'#9'F'
        'Portador'#9'44'#9'Portador'#9'F'
        'NumeroTitulo'#9'16'#9'N'#186' T'#237'tulo'#9'F'
        'Selo'#9'13'#9'Selo'#9'F')
      IniAttributes.Delimiter = ';;'
      IniAttributes.UnicodeIniFile = False
      TitleColor = clBtnFace
      FixedCols = 1
      ShowHorzScrollBar = True
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
      ParentFont = False
      PopupMenu = PM
      ReadOnly = True
      TabOrder = 1
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'Arial'
      TitleFont.Style = []
      TitleLines = 1
      TitleButtons = False
      UseTFields = False
      OnKeyDown = GridKeyDown
      OnMouseMove = GridMouseMove
      OnMouseUp = GridMouseUp
    end
    object P2: TsPanel
      Left = 6
      Top = 310
      Width = 771
      Height = 138
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'BARTITLE'
      object edDevedor: TsDBEdit
        Left = 11
        Top = 66
        Width = 305
        Height = 22
        Color = clWhite
        DataField = 'Devedor'
        DataSource = dsRX
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Devedor'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edDataTitulo: TsDBDateEdit
        Left = 11
        Top = 25
        Width = 98
        Height = 22
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
        Text = '  /  /    '
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Data de Emiss'#227'o'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        DataField = 'DataEmissao'
        DataSource = dsRX
      end
      object edValor: TsDBEdit
        Left = 128
        Top = 25
        Width = 84
        Height = 22
        Color = clWhite
        DataField = 'ValorTitulo'
        DataSource = dsRX
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Valor'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edEspecie: TsDBEdit
        Left = 444
        Top = 25
        Width = 238
        Height = 22
        Color = clWhite
        DataField = 'Natureza'
        DataSource = dsRX
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Esp'#233'cie'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edDataVencimento: TsDBDateEdit
        Left = 339
        Top = 25
        Width = 98
        Height = 22
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 3
        Text = '  /  /    '
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Data do Vencimento'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        DataField = 'DataVencimento'
        DataSource = dsRX
      end
      object edDocumento: TsDBEdit
        Left = 339
        Top = 66
        Width = 158
        Height = 22
        Color = clWhite
        DataField = 'DocDevedor'
        DataSource = dsRX
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Documento'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edEndereco: TsDBEdit
        Left = 11
        Top = 107
        Width = 201
        Height = 22
        Color = clWhite
        DataField = 'Endereco'
        DataSource = dsRX
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Endere'#231'o'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edAgencia: TsDBEdit
        Left = 504
        Top = 107
        Width = 124
        Height = 22
        Color = clWhite
        DataField = 'AgenciaCedente'
        DataSource = dsRX
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 13
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Ag'#234'ncia Cedente'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edUF: TsDBEdit
        Left = 339
        Top = 107
        Width = 23
        Height = 22
        Color = clWhite
        DataField = 'UF'
        DataSource = dsRX
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 11
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'UF'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edCidade: TsDBEdit
        Left = 368
        Top = 107
        Width = 129
        Height = 22
        Color = clWhite
        DataField = 'Cidade'
        DataSource = dsRX
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 12
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Cidade'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edPraca: TsDBEdit
        Left = 504
        Top = 66
        Width = 255
        Height = 22
        Color = clWhite
        DataField = 'PracaPagamento'
        DataSource = dsRX
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Pra'#231'a'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edNossoNumero: TsDBEdit
        Left = 635
        Top = 107
        Width = 124
        Height = 22
        Color = clWhite
        DataField = 'NossoNumero'
        DataSource = dsRX
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 14
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Nosso N'#250'mero'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edDistribuicao: TsDBEdit
        Left = 689
        Top = 25
        Width = 70
        Height = 22
        Color = clWhite
        DataField = 'ValorDistribuicao'
        DataSource = dsRX
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Distribui'#231#227'o'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edSaldo: TsDBEdit
        Left = 232
        Top = 25
        Width = 84
        Height = 22
        Color = clWhite
        DataField = 'SaldoTitulo'
        DataSource = dsRX
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Saldo'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edCEP: TsDBEdit
        Left = 232
        Top = 105
        Width = 84
        Height = 22
        Color = clWhite
        DataField = 'CEPDevedor'
        DataSource = dsRX
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'CEP'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
    end
    object PB: TsProgressBar
      Left = 6
      Top = 455
      Width = 771
      Height = 41
      TabOrder = 3
      Visible = False
      ProgressSkin = 'DRAGBAR'
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object P3: TsPanel
      Left = 6
      Top = 273
      Width = 771
      Height = 30
      TabOrder = 5
      Visible = False
      SkinData.SkinSection = 'HINT'
      object sLabel1: TsLabel
        Left = 6
        Top = 7
        Width = 83
        Height = 15
        Caption = 'Irregularidade:'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 5059883
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      object txMotivo: TsDBText
        Left = 95
        Top = 7
        Width = 671
        Height = 15
        Caption = 'txMotivo'
        ParentFont = False
        ShowAccelChar = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 5059883
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        DataField = 'MotivoRejeicao'
        DataSource = dsRX
      end
    end
  end
  object Opd: TsOpenDialog
    Left = 232
    Top = 192
  end
  object RX: TRxMemoryData
    FieldDefs = <
      item
        Name = 'Protocolo'
        DataType = ftString
        Size = 7
      end
      item
        Name = 'CodigoPortador'
        DataType = ftInteger
      end
      item
        Name = 'Portador'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'NumeroTitulo'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'NumeroDistribuicao'
        DataType = ftString
        Size = 7
      end
      item
        Name = 'Sacador'
        DataType = ftString
        Size = 45
      end
      item
        Name = 'Cedente'
        DataType = ftString
        Size = 45
      end
      item
        Name = 'Devedor'
        DataType = ftString
        Size = 45
      end
      item
        Name = 'Natureza'
        DataType = ftString
        Size = 3
      end
      item
        Name = 'DataEmissao'
        DataType = ftDate
      end
      item
        Name = 'DataVencimento'
        DataType = ftDate
      end
      item
        Name = 'ValorTitulo'
        DataType = ftFloat
      end
      item
        Name = 'SaldoTitulo'
        DataType = ftFloat
      end
      item
        Name = 'PracaPagamento'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Endosso'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Aceite'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TipoDevedor'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DocDevedor'
        DataType = ftString
        Size = 14
      end
      item
        Name = 'IdentidadeDevedor'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'Endereco'
        DataType = ftString
        Size = 45
      end
      item
        Name = 'Cidade'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'UF'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'ValorDistribuicao'
        DataType = ftFloat
      end
      item
        Name = 'AgenciaCedente'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'NossoNumero'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'Irregularidade'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'MotivoRejeicao'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Retirado'
        DataType = ftBoolean
      end
      item
        Name = 'Selo'
        DataType = ftString
        Size = 8
      end>
    BeforePost = RXBeforePost
    Left = 152
    Top = 192
    object RXProtocolo: TStringField
      Alignment = taCenter
      DisplayWidth = 11
      FieldName = 'Protocolo'
      Size = 7
    end
    object RXCodigoPortador: TIntegerField
      Alignment = taCenter
      DisplayLabel = '#'
      DisplayWidth = 5
      FieldName = 'CodigoPortador'
    end
    object RXPortador: TStringField
      DisplayWidth = 33
      FieldName = 'Portador'
      Size = 100
    end
    object RXNumeroTitulo: TStringField
      DisplayLabel = 'N'#186' T'#237'tulo'
      DisplayWidth = 11
      FieldName = 'NumeroTitulo'
      Size = 11
    end
    object RXNumeroDistribuicao: TStringField
      Alignment = taCenter
      DisplayLabel = 'N'#186' Distribui'#231#227'o'
      DisplayWidth = 12
      FieldName = 'NumeroDistribuicao'
      Size = 7
    end
    object RXSacador: TStringField
      DisplayWidth = 28
      FieldName = 'Sacador'
      Visible = False
      Size = 45
    end
    object RXCedente: TStringField
      DisplayWidth = 10
      FieldName = 'Cedente'
      Visible = False
      Size = 45
    end
    object RXDevedor: TStringField
      DisplayWidth = 45
      FieldName = 'Devedor'
      Visible = False
      Size = 45
    end
    object RXNatureza: TStringField
      DisplayWidth = 3
      FieldName = 'Natureza'
      Visible = False
      Size = 3
    end
    object RXDataEmissao: TDateField
      DisplayWidth = 10
      FieldName = 'DataEmissao'
      Visible = False
    end
    object RXDataVencimento: TDateField
      DisplayWidth = 10
      FieldName = 'DataVencimento'
      Visible = False
    end
    object RXValorTitulo: TFloatField
      DisplayWidth = 10
      FieldName = 'ValorTitulo'
      Visible = False
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXSaldoTitulo: TFloatField
      DisplayWidth = 10
      FieldName = 'SaldoTitulo'
      Visible = False
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXPracaPagamento: TStringField
      DisplayWidth = 20
      FieldName = 'PracaPagamento'
      Visible = False
    end
    object RXEndosso: TStringField
      DisplayWidth = 1
      FieldName = 'Endosso'
      Visible = False
      Size = 1
    end
    object RXAceite: TStringField
      DisplayWidth = 1
      FieldName = 'Aceite'
      Visible = False
      Size = 1
    end
    object RXTipoDevedor: TStringField
      DisplayWidth = 1
      FieldName = 'TipoDevedor'
      Visible = False
      Size = 1
    end
    object RXDocDevedor: TStringField
      DisplayWidth = 14
      FieldName = 'DocDevedor'
      Visible = False
      Size = 14
    end
    object RXIdentidadeDevedor: TStringField
      DisplayWidth = 11
      FieldName = 'IdentidadeDevedor'
      Visible = False
      Size = 11
    end
    object RXValorDistribuicao: TFloatField
      DisplayWidth = 10
      FieldName = 'ValorDistribuicao'
      Visible = False
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXEndereco: TStringField
      DisplayWidth = 45
      FieldName = 'Endereco'
      Visible = False
      Size = 45
    end
    object RXCidade: TStringField
      DisplayWidth = 20
      FieldName = 'Cidade'
      Visible = False
    end
    object RXUF: TStringField
      DisplayWidth = 2
      FieldName = 'UF'
      Visible = False
      Size = 2
    end
    object RXAgenciaCedente: TStringField
      DisplayWidth = 15
      FieldName = 'AgenciaCedente'
      Visible = False
      Size = 15
    end
    object RXNossoNumero: TStringField
      DisplayWidth = 15
      FieldName = 'NossoNumero'
      Visible = False
      Size = 15
    end
    object RXIrregularidade: TStringField
      FieldName = 'Irregularidade'
      Size = 2
    end
    object RXMotivoRejeicao: TStringField
      FieldName = 'MotivoRejeicao'
      Size = 100
    end
    object RXRetirado: TBooleanField
      FieldName = 'Retirado'
      OnChange = RXRetiradoChange
    end
    object RXSelo: TStringField
      Alignment = taCenter
      FieldName = 'Selo'
      Size = 8
    end
    object RXAVista: TStringField
      FieldName = 'AVista'
      Size = 1
    end
    object RXCEPDevedor: TStringField
      FieldName = 'CEPDevedor'
      EditMask = '99.999-99;0;_'
      Size = 8
    end
  end
  object dsRX: TDataSource
    DataSet = RX
    OnDataChange = dsRXDataChange
    Left = 192
    Top = 192
  end
  object PM: TPopupMenu
    Left = 312
    Top = 192
    object Aceitar1: TMenuItem
      Caption = 'Marcar todos'
      OnClick = Aceitar1Click
    end
    object Rejeitar1: TMenuItem
      Caption = 'Desmarcar todos'
      OnClick = Rejeitar1Click
    end
  end
  object Duplicado: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select * from titulos where '
      ''
      'NUMERO_TITULO=:NUMERO and '
      'CODIGO_APRESENTANTE=:CODIGO'
      'and DT_PROTOCOLO=:DATA')
    Left = 112
    Top = 192
    ParamData = <
      item
        Name = 'NUMERO'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CODIGO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'DATA'
        DataType = ftDate
        ParamType = ptInput
      end>
    object DuplicadoID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object DuplicadoCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
    end
    object DuplicadoRECIBO: TIntegerField
      FieldName = 'RECIBO'
      Origin = 'RECIBO'
    end
    object DuplicadoDT_ENTRADA: TDateField
      FieldName = 'DT_ENTRADA'
      Origin = 'DT_ENTRADA'
    end
    object DuplicadoDT_PROTOCOLO: TDateField
      FieldName = 'DT_PROTOCOLO'
      Origin = 'DT_PROTOCOLO'
    end
    object DuplicadoPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object DuplicadoLIVRO_PROTOCOLO: TIntegerField
      FieldName = 'LIVRO_PROTOCOLO'
      Origin = 'LIVRO_PROTOCOLO'
    end
    object DuplicadoFOLHA_PROTOCOLO: TStringField
      FieldName = 'FOLHA_PROTOCOLO'
      Origin = 'FOLHA_PROTOCOLO'
      Size = 10
    end
    object DuplicadoDT_PRAZO: TDateField
      FieldName = 'DT_PRAZO'
      Origin = 'DT_PRAZO'
    end
    object DuplicadoDT_REGISTRO: TDateField
      FieldName = 'DT_REGISTRO'
      Origin = 'DT_REGISTRO'
    end
    object DuplicadoREGISTRO: TIntegerField
      FieldName = 'REGISTRO'
      Origin = 'REGISTRO'
    end
    object DuplicadoLIVRO_REGISTRO: TIntegerField
      FieldName = 'LIVRO_REGISTRO'
      Origin = 'LIVRO_REGISTRO'
    end
    object DuplicadoFOLHA_REGISTRO: TStringField
      FieldName = 'FOLHA_REGISTRO'
      Origin = 'FOLHA_REGISTRO'
      Size = 10
    end
    object DuplicadoSELO_REGISTRO: TStringField
      FieldName = 'SELO_REGISTRO'
      Origin = 'SELO_REGISTRO'
      Size = 9
    end
    object DuplicadoDT_PAGAMENTO: TDateField
      FieldName = 'DT_PAGAMENTO'
      Origin = 'DT_PAGAMENTO'
    end
    object DuplicadoSELO_PAGAMENTO: TStringField
      FieldName = 'SELO_PAGAMENTO'
      Origin = 'SELO_PAGAMENTO'
      Size = 9
    end
    object DuplicadoRECIBO_PAGAMENTO: TIntegerField
      FieldName = 'RECIBO_PAGAMENTO'
      Origin = 'RECIBO_PAGAMENTO'
    end
    object DuplicadoCOBRANCA: TStringField
      FieldName = 'COBRANCA'
      Origin = 'COBRANCA'
      FixedChar = True
      Size = 2
    end
    object DuplicadoEMOLUMENTOS: TFloatField
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
    end
    object DuplicadoFETJ: TFloatField
      FieldName = 'FETJ'
      Origin = 'FETJ'
    end
    object DuplicadoFUNDPERJ: TFloatField
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
    end
    object DuplicadoFUNPERJ: TFloatField
      FieldName = 'FUNPERJ'
      Origin = 'FUNPERJ'
    end
    object DuplicadoFUNARPEN: TFloatField
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
    end
    object DuplicadoPMCMV: TFloatField
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
    end
    object DuplicadoISS: TFloatField
      FieldName = 'ISS'
      Origin = 'ISS'
    end
    object DuplicadoMUTUA: TFloatField
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
    end
    object DuplicadoDISTRIBUICAO: TFloatField
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
    end
    object DuplicadoACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
    end
    object DuplicadoTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
    end
    object DuplicadoTIPO_PROTESTO: TIntegerField
      FieldName = 'TIPO_PROTESTO'
      Origin = 'TIPO_PROTESTO'
    end
    object DuplicadoTIPO_TITULO: TIntegerField
      FieldName = 'TIPO_TITULO'
      Origin = 'TIPO_TITULO'
    end
    object DuplicadoNUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
      Origin = 'NUMERO_TITULO'
    end
    object DuplicadoDT_TITULO: TDateField
      FieldName = 'DT_TITULO'
      Origin = 'DT_TITULO'
    end
    object DuplicadoBANCO: TStringField
      FieldName = 'BANCO'
      Origin = 'BANCO'
    end
    object DuplicadoAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Origin = 'AGENCIA'
      Size = 10
    end
    object DuplicadoCONTA: TStringField
      FieldName = 'CONTA'
      Origin = 'CONTA'
      Size = 10
    end
    object DuplicadoVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
      Origin = 'VALOR_TITULO'
    end
    object DuplicadoSALDO_PROTESTO: TFloatField
      FieldName = 'SALDO_PROTESTO'
      Origin = 'SALDO_PROTESTO'
    end
    object DuplicadoDT_VENCIMENTO: TDateField
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
    end
    object DuplicadoCONVENIO: TStringField
      FieldName = 'CONVENIO'
      Origin = 'CONVENIO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoTIPO_APRESENTACAO: TStringField
      FieldName = 'TIPO_APRESENTACAO'
      Origin = 'TIPO_APRESENTACAO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoDT_ENVIO: TDateField
      FieldName = 'DT_ENVIO'
      Origin = 'DT_ENVIO'
    end
    object DuplicadoCODIGO_APRESENTANTE: TStringField
      FieldName = 'CODIGO_APRESENTANTE'
      Origin = 'CODIGO_APRESENTANTE'
      Size = 10
    end
    object DuplicadoTIPO_INTIMACAO: TStringField
      FieldName = 'TIPO_INTIMACAO'
      Origin = 'TIPO_INTIMACAO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoMOTIVO_INTIMACAO: TMemoField
      FieldName = 'MOTIVO_INTIMACAO'
      Origin = 'MOTIVO_INTIMACAO'
      BlobType = ftMemo
    end
    object DuplicadoDT_INTIMACAO: TDateField
      FieldName = 'DT_INTIMACAO'
      Origin = 'DT_INTIMACAO'
    end
    object DuplicadoDT_PUBLICACAO: TDateField
      FieldName = 'DT_PUBLICACAO'
      Origin = 'DT_PUBLICACAO'
    end
    object DuplicadoVALOR_PAGAMENTO: TFloatField
      FieldName = 'VALOR_PAGAMENTO'
      Origin = 'VALOR_PAGAMENTO'
    end
    object DuplicadoAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Origin = 'APRESENTANTE'
      Size = 100
    end
    object DuplicadoCPF_CNPJ_APRESENTANTE: TStringField
      FieldName = 'CPF_CNPJ_APRESENTANTE'
      Origin = 'CPF_CNPJ_APRESENTANTE'
      Size = 15
    end
    object DuplicadoTIPO_APRESENTANTE: TStringField
      FieldName = 'TIPO_APRESENTANTE'
      Origin = 'TIPO_APRESENTANTE'
      FixedChar = True
      Size = 1
    end
    object DuplicadoCEDENTE: TStringField
      FieldName = 'CEDENTE'
      Origin = 'CEDENTE'
      Size = 100
    end
    object DuplicadoNOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Origin = 'NOSSO_NUMERO'
      Size = 15
    end
    object DuplicadoSACADOR: TStringField
      FieldName = 'SACADOR'
      Origin = 'SACADOR'
      Size = 100
    end
    object DuplicadoDEVEDOR: TStringField
      FieldName = 'DEVEDOR'
      Origin = 'DEVEDOR'
      Size = 100
    end
    object DuplicadoCPF_CNPJ_DEVEDOR: TStringField
      FieldName = 'CPF_CNPJ_DEVEDOR'
      Origin = 'CPF_CNPJ_DEVEDOR'
      Size = 15
    end
    object DuplicadoTIPO_DEVEDOR: TStringField
      FieldName = 'TIPO_DEVEDOR'
      Origin = 'TIPO_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object DuplicadoAGENCIA_CEDENTE: TStringField
      FieldName = 'AGENCIA_CEDENTE'
      Origin = 'AGENCIA_CEDENTE'
      Size = 15
    end
    object DuplicadoPRACA_PROTESTO: TStringField
      FieldName = 'PRACA_PROTESTO'
      Origin = 'PRACA_PROTESTO'
    end
    object DuplicadoTIPO_ENDOSSO: TStringField
      FieldName = 'TIPO_ENDOSSO'
      Origin = 'TIPO_ENDOSSO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoACEITE: TStringField
      FieldName = 'ACEITE'
      Origin = 'ACEITE'
      FixedChar = True
      Size = 1
    end
    object DuplicadoCPF_ESCREVENTE: TStringField
      FieldName = 'CPF_ESCREVENTE'
      Origin = 'CPF_ESCREVENTE'
      Size = 11
    end
    object DuplicadoCPF_ESCREVENTE_PG: TStringField
      FieldName = 'CPF_ESCREVENTE_PG'
      Origin = 'CPF_ESCREVENTE_PG'
      Size = 11
    end
    object DuplicadoOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      BlobType = ftMemo
    end
    object DuplicadoDT_SUSTADO: TDateField
      FieldName = 'DT_SUSTADO'
      Origin = 'DT_SUSTADO'
    end
    object DuplicadoDT_RETIRADO: TDateField
      FieldName = 'DT_RETIRADO'
      Origin = 'DT_RETIRADO'
    end
    object DuplicadoSTATUS: TStringField
      FieldName = 'STATUS'
      Origin = 'STATUS'
    end
    object DuplicadoPROTESTADO: TStringField
      FieldName = 'PROTESTADO'
      Origin = 'PROTESTADO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoENVIADO_APONTAMENTO: TStringField
      FieldName = 'ENVIADO_APONTAMENTO'
      Origin = 'ENVIADO_APONTAMENTO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoENVIADO_PROTESTO: TStringField
      FieldName = 'ENVIADO_PROTESTO'
      Origin = 'ENVIADO_PROTESTO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoENVIADO_PAGAMENTO: TStringField
      FieldName = 'ENVIADO_PAGAMENTO'
      Origin = 'ENVIADO_PAGAMENTO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoENVIADO_RETIRADO: TStringField
      FieldName = 'ENVIADO_RETIRADO'
      Origin = 'ENVIADO_RETIRADO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoENVIADO_SUSTADO: TStringField
      FieldName = 'ENVIADO_SUSTADO'
      Origin = 'ENVIADO_SUSTADO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoENVIADO_DEVOLVIDO: TStringField
      FieldName = 'ENVIADO_DEVOLVIDO'
      Origin = 'ENVIADO_DEVOLVIDO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoEXPORTADO: TStringField
      FieldName = 'EXPORTADO'
      Origin = 'EXPORTADO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoAVALISTA_DEVEDOR: TStringField
      FieldName = 'AVALISTA_DEVEDOR'
      Origin = 'AVALISTA_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object DuplicadoFINS_FALIMENTARES: TStringField
      FieldName = 'FINS_FALIMENTARES'
      Origin = 'FINS_FALIMENTARES'
      FixedChar = True
      Size = 1
    end
    object DuplicadoTARIFA_BANCARIA: TFloatField
      FieldName = 'TARIFA_BANCARIA'
      Origin = 'TARIFA_BANCARIA'
    end
    object DuplicadoFORMA_PAGAMENTO: TStringField
      FieldName = 'FORMA_PAGAMENTO'
      Origin = 'FORMA_PAGAMENTO'
      FixedChar = True
      Size = 2
    end
    object DuplicadoNUMERO_PAGAMENTO: TStringField
      FieldName = 'NUMERO_PAGAMENTO'
      Origin = 'NUMERO_PAGAMENTO'
      Size = 40
    end
    object DuplicadoARQUIVO: TStringField
      FieldName = 'ARQUIVO'
      Origin = 'ARQUIVO'
    end
    object DuplicadoRETORNO: TStringField
      FieldName = 'RETORNO'
      Origin = 'RETORNO'
    end
    object DuplicadoDT_DEVOLVIDO: TDateField
      FieldName = 'DT_DEVOLVIDO'
      Origin = 'DT_DEVOLVIDO'
    end
    object DuplicadoCPF_CNPJ_SACADOR: TStringField
      FieldName = 'CPF_CNPJ_SACADOR'
      Origin = 'CPF_CNPJ_SACADOR'
      Size = 14
    end
    object DuplicadoID_MSG: TIntegerField
      FieldName = 'ID_MSG'
      Origin = 'ID_MSG'
    end
    object DuplicadoVALOR_AR: TFloatField
      FieldName = 'VALOR_AR'
      Origin = 'VALOR_AR'
    end
    object DuplicadoELETRONICO: TStringField
      FieldName = 'ELETRONICO'
      Origin = 'ELETRONICO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoIRREGULARIDADE: TIntegerField
      FieldName = 'IRREGULARIDADE'
      Origin = 'IRREGULARIDADE'
    end
    object DuplicadoAVISTA: TStringField
      FieldName = 'AVISTA'
      Origin = 'AVISTA'
      FixedChar = True
      Size = 1
    end
    object DuplicadoSALDO_TITULO: TFloatField
      FieldName = 'SALDO_TITULO'
      Origin = 'SALDO_TITULO'
    end
    object DuplicadoTIPO_SUSTACAO: TStringField
      FieldName = 'TIPO_SUSTACAO'
      Origin = 'TIPO_SUSTACAO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoRETORNO_PROTESTO: TStringField
      FieldName = 'RETORNO_PROTESTO'
      Origin = 'RETORNO_PROTESTO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoDT_RETORNO_PROTESTO: TDateField
      FieldName = 'DT_RETORNO_PROTESTO'
      Origin = 'DT_RETORNO_PROTESTO'
    end
    object DuplicadoDT_DEFINITIVA: TDateField
      FieldName = 'DT_DEFINITIVA'
      Origin = 'DT_DEFINITIVA'
    end
    object DuplicadoJUDICIAL: TStringField
      FieldName = 'JUDICIAL'
      Origin = 'JUDICIAL'
      FixedChar = True
      Size = 1
    end
    object DuplicadoALEATORIO_PROTESTO: TStringField
      FieldName = 'ALEATORIO_PROTESTO'
      Origin = 'ALEATORIO_PROTESTO'
      Size = 3
    end
    object DuplicadoALEATORIO_SOLUCAO: TStringField
      FieldName = 'ALEATORIO_SOLUCAO'
      Origin = 'ALEATORIO_SOLUCAO'
      Size = 3
    end
    object DuplicadoCCT: TStringField
      FieldName = 'CCT'
      Origin = 'CCT'
      Size = 9
    end
    object DuplicadoDETERMINACAO: TStringField
      FieldName = 'DETERMINACAO'
      Origin = 'DETERMINACAO'
      Size = 100
    end
    object DuplicadoANTIGO: TStringField
      FieldName = 'ANTIGO'
      Origin = 'ANTIGO'
      FixedChar = True
      Size = 1
    end
    object DuplicadoLETRA: TStringField
      FieldName = 'LETRA'
      Origin = 'LETRA'
      Size = 1
    end
    object DuplicadoPROTOCOLO_CARTORIO: TIntegerField
      FieldName = 'PROTOCOLO_CARTORIO'
      Origin = 'PROTOCOLO_CARTORIO'
    end
    object DuplicadoARQUIVO_CARTORIO: TStringField
      FieldName = 'ARQUIVO_CARTORIO'
      Origin = 'ARQUIVO_CARTORIO'
    end
    object DuplicadoRETORNO_CARTORIO: TStringField
      FieldName = 'RETORNO_CARTORIO'
      Origin = 'RETORNO_CARTORIO'
    end
    object DuplicadoDT_DEVOLVIDO_CARTORIO: TDateField
      FieldName = 'DT_DEVOLVIDO_CARTORIO'
      Origin = 'DT_DEVOLVIDO_CARTORIO'
    end
    object DuplicadoCARTORIO: TIntegerField
      FieldName = 'CARTORIO'
      Origin = 'CARTORIO'
    end
    object DuplicadoDT_PROTOCOLO_CARTORIO: TDateField
      FieldName = 'DT_PROTOCOLO_CARTORIO'
      Origin = 'DT_PROTOCOLO_CARTORIO'
    end
  end
  object Faixa: TFDQuery
    Connection = dm.conCENTRAL
    SQL.Strings = (
      'select COD,EMOL,FETJ,FUND,FUNP,MUTUA,ACOTERJ,'
      'DISTRIB,TOT, TITULO from COD'
      ''
      'where '
      ''
      '(ATRIB=4) AND'
      ''
      '(CONVENIO=:C) AND (OCULTO=:OCULTO1 OR :OCULTO2 = '#39'T'#39') AND'
      ''
      '(MINIMO <= :VALOR1 AND MAXIMO >= :VALOR2)'
      ''
      'group by COD,EMOL,FETJ,FUND,FUNP,MUTUA,ACOTERJ,'
      'DISTRIB,TOT, TITULO')
    Left = 272
    Top = 192
    ParamData = <
      item
        Name = 'C'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'OCULTO1'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'OCULTO2'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'VALOR1'
        DataType = ftFloat
        ParamType = ptInput
      end
      item
        Name = 'VALOR2'
        DataType = ftFloat
        ParamType = ptInput
      end>
    object FaixaCOD: TIntegerField
      FieldName = 'COD'
      Origin = 'COD'
    end
    object FaixaEMOL: TFloatField
      FieldName = 'EMOL'
      Origin = 'EMOL'
    end
    object FaixaFETJ: TFloatField
      FieldName = 'FETJ'
      Origin = 'FETJ'
    end
    object FaixaFUND: TFloatField
      FieldName = 'FUND'
      Origin = 'FUND'
    end
    object FaixaFUNP: TFloatField
      FieldName = 'FUNP'
      Origin = 'FUNP'
    end
    object FaixaMUTUA: TFloatField
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
    end
    object FaixaACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
    end
    object FaixaDISTRIB: TFloatField
      FieldName = 'DISTRIB'
      Origin = 'DISTRIB'
    end
    object FaixaTOT: TFloatField
      FieldName = 'TOT'
      Origin = 'TOT'
    end
    object FaixaTITULO: TStringField
      FieldName = 'TITULO'
      Origin = 'TITULO'
      Size = 50
    end
  end
end
