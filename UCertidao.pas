unit UCertidao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, DB, StdCtrls, RxDBComb, Buttons, sBitBtn,
  Mask, sMaskEdit, sCustomComboEdit, sTooledit, sDBDateEdit, sSpeedButton,
  DBCtrls, sDBEdit, sDBComboBox, sLabel, sDBLookupComboBox, sDBMemo,
  sCurrEdit, sDBCalcEdit, sCheckBox, sDBCheckBox, sGroupBox, FMTBcd,
  SqlExpr, Provider, DBClient, Grids, Wwdbigrd, Wwdbgrid, acPNG, sDBText,
  sButton, sCurrencyEdit, DBGrids, Math, acImage, acDBComboBoxEx,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFCertidao = class(TForm)
    P1: TsPanel;
    btSalvar: TsBitBtn;
    btCancelar: TsBitBtn;
    dsCertidao: TDataSource;
    dsEscreventes: TDataSource;
    ckConvenio: TsDBCheckBox;
    Grid: TwwDBGrid;
    dsConsulta: TDataSource;
    Consulta: TClientDataSet;
    dspConsulta: TDataSetProvider;
    ConsultaID_ATO: TIntegerField;
    ConsultaLIVRO_PROTOCOLO: TIntegerField;
    ConsultaFOLHA_PROTOCOLO: TStringField;
    ConsultaPROTOCOLO: TIntegerField;
    ConsultaDT_PROTOCOLO: TDateField;
    ConsultaDT_REGISTRO: TDateField;
    ConsultaAPRESENTANTE: TStringField;
    ConsultaSTATUS: TStringField;
    GbCertidao: TsGroupBox;
    edFolhas: TsDBEdit;
    edDataCertidao: TsDBDateEdit;
    edSelo: TsDBEdit;
    lkEscrevente: TsDBLookupComboBox;
    MObservacao: TsDBMemo;
    Bv: TBevel;
    lbInformacoes: TsLabel;
    btBusca: TsBitBtn;
    P2: TsPanel;
    txDT_PEDIDO: TsDBText;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    txANOS: TsDBText;
    sLabel3: TsLabel;
    txNOME_CERTIDAO: TsDBText;
    ckEmitir: TsCheckBox;
    lkResponsavel: TsDBLookupComboBox;
    dsResponsavel: TDataSource;
    btRecibo: TsBitBtn;
    edEntrega: TsDBDateEdit;
    edAleatorio: TsDBEdit;
    GBExcedentes: TsGroupBox;
    edExcedentes: TsCurrencyEdit;
    edEmolumentos: TsDBEdit;
    edFetj: TsDBEdit;
    edFundperj: TsDBEdit;
    edFunperj: TsDBEdit;
    edTotal: TsDBEdit;
    edFunarpen: TsDBEdit;
    edCCT: TsDBEdit;
    edRecibo: TsDBEdit;
    ConsultaTotal: TFloatField;
    ConsultaCONVENIO: TStringField;
    ConsultaSALDO_TITULO: TFloatField;
    ConsultaVALOR_TITULO: TFloatField;
    ImLimparCCT: TsImage;
    edIss: TsDBEdit;
    cbTipoCobranca: TsDBComboBoxEx;
    qryTitulo: TFDQuery;
    qryConsulta: TFDQuery;
    qryTituloID_ATO: TIntegerField;
    qryTituloAPRESENTANTE: TStringField;
    qryTituloCEDENTE: TStringField;
    qryTituloDEVEDOR: TStringField;
    qryTituloDT_REGISTRO: TDateField;
    qryTituloDT_TITULO: TDateField;
    qryTituloTIPO_ENDOSSO: TStringField;
    qryTituloTIPO_TITULO: TIntegerField;
    qryTituloLIVRO_REGISTRO: TIntegerField;
    qryTituloFOLHA_REGISTRO: TStringField;
    qryTituloPROTOCOLO: TIntegerField;
    qryTituloSACADOR: TStringField;
    qryTituloNUMERO_TITULO: TStringField;
    qryTituloVALOR_TITULO: TFloatField;
    qryTituloDT_VENCIMENTO: TDateField;
    qryTituloCPF_CNPJ_SACADOR: TStringField;
    qryConsultaID_ATO: TIntegerField;
    qryConsultaLIVRO_PROTOCOLO: TIntegerField;
    qryConsultaFOLHA_PROTOCOLO: TStringField;
    qryConsultaPROTOCOLO: TIntegerField;
    qryConsultaDT_PROTOCOLO: TDateField;
    qryConsultaDT_REGISTRO: TDateField;
    qryConsultaAPRESENTANTE: TStringField;
    qryConsultaSTATUS: TStringField;
    qryConsultaCONVENIO: TStringField;
    qryConsultaSALDO_TITULO: TFloatField;
    qryConsultaVALOR_TITULO: TFloatField;
    procedure btCancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btSalvarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btBuscaClick(Sender: TObject);
    procedure edDocumento2Enter(Sender: TObject);
    procedure btReciboClick(Sender: TObject);
    procedure edFolhasChange(Sender: TObject);
    procedure Calcular;
    procedure ConsultaCalcFields(DataSet: TDataSet);
    procedure FolhasExcedentes;
    procedure edFolhasExit(Sender: TObject);
    procedure edFolhasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ImLimparCCTClick(Sender: TObject);
    procedure CertidaoInteiroTeor;
    procedure cbTipoCobrancaChange(Sender: TObject);
  private
    { Private declarations }
    vIdReservado: Integer;
  public
    { Public declarations }
  end;

var
  FCertidao: TFCertidao;

implementation

uses
  UDM,UPF,DateUtils,UCustas,UQuickPedido1,
  UQuickCertidao1,UPedidoCertidao,UGeral,UGDM,QRCtrls, UQuickInstrumento1;

{$R *.dfm}

procedure TFCertidao.btCancelarClick(Sender: TObject);
begin
  dm.Certidoes.Cancel;
  Gdm.LiberarSelo('PROTESTO',dm.CertidoesSELO.AsString,False,-1,dm.vIdReservado);
  Gdm.LiberarSelo('PROTESTO',dm.CertidoesCCT.AsString,False,-1,vIdReservado);
  Close;
end;

procedure TFCertidao.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then btCancelarClick(Sender);

  if ActiveControl<>MObservacao then
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFCertidao.btSalvarClick(Sender: TObject);

  function TabelaCadastrada: Boolean;
  var
    Q: TFDQuery;
  begin
      Q:=TFDQuery.Create(Nil);
      Q.Connection:=dm.conCENTRAL;
      Q.SQL.Add('SELECT * FROM TABCUS WHERE COD=4022 AND TAB='''+'16'+''' AND ITEM='''+'2'+''' AND SUB='''+'*'+'''');
      Q.Open;
      Result:=not Q.IsEmpty;
      Q.Close;
      Q.Free;
  end;

var
  vTipo: String;
  Posicao: Integer;
  Q: TFDQuery;
begin
  if edDataCertidao.Date=0 then
  begin
      GR.Aviso('INFORME A DATA DA CERTID�O!');
      edDataCertidao.SetFocus;
      Exit;
  end;

  if (edSelo.Text='') or (edSelo.Text='*') then
  begin
      GR.Aviso('INFORME O SELO DA CERTID�O!');
      edSelo.SetFocus;
      Exit;
  end;

  dm.CertidoesDT_ENTREGA.AsDateTime:=edEntrega.Date;

  if Consulta.Active then
  begin
      dm.CertidoesREGISTROS.AsInteger:=Consulta.RecordCount;
      if Consulta.RecordCount>0 then
        dm.CertidoesRESULTADO.AsString:='P'
          else dm.CertidoesRESULTADO.AsString:='N';
  end;

  if dm.CertidoesSELO.AsString=''   then dm.CertidoesSELO.Clear;
  if dm.CertidoesFOLHAS.AsInteger=0 then dm.CertidoesFOLHAS.AsInteger:=1;

  if dm.ValorParametro(72)<>'0' then
    if GR.Pergunta('Gerar N� de Ordem') then
    begin
        dm.CertidoesORDEM.AsInteger:=StrToInt(dm.ValorParametro(72));
        dm.AtualizarParametro(72,IntToStr(dm.CertidoesORDEM.AsInteger+1));
    end;

  dm.Certidoes.Post;
  dm.Certidoes.ApplyUpdates(0);

  if dm.CertidoesTIPO_CERTIDAO.AsString='N' then vTipo:=' NORMAL';
  if dm.CertidoesTIPO_CERTIDAO.AsString='I' then vTipo:=' DE INTEIRO TEOR';

  if dm.vIdReservado<>0 then
    Gdm.BaixarSelo(dm.CertidoesSELO.AsString,
                   dm.vIdReservado,
                   'PROTESTO',
                   Self.Name,
                   dm.CertidoesID_CERTIDAO.AsInteger,
                   0,
                   4,
                   dm.CertidoesDT_CERTIDAO.AsDateTime,
                   '',0,0,0,0,
                   dm.CertidoesRECIBO.AsInteger,
                   dm.CertidoesCODIGO.AsInteger,
                   'CERTID�O'+vTipo,
                   lkEscrevente.Text,
                   dm.CertidoesCONVENIO.AsString,
                   dm.CertidoesCOBRANCA.AsString,
                   dm.CertidoesEMOLUMENTOS.AsFloat,
                   dm.CertidoesFETJ.AsFloat,
                   dm.CertidoesFUNDPERJ.AsFloat,
                   dm.CertidoesFUNPERJ.AsFloat,
                   dm.CertidoesFUNARPEN.AsFloat,
                   dm.CertidoesPMCMV.AsFloat,
                   dm.CertidoesMUTUA.AsFloat,
                   dm.CertidoesACOTERJ.AsFloat,
                   dm.CertidoesDISTRIBUICAO.AsFloat,
                   0,
                   dm.CertidoesAPONTAMENTO.AsFloat,
                   0,0,
                   dm.CertidoesTOTAL.AsFloat,
                   dm.CertidoesISS.AsFloat);

  if vIdReservado<>0 then
  begin
      Gdm.BaixarSelo(dm.CertidoesCCT.AsString,vIdReservado,'PROTESTO',Self.Name,dm.CertidoesID_CERTIDAO.AsInteger,0,4,
                     dm.CertidoesDT_ENTREGA.AsDateTime,'',0,0,0,0,dm.CertidoesEXRECIBO.AsInteger,4023,'FOLHAS EXCEDENTES',dm.vNome,'N','CC',
                     dm.CertidoesEXEMOLUMENTOS.AsFloat,dm.CertidoesEXFETJ.AsFloat,dm.CertidoesEXFUNDPERJ.AsFloat,dm.CertidoesEXFUNPERJ.AsFloat,
                     dm.CertidoesEXFUNARPEN.AsFloat,0,0,0,0,0,0,0,0,dm.CertidoesEXTOTAL.AsFloat,dm.CertidoesEXISS.AsFloat);
      vIdReservado:=0;
  end;

  {CASO TENHA FOLHA EXCEDENTE EU ATUALIZO AS CUSTAS}
  dm.Custas.Close;
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT ID_CUSTA FROM CUSTAS WHERE ID_ATO=:ID AND TABELA=:TB AND ITEM=:IT AND SUBITEM=:SB');
  Q.ParamByName('ID').AsInteger:=dm.CertidoesID_CERTIDAO.AsInteger;
  Q.ParamByName('TB').AsString :='16';
  Q.ParamByName('IT').AsString :='2';
  Q.ParamByName('SB').AsString :='*';
  Q.Open;
  dm.Custas.Params[0].AsInteger:=IfThen(Q.IsEmpty,-1,Q.FieldByName('ID_CUSTA').AsInteger);
  dm.Custas.Open;
  if dm.Custas.IsEmpty then
  begin
      dm.Custas.Append;
      dm.CustasID_CUSTA.AsInteger:=dm.IdAtual('ID_CUSTA','S');
  end
  else
  begin
      {SE O CART�RIO N�O USA ESSA TABELA ENT�O EU EXCLUO ELA}
      {SE USA EU ATUALIZO A QUANTIDADE DE ACORDO COM AS FOLHAS}
      if not TabelaCadastrada then
      begin
          dm.Custas.Delete;
          dm.Custas.ApplyUpdates(0);
      end
      else
      begin
          dm.Custas.Edit;
          dm.CustasID_ATO.AsInteger   :=dm.CertidoesID_CERTIDAO.AsInteger;
          dm.CustasTABELA.AsString    :='16';
          dm.CustasITEM.AsString      :='2';
          dm.CustasSUBITEM.AsString   :='*';
          dm.CustasVALOR.AsFloat      :=dm.SubitemVALOR.AsFloat;
          dm.CustasQTD.AsInteger      :=dm.CertidoesFOLHAS.AsInteger;
          dm.CustasTOTAL.AsFloat      :=dm.CustasVALOR.AsFloat*dm.CustasQTD.AsInteger;
          dm.Custas.Post;
          dm.Custas.ApplyUpdates(0);
      end;
  end;
  Q.Close;
  Q.Free;
  {FIM DA ATUALIZA��O DAS CUSTAS}

  if ckEmitir.Checked then
  begin
      dm.vAssinatura:=dm.ResponsavelNOME.AsString+' - '+dm.ResponsavelQUALIFICACAO.AsString;
      dm.vConferente:=dm.ResponsavelNOME.AsString+' - '+dm.ResponsavelQUALIFICACAO.AsString;
      if dm.ServentiaCODIGO.AsInteger=1726 then
      dm.vAssinatura:=dm.EscreventesNOME.AsString+' - '+dm.EscreventesQUALIFICACAO.AsString;

      if dm.ResponsavelMATRICULA.AsString<>'' then
        dm.vMatricula:='Matr�cula: '+dm.ResponsavelMATRICULA.AsString
          else dm.vMatricula:='';

      if dm.CertidoesTIPO_CERTIDAO.AsString='I' then
      begin
          CertidaoInteiroTeor;
          Close;
          Exit;
      end;
      
      Application.CreateForm(TFQuickCertidao1,FQuickCertidao1);
      with FQuickCertidao1 do
      begin
          RX.Close;
          RX.Open;
          Posicao:=Consulta.RecNo;
          Consulta.DisableControls;
          Consulta.First;
          while not Consulta.Eof do
          begin
              Application.ProcessMessages;
              PF.Aguarde(True);
              qryTitulo.Close;
              qryTitulo.Params[0].AsInteger:=ConsultaID_ATO.AsInteger;
              qryTitulo.Open;

              RX.Append;
              RXApresentante.AsString   :=qryTituloAPRESENTANTE.AsString;
              RXCedente.AsString        :=qryTituloCEDENTE.AsString;
              RXDevedor.AsString        :=qryTituloDEVEDOR.AsString;
              RXDt_Protesto.AsDateTime  :=qryTituloDT_REGISTRO.AsDateTime;
              if qryTituloDT_TITULO.AsDateTime<>0 then
              RXDt_Titulo.AsDateTime    :=qryTituloDT_TITULO.AsDateTime;

              if qryTituloTIPO_ENDOSSO.AsString='M' then
                RXEndosso.AsString:='MANDATO'
                  else if qryTituloTIPO_ENDOSSO.AsString='T' then
                    RXEndosso.AsString:='TRANSLATIVO'
                      else RXEndosso.AsString:='';

              RXEspecie.AsString        :=PF.RetornarTitulo(qryTituloTIPO_TITULO.AsInteger);
              RXLivroFolha.AsString     :=qryTituloLIVRO_REGISTRO.AsString+'/'+qryTituloFOLHA_REGISTRO.AsString;
              RXProtocolo.AsString      :=qryTituloPROTOCOLO.AsString;
              RXSacador.AsString        :=qryTituloSACADOR.AsString+' - '+qryTituloCPF_CNPJ_SACADOR.AsString;
// MARCELO - JUNTEI O CAMPO SACADOR COM O DOC DELE              RXSacador.AsString        :=qryTituloSACADOR.AsString;
              RXTitulo.AsString         :=qryTituloNUMERO_TITULO.AsString;
              RXValor.AsFloat           :=qryTituloVALOR_TITULO.AsFloat;
              RXTotal.AsFloat           :=ConsultaTotal.AsFloat;
              if qryTituloDT_VENCIMENTO.AsDateTime<>0 then
              RXVencimento.AsDateTime   :=qryTituloDT_VENCIMENTO.AsDateTime;
              RX.Post;

              Consulta.Next;
          end;
          Consulta.RecNo:=Posicao;
          Consulta.EnableControls;
          qryTitulo.Close;

          if RX.IsEmpty then Detalhe.Enabled:=False;

          PF.Aguarde(False);
          qkCertidao.Preview;
      end;
  end;
  Close;
end;

procedure TFCertidao.FormCreate(Sender: TObject);
begin
  dm.Subitem.Close;
  dm.Subitem.Params[0].AsString :='16';
  dm.Subitem.Params[1].AsString :='2';
  dm.Subitem.Params[2].AsString :='*';
  dm.Subitem.Params[3].AsInteger:=StrToInt(dm.ValorParametro(77));
  dm.Subitem.Open;

  dm.Escreventes.Close;
  dm.Escreventes.Open;

  dm.Responsavel.Close;
  dm.Responsavel.Open;

  if dm.ServentiaCODIGO.AsInteger=1726 then
  lkResponsavel.BoundLabel.Caption:='Conferente';

  btBuscaClick(Sender);
end;

procedure TFCertidao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.RxCustas.Close;
  dm.Escreventes.Close;
  if dm.Certidoes.State<>dsBrowse then
  btCancelar.Click;
end;

procedure TFCertidao.btBuscaClick(Sender: TObject);
var
  Posicao: Integer;
  Qtd,Palavra,Query: String;
begin
  ActiveControl:=Nil;

  Consulta.Close;
  if dm.CertidoesTIPO_CERTIDAO.AsString='I' then
  begin
      Query:='SELECT ID_ATO,LIVRO_PROTOCOLO,FOLHA_PROTOCOLO,PROTOCOLO,DT_PROTOCOLO,DT_REGISTRO,APRESENTANTE,STATUS,CONVENIO,SALDO_TITULO,'+
             'VALOR_TITULO FROM TITULOS WHERE ID_ATO=:ID_ATO AND STATUS='''+'PROTESTADO'+
             ''' ORDER BY DT_PROTOCOLO';
      Consulta.CommandText:=Query;
      Consulta.Params[0].AsInteger:=dm.CertidoesID_ATO.AsInteger;
  end
  else
  begin
      Query:='SELECT ID_ATO,LIVRO_PROTOCOLO,FOLHA_PROTOCOLO,PROTOCOLO,DT_PROTOCOLO,DT_REGISTRO,APRESENTANTE,STATUS,CONVENIO,SALDO_TITULO,'+
             'VALOR_TITULO FROM TITULOS ';

      if dm.CertidoesNUMERO_TITULO.AsString='' then
        Query:=Query+'WHERE CPF_CNPJ_DEVEDOR=:DOCUMENTO AND DT_REGISTRO BETWEEN :DATA1 AND :DATA2 AND STATUS='''+'PROTESTADO'+''' ORDER BY DT_PROTOCOLO,PROTOCOLO'
          else Query:=Query+'WHERE NUMERO_TITULO=:DOCUMENTO AND DT_REGISTRO BETWEEN :DATA1 AND :DATA2 AND STATUS='''+'PROTESTADO'+''' ORDER BY DT_PROTOCOLO,PROTOCOLO';

      Consulta.CommandText:=Query;
      Consulta.Params[0].AsString:=GR.iif(dm.CertidoesNUMERO_TITULO.AsString='',dm.CertidoesCPF_CNPJ_REQUERIDO.AsString,dm.CertidoesNUMERO_TITULO.AsString);
      Consulta.Params[1].AsDate  :=IncYear(dm.CertidoesDT_PEDIDO.AsDateTime,-dm.CertidoesANOS.AsInteger);
      Consulta.Params[2].AsDate  :=dm.CertidoesDT_PEDIDO.AsDateTime;
  end;
  Consulta.Open;

  Qtd:=IntToStr(Consulta.RecordCount);

  if dm.CertidoesTIPO_CERTIDAO.AsString='I' then
  begin
      if ConsultaSTATUS.AsString='PROTESTADO' then
        lbInformacoes.Caption:='RESULTADO POSITIVO'
          else lbInformacoes.Caption:='RESULTADO NEGATIVO';
  end
  else
  begin
      if Consulta.RecordCount>1 then
        Palavra:=Qtd+' REGISTROS DE PROTESTOS ENCONTRADOS'
          else Palavra:=Qtd+' REGISTRO DE PROTESTO ENCONTRADO';

      if Consulta.IsEmpty then
        lbInformacoes.Caption:='RESULTADO NEGATIVO - N�O CONSTAM REGISTROS DE PROTESTOS'
          else lbInformacoes.Caption:='RESULTADO POSITIVO - '+Palavra;
  end;

  if dm.vBuscar then
  begin
      Application.CreateForm(TFQuickCertidao1,FQuickCertidao1);
      with FQuickCertidao1 do
      begin
          RX.Close;
          RX.Open;
          Posicao:=Consulta.RecNo;
          Consulta.DisableControls;
          Consulta.First;
          while not Consulta.Eof do
          begin
              Application.ProcessMessages;
              PF.Aguarde(True);
              qryTitulo.Close;
              qryTitulo.Params[0].AsInteger:=ConsultaID_ATO.AsInteger;
              qryTitulo.Open;

              RX.Append;
              RXApresentante.AsString   :=qryTituloAPRESENTANTE.AsString;
              RXCedente.AsString        :=qryTituloCEDENTE.AsString;
              RXDevedor.AsString        :=qryTituloDEVEDOR.AsString;
              RXDt_Protesto.AsDateTime  :=qryTituloDT_REGISTRO.AsDateTime;
              if qryTituloDT_TITULO.AsDateTime<>0 then
              RXDt_Titulo.AsDateTime    :=qryTituloDT_TITULO.AsDateTime;

              if qryTituloTIPO_ENDOSSO.AsString='M' then
                RXEndosso.AsString:='MANDATO'
                  else if qryTituloTIPO_ENDOSSO.AsString='T' then
                    RXEndosso.AsString:='TRANSLATIVO'
                      else RXEndosso.AsString:='';

              RXEspecie.AsString        :=PF.RetornarTitulo(qryTituloTIPO_TITULO.AsInteger);
              RXLivroFolha.AsString     :=qryTituloLIVRO_REGISTRO.AsString+'/'+qryTituloFOLHA_REGISTRO.AsString;
              RXProtocolo.AsString      :=qryTituloPROTOCOLO.AsString;
              RXSacador.AsString        :=qryTituloSACADOR.AsString;
              RXTitulo.AsString         :=qryTituloNUMERO_TITULO.AsString;
              RXValor.AsFloat           :=qryTituloVALOR_TITULO.AsFloat;
              RXTotal.AsFloat           :=ConsultaTotal.AsFloat;
              if qryTituloDT_VENCIMENTO.AsDateTime<>0 then
              RXVencimento.AsDateTime   :=qryTituloDT_VENCIMENTO.AsDateTime;
              RX.Post;

              Consulta.Next;
          end;
          Consulta.RecNo:=Posicao;
          Consulta.EnableControls;
          qryTitulo.Close;

          if RX.IsEmpty then Detalhe.Enabled :=False;

          if dm.CertidoesTIPO_CERTIDAO.AsString='I' then
          lbTexto4.Caption:='I N T E I R O   T E O R';
          PF.Aguarde(False);
          qkCertidao.Prepare;
          dm.CertidoesFOLHAS.AsString:=IntToStr(qkCertidao.PageNumber);
          FQuickCertidao1.Free;
      end;
  end;
end;

procedure TFCertidao.edDocumento2Enter(Sender: TObject);
begin
  dm.CertidoesCPF_CNPJ_REQUERENTE.EditMask:='';
  dm.CertidoesCPF_CNPJ_REQUERENTE.AsString:=GR.PegarNumeroTexto(dm.CertidoesCPF_CNPJ_REQUERENTE.AsString);
end;

procedure TFCertidao.btReciboClick(Sender: TObject);
var
  vResultado: String;
begin
  if dm.CertidoesFOLHAS.AsString='' then
  begin
      GR.Aviso('QTD. DE FOLHAS N�O INFORMADAS!');
      edFolhas.SetFocus;
      Exit;
  end;

  if dm.CertidoesFOLHAS.AsInteger<2 then
  begin
      GR.Aviso('SEM FOLHAS EXCEDENTES!');
      edFolhas.SetFocus;
      Exit;
  end;

  dm.vReciboExcedente:=True;

  FolhasExcedentes;
  vResultado:='';
  if dm.CertidoesRESULTADO.AsString='P' then vResultado:='Resultado: Positivo';
  if dm.CertidoesRESULTADO.AsString='N' then vResultado:='Resultado: Negativo';

  if GR.Pergunta('GERAR NOVO N� DE RECIBO') then
  begin
      if StrToInt(dm.ValorParametro(14))<>0 then
      begin
          dm.CertidoesEXRECIBO.AsInteger:=StrToInt(dm.ValorParametro(14));
          dm.AtualizarParametro(14,IntToStr(dm.CertidoesEXRECIBO.AsInteger+1));
      end
      else
      begin
          dm.CertidoesEXRECIBO.AsInteger:=StrToInt(dm.ValorParametro(11));
          dm.AtualizarParametro(11,IntToStr(dm.CertidoesEXRECIBO.AsInteger+1));
      end;
  end;

  dm.vEmolumentos :=dm.CertidoesEXEMOLUMENTOS.AsFloat;
  dm.vFetj        :=dm.CertidoesEXFETJ.AsFloat;
  dm.vFundperj    :=dm.CertidoesEXFUNDPERJ.AsFloat;
  dm.vFunperj     :=dm.CertidoesEXFUNPERJ.AsFloat;
  dm.vFunarpen    :=dm.CertidoesEXFUNARPEN.AsFloat;
  dm.vIss         :=dm.CertidoesEXISS.AsFloat;
  dm.vTotal       :=dm.CertidoesEXTOTAL.AsFloat;

  if dm.ValorParametro(42)='S' then
    dm.ReciboMatricial(dm.CertidoesID_CERTIDAO.AsInteger,dm.CertidoesTIPO_CERTIDAO.AsString)
      else
      begin
          dm.vAviso:='2. Recibo de Folhas Excedentes';
          Application.CreateForm(TFQuickPedido1,FQuickPedido1);
          FQuickPedido1.Free;
      end;

  dm.vAviso:='';
  dm.vReciboExcedente:=False;
end;

procedure TFCertidao.edFolhasChange(Sender: TObject);
begin
  if edFolhas.Text<>'' then
    if StrToInt(edFolhas.Text)>1 then
      edExcedentes.Value:=StrToInt(edFolhas.Text)-1
        else edExcedentes.Value:=0;
end;

procedure TFCertidao.Calcular;
var
  Q: TFDQuery;
begin
  if dm.Certidoes.State=dsBrowse then Exit;

  dm.RxCustas.Close;

  if edExcedentes.AsInteger>0 then
  begin
      {VERIFICO SE J� FOI SALVO A TABELA DE FOLHAS EXCEDENTES AMARRADA A CERTID�O, SE SIM PEGO O ID_CUSTA}
      Q:=TFDQuery.Create(Nil);
      Q.Connection:=dm.conSISTEMA;
      Q.SQL.Add('SELECT ID_CUSTA FROM CUSTAS WHERE ID_ATO=:ID AND TABELA=:TB AND ITEM=:IT AND SUBITEM=:SB');
      Q.ParamByName('ID').AsInteger:=dm.CertidoesID_CERTIDAO.AsInteger;
      Q.ParamByName('TB').AsString :='16';
      Q.ParamByName('IT').AsString :='2';
      Q.ParamByName('SB').AsString :='*';
      Q.Open;

      {DOU UM APPEND NO RXCUSTAS PARA LIMPAR AS CUSTAS DA CERTID�O E INCLUIR SOMENTE O ITEM DE FOLHA EXCEDENTE}
      dm.RxCustas.Open;
      dm.RxCustas.Append;
      dm.RxCustasID_CUSTA.AsInteger   :=Q.FieldByName('ID_CUSTA').AsInteger;
      dm.RxCustasID_ATO.AsInteger     :=dm.CertidoesID_CERTIDAO.AsInteger;
      dm.RxCustasTABELA.AsString      :='16';
      dm.RxCustasITEM.AsString        :='2';
      dm.RxCustasSUBITEM.AsString     :='*';
      dm.RxCustasVALOR.AsFloat        :=dm.SubitemVALOR.AsFloat;
      dm.RxCustasQTD.AsInteger        :=edExcedentes.AsInteger;
      dm.RxCustasTOTAL.AsFloat        :=dm.RxCustasQTD.AsInteger*dm.RxCustasVALOR.AsFloat;
      dm.RxCustasDESCRICAO.AsString   :=dm.SubitemDESCR.AsString;
      dm.RxCustas.Post;

      Q.Close;
      Q.Free;

      if dm.CertidoesEXCOBRANCA.AsString='' then
      dm.CertidoesEXCOBRANCA.AsString:='CC';

      dm.CertidoesEXEMOLUMENTOS.AsFloat   :=dm.RxCustasTOTAL.AsFloat;
      dm.CertidoesEXFETJ.AsFloat          :=GR.NoRound(GR.NoRound(dm.RxCustasVALOR.AsFloat*0.2,2)*dm.RxCustasQTD.AsInteger,2);
      dm.CertidoesEXFUNDPERJ.AsFloat      :=GR.NoRound(GR.NoRound(dm.RxCustasVALOR.AsFloat*0.05,2)*dm.RxCustasQTD.AsInteger,2);
      dm.CertidoesEXFUNPERJ.AsFloat       :=GR.NoRound(GR.NoRound(dm.RxCustasVALOR.AsFloat*0.05,2)*dm.RxCustasQTD.AsInteger,2);
      dm.CertidoesEXFUNARPEN.AsFloat      :=GR.NoRound(GR.NoRound(dm.RxCustasVALOR.AsFloat*0.04,2)*dm.RxCustasQTD.AsInteger,2);
      dm.CertidoesEXISS.AsFloat           :=GR.NoRound(GR.NoRound(dm.RxCustasVALOR.AsFloat*Gdm.vAliquotaISS,2)*dm.RxCustasQTD.AsInteger,2);
      dm.CertidoesEXTOTAL.AsFloat         :=dm.CertidoesEXEMOLUMENTOS.AsFloat+
                                            dm.CertidoesEXFETJ.AsFloat+
                                            dm.CertidoesEXFUNDPERJ.AsFloat+
                                            dm.CertidoesEXFUNPERJ.AsFloat+
                                            dm.CertidoesEXFUNARPEN.AsFloat+
                                            dm.CertidoesEXISS.AsFloat;
  end
  else
  begin
      dm.CertidoesEXCOBRANCA.Clear;
      dm.CertidoesEXEMOLUMENTOS.AsFloat   :=0;
      dm.CertidoesEXFETJ.AsFloat          :=0;
      dm.CertidoesEXFUNDPERJ.AsFloat      :=0;
      dm.CertidoesEXFUNPERJ.AsFloat       :=0;
      dm.CertidoesEXFUNARPEN.AsFloat      :=0;
      dm.CertidoesEXISS.AsFloat           :=0;
      dm.CertidoesEXTOTAL.AsFloat         :=0;
  end;
end;

procedure TFCertidao.ConsultaCalcFields(DataSet: TDataSet);
begin
  dm.qryFaixa.Close;
  dm.qryFaixa.ParamByName('C').AsString       :=ConsultaCONVENIO.AsString;
  dm.qryFaixa.ParamByName('OCULTO1').AsString :='N';
  dm.qryFaixa.ParamByName('OCULTO2').AsString :='N';
  dm.qryFaixa.ParamByName('VALOR1').AsFloat   :=ConsultaSALDO_TITULO.AsFloat;
  dm.qryFaixa.ParamByName('VALOR2').AsFloat   :=ConsultaSALDO_TITULO.AsFloat;
  dm.qryFaixa.Open;
  ConsultaTotal.AsFloat:=dm.qryFaixaTOT.AsFloat;
end;

procedure TFCertidao.FolhasExcedentes;
begin
  {Calcular;
  if (edExcedentes.AsInteger>0) and (dm.CertidoesCCT.AsString='') then
  begin
      vIdReservado:=GR.ValorGeneratorDBX('ID_RESERVADO',Gdm.BDGerencial);
      dm.CertidoesCCT.AsString:=Gdm.ProximoSelo(dm.SerieAtual,'PROTESTO',Self.Name,'S','',0,0,0,'FOLHAS EXCEDENTES',dm.vNome,Now,vIdReservado);
  end
  else
  begin
      vIdReservado:=0;
      Gdm.LiberarSelo('PROTESTO',dm.CertidoesCCT.AsString,False);
      if edExcedentes.AsInteger=0 then
        if dm.Certidoes.State=dsEdit then
          dm.CertidoesCCT.Clear;
  end;}
end;

procedure TFCertidao.edFolhasExit(Sender: TObject);
begin
  FolhasExcedentes;
end;

procedure TFCertidao.edFolhasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFCertidao.ImLimparCCTClick(Sender: TObject);
begin
  if edCCT.Text='' then Exit;
  if GR.PasswordInputBox('SEGURAN�A','SENHA')=dm.ServentiaCODIGO.AsString then
    if GR.Pergunta('LIMPAR CCT "'+GR.FormatarSelo(edCCT.Text)) then
    begin
        if GR.Pergunta('DESEJA LIBERAR O CCT NO GERENCIAL') then
        GDM.LiberarSelo('PROTESTO',edCCT.Text,False);
        dm.CertidoesCCT.Clear;
        GR.Aviso('INFORMA��ES ATUALIZADAS COM SUCESSO!');
    end;
end;

procedure TFCertidao.CertidaoInteiroTeor;
var
  Praca: String;
begin
  try
    PF.Aguarde(True);
    Application.CreateForm(TFQuickInstrumento1,FQuickInstrumento1);

    with FQuickInstrumento1 do
    begin
        RX.Close;
        RX.Open;

        qrInstrumento.Caption:='CERTID�O DE INTEIRO TEOR';

        dm.Titulos.Close;
        dm.Titulos.Params[0].AsInteger:=dm.CertidoesID_ATO.AsInteger;
        dm.Titulos.Open;

        RX.Append;
        RXTitulo.AsString         :=PF.RetornarTitulo(dm.TitulosTIPO_TITULO.AsInteger);
        RXApresentante.AsString   :=dm.TitulosAPRESENTANTE.AsString;
        RXCredor.AsString         :=dm.TitulosCEDENTE.AsString;
        RXSacador.AsString        :=dm.TitulosSACADOR.AsString;
        RXDevedor.AsString        :=dm.TitulosDEVEDOR.AsString;
        RXValor.AsFloat           :=dm.TitulosVALOR_TITULO.AsFloat;
        RXCustas.AsFloat          :=dm.TitulosTOTAL.AsFloat;
        RXSaldoProtesto.AsFloat   :=dm.TitulosSALDO_PROTESTO.AsFloat;
        RXStatus.AsString         :=dm.TitulosSTATUS.AsString;

        if dm.TitulosSALDO_TITULO.AsFloat<>0 then
          RXSaldoTitulo.AsFloat:=dm.TitulosSALDO_TITULO.AsFloat
            else RXSaldoTitulo.AsFloat:=dm.TitulosVALOR_TITULO.AsFloat;

        RXVencimento.AsDateTime   :=dm.TitulosDT_VENCIMENTO.AsDateTime;
        RXIntimacao.AsDateTime    :=dm.TitulosDT_INTIMACAO.AsDateTime;
        RXProtocolo.AsInteger     :=dm.TitulosPROTOCOLO.AsInteger;
        RXDt_Protocolo.AsDateTime :=dm.TitulosDT_PROTOCOLO.AsDateTime;
        RXNumero.AsString         :=dm.TitulosNUMERO_TITULO.AsString;
        RXLivro.AsString          :=dm.TitulosLIVRO_REGISTRO.AsString;
        RXFolha.AsString          :=dm.TitulosFOLHA_REGISTRO.AsString;
        RXDt_Registro.AsDateTime  :=dm.TitulosDT_REGISTRO.AsDateTime;
        RXNossoNumero.AsString    :=dm.TitulosNOSSO_NUMERO.AsString;
        RXTipo.AsString           :=dm.TitulosTIPO_DEVEDOR.AsString;
        RXMotivo.AsString         :=dm.TitulosMOTIVO_INTIMACAO.AsString;
        RXCedente.AsString        :=dm.TitulosCEDENTE.AsString;
        RXDt_Titulo.AsDateTime    :=dm.TitulosDT_TITULO.AsDateTime;
        RXEmolumentos.AsFloat     :=dm.CertidoesEMOLUMENTOS.AsFloat;
        RXFetj.AsFloat            :=dm.CertidoesFETJ.AsFloat;
        RXFundperj.AsFloat        :=dm.CertidoesFUNDPERJ.AsFloat;
        RXFunperj.AsFloat         :=dm.CertidoesFUNPERJ.AsFloat;
        RXFunarpen.AsFloat        :=dm.CertidoesFUNARPEN.AsFloat;
        RXPmcmv.AsFloat           :=dm.CertidoesPMCMV.AsFloat;
        RXIss.AsFloat             :=dm.CertidoesISS.AsFloat;
        RXMutua.AsFloat           :=dm.CertidoesMUTUA.AsFloat;
        RXAcoterj.AsFloat         :=dm.CertidoesACOTERJ.AsFloat;
        RXDistribuicao.AsFloat    :=0;
        RXTarifa.AsFloat          :=0;
        RXAR.AsFloat              :=0;
        RXTotal.AsFloat           :=dm.CertidoesTOTAL.AsFloat;
        RXSelo.AsString           :=dm.CertidoesSELO.AsString;
        RXAleatorio.AsString      :=dm.CertidoesALEATORIO.AsString;
        RXTipoProtesto.AsInteger  :=dm.TitulosTIPO_PROTESTO.AsInteger;
        RXObservacao.AsString     :=dm.TitulosOBSERVACAO.AsString;
        RXTermo.AsInteger         :=dm.TitulosREGISTRO.AsInteger;
        RXDt_Publicacao.AsDateTime:=dm.TitulosDT_PUBLICACAO.AsDateTime;
        RXConvenio.AsString       :=dm.TitulosCONVENIO.AsString;
        RXMensagem.AsString       :=PF.RetornarMensagem(dm.TitulosID_MSG.AsInteger);
        RXFins.AsString           :=GR.iif(dm.TitulosFINS_FALIMENTARES.AsString='S','SIM','N�O');
        RXSeloProtesto.AsString   :=GR.SeloFormatado(dm.TitulosSELO_REGISTRO.AsString,dm.TitulosALEATORIO_PROTESTO.AsString);

        if dm.TitulosSTATUS.AsString='CANCELADO' then
        begin
            RXDt_Cancelamento.AsDateTime:=dm.TitulosDT_PAGAMENTO.AsDateTime;
            RXSeloCancelamento.AsString:=GR.SeloFormatado(dm.TitulosSELO_PAGAMENTO.AsString,dm.TitulosALEATORIO_SOLUCAO.AsString);
        end;

        if dm.TitulosPRACA_PROTESTO.AsString='' then
        begin
            InputQuery('Atualiza��o de dados','Pra�a de Pagamento',Praca);
            dm.Titulos.Edit;
            dm.TitulosPRACA_PROTESTO.AsString:=Praca;
            dm.Titulos.Post;
            dm.Titulos.ApplyUpdates(0);
        end;
        RXPraca.AsString:=dm.TitulosPRACA_PROTESTO.AsString;

        if RXTipo.AsString='F' then
          RXDocumento.AsString:=PF.FormatarCPF(dm.TitulosCPF_CNPJ_DEVEDOR.AsString)
            else RXDocumento.AsString:=PF.FormatarCNPJ(dm.TitulosCPF_CNPJ_DEVEDOR.AsString);

        if dm.TitulosTIPO_INTIMACAO.AsString='P' then RXIntimado.AsString:='PESSOAL';
        if dm.TitulosTIPO_INTIMACAO.AsString='E' then RXIntimado.AsString:='EDITAL';
        if dm.TitulosTIPO_INTIMACAO.AsString='C' then RXIntimado.AsString:='CARTA';

        if dm.TitulosTIPO_ENDOSSO.AsString='M' then RXEndosso.AsString:='MANDATO';
        if dm.TitulosTIPO_ENDOSSO.AsString='T' then RXEndosso.AsString:='TRANSLATIVO';
        if dm.TitulosTIPO_ENDOSSO.AsString='S' then RXEndosso.AsString:='SEM ENDOSSO, PARA CONTRATOS NO PR�PRIO BANCO';
        if dm.TitulosTIPO_ENDOSSO.AsString=''  then RXEndosso.AsString:='SEM ENDOSSO, PARA CONTRATOS NO PR�PRIO BANCO';

        PF.CarregarDevedor(dm.TitulosID_ATO.AsInteger);

        RXEndereco.AsString:=dm.RxDevedorENDERECO.AsString+' '+dm.RxDevedorBAIRRO.AsString+' '+
                             dm.RxDevedorMUNICIPIO.AsString+' '+dm.RxDevedorUF.AsString;

        if dm.RxDevedorCEP.AsString<>'' then
        RXEndereco.AsString:=RXEndereco.AsString+' - CEP: '+PF.FormatarCEP(dm.RxDevedorCEP.AsString);

        PF.CarregarSacador(dm.TitulosID_ATO.AsInteger);

        RXEnderecoSa.AsString:=dm.RxSacadorENDERECO.AsString+' '+dm.RxSacadorBAIRRO.AsString+' '+
                               dm.RxSacadorMUNICIPIO.AsString+' '+dm.RxSacadorUF.AsString;

        if dm.TitulosCODIGO_APRESENTANTE.AsString<>'' then
          PF.CarregarPortador('C',dm.TitulosCODIGO_APRESENTANTE.AsString,dm.TitulosAPRESENTANTE.AsString,
                                  dm.TitulosTIPO_APRESENTANTE.AsString,dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosCODIGO_APRESENTANTE.AsString)
            else
              if dm.TitulosCPF_CNPJ_APRESENTANTE.AsString<>'' then
                PF.CarregarPortador('D',dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosAPRESENTANTE.AsString,
                                        dm.TitulosTIPO_APRESENTANTE.AsString,dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosCODIGO_APRESENTANTE.AsString)
                else
                  PF.CarregarPortador('N',dm.TitulosAPRESENTANTE.AsString,dm.TitulosAPRESENTANTE.AsString,
                                          dm.TitulosTIPO_APRESENTANTE.AsString,dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosCODIGO_APRESENTANTE.AsString);

        RXEnderecoAp.AsString     :=dm.RxPortadorENDERECO.AsString;
        RXDt_Intimacao.AsDateTime :=dm.TitulosDT_INTIMACAO.AsDateTime;
        RXDt_Certidao.AsDateTime  :=dm.CertidoesDT_CERTIDAO.AsDateTime;
        RX.Post;

        qrFuncionario.Caption:=dm.vAssinatura;
        qrQualificacao.Caption:=dm.vMatricula;

        if dm.ServentiaCODIGO.AsInteger=1554 then
        begin
            qrFuncionario.Caption:='Tabeli�o de Protesto';
            qrQualificacao.Caption:='';
        end;

        qkInstrumento.Preview;
        Free;
    end;
  except
    on E:Exception do
    begin
        PF.Aguarde(False);
        GR.Aviso('Erro: '+E.Message);
    end;
  end;
end;

procedure TFCertidao.cbTipoCobrancaChange(Sender: TObject);
begin
  if (cbTipoCobranca.ItemIndex=0) or (cbTipoCobranca.ItemIndex=2) then
  begin
      dm.CertidoesEXEMOLUMENTOS.AsFloat :=0;
      dm.CertidoesEXFETJ.AsFloat        :=0;
      dm.CertidoesEXFUNDPERJ.AsFloat    :=0;
      dm.CertidoesEXFUNPERJ.AsFloat     :=0;
      dm.CertidoesEXFUNARPEN.AsFloat    :=0;
      dm.CertidoesEXISS.AsFloat         :=0;
      dm.CertidoesEXTOTAL.AsFloat       :=0;
  end;

  if (cbTipoCobranca.ItemIndex=1) then
  FolhasExcedentes;

  if (cbTipoCobranca.ItemIndex=3) then
  begin
      dm.CertidoesEXEMOLUMENTOS.AsFloat :=0;
      dm.CertidoesEXTOTAL.AsFloat       :=dm.CertidoesEXEMOLUMENTOS.AsFloat+
                                          dm.CertidoesEXFETJ.AsFloat+
                                          dm.CertidoesEXFUNDPERJ.AsFloat+
                                          dm.CertidoesEXFUNPERJ.AsFloat+
                                          dm.CertidoesEXFUNARPEN.AsFloat+
                                          dm.CertidoesEXISS.AsFloat;
  end;
end;

end.
