unit URelDistribuidos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, Buttons, sBitBtn, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, Vcl.DBCtrls, sDBLookupComboBox, sLabel,
  sRadioButton, sGroupBox, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.DateUtils;

type
  TFRelDistribuidos = class(TForm)
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    P1: TsPanel;
    gbServentiaA: TsGroupBox;
    gbDataPeriodo: TsGroupBox;
    dtePeriodoIni: TsDateEdit;
    dtePeriodoFim: TsDateEdit;
    rbData: TsRadioButton;
    rbPeriodo: TsRadioButton;
    dteData: TsDateEdit;
    sLabel1: TsLabel;
    lcbServentiaA: TsDBLookupComboBox;
    rbTodas: TsRadioButton;
    rbEspecifica: TsRadioButton;
    qryServAg: TFDQuery;
    dsServAg: TDataSource;
    procedure btSairClick(Sender: TObject);
    procedure btVisualizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure rbEspecificaClick(Sender: TObject);
    procedure rbEspecificaExit(Sender: TObject);
    procedure rbTodasClick(Sender: TObject);
    procedure rbTodasExit(Sender: TObject);
    procedure rbDataClick(Sender: TObject);
    procedure rbDataExit(Sender: TObject);
    procedure rbPeriodoClick(Sender: TObject);
    procedure rbPeriodoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelDistribuidos: TFRelDistribuidos;

implementation

uses UQuickDistribuidos, UDM;

{$R *.dfm}

procedure TFRelDistribuidos.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFRelDistribuidos.btVisualizarClick(Sender: TObject);
var
  lContinua: Boolean;
begin
  lContinua := True;

  if rbData.Checked and
    (dteData.Date = 0) then
  begin
    Application.MessageBox('Por favor, preencha a Data corretamente.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);

    if dteData.CanFocus then
      dteData.SetFocus;

    lContinua := False;
  end
  else if rbPeriodo.Checked then
  begin
    if dtePeriodoIni.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data de In�cio corretamente.',
                             'Aviso',
                             MB_OK + MB_ICONWARNING);

      if dtePeriodoIni.CanFocus then
        dtePeriodoIni.SetFocus;

      lContinua := False;
    end
    else if dtePeriodoFim.Date = 0 then
    begin
      Application.MessageBox('Por favor, preencha a Data de Fim corretamente.',
                             'Aviso',
                             MB_OK + MB_ICONWARNING);

      if dtePeriodoFim.CanFocus then
        dtePeriodoFim.SetFocus;

      lContinua := False;
    end;
  end
  else if rbEspecifica.Checked and
    (Trim(lcbServentiaA.Text) = '') then
  begin
    Application.MessageBox('Por favor, indique a Serventia Agregada.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);

    if lcbServentiaA.CanFocus then
      lcbServentiaA.SetFocus;

    lContinua := False;
  end;

  if not lContinua then
    Exit;

  Application.CreateForm(TFQuickDistribuidos,FQuickDistribuidos);

  with FQuickDistribuidos do
  begin
    Titulos.Close;

    if rbData.Checked then
    begin
      lbPeriodo.Caption := 'Data: ' + dteData.Text;

      Titulos.Params.ParamByName('DATA1').Value := dteData.Date;
      Titulos.Params.ParamByName('DATA2').Value := dteData.Date;
    end
    else
    begin
      lbPeriodo.Caption := 'Per�odo: ' + dtePeriodoIni.Text + ' - ' + dtePeriodoFim.Text;

      Titulos.Params.ParamByName('DATA1').Value := dtePeriodoIni.Date;
      Titulos.Params.ParamByName('DATA2').Value := dtePeriodoFim.Date;
    end;

    if rbTodas.Checked then
    begin
      Titulos.Params.ParamByName('CART1').Value := 0;
      Titulos.Params.ParamByName('CART2').Value := 0;
    end
    else
    begin
      Titulos.Params.ParamByName('CART1').Value := qryServAg.FieldByName('CODIGO_DISTRIBUICAO').AsInteger;
      Titulos.Params.ParamByName('CART2').Value := qryServAg.FieldByName('CODIGO_DISTRIBUICAO').AsInteger;
    end;

    Titulos.Open;

    if Titulos.RecordCount > 0 then
      Relatorio.Preview
    else
      Application.MessageBox('N�o foram encontrados T�tulos para a pesquisa realizada.',
                             'Aviso',
                             MB_OK + MB_ICONWARNING);

    Free;
  end;
end;

procedure TFRelDistribuidos.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_ESCAPE then
    Close;
end;

procedure TFRelDistribuidos.FormShow(Sender: TObject);
begin
  qryServAg.Close;
  qryServAg.Open;

  rbData.Checked        := True;
  dteData.Enabled       := True;
  dteData.Date          := Date;

  rbPeriodo.Checked     := False;
  dtePeriodoIni.Clear;
  dtePeriodoFim.Clear;
  dtePeriodoIni.Enabled := False;
  dtePeriodoFim.Enabled := False;

  rbTodas.Checked        := True;
  rbEspecifica.Checked   := False;
  lcbServentiaA.Enabled  := False;
end;

procedure TFRelDistribuidos.rbDataClick(Sender: TObject);
begin
  if rbData.Checked then
  begin
    dteData.Enabled := True;
    dteData.Date    := Date;

    dtePeriodoIni.Clear;
    dtePeriodoFim.Clear;
    dtePeriodoIni.Enabled := False;
    dtePeriodoFim.Enabled := False;
  end
  else
  begin
    dteData.Clear;
    dteData.Enabled := False;

    dtePeriodoIni.Enabled := True;
    dtePeriodoFim.Enabled := True;
    dtePeriodoIni.Date    := StartOfTheMonth(Date);
    dtePeriodoFim.Date    := Date;
  end;

  rbPeriodo.Checked := not rbData.Checked;
end;

procedure TFRelDistribuidos.rbDataExit(Sender: TObject);
begin
  if rbData.Checked then
  begin
    dteData.Enabled := True;
    dteData.Date    := Date;

    dtePeriodoIni.Clear;
    dtePeriodoFim.Clear;
    dtePeriodoIni.Enabled := False;
    dtePeriodoFim.Enabled := False;
  end
  else
  begin
    dteData.Clear;
    dteData.Enabled := False;

    dtePeriodoIni.Enabled := True;
    dtePeriodoFim.Enabled := True;
    dtePeriodoIni.Date    := StartOfTheMonth(Date);
    dtePeriodoFim.Date    := Date;
  end;

  rbPeriodo.Checked := not rbData.Checked;
end;

procedure TFRelDistribuidos.rbEspecificaClick(Sender: TObject);
begin
  if rbEspecifica.Checked then
    lcbServentiaA.Enabled := True
  else
  begin
    lcbServentiaA.KeyValue := -1;
    lcbServentiaA.Enabled  := False;
  end;

  rbTodas.Checked := not rbEspecifica.Checked;
end;

procedure TFRelDistribuidos.rbEspecificaExit(Sender: TObject);
begin
  if rbEspecifica.Checked then
    lcbServentiaA.Enabled := True
  else
  begin
    lcbServentiaA.KeyValue := -1;
    lcbServentiaA.Enabled  := False;
  end;

  rbTodas.Checked := not rbEspecifica.Checked;
end;

procedure TFRelDistribuidos.rbPeriodoClick(Sender: TObject);
begin
  if rbPeriodo.Checked then
  begin
    dtePeriodoIni.Enabled := True;
    dtePeriodoFim.Enabled := True;
    dtePeriodoIni.Date    := StartOfTheMonth(Date);
    dtePeriodoFim.Date    := Date;

    dteData.Clear;
    dteData.Enabled := False;
  end
  else
  begin
    dtePeriodoIni.Clear;
    dtePeriodoFim.Clear;
    dtePeriodoIni.Enabled := False;
    dtePeriodoFim.Enabled := False;

    dteData.Enabled := True;
    dteData.Date    := Date;
  end;

  rbData.Checked := not rbPeriodo.Checked;
end;

procedure TFRelDistribuidos.rbPeriodoExit(Sender: TObject);
begin
  if rbPeriodo.Checked then
  begin
    dtePeriodoIni.Enabled := True;
    dtePeriodoFim.Enabled := True;
    dtePeriodoIni.Date    := StartOfTheMonth(Date);
    dtePeriodoFim.Date    := Date;

    dteData.Clear;
    dteData.Enabled := False;
  end
  else
  begin
    dtePeriodoIni.Clear;
    dtePeriodoFim.Clear;
    dtePeriodoIni.Enabled := False;
    dtePeriodoFim.Enabled := False;

    dteData.Enabled := True;
    dteData.Date    := Date;
  end;

  rbData.Checked := not rbPeriodo.Checked;
end;

procedure TFRelDistribuidos.rbTodasClick(Sender: TObject);
begin
  if rbTodas.Checked then
  begin
    lcbServentiaA.KeyValue := -1;
    lcbServentiaA.Enabled  := False;
  end
  else
    lcbServentiaA.Enabled := True;

  rbEspecifica.Checked := not rbTodas.Checked;
end;

procedure TFRelDistribuidos.rbTodasExit(Sender: TObject);
begin
  if rbTodas.Checked then
  begin
    lcbServentiaA.KeyValue := -1;
    lcbServentiaA.Enabled  := False;
  end
  else
    lcbServentiaA.Enabled := True;

  rbEspecifica.Checked := not rbTodas.Checked;
end;

end.


