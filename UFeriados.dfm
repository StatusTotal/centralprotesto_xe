object FFeriados: TFFeriados
  Left = 290
  Top = 193
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Feriados'
  ClientHeight = 248
  ClientWidth = 717
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 14
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 717
    Height = 41
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object btIncluir: TsBitBtn
      Left = 7
      Top = 8
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Hint = 'INCLUIR NOVO T'#205'TULO'
      Caption = 'Incluir'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btIncluirClick
      ImageIndex = 6
      Images = dm.Imagens
      Reflected = True
      SkinData.SkinSection = 'BUTTON'
    end
    object btAlterar: TsBitBtn
      Left = 86
      Top = 8
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Hint = 'ALTERAR INFORMA'#199#213'ES DO T'#205'TULO'
      Caption = 'Alterar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btAlterarClick
      ImageIndex = 8
      Images = dm.Imagens
      Reflected = True
      SkinData.SkinSection = 'BUTTON'
    end
    object btExcluir: TsBitBtn
      Left = 165
      Top = 8
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Hint = 'EXCLUIR T'#205'TULO'
      Caption = 'Excluir'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btExcluirClick
      ImageIndex = 9
      Images = dm.Imagens
      Reflected = True
      SkinData.SkinSection = 'BUTTON'
    end
    object btSalvar: TsBitBtn
      Left = 546
      Top = 8
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Caption = 'Salvar'
      TabOrder = 3
      OnClick = btSalvarClick
      ImageIndex = 3
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object btCancelar: TsBitBtn
      Left = 625
      Top = 8
      Width = 83
      Height = 25
      Cursor = crHandPoint
      Caption = 'Cancelar'
      TabOrder = 4
      OnClick = btCancelarClick
      ImageIndex = 5
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object P2: TsPanel
    Left = 0
    Top = 41
    Width = 717
    Height = 47
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'SCROLLBAR2V'
    object edDescricao: TsDBEdit
      Left = 370
      Top = 13
      Width = 253
      Height = 22
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'DESCRICAO'
      DataSource = dsFeriados
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Descri'#231#227'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
    end
    object cbMes: TsComboBox
      Left = 214
      Top = 13
      Width = 93
      Height = 22
      Alignment = taLeftJustify
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'M'#234's'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = -1
      ParentFont = False
      TabOrder = 2
      OnChange = cbMesChange
      Items.Strings = (
        'JANEIRO'
        'FEVEREIRO'
        'MAR'#199'O'
        'ABRIL'
        'MAIO'
        'JUNHO'
        'JULHO'
        'AGOSTO'
        'SETEMBRO'
        'OUTUBRO'
        'NOVEMBRO'
        'DEZEMBRO')
    end
    object cbDia: TsDBComboBox
      Left = 127
      Top = 13
      Width = 53
      Height = 22
      Style = csDropDownList
      Color = clWhite
      DataField = 'DIA'
      DataSource = dsFeriados
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12'
        '13'
        '14'
        '15'
        '16'
        '17'
        '18'
        '19'
        '20'
        '21'
        '22'
        '23'
        '24'
        '25'
        '26'
        '27'
        '28'
        '29'
        '30'
        '31')
      ParentFont = False
      TabOrder = 1
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Dia'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'COMBOBOX'
    end
    object cbAno: TsDBComboBox
      Left = 34
      Top = 13
      Width = 65
      Height = 22
      Style = csDropDownList
      Color = clWhite
      DataField = 'ANO'
      DataSource = dsFeriados
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        '1940'
        '1941'
        '1942'
        '1943'
        '1944'
        '1945'
        '1946'
        '1947'
        '1948'
        '1949'
        '1950'
        '1951'
        '1952'
        '1953'
        '1954'
        '1955'
        '1956'
        '1957'
        '1958'
        '1959'
        '1960'
        '1961'
        '1962'
        '1963'
        '1964'
        '1965'
        '1966'
        '1967'
        '1968'
        '1969'
        '1970'
        '1971'
        '1972'
        '1973'
        '1974'
        '1975'
        '1976'
        '1977'
        '1978'
        '1979'
        '1980'
        '1981'
        '1982'
        '1983'
        '1984'
        '1985'
        '1986'
        '1987'
        '1988'
        '1989'
        '1990'
        '1991'
        '1992'
        '1993'
        '1994'
        '1995'
        '1996'
        '1997'
        '1998'
        '1999'
        '2000'
        '2001'
        '2002'
        '2003'
        '2004'
        '2005'
        '2006'
        '2007'
        '2008'
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030'
        '2031'
        '2032'
        '2033'
        '2034'
        '2035'
        '2036'
        '2037'
        '2038'
        '2039'
        '2040')
      ParentFont = False
      TabOrder = 0
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Ano'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'COMBOBOX'
    end
    object cbFixo: TsDBComboBox
      Left = 655
      Top = 13
      Width = 53
      Height = 22
      Style = csDropDownList
      Color = clWhite
      DataField = 'FLG_FIXO'
      DataSource = dsFeriados
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'S'
        'N')
      ParentFont = False
      TabOrder = 4
      OnExit = cbFixoExit
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Fixo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'COMBOBOX'
    end
  end
  object Grid: TwwDBGrid
    Left = 0
    Top = 88
    Width = 717
    Height = 160
    Selected.Strings = (
      'ANO'#9'9'#9'Ano'#9'F'
      'DIA'#9'8'#9'Dia'#9'F'
      'DESCR_MES'#9'19'#9'M'#234's'#9'F'
      'DESCRICAO'#9'53'#9'Descri'#231#227'o'#9'F'
      'FLG_FIXO'#9'5'#9'Fixo'#9'F')
    IniAttributes.Delimiter = ';;'
    IniAttributes.UnicodeIniFile = False
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    BorderStyle = bsNone
    DataSource = dsFeriados
    KeyOptions = []
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
    ReadOnly = True
    TabOrder = 2
    TitleAlignment = taLeftJustify
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = False
    UseTFields = False
  end
  object dsFeriados: TDataSource
    DataSet = dm.Feriados
    OnStateChange = dsFeriadosStateChange
    OnDataChange = dsFeriadosDataChange
    Left = 32
    Top = 168
  end
end
