unit UOnline;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, sMemo, Grids, Wwdbigrd, Wwdbgrid,
  Mask, sMaskEdit, sCustomComboEdit, sTooledit, sGroupBox, Buttons, sBitBtn,
  sLabel, DateUtils, DB, DBClient, SimpleDS, RxMemDS, sDBText, acPNG,
  ComCtrls, sStatusBar, Menus, sGauge, ShellAPI, sSpeedButton, SQLExpr, Math,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFOnline = class(TForm)
    P1: TsPanel;
    btFiltrar: TsBitBtn;
    btGerar: TsBitBtn;
    RgTipo: TsRadioGroup;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    sPanel1: TsPanel;
    M: TsMemo;
    Grid2: TwwDBGrid;
    sLabel1: TsLabel;
    sPanel2: TsPanel;
    RX: TRxMemoryData;
    RXProtocolo: TStringField;
    RXDataProtocolo: TStringField;
    RXDataProtesto: TStringField;
    RXNomeApresentante: TStringField;
    RXNomeDevedor: TStringField;
    RXTipoDevedor: TStringField;
    RXDocumentoDevendor: TStringField;
    RXDataVencimento: TStringField;
    RXCodigoTitulo: TStringField;
    RXEspecieTitulo: TStringField;
    RXAcao: TStringField;
    RXDataRegistro: TStringField;
    dsRX: TDataSource;
    RXStatus: TStringField;
    P2: TsPanel;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sLabel5: TsLabel;
    sLabel6: TsLabel;
    txDevedor: TsDBText;
    txTipo: TsDBText;
    txDocumento: TsDBText;
    txEspecie: TsDBText;
    txVencimento: TsDBText;
    sLabel7: TsLabel;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    dsOnline: TDataSource;
    sStatusBar1: TsStatusBar;
    PM: TPopupMenu;
    Marcartodos1: TMenuItem;
    Desmarcartodos1: TMenuItem;
    sPanel3: TsPanel;
    G: TsGauge;
    Grid1: TwwDBGrid;
    P3: TsPanel;
    PM2: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    Nomedo1: TMenuItem;
    RXDtProtesto: TDateField;
    RXDtProtocolo: TDateField;
    N1: TMenuItem;
    Mostrartodos1: TMenuItem;
    RXId: TIntegerField;
    sLabel8: TsLabel;
    txStatus: TsDBText;
    DocumentodoDevedornoinformadp1: TMenuItem;
    RXP: TRxMemoryData;
    RXPProtocolo: TIntegerField;
    RXDtTitulo: TDateField;
    RXFormatacao: TBooleanField;
    RXCheck: TBooleanField;
    qrySustados: TFDQuery;
    qryProtestados: TFDQuery;
    qryCancelados: TFDQuery;
    qryAlteracao: TFDQuery;
    qrySustadosID_ATO: TIntegerField;
    qrySustadosPROTOCOLO: TIntegerField;
    qrySustadosDT_PROTOCOLO: TDateField;
    qrySustadosDT_SUSTADO: TDateField;
    qrySustadosLIVRO_REGISTRO: TIntegerField;
    qrySustadosFOLHA_REGISTRO: TStringField;
    qrySustadosDT_REGISTRO: TDateField;
    qrySustadosAPRESENTANTE: TStringField;
    qrySustadosCEDENTE: TStringField;
    qrySustadosDEVEDOR: TStringField;
    qrySustadosTIPO_DEVEDOR: TStringField;
    qrySustadosCPF_CNPJ_DEVEDOR: TStringField;
    qrySustadosDT_VENCIMENTO: TDateField;
    qrySustadosTIPO_TITULO: TIntegerField;
    qrySustadosDT_TITULO: TDateField;
    qryProtestadosID_ATO: TIntegerField;
    qryProtestadosPROTOCOLO: TIntegerField;
    qryProtestadosDT_PROTOCOLO: TDateField;
    qryProtestadosLIVRO_REGISTRO: TIntegerField;
    qryProtestadosFOLHA_REGISTRO: TStringField;
    qryProtestadosDT_REGISTRO: TDateField;
    qryProtestadosAPRESENTANTE: TStringField;
    qryProtestadosCEDENTE: TStringField;
    qryProtestadosDEVEDOR: TStringField;
    qryProtestadosTIPO_DEVEDOR: TStringField;
    qryProtestadosCPF_CNPJ_DEVEDOR: TStringField;
    qryProtestadosDT_VENCIMENTO: TDateField;
    qryProtestadosTIPO_TITULO: TIntegerField;
    qryProtestadosSTATUS: TStringField;
    qryProtestadosPROTESTADO: TStringField;
    qryProtestadosDT_TITULO: TDateField;
    qryCanceladosID_ATO: TIntegerField;
    qryCanceladosPROTOCOLO: TIntegerField;
    qryCanceladosDT_PROTOCOLO: TDateField;
    qryCanceladosDT_PAGAMENTO: TDateField;
    qryCanceladosLIVRO_REGISTRO: TIntegerField;
    qryCanceladosFOLHA_REGISTRO: TStringField;
    qryCanceladosDT_REGISTRO: TDateField;
    qryCanceladosAPRESENTANTE: TStringField;
    qryCanceladosCEDENTE: TStringField;
    qryCanceladosDEVEDOR: TStringField;
    qryCanceladosTIPO_DEVEDOR: TStringField;
    qryCanceladosCPF_CNPJ_DEVEDOR: TStringField;
    qryCanceladosDT_VENCIMENTO: TDateField;
    qryCanceladosTIPO_TITULO: TIntegerField;
    qryCanceladosDT_TITULO: TDateField;
    qryAlteracaoID_ATO: TIntegerField;
    qryAlteracaoPROTOCOLO: TIntegerField;
    qryAlteracaoDT_PROTOCOLO: TDateField;
    qryAlteracaoLIVRO_REGISTRO: TIntegerField;
    qryAlteracaoFOLHA_REGISTRO: TStringField;
    qryAlteracaoDT_REGISTRO: TDateField;
    qryAlteracaoAPRESENTANTE: TStringField;
    qryAlteracaoDEVEDOR: TStringField;
    qryAlteracaoTIPO_DEVEDOR: TStringField;
    qryAlteracaoCPF_CNPJ_DEVEDOR: TStringField;
    qryAlteracaoDT_VENCIMENTO: TDateField;
    qryAlteracaoTIPO_TITULO: TIntegerField;
    procedure RgTipoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btFiltrarClick(Sender: TObject);
    procedure edInicioChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btGerarClick(Sender: TObject);
    procedure Grid1MouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure Grid1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Marcar(Tipo: String);
    procedure Marcartodos1Click(Sender: TObject);
    procedure Desmarcartodos1Click(Sender: TObject);
    procedure RXFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure Nomedo1Click(Sender: TObject);
    procedure Mostrartodos1Click(Sender: TObject);
    procedure Grid2DblClick(Sender: TObject);
    procedure Grid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Image1Click(Sender: TObject);
    procedure DocumentodoDevedornoinformadp1Click(Sender: TObject);
    procedure Grid2MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Grid2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    vCondicao: String;
  end;

var
  FOnline: TFOnline;

implementation

uses UDM, UPF, UTitulo, UGeral, StrUtils;

{$R *.dfm}

procedure TFOnline.RgTipoClick(Sender: TObject);
begin
  if RgTipo.ItemIndex=0 then
  begin
      edFim.Date    :=Now;
      edInicio.Date :=IncYear(Now,-5);
      edFim.Enabled :=True;
  end
  else
  begin
      edInicio.Date:=Now;
      edFim.Enabled:=False;
  end;
end;

procedure TFOnline.FormCreate(Sender: TObject);
begin
  qryProtestados.Connection:=dm.conSISTEMA;
  dm.Online.Close;
  dm.Online.Open;
end;

procedure TFOnline.btFiltrarClick(Sender: TObject);
begin
  Screen.Cursor:=crHourGlass;
  RX.Close;
  RX.Open;
  RX.DisableControls;
  PF.Aguarde(True);
  qryProtestados.Close;
  qryProtestados.Params[0].AsDate:=edInicio.Date;
  qryProtestados.Params[1].AsDate:=edFim.Date;
  qryProtestados.Open;

  qryProtestados.Filtered:=False;
  if RgTipo.ItemIndex=0 then
  begin
      qryProtestados.Filtered:=False;
      //Protestados.Filter:='PROTESTADO='+QuotedStr('S');
      qryProtestados.Filter:='STATUS='+QuotedStr('PROTESTADO');
      qryProtestados.Filtered:=True;
  end;

  qryProtestados.First;
  while not qryProtestados.Eof do
  begin
      RX.Append;
      RXCheck.AsBoolean           :=True;
      RXId.AsInteger              :=qryProtestadosID_ATO.AsInteger;
      RXProtocolo.AsString        :=qryProtestadosPROTOCOLO.AsString;
      RXDataProtocolo.AsString    :=FormatDateTime('DDMMYYYY',qryProtestadosDT_PROTOCOLO.AsDateTime);
      RXDtProtocolo.AsDateTime    :=qryProtestadosDT_PROTOCOLO.AsDateTime;
      RXDataProtesto.AsString     :=FormatDateTime('DDMMYYYY',qryProtestadosDT_REGISTRO.AsDateTime);
      RXDtProtesto.AsDateTime     :=qryProtestadosDT_REGISTRO.AsDateTime;
      RXDtTitulo.AsDateTime       :=qryProtestadosDT_TITULO.AsDateTime;
      RXNomeApresentante.AsString :=PF.RemoverAcento(qryProtestadosAPRESENTANTE.AsString);
      if RXNomeApresentante.AsString='' then
      RXNomeApresentante.AsString :=PF.RemoverAcento(qryProtestadosCEDENTE.AsString);
      RXNomeDevedor.AsString      :=PF.RemoverAcento(qryProtestadosDEVEDOR.AsString);
      if qryProtestadosTIPO_DEVEDOR.AsString='J' then
        RXTipoDevedor.AsString:='2'
          else RXTipoDevedor.AsString:='1';

      RXDocumentoDevendor.AsString:=qryProtestadosCPF_CNPJ_DEVEDOR.AsString;
      if qryProtestadosDT_VENCIMENTO.AsDateTime=0 then
        RXDataVencimento.AsString:='A vista'
          else RXDataVencimento.AsString:=FormatDateTime('DDMMYYYY',qryProtestadosDT_VENCIMENTO.AsDateTime);
      RXCodigoTitulo.AsString     :=PF.RetornarSigla(qryProtestadosTIPO_TITULO.AsInteger);
      RXEspecieTitulo.AsString    :=PF.RemoverAcento(PF.RetornarTitulo(qryProtestadosTIPO_TITULO.AsInteger));
      if Trim(RXEspecieTitulo.AsString)='' then
      begin
          RXEspecieTitulo.AsString:='Outros';
          RXCodigoTitulo.AsString :='DS';
      end;
      RXAcao.AsString             :='1';
      RXDataRegistro.AsString     :=FormatDateTime('DDMMYYYY',qryProtestadosDT_REGISTRO.AsDateTime);
      RXStatus.AsString           :='PROTESTADO';
      RX.Post;

      qryProtestados.Next;
  end;

  if RgTipo.ItemIndex=1 then
  begin
      qryCancelados.Close;
      qryCancelados.Params[0].AsDate:=edInicio.Date;
      qryCancelados.Params[1].AsDate:=edFim.Date;
      qryCancelados.Open;
      qryCancelados.First;
      while not qryCancelados.Eof do
      begin
          RX.Append;
          RXId.AsInteger              :=qryCanceladosID_ATO.AsInteger;
          RXProtocolo.AsString        :=qryCanceladosPROTOCOLO.AsString;
          RXDataProtocolo.AsString    :=FormatDateTime('DDMMYYYY',qryCanceladosDT_PROTOCOLO.AsDateTime);
          RXDtProtocolo.AsDateTime    :=qryCanceladosDT_PROTOCOLO.AsDateTime;
          RXDataProtesto.AsString     :=FormatDateTime('DDMMYYYY',qryCanceladosDT_REGISTRO.AsDateTime);
          RXDtProtesto.AsDateTime     :=qryCanceladosDT_REGISTRO.AsDateTime;
          RXDtTitulo.AsDateTime       :=qryCanceladosDT_TITULO.AsDateTime;
          RXNomeApresentante.AsString :=PF.RemoverAcento(qryCanceladosAPRESENTANTE.AsString);
          if RXNomeApresentante.AsString='' then
          RXNomeApresentante.AsString :=PF.RemoverAcento(qryCanceladosCEDENTE.AsString);
          RXNomeDevedor.AsString      :=PF.RemoverAcento(qryCanceladosDEVEDOR.AsString);
          if qryCanceladosTIPO_DEVEDOR.AsString='J' then
            RXTipoDevedor.AsString:='2'
              else RXTipoDevedor.AsString:='1';
          RXDocumentoDevendor.AsString:=qryCanceladosCPF_CNPJ_DEVEDOR.AsString;
          if qryCanceladosDT_VENCIMENTO.AsDateTime=0 then
            RXDataVencimento.AsString:='A vista'
              else RXDataVencimento.AsString:=FormatDateTime('DDMMYYYY',qryCanceladosDT_VENCIMENTO.AsDateTime);
          RXCodigoTitulo.AsString     :=PF.RetornarSigla(qryCanceladosTIPO_TITULO.AsInteger);
          RXEspecieTitulo.AsString    :=PF.RemoverAcento(PF.RetornarTitulo(qryCanceladosTIPO_TITULO.AsInteger));
          if Trim(RXEspecieTitulo.AsString)='' then
          begin
              RXEspecieTitulo.AsString:='Outros';
              RXCodigoTitulo.AsString :='DS';
          end;
          RXAcao.AsString             :='2';
          RXDataRegistro.AsString     :=FormatDateTime('DDMMYYYY',qryCanceladosDT_PAGAMENTO.AsDateTime);
          RXStatus.AsString           :='CANCELADO';
          RX.Post;
          qryCancelados.Next;
      end;

      qrySustados.Close;
      qrySustados.Params[0].AsDate:=edInicio.Date;
      qrySustados.Params[1].AsDate:=edFim.Date;
      qrySustados.Open;
      qrySustados.First;
      while not qrySustados.Eof do
      begin
          RX.Append;
          RXId.AsInteger              :=qrySustadosID_ATO.AsInteger;
          RXProtocolo.AsString        :=qrySustadosPROTOCOLO.AsString;
          RXDataProtocolo.AsString    :=FormatDateTime('DDMMYYYY',qrySustadosDT_PROTOCOLO.AsDateTime);
          RXDtProtocolo.AsDateTime    :=qrySustadosDT_PROTOCOLO.AsDateTime;
          RXDataProtesto.AsString     :=FormatDateTime('DDMMYYYY',qrySustadosDT_REGISTRO.AsDateTime);
          RXDtProtesto.AsDateTime     :=qrySustadosDT_SUSTADO.AsDateTime;
          RXDtTitulo.AsDateTime       :=qrySustadosDT_TITULO.AsDateTime;
          RXNomeApresentante.AsString :=PF.RemoverAcento(qrySustadosAPRESENTANTE.AsString);
          if RXNomeApresentante.AsString='' then
          RXNomeApresentante.AsString :=PF.RemoverAcento(qrySustadosCEDENTE.AsString);
          RXNomeDevedor.AsString      :=PF.RemoverAcento(qrySustadosDEVEDOR.AsString);
          if qrySustadosTIPO_DEVEDOR.AsString='J' then
            RXTipoDevedor.AsString:='2'
              else RXTipoDevedor.AsString:='1';
          RXDocumentoDevendor.AsString:=qrySustadosCPF_CNPJ_DEVEDOR.AsString;
          if qrySustadosDT_VENCIMENTO.AsDateTime=0 then
            RXDataVencimento.AsString:='A vista'
              else RXDataVencimento.AsString:=FormatDateTime('DDMMYYYY',qrySustadosDT_VENCIMENTO.AsDateTime);
          RXCodigoTitulo.AsString     :=PF.RetornarSigla(qrySustadosTIPO_TITULO.AsInteger);
          RXEspecieTitulo.AsString    :=PF.RemoverAcento(PF.RetornarTitulo(qrySustadosTIPO_TITULO.AsInteger));
          if Trim(RXEspecieTitulo.AsString)='' then
          begin
              RXEspecieTitulo.AsString:='Outros';
              RXCodigoTitulo.AsString :='DS';
          end;
          RXAcao.AsString             :='3';
          RXDataRegistro.AsString     :=FormatDateTime('DDMMYYYY',qrySustadosDT_SUSTADO.AsDateTime);
          RXStatus.AsString           :='SUSTADO';
          RX.Post;
          qrySustados.Next;
      end;
  end;
  RX.SortOnFields('DtProtesto');
  if RX.IsEmpty then
  begin
      P2.Visible:=False;
      P3.Visible:=False;
  end
  else
  begin
      P2.Visible:=True;
      P3.Visible:=True;
      P3.Caption:='T�tulos encontrados: '+IntToStr(RX.RecordCount);
  end;
  Screen.Cursor:=crDefault;
  RX.First;
  RX.EnableControls;
  PF.Aguarde(False);
end;

procedure TFOnline.edInicioChange(Sender: TObject);
begin
  if RgTipo.ItemIndex=1 then edFim.Date:=edInicio.Date;
end;

procedure TFOnline.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFOnline.btGerarClick(Sender: TObject);
var
  PT: Pointer;
  DataAtual: TDate;
  I,Q,C,S,P: Integer;
  Devedor,Tipo: String;
  DataVencimento,NomeApresentante,Especie,Codigo: String;
begin
  RXP.Close;
  RXP.Open;
  M.Lines.Clear;
  {ARQUIVO DE 5 ANOS}
  if RgTipo.ItemIndex=0 then
  begin
      if RX.IsEmpty then Exit;
      DataAtual:=edInicio.Date;
      repeat
          Application.ProcessMessages;
          PF.Aguarde(True,'Aguarde: '+FormatDateTime('dd/mm/yyyy',DataAtual));
          if PF.FimDeSemana(DataAtual) then
          begin
              DataAtual:=IncDay(DataAtual);
          end
          else
          begin
              qryProtestados.Close;
              qryProtestados.Params[0].AsDate:=DataAtual;
              qryProtestados.Params[1].AsDate:=DataAtual;
              qryProtestados.Open;
              if qryProtestados.IsEmpty then
              begin
                  {Header}
                  M.Lines.Add('0'+                                     {Constante 0}
                              FormatDateTime('DDMMYYYY',DataAtual)+    {Data de gera��o do arquivo}
                              FormatDateTime('DDMMYYYY',DataAtual)+    {Data da pr�tica do ato}
                              PF.Espaco('','E',336)+                   {Complemento em branco}
                              '00001');                                {Contador}
                  {Trailler}
                  M.Lines.Add('9'+                                     {Constante 9}
                              FormatDateTime('DDMMYYYY',DataAtual)+    {Data de gera��o do arquivo}
                              '000000'+                                {Qtd. de registros}
                              PF.Espaco('','E',338)+                   {Complemento em branco}
                              '00002');                                {Contador}
                  dm.AtualizarRemessa(DataAtual,'S',0,0,0);
                  DataAtual:=IncDay(DataAtual);
              end
              else
              begin
                  {Header}
                  M.Lines.Add('0'+                                     {Constante 0}
                              FormatDateTime('DDMMYYYY',DataAtual)+    {Data de gera��o do arquivo}
                              FormatDateTime('DDMMYYYY',DataAtual)+    {Data da pr�tica do ato}
                              PF.Espaco('','E',336)+                   {Complemento em branco}
                              '00001');                                {Contador}

                  I:=2;
                  Q:=0;
                  qryProtestados.First;
                  while not qryProtestados.Eof do
                  begin
                      PF.CarregarDevedor(qryProtestadosID_ATO.AsInteger);
                      while not dm.RxDevedor.Eof do
                      begin
                          if (Trim(dm.RxDevedorTIPO.AsString)     <>'') and
                             (Trim(dm.RxDevedorDOCUMENTO.AsString)<>'') and
                             (qryProtestadosDT_PROTOCOLO.AsDateTime<qryProtestadosDT_REGISTRO.AsDateTime) and
                             (qryProtestadosPROTOCOLO.AsInteger            <>0)  and
                             (qryProtestadosAPRESENTANTE.AsString          <>'') and
                             (qryProtestadosDT_TITULO.AsDateTime           <>0)  and
                             (not (RXP.Locate('PROTOCOLO',qryProtestadosPROTOCOLO.AsString,[]))) then
                          begin
                              RXP.Append;
                              RXPProtocolo.AsInteger:=qryProtestadosPROTOCOLO.AsInteger;
                              RXP.Post;

                              Inc(Q);

                              if qryProtestadosDT_VENCIMENTO.AsDateTime=0 then
                                DataVencimento:='A vista'
                                  else DataVencimento:=qryProtestadosDT_VENCIMENTO.AsString;

                              if qryProtestadosAPRESENTANTE.AsString='' then
                                NomeApresentante:=Trim(PF.RetirarEnter(qryProtestadosCEDENTE.AsString))
                                  else NomeApresentante:=Trim(PF.RetirarEnter(qryProtestadosAPRESENTANTE.AsString));

                              Codigo  :=PF.RetornarSigla(qryProtestadosTIPO_TITULO.AsInteger);
                              Especie :=PF.RetornarTitulo(qryProtestadosTIPO_TITULO.AsInteger);
                              if Trim(Especie)='' then
                              begin
                                  Codigo :='DS';
                                  Especie:='Outros';
                              end;

                              Devedor:=PF.RetirarEnter(dm.RxDevedorNOME.AsString);

                              if dm.RxDevedorTIPO.AsString='J' then
                                Tipo:='2'
                                  else Tipo:='1';

                              M.Lines.Add({1} '1'+
                                          {2} PF.Espaco(qryProtestadosPROTOCOLO.AsString,'E',10)+
                                          {3} FormatDateTime('DDMMYYYY',qryProtestadosDT_PROTOCOLO.AsDateTime)+
                                          {4} '          '+
                                          {5} '          '+
                                          {6} FormatDateTime('DDMMYYYY',qryProtestadosDT_REGISTRO.AsDateTime)+
                                          {7} PF.Espaco(Copy(NomeApresentante,1,100),'E',100)+
                                          {8} PF.Espaco(Copy(Devedor,1,100),'E',100)+
                                          {9} Tipo+
                                          {10}PF.Espaco(Copy(dm.RxDevedorDOCUMENTO.AsString,1,14),'E',14)+
                                          {11}Copy(PF.Espaco(DataVencimento,'E',20),1,20)+
                                          {12}FormatDateTime('DDMMYYYY',qryProtestadosDT_TITULO.AsDateTime)+
                                          {13}PF.Espaco(Codigo,'E',3)+
                                          //{13}Copy(PF.RemoverAcento(PF.Espaco(Especie,'E',40)),1,40)+
                                          {14}'1'+
                                          {15}'1'+
                                          {16}FormatDateTime('DDMMYYYY',qryProtestadosDT_REGISTRO.AsDateTime)+
                                          {17}PF.Espaco('','E',50)+
                                          {18}PF.Zeros(IntToStr(I),'D',5));
                              Inc(I);
                          end;
                          dm.RxDevedor.Next;
                      end;
                      qryProtestados.Next;
                  end;
                  {Trailler}
                  M.Lines.Add('9'+                                     {Constante 9}
                              FormatDateTime('DDMMYYYY',DataAtual)+    {Data de gera��o do arquivo}
                              PF.Zeros(IntToStr(Q),'D',6)+             {Qtd. de registros}
                              PF.Espaco('','E',338)+                   {Complemento em branco}
                              PF.Zeros(IntToStr(I),'D',5));            {Contador}
                  dm.AtualizarRemessa(DataAtual,'S',qryProtestados.RecordCount,0,0);
                  DataAtual:=IncDay(DataAtual);
              end;
          end;
      until DataAtual>edFim.Date;
  end
  else
  begin
      dm.Q1:=TFDQuery.Create(Nil);
      dm.Q1.Connection:=dm.conSISTEMA;
      dm.Q1.SQL.Add('SELECT * FROM ONLINE WHERE ENVIADO='''+'N'+'''AND DATA <:DATA');
      dm.Q1.ParamByName('DATA').AsDate:=edInicio.Date;
      dm.Q1.Open;
      if not dm.Q1.IsEmpty then
      begin
          GR.Aviso('N�o foi poss�vel gerar o arquivo!'+#13#13+
                   'Existe remessa de data anterior n�o enviada.');
          Exit;
      end;
      dm.Q1.Close;
      dm.Q1.Free;

      {Header}
      M.Lines.Add('0'+                                      {Constante 0}
                  FormatDateTime('DDMMYYYY',edInicio.Date)+ {Data de gera��o do arquivo}
                  FormatDateTime('DDMMYYYY',edInicio.Date)+ {Data da pr�tica do ato}
                  PF.Espaco('','E',336)+                    {Complemento em branco}
                  '00001');                                 {Contador}

      I:=2;
      Q:=0;
      C:=0;{CANCELADOS}
      P:=0;{PROTESTADOS}
      S:=0;{SUSTADOS}

      PT:=RX.GetBookmark;
      RX.DisableControls;
      RX.First;
      while not RX.Eof do
      begin
          if RXCheck.AsBoolean then
          begin
              PF.CarregarDevedor(RXId.AsInteger);
              while not dm.RxDevedor.Eof do
              begin
                  if (dm.RxDevedorTIPO.AsString<>'') and (dm.RxDevedorNOME.AsString<>'') and (RXDtTitulo.AsDateTime<>0) then
                  begin
                      Inc(Q);
                      M.Lines.Add({01}'1'+
                                  {02}PF.Espaco(RXProtocolo.AsString,'E',10)+
                                  {03}RXDataProtocolo.AsString+
                                  {04}'          '+
                                  {05}'          '+
                                  {06}RXDataProtesto.AsString+
                                  {07}PF.Espaco(RXNomeApresentante.AsString,'E',100)+
                                  {08}PF.Espaco(IfThen(RXFormatacao.AsBoolean,dm.RxDevedorNOME.AsString,GR.RemoverAcento(dm.RxDevedorNOME.AsString)),'E',100)+
                                  {09}IfThen(dm.RxDevedorTIPO.AsString='F','1','2')+
                                  {10}PF.Espaco(dm.RxDevedorDOCUMENTO.AsString,'E',14)+
                                  {11}PF.Espaco(RXDataVencimento.AsString,'E',20)+
                                  {12}FormatDateTime('DDMMYYYY',RXDtTitulo.AsDateTime)+
                                  {13}PF.Espaco(RXCodigoTitulo.AsString,'E',3)+
                                  //{13}PF.Espaco(RXEspecieTitulo.AsString,'E',40)+
                                  {14}'1'+
                                  {15}RXAcao.AsString+
                                  {16}RXDataRegistro.AsString+
                                  {17}PF.Espaco('','E',50)+
                                  {18}PF.Zeros(IntToStr(I),'D',5));
                      if RXStatus.AsString='CANCELADO'  then Inc(C);
                      if RXStatus.AsString='PROTESTADO' then Inc(P);
                      if RXStatus.AsString='SUSTADO'    then Inc(S);
                      Inc(I);
                  end;
                  dm.RxDevedor.Next;
              end;
              RX.Edit;
              RXCheck.AsBoolean:=False;
              RX.Post;
          end;
          RX.Next;
      end;
      dm.AtualizarRemessa(edInicio.Date,'N',P,S,C);
      {Trailler}
      M.Lines.Add('9'+                                     {Constante 9}
                  FormatDateTime('DDMMYYYY',edInicio.Date)+{Data de gera��o do arquivo}
                  PF.Zeros(IntToStr(Q),'D',6)+             {Qtd. de registros}
                  PF.Espaco('','E',338)+                   {Complemento em branco}
                  PF.Zeros(IntToStr(I),'D',5));            {Contador}

      if PT<>Nil then
        if RX.BookmarkValid(PT) then
          RX.GotoBookmark(PT);
      RX.EnableControls;
  end;
  M.Lines.SaveToFile(dm.ValorParametro(41)+FormatDateTime('DDMMYYYY',edInicio.Date)+'.txt');
  dm.Online.Refresh;
  PF.Aguarde(False);
  GR.Aviso('Arquivo gerado com sucesso!');
end;

procedure TFOnline.Grid1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  if X in [11..46] then
    Screen.Cursor:=crHandPoint
      else Screen.Cursor:=crDefault;
end;

procedure TFOnline.Grid1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Screen.Cursor=crHandPoint then
  begin
      if dm.Online.IsEmpty then Exit;
      if Screen.Cursor=crHandPoint then
      begin
          dm.Online.Edit;
          if dm.OnlineENVIADO.AsString='N' then
            dm.OnlineENVIADO.AsString:='S'
              else dm.OnlineENVIADO.AsString:='N';
          dm.Online.Post;
          dm.Online.ApplyUpdates(0);
      end;
  end;
end;

procedure TFOnline.Marcar(Tipo: String);
var
  Q: TFDQuery;
  Posicao: Integer;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT COUNT(ID_ONLINE) AS QTD FROM ONLINE');
  Q.Open;
  G.MaxValue:=Q.FieldByName('QTD').AsInteger;
  Q.Close;
  Q.Free;
  G.Progress:=0;
  G.Visible:=True;
  Posicao:=dm.Online.RecNo;
  dm.Online.DisableControls;
  dm.Online.First;
  while not dm.Online.Eof do
  begin
      Application.ProcessMessages;
      G.AddProgress(1);
      dm.Online.Edit;
      dm.OnlineENVIADO.AsString:=Tipo;
      dm.Online.Post;
      dm.Online.Next;
  end;
  dm.Online.ApplyUpdates(0);
  dm.Online.RecNo:=Posicao;
  dm.Online.EnableControls;
  G.Visible:=False;
end;

procedure TFOnline.Marcartodos1Click(Sender: TObject);
begin
  Marcar('S');
end;

procedure TFOnline.Desmarcartodos1Click(Sender: TObject);
begin
  Marcar('N');
end;

procedure TFOnline.RXFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
  if vCondicao='Nada'         then Exit;
  if vCondicao='Data'         then Accept:=RXDtProtesto.AsDateTime<RXDtProtocolo.AsDateTime;
  if vCondicao='Protocolo'    then Accept:=(RXProtocolo.AsString='') or (RXProtocolo.AsString='0');
  if vCondicao='Apresentante' then Accept:=Trim(RXNomeApresentante.AsString)='';
  if vCondicao='Documento'    then Accept:=Trim(RXDocumentoDevendor.AsString)='';
end;

procedure TFOnline.MenuItem1Click(Sender: TObject);
begin
  vCondicao  :='Data';
  RX.Filtered:=False;
  RX.Filtered:=True;
end;

procedure TFOnline.MenuItem2Click(Sender: TObject);
begin
  vCondicao  :='Protocolo';
  RX.Filtered:=False;
  RX.Filtered:=True;
end;

procedure TFOnline.Nomedo1Click(Sender: TObject);
begin
  vCondicao  :='Apresentante';
  RX.Filtered:=False;
  RX.Filtered:=True;
end;

procedure TFOnline.Mostrartodos1Click(Sender: TObject);
begin
  RX.Filtered:=False;
end;

procedure TFOnline.Grid2DblClick(Sender: TObject);
begin
  if RX.IsEmpty then Exit;

  Self.Visible:=False;

  dm.vTipo:='A';

  Application.CreateForm(TFTitulo,FTitulo);
  FTitulo.CarregarTitulo(RXId.AsInteger);
  FTitulo.Visualizacao;
  FTitulo.ShowModal;
  FTitulo.Free;

  qryAlteracao.Close;
  qryAlteracao.Params[0].AsInteger:=RXId.AsInteger;
  qryAlteracao.Open;

  RX.Edit;
  RXProtocolo.AsString        :=qryAlteracaoPROTOCOLO.AsString;
  RXDataProtocolo.AsString    :=FormatDateTime('DDMMYYYY',qryAlteracaoDT_PROTOCOLO.AsDateTime);
  RXDtProtocolo.AsDateTime    :=qryAlteracaoDT_PROTOCOLO.AsDateTime;
  RXDataProtesto.AsString     :=FormatDateTime('DDMMYYYY',qryAlteracaoDT_REGISTRO.AsDateTime);
  RXDtProtesto.AsDateTime     :=qryAlteracaoDT_REGISTRO.AsDateTime;
  RXNomeApresentante.AsString :=PF.RemoverAcento(qryAlteracaoAPRESENTANTE.AsString);
  RXNomeDevedor.AsString      :=PF.RemoverAcento(qryAlteracaoDEVEDOR.AsString);
  RXTipoDevedor.AsString      :=qryAlteracaoTIPO_DEVEDOR.AsString;
  RXDocumentoDevendor.AsString:=qryAlteracaoCPF_CNPJ_DEVEDOR.AsString;
  RXDataVencimento.AsString   :=FormatDateTime('DDMMYYYY',qryAlteracaoDT_VENCIMENTO.AsDateTime);
  RXCodigoTitulo.AsString     :=PF.RetornarSigla(qryAlteracaoTIPO_TITULO.AsInteger);
  RXEspecieTitulo.AsString    :=PF.RemoverAcento(PF.RetornarTitulo(qryAlteracaoTIPO_TITULO.AsInteger));
  RXAcao.AsString             :='1';
  RXDataRegistro.AsString     :=FormatDateTime('DDMMYYYY',qryAlteracaoDT_REGISTRO.AsDateTime);
  RXStatus.AsString           :='PROTESTADO';
  RX.Post;

  Self.Visible:=True;
end;

procedure TFOnline.Grid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_DELETE then
  begin
      if dm.Online.IsEmpty then Exit;
      dm.Online.Delete;
      dm.Online.ApplyUpdates(0);
  end;
end;

procedure TFOnline.Image1Click(Sender: TObject);
begin
ShellExecute(Handle,'Open','https://rj.bancoprotesto.com.br/banco/site/admin.php',Nil,Nil,SW_SHOW);
end;

procedure TFOnline.DocumentodoDevedornoinformadp1Click(Sender: TObject);
begin
  vCondicao  :='Documento';
  RX.Filtered:=False;
  RX.Filtered:=True;
end;

procedure TFOnline.Grid2MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if X in [11..93] then
    Screen.Cursor:=crHandPoint
      else Screen.Cursor:=crDefault;
end;

procedure TFOnline.Grid2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Screen.Cursor=crHandPoint then
    if not RX.IsEmpty then
    begin
        RX.Edit;
        if X in [11..52] then
        RXCheck.AsBoolean:=not RXCheck.AsBoolean;
        if X in [53..93] then
        RXFormatacao.AsBoolean:=not RXFormatacao.AsBoolean;
        RX.Post;
    end;
end;

end.
