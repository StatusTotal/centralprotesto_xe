�
 TFQUICKCERTIDAO1 0�q  TPF0TFQuickCertidao1FQuickCertidao1Left� TopWidthHeight�VertScrollBar.Position� Caption	   CertidãoColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderScaledOnCreate
FormCreatePixelsPerInch`
TextHeight 	TQuickRep
qkCertidaoLeft� Top�WidthHeightcFrame.ColorclBlackFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	DataSetRXFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' 	OnEndPageqkCertidaoEndPageOptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPrevInitialZoomqrZoomToWidthPreviewDefaultSaveTypestQRP TQRBand	CabecalhoLeft9Top@Width�HeightoFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintCabecalhoBeforePrintColorclWhiteTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUU��@TUUUUU��	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbTitle TQRRichTextRT1LeftTop� Width�Height2Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������J�@UUUUUUQ�@������B�@      ��	@ 	AlignmenttaRightJustifyAutoStretchColorclWindowFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style ParentRichEditRE1  	TQRDBTexttxNomeServentiaLeft!TopWidthbHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@UUUUUU)�@������j�@UUUUUU��@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchColorclWhiteDataSetdm.Serventia	DataField	DESCRICAOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText
txEnderecoLeft�Top7WidthFHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      l�@UUUUUU��@UUUUUU5�@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchColorclWhiteDataSetdm.Serventia	DataFieldENDERECOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize	  TQRLabel
lbCertidaoLeftTop� WidthiHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������j�@UUUUUU-�@VUUUUU�@      �@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaption   C E R T I D Ã OColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsUnderline 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabel	lbTitularLeft]Top� Width1HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU�	@TUUUUU��@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionTITULARColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize	  	TQRDBTexttxEmailLeftATop^Width#HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      T�@TUUUUU��@UUUUUU5�@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchColorclWhiteDataSetdm.Serventia	DataFieldEMAILFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize	  	TQRDBTexttxCNPJLeftBTopzWidth!HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU�@TUUUUU��@UUUUUUe�@      ��@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchColorclWhiteDataSetdm.Serventia	DataFieldCNPJFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize	  TQRRichTextRT2Left
TopWidth�HeightLFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU�@��������@��������@������Q�	@ 	AlignmenttaCenterAutoStretchColorclWindowFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style ParentRichEditRE2  	TQRDBTexttxSiteLeft�Top� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU�@UUUUUU��@      ��@      ��@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchColorclWhiteDataSetdm.Serventia	DataFieldSITEFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize	  TQRLabel
lbTelefoneLeft�TopKWidth@HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������g�@      p�@UUUUUUU�@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaptionTELEFONEColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize	  TQRLabellbPedidoLeftTop� Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU�@�������@������z�@�������@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaptionData do Pedido: 00/00/0000ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabellbTexto4Left� TopKWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������j�@UUUUUU��@VUUUUU��@      ��@ 	AlignmenttaCenterAlignToBand	AutoSizeAutoStretchCaption   NÃO CONSTAM PROTESTOSColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabellbOutrossimLeft"Top`Width`HeightEnabledFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@�������@VUUUUU��@UUUUUU�	@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretch	Caption�   Outrossim, informo que o período anterior deverá ser pesquisado junto ao Cartório do 3º Ofício de Nova Iguaçu, neste Estado.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize   TQRBandDetalheLeft9Top�Width�HeightkFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRight
AfterPrintDetalheAfterPrintAlignToBottomBeforePrintDetalheBeforePrintColorclWhiteTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUU��@TUUUUU��	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail TQRShapeQRShape1LeftTopWidthWHeighteFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@       �@       �@      0�@ ShapeqrsRectangle
VertAdjust   TQRLabelQRLabel5Left%TopWidth4HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@       �@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionPortadorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel6Left*TopWidth/HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@     @�@VUUUUU��@TUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption   EspécieColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRShapeQRShape2LeftaTopWidth�HeighteFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@������R�@       �@UUUUUUo�	@ ShapeqrsRectangle
VertAdjust   TQRShapeQRShape3Left?TopWidthUHeighteFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU+�	@       �@UUUUUU��@ ShapeqrsRectangle
VertAdjust   TQRLabelQRLabel7LeftTop&WidthBHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������j�@UUUUUU�@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption   Nº do TítuloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText	QRDBText3LefteTopWidth^HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��@       �@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetRX	DataFieldApresentanteTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText	QRDBText4LefteTopWidth^HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��@VUUUUU��@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetRX	DataFieldEspecieTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText	QRDBText5LefteTop&WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��@UUUUUU�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetRX	DataFieldTituloTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel8LeftTop6WidthNHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@VUUUUU��@      ��@      `�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption   Data do TítuloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText	QRDBText6LefteTop6Width*HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��@      ��@     @�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetRX	DataField	Dt_TituloTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel9Left�TopWidth9HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUW�	@       �@      Ж@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption	ProtocoloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabel	QRLabel10Left�TopWidthSHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��	@VUUUUU��@��������@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionProtestado emColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabel	QRLabel11Left�Top&Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@�������	@UUUUUU�@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionLivro/FolhaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabel	QRLabel12Left�Top6WidthTHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUi�	@      ��@      @�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption   Valor do TítuloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabel	QRLabel14Left� Top6WidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@TUUUUU��@      ��@������ڻ@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption
VencimentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText	QRDBText7LeftTop6Width:HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@      ��@UUUUUUu�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetRX	DataField
VencimentoTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText	QRDBText8LeftBTopWidthMHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU)�	@       �@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetRX	DataField	ProtocoloTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText	QRDBText9LeftBTopWidthMHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU)�	@VUUUUU��@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetRX	DataFieldDt_ProtestoTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText
QRDBText10LeftBTop&WidthMHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU)�	@UUUUUU�@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetRX	DataField
LivroFolhaTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText
QRDBText11LeftBTop6WidthMHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU)�	@      ��@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetRX	DataFieldValorTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel2Left,TopFWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@VUUUUU��@UUUUUU5�@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionSacadorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText	QRDBText1LefteTopFWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��@UUUUUU5�@      �	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetRX	DataFieldSacadorTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel3Left)TopVWidth0HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��@��������@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionCedenteColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText	QRDBText2LefteTopVWidth^HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��@��������@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetRX	DataFieldCedenteTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel4Left�TopVWidthTHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUi�	@��������@      @�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionEndossoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText
QRDBText12LeftBTopVWidthMHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU)�	@��������@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetRX	DataFieldEndossoTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel1Left�TopFWidth8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��	@UUUUUU5�@������*�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption	Custas R$ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText
QRDBText13LeftBTopFWidthMHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU)�	@UUUUUU5�@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetRX	DataFieldTotalTransparent	WordWrap	ExportAsexptTextFontSize   TQRBandSummaryLeft9TopWidth�Height� Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintSummaryBeforePrintColorclWhiteTransparentBandForceNewColumnForceNewPageSize.Values��������@TUUUUU��	@ PreCaluculateBandHeightKeepOnOnePageBandType	rbSummary TQRLabellbEscreventeLeft	Top
Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU�@      ��@��������@      P�	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption5Eu,______________________Escrevente, efetuei a busca.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrapExportAsexptTextFontSize  TQRLabel	QRLabel17Left� Top'Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUe�@      `�@      �@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaption   O referido é verdade e dou féColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize
  TQRLabellbCidadeLeft9Top7Width3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU	�@UUUUUU��@      ��@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaptionlbCidadeColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize
  TQRLabellbAssinaturaLeft4TopgWidth<HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU�@��������@������B�@      ��@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaption
AssinaturaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize	  TQRShapeQRShape5Left� TopeWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU� @UUUUUU��@UUUUUU��@�������@ Brush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabellbMatriculaLeft9TopuWidth2HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU�@UUUUUU	�@      Ț@������J�@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaption	MatriculaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize	  TQRLabellbOrdemLeft;TopoWidthKHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU�@������ؼ	@      ؒ@      p�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Nº de Ordem:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize	  TQRMemoMLeftTop� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@UUUUUUM�@������K�	@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretch	ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellbCGJ1LeftTopWidthhHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������	@UUUUUUu�@UUUUUU��@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption   Poder Judiciário - TJERJColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRLabellbCGJ2LeftTop(Width}HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUQ�	@��������@UUUUUU]�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption   Corregedoria Geral da JustiçaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRLabellbCGJ3LeftTop3Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��	@      ��@��������@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption"   Selo de Fiscalização EletrônicoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRLabellbSeloEletronicoLeftTop@WidthVHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@�������	@UUUUUUU�@��������@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaptionLLLL 00000 XXXColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRLabellbCGJ4LeftTopMWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��	@��������@�������@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaptionConsulte a validade do selo em:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRLabellbCGJ5Left�TopXWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������ �	@VUUUUU��@UUUUUU5�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption$https://www3.tjrj.jus.br/sitepublicoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize   TQRBandCBRodapeLeft9Top�Width�Height#Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUU5�@TUUUUU��	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageFooter TQRLabel	QRLabel18Left� TopWidth{HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU�@UUUUUU�@UUUUUUU� @UUUUUU��@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaption9   A PRESENTE CERTIDÃO NÃO CONTÉM RASURAS NEM "EM TEMPO".ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize	  TQRLabelqrAvisoLeft� TopWidth HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU�@      �@UUUUUUU�@UUUUUUU�@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretch	Caption.   O PRAZO DESTA CERTIDÃO É VÁLIDO POR 90 DIASColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  
TQRSysData	sysPaginaLeft>Top	WidthJHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������ֽ	@      ��@��������@ 	AlignmenttaRightJustifyAlignToBandAutoSize	ColorclWhiteDataqrsPageNumberText   Página Transparent	ExportAsexptTextFontSize    	TRichEditRE2LeftTopq�Width�HeightFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTabOrder  	TRichEditRE1LeftTop�Width,HeightFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTabOrder  TRxMemoryDataRX	FieldDefs LeftTop� TStringFieldRXTitulo	FieldNameTitulo  TStringFieldRXApresentante	FieldNameApresentanteSized  TStringField	RXDevedor	FieldNameDevedorSized  TFloatFieldRXValor	FieldNameValorDisplayFormat
###,##0.00  
TDateFieldRXVencimento	FieldName
VencimentoDisplayFormat
DD/MM/YYYY  TStringFieldRXProtocolo	FieldName	Protocolo  
TDateFieldRXDt_Titulo	FieldName	Dt_TituloDisplayFormat
DD/MM/YYYY  TStringField	RXEspecie	FieldNameEspecieSized  TStringField	RXSacador	FieldNameSacadorSized  TStringField	RXCedente	FieldNameCedenteSized  
TDateFieldRXDt_Protesto	FieldNameDt_ProtestoDisplayFormat
DD/MM/YYYY  TStringField	RXEndosso	FieldNameEndossoSize  TStringFieldRXLivroFolha	FieldName
LivroFolha  TFloatFieldRXTotal	FieldNameTotalDisplayFormat
###,##0.00    