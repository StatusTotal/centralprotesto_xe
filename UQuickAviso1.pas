unit UQuickAviso1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, DB, RxMemDS, StdCtrls, ComCtrls;

type
  TFQuickAviso1 = class(TForm)
    Aviso: TQuickRep;
    Cabecalho: TQRBand;
    QRLabel1: TQRLabel;
    Detalhe: TQRBand;
    RX: TRxMemoryData;
    RXTitulo: TStringField;
    RXApresentante: TStringField;
    RXDevedor: TStringField;
    RXValor: TFloatField;
    RXVencimento: TDateField;
    RXProtocolo: TStringField;
    RXSaldo: TFloatField;
    RXCustas: TFloatField;
    RXEndereco: TStringField;
    RXDt_Titulo: TDateField;
    RXDt_Prazo: TDateField;
    RXDt_Protocolo: TDateField;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRLabel7: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    RXEspecie: TStringField;
    QRLabel8: TQRLabel;
    QRDBText6: TQRDBText;
    qrDocumento: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText7: TQRDBText;
    QRLabel15: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRBand1: TQRBand;
    QRShape4: TQRShape;
    lbCidade: TQRLabel;
    lbAssinatura: TQRLabel;
    lbTabeliao: TQRLabel;
    lbCartorio: TQRLabel;
    QRLabel16: TQRLabel;
    lbEndereco: TQRLabel;
    lbTelefone: TQRLabel;
    lbSite: TQRLabel;
    RXIrregularidade: TStringField;
    lbHorario: TQRLabel;
    RE: TRichEdit;
    RXDt_Edital: TDateField;
    RXSacador: TStringField;
    RXTarifa: TFloatField;
    RXDt_Intimacao: TDateField;
    RXCPF_CNPJ_Devedor: TStringField;
    QRDBText10: TQRDBText;
    RXTipo: TStringField;
    RXID_ATO: TIntegerField;
    RXEmol: TFloatField;
    RXFund: TFloatField;
    RXFunp: TFloatField;
    RXMutua: TFloatField;
    RXAcoterj: TFloatField;
    RXDist: TFloatField;
    RXTotal: TFloatField;
    RXFetj: TFloatField;
    lbMatricula: TQRLabel;
    QRLabel19: TQRLabel;
    QRShape5: TQRShape;
    QRDBText14: TQRDBText;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel3: TQRLabel;
    lbPortador: TQRLabel;
    lbConsiderando: TQRLabel;
    lbProtocolo: TQRLabel;
    lbOficio: TQRLabel;
    QRLabel9: TQRLabel;
    lbDevolvido: TQRLabel;
    procedure CabecalhoBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure AvisoEndPage(Sender: TCustomQuickRep);
    procedure FormCreate(Sender: TObject);
    procedure DetalheBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickAviso1: TFQuickAviso1;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFQuickAviso1.CabecalhoBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbCartorio.Caption:=dm.ServentiaDESCRICAO.AsString;
  lbTabeliao.Caption:=dm.ServentiaTABELIAO.AsString+' - '+dm.ServentiaTITULO.AsString;
  lbEndereco.Caption:=dm.ServentiaENDERECO.AsString+' - '+dm.ServentiaBAIRRO.AsString+' - Cep: '+dm.ServentiaCEP.AsString;
  lbTelefone.Caption:='E-mail: '+dm.ServentiaEMAIL.AsString+' - '+'('+Copy(dm.ServentiaTELEFONE.AsString,1,2)+')'+Copy(dm.ServentiaTELEFONE.AsString,3,4)+'-'+Copy(dm.ServentiaTELEFONE.AsString,7,4);
  lbSite.Caption    :=dm.ServentiaSITE.AsString;
end;

procedure TFQuickAviso1.AvisoEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickAviso1.FormCreate(Sender: TObject);
begin
  lbHorario.Caption     :='Hor�rio de Funcionamento: '+dm.ValorParametro(25);
end;

procedure TFQuickAviso1.DetalheBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  RE.Clear;
  RE.Text:='           Nos termos do art. 15, da Lei n� 9.492, de 10 de setembro de 1997, intimo atrav�s deste '+
           'edital, o sacado abaixo-descrito a vir pagar o t�tulo correspondente ou dar as raz�es pelo qual n�o o '+
           'faz, nesta Serventia, em prazo de tr�s dias �teis contados da protocoliza��o dos mesmos, de acordo com o '+
           'art. 125 da Lei supra, em '+FormatDateTime('dd/mm/yyyy',RXDt_Edital.AsDateTime)+', pelo seguinte motivo:';
  PF.JustificarRichEdit(RE,True);
  if RXTipo.AsString='J' then
    qrDocumento.Caption:='CNPJ:'
      else
        if RXTipo.AsString='J' then
          qrDocumento.Caption:='CPF:'
            else qrDocumento.Caption:='Documento:';

  lbOficio.Caption:='Of�cio n� '+InputBox('Of�cio','N�','')+'/'+FormatDateTime('yyyy',Now)+' - PROTESTO';
  lbProtocolo.Caption:='Protocolo: '+RXProtocolo.AsString;
  lbCidade.Caption:=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',Now);
  lbConsiderando.Caption:='considerando apresenta��o, em '+FormatDateTime('dd/mm/yyyy',RXDt_Protocolo.AsDateTime)+', do t�tulo abaixo:';
  lbDevolvido.Caption:='Esclare�a-se, por fim, que o n�o atendimento at� o dia '+FormatDateTime('dd/mm/yyyy',RXDt_Prazo.AsDateTime)+
                       ', inviabilizar� o prosseguimento regular do procedimento do protesto extrajudicial e o '+
                       'referido ser� devolvido sem protesto.';
end;

procedure TFQuickAviso1.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbAssinatura.Caption :=dm.vAssinatura;
  lbMatricula.Caption  :=dm.vMatricula;
  lbPortador.Caption   :=RXApresentante.AsString+'.';
end;

end.



