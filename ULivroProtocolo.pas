unit ULivroProtocolo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, Mask, sMaskEdit, sCustomComboEdit, sTooledit,
  ExtCtrls, sPanel, Buttons, sBitBtn, DB, DBCtrls, sDBLookupComboBox, SQLExpr,
  sCheckBox, sGroupBox, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFLivroProtocolo = class(TForm)
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    P1: TsPanel;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    edLivro: TsEdit;
    edFolha: TsEdit;
    ckCabecalho: TsCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btVisualizarClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ckCabecalhoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SalvarRX;
    procedure FolhasRelatorio(Paginas: Integer);
    procedure Modelo1;
    procedure Modelo2;
  private
    { Private declarations }
    Q: TFDQuery;
  public
    { Public declarations }
  end;

var
  FLivroProtocolo: TFLivroProtocolo;

implementation

uses UDM, UPF, UQuickLivroProtocolo1, UAssinatura, UGeral, StrUtils,QRCtrls, UGDM;

{$R *.dfm}

procedure TFLivroProtocolo.FolhasRelatorio(Paginas: Integer);
var
  I: Integer;
begin
  Gdm.cQuickFolhas.EmptyDataSet;
  for I := 1 to Paginas do
  begin
      Gdm.cQuickFolhas.AppendRecord([vLivro,I,vFolha]);
      Inc(vFolha);
      if vFolha>StrToInt(dm.ValorParametro(86)) then
      begin
          Inc(vLivro);
          vFolha:=1;
      end;
  end;
end;

procedure TFLivroProtocolo.FormCreate(Sender: TObject);
begin
  dm.Escreventes.Close;
  dm.Escreventes.Open;

  if dm.vLivroConvenio then
  Self.Caption:='Livro de Conv�nio';

  ckCabecalho.Checked :=dm.ValorParametro(21)='S';
  edLivro.Text        :=IntToStr(dm.ValorAtual('LPR','N'));
  edFolha.Text        :=IntToStr(dm.ValorAtual('FPR','N'));
end;

procedure TFLivroProtocolo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.Escreventes.Close;
end;

procedure TFLivroProtocolo.btVisualizarClick(Sender: TObject);
begin
  {S�O JOS� DE UB� E OF�CIO �NICO DE QUATIS}
  if (dm.ServentiaCODIGO.AsInteger=4448) or (dm.ServentiaCODIGO.AsInteger=2357) then
    Modelo2
      else Modelo1;
end;

procedure TFLivroProtocolo.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFLivroProtocolo.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFLivroProtocolo.ckCabecalhoClick(Sender: TObject);
begin
  if ckCabecalho.Checked then
    dm.AtualizarParametro(21,'S')
      else dm.AtualizarParametro(21,'N');
end;

procedure TFLivroProtocolo.FormShow(Sender: TObject);
begin
  edInicio.SetFocus;
end;

procedure TFLivroProtocolo.SalvarRX;
begin
  with FQuickLivroProtocolo1 do
  begin
      RX.Append;
      RXIdAto.AsInteger           :=Q.FieldByName('ID_ATO').AsInteger;
      RXTitulo.AsString           :=Q.FieldByName('NUMERO_TITULO').AsString;
      RXApresentante.AsString     :=Q.FieldByName('APRESENTANTE').AsString;
      RXDevedor.AsString          :=Q.FieldByName('DEVEDOR').AsString;
      RXValor.AsFloat             :=Q.FieldByName('VALOR_TITULO').AsFloat;
      RXSaldoTitulo.AsFloat       :=Q.FieldByName('SALDO_TITULO').AsFloat;
      RXDataVencimento.AsString   :=IfThen(Q.FieldByName('AVISTA').AsString='S',GR.FormatarData(Q.FieldByName('DT_TITULO').AsDateTime),
                                                                                IfThen(Q.FieldByName('DT_VENCIMENTO').AsDateTime=0,
                                                                                '� VISTA',GR.FormatarData(Q.FieldByName('DT_VENCIMENTO').AsDateTime)));
      RXProtocolo.AsString        :=Q.FieldByName('PROTOCOLO').AsString;
      RXSaldo.AsFloat             :=Q.FieldByName('SALDO_PROTESTO').AsFloat;
      RXCustas.AsFloat            :=Q.FieldByName('TOTAL').AsFloat;
      PF.CarregarDevedor(Q.FieldByName('ID_ATO').AsInteger);
      RXEndereco.AsString         :=dm.RxDevedorENDERECO.AsString;
      RXDataTitulo.AsDateTime     :=Q.FieldByName('DT_TITULO').AsDateTime;
      RXDataProtocolo.AsDateTime  :=Q.FieldByName('DT_PROTOCOLO').AsDateTime;
      if Q.FieldByName('DT_PRAZO').AsDateTime<>0 then
      RXDataPrazo.AsDateTime      :=Q.FieldByName('DT_PRAZO').AsDateTime;
      RXEspecie.AsString          :=PF.RetornarTitulo(Q.FieldByName('TIPO_TITULO').AsInteger);
      RXSacador.AsString          :=Q.FieldByName('SACADOR').AsString;
      RXCedente.AsString          :=Q.FieldByName('CEDENTE').AsString;
      RXStatus.AsString           :=Q.FieldByName('STATUS').AsString;
      RXConvenio.AsString         :=Q.FieldByName('CONVENIO').AsString;

      if Q.FieldByName('STATUS').AsString='PROTESTADO' then RXDataSolucao.AsDateTime:=Q.FieldByName('DT_REGISTRO').AsDateTime;
      if Q.FieldByName('STATUS').AsString='CANCELADO'  then RXDataSolucao.AsDateTime:=Q.FieldByName('DT_PAGAMENTO').AsDateTime;
      if Q.FieldByName('STATUS').AsString='PAGO'       then RXDataSolucao.AsDateTime:=Q.FieldByName('DT_PAGAMENTO').AsDateTime;
      if Q.FieldByName('STATUS').AsString='RETIRADO'   then RXDataSolucao.AsDateTime:=Q.FieldByName('DT_RETIRADO').AsDateTime;
      if Q.FieldByName('STATUS').AsString='DEVOLVIDO'  then RXDataSolucao.AsDateTime:=Q.FieldByName('DT_DEVOLVIDO').AsDateTime;

      if Q.FieldByName('STATUS').AsString='SUSTADO' then
        if Q.FieldByName('TIPO_SUSTACAO').AsString='L' then
          RXDataSolucao.AsDateTime:=Q.FieldByName('DT_SUSTADO').AsDateTime
            else
              if Q.FieldByName('TIPO_SUSTACAO').AsString='D' then
                RXDataSolucao.AsDateTime:=Q.FieldByName('DT_DEFINITIVA').AsDateTime
                  else
                    RXDataSolucao.AsDateTime:=Q.FieldByName('DT_RETORNO_PROTESTO').AsDateTime;

      if Q.FieldByName('STATUS').AsString='APONTADO' then
        RXHora.AsDateTime := Q.FieldByName('HORA').AsDateTime;


      RXDataTermo.AsDateTime:=GR.iif(dm.vLivroConvenio,RXDataSolucao.AsDateTime,RXDataProtocolo.AsDateTime);

      if Q.FieldByName('TIPO_ENDOSSO').AsString='M' then RXEndosso.AsString:='Mandato';
      if Q.FieldByName('TIPO_ENDOSSO').AsString='T' then RXEndosso.AsString:='Translativo';
      if Q.FieldByName('TIPO_ENDOSSO').AsString='S' then RXEndosso.AsString:='Sem Endosso';

      RXDocumento.AsString      :=Q.FieldByName('CPF_CNPJ_DEVEDOR').AsString;
      RXAssina.AsString         :=dm.vAssinatura;
      RXEncerramento.AsBoolean  :=False;
      RX.Post;
  end;
end;

procedure TFLivroProtocolo.Modelo1;
var
  I: Integer;
  vWhere: String;
  vField: String;
  Data,Movimento: TDate;
begin
  if edLivro.Text='' then
  begin
      GR.Aviso('INFORME O LIVRO!');
      edLivro.SetFocus;
      Exit;
  end;

  if edFolha.Text='' then
  begin
      GR.Aviso('INFORME A FOLHA!');
      edFolha.SetFocus;
      Exit;
  end;

  PF.Aguarde(True);
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;

  if dm.vLivroConvenio then
    vWhere:='((DT_PAGAMENTO BETWEEN :D1 AND :D2) OR (DT_SUSTADO BETWEEN :D1 AND :D2)) AND (CONVENIO=''S'') AND (STATUS=''CANCELADO'' OR STATUS=''SUSTADO'') ORDER BY DT_PROTOCOLO,PROTOCOLO,LIVRO_PROTOCOLO,FOLHA_PROTOCOLO'
      else vwhere:='DT_PROTOCOLO BETWEEN :D1 AND :D2 ORDER BY DT_PROTOCOLO,PROTOCOLO,LIVRO_PROTOCOLO,FOLHA_PROTOCOLO';

  Q.SQL.Add('SELECT ID_ATO,NUMERO_TITULO,APRESENTANTE,DEVEDOR,VALOR_TITULO,SALDO_TITULO,DT_VENCIMENTO,PROTOCOLO,SALDO_PROTESTO,'+
            'TOTAL,DT_TITULO,DT_PROTOCOLO,DT_PRAZO,TIPO_TITULO,SACADOR,CEDENTE,STATUS,FOLHA_PROTOCOLO,DT_REGISTRO,DT_PAGAMENTO,'+
            'TIPO_SUSTACAO,DT_SUSTADO,DT_RETIRADO,TIPO_ENDOSSO,CPF_CNPJ_DEVEDOR,LIVRO_PROTOCOLO,DT_DEVOLVIDO,DT_DEFINITIVA,'+
            'DT_RETORNO_PROTESTO,CONVENIO,AVISTA FROM TITULOS WHERE '+vWhere);

  GR.CriarForm(TFAssinatura,FAssinatura);

  vLivro    :=StrToInt(edLivro.Text);
  vFolha    :=StrToInt(edFolha.Text);

  Application.CreateForm(TFQuickLivroProtocolo1,FQuickLivroProtocolo1);
  with FQuickLivroProtocolo1 do
  begin
      qrTitulo2.Caption:=qrTitulo1.Caption;

      RX.Close;
      RX.Open;
      Data:=edInicio.Date;
      repeat
          Q.Close;
          Q.ParamByName('D1').AsDate:=Data;
          Q.ParamByName('D2').AsDate:=Data;
          Q.Open;
          if Q.IsEmpty then
          begin
              for I := 1 to 5 do
              begin
                  RX.Append;
                  if I=3 then
                  RXIdAto.AsInteger:=-1;
                  RXDataTermo.AsDateTime:=Data;
                  RXVazio.AsBoolean:=True;
                  RX.Post;
              end;

              Data:=Data+1;

              if Data>edFim.Date then
              begin
                  qkLivro.Prepare;
                  FolhasRelatorio(qkLivro.PageNumber);
                  qkLivro.Preview;
                  RX.Close;
                  RX.Open;
              end;
          end
          else
          begin
              if dm.vLivroConvenio then
                vField:=IfThen(Q.FieldByName('STATUS').AsString='CANCELADO','DT_PAGAMENTO','DT_SUSTADO')
                  else vField:='DT_PROTOCOLO';

              Q.First;
              Movimento:=Q.FieldByName(vField).AsDateTime;

              while not Q.Eof do
              begin
                  SalvarRX;

                  Q.Next;

                  if vFolha>StrToInt(dm.ValorParametro(86)) then
                  begin
                      Inc(vLivro);
                      vFolha:=1;
                      RX.Edit;
                      RXEncerramento.AsBoolean:=True;
                      RX.Post;
                      qkLivro.Prepare;
                      FolhasRelatorio(qkLivro.PageNumber);
                      qkLivro.Preview;
                      RX.Close;
                      RX.Open;
                  end;

                  if dm.vLivroConvenio then
                    vField:=IfThen(Q.FieldByName('STATUS').AsString='CANCELADO','DT_PAGAMENTO','DT_SUSTADO')
                      else vField:='DT_PROTOCOLO';

                  if (Q.FieldByName(vField).AsDateTime<>Movimento) or (Q.Eof) then
                  begin
                      Movimento:=Q.FieldByName(vField).AsDateTime;
                      RX.Edit;
                      RXEncerramento.AsBoolean:=True;
                      RX.Post;
                      qkLivro.Prepare;
                      FolhasRelatorio(qkLivro.PageNumber);
                      qkLivro.Preview;
                      RX.Close;
                      RX.Open;
                  end;
              end;
              Data:=Data+1;
          end;
      until (Data>edFim.Date);

      Q.Close;
      Q.Free;
      Free;
  end;

  edLivro.Text:=IntToStr(vLivro);
  edFolha.Text:=IntToStr(vFolha);

  GR.ExecutarSQLDAC('UPDATE CONTROLE SET VALOR='+edLivro.Text+' WHERE SIGLA=''LPR''',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE CONTROLE SET VALOR='+edFolha.Text+' WHERE SIGLA=''FPR''',dm.conSISTEMA);
  PF.Aguarde(False);
end;

procedure TFLivroProtocolo.Modelo2;
var
  vWhere: String;
  vField: String;
  Data{,Movimento}: TDate;
begin
  if edLivro.Text='' then
  begin
      GR.Aviso('INFORME O LIVRO!');
      edLivro.SetFocus;
      Exit;
  end;

  if edFolha.Text='' then
  begin
      GR.Aviso('INFORME A FOLHA!');
      edFolha.SetFocus;
      Exit;
  end;

  PF.Aguarde(True);
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;

  if dm.vLivroConvenio then
    vWhere:='((DT_PAGAMENTO BETWEEN :D1 AND :D2) OR (DT_SUSTADO BETWEEN :D1 AND :D2)) AND (CONVENIO=''S'') AND (STATUS=''CANCELADO'' OR STATUS=''SUSTADO'') ORDER BY DT_PROTOCOLO,PROTOCOLO,LIVRO_PROTOCOLO,FOLHA_PROTOCOLO'
      else vwhere:='DT_PROTOCOLO BETWEEN :D1 AND :D2 ORDER BY DT_PROTOCOLO,PROTOCOLO,LIVRO_PROTOCOLO,FOLHA_PROTOCOLO';

  Q.SQL.Add('SELECT ID_ATO,NUMERO_TITULO,APRESENTANTE,DEVEDOR,VALOR_TITULO,SALDO_TITULO,DT_VENCIMENTO,PROTOCOLO,SALDO_PROTESTO,'+
            'TOTAL,DT_TITULO,DT_PROTOCOLO,DT_PRAZO,TIPO_TITULO,SACADOR,CEDENTE,STATUS,FOLHA_PROTOCOLO,DT_REGISTRO,DT_PAGAMENTO,'+
            'TIPO_SUSTACAO,DT_SUSTADO,DT_RETIRADO,TIPO_ENDOSSO,CPF_CNPJ_DEVEDOR,LIVRO_PROTOCOLO,DT_DEVOLVIDO,DT_DEFINITIVA,'+
            'DT_RETORNO_PROTESTO,CONVENIO,AVISTA FROM TITULOS WHERE '+vWhere);

  GR.CriarForm(TFAssinatura,FAssinatura);

  vLivro    :=StrToInt(edLivro.Text);
  vFolha    :=StrToInt(edFolha.Text);

  Application.CreateForm(TFQuickLivroProtocolo1,FQuickLivroProtocolo1);
  with FQuickLivroProtocolo1 do
  begin
      qrTitulo2.Caption:=qrTitulo1.Caption;

      RX.Close;
      RX.Open;
      Data:=edInicio.Date;
      repeat
          Q.Close;
          Q.ParamByName('D1').AsDate:=Data;
          Q.ParamByName('D2').AsDate:=Data;
          Q.Open;
          if Q.IsEmpty then
          begin
              {for I := 1 to 5 do
              begin}
                  RX.Append;
                  //if I=3 then
                  RXIdAto.AsInteger:=-1;
                  RXDataTermo.AsDateTime:=Data;
                  RXVazio.AsBoolean:=True;
                  RX.Post;
              {end;}

              Data:=Data+1;

              {if Data>edFim.Date then
              begin
                  qkLivro.Prepare;
                  FolhasRelatorio(qkLivro.PageNumber);
                  qkLivro.Preview;
                  RX.Close;
                  RX.Open;
              end;}
          end
          else
          begin
              if dm.vLivroConvenio then
                vField:=IfThen(Q.FieldByName('STATUS').AsString='CANCELADO','DT_PAGAMENTO','DT_SUSTADO')
                  else vField:='DT_PROTOCOLO';

              Q.First;
              //Movimento:=Q.FieldByName(vField).AsDateTime;

              while not Q.Eof do
              begin
                  SalvarRX;

                  Q.Next;

                  {if vFolha>StrToInt(dm.ValorParametro(86)) then
                  begin
                      Inc(vLivro);
                      vFolha:=1;
                      RX.Edit;
                      RXEncerramento.AsBoolean:=True;
                      RX.Post;
                      qkLivro.Prepare;
                      FolhasRelatorio(qkLivro.PageNumber);
                      qkLivro.Preview;
                      RX.Close;
                      RX.Open;
                  end;}

                  if dm.vLivroConvenio then
                    vField:=IfThen(Q.FieldByName('STATUS').AsString='CANCELADO','DT_PAGAMENTO','DT_SUSTADO')
                      else vField:='DT_PROTOCOLO';

                  {if (Q.FieldByName(vField).AsDateTime<>Movimento) or (Q.Eof) then
                  begin
                      Movimento:=Q.FieldByName(vField).AsDateTime;
                      RX.Edit;
                      RXEncerramento.AsBoolean:=True;
                      RX.Post;
                      qkLivro.Prepare;
                      FolhasRelatorio(qkLivro.PageNumber);
                      qkLivro.Preview;
                      RX.Close;
                      RX.Open;
                  end;}
              end;
              Data:=Data+1;
          end;
      until (Data>edFim.Date);

      qkLivro.Prepare;
      FolhasRelatorio(qkLivro.PageNumber);
      qkLivro.Preview;

      Q.Close;
      Q.Free;
      Free;
  end;

  edLivro.Text:=IntToStr(vLivro);
  edFolha.Text:=IntToStr(vFolha);

  GR.ExecutarSQLDAC('UPDATE CONTROLE SET VALOR='+edLivro.Text+' WHERE SIGLA=''LPR''',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE CONTROLE SET VALOR='+edFolha.Text+' WHERE SIGLA=''FPR''',dm.conSISTEMA);
  PF.Aguarde(False);
end;

end.
