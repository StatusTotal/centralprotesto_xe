object FConsultaPortador: TFConsultaPortador
  Left = 256
  Top = 185
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Portadores'
  ClientHeight = 276
  ClientWidth = 559
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 15
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 559
    Height = 40
    Align = alTop
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object edLocalizar: TsEdit
      Left = 70
      Top = 10
      Width = 275
      Height = 22
      Hint = 'DIGITE O NOME DO PORTADOR E PRESSIONE ENTER PARA LOCALIZAR'
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnKeyPress = edLocalizarKeyPress
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Localizar'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object btSelecionar: TsBitBtn
      Left = 374
      Top = 8
      Width = 87
      Height = 25
      Hint = 'SELECIONAR PORTADOR'
      Caption = 'Selecionar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btSelecionarClick
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 3
      Images = dm.Imagens
    end
    object btVoltar: TsBitBtn
      Left = 466
      Top = 7
      Width = 75
      Height = 25
      Hint = 'VOLTAR SEM SELECIONAR PORTADOR'
      Caption = 'Voltar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btVoltarClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777777777777777777777777777777777777777777777777777777777777
        7777777487777777777777847777744444777747777777444477774777777774
        4477774777777747447777847777447774777778444477777777777777777777
        7777777777777777777777777777777777777777777777777777}
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object GridPortadores: TwwDBGrid
    Left = 0
    Top = 40
    Width = 559
    Height = 236
    Selected.Strings = (
      'CODIGO'#9'9'#9'C'#243'digo'#9'F'
      'NOME'#9'77'#9'Nome'#9'F')
    IniAttributes.Delimiter = ';;'
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    BorderStyle = bsNone
    DataSource = dsPortadores
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Arial'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    OnDblClick = GridPortadoresDblClick
    OnKeyPress = GridPortadoresKeyPress
  end
  object dsPortadores: TDataSource
    DataSet = dm.Portadores
    Left = 48
    Top = 96
  end
end
