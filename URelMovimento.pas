unit URelMovimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, Buttons, sBitBtn, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, sBevel, sRadioButton, DB, DBCtrls,
  sDBLookupComboBox, sCheckBox, sEdit;

type
  TFRelMovimento = class(TForm)
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    P1: TsPanel;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    lkPortador: TsDBLookupComboBox;
    dsPortador: TDataSource;
    Rb3: TsRadioButton;
    Rb4: TsRadioButton;
    Rb5: TsRadioButton;
    Rb6: TsRadioButton;
    Rb8: TsRadioButton;
    sBevel1: TsBevel;
    Rb1: TsRadioButton;
    edProtocolo: TsEdit;
    edCodigo: TsEdit;
    Rb9: TsRadioButton;
    Rb10: TsRadioButton;
    Rb11: TsRadioButton;
    procedure btSairClick(Sender: TObject);
    procedure btVisualizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure lkPortadorKeyPress(Sender: TObject; var Key: Char);
    procedure edProtocoloKeyPress(Sender: TObject; var Key: Char);
    procedure edCodigoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lkPortadorCloseUp(Sender: TObject);
    procedure lkPortadorClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelMovimento: TFRelMovimento;

implementation

uses UQuickMovimento, UDM, UPF;

{$R *.dfm}

procedure TFRelMovimento.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFRelMovimento.btVisualizarClick(Sender: TObject);
var
  Query: String;
begin
  PF.Aguarde(True);
  Application.CreateForm(TFQuickMovimento,FQuickMovimento);

  Query:='SELECT APRESENTANTE,PROTOCOLO,DEVEDOR,CPF_CNPJ_DEVEDOR,TARIFA_BANCARIA,TIPO_TITULO,NUMERO_TITULO,DT_PROTOCOLO,DT_REGISTRO,'+
         'DT_PAGAMENTO,VALOR_TITULO,SACADOR,DT_SUSTADO,DT_RETIRADO,DT_DEVOLVIDO,TOTAL,SELO_REGISTRO,SELO_PAGAMENTO,STATUS,SALDO_PROTESTO,'+
         'CONVENIO,DISTRIBUICAO,SALDO_TITULO,ALEATORIO_PROTESTO,ALEATORIO_SOLUCAO FROM TITULOS ';

  with FQuickMovimento do
  begin
      Titulos.Close;
      lbPeriodo.Caption:='Per�odo: '+edInicio.Text+' - '+edFim.Text;

      {APONTADOS}
      if Rb1.Checked then
      begin
          if lkPortador.Text='' then
            Titulos.SQL.Text:=Query+'WHERE DT_PROTOCOLO BETWEEN :D1 AND :D2 ORDER BY PROTOCOLO'
              else Titulos.SQL.Text:=Query+'WHERE (CODIGO_APRESENTANTE=:C OR APRESENTANTE=:N) AND DT_PROTOCOLO BETWEEN :D1 AND :D2 ORDER BY PROTOCOLO';
          qrData.DataField:='DT_PROTESTO';
          qrSelo.DataField:='';
          qrAleatorio.DataField:='';
          qrRelacao.Caption:='RELA��O DE T�TULOS APONTADOS';
      end;

      {PROTESTADOS}
      if Rb3.Checked then
      begin
          if lkPortador.Text='' then
            Titulos.SQL.Text:=Query+'WHERE DT_REGISTRO BETWEEN :D1 AND :D2 AND PROTESTADO='''+'S'+''' ORDER BY PROTOCOLO'
              else Titulos.SQL.Text:=Query+'WHERE (CODIGO_APRESENTANTE=:C OR APRESENTANTE=:N) AND DT_REGISTRO BETWEEN :D1 AND :D2 AND PROTESTADO='''+'S'+''' ORDER BY PROTOCOLO';
          qrData.DataField:='DT_REGISTRO';
          qrSelo.DataField:='SELO_REGISTRO';
          qrAleatorio.DataField:='ALEATORIO_PROTESTO';
          qrRelacao.Caption:='RELA��O DE T�TULOS PROTESTADOS';
      end;

      {PAGOS}
      if Rb4.Checked then
      begin
          if lkPortador.Text='' then
            Titulos.SQL.Text:=Query+'WHERE DT_PAGAMENTO BETWEEN :D1 AND :D2 AND STATUS='''+'PAGO'+''' ORDER BY PROTOCOLO'
              else Titulos.SQL.Text:=Query+'WHERE (CODIGO_APRESENTANTE=:C OR APRESENTANTE=:N) AND DT_PAGAMENTO BETWEEN :D1 AND :D2 AND STATUS='''+'PAGO'+''' ORDER BY PROTOCOLO';
          qrData.DataField:='DT_PAGAMENTO';
          qrSelo.DataField:='SELO_PAGAMENTO';
          qrAleatorio.DataField:='ALEATORIO_SOLUCAO';
          qrRelacao.Caption:='RELA��O DE T�TULOS PAGOS';
      end;

      {CANCELADOS}
      if Rb5.Checked then
      begin
          if lkPortador.Text='' then
            Titulos.SQL.Text:=Query+'WHERE DT_PAGAMENTO BETWEEN :D1 AND :D2 AND STATUS='''+'CANCELADO'+''' ORDER BY PROTOCOLO'
              else Titulos.SQL.Text:=Query+'WHERE (CODIGO_APRESENTANTE=:C OR APRESENTANTE=:N) AND DT_PAGAMENTO BETWEEN :D1 AND :D2 AND STATUS='''+'CANCELADO'+''' ORDER BY PROTOCOLO';
          qrData.DataField:='DT_PAGAMENTO';
          qrSelo.DataField:='SELO_PAGAMENTO';
          qrAleatorio.DataField:='ALEATORIO_SOLUCAO';
          qrRelacao.Caption:='RELA��O DE T�TULOS CANCELADOS';
      end;

      {RETIRADOS}
      if Rb6.Checked then
      begin
          if lkPortador.Text='' then
            Titulos.SQL.Text:=Query+'WHERE DT_RETIRADO BETWEEN :D1 AND :D2 AND STATUS='''+'RETIRADO'+''' ORDER BY PROTOCOLO'
              else Titulos.SQL.Text:=Query+'WHERE (CODIGO_APRESENTANTE=:C OR APRESENTANTE=:N) AND DT_RETIRADO BETWEEN :D1 AND :D2 AND STATUS='''+'RETIRADO'+''' ORDER BY PROTOCOLO';
          qrData.DataField:='DT_RETIRADO';
          qrSelo.DataField:='SELO_PAGAMENTO';
          qrAleatorio.DataField:='ALEATORIO_SOLUCAO';
          qrRelacao.Caption:='RELA��O DE T�TULOS RETIRADOS';
      end;

      {DEVOLVIDOS}
      if Rb8.Checked then
      begin
          if lkPortador.Text='' then
            Titulos.SQL.Text:=Query+'WHERE DT_DEVOLVIDO BETWEEN :D1 AND :D2 AND STATUS='''+'DEVOLVIDO'+''' ORDER BY PROTOCOLO'
              else Titulos.SQL.Text:=Query+'WHERE (CODIGO_APRESENTANTE=:C OR APRESENTANTE=:N) AND DT_DEVOLVIDO BETWEEN :D1 AND :D2 AND STATUS='''+'DEVOLVIDO'+''' ORDER BY PROTOCOLO';
          qrData.DataField:='DT_DEVOLVIDO';
          qrSelo.DataField:='SELO_PAGAMENTO';
          qrAleatorio.DataField:='ALEATORIO_SOLUCAO';
          qrRelacao.Caption:='RELA��O DE T�TULOS DEVOLVIDOS';
      end;

      {INTIMADO PESSOAL}
      if Rb9.Checked then
      begin
          if lkPortador.Text='' then
            Titulos.SQL.Text:=Query+'WHERE DT_INTIMACAO BETWEEN :D1 AND :D2 AND STATUS='''+'INTIMADO PESSOAL'+''' ORDER BY PROTOCOLO'
              else Titulos.SQL.Text:=Query+'WHERE (CODIGO_APRESENTANTE=:C OR APRESENTANTE=:N) AND DT_INTIMACAO BETWEEN :D1 AND :D2 AND STATUS='''+'INTIMADO PESSOAL'+''' ORDER BY PROTOCOLO';
          qrData.DataField:='DT_INTIMACAO';
          qrSelo.DataField:='';
          qrAleatorio.DataField:='';
          qrRelacao.Caption:='RELA��O DE T�TULOS INTIMADOS';
      end;

      {INTIMADO EDITAL}
      if Rb10.Checked then
      begin
          if lkPortador.Text='' then
            Titulos.SQL.Text:=Query+'WHERE DT_INTIMACAO BETWEEN :D1 AND :D2 AND STATUS='''+'INTIMADO EDITAL'+''' ORDER BY PROTOCOLO'
              else Titulos.SQL.Text:=Query+'WHERE (CODIGO_APRESENTANTE=:C OR APRESENTANTE=:N) AND DT_INTIMACAO BETWEEN :D1 AND :D2 AND STATUS='''+'INTIMADO EDITAL'+''' ORDER BY PROTOCOLO';
          qrData.DataField:='DT_INTIMACAO';
          qrSelo.DataField:='';
          qrAleatorio.DataField:='';
          qrRelacao.Caption:='RELA��O DE T�TULOS INTIMADOS';
      end;

      {DEVOLVIDOS}
      if Rb11.Checked then
      begin
          if lkPortador.Text='' then
            Titulos.SQL.Text:=Query+'WHERE DT_SUSTADO BETWEEN :D1 AND :D2 AND STATUS='''+'SUSTADO'+''' ORDER BY PROTOCOLO'
              else Titulos.SQL.Text:=Query+'WHERE (CODIGO_APRESENTANTE=:C OR APRESENTANTE=:N) AND DT_SUSTADO BETWEEN :D1 AND :D2 AND STATUS='''+'SUSTADO'+''' ORDER BY PROTOCOLO';
          qrData.DataField:='DT_SUSTADO';
          qrSelo.DataField:='SELO_PAGAMENTO';
          qrAleatorio.DataField:='ALEATORIO_SOLUCAO';
          qrRelacao.Caption:='RELA��O DE T�TULOS SUSTADOS';
      end;

      qrPortador.Caption:=lkPortador.Text;
      if lkPortador.Text<>'' then
      begin
          Titulos.Params.ParamByName('C').AsString:=dm.PortadoresCODIGO.AsString;
          Titulos.Params.ParamByName('N').AsString:=dm.PortadoresNOME.AsString;
      end;
      Titulos.Params.ParamByName('D1').AsDate:=edInicio.Date;
      Titulos.Params.ParamByName('D2').AsDate:=edFim.Date;
      Titulos.Open;

      Titulos.Filtered:=False;
      if edProtocolo.Text<>'' then
      begin
          Titulos.Filter:='PROTOCOLO='+QuotedStr(edProtocolo.Text);
          Titulos.Filtered:=True;
      end;

      Relatorio.Preview;
      Free;
  end;
end;

procedure TFRelMovimento.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFRelMovimento.FormCreate(Sender: TObject);
begin
  dm.Portadores.Close;
  dm.Portadores.CommandText:='SELECT * FROM PORTADORES ORDER BY NOME';
  dm.Portadores.Open;
  dm.Portadores.Filtered:=False;
end;

procedure TFRelMovimento.lkPortadorKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key=#8 then lkPortador.KeyValue:=Null;
end;

procedure TFRelMovimento.edProtocoloKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in [#8,#13,'0'..'9']) then key:=#0;
end;

procedure TFRelMovimento.edCodigoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Trim(edCodigo.Text)<>'' then
  begin
      if dm.Portadores.Locate('CODIGO',edCodigo.Text,[]) then
        lkPortador.KeyValue:=dm.PortadoresID_PORTADOR.AsInteger;
  end
  else
    lkPortador.KeyValue:=Null;
end;

procedure TFRelMovimento.lkPortadorCloseUp(Sender: TObject);
begin
  if lkPortador.Text<>'' then
  edCodigo.Text:=dm.PortadoresCODIGO.AsString
  else edCodigo.Clear;
end;

procedure TFRelMovimento.lkPortadorClick(Sender: TObject);
begin
  if lkPortador.Text<>'' then
  edCodigo.Text:=dm.PortadoresCODIGO.AsString
  else edCodigo.Clear;
end;

end.
