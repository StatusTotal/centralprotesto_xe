unit UEntrada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, sDBDateEdit, DBCtrls, sDBEdit, sDBComboBox, StdCtrls,
  Buttons, sBitBtn, sTooledit, sDBLookupComboBox, Mask, sMaskEdit,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, acPNG, ExtCtrls,
  sSpeedButton, sComboBox, sEdit, sGroupBox, RxDBComb, FMTBcd, SqlExpr, sCheckBox, sDBCheckBox, sPanel, jpeg, sLabel,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  System.StrUtils, Vcl.Grids, vcl.wwdbigrd, vcl.wwdbgrid;

type
  TFEntrada = class(TForm)
    GbTitulo: TsGroupBox;
    ImTabela: TImage;
    lkEspecie: TsDBLookupComboBox;
    GbPartes: TsGroupBox;
    ImAviso1: TImage;
    sbLocalizar: TsSpeedButton;
    ImOk1: TImage;
    ImAviso2: TImage;
    sbDevedor: TsSpeedButton;
    ImOk2: TImage;
    Image1: TImage;
    sbSacador: TsSpeedButton;
    cbTipoApr: TsDBComboBox;
    edDocApr: TsDBEdit;
    edCodigo: TsDBEdit;
    edApresentante: TsDBEdit;
    btSalvar: TsBitBtn;
    btCancelar: TsBitBtn;
    dsTitulos: TDataSource;
    cbTipoProtesto: TRxDBComboBox;
    Label1: TLabel;
    edNumero: TsDBEdit;
    edDataTitulo: TsDBDateEdit;
    edDataVencimento: TsDBDateEdit;
    cbConvenio: TRxDBComboBox;
    Label2: TLabel;
    dsTipos: TDataSource;
    dsRXPortador: TDataSource;
    dsRXDevedor: TDataSource;
    edSacador: TsDBEdit;
    edCedente: TsDBEdit;
    edValor: TsDBEdit;
    edCustas: TsDBEdit;
    edSaldoTotal: TsDBEdit;
    edBanco: TsDBEdit;
    edAgencia: TsDBEdit;
    edConta: TsDBEdit;
    P1: TsPanel;
    ckAceite: TsDBCheckBox;
    sDBCheckBox1: TsDBCheckBox;
    sDBCheckBox2: TsDBCheckBox;
    sbPortador: TsSpeedButton;
    PA: TsPanel;
    Label4: TLabel;
    Label5: TLabel;
    Image3: TImage;
    Memo1: TMemo;
    edPraca: TsDBEdit;
    edDataEntrada: TsDBDateEdit;
    ckAntigo: TsCheckBox;
    cbTipoCobranca: TRxDBComboBox;
    Label3: TLabel;
    edProtocolo: TsDBEdit;
    sbDevedores: TsSpeedButton;
    cbTipoDev: TsComboBox;
    edDocDev: TsMaskEdit;
    edDevedor: TsEdit;
    edTelefone: TsMaskEdit;
    Tabela: TFDQuery;
    Faixa: TFDQuery;
    FaixaCOD: TIntegerField;
    FaixaEMOL: TFloatField;
    FaixaFETJ: TFloatField;
    FaixaFUND: TFloatField;
    FaixaFUNP: TFloatField;
    FaixaMUTUA: TFloatField;
    FaixaACOTERJ: TFloatField;
    FaixaDISTRIB: TFloatField;
    FaixaTOT: TFloatField;
    FaixaTITULO: TStringField;
    TabelaORDEM: TIntegerField;
    TabelaANO: TIntegerField;
    TabelaVAI: TStringField;
    TabelaTAB: TStringField;
    TabelaITEM: TStringField;
    TabelaSUB: TStringField;
    TabelaDESCR: TStringField;
    TabelaVALOR: TFloatField;
    TabelaTEXTO: TStringField;
    edtEndDevedor: TsEdit;
    edtBairroDevedor: TsEdit;
    medCEPDevedor: TsMaskEdit;
    cbUFDevedor: TsComboBox;
    edtNossoNumero: TsDBEdit;
    lblServentiaAgregada: TLabel;
    edtServentiaAgregada: TsDBEdit;
    dsMunicipios: TDataSource;
    lcbMunDevedor: TsDBLookupComboBox;
    edAR: TsDBEdit;
    lbAR: TsLabel;
    edSaldo: TsDBEdit;
    dbgDevedores: TwwDBGrid;
    sDBCheckBox3: TsDBCheckBox;
    procedure btSalvarClick(Sender: TObject);
    procedure cbTipoAprChange(Sender: TObject);
    procedure edDocAprClick(Sender: TObject);
    procedure edDocAprChange(Sender: TObject);
    procedure edDocAprExit(Sender: TObject);
    procedure sbPortadorClick(Sender: TObject);
    procedure edCodigoExit(Sender: TObject);
    procedure cbTipoDevChange(Sender: TObject);
    procedure edDocDevChange(Sender: TObject);
    procedure edDocDevClick(Sender: TObject);
    procedure edDocDevExit(Sender: TObject);
    procedure sbLocalizarClick(Sender: TObject);
    procedure sbDevedorClick(Sender: TObject);
    procedure edValorExit(Sender: TObject);
    procedure edValorClick(Sender: TObject);
    procedure edCustasClick(Sender: TObject);
    procedure edSaldoTotalClick(Sender: TObject);
    procedure lkEspecieClick(Sender: TObject);
    procedure lkEspecieKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btCancelarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure edSacadorExit(Sender: TObject);
    procedure sbSacadorClick(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure ImTabelaClick(Sender: TObject);
    procedure lbARClick(Sender: TObject);
    procedure ckAntigoClick(Sender: TObject);
    procedure sbDevedoresClick(Sender: TObject);
    procedure edApresentanteExit(Sender: TObject);
    procedure medCEPDevedorExit(Sender: TObject);
    procedure edtServentiaAgregadaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edSaldoClick(Sender: TObject);
    procedure edSaldoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEntrada: TFEntrada;

implementation

uses UPF, UDM, UDadosPortador, UConsultaPortador, UCadastro, UQuickRecibo1,
  USacador, UPrincipal, UParametros, UCustas, UGeral, UGDM, UDevedores,
  Math;

{$R *.dfm}

procedure TFEntrada.btSalvarClick(Sender: TObject);
var
  Protocolo, sValor, sMsgErro, sDias: String;
  vIdReservado, iIdAto: Integer;
begin
  iIdAto := 0;

  sValor   := '';
  sMsgErro := '';
  sDias    := '';

  if (ckAntigo.Checked) and (edProtocolo.Text='') then
  begin
      GR.Aviso('INFORME O N� DO PROTOCOLO!');
      edProtocolo.SetFocus;
      Exit;
  end;

  if edDocApr.Text='' then
  begin
      GR.Aviso('INFORME O DOCUMENTO DO APRESENTANTE!');
      edDocApr.SetFocus;
      Exit;
  end;    

  if edApresentante.Text='' then
  begin
      GR.Aviso('INFORME O NOME DO APRESENTANTE!');
      edApresentante.SetFocus;
      Exit;
  end;

  if edDocDev.Text='' then
  begin
      GR.Aviso('INFORME O DOCUMENTO DO DEVEDOR!');
      edDocDev.SetFocus;
      Exit;
  end;

  if edDevedor.Text='' then
  begin
      GR.Aviso('INFORMA O NOME DO DEVEDOR!');
      edDevedor.SetFocus;
      Exit;
  end;

  if dm.TitulosVALOR_TITULO.AsFloat=0 then
  begin
      GR.Aviso('INFORME O VALOR DO T�TULO!');
      edValor.SetFocus;
      Exit;
  end;

  if dm.TitulosSALDO_TITULO.AsFloat=0 then
  begin
      GR.Aviso('INFORME O SALDO DO T�TULO!');
      edSaldo.SetFocus;
      Exit;
  end;

  try
    if dm.conSISTEMA.Connected then
      dm.conSISTEMA.StartTransaction;

    if dm.conCENTRAL.Connected then
      dm.conCENTRAL.StartTransaction;

    iIdAto := dm.IdAtual('ID_ATO','S');

    dm.TitulosID_ATO.AsInteger              := iIdAto;
    dm.TitulosRECIBO.AsInteger              :=StrToInt(dm.ValorParametro(11));
    dm.AtualizarParametro(11,IntToStr(dm.TitulosRECIBO.AsInteger+1));

    if not ckAntigo.Checked then
    begin
      if dm.ValorParametro(16)='S' then
      begin
        dm.TitulosDT_PROTOCOLO.AsDateTime   :=edDataEntrada.Date;
        dm.TitulosDT_PRAZO.AsDateTime       :=PF.DataFinalFeriados(dm.TitulosDT_PROTOCOLO.AsDateTime,StrToInt(dm.ValorParametro(5)));
        dm.TitulosPROTOCOLO.AsInteger       :=dm.ValorAtual('NPR','S');
        dm.TitulosLIVRO_PROTOCOLO.AsInteger :=dm.ValorAtual('LPR','S');
        dm.TitulosFOLHA_PROTOCOLO.AsInteger :=dm.ValorAtual('FPR','S');
        dm.TitulosSTATUS.AsString           :='Aceito';  //APONTADO
        Protocolo                           :=dm.TitulosPROTOCOLO.AsString;
      end
      else
        dm.TitulosSTATUS.AsString:='ENTRADA';
    end
    else
    begin
      dm.TitulosDT_PROTOCOLO.AsDateTime:=edDataEntrada.Date;
      dm.TitulosSTATUS.AsString:='Aceito';  //APONTADO
    end;

    if edDataVencimento.Date <> 0 then
      dm.TitulosAVISTA.AsString := 'N'
    else
    begin
      if PF.RetornarEspecie(dm.TitulosTIPO_TITULO.AsString) in [19, 20, 21, 22, 23, 24, 27] then
      begin
        sDias := UpperCase(InputBox('� VISTA', PChar('1 = A um dia da vista' +
                                                     '30 = A 30 dias da vista' +
                                                     'O = Outro'), '1'));

        if (PF.PegarNumero(sDias) = 1) or
          (PF.PegarNumero(sDias) = 30) then
        begin
          dm.TitulosDIAS_AVISTA.AsInteger := StrToInt(sDias);
          dm.TitulosAVISTA.AsString       := 'S';
        end
        else
        begin
          dm.TitulosDIAS_AVISTA.AsInteger := 0;
          dm.TitulosAVISTA.AsString       := 'S';
        end;
      end
      else
        dm.TitulosDIAS_AVISTA.AsInteger := 0;

      dm.TitulosAVISTA.AsString := 'S';
    end;

    {GARANTINDO OS DADOS DO PRIMEIRO DEVEDOR}
    if dm.RxDevedor.IsEmpty then
      dm.RxDevedor.Append
    else
    begin
      dm.RxDevedor.Locate('ORDEM',1,[]);
      dm.RxDevedor.Edit;
    end;

    dm.RxDevedorORDEM.AsInteger     := 1;
    dm.RxDevedorNOME.AsString       := edDevedor.Text;
    dm.RxDevedorTIPO.AsString       := cbTipoDev.Text;
    dm.RxDevedorDOCUMENTO.AsString  := edDocDev.Text;
    dm.RxDevedorTELEFONE.AsString   := edTelefone.Text;
    dm.RxDevedorIGNORADO.AsString   := 'N';
    dm.RxDevedorENDERECO.AsString   := edtEndDevedor.Text;
    dm.RxDevedorBAIRRO.AsString     := edtBairroDevedor.Text;
    dm.RxDevedorMUNICIPIO.AsString  := lcbMunDevedor.Text;
    dm.RxDevedorUF.AsString         := cbUFDevedor.Text;
    dm.RxDevedorCEP.AsString        := medCEPDevedor.Text;
    dm.RxDevedor.Post;
    {GARANTINDO OS DADOS DO PRIMEIRO DEVEDOR}

    dm.TitulosTIPO_APRESENTANTE.AsString      :=dm.RxPortadorTIPO.AsString;
    dm.TitulosCPF_CNPJ_APRESENTANTE.AsString  :=dm.RxPortadorDOCUMENTO.AsString;
    dm.TitulosAPRESENTANTE.AsString           :=dm.RxPortadorNOME.AsString;
    dm.TitulosCODIGO_APRESENTANTE.AsString    :=dm.RxPortadorCODIGO.AsString;

    dm.TitulosDEVEDOR.AsString                :=dm.RxDevedorNOME.AsString;
    dm.TitulosTIPO_DEVEDOR.AsString           :=dm.RxDevedorTIPO.AsString;
    dm.TitulosCPF_CNPJ_DEVEDOR.AsString       :=dm.RxDevedorDOCUMENTO.AsString;
    dm.TitulosTIPO_APRESENTACAO.AsString      :='B';
    dm.TitulosANTIGO.AsString                 :=GR.iif(ckAntigo.Checked,'S','N');

    {CUSTAS RECEBEM AS VARI�VEIS}
    dm.TitulosCODIGO.AsInteger                := FaixaCOD.AsInteger;
    dm.TitulosEMOLUMENTOS.AsFloat             := dm.vEmolumentos;
    dm.TitulosFETJ.AsFloat                    := dm.vFetj;
    dm.TitulosFUNDPERJ.AsFloat                := dm.vFundperj;
    dm.TitulosFUNPERJ.AsFloat                 := dm.vFunperj;
    dm.TitulosFUNARPEN.AsFloat                := dm.vFunarpen;
    dm.TitulosPMCMV.AsFloat                   := dm.vPmcmv;
    dm.TitulosISS.AsFloat                     := dm.vIss;
    dm.TitulosMUTUA.AsFloat                   := dm.vMutua;
    dm.TitulosACOTERJ.AsFloat                 := dm.vAcoterj;
    dm.TitulosDISTRIBUICAO.AsFloat            := dm.vDistribuicao;
    dm.TitulosTOTAL.AsFloat                   := dm.vTotal;
    dm.Titulos.Post;

    if dm.Titulos.ApplyUpdates(0) > 0 then
      Raise Exception.Create('ApplyUpdates dm.Titulos');

    {GRAVANDO O DEVEDOR}
    dm.Devedores.Close;
    dm.Devedores.Params[0].AsInteger:=dm.TitulosID_ATO.AsInteger;
    dm.Devedores.Open;

    dm.RxDevedor.DisableControls;
    dm.RxDevedor.First;
    while not dm.RxDevedor.Eof do
    begin
      dm.Devedores.Append;
      dm.DevedoresID_DEVEDOR.AsInteger      :=dm.IdAtual('ID_DEVEDOR','S');
      dm.DevedoresID_ATO.AsInteger          :=dm.TitulosID_ATO.AsInteger;
      dm.DevedoresORDEM.AsInteger           :=dm.RxDevedorORDEM.AsInteger;
      dm.DevedoresNOME.AsString             :=dm.RxDevedorNOME.AsString;
      dm.DevedoresTIPO.AsString             :=dm.RxDevedorTIPO.AsString;
      dm.DevedoresDOCUMENTO.AsString        :=dm.RxDevedorDOCUMENTO.AsString;
      dm.DevedoresIFP_DETRAN.AsString       :=dm.RxDevedorIFP_DETRAN.AsString;
      dm.DevedoresIDENTIDADE.AsString       :=dm.RxDevedorIDENTIDADE.AsString;
      dm.DevedoresORGAO.AsString            :=dm.RxDevedorORGAO.AsString;
      dm.DevedoresENDERECO.AsString         :=dm.RxDevedorENDERECO.AsString;
      dm.DevedoresBAIRRO.AsString           :=dm.RxDevedorBAIRRO.AsString;
      dm.DevedoresMUNICIPIO.AsString        :=dm.RxDevedorMUNICIPIO.AsString;
      dm.DevedoresUF.AsString               :=dm.RxDevedorUF.AsString;
      dm.DevedoresJUSTIFICATIVA.AsString    :=dm.RxDevedorJUSTIFICATIVA.AsString;
      dm.DevedoresIGNORADO.AsString         :=dm.RxDevedorIGNORADO.AsString;
      dm.DevedoresTELEFONE.AsString         :=dm.RxDevedorTELEFONE.AsString;
      dm.DevedoresCEP.AsString              :=dm.RxDevedorCEP.AsString;
      dm.DevedoresDT_EMISSAO.AsDateTime     :=dm.RxDevedorDT_EMISSAO.AsDateTime;

      dm.Devedores.Post;
      if dm.Devedores.ApplyUpdates(0)>0 then
      Raise Exception.Create('ApplyUpdates dm.Devedores');

      dm.RxDevedor.Next;
    end;

    dm.RxDevedor.EnableControls;

    {GRAVANDO O PORTADOR}
    dm.Portadores.Close;
    dm.Portadores.CommandText:='SELECT * FROM PORTADORES WHERE DOCUMENTO='+QuotedStr(dm.RxPortadorDOCUMENTO.AsString);
    dm.Portadores.Open;
    if dm.Portadores.IsEmpty then
    begin
        dm.Portadores.Append;
    end
    else
      dm.Portadores.Edit;

    dm.PortadoresCODIGO.AsString      :=dm.RxPortadorCODIGO.AsString;
    dm.PortadoresNOME.AsString        :=dm.RxPortadorNOME.AsString;
    dm.PortadoresTIPO.AsString        :=dm.RxPortadorTIPO.AsString;
    dm.PortadoresDOCUMENTO.AsString   :=dm.RxPortadorDOCUMENTO.AsString;
    dm.PortadoresENDERECO.AsString    :=dm.RxPortadorENDERECO.AsString;
    dm.PortadoresBANCO.AsString       :=dm.RxPortadorBANCO.AsString;
    dm.PortadoresCONVENIO.AsString    :=dm.RxPortadorCONVENIO.AsString;
    dm.PortadoresCONTA.AsString       :=dm.RxPortadorCONTA.AsString;
    dm.PortadoresOBSERVACAO.AsString  :=dm.RxPortadorOBSERVACAO.AsString;
    dm.PortadoresCRA.AsString         :='N';
      
    dm.Portadores.Post;
    if dm.Portadores.ApplyUpdates(0)>0 then
    Raise Exception.Create('ApplyUpdates dm.Portadores');

    {GRAVANDO O SACADOR}
    if dm.TitulosSACADOR.AsString<>'' then
    begin
        dm.Sacadores.Close;
        dm.Sacadores.CommandText:='SELECT * FROM SACADORES WHERE ID_ATO=:ID_ATO';
        dm.Sacadores.Params[0].AsInteger:=dm.TitulosID_ATO.AsInteger;
        dm.Sacadores.Open;
        if dm.Sacadores.IsEmpty then
        begin
            dm.Sacadores.Append;
            dm.SacadoresID_SACADOR.AsInteger:=dm.IdAtual('ID_SACADOR','S');
        end
        else
          dm.Sacadores.Edit;

        dm.SacadoresID_ATO.AsInteger   :=dm.TitulosID_ATO.AsInteger;
        dm.SacadoresNOME.AsString      :=dm.RxSacadorNOME.AsString;
        dm.SacadoresTIPO.AsString      :=dm.RxSacadorTIPO.AsString;
        dm.SacadoresDOCUMENTO.AsString :=dm.RxSacadorDOCUMENTO.AsString;
        dm.SacadoresENDERECO.AsString  :=dm.RxSacadorENDERECO.AsString;
        dm.SacadoresBAIRRO.AsString    :=dm.RxSacadorBAIRRO.AsString;
        dm.SacadoresCEP.AsString       :=dm.RxSacadorCEP.AsString;
        dm.SacadoresMUNICIPIO.AsString :=dm.RxSacadorMUNICIPIO.AsString;
        dm.SacadoresUF.AsString        :=dm.RxSacadorUF.AsString;

        dm.Sacadores.Post;
        if dm.Sacadores.ApplyUpdates(0)>0 then
        Raise Exception.Create('ApplyUpdates dm.Sacadores');
    end;

    {GRAVANDO AS CUSTAS}
    if not dm.RxCustas.IsEmpty then
    begin
        dm.RxCustas.First;
        while not dm.RxCustas.Eof do
        begin
            dm.Custas.Close;
            dm.Custas.Params[0].AsInteger:=dm.RxCustasID_CUSTA.AsInteger;
            dm.Custas.Open;
            if dm.Custas.IsEmpty then
            begin
                dm.Custas.Append;
                dm.CustasID_CUSTA.AsInteger :=dm.IdAtual('ID_CUSTA','S');
            end
            else
              dm.Custas.Edit;

            dm.CustasID_ATO.AsInteger     :=dm.TitulosID_ATO.AsInteger;
            dm.CustasTABELA.AsString      :=dm.RxCustasTABELA.AsString;
            dm.CustasITEM.AsString        :=dm.RxCustasITEM.AsString;
            dm.CustasSUBITEM.AsString     :=dm.RxCustasSUBITEM.AsString;
            dm.CustasVALOR.AsFloat        :=dm.RxCustasVALOR.AsFloat;
            dm.CustasQTD.AsInteger        :=dm.RxCustasQTD.AsInteger;
            dm.CustasTOTAL.AsFloat        :=dm.RxCustasTOTAL.AsFloat;
            dm.Custas.Post;
            if dm.Custas.ApplyUpdates(0)>0 then
            Raise Exception.Create('ApplyUpdates dm.Custas');

            dm.RxCustas.Next;
        end;
    end;

    if dm.conSISTEMA.InTransaction then
      dm.conSISTEMA.Commit;

    if dm.conCENTRAL.InTransaction then
      dm.conCENTRAL.Commit;

    //Atribuicao de Cartorio de Destino (Serventia Agregada) ao Titulo
    try
      dm.Titulos.Close;
      dm.Titulos.Params.ParamByName('ID').Value := iIdAto;
      dm.Titulos.Open;

      if dm.conSISTEMA.Connected then
        dm.conSISTEMA.StartTransaction;

      dm.ServentiaAgregada.Close;
      dm.ServentiaAgregada.Open;

      dm.mdLinhaArqD.Close;
      dm.mdLinhaArqD.Open;

      dm.mdArqDAux.Close;
      dm.mdArqDAux.Open;

      //Preenche dados do Titulo para Distribuicao
      dm.mdArquivoD.Close;
      dm.mdArquivoD.Open;

      dm.mdArquivoD.Append;
      dm.mdArquivoDID_REGISTRO.AsString          := '1';
      dm.mdArquivoDNUM_COD_PORTADOR.AsString     := GR.Zeros(dm.TitulosCODIGO_APRESENTANTE.AsString, 'D', 3);
      dm.mdArquivoDNOME_PORTADOR.AsString        := dm.TitulosAPRESENTANTE.AsString;
      dm.mdArquivoDCOD_CEDENTE.AsString          := GR.Zeros(dm.TitulosAGENCIA_CEDENTE.AsString, 'D', 15);
      dm.mdArquivoDNOME_CEDENTE.AsString         := PF.Espaco(dm.TitulosCEDENTE.AsString, 'E', 45);

      //Sacador - Inicio
      dm.Sacadores.Close;
      dm.Sacadores.Params.ParamByName('ID_ATO').AsInteger := dm.TitulosID_ATO.AsInteger;
      dm.Sacadores.Open;

      dm.mdArquivoDNOME_SACADOR.AsString         := PF.Espaco(dm.SacadoresNOME.AsString, 'E', 45);
      dm.mdArquivoDNUM_DOC_SACADOR.AsString      := GR.Zeros(dm.SacadoresDOCUMENTO.AsString, 'D', 14);
      dm.mdArquivoDEND_SACADOR.AsString          := PF.Espaco(dm.SacadoresENDERECO.AsString, 'E', 45);
      dm.mdArquivoDCEP_SACADOR.AsString          := GR.Zeros(dm.SacadoresCEP.AsString, 'D', 8);
      dm.mdArquivoDCID_SACADOR.AsString          := PF.Espaco(dm.SacadoresMUNICIPIO.AsString, 'E', 20);
      dm.mdArquivoDUF_SACADOR.AsString           := PF.Espaco(dm.SacadoresUF.AsString, 'E', 2);
      //Sacador - Fim

      dm.mdArquivoDNOSSO_NUMERO.AsString         := GR.Zeros(dm.TitulosNOSSO_NUMERO.AsString, 'D', 15);

      //Especie - Inicio
      dm.Tipos.Close;
      dm.Tipos.Open;

      if dm.Tipos.Locate('CODIGO', dm.TitulosTIPO_TITULO.AsInteger, []) then
        dm.mdArquivoDESPECIE.AsString            := PF.Espaco(dm.TiposSIGLA.AsString, 'E', 3)
      else
        dm.mdArquivoDESPECIE.AsString            := PF.Espaco('', 'E', 3);
      //Especie - FIm

      dm.mdArquivoDNUM_TITULO.AsString           := PF.Espaco(dm.TitulosNUMERO_TITULO.AsString, 'E', 11);
      dm.mdArquivoDDATA_EMISSAO.AsString         := GR.Zeros(GR.PegarNumeroTexto(dm.TitulosDT_TITULO.AsString), 'D', 8);
      dm.mdArquivoDDATA_VENCIMENTO.AsString      := GR.Zeros(GR.PegarNumeroTexto(dm.TitulosDT_VENCIMENTO.AsString), 'D', 8);
      dm.mdArquivoDTIPO_MOEDA.AsString           := '001';  //Real
      dm.mdArquivoDVALOR_TITULO.AsString         := GR.Zeros(GR.PegarNumeroTexto(dm.TitulosVALOR_TITULO.AsString), 'D', 14);
      dm.mdArquivoDSALDO_TITULO.AsString         := GR.Zeros(GR.PegarNumeroTexto(dm.TitulosSALDO_TITULO.AsString), 'D', 14);
      dm.mdArquivoDPRACA_PROTESTO.AsString       := PF.Espaco(dm.TitulosPRACA_PROTESTO.AsString, 'E', 20);
      dm.mdArquivoDTIPO_ENDOSSO.AsString         := PF.Espaco(dm.TitulosTIPO_ENDOSSO.AsString, 'E', 1);
      dm.mdArquivoDINFO_ACEITE.AsString          := PF.Espaco(dm.TitulosACEITE.AsString, 'E', 1);

      //Devedor - Inicio
      dm.Devedores.Close;
      dm.Devedores.Params.ParamByName('ID_ATO').AsInteger := dm.TitulosID_ATO.AsInteger;
      dm.Devedores.Open;

      dm.mdArquivoDNUM_CONTROLE_DEV.AsString     := GR.Zeros(dm.DevedoresORDEM.AsString, 'D', 1);
      dm.mdArquivoDNOME_DEVEDOR.AsString         := PF.Espaco(dm.DevedoresNOME.AsString, 'E', 45);
      dm.mdArquivoDTIPO_ID_DEVEDOR.AsString      := GR.Zeros(dm.DevedoresTIPO.AsString, 'D', 3);
      dm.mdArquivoDNUM_ID_DEVEDOR.AsString       := GR.Zeros(dm.DevedoresID_DEVEDOR.AsString, 'D', 14);
      dm.mdArquivoDNUM_DOC_DEVEDOR.AsString      := GR.Zeros(dm.DevedoresDOCUMENTO.AsString, 'D', 11);
      dm.mdArquivoDEND_DEVEDOR.AsString          := PF.Espaco(dm.DevedoresENDERECO.AsString, 'E', 45);
      dm.mdArquivoDBAIRRO_DEVEDOR.AsString       := PF.Espaco(dm.DevedoresBAIRRO.AsString, 'E', 20);
      dm.mdArquivoDCEP_DEVEDOR.AsString          := GR.Zeros(dm.DevedoresCEP.AsString, 'D', 8);
      dm.mdArquivoDCID_DEVEDOR.AsString          := PF.Espaco(dm.DevedoresMUNICIPIO.AsString, 'E', 20);
      dm.mdArquivoDUF_DEVEDOR.AsString           := PF.Espaco(dm.DevedoresUF.AsString, 'E', 2);
      //Devedor - Fim

      dm.mdArquivoDNUM_PROTOCOLO_DIST.AsString   := GR.Zeros(dm.TitulosPROTOCOLO.AsString, 'D', 10);
      dm.mdArquivoDTIPO_OCORRENCIA.AsString      := GR.Espaco(' ', 'E', 1);
      dm.mdArquivoDDATA_PROTOCOLO.AsString       := GR.Zeros('0', 'D', 8);
      dm.mdArquivoDVALOR_CUSTAS_CART.AsString    := GR.Zeros('0', 'D', 10);
      dm.mdArquivoDDECLARACAO_PORT.AsString      := PF.Espaco(dm.TitulosLETRA.AsString, 'E', 1);
      dm.mdArquivoDDATA_OCORRENCIA.AsString      := GR.Zeros('0', 'D', 8);
      dm.mdArquivoDCOD_IRREGULARIDADE.AsString   := PF.Espaco(' ', 'E', 2);
      dm.mdArquivoDVALOR_CUSTAS_DISTRIB.AsString := GR.Zeros('0', 'D', 10);
      dm.mdArquivoDREG_DISTRIBUICAO.AsString     := GR.Zeros('0', 'D', 6);
      dm.mdArquivoDVALOR_GRAVACAO.AsString       := GR.Zeros('0', 'D', 10);
      dm.mdArquivoDNUM_OPERACAO.AsString         := GR.Zeros('0', 'D', 5);
      dm.mdArquivoDNUM_CONTRATO.AsString         := GR.Zeros('0', 'D', 15);
      dm.mdArquivoDNUM_PARCELA.AsString          := GR.Zeros('0', 'D', 3);
      dm.mdArquivoDTIPO_LETRA_CAMBIO.AsString    := PF.Espaco(' ', 'E', 1);
      dm.mdArquivoDCOMPL_COD_IRREG.AsString      := PF.Espaco(' ', 'E', 8);
      dm.mdArquivoDPROTESTO_FALENCIA.AsString    := PF.Espaco(IfThen(dm.TitulosFINS_FALIMENTARES.AsString = 'S', 'F', ' '), 'E', 1);
      dm.mdArquivoDINSTRUMENTO_PROT.AsString     := PF.Espaco(' ', 'E', 1);
      dm.mdArquivoDVALOR_DDESPESAS.AsString      := GR.Zeros(' ', 'D', 10);
      dm.mdArquivoDCOMPL_REGISTRO.AsString       := PF.Espaco(' ', 'E', 19);

      sValor := FloatToStr(GR.NoRound(dm.TitulosEMOLUMENTOS.AsFloat, 2));
      dm.mdArquivoDEMOLUMENTOS.AsString        := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

      sValor := FloatToStr(GR.NoRound(dm.TitulosFETJ.AsFloat, 2));
      dm.mdArquivoDFETJ.AsString               := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

      sValor := FloatToStr(GR.NoRound(dm.TitulosFUNDPERJ.AsFloat, 2));
      dm.mdArquivoDFUNDPERJ.AsString           := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

      sValor := FloatToStr(GR.NoRound(dm.TitulosFUNPERJ.AsFloat, 2));
      dm.mdArquivoDFUNPERJ.AsString            := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

      sValor := FloatToStr(GR.NoRound(dm.TitulosFUNARPEN.AsFloat, 2));
      dm.mdArquivoDFUNARPEN.AsString           := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

      sValor := FloatToStr(GR.NoRound(dm.TitulosPMCMV.AsFloat, 2));
      dm.mdArquivoDPMCMV.AsString              := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

      sValor := FloatToStr(GR.NoRound(dm.TitulosISS.AsFloat, 2));
      dm.mdArquivoDISS.AsString                := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

      sValor := FloatToStr(GR.NoRound(dm.TitulosMUTUA.AsFloat, 2));
      dm.mdArquivoDMUTUA.AsString              := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

      sValor := FloatToStr(GR.NoRound(dm.TitulosACOTERJ.AsFloat, 2));
      dm.mdArquivoDACOTERJ.AsString            := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

      sValor := FloatToStr(GR.NoRound(dm.TitulosDISTRIBUICAO.AsFloat, 2));
      dm.mdArquivoDDISTRIBUICAO.AsString       := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

      sValor := FloatToStr(GR.NoRound(dm.TitulosTOTAL.AsFloat, 2));
      dm.mdArquivoDTOTAL_CUSTAS.AsString       := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

      dm.mdArquivoDSEQUENCIAL_REG.AsInteger      := 1;
      dm.mdArquivoDSEQ_BANCO.AsInteger           := 1;
      dm.mdArquivoDID_ATO.AsInteger              := iIdAto;
      dm.mdArquivoD.Post;

      dm.iTotalTitulos     := 1;
      dm.cTotalEmolumentos := GR.NoRound(dm.TitulosTOTAL.AsFloat, 2);

      //Definir Serventia Agregada para qual ser� enviado o arquivo
      dm.DistribuirCartorios;

      dm.mdArquivoD.Close;

      if dm.conSISTEMA.InTransaction then
        dm.conSISTEMA.Commit;
    except
      if dm.conSISTEMA.InTransaction then
        dm.conSISTEMA.Rollback;
    end;

    Application.CreateForm(TFQuickRecibo1,FQuickRecibo1);

    dm.Titulos.Close;
    dm.Titulos.Params[0].AsInteger := iIdAto;
    dm.Titulos.Open;

    if dm.ValorParametro(16)='S' then
      GR.Aviso('Apontamento efetuado com sucesso!' + #13#13 +
               'PROTOCOLO: ' + Protocolo + #13#13 +
               'SERVENTIA AGREGADA: ' + dm.TitulosSERVENTIA_AGREGADA.AsString);

    if GR.Pergunta('DESEJA INCLUIR OUTRO T�TULO') then
    begin
      {if not GR.Pergunta('DESEJA MANTER AS INFORMA��ES') then
      begin}
      dm.Titulos.Close;
      dm.Titulos.Params[0].AsInteger:=-1;
      dm.Titulos.Open;
      dm.Titulos.Append;
      dm.TitulosCOBRANCA.AsString         :='CC';
      dm.TitulosDT_ENTRADA.AsDateTime     :=Now;
      dm.TitulosCONVENIO.AsString         :='N';
      dm.TitulosTIPO_APRESENTACAO.AsString:='B';
      dm.TitulosCPF_ESCREVENTE.AsString   :=dm.vCPF;
      dm.TitulosRECIBO.AsInteger          :=StrToInt(dm.ValorParametro(11));
      dm.TitulosTIPO_PROTESTO.AsInteger   :=1;
      dm.TitulosACEITE.AsString           :='N';
      dm.TitulosFINS_FALIMENTARES.AsString:='N';
      dm.TitulosAVALISTA_DEVEDOR.AsString :='N';
//      dm.TitulosVALOR_AR.AsFloat          :=0;
      dm.TitulosVALOR_TITULO.AsFloat      :=0;
      dm.TitulosSALDO_TITULO.AsFloat      :=0;
      dm.TitulosTOTAL.AsFloat             :=0;
      dm.TitulosSALDO_PROTESTO.AsFloat    :=0;
      dm.TitulosPRACA_PROTESTO.AsString   :=dm.ValorParametro(6);

//      if dm.ValorParametro(35) <> '' then
//        dm.TitulosVALOR_AR.AsFloat        := StrToFloat(dm.ValorParametro(35));

      dm.RxPortador.Close;
      dm.RxPortador.Open;
      dm.RxPortador.Append;
      dm.RxPortadorTIPO.AsString          :='F';
      dm.RxPortadorCONVENIO.AsString      :='N';
      dm.RxPortadorBANCO.AsString         :='N';

      dm.RxDevedor.Close;
      dm.RxDevedor.Open;

      dm.RxSacador.Close;
      dm.RxSacador.Open;
      dm.RxSacador.Append;
      dm.RxSacadorTIPO.AsString           :='F';

      dm.RxCustas.Close;
      dm.RxCustas.Open;
      {end;}

      lkEspecie.SetFocus;
    end
    else
      Close;
  except
    on E:Exception do
    begin
      //Gdm.LiberarSelo('PROTESTO',dm.TitulosCCT.AsString,False);

      if dm.conSISTEMA.InTransaction then
        dm.conSISTEMA.Rollback;

      if dm.conCENTRAL.InTransaction then
        dm.conCENTRAL.Rollback;

      GR.Aviso('Erro: '+E.Message);
    end;
  end;
end;

procedure TFEntrada.cbTipoAprChange(Sender: TObject);
begin
  if cbTipoApr.ItemIndex=0 then
  begin
      edDocApr.BoundLabel.Caption     :='CPF';
      dm.RxPortadorDOCUMENTO.EditMask :='999.999.999-99;0;_';
      dm.RxPortadorTIPO.AsString      :='F';
  end
  else
  begin
      edDocApr.BoundLabel.Caption     :='CNPJ';
      dm.RxPortadorDOCUMENTO.EditMask :='99.999.999/9999-99;0;_';
      dm.RxPortadorTIPO.AsString      :='J';
  end;
end;

procedure TFEntrada.edDocAprClick(Sender: TObject);
begin
  edDocApr.SelectAll;
end;

procedure TFEntrada.edDocAprChange(Sender: TObject);
begin
  if GR.PegarNumeroTexto(edDocApr.Text)='' then
  begin
      ImAviso1.Visible:=True;
      ImOk1.Visible   :=False;
      Exit;
  end;

  if cbTipoApr.ItemIndex=0 then ImAviso1.Visible:=not PF.VerificarCPF(GR.PegarNumeroTexto(edDocApr.Text));
  if cbTipoApr.ItemIndex=1 then ImAviso1.Visible:=not PF.VerificarCNPJ(GR.PegarNumeroTexto(edDocApr.Text));

  ImOk1.Visible:=not ImAviso1.Visible;
end;

procedure TFEntrada.edDocAprExit(Sender: TObject);
begin
  if GR.PegarNumeroTexto(edDocApr.Text)='' then Exit;

  dm.Portadores.Close;
  dm.Portadores.CommandText:='SELECT * from PORTADORES WHERE DOCUMENTO =:D';
  dm.Portadores.Params.ParamByName('D').AsString:=GR.PegarNumeroTexto(edDocApr.Text);
  dm.Portadores.Open;
  if not dm.Portadores.IsEmpty then
  begin
      if dm.PortadoresCODIGO.AsString<>'' then
        PF.CarregarPortador('C',dm.PortadoresCODIGO.AsString,dm.PortadoresNOME.AsString,
                            dm.PortadoresTIPO.AsString,dm.PortadoresDOCUMENTO.AsString,dm.PortadoresCODIGO.AsString)
      else if dm.PortadoresDOCUMENTO.AsString<>'' then
          PF.CarregarPortador('D',dm.PortadoresDOCUMENTO.AsString,dm.PortadoresNOME.AsString,
                              dm.PortadoresTIPO.AsString,dm.PortadoresDOCUMENTO.AsString,dm.PortadoresCODIGO.AsString)
      else
        PF.CarregarPortador('N',dm.PortadoresNOME.AsString,dm.PortadoresNOME.AsString,
                            dm.PortadoresTIPO.AsString,dm.PortadoresDOCUMENTO.AsString,dm.PortadoresCODIGO.AsString);

      //cbTipoAprChange(Sender);
  end
  else
    sbPortadorClick(Sender);

  if dm.TitulosSACADOR.AsString = '' then
    dm.TitulosSACADOR.AsString := dm.RxPortadorNOME.AsString;

  if dm.TitulosCEDENTE.AsString = '' then
    dm.TitulosCEDENTE.AsString := dm.TitulosSACADOR.AsString;

  dm.RxSacadorNOME.AsString := dm.TitulosSACADOR.AsString;

  edSacador.SetFocus;
end;

procedure TFEntrada.sbPortadorClick(Sender: TObject);
begin
  ActiveControl:=Nil;
  GR.CriarForm(TFDadosPortador,FDadosPortador);
end;

procedure TFEntrada.edApresentanteExit(Sender: TObject);
begin
  if dm.TitulosSACADOR.AsString = '' then
    dm.TitulosSACADOR.AsString := dm.RxPortadorNOME.AsString;

  if dm.TitulosCEDENTE.AsString = '' then
    dm.TitulosCEDENTE.AsString := dm.TitulosSACADOR.AsString;

  dm.RxSacadorNOME.AsString := dm.TitulosSACADOR.AsString;
end;

procedure TFEntrada.edCodigoExit(Sender: TObject);
begin
  if edCodigo.Text='' then Exit;

  dm.Portadores.Close;
  dm.Portadores.CommandText:='SELECT * FROM PORTADORES WHERE CODIGO=:C';
  dm.Portadores.Params.ParamByName('C').AsString:=edCodigo.Text;
  dm.Portadores.Open;
  if not dm.Portadores.IsEmpty then
  begin
    dm.RxPortadorTIPO.AsString        :=dm.PortadoresTIPO.AsString;
    dm.RxPortadorDOCUMENTO.AsString   :=dm.PortadoresDOCUMENTO.AsString;
    dm.RxPortadorNOME.AsString        :=dm.PortadoresNOME.AsString;

    if dm.TitulosSACADOR.AsString = '' then
      dm.TitulosSACADOR.AsString := dm.RxPortadorNOME.AsString;

    if dm.TitulosCEDENTE.AsString = '' then
      dm.TitulosCEDENTE.AsString := dm.TitulosSACADOR.AsString;

    dm.RxSacadorNOME.AsString := dm.TitulosSACADOR.AsString;

    cbTipoAprChange(Sender);
    edSacador.SetFocus;
  end;

  dm.Portadores.Close;
  dm.Portadores.CommandText:='SELECT * FROM PORTADORES ORDER BY NOME';
end;

procedure TFEntrada.cbTipoDevChange(Sender: TObject);
begin
  if cbTipoDev.ItemIndex=0 then
  begin
      edDocDev.BoundLabel.Caption :='CPF';
      edDocDev.EditMask           :='999.999.999-99;0;_';
  end
  else
  begin
      edDocDev.BoundLabel.Caption :='CNPJ';
      edDocDev.EditMask           :='99.999.999/9999-99;0;_';
  end;
end;

procedure TFEntrada.edDocDevChange(Sender: TObject);
begin
  if GR.PegarNumeroTexto(edDocDev.Text)='' then
  begin
      ImAviso2.Visible:=True;
      ImOk2.Visible   :=False;
      Exit;
  end;

  if cbTipoDev.ItemIndex=0 then ImAviso2.Visible:= not PF.VerificarCPF(GR.PegarNumeroTexto(edDocDev.Text));
  if cbTipoDev.ItemIndex=1 then ImAviso2.Visible:= not PF.VerificarCNPJ(GR.PegarNumeroTexto(edDocDev.Text));

  ImOk2.Visible:=not ImAviso2.Visible;
end;

procedure TFEntrada.edDocDevClick(Sender: TObject);
begin
  edDocDev.SelectAll;
end;

procedure TFEntrada.edDocDevExit(Sender: TObject);
var
  Q: TFDQuery;
begin
  if GR.PegarNumeroTexto(edDocDev.Text)='' then Exit;

  if dm.RxDevedor.IsEmpty then
  begin
      dm.RxDevedor.Append;
      dm.RxDevedorORDEM.AsInteger:=1;
  end
  else
  begin
      dm.RxDevedor.Locate('ORDEM',1,[]);
      dm.RxDevedor.Edit;
  end;

  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT * FROM DEVEDORES WHERE DOCUMENTO='+QuotedStr(edDocDev.Text)+' ORDER BY ID_DEVEDOR DESCENDING');
  Q.Open;

  if not Q.IsEmpty then
  begin
    dm.RxDevedorNOME.AsString           := Q.FieldByName('NOME').AsString;
    dm.RxDevedorDOCUMENTO.AsString      := Q.FieldByName('DOCUMENTO').AsString;
    dm.RxDevedorTIPO.AsString           := Q.FieldByName('TIPO').AsString;
    dm.RxDevedorIFP_DETRAN.AsString     := Q.FieldByName('IFP_DETRAN').AsString;
    dm.RxDevedorIDENTIDADE.AsString     := Q.FieldByName('IDENTIDADE').AsString;
    dm.RxDevedorORGAO.AsString          := Q.FieldByName('ORGAO').AsString;
    dm.RxDevedorENDERECO.AsString       := Q.FieldByName('ENDERECO').AsString;
    dm.RxDevedorBAIRRO.AsString         := Q.FieldByName('BAIRRO').AsString;
    dm.RxDevedorMUNICIPIO.AsString      := Q.FieldByName('MUNICIPIO').AsString;
    dm.RxDevedorUF.AsString             := Q.FieldByName('UF').AsString;
    dm.RxDevedorIGNORADO.AsString       := Q.FieldByName('IGNORADO').AsString;
    dm.RxDevedorJUSTIFICATIVA.AsString  := Q.FieldByName('JUSTIFICATIVA').AsString;
    dm.RxDevedorTELEFONE.AsString       := Q.FieldByName('TELEFONE').AsString;
    dm.RxDevedorCEP.AsString            := Q.FieldByName('CEP').AsString;

    if Q.FieldByName('DT_EMISSAO').AsDateTime<>0 then
    dm.RxDevedorDT_EMISSAO.AsDateTime:=Q.FieldByName('DT_EMISSAO').AsDateTime;

    edDevedor.Text         := dm.RxDevedorNOME.AsString;
    edDocDev.Text          := dm.RxDevedorDOCUMENTO.AsString;
    edTelefone.Text        := dm.RxDevedorTELEFONE.AsString;
    edtEndDevedor.Text     := dm.RxDevedorENDERECO.AsString;
    edtBairroDevedor.Text  := dm.RxDevedorBAIRRO.AsString;
    lcbMunDevedor.KeyValue := dm.RxDevedorMUNICIPIO.AsString;
    cbUFDevedor.ItemIndex  := cbUFDevedor.IndexOf(dm.RxDevedorUF.AsString);
    medCEPDevedor.Text     := dm.RxDevedorCEP.AsString;
  end;

  dm.RxDevedor.Post;

  Q.Close;
  Q.Free;
end;

procedure TFEntrada.sbLocalizarClick(Sender: TObject);
begin
  dm.Portadores.Close;
  GR.CriarForm(TFConsultaPortador,FConsultaPortador);

  if dm.vOkGeral then
  begin
    if dm.PortadoresCODIGO.AsString <> '' then
      PF.CarregarPortador('C', dm.PortadoresCODIGO.AsString, dm.PortadoresNOME.AsString,
                          dm.PortadoresTIPO.AsString, dm.PortadoresDOCUMENTO.AsString, dm.PortadoresCODIGO.AsString)
    else if dm.PortadoresDOCUMENTO.AsString <> '' then
      PF.CarregarPortador('D', dm.PortadoresDOCUMENTO.AsString, dm.PortadoresNOME.AsString,
                          dm.PortadoresTIPO.AsString, dm.PortadoresDOCUMENTO.AsString, dm.PortadoresCODIGO.AsString)
    else
      PF.CarregarPortador('N', dm.PortadoresNOME.AsString, dm.PortadoresNOME.AsString,
                          dm.PortadoresTIPO.AsString, dm.PortadoresDOCUMENTO.AsString, dm.PortadoresCODIGO.AsString);

    if dm.TitulosSACADOR.AsString = '' then
      dm.TitulosSACADOR.AsString := dm.RxPortadorNOME.AsString;

    if dm.TitulosCEDENTE.AsString = '' then
      dm.TitulosCEDENTE.AsString := dm.TitulosSACADOR.AsString;

    dm.RxSacadorNOME.AsString := dm.TitulosSACADOR.AsString;

    cbTipoAprChange(Sender);
    edSacador.SetFocus;
  end
  else
    cbTipoApr.SetFocus;
end;

procedure TFEntrada.sbDevedorClick(Sender: TObject);
begin
  if edDocDev.Text='' then
  begin
      GR.Aviso('INFORME O DOCUMENTO DO DEVEDOR!');
      edDocDev.SetFocus;
      Exit;
  end;

  if edDevedor.Text='' then
  begin
      GR.Aviso('INFORME O NOME DO DEVEDOR!');
      edDevedor.SetFocus;
      Exit;
  end;

  if dm.RxDevedor.IsEmpty then
  begin
    dm.RxDevedor.Append;
    dm.RxDevedorORDEM.AsInteger     :=1;
    dm.RxDevedorNOME.AsString       :=edDevedor.Text;
    dm.RxDevedorTIPO.AsString       :=cbTipoDev.Text;
    dm.RxDevedorDOCUMENTO.AsString  :=edDocDev.Text;
    dm.RxDevedorTELEFONE.AsString   :=edTelefone.Text;
    dm.RxDevedorIGNORADO.AsString   :='N';
    dm.RxDevedorENDERECO.AsString   := edtEndDevedor.Text;
    dm.RxDevedorBAIRRO.AsString     := edtBairroDevedor.Text;
    dm.RxDevedorMUNICIPIO.AsString  := lcbMunDevedor.Text;
    dm.RxDevedorUF.AsString         := cbUFDevedor.Text;
    dm.RxDevedorCEP.AsString        := medCEPDevedor.Text;
    dm.RxDevedor.Post;
  end
  else
    dm.RxDevedor.Locate('ORDEM',1,[]);

  dm.RxDevedor.Edit;

  GR.CriarForm(TFCadastro,FCadastro);

  cbTipoDev.ItemIndex    := IfThen(dm.RxDevedorTIPO.AsString<>'',cbTipoDev.IndexOf(dm.RxDevedorTIPO.AsString),0);
  edDocDev.Text          := dm.RxDevedorDOCUMENTO.AsString;
  edDevedor.Text         := dm.RxDevedorNOME.AsString;
  edTelefone.Text        := dm.RxDevedorTELEFONE.AsString;
  edtEndDevedor.Text     := dm.RxDevedorENDERECO.AsString;
  edtBairroDevedor.Text  := dm.RxDevedorBAIRRO.AsString;
  lcbMunDevedor.KeyValue := dm.RxDevedorMUNICIPIO.AsString;
  cbUFDevedor.ItemIndex  := cbUFDevedor.IndexOf(dm.RxDevedorUF.AsString);
  medCEPDevedor.Text     := dm.RxDevedorCEP.AsString;
end;

procedure TFEntrada.edValorExit(Sender: TObject);
begin
  dm.TitulosSALDO_TITULO.AsFloat := dm.TitulosVALOR_TITULO.AsFloat;
end;

procedure TFEntrada.edValorClick(Sender: TObject);
begin
  edValor.SelectAll;
  dm.TitulosSALDO_TITULO.AsFloat := dm.TitulosVALOR_TITULO.AsFloat;
end;

procedure TFEntrada.edCustasClick(Sender: TObject);
begin
  edCustas.SelectAll;
end;

procedure TFEntrada.edSaldoClick(Sender: TObject);
begin
  edSaldo.SelectAll;
end;

procedure TFEntrada.edSaldoExit(Sender: TObject);
var
  //S: String;
  Emolumentos, {Valor,} Comuns: Double;
begin
  dm.RxCustas.Close;
  dm.RxCustas.Open;

  Comuns := 0;
  Emolumentos := 0;
  {Valor:=StrToFloat(edSaldo.Text);}

  //Coloquei essa faixa para cima do if do tipo de cobran�a, pois mesmo o titulo sendo JG ou SC ele tem q ter codigo para ser
  //contabilizado nos relatorios - Pedro - 12/05/2016
  Faixa.Close;
  Faixa.ParamByName('C').AsString        := dm.RxPortadorCONVENIO.AsString;
  Faixa.ParamByName('VALOR1').AsFloat    := StrToFloat(edSaldo.Text);
  Faixa.ParamByName('VALOR2').AsFloat    := StrToFloat(edSaldo.Text);
  Faixa.ParamByName('OCULTO1').AsString  := 'N';
  Faixa.ParamByName('OCULTO2').AsString  := 'N';
  Faixa.Open;

  if cbTipoCobranca.ItemIndex=1 then
  begin
      {Faixa.Close;
      Faixa.ParamByName('C').AsString       :=dm.RxPortadorCONVENIO.AsString;
      Faixa.ParamByName('VALOR').AsFloat    :=StrToFloat(edSaldo.Text);
      Faixa.ParamByName('OCULTO').AsString  :='N';
      Faixa.Open;}

      {if ckInternet.Checked then
      begin}
    dm.Itens.Close;
    dm.Itens.Params[0].AsInteger := FaixaCOD.AsInteger;
    dm.Itens.Open;
    dm.Itens.First;

    while not dm.Itens.Eof do
    begin
      dm.RxCustas.Append;
      dm.RxCustasTABELA.AsString    := dm.ItensTAB.AsString;
      dm.RxCustasITEM.AsString      := dm.ItensITEM.AsString;
      dm.RxCustasSUBITEM.AsString   := dm.ItensSUB.AsString;
      dm.RxCustasVALOR.AsFloat      := dm.ItensVALOR.AsFloat;
      dm.RxCustasQTD.AsString       := dm.ItensQTD.AsString;
      dm.RxCustasTOTAL.AsFloat      := dm.RxCustasVALOR.AsFloat * dm.RxCustasQTD.AsInteger;
      dm.RxCustasDESCRICAO.AsString := dm.ItensDESCR.AsString;
      dm.RxCustas.Post;

      Emolumentos := Emolumentos + dm.RxCustasTOTAL.AsFloat;

      if dm.ItensTAB.AsString = '16' then
        Comuns := Comuns + dm.ItensTOTAL.AsFloat;

      dm.Itens.Next;
    end;
      {end
      else
      begin
          dm.Balcao.Close;
          if cbConvenio.ItemIndex=0 then
            dm.Balcao.Params[0].AsString:='S'
              else dm.Balcao.Params[0].AsString:='N';
          dm.Balcao.Open;
          dm.Balcao.First;
          while not dm.Balcao.Eof do
          begin
              dm.RxCustas.Append;
              dm.RxCustasTABELA.AsString    :=dm.BalcaoTABELA.AsString;
              dm.RxCustasITEM.AsString      :=dm.BalcaoITEM.AsString;
              dm.RxCustasSUBITEM.AsString   :=dm.BalcaoSUBITEM.AsString;
              dm.RxCustasVALOR.AsFloat      :=dm.BalcaoVALOR.AsFloat;
              dm.RxCustasQTD.AsString       :=dm.BalcaoQTD.AsString;
              dm.RxCustasTOTAL.AsFloat      :=dm.RxCustasVALOR.AsFloat*dm.RxCustasQTD.AsInteger;
              dm.RxCustasDESCRICAO.AsString :=dm.BalcaoDESCRICAO.AsString;
              dm.RxCustas.Post;
              Emolumentos:=Emolumentos+dm.RxCustasTOTAL.AsFloat;
              dm.Balcao.Next;
          end;
      end;}

      {ADICIONANDO O �TEM VARI�VEL SE N�O ESTIVER PR�-CADASTRADO}
      {if (Valor<=50.00)                          then S:='A';
      if (Valor>50.00)    and  (Valor<=100.00)   then S:='B';
      if (Valor>100.00)   and  (Valor<=200.00)   then S:='C';
      if (Valor>200.00)   and  (Valor<=500.00)   then S:='D';
      if (Valor>500.00)   and  (Valor<=1000.00)  then S:='E';
      if (Valor>1000.00)  and  (Valor<=5000.00)  then S:='F';
      if (Valor>5000.00)  and  (Valor<=10000.00) then S:='G';
      if (Valor>10000.00)                        then S:='H';
      Tabela.Close;
      Tabela.Params[0].AsString:=S;
      Tabela.Open;
      if not dm.RxCustas.Locate('TABELA;ITEM',VarArrayOf([TabelaTAB.AsString,TabelaITEM.AsString]),[loCaseInsensitive, loPartialKey]) then
      begin
          dm.RxCustas.Append;
          dm.RxCustasTABELA.AsString    :=TabelaTAB.AsString;
          dm.RxCustasITEM.AsString      :=TabelaITEM.AsString;
          dm.RxCustasSUBITEM.AsString   :=TabelaSUB.AsString;
          dm.RxCustasVALOR.AsFloat      :=TabelaVALOR.AsFloat;
          dm.RxCustasQTD.AsString       :='1';
          dm.RxCustasTOTAL.AsFloat      :=TabelaVALOR.AsFloat;
          dm.RxCustasDESCRICAO.AsString :=TabelaDESCR.AsString;
          dm.RxCustas.Post;
          Emolumentos:=Emolumentos+dm.RxCustasTOTAL.AsFloat;
      end;}

    dm.vEmolumentos   := Emolumentos;
    dm.vFetj          := GR.NoRound(Emolumentos*0.2,2);
    dm.vFundperj      := GR.NoRound(Emolumentos*0.05,2);
    dm.vFunperj       := GR.NoRound(Emolumentos*0.05,2);
    dm.vFunarpen      := GR.NoRound(Emolumentos*0.04,2);
    dm.vPmcmv         := GR.NoRound((Emolumentos-Comuns)*0.02,2);
    dm.vIss           := GR.NoRound(Emolumentos*Gdm.vAliquotaISS,2);
    dm.vMutua         := StrToFloat(dm.ValorParametro(26));
    dm.vAcoterj       := StrToFloat(dm.ValorParametro(27));

    if cbConvenio.ItemIndex = 0 then
      dm.vDistribuicao := StrToFloat(dm.ValorParametro(75))
    else
      dm.vDistribuicao := StrToFloat(dm.ValorParametro(74));

    dm.vAAR           := 0;  //dm.TitulosVALOR_AR.AsFloat;
    dm.vTotal         := dm.vEmolumentos +
                         dm.vFetj +
                         dm.vFundperj +
                         dm.vFunperj +
                         dm.vFunarpen +
                         dm.vPmcmv +
                         dm.vIss +
                         dm.vMutua +
                         dm.vAcoterj +
                         dm.vDistribuicao +
                         dm.vAAR;

    dm.TitulosTOTAL.AsFloat           := dm.vTotal;
    dm.TitulosSALDO_PROTESTO.AsFloat  := dm.TitulosSALDO_TITULO.AsFloat + dm.TitulosTOTAL.AsFloat;
//    dm.TitulosSALDO_TITULO.AsFloat    := dm.TitulosVALOR_TITULO.AsFloat;
  end
  else
    if (cbTipoCobranca.ItemIndex = 0) or (cbTipoCobranca.ItemIndex = 2) then
    begin
      dm.vEmolumentos                  := 0;
      dm.vFetj                         := 0;
      dm.vFundperj                     := 0;
      dm.vFunperj                      := 0;
      dm.vFunarpen                     := 0;
      dm.vPmcmv                        := 0;
      dm.vIss                          := 0;
      dm.vMutua                        := 0;
      dm.vAcoterj                      := 0;
      dm.vDistribuicao                 := 0;
      dm.vAAR                          := 0;
      dm.vTotal                        := 0;
      dm.TitulosTOTAL.AsFloat          := 0;
      dm.TitulosSALDO_PROTESTO.AsFloat := 0;
    end;
end;

procedure TFEntrada.edSaldoTotalClick(Sender: TObject);
begin
  edSaldoTotal.SelectAll;
end;

procedure TFEntrada.edtServentiaAgregadaChange(Sender: TObject);
begin
  if dm.TitulosCARTORIO.IsNull then
    lblServentiaAgregada.Caption := '-'
  else
    lblServentiaAgregada.Caption := dm.TitulosCARTORIO.AsString;
end;

procedure TFEntrada.lkEspecieClick(Sender: TObject);
begin
  if dm.TiposCODIGO.AsInteger=10 then
  begin
      edBanco.Enabled   :=True;
      edAgencia.Enabled :=True;
      edConta.Enabled   :=True;
  end
  else
  begin
      dm.TitulosBANCO.Clear;
      dm.TitulosAGENCIA.Clear;
      dm.TitulosCONTA.Clear;
      edBanco.Enabled   :=False;
      edAgencia.Enabled :=False;
      edConta.Enabled   :=False;
  end;
end;

procedure TFEntrada.lkEspecieKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#8 then lkEspecie.KeyValue:=0;
end;

procedure TFEntrada.FormCreate(Sender: TObject);
begin
  if FPrincipal.ckAssistente.Checked then
    Self.Height := 700  //600
  else
    Self.Height := 580;  //480;

  lkEspecieClick(Sender);
  cbTipoAprChange(Sender);
  cbTipoDevChange(Sender);

  dm.Municipios.Close;
  dm.Municipios.Params[0].AsString := 'RJ';
  dm.Municipios.Open;
end;

procedure TFEntrada.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
//  if key=VK_F1     then lbARClick(Sender);
end;

procedure TFEntrada.FormShow(Sender: TObject);
begin
  edtServentiaAgregada.ReadOnly := True;

  edNumero.MaxLength       := 11;
  edtNossoNumero.MaxLength := 15;

  lbAR.Visible := False;
  edAR.Visible := False;
end;

procedure TFEntrada.btCancelarClick(Sender: TObject);
begin
  dm.Titulos.Cancel;
  Close;
end;

procedure TFEntrada.FormActivate(Sender: TObject);
begin
  lkEspecie.SetFocus;
end;

procedure TFEntrada.edSacadorExit(Sender: TObject);
begin
  if dm.TitulosCEDENTE.AsString = '' then
    dm.TitulosCEDENTE.AsString := dm.TitulosSACADOR.AsString;

  dm.RxSacadorNOME.AsString := dm.TitulosSACADOR.AsString;
end;

procedure TFEntrada.sbSacadorClick(Sender: TObject);
begin
  ActiveControl:=Nil;
  GR.CriarForm(TFSacador,FSacador);
end;

procedure TFEntrada.medCEPDevedorExit(Sender: TObject);
begin
  btSalvar.SetFocus;
end;

procedure TFEntrada.Label4Click(Sender: TObject);
begin
  GR.CriarForm(TFParametros,FParametros);
end;

procedure TFEntrada.ImTabelaClick(Sender: TObject);
begin
  if (cbTipoCobranca.ItemIndex=0) or (cbTipoCobranca.ItemIndex=2) then Exit;

  if dm.TitulosSALDO_TITULO.AsFloat=0 then
  begin
      GR.Aviso('Informe o saldo do t�tulo.');
      edValor.SetFocus;
      Exit;
  end;

  dm.Tabela.Close;
  dm.Tabela.Open;

  //dm.vCertidao:=False;
  GR.CriarForm(TFCustas,FCustas);

  dm.Tabela.Close;

  dm.TitulosTOTAL.AsFloat:=dm.vTotal;
  dm.TitulosSALDO_PROTESTO.AsFloat:=dm.TitulosTOTAL.AsFloat+dm.TitulosSALDO_TITULO.AsFloat;
end;

procedure TFEntrada.lbARClick(Sender: TObject);
begin
  { Por solicitacao do Vinicius (BP 1 Ofic.), todos as referencias ao valor do AR
    foram comentadas para que o mesmo nao entre nos calculos - Cristina (04-08-2017) }

{  if dm.ValorParametro(35) = '' then
    dm.TitulosVALOR_AR.AsFloat := 0
  else
    dm.TitulosVALOR_AR.AsFloat:=StrToFloat(dm.ValorParametro(35));  }
end;

procedure TFEntrada.ckAntigoClick(Sender: TObject);
begin
  if ckAntigo.Checked then
    edProtocolo.Enabled:=True
      else
      begin
          edProtocolo.Enabled:=False;
          if dm.Titulos.State in [dsEdit,dsInsert] then
          dm.TitulosPROTOCOLO.Clear;
      end;
end;

procedure TFEntrada.sbDevedoresClick(Sender: TObject);
begin
  if edDocDev.Text='' then
  begin
      GR.Aviso('INFORME O DOCUMENTO DO DEVEDOR!');
      edDocDev.SetFocus;
      Exit;
  end;

  if edDevedor.Text='' then
  begin
      GR.Aviso('INFORME O NOME DO DEVEDOR!');
      edDevedor.SetFocus;
      Exit;
  end;

  if dm.RxDevedor.IsEmpty then
  begin
      dm.RxDevedor.Append;
      dm.RxDevedorORDEM.AsInteger     :=1;
  end
  else
  begin
      dm.RxDevedor.Locate('ORDEM','1',[]);
      dm.RxDevedor.Edit;
  end;

  dm.RxDevedorNOME.AsString       :=edDevedor.Text;
  dm.RxDevedorTIPO.AsString       :=cbTipoDev.Text;
  dm.RxDevedorDOCUMENTO.AsString  :=edDocDev.Text;
  dm.RxDevedorTELEFONE.AsString   :=edTelefone.Text;
  dm.RxDevedorIGNORADO.AsString   :='N';
  dm.RxDevedorENDERECO.AsString   := edtEndDevedor.Text;
  dm.RxDevedorBAIRRO.AsString     := edtBairroDevedor.Text;
  dm.RxDevedorMUNICIPIO.AsString  := lcbMunDevedor.Text;
  dm.RxDevedorUF.AsString         := cbUFDevedor.Text;
  dm.RxDevedorCEP.AsString        := medCEPDevedor.Text;
  dm.RxDevedor.Post;

  GR.CriarForm(TFDevedores,FDevedores);

  dm.RxDevedor.Locate('ORDEM',1,[]);
  cbTipoDev.ItemIndex    := IfThen(dm.RxDevedorTIPO.AsString<>'',cbTipoDev.IndexOf(dm.RxDevedorTIPO.AsString),0);
  edDocDev.Text          := dm.RxDevedorDOCUMENTO.AsString;
  edDevedor.Text         := dm.RxDevedorNOME.AsString;
  edTelefone.Text        := dm.RxDevedorTELEFONE.AsString;
  edtEndDevedor.Text     := dm.RxDevedorENDERECO.AsString;
  edtBairroDevedor.Text  := dm.RxDevedorBAIRRO.AsString;
  lcbMunDevedor.KeyValue := dm.RxDevedorMUNICIPIO.AsString;
  cbUFDevedor.ItemIndex  := cbUFDevedor.IndexOf(dm.RxDevedorUF.AsString);
  medCEPDevedor.Text     := dm.RxDevedorCEP.AsString;
end;

end.
