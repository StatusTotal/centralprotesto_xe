�
 TFQUICKRETORNO1 0�7  TPF0TFQuickRetorno1FQuickRetorno1LeftTop� Width>HeightxCaptionRetornoColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style OldCreateOrderScaledPixelsPerInch`
TextHeight 	TQuickRep	RelatorioLeftTopWidthHeightcFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightDataSetFRelRetorno.RXFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' 	OnEndPageRelatorioEndPageOptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPrevInitialZoomqrZoomToWidthPreviewDefaultSaveTypestQRP TQRBandQRBand1LeftLTop&Width�Height� Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.ValuesVUUUUU�@VUUUUU��	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbTitle TQRLabellbDataLeftTopWidth"HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@UUUUUUU�@�������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionlbDataColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRLabel
lbCartorioLeftTop Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values ������@ XUUUUU�@ XUUUUU�@ �������	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption   De: CartórioColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRLabellbBancoLeftTop3Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values ������@ XUUUUU�@      ��@ �������	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionAo: PortadorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel1LefttTop� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUu�@      ��@      �	@ 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaptionK   Estamos enviando os documentos referentes aos títulos abaixo relacionados:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsUnderline 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize	  TQRLabel	lbArquivoLeftTopGWidth*HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@������ڻ@      @�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionArquivo:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRLabellbTotalLeftTop[WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@TUUUUU��@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionTotal:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel8LeftTopoWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@      ؒ@       �@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionQtd.:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptTextFontSize  
TQRSysData
QRSysData1Left"TopoWidthAHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@�������@      ؒ@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	ColorclWhiteDataqrsDetailCountFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptTextFontSize   TQRBandQRBand2LeftLTop� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.Values������j�@VUUUUU��	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail TQRShapeQRShape6LeftXTop WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����j�@ `UUUU��@           XUUUUU� @ Brush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShapeQRShape7Left� Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����j�@ �����
�@           XUUUUU� @ Brush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShapeQRShape8LeftfTop WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����j�@ XUUUU��@           XUUUUU� @ Brush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShapeQRShape9Left�Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����j�@ XUUUUa�	@           XUUUUU� @ Brush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShape	QRShape10LeftFTop WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����j�@      |�	@           XUUUUU� @ Brush.ColorclBlackShapeqrsRectangle
VertAdjust   	TQRDBText	QRDBText1LeftTopWidthOHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@ �������@ XUUUUU�@ XUUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetFRelRetorno.RX	DataField	ProtocoloFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText	QRDBText2Left^TopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@ XUUUU��@ XUUUUU�@ XUUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetFRelRetorno.RX	DataFieldNossoNumeroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText	QRDBText3Left� TopWidthcHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@ ������@ XUUUUU�@      ��@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchColorclWhiteDataSetFRelRetorno.RX	DataFieldDataFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText	QRDBText4LeftlTopWidth}HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@ XUUUU��@ XUUUUU�@ XUUUU]�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetFRelRetorno.RX	DataFieldSituacaoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText	QRDBText5Left�TopWidthMHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@ XUUUU]�	@ XUUUUU�@ �������@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetFRelRetorno.RX	DataFieldCustasFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  	TQRDBText	QRDBText6LeftLTopWidthCHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      x�	@ XUUUUU�@ XUUUUE�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetFRelRetorno.RX	DataFieldValorTituloFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize   TQRChildBand
ChildBand1LeftLTop� Width�HeightFrame.ColorclBlackFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.Values������j�@VUUUUU��	@ PreCaluculateBandHeightKeepOnOnePage
ParentBandQRBand1
PrintOrdercboAfterParent TQRLabelQRLabel2LeftTopWidth.HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@ `UUUU��@ XUUUUU�@ �����j�@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaption	ProtocoloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel3Left� TopWidthHHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@ XUUUUE�@ XUUUUU�@      ��@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaption   Nosso NúmeroColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel4Left TopWidth_HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@ XUUUUU�@ XUUUUU�@ �����Z�@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaption   Data de OcorrênciaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel5Left�TopWidth+HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      �	@ XUUUUU�@ �������@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaption
   SituaçãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel6Left
TopWidth#HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��	@ XUUUUU�@ XUUUU5�@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaptionCustasColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRLabelQRLabel7Left`TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU�	@UUUUUUU�@      ��@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaptionValorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	WordWrap	ExportAsexptTextFontSize  TQRShapeQRShape1LeftXTop WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����j�@ `UUUU��@           XUUUUU� @ Brush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShapeQRShape2Left� Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����j�@ �����
�@           XUUUUU� @ Brush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShapeQRShape3LeftfTop WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����j�@ XUUUU��@           XUUUUU� @ Brush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShapeQRShape4Left�Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����j�@ XUUUUa�	@           XUUUUU� @ Brush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRShapeQRShape5LeftFTop WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �����j�@      |�	@           XUUUUU� @ Brush.ColorclBlackShapeqrsRectangle
VertAdjust      