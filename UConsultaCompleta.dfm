object FConsultaCompleta: TFConsultaCompleta
  Left = 213
  Top = 88
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'CONSULTA COMPLETA'
  ClientHeight = 571
  ClientWidth = 1005
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 1005
    Height = 41
    Align = alTop
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object cbBuscar: TsComboBox
      Left = 14
      Top = 9
      Width = 214
      Height = 24
      Cursor = crHandPoint
      Alignment = taLeftJustify
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = -1
      ParentFont = False
      TabOrder = 0
      OnChange = cbBuscarChange
      Items.Strings = (
        'PROTOCOLO (DISTRIBUIDOR)'
        'PROTOCOLO (SERV. AGREG.)'
        'PORTADOR'
        'DEVEDOR'
        'CPF/CNPJ DO DEVEDOR'
        'ENDERE'#199'O DO DEVEDOR'
        'SACADOR'
        'CEDENTE'
        'N'#186' T'#205'TULO'
        'DATA DO PROTOCOLO'
        'DATA DA SOLU'#199#195'O')
    end
    object edPesquisar: TsEdit
      Left = 234
      Top = 9
      Width = 309
      Height = 24
      AutoSize = False
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnKeyPress = edPesquisarKeyPress
      SkinData.SkinSection = 'EDIT'
    end
    object edData: TsDateEdit
      Left = 549
      Top = 9
      Width = 105
      Height = 24
      AutoSize = False
      Color = clWhite
      Enabled = False
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      OnKeyPress = edDataKeyPress
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Blend = 0
      DefaultToday = True
    end
    object DBNavigator1: TDBNavigator
      Left = 884
      Top = 1
      Width = 120
      Height = 39
      Cursor = crHandPoint
      DataSource = dsConsulta
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
      Align = alRight
      Flat = True
      TabOrder = 3
    end
  end
  object P3: TsPanel
    Left = 0
    Top = 41
    Width = 1005
    Height = 530
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    SkinData.SkinSection = 'TOOLBAR'
    object lbEspecie: TLabel
      Left = 41
      Top = 206
      Width = 333
      Height = 16
      Caption = 'Esp'#233'cie: Duplicata de Presta'#231#227'o de Servi'#231'os por Indica'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbTitulo: TLabel
      Left = 52
      Top = 185
      Width = 160
      Height = 18
      AutoSize = False
      Caption = 'T'#237'tulo: 99999999999'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbPrazo: TLabel
      Left = 827
      Top = 157
      Width = 126
      Height = 16
      Caption = 'Prazo: 00/00/0000'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbEmissao: TLabel
      Left = 429
      Top = 185
      Width = 122
      Height = 16
      Caption = 'Emiss'#227'o: 00/00/0000'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbVencimento: TLabel
      Left = 578
      Top = 185
      Width = 142
      Height = 16
      Caption = 'Vencimento: 00/00/0000'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbValor: TLabel
      Left = 53
      Top = 226
      Width = 35
      Height = 16
      Caption = 'Valor:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbCustas: TLabel
      Left = 526
      Top = 224
      Width = 65
      Height = 16
      Caption = '(+) Custas:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
      OnClick = lbCustasClick
    end
    object lbSacador: TLabel
      Left = 36
      Top = 266
      Width = 684
      Height = 16
      AutoSize = False
      Caption = 'Sacador:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbCedente: TLabel
      Left = 36
      Top = 286
      Width = 684
      Height = 16
      AutoSize = False
      Caption = 'Cedente:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbStatus: TLabel
      Left = 332
      Top = 24
      Width = 279
      Height = 63
      Alignment = taCenter
      AutoSize = False
      Caption = 'INTIMADO'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -24
      Font.Name = 'Tahoma'
      Font.Style = [fsBold, fsUnderline]
      ParentFont = False
      Transparent = True
      WordWrap = True
    end
    object lbData: TLabel
      Left = 24
      Top = 157
      Width = 149
      Height = 16
      Caption = 'Data (D.): 00/00/0000'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbTotal: TLabel
      Left = 813
      Top = 226
      Width = 57
      Height = 16
      Caption = '(=) Total:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbTipo: TLabel
      Left = 840
      Top = 206
      Width = 30
      Height = 16
      Caption = 'Tipo:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbSaldo: TLabel
      Left = 280
      Top = 226
      Width = 59
      Height = 16
      Caption = '(+) Saldo:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lblDataSA: TLabel
      Left = 391
      Top = 157
      Width = 174
      Height = 16
      Caption = 'Data (S. Ag.): 00/00/0000'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lblPracaPagamento: TLabel
      Left = 833
      Top = 185
      Width = 37
      Height = 16
      Caption = 'Pra'#231'a:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lblNomeDevedor: TLabel
      Left = 10
      Top = 99
      Width = 695
      Height = 20
      AutoSize = False
      Caption = 'Devedor:'
      Color = clBtnFace
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -17
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object lblEndDevedor: TLabel
      Left = 14
      Top = 123
      Width = 976
      Height = 18
      AutoSize = False
      Caption = 'Endere'#231'o:'
      Color = clBtnFace
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object lblDocDevedor: TLabel
      Left = 711
      Top = 100
      Width = 283
      Height = 20
      AutoSize = False
      Caption = 'Documento: 999999999999999999'
      Color = clBtnFace
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object lblNossoNumero: TLabel
      Left = 211
      Top = 185
      Width = 180
      Height = 18
      AutoSize = False
      Caption = 'N. N'#250'mero: 999999999999999'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lblNomePortador: TLabel
      Left = 35
      Top = 246
      Width = 466
      Height = 16
      AutoSize = False
      Caption = 'Portador:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lblCodigoPortador: TLabel
      Left = 507
      Top = 246
      Width = 84
      Height = 16
      Caption = 'C'#243'd. Portador:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lblDocumentoPortador: TLabel
      Left = 786
      Top = 246
      Width = 83
      Height = 16
      Caption = 'Doc. Portador:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object GridAtos: TwwDBGrid
      Left = 14
      Top = 308
      Width = 976
      Height = 213
      Cursor = crHandPoint
      Selected.Strings = (
        'DT_PROTOCOLO'#9'10'#9'Data'#9'F'
        'PROTOCOLO'#9'11'#9'Protocolo (D)'#9'F'
        'PROTOCOLO_CARTORIO'#9'11'#9'Protocolo (S.A.)'#9'F'
        'DEVEDOR'#9'36'#9'Devedor'#9'F'
        'APRESENTANTE'#9'31'#9'Portador'#9'F'
        'DESCRICAO'#9'31'#9'Serventia Agregada'#9'F')
      IniAttributes.Delimiter = ';;'
      IniAttributes.UnicodeIniFile = False
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      BorderStyle = bsNone
      DataSource = dsConsulta
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
      ParentFont = False
      PopupMenu = PM
      ReadOnly = True
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      TitleLines = 1
      TitleButtons = True
      UseTFields = False
      OnTitleButtonClick = GridAtosTitleButtonClick
      OnDblClick = GridAtosDblClick
      OnKeyDown = GridAtosKeyDown
    end
    object gbProtocolo: TsGroupBox
      Left = 14
      Top = 6
      Width = 312
      Height = 81
      Caption = 'N'#186' Protocolo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      SkinData.CustomFont = True
      object lbProtocoloD: TLabel
        Left = 3
        Top = 27
        Width = 150
        Height = 23
        Alignment = taCenter
        AutoSize = False
        Caption = '10000'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object Label1: TLabel
        Left = 3
        Top = 54
        Width = 150
        Height = 16
        Alignment = taCenter
        AutoSize = False
        Caption = '(Distribuidor)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object lbProtocoloSA: TLabel
        Left = 161
        Top = 27
        Width = 150
        Height = 23
        Alignment = taCenter
        AutoSize = False
        Caption = '10000'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object Label2: TLabel
        Left = 161
        Top = 54
        Width = 150
        Height = 16
        Alignment = taCenter
        AutoSize = False
        Caption = '(Serventia Agregada)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object gbServentiaAgregada: TsGroupBox
      Left = 617
      Top = 6
      Width = 377
      Height = 81
      Caption = 'Serventia Agregada'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      SkinData.CustomFont = True
      object lblNomeServAgregada: TLabel
        Left = 7
        Top = 51
        Width = 311
        Height = 18
        AutoSize = False
        Caption = 'Nome Serventia Agregada'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object lblServentiaAgregada: TLabel
        Left = 324
        Top = 6
        Width = 41
        Height = 77
        Alignment = taCenter
        Caption = '2'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -64
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object dspConsulta: TDataSetProvider
    DataSet = qryConsulta
    Options = [poAllowCommandText]
    Left = 392
    Top = 393
  end
  object Consulta: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspConsulta'
    AfterOpen = ConsultaAfterOpen
    Left = 448
    Top = 393
    object ConsultaID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Required = True
    end
    object ConsultaCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
    object ConsultaRECIBO: TIntegerField
      FieldName = 'RECIBO'
    end
    object ConsultaDT_ENTRADA: TDateField
      FieldName = 'DT_ENTRADA'
    end
    object ConsultaDT_PROTOCOLO: TDateField
      FieldName = 'DT_PROTOCOLO'
    end
    object ConsultaPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
    end
    object ConsultaLIVRO_PROTOCOLO: TIntegerField
      FieldName = 'LIVRO_PROTOCOLO'
    end
    object ConsultaFOLHA_PROTOCOLO: TStringField
      FieldName = 'FOLHA_PROTOCOLO'
      Size = 10
    end
    object ConsultaDT_PRAZO: TDateField
      FieldName = 'DT_PRAZO'
    end
    object ConsultaDT_REGISTRO: TDateField
      FieldName = 'DT_REGISTRO'
    end
    object ConsultaREGISTRO: TIntegerField
      FieldName = 'REGISTRO'
    end
    object ConsultaLIVRO_REGISTRO: TIntegerField
      FieldName = 'LIVRO_REGISTRO'
    end
    object ConsultaFOLHA_REGISTRO: TStringField
      FieldName = 'FOLHA_REGISTRO'
      Size = 10
    end
    object ConsultaSELO_REGISTRO: TStringField
      FieldName = 'SELO_REGISTRO'
      Size = 9
    end
    object ConsultaDT_PAGAMENTO: TDateField
      FieldName = 'DT_PAGAMENTO'
    end
    object ConsultaSELO_PAGAMENTO: TStringField
      FieldName = 'SELO_PAGAMENTO'
      Size = 9
    end
    object ConsultaRECIBO_PAGAMENTO: TIntegerField
      FieldName = 'RECIBO_PAGAMENTO'
    end
    object ConsultaCOBRANCA: TStringField
      FieldName = 'COBRANCA'
      FixedChar = True
      Size = 2
    end
    object ConsultaEMOLUMENTOS: TFloatField
      FieldName = 'EMOLUMENTOS'
    end
    object ConsultaFETJ: TFloatField
      FieldName = 'FETJ'
    end
    object ConsultaFUNDPERJ: TFloatField
      FieldName = 'FUNDPERJ'
    end
    object ConsultaFUNPERJ: TFloatField
      FieldName = 'FUNPERJ'
    end
    object ConsultaFUNARPEN: TFloatField
      FieldName = 'FUNARPEN'
    end
    object ConsultaPMCMV: TFloatField
      FieldName = 'PMCMV'
    end
    object ConsultaISS: TFloatField
      FieldName = 'ISS'
    end
    object ConsultaMUTUA: TFloatField
      FieldName = 'MUTUA'
    end
    object ConsultaDISTRIBUICAO: TFloatField
      FieldName = 'DISTRIBUICAO'
    end
    object ConsultaACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
    end
    object ConsultaTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
    object ConsultaTIPO_PROTESTO: TIntegerField
      FieldName = 'TIPO_PROTESTO'
    end
    object ConsultaTIPO_TITULO: TIntegerField
      FieldName = 'TIPO_TITULO'
    end
    object ConsultaNUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
    end
    object ConsultaDT_TITULO: TDateField
      FieldName = 'DT_TITULO'
    end
    object ConsultaBANCO: TStringField
      FieldName = 'BANCO'
    end
    object ConsultaAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 10
    end
    object ConsultaCONTA: TStringField
      FieldName = 'CONTA'
      Size = 10
    end
    object ConsultaVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
    end
    object ConsultaSALDO_PROTESTO: TFloatField
      FieldName = 'SALDO_PROTESTO'
    end
    object ConsultaDT_VENCIMENTO: TDateField
      FieldName = 'DT_VENCIMENTO'
    end
    object ConsultaCONVENIO: TStringField
      FieldName = 'CONVENIO'
      FixedChar = True
      Size = 1
    end
    object ConsultaTIPO_APRESENTACAO: TStringField
      FieldName = 'TIPO_APRESENTACAO'
      FixedChar = True
      Size = 1
    end
    object ConsultaDT_ENVIO: TDateField
      FieldName = 'DT_ENVIO'
    end
    object ConsultaCODIGO_APRESENTANTE: TStringField
      FieldName = 'CODIGO_APRESENTANTE'
      Size = 10
    end
    object ConsultaTIPO_INTIMACAO: TStringField
      FieldName = 'TIPO_INTIMACAO'
      FixedChar = True
      Size = 1
    end
    object ConsultaMOTIVO_INTIMACAO: TMemoField
      FieldName = 'MOTIVO_INTIMACAO'
      BlobType = ftMemo
    end
    object ConsultaDT_INTIMACAO: TDateField
      FieldName = 'DT_INTIMACAO'
    end
    object ConsultaDT_PUBLICACAO: TDateField
      FieldName = 'DT_PUBLICACAO'
    end
    object ConsultaVALOR_PAGAMENTO: TFloatField
      FieldName = 'VALOR_PAGAMENTO'
    end
    object ConsultaAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Size = 100
    end
    object ConsultaCPF_CNPJ_APRESENTANTE: TStringField
      FieldName = 'CPF_CNPJ_APRESENTANTE'
      Size = 15
    end
    object ConsultaTIPO_APRESENTANTE: TStringField
      FieldName = 'TIPO_APRESENTANTE'
      FixedChar = True
      Size = 1
    end
    object ConsultaCEDENTE: TStringField
      FieldName = 'CEDENTE'
      Size = 100
    end
    object ConsultaNOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Size = 15
    end
    object ConsultaSACADOR: TStringField
      FieldName = 'SACADOR'
      Size = 100
    end
    object ConsultaDEVEDOR: TStringField
      FieldName = 'DEVEDOR'
      Size = 100
    end
    object ConsultaCPF_CNPJ_DEVEDOR: TStringField
      FieldName = 'CPF_CNPJ_DEVEDOR'
      Size = 15
    end
    object ConsultaTIPO_DEVEDOR: TStringField
      FieldName = 'TIPO_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object ConsultaAGENCIA_CEDENTE: TStringField
      FieldName = 'AGENCIA_CEDENTE'
      Size = 15
    end
    object ConsultaPRACA_PROTESTO: TStringField
      FieldName = 'PRACA_PROTESTO'
    end
    object ConsultaTIPO_ENDOSSO: TStringField
      FieldName = 'TIPO_ENDOSSO'
      FixedChar = True
      Size = 1
    end
    object ConsultaACEITE: TStringField
      FieldName = 'ACEITE'
      FixedChar = True
      Size = 1
    end
    object ConsultaCPF_ESCREVENTE: TStringField
      FieldName = 'CPF_ESCREVENTE'
      Size = 11
    end
    object ConsultaCPF_ESCREVENTE_PG: TStringField
      FieldName = 'CPF_ESCREVENTE_PG'
      Size = 11
    end
    object ConsultaOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      BlobType = ftMemo
    end
    object ConsultaDT_SUSTADO: TDateField
      FieldName = 'DT_SUSTADO'
    end
    object ConsultaDT_RETIRADO: TDateField
      FieldName = 'DT_RETIRADO'
    end
    object ConsultaSTATUS: TStringField
      FieldName = 'STATUS'
    end
    object ConsultaPROTESTADO: TStringField
      FieldName = 'PROTESTADO'
      FixedChar = True
      Size = 1
    end
    object ConsultaENVIADO_APONTAMENTO: TStringField
      FieldName = 'ENVIADO_APONTAMENTO'
      FixedChar = True
      Size = 1
    end
    object ConsultaENVIADO_PROTESTO: TStringField
      FieldName = 'ENVIADO_PROTESTO'
      FixedChar = True
      Size = 1
    end
    object ConsultaENVIADO_PAGAMENTO: TStringField
      FieldName = 'ENVIADO_PAGAMENTO'
      FixedChar = True
      Size = 1
    end
    object ConsultaENVIADO_RETIRADO: TStringField
      FieldName = 'ENVIADO_RETIRADO'
      FixedChar = True
      Size = 1
    end
    object ConsultaENVIADO_SUSTADO: TStringField
      FieldName = 'ENVIADO_SUSTADO'
      FixedChar = True
      Size = 1
    end
    object ConsultaENVIADO_DEVOLVIDO: TStringField
      FieldName = 'ENVIADO_DEVOLVIDO'
      FixedChar = True
      Size = 1
    end
    object ConsultaEXPORTADO: TStringField
      FieldName = 'EXPORTADO'
      FixedChar = True
      Size = 1
    end
    object ConsultaAVALISTA_DEVEDOR: TStringField
      FieldName = 'AVALISTA_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object ConsultaFINS_FALIMENTARES: TStringField
      FieldName = 'FINS_FALIMENTARES'
      FixedChar = True
      Size = 1
    end
    object ConsultaTARIFA_BANCARIA: TFloatField
      FieldName = 'TARIFA_BANCARIA'
    end
    object ConsultaFORMA_PAGAMENTO: TStringField
      FieldName = 'FORMA_PAGAMENTO'
      FixedChar = True
      Size = 2
    end
    object ConsultaNUMERO_PAGAMENTO: TStringField
      FieldName = 'NUMERO_PAGAMENTO'
      Size = 40
    end
    object ConsultaARQUIVO: TStringField
      FieldName = 'ARQUIVO'
    end
    object ConsultaRETORNO: TStringField
      FieldName = 'RETORNO'
    end
    object ConsultaDT_DEVOLVIDO: TDateField
      FieldName = 'DT_DEVOLVIDO'
    end
    object ConsultaCPF_CNPJ_SACADOR: TStringField
      FieldName = 'CPF_CNPJ_SACADOR'
      Size = 14
    end
    object ConsultaID_MSG: TIntegerField
      FieldName = 'ID_MSG'
    end
    object ConsultaVALOR_AR: TFloatField
      FieldName = 'VALOR_AR'
    end
    object ConsultaELETRONICO: TStringField
      FieldName = 'ELETRONICO'
      FixedChar = True
      Size = 1
    end
    object ConsultaIRREGULARIDADE: TIntegerField
      FieldName = 'IRREGULARIDADE'
    end
    object ConsultaAVISTA: TStringField
      FieldName = 'AVISTA'
      FixedChar = True
      Size = 1
    end
    object ConsultaSALDO_TITULO: TFloatField
      FieldName = 'SALDO_TITULO'
    end
    object ConsultaTIPO_SUSTACAO: TStringField
      FieldName = 'TIPO_SUSTACAO'
      FixedChar = True
      Size = 1
    end
    object ConsultaRETORNO_PROTESTO: TStringField
      FieldName = 'RETORNO_PROTESTO'
      FixedChar = True
      Size = 1
    end
    object ConsultaDT_RETORNO_PROTESTO: TDateField
      FieldName = 'DT_RETORNO_PROTESTO'
    end
    object ConsultaDT_DEFINITIVA: TDateField
      FieldName = 'DT_DEFINITIVA'
    end
    object ConsultaJUDICIAL: TStringField
      FieldName = 'JUDICIAL'
      FixedChar = True
      Size = 1
    end
    object ConsultaALEATORIO_PROTESTO: TStringField
      FieldName = 'ALEATORIO_PROTESTO'
      Size = 3
    end
    object ConsultaALEATORIO_SOLUCAO: TStringField
      FieldName = 'ALEATORIO_SOLUCAO'
      Size = 3
    end
    object ConsultaCCT: TStringField
      FieldName = 'CCT'
      Size = 9
    end
    object ConsultaDETERMINACAO: TStringField
      FieldName = 'DETERMINACAO'
      Size = 100
    end
    object ConsultaANTIGO: TStringField
      FieldName = 'ANTIGO'
      FixedChar = True
      Size = 1
    end
    object ConsultaLETRA: TStringField
      FieldName = 'LETRA'
      Size = 1
    end
    object ConsultaPROTOCOLO_CARTORIO: TIntegerField
      FieldName = 'PROTOCOLO_CARTORIO'
    end
    object ConsultaARQUIVO_CARTORIO: TStringField
      FieldName = 'ARQUIVO_CARTORIO'
    end
    object ConsultaRETORNO_CARTORIO: TStringField
      FieldName = 'RETORNO_CARTORIO'
    end
    object ConsultaDT_DEVOLVIDO_CARTORIO: TDateField
      FieldName = 'DT_DEVOLVIDO_CARTORIO'
    end
    object ConsultaCARTORIO: TIntegerField
      FieldName = 'CARTORIO'
    end
    object ConsultaDT_PROTOCOLO_CARTORIO: TDateField
      FieldName = 'DT_PROTOCOLO_CARTORIO'
    end
    object ConsultaID_SERVENTIA_AGREGADA: TIntegerField
      FieldName = 'ID_SERVENTIA_AGREGADA'
      ReadOnly = True
    end
    object ConsultaCODIGO_1: TIntegerField
      FieldName = 'CODIGO_1'
      ReadOnly = True
    end
    object ConsultaDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      ReadOnly = True
      Size = 100
    end
    object ConsultaCODIGO_DISTRIBUICAO: TIntegerField
      FieldName = 'CODIGO_DISTRIBUICAO'
      ReadOnly = True
    end
    object ConsultaVALOR_ULTIMO_SALDO: TFloatField
      FieldName = 'VALOR_ULTIMO_SALDO'
      ReadOnly = True
    end
    object ConsultaQTD_ULTIMO_SALDO: TIntegerField
      FieldName = 'QTD_ULTIMO_SALDO'
      ReadOnly = True
    end
    object ConsultaDATA_ULTIMO_SALDO: TDateField
      FieldName = 'DATA_ULTIMO_SALDO'
      ReadOnly = True
    end
    object ConsultaFLG_ATIVA: TStringField
      FieldName = 'FLG_ATIVA'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object ConsultaDATA_INATIVACAO: TDateField
      FieldName = 'DATA_INATIVACAO'
      ReadOnly = True
    end
    object ConsultaFLG_USAPROTESTO: TStringField
      FieldName = 'FLG_USAPROTESTO'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object ConsultaLOGRADOURO: TStringField
      FieldName = 'LOGRADOURO'
      ReadOnly = True
      Size = 300
    end
    object ConsultaCIDADE_LOGRADOURO: TStringField
      FieldName = 'CIDADE_LOGRADOURO'
      ReadOnly = True
      Size = 150
    end
    object ConsultaUF_LOGRADOURO: TStringField
      FieldName = 'UF_LOGRADOURO'
      ReadOnly = True
      FixedChar = True
      Size = 2
    end
    object ConsultaCEP: TStringField
      FieldName = 'CEP'
      ReadOnly = True
      Size = 10
    end
    object ConsultaNOME_TITULAR: TStringField
      FieldName = 'NOME_TITULAR'
      ReadOnly = True
      Size = 250
    end
    object ConsultaTELEFONE1: TStringField
      FieldName = 'TELEFONE1'
      ReadOnly = True
      Size = 14
    end
    object ConsultaTELEFONE2: TStringField
      FieldName = 'TELEFONE2'
      ReadOnly = True
      Size = 14
    end
    object ConsultaEMAIL1: TStringField
      FieldName = 'EMAIL1'
      ReadOnly = True
      Size = 300
    end
    object ConsultaEMAIL2: TStringField
      FieldName = 'EMAIL2'
      ReadOnly = True
      Size = 300
    end
    object ConsultaDIAS_AVISTA: TIntegerField
      FieldName = 'DIAS_AVISTA'
    end
  end
  object dsConsulta: TDataSource
    DataSet = Consulta
    OnDataChange = dsConsultaDataChange
    Left = 504
    Top = 393
  end
  object PM: TPopupMenu
    OnPopup = PMPopup
    Left = 552
    Top = 393
    object Detalhes: TMenuItem
      Bitmap.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000A0A000660B800107DF25555743D000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000A0A000662BF002AB0FF0030B8FF555A9DA6000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000A0A000564BF0023B9FF002DBBFF001687F455557438000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0A0A000465BF001DC3FF0025C7FF00138BF400001F3F00000000000000000000
        0000000000000010221E0000000000000000000000000019331E00000A0A0003
        67BF0016CCFF001ED1FF001090F400001F3F0000000000000000000000000000
        000000000000044F9CE20019332300000000002A554C084F9BC1000162B60010
        D1FF0018DBFF000D95F400001F3F000000000000000000000000000000000000
        0000000000005692C1E93577B3D9002A554C5697C3F4173286FD000CD6FF0012
        E4FF000B9FF90000344800000000000000000000000000000000001933280019
        3321001933154C8BBEE0BFEDF8FF62A1C9FF3D9953FF036133FF0013DBFF000A
        A9FF0018676D0000000000000000000000000000000000000000004388A45C97
        C4FF7BB2D5FF8CC2DFFFC1F3F9FF419D57FF20B230FF18AD24FF055C2BFF2053
        9BFD0032666A0000000000000000000000000000000000000000000811010842
        7C9197CBE4FFC5F6FFFF59AC76FF35C750FF32CB4BFF179823FF247881F90133
        675A000000000000000000000000000000000000000000000000000000000019
        33314A8CC0F7BEF4FFFF47A564FF45DA67FF24A537FF68BBACFF1C68ACD80008
        11110000000000000000000000000000000000000000000000000019333F5192
        C4F0BDF2FEFFB3EDFCFFA3E4EDFF4DA576FF76C7BDFF99E4FCFF92DDF8FF2672
        B2D90019331C00000000000000000000000000000000000000000032668B0A3C
        6C910032667F3A83BCE9A7ECFFFF8FD8F4FF9DE8FFFF115494CF0032667F083B
        6C940032666A0000000000000000000000000000000000000000000000000000
        000000000000438DC3E961AAD6FD064A8DB87DC8EAFF1A4C77AA000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000001B67ABF00234686C000811010E5192A8115494B6000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000557FAA730000000000000000555D66055587BB6C000000000000
        0000000000000000000000000000000000000000000000000000}
      Caption = 'Alterar'
      OnClick = DetalhesClick
    end
    object Excluir: TMenuItem
      Bitmap.Data = {
        B2050000424DB205000000000000360400002800000012000000130000000100
        0800000000007C010000C30E0000C30E00000001000000000000000000007B00
        0000BD000000FF0000007B7B000000FF0000FFFF000000315A0008427300084A
        730000007B007B007B00084A7B0008527B0021527B00007B7B007B7B7B00004A
        840008528400105284002963840008528C00085A8C00185A8C0018638C002963
        8C0008529400185A9400296B9400316B94002973940031739400085A9C001863
        9C0018739C0029739C0031739C0031849C00005AA5002173A5003973A500297B
        A500186BAD00106BB500317BB5002984B5004A8CB500086BBD000073BD001073
        BD001873BD00217BBD004284BD00318CBD004A8CBD005294BD005A9CBD00BDBD
        BD001073C600007BC600217BC600428CC6004A8CC6005A9CC6000084CE000884
        CE00398CCE005A9CCE00639CCE001884D6002184D600088CD600108CD6004294
        D600399CD6004A9CD600529CD6005A9CD6005AA5D60063ADD6001884DE002184
        DE00188CDE001094DE001894DE003194DE00299CDE004AA5DE005AA5DE004AB5
        DE00189CE700219CE700429CE7005AA5E7005AADE70063ADE7006BB5E70021A5
        EF0029A5EF0031A5EF0063B5EF006BBDEF0029ADF70031ADF7006BBDF7000000
        FF00FF00FF0031B5FF0039B5FF0042B5FF0039BDFF0042BDFF0042C6FF004AC6
        FF006BC6FF004ACEFF0052CEFF0052D6FF005ADEFF0063E7FF0000FFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00393939393939
        3939393939393939393939690000396939393939393939393939393939396939
        0000396969393910101010101010101010693939000039696969000000000000
        0000000069693939000039396969007879797978797979696939393900003939
        3969697903030303030379691039393900003939396969697978797979786969
        1039393900003939393969696903030369696900103939390000393939390069
        6969797869697900103939390000393939390079036969696903790010393939
        0000393939390079796969696978790010393939000039393939007903696969
        6900000010393939000039393939007869697900696979001039393900003939
        3939006969697900696969693939393900003939396969697978790079003969
        6939393900003969696969000000000000393939696939390000396969693939
        3939393939393939393969390000393939393939393939393939393939393969
        00003939393939393939393939393939393939390000}
      Caption = 'Excluir'
      OnClick = ExcluirClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object R2: TMenuItem
      Bitmap.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FFFFFF00FFFF
        FF00B3B3B3FFA7A7A7FFA5A5A5FFA5A5A5FFA5A5A5FFA5A5A5FFA5A5A5FFA5A5
        A5FFA5A5A5FF969696FF878787FF878787CF87878750FFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFDFDFDFFFBFBFBFFFBFBFBFFFBFBFBFFFBFBFBFFFBFBFBFFFBFB
        FBFFFAFAFAFFF3F3F3FFEBEBEBFFC8C8C8FF878787EFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFF8F8F8FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0
        F0FFF0F0F0FFEEEEEEFFE0E0E0FFEBEBEBFF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFE8DED4FFE7DCD2FFE7DCD2FFE7DCD2FFE7DCD2FFE7DCD2FFE7DC
        D2FFE7DCD2FFE7DCD2FFE6DBD2FFE5DAD1FF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFBFBFBFFF8F8F8FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8
        F8FFF8F8F8FFF8F8F8FFF8F8F8FFFBFBFBFF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFE9DFD5FFE8DED4FFE8DED4FFE8DED4FFE8DED4FFE8DED4FFE8DE
        D4FFE8DED4FFE8DED4FFE8DED4FFE9DFD5FF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFEFEFEFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFD
        FDFFFDFDFDFFFDFDFDFFFDFDFDFFFEFEFEFF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFEADFD6FFE9DFD5FFE9DFD5FFE9DFD5FFE9DFD5FFE9DFD5FFE9DF
        D5FFE9DFD5FFE9DFD5FFE9DFD5FFEADFD6FF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADF
        D6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADF
        D6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFFFFFFFFF6F6F6FFFFFFFFFFFAFAFAFFFAFAFAFFFFFFFFFFFAFA
        FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8CFFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFB6B6B6FFB6B6B6FFB6B6B6FFB6B6B6FFB6B6B6FFB6B6B6FFB6B6
        B6FFB6B6B6FFB6B6B6FFB6B6B6FFB6B6B6FFACACACFFFFFFFF00}
      Caption = 'Recibo de Apontamento'
      object M2: TMenuItem
        Caption = 'Matricial'
        OnClick = M2Click
      end
      object J1: TMenuItem
        Caption = 'Jato de Tinta'
        OnClick = J1Click
      end
    end
    object RecibodeBaixa1: TMenuItem
      Bitmap.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FFFFFF00FFFF
        FF00B3B3B3FFA7A7A7FFA5A5A5FFA5A5A5FFA5A5A5FFA5A5A5FFA5A5A5FFA5A5
        A5FFA5A5A5FF969696FF878787FF878787CF87878750FFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFDFDFDFFFBFBFBFFFBFBFBFFFBFBFBFFFBFBFBFFFBFBFBFFFBFB
        FBFFFAFAFAFFF3F3F3FFEBEBEBFFC8C8C8FF878787EFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFF8F8F8FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0
        F0FFF0F0F0FFEEEEEEFFE0E0E0FFEBEBEBFF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFE8DED4FFE7DCD2FFE7DCD2FFE7DCD2FFE7DCD2FFE7DCD2FFE7DC
        D2FFE7DCD2FFE7DCD2FFE6DBD2FFE5DAD1FF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFBFBFBFFF8F8F8FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8
        F8FFF8F8F8FFF8F8F8FFF8F8F8FFFBFBFBFF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFE9DFD5FFE8DED4FFE8DED4FFE8DED4FFE8DED4FFE8DED4FFE8DE
        D4FFE8DED4FFE8DED4FFE8DED4FFE9DFD5FF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFEFEFEFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFD
        FDFFFDFDFDFFFDFDFDFFFDFDFDFFFEFEFEFF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFEADFD6FFE9DFD5FFE9DFD5FFE9DFD5FFE9DFD5FFE9DFD5FFE9DF
        D5FFE9DFD5FFE9DFD5FFE9DFD5FFEADFD6FF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADF
        D6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADF
        D6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF878787FFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFFFFFFFFFF6F6F6FFFFFFFFFFFAFAFAFFFAFAFAFFFFFFFFFFFAFA
        FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8CFFFFFFFF00FFFFFF00FFFF
        FF00B6B6B6FFB6B6B6FFB6B6B6FFB6B6B6FFB6B6B6FFB6B6B6FFB6B6B6FFB6B6
        B6FFB6B6B6FFB6B6B6FFB6B6B6FFB6B6B6FFACACACFFFFFFFF00}
      Caption = 'Recibo de Baixa'
      OnClick = RecibodeBaixa1Click
    end
    object MenuEtiqueta: TMenuItem
      Bitmap.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000B3B3B3FFA7A7A7FFA5A5A5FFA5A5A5FFA5A5A5FFA5A5A5FFA5A5A5FFA5A5
        A5FFA5A5A5FF969696FF878787FF878787CF8787875000000000000000000000
        0000B6B6B6FFFDFDFDFFFBFBFBFFFBFBFBFFFBFBFBFFFBFBFBFFFBFBFBFFFBFB
        FBFFFAFAFAFFF3F3F3FFEBEBEBFFC8C8C8FF878787EF00000000000000000000
        0000B6B6B6FFF8F8F8FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0F0FFF0F0
        F0FFF0F0F0FFEEEEEEFFE0E0E0FFEBEBEBFF878787FF00000000000000000000
        0000B6B6B6FFE8DED4FFE7DCD2FFE7DCD2FFE7DCD2FFE7DCD2FFE7DCD2FFE7DC
        D2FFE7DCD2FFE7DCD2FFE6DBD2FFE5DAD1FF878787FF00000000000000000000
        0000B6B6B6FFFBFBFBFFF8F8F8FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8
        F8FFF8F8F8FFF8F8F8FFF8F8F8FFFBFBFBFF878787FF00000000000000000000
        0000B6B6B6FFE9DFD5FFE8DED4FFE8DED4FFE8DED4FFE8DED4FFE8DED4FFE8DE
        D4FFE8DED4FFE8DED4FFE8DED4FFE9DFD5FF878787FF00000000000000000000
        0000B6B6B6FFFEFEFEFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFD
        FDFFFDFDFDFFFDFDFDFFFDFDFDFFFEFEFEFF878787FF00000000000000000000
        0000B6B6B6FFEADFD6FFE9DFD5FFE9DFD5FFE9DFD5FFE9DFD5FFE9DFD5FFE9DF
        D5FFE9DFD5FFE9DFD5FFE9DFD5FFEADFD6FF878787FF00000000000000000000
        0000B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF878787FF00000000000000000000
        0000B6B6B6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADF
        D6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FF878787FF00000000000000000000
        0000B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF878787FF00000000000000000000
        0000B6B6B6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FFEADF
        D6FFEADFD6FFEADFD6FFEADFD6FFEADFD6FF878787FF00000000000000000000
        0000B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF878787FF00000000000000000000
        0000B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF878787FF00000000000000000000
        0000B6B6B6FFFFFFFFFFF6F6F6FFFFFFFFFFFAFAFAFFFAFAFAFFFFFFFFFFFAFA
        FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8CFF00000000000000000000
        0000B6B6B6FFB6B6B6FFB6B6B6FFB6B6B6FFB6B6B6FFB6B6B6FFB6B6B6FFB6B6
        B6FFB6B6B6FFB6B6B6FFB6B6B6FFB6B6B6FFACACACFF00000000}
      Caption = 'Etiqueta de Averba'#231#227'o de Retifica'#231#227'o'
      OnClick = MenuEtiquetaClick
    end
  end
  object dsTitulos: TDataSource
    DataSet = dm.Titulos
    Left = 392
    Top = 448
  end
  object dsObs: TDataSource
    Left = 496
    Top = 448
  end
  object sSkinProvider: TsSkinProvider
    AddedTitle.Font.Charset = DEFAULT_CHARSET
    AddedTitle.Font.Color = clNone
    AddedTitle.Font.Height = -11
    AddedTitle.Font.Name = 'MS Sans Serif'
    AddedTitle.Font.Style = []
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 552
    Top = 448
  end
  object Obs: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT ID_ATO, OBSERVACAO'
      '  FROM TITULOS'
      ' WHERE ID_ATO = :ID_ATO')
    Left = 448
    Top = 449
    ParamData = <
      item
        Name = 'ID_ATO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object ObsID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ObsOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      BlobType = ftMemo
    end
  end
  object qryConsulta: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT T.*,'
      '       SA.*'
      '  FROM TITULOS T'
      '  LEFT JOIN SERVENTIA_AGREGADA SA'
      '    ON T.CARTORIO = SA.CODIGO_DISTRIBUICAO'
      'ORDER BY PROTOCOLO')
    Left = 328
    Top = 393
  end
  object qryTitulo: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT T.*,'
      '       SA.*,'
      '       D.ENDERECO AS END_DEV,'
      '       D.BAIRRO AS BAIRRO_DEV,'
      '       D.MUNICIPIO AS MUN_DEV,'
      '       D.UF AS UF_DEV,'
      '       D.CEP AS CEP_DEV'
      '  FROM TITULOS T'
      '  LEFT JOIN SERVENTIA_AGREGADA SA'
      '    ON T.CARTORIO = SA.CODIGO_DISTRIBUICAO'
      '  LEFT JOIN DEVEDORES D'
      '    ON T.ID_ATO = D.ID_ATO'
      ' WHERE T.ID_ATO = :ID_ATO')
    Left = 328
    Top = 449
    ParamData = <
      item
        Name = 'ID_ATO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryTituloSTATUS: TStringField
      FieldName = 'STATUS'
      Origin = 'STATUS'
    end
    object qryTituloDT_PROTOCOLO: TDateField
      FieldName = 'DT_PROTOCOLO'
      Origin = 'DT_PROTOCOLO'
    end
    object qryTituloDT_INTIMACAO: TDateField
      FieldName = 'DT_INTIMACAO'
      Origin = 'DT_INTIMACAO'
    end
    object qryTituloDT_REGISTRO: TDateField
      FieldName = 'DT_REGISTRO'
      Origin = 'DT_REGISTRO'
    end
    object qryTituloDT_PAGAMENTO: TDateField
      FieldName = 'DT_PAGAMENTO'
      Origin = 'DT_PAGAMENTO'
    end
    object qryTituloDT_RETIRADO: TDateField
      FieldName = 'DT_RETIRADO'
      Origin = 'DT_RETIRADO'
    end
    object qryTituloDT_SUSTADO: TDateField
      FieldName = 'DT_SUSTADO'
      Origin = 'DT_SUSTADO'
    end
    object qryTituloDT_PRAZO: TDateField
      FieldName = 'DT_PRAZO'
      Origin = 'DT_PRAZO'
    end
    object qryTituloDT_TITULO: TDateField
      FieldName = 'DT_TITULO'
      Origin = 'DT_TITULO'
    end
    object qryTituloDT_VENCIMENTO: TDateField
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
    end
    object qryTituloDT_DEVOLVIDO: TDateField
      FieldName = 'DT_DEVOLVIDO'
      Origin = 'DT_DEVOLVIDO'
    end
    object qryTituloDT_PUBLICACAO: TDateField
      FieldName = 'DT_PUBLICACAO'
      Origin = 'DT_PUBLICACAO'
    end
    object qryTituloTIPO_INTIMACAO: TStringField
      FieldName = 'TIPO_INTIMACAO'
      Origin = 'TIPO_INTIMACAO'
      FixedChar = True
      Size = 1
    end
    object qryTituloMOTIVO_INTIMACAO: TMemoField
      FieldName = 'MOTIVO_INTIMACAO'
      Origin = 'MOTIVO_INTIMACAO'
      BlobType = ftMemo
    end
    object qryTituloPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object qryTituloTIPO_TITULO: TIntegerField
      FieldName = 'TIPO_TITULO'
      Origin = 'TIPO_TITULO'
    end
    object qryTituloNUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
      Origin = 'NUMERO_TITULO'
    end
    object qryTituloVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
      Origin = 'VALOR_TITULO'
    end
    object qryTituloTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
    end
    object qryTituloSALDO_PROTESTO: TFloatField
      FieldName = 'SALDO_PROTESTO'
      Origin = 'SALDO_PROTESTO'
    end
    object qryTituloSACADOR: TStringField
      FieldName = 'SACADOR'
      Origin = 'SACADOR'
      Size = 100
    end
    object qryTituloCEDENTE: TStringField
      FieldName = 'CEDENTE'
      Origin = 'CEDENTE'
      Size = 100
    end
    object qryTituloOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      BlobType = ftMemo
    end
    object qryTituloTARIFA_BANCARIA: TFloatField
      FieldName = 'TARIFA_BANCARIA'
      Origin = 'TARIFA_BANCARIA'
    end
    object qryTituloVALOR_AR: TFloatField
      FieldName = 'VALOR_AR'
      Origin = 'VALOR_AR'
    end
    object qryTituloIRREGULARIDADE: TIntegerField
      FieldName = 'IRREGULARIDADE'
      Origin = 'IRREGULARIDADE'
    end
    object qryTituloLIVRO_REGISTRO: TIntegerField
      FieldName = 'LIVRO_REGISTRO'
      Origin = 'LIVRO_REGISTRO'
    end
    object qryTituloFOLHA_REGISTRO: TStringField
      FieldName = 'FOLHA_REGISTRO'
      Origin = 'FOLHA_REGISTRO'
      Size = 10
    end
    object qryTituloTIPO_APRESENTACAO: TStringField
      FieldName = 'TIPO_APRESENTACAO'
      Origin = 'TIPO_APRESENTACAO'
      FixedChar = True
      Size = 1
    end
    object qryTituloDISTRIBUICAO: TFloatField
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
    end
    object qryTituloSALDO_TITULO: TFloatField
      FieldName = 'SALDO_TITULO'
      Origin = 'SALDO_TITULO'
    end
    object qryTituloLIVRO_PROTOCOLO: TIntegerField
      FieldName = 'LIVRO_PROTOCOLO'
      Origin = 'LIVRO_PROTOCOLO'
    end
    object qryTituloFOLHA_PROTOCOLO: TStringField
      FieldName = 'FOLHA_PROTOCOLO'
      Origin = 'FOLHA_PROTOCOLO'
      Size = 10
    end
    object qryTituloTIPO_DEVEDOR: TStringField
      FieldName = 'TIPO_DEVEDOR'
      Origin = 'TIPO_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object qryTituloCPF_CNPJ_DEVEDOR: TStringField
      FieldName = 'CPF_CNPJ_DEVEDOR'
      Origin = 'CPF_CNPJ_DEVEDOR'
      Size = 15
    end
    object qryTituloPROTOCOLO_CARTORIO: TIntegerField
      FieldName = 'PROTOCOLO_CARTORIO'
      Origin = 'PROTOCOLO_CARTORIO'
    end
    object qryTituloARQUIVO: TStringField
      FieldName = 'ARQUIVO'
      Origin = 'ARQUIVO'
    end
    object qryTituloARQUIVO_CARTORIO: TStringField
      FieldName = 'ARQUIVO_CARTORIO'
      Origin = 'ARQUIVO_CARTORIO'
    end
    object qryTituloRETORNO_CARTORIO: TStringField
      FieldName = 'RETORNO_CARTORIO'
      Origin = 'RETORNO_CARTORIO'
    end
    object qryTituloRETORNO: TStringField
      FieldName = 'RETORNO'
      Origin = 'RETORNO'
    end
    object qryTituloCARTORIO: TIntegerField
      FieldName = 'CARTORIO'
      Origin = 'CARTORIO'
    end
    object qryTituloDT_PROTOCOLO_CARTORIO: TDateField
      FieldName = 'DT_PROTOCOLO_CARTORIO'
      Origin = 'DT_PROTOCOLO_CARTORIO'
    end
    object qryTituloDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object qryTituloSERVENTIA_AGREGADA: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'SERVENTIA_AGREGADA'
      Size = 100
    end
    object qryTituloID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryTituloCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
    end
    object qryTituloRECIBO: TIntegerField
      FieldName = 'RECIBO'
      Origin = 'RECIBO'
    end
    object qryTituloDT_ENTRADA: TDateField
      FieldName = 'DT_ENTRADA'
      Origin = 'DT_ENTRADA'
    end
    object qryTituloREGISTRO: TIntegerField
      FieldName = 'REGISTRO'
      Origin = 'REGISTRO'
    end
    object qryTituloSELO_REGISTRO: TStringField
      FieldName = 'SELO_REGISTRO'
      Origin = 'SELO_REGISTRO'
      Size = 9
    end
    object qryTituloSELO_PAGAMENTO: TStringField
      FieldName = 'SELO_PAGAMENTO'
      Origin = 'SELO_PAGAMENTO'
      Size = 9
    end
    object qryTituloRECIBO_PAGAMENTO: TIntegerField
      FieldName = 'RECIBO_PAGAMENTO'
      Origin = 'RECIBO_PAGAMENTO'
    end
    object qryTituloCOBRANCA: TStringField
      FieldName = 'COBRANCA'
      Origin = 'COBRANCA'
      FixedChar = True
      Size = 2
    end
    object qryTituloEMOLUMENTOS: TFloatField
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
    end
    object qryTituloFETJ: TFloatField
      FieldName = 'FETJ'
      Origin = 'FETJ'
    end
    object qryTituloFUNDPERJ: TFloatField
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
    end
    object qryTituloFUNPERJ: TFloatField
      FieldName = 'FUNPERJ'
      Origin = 'FUNPERJ'
    end
    object qryTituloFUNARPEN: TFloatField
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
    end
    object qryTituloPMCMV: TFloatField
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
    end
    object qryTituloISS: TFloatField
      FieldName = 'ISS'
      Origin = 'ISS'
    end
    object qryTituloMUTUA: TFloatField
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
    end
    object qryTituloACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
    end
    object qryTituloTIPO_PROTESTO: TIntegerField
      FieldName = 'TIPO_PROTESTO'
      Origin = 'TIPO_PROTESTO'
    end
    object qryTituloBANCO: TStringField
      FieldName = 'BANCO'
      Origin = 'BANCO'
    end
    object qryTituloAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Origin = 'AGENCIA'
      Size = 10
    end
    object qryTituloCONTA: TStringField
      FieldName = 'CONTA'
      Origin = 'CONTA'
      Size = 10
    end
    object qryTituloCONVENIO: TStringField
      FieldName = 'CONVENIO'
      Origin = 'CONVENIO'
      FixedChar = True
      Size = 1
    end
    object qryTituloDT_ENVIO: TDateField
      FieldName = 'DT_ENVIO'
      Origin = 'DT_ENVIO'
    end
    object qryTituloCODIGO_APRESENTANTE: TStringField
      FieldName = 'CODIGO_APRESENTANTE'
      Origin = 'CODIGO_APRESENTANTE'
      Size = 10
    end
    object qryTituloVALOR_PAGAMENTO: TFloatField
      FieldName = 'VALOR_PAGAMENTO'
      Origin = 'VALOR_PAGAMENTO'
    end
    object qryTituloAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Origin = 'APRESENTANTE'
      Size = 100
    end
    object qryTituloCPF_CNPJ_APRESENTANTE: TStringField
      FieldName = 'CPF_CNPJ_APRESENTANTE'
      Origin = 'CPF_CNPJ_APRESENTANTE'
      Size = 15
    end
    object qryTituloTIPO_APRESENTANTE: TStringField
      FieldName = 'TIPO_APRESENTANTE'
      Origin = 'TIPO_APRESENTANTE'
      FixedChar = True
      Size = 1
    end
    object qryTituloNOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Origin = 'NOSSO_NUMERO'
      Size = 15
    end
    object qryTituloDEVEDOR: TStringField
      FieldName = 'DEVEDOR'
      Origin = 'DEVEDOR'
      Size = 100
    end
    object qryTituloAGENCIA_CEDENTE: TStringField
      FieldName = 'AGENCIA_CEDENTE'
      Origin = 'AGENCIA_CEDENTE'
      Size = 15
    end
    object qryTituloPRACA_PROTESTO: TStringField
      FieldName = 'PRACA_PROTESTO'
      Origin = 'PRACA_PROTESTO'
    end
    object qryTituloTIPO_ENDOSSO: TStringField
      FieldName = 'TIPO_ENDOSSO'
      Origin = 'TIPO_ENDOSSO'
      FixedChar = True
      Size = 1
    end
    object qryTituloACEITE: TStringField
      FieldName = 'ACEITE'
      Origin = 'ACEITE'
      FixedChar = True
      Size = 1
    end
    object qryTituloCPF_ESCREVENTE: TStringField
      FieldName = 'CPF_ESCREVENTE'
      Origin = 'CPF_ESCREVENTE'
      Size = 11
    end
    object qryTituloCPF_ESCREVENTE_PG: TStringField
      FieldName = 'CPF_ESCREVENTE_PG'
      Origin = 'CPF_ESCREVENTE_PG'
      Size = 11
    end
    object qryTituloPROTESTADO: TStringField
      FieldName = 'PROTESTADO'
      Origin = 'PROTESTADO'
      FixedChar = True
      Size = 1
    end
    object qryTituloENVIADO_APONTAMENTO: TStringField
      FieldName = 'ENVIADO_APONTAMENTO'
      Origin = 'ENVIADO_APONTAMENTO'
      FixedChar = True
      Size = 1
    end
    object qryTituloENVIADO_PROTESTO: TStringField
      FieldName = 'ENVIADO_PROTESTO'
      Origin = 'ENVIADO_PROTESTO'
      FixedChar = True
      Size = 1
    end
    object qryTituloENVIADO_PAGAMENTO: TStringField
      FieldName = 'ENVIADO_PAGAMENTO'
      Origin = 'ENVIADO_PAGAMENTO'
      FixedChar = True
      Size = 1
    end
    object qryTituloENVIADO_RETIRADO: TStringField
      FieldName = 'ENVIADO_RETIRADO'
      Origin = 'ENVIADO_RETIRADO'
      FixedChar = True
      Size = 1
    end
    object qryTituloENVIADO_SUSTADO: TStringField
      FieldName = 'ENVIADO_SUSTADO'
      Origin = 'ENVIADO_SUSTADO'
      FixedChar = True
      Size = 1
    end
    object qryTituloENVIADO_DEVOLVIDO: TStringField
      FieldName = 'ENVIADO_DEVOLVIDO'
      Origin = 'ENVIADO_DEVOLVIDO'
      FixedChar = True
      Size = 1
    end
    object qryTituloEXPORTADO: TStringField
      FieldName = 'EXPORTADO'
      Origin = 'EXPORTADO'
      FixedChar = True
      Size = 1
    end
    object qryTituloAVALISTA_DEVEDOR: TStringField
      FieldName = 'AVALISTA_DEVEDOR'
      Origin = 'AVALISTA_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object qryTituloFINS_FALIMENTARES: TStringField
      FieldName = 'FINS_FALIMENTARES'
      Origin = 'FINS_FALIMENTARES'
      FixedChar = True
      Size = 1
    end
    object qryTituloFORMA_PAGAMENTO: TStringField
      FieldName = 'FORMA_PAGAMENTO'
      Origin = 'FORMA_PAGAMENTO'
      FixedChar = True
      Size = 2
    end
    object qryTituloNUMERO_PAGAMENTO: TStringField
      FieldName = 'NUMERO_PAGAMENTO'
      Origin = 'NUMERO_PAGAMENTO'
      Size = 40
    end
    object qryTituloCPF_CNPJ_SACADOR: TStringField
      FieldName = 'CPF_CNPJ_SACADOR'
      Origin = 'CPF_CNPJ_SACADOR'
      Size = 14
    end
    object qryTituloID_MSG: TIntegerField
      FieldName = 'ID_MSG'
      Origin = 'ID_MSG'
    end
    object qryTituloELETRONICO: TStringField
      FieldName = 'ELETRONICO'
      Origin = 'ELETRONICO'
      FixedChar = True
      Size = 1
    end
    object qryTituloAVISTA: TStringField
      FieldName = 'AVISTA'
      Origin = 'AVISTA'
      FixedChar = True
      Size = 1
    end
    object qryTituloTIPO_SUSTACAO: TStringField
      FieldName = 'TIPO_SUSTACAO'
      Origin = 'TIPO_SUSTACAO'
      FixedChar = True
      Size = 1
    end
    object qryTituloRETORNO_PROTESTO: TStringField
      FieldName = 'RETORNO_PROTESTO'
      Origin = 'RETORNO_PROTESTO'
      FixedChar = True
      Size = 1
    end
    object qryTituloDT_RETORNO_PROTESTO: TDateField
      FieldName = 'DT_RETORNO_PROTESTO'
      Origin = 'DT_RETORNO_PROTESTO'
    end
    object qryTituloDT_DEFINITIVA: TDateField
      FieldName = 'DT_DEFINITIVA'
      Origin = 'DT_DEFINITIVA'
    end
    object qryTituloJUDICIAL: TStringField
      FieldName = 'JUDICIAL'
      Origin = 'JUDICIAL'
      FixedChar = True
      Size = 1
    end
    object qryTituloALEATORIO_PROTESTO: TStringField
      FieldName = 'ALEATORIO_PROTESTO'
      Origin = 'ALEATORIO_PROTESTO'
      Size = 3
    end
    object qryTituloALEATORIO_SOLUCAO: TStringField
      FieldName = 'ALEATORIO_SOLUCAO'
      Origin = 'ALEATORIO_SOLUCAO'
      Size = 3
    end
    object qryTituloCCT: TStringField
      FieldName = 'CCT'
      Origin = 'CCT'
      Size = 9
    end
    object qryTituloDETERMINACAO: TStringField
      FieldName = 'DETERMINACAO'
      Origin = 'DETERMINACAO'
      Size = 100
    end
    object qryTituloANTIGO: TStringField
      FieldName = 'ANTIGO'
      Origin = 'ANTIGO'
      FixedChar = True
      Size = 1
    end
    object qryTituloLETRA: TStringField
      FieldName = 'LETRA'
      Origin = 'LETRA'
      Size = 1
    end
    object qryTituloDT_DEVOLVIDO_CARTORIO: TDateField
      FieldName = 'DT_DEVOLVIDO_CARTORIO'
      Origin = 'DT_DEVOLVIDO_CARTORIO'
    end
    object qryTituloID_SERVENTIA_AGREGADA: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ID_SERVENTIA_AGREGADA'
      Origin = 'ID_SERVENTIA_AGREGADA'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryTituloCODIGO_1: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'CODIGO_1'
      Origin = 'CODIGO'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryTituloCODIGO_DISTRIBUICAO: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'CODIGO_DISTRIBUICAO'
      Origin = 'CODIGO_DISTRIBUICAO'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryTituloVALOR_ULTIMO_SALDO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'VALOR_ULTIMO_SALDO'
      Origin = 'VALOR_ULTIMO_SALDO'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryTituloQTD_ULTIMO_SALDO: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'QTD_ULTIMO_SALDO'
      Origin = 'QTD_ULTIMO_SALDO'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryTituloDATA_ULTIMO_SALDO: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'DATA_ULTIMO_SALDO'
      Origin = 'DATA_ULTIMO_SALDO'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryTituloFLG_ATIVA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'FLG_ATIVA'
      Origin = 'FLG_ATIVA'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object qryTituloDATA_INATIVACAO: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'DATA_INATIVACAO'
      Origin = 'DATA_INATIVACAO'
      ProviderFlags = []
      ReadOnly = True
    end
    object qryTituloFLG_USAPROTESTO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'FLG_USAPROTESTO'
      Origin = 'FLG_USAPROTESTO'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object qryTituloLOGRADOURO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'LOGRADOURO'
      Origin = 'LOGRADOURO'
      ProviderFlags = []
      ReadOnly = True
      Size = 300
    end
    object qryTituloCIDADE_LOGRADOURO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CIDADE_LOGRADOURO'
      Origin = 'CIDADE_LOGRADOURO'
      ProviderFlags = []
      ReadOnly = True
      Size = 150
    end
    object qryTituloUF_LOGRADOURO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'UF_LOGRADOURO'
      Origin = 'UF_LOGRADOURO'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 2
    end
    object qryTituloCEP: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CEP'
      Origin = 'CEP'
      ProviderFlags = []
      ReadOnly = True
      Size = 10
    end
    object qryTituloNOME_TITULAR: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOME_TITULAR'
      Origin = 'NOME_TITULAR'
      ProviderFlags = []
      ReadOnly = True
      Size = 250
    end
    object qryTituloTELEFONE1: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'TELEFONE1'
      Origin = 'TELEFONE1'
      ProviderFlags = []
      ReadOnly = True
      Size = 14
    end
    object qryTituloTELEFONE2: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'TELEFONE2'
      Origin = 'TELEFONE2'
      ProviderFlags = []
      ReadOnly = True
      Size = 14
    end
    object qryTituloEMAIL1: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'EMAIL1'
      Origin = 'EMAIL1'
      ProviderFlags = []
      ReadOnly = True
      Size = 300
    end
    object qryTituloEMAIL2: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'EMAIL2'
      Origin = 'EMAIL2'
      ProviderFlags = []
      ReadOnly = True
      Size = 300
    end
    object qryTituloEND_DEV: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'END_DEV'
      Origin = 'ENDERECO'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object qryTituloBAIRRO_DEV: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'BAIRRO_DEV'
      Origin = 'BAIRRO'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object qryTituloMUN_DEV: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'MUN_DEV'
      Origin = 'MUNICIPIO'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object qryTituloUF_DEV: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'UF_DEV'
      Origin = 'UF'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 2
    end
    object qryTituloCEP_DEV: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'CEP_DEV'
      Origin = 'CEP'
      ProviderFlags = []
      ReadOnly = True
      Size = 8
    end
    object qryTituloDIAS_AVISTA: TIntegerField
      FieldName = 'DIAS_AVISTA'
      Origin = 'DIAS_AVISTA'
    end
  end
end
