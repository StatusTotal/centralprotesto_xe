unit UTitulo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, sPanel, DB, StdCtrls,
  sEdit, Mask, sMaskEdit, sCustomComboEdit, sTooledit, Buttons, sBitBtn,
  sGroupBox, sComboBox, DBCtrls, sDBLookupComboBox, sCurrEdit,
  sCurrencyEdit, sMemo, FMTBcd, SqlExpr, sSpeedButton, DBGrids,
  RXDBCtrl, acPNG, sButton, sDBComboBox, sDBEdit, sLabel, sCheckBox,
  acImage, RXCtrls, sSkinProvider, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, System.StrUtils, vcl.wwdbigrd, vcl.wwdbgrid,
  vcl.wwspeedbutton, vcl.wwdbnavigator, vcl.wwclearpanel;

type
  TFTitulo = class(TForm)
    btSalvar: TsBitBtn;
    btCancelar: TsBitBtn;
    GbProtocolo: TsGroupBox;
    edDataProtocoloD: TsDateEdit;
    edProtocoloD: TsEdit;
    edRecibo: TsEdit;
    cbEndosso: TsComboBox;
    GbTitulo: TsGroupBox;
    cbTipoApresentacao: TsComboBox;
    edValor: TsCurrencyEdit;
    lkEspecie: TsDBLookupComboBox;
    dsTipos: TDataSource;
    cbTipoProtesto: TsComboBox;
    edNumero: TsEdit;
    edBanco: TsEdit;
    edDataVencimento: TsDateEdit;
    cbTipoIntimacao: TsComboBox;
    edDataIntimacao: TsDateEdit;
    edTotal: TsCurrencyEdit;
    edDataPublicacao: TsDateEdit;
    sSpeedButton1: TsSpeedButton;
    edCustas: TsCurrencyEdit;
    dsRXDevedor: TDataSource;
    GbPartes: TsGroupBox;
    ImAviso1: TImage;
    ImOk1: TImage;
    edCedente: TsEdit;
    Image1: TImage;
    edLivroProtocolo: TsEdit;
    edFolhaProtocolo: TsEdit;
    P1: TsPanel;
    btIncluir: TsBitBtn;
    btAlterar: TsBitBtn;
    btExcluir: TsBitBtn;
    btPesquisar: TsBitBtn;
    edDataTitulo: TsDateEdit;
    btProximo: TsBitBtn;
    btAnterior: TsBitBtn;
    btLimpar: TsBitBtn;
    dsEscreventes: TDataSource;
    edDataEnvio: TsDateEdit;
    GbRegistro: TsGroupBox;
    edDataRegistro: TsDateEdit;
    edRegistro: TsEdit;
    edLivroRegistro: TsEdit;
    edFolhaRegistro: TsEdit;
    edSeloRegistro: TsEdit;
    lkEscrevente: TsDBLookupComboBox;
    btObs: TsBitBtn;
    ImTabela: TImage;
    lbStatus: TsLabel;
    edDataPagamento: TsDateEdit;
    edPagamento: TsCurrencyEdit;
    edDataSustacao: TsDateEdit;
    lkEscreventeP: TsDBLookupComboBox;
    edDataRetirada: TsDateEdit;
    dsRXPortador: TDataSource;
    cbTipoApr: TsDBComboBox;
    edDocApr: TsDBEdit;
    edCodigo: TsDBEdit;
    edApresentante: TsDBEdit;
    cbConv: TsDBComboBox;
    edSeloSolucao: TsEdit;
    sbSacador: TsSpeedButton;
    dsRxSacador: TDataSource;
    edSacador: TsDBEdit;
    edAgencia: TsEdit;
    edConta: TsEdit;
    btSair: TsBitBtn;
    edDataPrazo: TsDateEdit;
    ImGerar: TImage;
    edSaldo: TsCurrencyEdit;
    edLAE: TsEdit;
    Image2: TImage;
    edCCT: TsEdit;
    ckAntigo: TsCheckBox;
    cbAceite: TsComboBox;
    edAleatorioProtesto: TsEdit;
    edAleatorioSolucao: TsEdit;
    ImNovoCCT: TsImage;
    ImLimparCCT: TsImage;
    ImNovoSelo1: TsImage;
    ImLimparSelo1: TsImage;
    ImNovoSelo2: TsImage;
    ImLimparSelo2: TsImage;
    ImAle2: TsImage;
    ImAle1: TsImage;
    sbPortador: TsSpeedButton;
    sbLocalizar: TsSpeedButton;
    sSkinProvider: TsSkinProvider;
    ckFins: TsCheckBox;
    edtArquivoD: TsEdit;
    edtArquivoR: TsEdit;
    edtServentiaAgregada: TsEdit;
    lblServentiaAgregada: TLabel;
    qryTabela: TFDQuery;
    qryTabelaORDEM: TIntegerField;
    qryTabelaANO: TIntegerField;
    qryTabelaVAI: TStringField;
    qryTabelaTAB: TStringField;
    qryTabelaITEM: TStringField;
    qryTabelaSUB: TStringField;
    qryTabelaDESCR: TStringField;
    qryTabelaVALOR: TFloatField;
    qryTabelaTEXTO: TStringField;
    edDataProtocoloSA: TsDateEdit;
    edProtocoloSA: TsEdit;
    edtNossoNumero: TsEdit;
    dsMunicipios: TDataSource;
    lblVencimentoAVista: TLabel;
    btnDesfazerExportacaoSA: TsBitBtn;
    gbDevedor: TsGroupBox;
    ImAviso2: TImage;
    ImOk2: TImage;
    sbDevedores: TsSpeedButton;
    sbDevedor: TsSpeedButton;
    cbTipoDev: TsComboBox;
    edDocDev: TsMaskEdit;
    edDevedor: TsEdit;
    edtEndDevedor: TsEdit;
    edtBairroDevedor: TsEdit;
    cbUFDevedor: TsComboBox;
    medCEPDevedor: TsMaskEdit;
    lcbMunDevedor: TsDBLookupComboBox;
    dbgDevedores: TwwDBGrid;
    btnCancelarDev: TsSpeedButton;
    btnConfirmarDev: TsSpeedButton;
    btnExcluirDev: TsSpeedButton;
    btnEditarDev: TsSpeedButton;
    btnIncluirDev: TsSpeedButton;
    ckPostecipado: TsCheckBox;
    procedure btCancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);
    procedure btSalvarClick(Sender: TObject);
    procedure edValorExit(Sender: TObject);
    procedure sSpeedButton1Click(Sender: TObject);
    procedure edValorClick(Sender: TObject);
    procedure edTotalClick(Sender: TObject);
    procedure edCustasClick(Sender: TObject);
    procedure edSeloRegistroExit(Sender: TObject);
    procedure edReciboKeyPress(Sender: TObject; var Key: Char);
    procedure edProtocoloDKeyPress(Sender: TObject; var Key: Char);
    procedure edRegistroKeyPress(Sender: TObject; var Key: Char);
    procedure edLivroRegistroKeyPress(Sender: TObject; var Key: Char);
    procedure edFolhaRegistroKeyPress(Sender: TObject; var Key: Char);
    procedure lkEspecieKeyPress(Sender: TObject; var Key: Char);
    procedure cbTipoAprChange(Sender: TObject);
    procedure edDocAprChange(Sender: TObject);
    procedure edDocDevChange(Sender: TObject);
    procedure sbDevedorClick(Sender: TObject);
    procedure cbTipoDevChange(Sender: TObject);
    procedure edCedenteExit(Sender: TObject);
    procedure lkEspecieClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sbLocalizarClick(Sender: TObject);
    procedure edDocAprExit(Sender: TObject);
    procedure edDocAprClick(Sender: TObject);
    procedure edDocDevClick(Sender: TObject);
    procedure edDocDevExit(Sender: TObject);
    procedure edSacadorExit(Sender: TObject);
    procedure edCodigoExit(Sender: TObject);
    procedure edLivroProtocoloKeyPress(Sender: TObject; var Key: Char);
    procedure btObsClick(Sender: TObject);
    procedure btProximoClick(Sender: TObject);
    procedure btAnteriorClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btLimparClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure CarregarTitulo(ID_ATO: Integer);
    procedure Inclusao;
    procedure Alteracao;
    procedure Fechado;
    procedure Visualizacao;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure ImTabelaClick(Sender: TObject);
    procedure sbPortadorClick(Sender: TObject);
    procedure cbTipoIntimacaoClick(Sender: TObject);
    procedure cbTipoIntimacaoChange(Sender: TObject);
    procedure sbSacadorClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure lbStatusClick(Sender: TObject);
    procedure ImGerarClick(Sender: TObject);
    procedure edLAEKeyPress(Sender: TObject; var Key: Char);
    procedure Image2Click(Sender: TObject);
    procedure ImLimparCCTClick(Sender: TObject);
    procedure ImNovoCCTClick(Sender: TObject);
    procedure ImLimparSelo1Click(Sender: TObject);
    procedure ImNovoSelo1Click(Sender: TObject);
    procedure ImLimparSelo2Click(Sender: TObject);
    procedure ImNovoSelo2Click(Sender: TObject);
    procedure ImAle2Click(Sender: TObject);
    procedure ImAle1Click(Sender: TObject);
    procedure sbDevedoresClick(Sender: TObject);

    function  SalvarTipoEndosso: String;
    function  RetornarTipoEndosso: Integer;
    function  RetornarTipoCobranca: Integer;
    function  RetornarTipoApresentacao: Integer;
    function  RetornarConvenio: Integer;
    function  RetornarTipoIntimacao: Integer;
    procedure edProtocoloSAKeyPress(Sender: TObject; var Key: Char);
    procedure edApresentanteExit(Sender: TObject);
    procedure cbConvExit(Sender: TObject);
    procedure btnDesfazerExportacaoSAClick(Sender: TObject);
    procedure dsRXDevedorDataChange(Sender: TObject; Field: TField);
    procedure btnIncluirDevClick(Sender: TObject);
    procedure btnEditarDevClick(Sender: TObject);
    procedure btnExcluirDevClick(Sender: TObject);
    procedure btnConfirmarDevClick(Sender: TObject);
    procedure btnCancelarDevClick(Sender: TObject);
  private
    { Private declarations }

    procedure HasDesabBotoesDevedor(Hab: Boolean);
  public
    { Public declarations }
  end;

var
  FTitulo: TFTitulo;

implementation

uses UDM, UPF, UCadastro, UMotivo, UCustas, UPrincipal, UConsultaPortador,
     UObservacao, UQuickIntimacao1, UDadosPortador, USacador, UStatus, UGeral,
     UTabela, UGDM, Math, UDevedores;

{$R *.dfm}

procedure TFTitulo.CarregarTitulo(ID_ATO: Integer);
var
  DataRetiradaDevolvido: TDate;
begin
  btLimparClick(Self);

  dm.Titulos.Close;
  dm.Titulos.Params[0].AsInteger:=ID_ATO;
  dm.Titulos.Open;

  if dm.TitulosSTATUS.AsString='DEVOLVIDO' then
    DataRetiradaDevolvido:=dm.TitulosDT_DEVOLVIDO.AsDateTime
      else DataRetiradaDevolvido:=dm.TitulosDT_RETIRADO.AsDateTime;

  dm.vMotivo        :=dm.TitulosMOTIVO_INTIMACAO.AsString;
  dm.vObservacao    :=dm.TitulosOBSERVACAO.AsString;
  dm.vEmolumentos   :=dm.TitulosEMOLUMENTOS.AsFloat;
  dm.vFetj          :=dm.TitulosFETJ.AsFloat;
  dm.vFundperj      :=dm.TitulosFUNDPERJ.AsFloat;
  dm.vFunperj       :=dm.TitulosFUNPERJ.AsFloat;
  dm.vFunarpen      :=dm.TitulosFUNARPEN.AsFloat;
  dm.vPmcmv         :=dm.TitulosPMCMV.AsFloat;
  dm.vIss           :=dm.TitulosISS.AsFloat;
  dm.vMutua         :=dm.TitulosMUTUA.AsFloat;
  dm.vAcoterj       :=dm.TitulosACOTERJ.AsFloat;
  dm.vDistribuicao  :=dm.TitulosDISTRIBUICAO.AsFloat;
  dm.vAAR           :=dm.TitulosVALOR_AR.AsFloat;
  dm.vTarifa        :=dm.TitulosTARIFA_BANCARIA.AsFloat;
  dm.vTotal         :=dm.TitulosTOTAL.AsFloat;
  //dm.vCodigo        :=dm.TitulosCODIGO.AsInteger;

  if dm.TitulosDT_PROTOCOLO.AsDateTime <> 0 then
    edDataProtocoloD.Date  := dm.TitulosDT_PROTOCOLO.AsDateTime;

  if dm.TitulosDT_PROTOCOLO_CARTORIO.AsDateTime <> 0 then
    edDataProtocoloSA.Date := dm.TitulosDT_PROTOCOLO_CARTORIO.AsDateTime;

  if dm.TitulosDT_REGISTRO.AsDateTime <> 0 then
    edDataRegistro.Date    := dm.TitulosDT_REGISTRO.AsDateTime;

  if dm.TitulosDT_TITULO.AsDateTime <> 0 then
    edDataTitulo.Date      := dm.TitulosDT_TITULO.AsDateTime;

  if not dm.TitulosDIAS_AVISTA.IsNull then
  begin
    edDataVencimento.Visible    := False;
    lblVencimentoAVista.Visible := True;

    lblVencimentoAVista.Left := (edDataTitulo.Left + edDataTitulo.Width + 16);
    lblVencimentoAVista.Top  := (edDataTitulo.Top + 3);
  end
  else
  begin
    edDataVencimento.Visible    := True;
    lblVencimentoAVista.Visible := False;

    edDataVencimento.Left := (edDataTitulo.Left + edDataTitulo.Width + 6);
    edDataVencimento.Top  := edDataTitulo.Top;

    if dm.TitulosDT_VENCIMENTO.AsDateTime <> 0 then
      edDataVencimento.Date := dm.TitulosDT_VENCIMENTO.AsDateTime;
  end;

  if dm.TitulosDT_INTIMACAO.AsDateTime <> 0 then
    edDataIntimacao.Date   := dm.TitulosDT_INTIMACAO.AsDateTime;

  if dm.TitulosDT_PUBLICACAO.AsDateTime <> 0 then
    edDataPublicacao.Date  := dm.TitulosDT_PUBLICACAO.AsDateTime;

  if dm.TitulosDT_ENVIO.AsDateTime <> 0 then
    edDataEnvio.Date       := dm.TitulosDT_ENVIO.AsDateTime;

  if dm.TitulosDT_PAGAMENTO.AsDateTime  <> 0 then
    edDataPagamento.Date   := dm.TitulosDT_PAGAMENTO.AsDateTime;

  if dm.TitulosDT_SUSTADO.AsDateTime  <> 0 then
    edDataSustacao.Date    := dm.TitulosDT_SUSTADO.AsDateTime;

  if dm.TitulosDT_PRAZO.AsDateTime <> 0 then
    edDataPrazo.Date       := dm.TitulosDT_PRAZO.AsDateTime;

  if DataRetiradaDevolvido <> 0 then
    edDataRetirada.Date    := DataRetiradaDevolvido;

  edProtocoloD.Text                 := dm.TitulosPROTOCOLO.AsString;
  edProtocoloSA.Text                := dm.TitulosPROTOCOLO_CARTORIO.AsString;
  edRecibo.Text                     := dm.TitulosRECIBO.AsString;
  edCCT.Text                        := dm.TitulosCCT.AsString;
  cbEndosso.ItemIndex               := RetornarTipoEndosso;
  cbTipoApresentacao.ItemIndex      := RetornarTipoApresentacao;
  lkEspecie.KeyValue                := dm.TitulosTIPO_TITULO.AsInteger;
  edLAE.Text                        := dm.TitulosCODIGO.AsString;
  cbTipoProtesto.ItemIndex          := dm.TitulosTIPO_PROTESTO.AsInteger-1;
  edNumero.Text                     := dm.TitulosNUMERO_TITULO.AsString;
  edtNossoNumero.Text               := dm.TitulosNOSSO_NUMERO.AsString;
  cbTipoIntimacao.ItemIndex         := RetornarTipoIntimacao;
  edValor.Value                     := dm.TitulosVALOR_TITULO.AsFloat;
  edSaldo.Value                     := dm.TitulosSALDO_TITULO.AsFloat;
  edCustas.Value                    := dm.TitulosTOTAL.AsFloat;
  edTotal.Value                     := dm.TitulosSALDO_PROTESTO.AsFloat;
  edtArquivoD.Text                  := dm.TitulosARQUIVO.AsString;
  edtArquivoR.Text                  := dm.TitulosARQUIVO_CARTORIO.AsString;
  edtServentiaAgregada.Text         := dm.TitulosSERVENTIA_AGREGADA.AsString;
  lblServentiaAgregada.Caption      := dm.TitulosCARTORIO.AsString;
  edBanco.Text                      := dm.TitulosBANCO.AsString;
  edAgencia.Text                    := dm.TitulosAGENCIA.AsString;
  edConta.Text                      := dm.TitulosCONTA.AsString;
  edPagamento.Value                 := dm.TitulosVALOR_PAGAMENTO.AsFloat;
  //cbConvenio.ItemIndex              :=RetornarConvenio;
  edRegistro.Text                   := dm.TitulosREGISTRO.AsString;
  edLivroRegistro.Text              := dm.TitulosLIVRO_REGISTRO.AsString;
  edLivroProtocolo.Text             := dm.TitulosLIVRO_PROTOCOLO.AsString;
  edFolhaRegistro.Text              := dm.TitulosFOLHA_REGISTRO.AsString;
  edFolhaProtocolo.Text             := dm.TitulosFOLHA_PROTOCOLO.AsString;
  edSeloRegistro.Text               := dm.TitulosSELO_REGISTRO.AsString;
  edAleatorioProtesto.Text          := dm.TitulosALEATORIO_PROTESTO.AsString;
  edAleatorioSolucao.Text           := dm.TitulosALEATORIO_SOLUCAO.AsString;
  edSeloSolucao.Text                := dm.TitulosSELO_PAGAMENTO.AsString;
  edApresentante.Text               := Trim(dm.TitulosAPRESENTANTE.AsString);

  if dm.TitulosCODIGO.AsInteger <> 0 then
    edCodigo.Text                   := dm.TitulosCODIGO_APRESENTANTE.AsString;

  cbTipoApr.ItemIndex               := cbTipoApr.Items.IndexOf(dm.TitulosTIPO_APRESENTANTE.AsString);
  edDocApr.Text                     := Trim(dm.TitulosCPF_CNPJ_APRESENTANTE.AsString);
  edCedente.Text                    := dm.TitulosCEDENTE.AsString;
  edSacador.Text                    := dm.TitulosSACADOR.AsString;
  lkEscrevente.KeyValue             := dm.TitulosCPF_ESCREVENTE.AsString;
  lkEscreventeP.KeyValue            := dm.TitulosCPF_ESCREVENTE_PG.AsString;

  if Trim(dm.TitulosSTATUS.AsString) = 'Aceito' then
    lbStatus.Caption                := '(D) ' + dm.TitulosPROTOCOLO.AsString + '  |  ' +
                                       '(SA) ' + IfThen((Trim(dm.TitulosPROTOCOLO_CARTORIO.AsString) = ''),
                                                        '0',
                                                        dm.TitulosPROTOCOLO_CARTORIO.AsString)
  else
    lbStatus.Caption                := '(D) ' + dm.TitulosPROTOCOLO.AsString + '  |  ' +
                                       '(SA) ' + IfThen((Trim(dm.TitulosPROTOCOLO_CARTORIO.AsString) = ''),
                                                        '0',
                                                        dm.TitulosPROTOCOLO_CARTORIO.AsString) +
                                       ' - ' +
                                       dm.TitulosSTATUS.AsString;

  edPagamento.Value                 := dm.TitulosVALOR_PAGAMENTO.AsFloat;
  ckAntigo.Checked                  := GR.iif(dm.TitulosANTIGO.AsString='S',True,False);
  ckFins.Checked                    := GR.iif(dm.TitulosFINS_FALIMENTARES.AsString='S',True,False);
  ckPostecipado.Checked             := GR.iif(dm.TitulosPOSTECIPADO.AsString='P',True,False);
  cbAceite.ItemIndex                := GR.iif(Trim(dm.TitulosACEITE.AsString)='',0,GR.iif(dm.TitulosACEITE.AsString='A',1,2));

  PF.CarregarDevedor(dm.TitulosID_ATO.AsInteger);

  cbTipoDev.ItemIndex    := IfThen(dm.RxDevedorTIPO.AsString<>'',cbTipoDev.IndexOf(dm.RxDevedorTIPO.AsString),0);
  edDocDev.Text          := dm.RxDevedorDOCUMENTO.AsString;
  edDevedor.Text         := dm.RxDevedorNOME.AsString;
  edtEndDevedor.Text     := dm.RxDevedorENDERECO.AsString;
  edtBairroDevedor.Text  := dm.RxDevedorBAIRRO.AsString;
  lcbMunDevedor.KeyValue := dm.RxDevedorMUNICIPIO.AsString;
  cbUFDevedor.ItemIndex  := cbUFDevedor.IndexOf(dm.RxDevedorUF.AsString);
  medCEPDevedor.Text     := dm.RxDevedorCEP.AsString;

  PF.CarregarSacador(dm.TitulosID_ATO.AsInteger);

  if dm.TitulosCODIGO_APRESENTANTE.AsString<>'' then
    PF.CarregarPortador('C',dm.TitulosCODIGO_APRESENTANTE.AsString,dm.TitulosAPRESENTANTE.AsString,
                            dm.TitulosTIPO_APRESENTANTE.AsString,dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,
                            dm.TitulosCODIGO_APRESENTANTE.AsString)
      else
        if dm.TitulosCPF_CNPJ_APRESENTANTE.AsString<>'' then
          PF.CarregarPortador('D',dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosAPRESENTANTE.AsString,
                                  dm.TitulosTIPO_APRESENTANTE.AsString,dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosCODIGO_APRESENTANTE.AsString)
            else PF.CarregarPortador('N',dm.TitulosAPRESENTANTE.AsString,dm.TitulosAPRESENTANTE.AsString,
                                         dm.TitulosTIPO_APRESENTANTE.AsString,dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosCODIGO_APRESENTANTE.AsString);

  if dm.RxPortadorCONVENIO.AsString='S' then
  begin
    cbConv.ItemIndex := 0;
    edRecibo.ReadOnly := True;
  end
  else
  begin
    cbConv.ItemIndex := 1;
    edRecibo.ReadOnly := False;
  end;

  PF.CarregarCustas(dm.TitulosID_ATO.AsInteger);

  cbTipoAprChange(Self);
  cbTipoDevChange(Self);

  if dm.TitulosSTATUS.AsString='CANCELADO' then
    edDataPagamento.BoundLabel.Caption:='Data Cancelamento'
      else edDataPagamento.BoundLabel.Caption:='Data Pagamento';

  if dm.TitulosSTATUS.AsString='DEVOLVIDO' then
    edDataRetirada.BoundLabel.Caption:='Data da Devolu��o'
      else edDataRetirada.BoundLabel.Caption:='Data da Retirada';

  edtServentiaAgregada.Visible := dm.TitulosCARTORIO.AsInteger > 0;

  if dm.Titulos.State = dsBrowse then
  begin
    btnDesfazerExportacaoSA.Visible := (dm.Titulos.FieldByName('FLG_SALDO').AsString = 'S') and
                                       (dm.Titulos.FieldByName('TIPO_APRESENTACAO').AsString = 'B') and
                                       dm.Titulos.FieldByName('RETORNO_CARTORIO').IsNull;
  end
  else
    btnDesfazerExportacaoSA.Visible := False;
end;

procedure TFTitulo.btCancelarClick(Sender: TObject);
begin
  if dm.vTipo='I' then
  begin
      Fechado;
      btLimparClick(Sender);
  end
  else
    Visualizacao;
  Close;
end;

procedure TFTitulo.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
  if key=VK_ESCAPE then Close;
end;

procedure TFTitulo.btSalvarClick(Sender: TObject);
var
  TipoTitulo: Integer;
  sDias: String;
begin
  sDias := '';

  TipoTitulo:=dm.TiposCODIGO.AsInteger;

  if edLAE.Text='' then
  begin
      GR.Aviso('INFORME O C�DIGO DO LIVRO ADICIONAL!');
      edCodigo.SetFocus;
      Exit;
  end;

  if edApresentante.Text='' then
  begin
      GR.Aviso('INFORME O NOME DO APRESENTANTE!');
      edApresentante.SetFocus;
      Exit;
  end;

  if GR.PegarNumeroTexto(edDocDev.Text)='' then
  begin
      GR.Aviso('INFORME O DOCUMENTO DO DEVEDOR!');
      edDocDev.SetFocus;
      Exit;
  end;

  if edDevedor.Text='' then
  begin
      GR.Aviso('INFORME O NOME DO DEVEDOR!');
      edDevedor.SetFocus;
      Exit;
  end;

  if dm.conSISTEMA.Connected then
    dm.conSISTEMA.StartTransaction;

  if dm.conCENTRAL.Connected then
    dm.conCENTRAL.StartTransaction;

  try
      if dm.vTipo='I' then
      begin
          dm.Titulos.Close;
          dm.Titulos.Params[0].AsInteger:=-1;
          dm.Titulos.Open;
          dm.Titulos.Append;
          dm.TitulosID_ATO.AsInteger:=dm.IdAtual('ID_ATO','S');
          dm.TitulosSTATUS.AsString :='APONTADO';
          dm.GerarValor('NPR');
          dm.GerarValor('FPR');
      end
      else
        dm.Titulos.Edit;

      {CAMPOS INTEIROS QUE PRECISAM DE TRATAMENTO ANTES DE SALVAR}
      if edRecibo.Text            <>''   then dm.TitulosRECIBO.AsInteger              :=StrToInt(edRecibo.Text)           else dm.TitulosRECIBO.Clear;
      if edProtocoloD.Text        <>''   then dm.TitulosPROTOCOLO.AsInteger           :=StrToInt(edProtocoloD.Text)       else dm.TitulosPROTOCOLO.Clear;
      if edProtocoloSa.Text       <>''   then dm.TitulosPROTOCOLO_CARTORIO.AsInteger  :=StrToInt(edProtocoloSA.Text)      else dm.TitulosPROTOCOLO_CARTORIO.Clear;
      if edRegistro.Text          <>''   then dm.TitulosREGISTRO.AsInteger            :=StrToInt(edRegistro.Text)         else dm.TitulosREGISTRO.Clear;
      if edLivroRegistro.Text     <>''   then dm.TitulosLIVRO_REGISTRO.AsInteger      :=StrToInt(edLivroRegistro.Text)    else dm.TitulosLIVRO_REGISTRO.Clear;
      if edLivroProtocolo.Text    <>''   then dm.TitulosLIVRO_PROTOCOLO.AsInteger     :=StrToInt(edLivroProtocolo.Text)   else dm.TitulosLIVRO_PROTOCOLO.Clear;
      if cbTipoProtesto.ItemIndex <>-1   then dm.TitulosTIPO_PROTESTO.AsInteger       :=cbTipoProtesto.ItemIndex+1        else dm.TitulosTIPO_PROTESTO.Clear;
      if lkEspecie.Text           <>''   then dm.TitulosTIPO_TITULO.AsInteger         :=TipoTitulo                        else dm.TitulosTIPO_TITULO.Clear;
      if lkEscrevente.KeyValue    <>Null then
      begin
          if dm.Escreventes.Locate('NOME',lkEscrevente.Text,[loPartialKey,loCaseInsensitive]) then
          dm.TitulosCPF_ESCREVENTE.AsString:=dm.EscreventesCPF.AsString;
      end
      else
        dm.TitulosCPF_ESCREVENTE.Clear;

      if lkEscreventeP.KeyValue<>Null then
      begin
          if dm.Escreventes.Locate('NOME',lkEscreventeP.Text,[loPartialKey,loCaseInsensitive]) then
          dm.TitulosCPF_ESCREVENTE_PG.AsString:=dm.EscreventesCPF.AsString;
      end
      else
        dm.TitulosCPF_ESCREVENTE_PG.Clear;

      {CAMPOS DATA QUE PRECISAM DE TRATAMENTO ANTES DE SALVAR}
      if edDataProtocoloD.Date  <>0 then dm.TitulosDT_PROTOCOLO.AsDateTime  :=edDataProtocoloD.Date   else dm.TitulosDT_PROTOCOLO.Clear;
      if edDataProtocoloSA.Date <>0 then dm.TitulosDT_PROTOCOLO_CARTORIO.AsDateTime  :=edDataProtocoloSA.Date  else dm.TitulosDT_PROTOCOLO_CARTORIO.Clear;
      if edDataTitulo.Date      <>0 then dm.TitulosDT_TITULO.AsDateTime     :=edDataTitulo.Date       else dm.TitulosDT_TITULO.Clear;
      if edDataIntimacao.Date   <>0 then dm.TitulosDT_INTIMACAO.AsDateTime  :=edDataIntimacao.Date    else dm.TitulosDT_INTIMACAO.Clear;
      if edDataPublicacao.Date  <>0 then dm.TitulosDT_PUBLICACAO.AsDateTime :=edDataPublicacao.Date   else dm.TitulosDT_PUBLICACAO.Clear;
      if edDataRegistro.Date    <>0 then dm.TitulosDT_REGISTRO.AsDateTime   :=edDataRegistro.Date     else dm.TitulosDT_REGISTRO.Clear;
      if edDataPagamento.Date   <>0 then dm.TitulosDT_PAGAMENTO.AsDateTime  :=edDataPagamento.Date    else dm.TitulosDT_PAGAMENTO.Clear;
      if edDataPrazo.Date       <>0 then dm.TitulosDT_PRAZO.AsDateTime      :=edDataPrazo.Date        else dm.TitulosDT_PRAZO.Clear;
      if edDataSustacao.Date    <>0 then dm.TitulosDT_SUSTADO.AsDateTime    :=edDataSustacao.Date     else dm.TitulosDT_SUSTADO.Clear;

      if edDataVencimento.Date <> 0 then
      begin
        dm.TitulosDT_VENCIMENTO.AsDateTime := edDataVencimento.Date;
        dm.TitulosAVISTA.AsString          := 'N';
      end
      else
      begin
        dm.TitulosDT_VENCIMENTO.Clear;

        if TipoTitulo in [19, 20, 21, 22, 23, 24, 27] then
        begin
          sDias := UpperCase(InputBox('� VISTA', PChar('1 = A um dia da vista' +
                                                       '30 = A 30 dias da vista' +
                                                       'O = Outro'), '1'));

          if (PF.PegarNumero(sDias) = 1) or
            (PF.PegarNumero(sDias) = 30) then
          begin
            dm.TitulosDIAS_AVISTA.AsInteger := StrToInt(sDias);
            dm.TitulosAVISTA.AsString       := 'S';
          end
          else
          begin
            dm.TitulosDIAS_AVISTA.AsInteger := 0;
            dm.TitulosAVISTA.AsString       := 'S';
          end;
        end
        else
          dm.TitulosDIAS_AVISTA.AsInteger := 0;

        dm.TitulosAVISTA.AsString := 'S';
      end;

      if dm.TitulosSTATUS.AsString='RETIRADO' then
      begin
          if edDataRetirada.Date<>0 then
            dm.TitulosDT_RETIRADO.AsDateTime:=edDataRetirada.Date
              else dm.TitulosDT_RETIRADO.Clear;
      end
      else
        if dm.TitulosSTATUS.AsString='DEVOLVIDO' then
        begin
            if edDataRetirada.Date<>0 then
              dm.TitulosDT_DEVOLVIDO.AsDateTime:=edDataRetirada.Date
                else dm.TitulosDT_DEVOLVIDO.Clear;
        end;

      dm.TitulosCODIGO.AsInteger                :=StrToInt(edLAE.Text);
      dm.TitulosSELO_REGISTRO.AsString          :=edSeloRegistro.Text;
      dm.TitulosSELO_PAGAMENTO.AsString         :=edSeloSolucao.Text;
      dm.TitulosALEATORIO_PROTESTO.AsString     :=edAleatorioProtesto.Text;
      dm.TitulosALEATORIO_SOLUCAO.AsString      :=edAleatorioSolucao.Text;
      dm.TitulosFOLHA_PROTOCOLO.AsString        :=edFolhaProtocolo.Text;
      dm.TitulosFOLHA_REGISTRO.AsString         :=edFolhaRegistro.Text;
      dm.TitulosTIPO_ENDOSSO.AsString           :=SalvarTipoEndosso;
      dm.TitulosFINS_FALIMENTARES.AsString      :=GR.iif(ckFins.Checked,'S','N');
      dm.TitulosPOSTECIPADO.AsString            :=GR.iif(ckPostecipado.Checked,'P','');

      {CUSTAS RECEBEM AS VARI�VEIS}
      dm.TitulosEMOLUMENTOS.AsFloat             :=dm.vEmolumentos;
      dm.TitulosFETJ.AsFloat                    :=dm.vFetj;
      dm.TitulosFUNDPERJ.AsFloat                :=dm.vFundperj;
      dm.TitulosFUNPERJ.AsFloat                 :=dm.vFunperj;
      dm.TitulosFUNARPEN.AsFloat                :=dm.vFunarpen;
      dm.TitulosPMCMV.AsFloat                   :=dm.vPmcmv;
      dm.TitulosISS.AsFloat                     :=dm.vIss;
      dm.TitulosMUTUA.AsFloat                   :=dm.vMutua;
      dm.TitulosACOTERJ.AsFloat                 :=dm.vAcoterj;
      dm.TitulosDISTRIBUICAO.AsFloat            :=dm.vDistribuicao;
      dm.TitulosVALOR_AR.AsFloat                :=dm.vAAR;
      dm.TitulosTARIFA_BANCARIA.AsFloat         :=dm.vTarifa;
      dm.TitulosTOTAL.AsFloat                   :=dm.vTotal;
      dm.TitulosNUMERO_TITULO.AsString          :=edNumero.Text;
      dm.TitulosNOSSO_NUMERO.AsString           := edtNossoNumero.Text;
      dm.TitulosBANCO.AsString                  :=edBanco.Text;
      dm.TitulosAGENCIA.AsString                :=edAgencia.Text;
      dm.TitulosCONTA.AsString                  :=edConta.Text;
      dm.TitulosVALOR_TITULO.AsFloat            :=edValor.Value;
      dm.TitulosSALDO_PROTESTO.AsFloat          :=edTotal.Value;
      dm.TitulosARQUIVO.AsString                :=edtArquivoD.Text;
      dm.TitulosARQUIVO_CARTORIO.AsString       :=edtArquivoR.Text;
      dm.TitulosSERVENTIA_AGREGADA.AsString     :=edtServentiaAgregada.Text;
      dm.TitulosSALDO_TITULO.AsFloat            :=edSaldo.Value;
      dm.TitulosVALOR_PAGAMENTO.AsFloat         :=edPagamento.Value;
      dm.TitulosCONVENIO.AsString               :=dm.RxPortadorCONVENIO.AsString; //Copy(cbConvenio.Text,1,1);
      dm.TitulosTIPO_APRESENTACAO.AsString      :=Copy(cbTipoApresentacao.Text,1,1);
      dm.TitulosTIPO_INTIMACAO.AsString         :=Copy(cbTipoIntimacao.Text,1,1);
      dm.TitulosMOTIVO_INTIMACAO.AsString       :=dm.vMotivo;
      dm.TitulosOBSERVACAO.AsString             :=dm.vObservacao;
      dm.TitulosAPRESENTANTE.AsString           :=edApresentante.Text;
      dm.TitulosTIPO_APRESENTANTE.AsString      :=cbTipoApr.Text;
      dm.TitulosCPF_CNPJ_APRESENTANTE.AsString  :=edDocApr.Text;
      dm.TitulosCODIGO_APRESENTANTE.AsString    :=edCodigo.Text;
      dm.TitulosSACADOR.AsString                :=edSacador.Text;
      if edCedente.Text='' then edCedente.Text  :=edSacador.Text;
      dm.TitulosCEDENTE.AsString                :=edCedente.Text;
      dm.TitulosDEVEDOR.AsString                :=edDevedor.Text;
      dm.TitulosTIPO_DEVEDOR.AsString           :=cbTipoDev.Text;
      dm.TitulosCPF_CNPJ_DEVEDOR.AsString       :=edDocDev.Text;
      dm.TitulosACEITE.AsString                 :=Trim(GR.RemoverAcento(Copy(cbAceite.Text,1,1)));

      if dm.TitulosSTATUS.AsString='PROTESTADO' then
      dm.TitulosPROTESTADO.AsString:='S';

      dm.Titulos.Post;
      if dm.Titulos.ApplyUpdates(0)>0 then
      Raise Exception.Create('ApplyUpdates dm.Titulos');

      {GRAVANDO O DEVEDOR}
      dm.Devedores.Close;
      dm.Devedores.Params[0].AsInteger:=dm.TitulosID_ATO.AsInteger;
      dm.Devedores.Open;

      dm.RxDevedor.DisableControls;
      dm.RxDevedor.First;

      while not dm.RxDevedor.Eof do
      begin
          if not dm.Devedores.Locate('ID_DEVEDOR',dm.RxDevedorID_DEVEDOR.AsInteger,[]) then
          begin
              dm.Devedores.Append;
              dm.DevedoresID_DEVEDOR.AsInteger:=dm.IdAtual('ID_DEVEDOR','S');
          end
          else
            dm.Devedores.Edit;

          dm.DevedoresID_ATO.AsInteger          :=dm.TitulosID_ATO.AsInteger;
          dm.DevedoresORDEM.AsInteger           :=dm.RxDevedorORDEM.AsInteger;
          dm.DevedoresNOME.AsString             :=dm.RxDevedorNOME.AsString;
          dm.DevedoresTIPO.AsString             :=dm.RxDevedorTIPO.AsString;
          dm.DevedoresDOCUMENTO.AsString        :=dm.RxDevedorDOCUMENTO.AsString;
          dm.DevedoresIFP_DETRAN.AsString       :=dm.RxDevedorIFP_DETRAN.AsString;
          dm.DevedoresIDENTIDADE.AsString       :=dm.RxDevedorIDENTIDADE.AsString;
          dm.DevedoresORGAO.AsString            :=dm.RxDevedorORGAO.AsString;
          dm.DevedoresENDERECO.AsString         :=dm.RxDevedorENDERECO.AsString;
          dm.DevedoresBAIRRO.AsString           :=dm.RxDevedorBAIRRO.AsString;
          dm.DevedoresMUNICIPIO.AsString        :=dm.RxDevedorMUNICIPIO.AsString;
          dm.DevedoresUF.AsString               :=dm.RxDevedorUF.AsString;
          dm.DevedoresJUSTIFICATIVA.AsString    :=dm.RxDevedorJUSTIFICATIVA.AsString;
          dm.DevedoresIGNORADO.AsString         :=dm.RxDevedorIGNORADO.AsString;
          dm.DevedoresCEP.AsString              :=dm.RxDevedorCEP.AsString;
          dm.DevedoresDT_EMISSAO.AsDateTime     :=dm.RxDevedorDT_EMISSAO.AsDateTime;
          dm.Devedores.Post;

          if dm.Devedores.ApplyUpdates(0)>0 then
          Raise Exception.Create('ApplyUpdates dm.Devedores');

          dm.RxDevedor.Next;
      end;
      dm.RxDevedor.EnableControls;
      
      {GRAVANDO O PORTADOR}
      dm.Portadores.Close;

      if dm.RxPortadorDOCUMENTO.AsString<>'' then
      begin
          dm.Portadores.CommandText         :='SELECT * FROM PORTADORES WHERE DOCUMENTO=:P';
          dm.Portadores.Params[0].DataType  :=ftString;
          dm.Portadores.Params[0].ParamType :=ptInput;
          dm.Portadores.Params[0].AsString  :=dm.RxPortadorDOCUMENTO.AsString;
      end
      else
        if dm.RxPortadorCODIGO.AsString<>'' then
        begin
            dm.Portadores.CommandText         :='SELECT * FROM PORTADORES WHERE CODIGO=:P';
            dm.Portadores.Params[0].DataType  :=ftString;
            dm.Portadores.Params[0].ParamType :=ptInput;
            dm.Portadores.Params[0].AsString  :=dm.RxPortadorCODIGO.AsString;
        end
        else
        begin
            dm.Portadores.CommandText         :='SELECT * FROM PORTADORES WHERE NOME=:P';
            dm.Portadores.Params[0].DataType  :=ftString;
            dm.Portadores.Params[0].ParamType :=ptInput;
            dm.Portadores.Params[0].AsString  :=dm.RxPortadorNOME.AsString;
        end;
      dm.Portadores.Open;
      if dm.Portadores.IsEmpty then
      begin
          dm.Portadores.Append;
          dm.PortadoresID_PORTADOR.AsInteger:=dm.IdAtual('ID_PORTADOR','S');
      end
      else
        dm.Portadores.Edit;

      dm.PortadoresCODIGO.AsString             := dm.RxPortadorCODIGO.AsString;
      dm.PortadoresDOCUMENTO.AsString          := dm.RxPortadorDOCUMENTO.AsString;
      dm.PortadoresNOME.AsString               := dm.RxPortadorNOME.AsString;
      dm.PortadoresTIPO.AsString               := dm.RxPortadorTIPO.AsString;
      dm.PortadoresDOCUMENTO.AsString          := dm.RxPortadorDOCUMENTO.AsString;
      dm.PortadoresENDERECO.AsString           := dm.RxPortadorENDERECO.AsString;
      dm.PortadoresBANCO.AsString              := dm.RxPortadorBANCO.AsString;
      dm.PortadoresCONVENIO.AsString           := dm.RxPortadorCONVENIO.AsString;
      dm.PortadoresCONTA.AsString              := dm.RxPortadorCONTA.AsString;
      dm.PortadoresOBSERVACAO.AsString         := dm.RxPortadorOBSERVACAO.AsString;
      dm.PortadoresFLG_PAGAANTECIPADO.AsString := dm.RxPortadorFLG_PAGAANTECIPADO.AsString;

      dm.Portadores.Post;
      if dm.Portadores.ApplyUpdates(0)>0 then
      Raise Exception.Create('ApplyUpdates dm.Portadores');

      {GRAVANDO O SACADOR}
      if dm.TitulosSACADOR.AsString<>'' then
      begin
          dm.Sacadores.Close;
          dm.Sacadores.CommandText:='SELECT * FROM SACADORES WHERE ID_ATO=:ID_ATO';
          dm.Sacadores.Params[0].AsInteger:=dm.TitulosID_ATO.AsInteger;
          dm.Sacadores.Open;
          if dm.Sacadores.IsEmpty then
          begin
              dm.Sacadores.Append;
              dm.SacadoresID_SACADOR.AsInteger:=dm.IdAtual('ID_SACADOR','S');
          end
          else
            dm.Sacadores.Edit;

          dm.SacadoresID_ATO.AsInteger          :=dm.TitulosID_ATO.AsInteger;
          dm.SacadoresNOME.AsString             :=dm.RxSacadorNOME.AsString;
          dm.SacadoresTIPO.AsString             :=dm.RxSacadorTIPO.AsString;
          dm.SacadoresDOCUMENTO.AsString        :=dm.RxSacadorDOCUMENTO.AsString;
          dm.SacadoresENDERECO.AsString         :=dm.RxSacadorENDERECO.AsString;
          dm.SacadoresBAIRRO.AsString           :=dm.RxSacadorBAIRRO.AsString;
          dm.SacadoresMUNICIPIO.AsString        :=dm.RxSacadorMUNICIPIO.AsString;
          dm.SacadoresUF.AsString               :=dm.RxSacadorUF.AsString;

          dm.Sacadores.Post;
          if dm.Sacadores.ApplyUpdates(0)>0 then
          Raise Exception.Create('ApplyUpdates dm.Sacadores');
      end;

      {GRAVANDO AS CUSTAS}
      if not dm.RxCustas.IsEmpty then
      begin
          dm.RxCustas.First;
          while not dm.RxCustas.Eof do
          begin
              dm.Custas.Close;
              dm.Custas.Params[0].AsInteger:=dm.RxCustasID_CUSTA.AsInteger;
              dm.Custas.Open;
              if dm.Custas.IsEmpty then
              begin
                  dm.Custas.Append;
                  dm.CustasID_CUSTA.AsInteger :=dm.IdAtual('ID_CUSTA','S');
              end
              else
                dm.Custas.Edit;

              dm.CustasID_ATO.AsInteger     :=dm.TitulosID_ATO.AsInteger;
              dm.CustasTABELA.AsString      :=dm.RxCustasTABELA.AsString;
              dm.CustasITEM.AsString        :=dm.RxCustasITEM.AsString;
              dm.CustasSUBITEM.AsString     :=dm.RxCustasSUBITEM.AsString;
              dm.CustasVALOR.AsFloat        :=dm.RxCustasVALOR.AsFloat;
              dm.CustasQTD.AsInteger        :=dm.RxCustasQTD.AsInteger;
              dm.CustasTOTAL.AsFloat        :=dm.RxCustasTOTAL.AsFloat;
              dm.Custas.Post;
              if dm.Custas.ApplyUpdates(0)>0 then
              Raise Exception.Create('ApplyUpdates dm.Custas');

              dm.RxCustas.Next;
          end;
      end;

      if dm.conSISTEMA.InTransaction then
        dm.conSISTEMA.Commit;

      if dm.conCENTRAL.InTransaction then
        dm.conCENTRAL.Commit;

      Visualizacao;

      if dm.vTipo='A' then GR.Aviso('N�O SE ESQUE�A DE ATUALIZAR AS ALTERA��ES NO GERENCIAL!');
  except
      on E:Exception do
      begin
          if dm.conSISTEMA.InTransaction then
            dm.conSISTEMA.Rollback;

          if dm.conCENTRAL.InTransaction then
            dm.conCENTRAL.Rollback;

          GR.Aviso('Erro: '+E.Message);
      end;
  end;

  if dm.Titulos.State = dsBrowse then
  begin
    btnDesfazerExportacaoSA.Visible := (dm.Titulos.FieldByName('FLG_SALDO').AsString = 'S') and
                                       (dm.Titulos.FieldByName('TIPO_APRESENTACAO').AsString = 'B') and
                                       dm.Titulos.FieldByName('RETORNO_CARTORIO').IsNull;
  end
  else
    btnDesfazerExportacaoSA.Visible := False;
end;

procedure TFTitulo.edValorExit(Sender: TObject);
begin
  if dm.RxCustas.IsEmpty then
  begin
      dm.qryFaixa.Close;
      dm.qryFaixa.ParamByName('C').AsString       :=dm.RxPortadorCONVENIO.AsString;
      dm.qryFaixa.ParamByName('OCULTO1').AsString :='N';
      dm.qryFaixa.ParamByName('OCULTO2').AsString :='N';
      dm.qryFaixa.ParamByName('VALOR1').AsFloat   :=edValor.Value;
      dm.qryFaixa.ParamByName('VALOR2').AsFloat   :=edValor.Value;
      dm.qryFaixa.Open;

      edCustas.Value    := dm.qryFaixaTOT.AsFloat;
      edTotal.Value     := edCustas.Value+edSaldo.Value;

      edLAE.Text        := dm.qryFaixaCOD.AsString;
      dm.vEmolumentos   := dm.qryFaixaEMOL.AsFloat;
      dm.vFetj          := dm.qryFaixaFETJ.AsFloat;
      dm.vFundperj      := dm.qryFaixaFUND.AsFloat;
      dm.vFunperj       := dm.qryFaixaFUNP.AsFloat;
      dm.vFunarpen      := dm.qryFaixaFUNA.AsFloat;
      dm.vPmcmv         := dm.qryFaixaPMCMV.AsFloat;
      dm.vIss           := dm.qryFaixaISS.AsFloat;
      dm.vMutua         := StrToFloat(dm.ValorParametro(26)); //dm.qryFaixaMUTUA.AsFloat;
      dm.vAcoterj       := StrToFloat(dm.ValorParametro(27)); //dm.qryFaixaACOTERJ.AsFloat;
      dm.vDistribuicao  := dm.qryFaixaDISTRIB.AsFloat;
      dm.vTotal         := dm.qryFaixaTOT.AsFloat;

      dm.Itens.Close;
      dm.Itens.Params[0].AsInteger:=dm.qryFaixaCOD.AsInteger;
      dm.Itens.Open;
      dm.Itens.First;

      dm.RxCustas.Close;
      dm.RxCustas.Open;

      while not dm.Itens.Eof do
      begin
          dm.RxCustas.Append;
          dm.RxCustasTABELA.AsString    :=dm.ItensTAB.AsString;
          dm.RxCustasITEM.AsString      :=dm.ItensITEM.AsString;
          dm.RxCustasSUBITEM.AsString   :=dm.ItensSUB.AsString;
          dm.RxCustasVALOR.AsFloat      :=dm.ItensVALOR.AsFloat;
          dm.RxCustasQTD.AsString       :=dm.ItensQTD.AsString;
          dm.RxCustasTOTAL.AsFloat      :=dm.ItensTOTAL.AsFloat;
          dm.RxCustasDESCRICAO.AsString :=dm.ItensDESCR.AsString;
          dm.RxCustas.Post;
          dm.Itens.Next;
      end;
  end;
end;

procedure TFTitulo.sSpeedButton1Click(Sender: TObject);
begin
  GR.CriarForm(TFMotivo,FMotivo);
end;

procedure TFTitulo.edValorClick(Sender: TObject);
begin
  edValor.SelectAll;
end;

procedure TFTitulo.edTotalClick(Sender: TObject);
begin
  edTotal.SelectAll;
end;

procedure TFTitulo.edCustasClick(Sender: TObject);
begin
  edCustas.SelectAll;
end;

procedure TFTitulo.edSeloRegistroExit(Sender: TObject);
begin
  edSeloRegistro.Text:=PF.FormatarSelo(edSeloRegistro.Text);
end;

function TFTitulo.SalvarTipoEndosso: String;
begin
  case cbEndosso.ItemIndex of
    -1: Result:='';
     0: Result:='M';
     1: Result:='T';
     2: Result:='S';
  end;
end;

procedure TFTitulo.edReciboKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFTitulo.edProtocoloDKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFTitulo.edRegistroKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFTitulo.edLivroRegistroKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFTitulo.edFolhaRegistroKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFTitulo.lkEspecieKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#8 then lkEspecie.KeyValue:=0;
end;

function TFTitulo.RetornarTipoCobranca: Integer;
begin
  Result:=-1;
  if dm.TitulosCOBRANCA.AsString='JG' then Result:=0;
  if dm.TitulosCOBRANCA.AsString='CC' then Result:=1;
  if dm.TitulosCOBRANCA.AsString='SC' then Result:=2;
  if dm.TitulosCOBRANCA.AsString='NH' then Result:=3;
end;

function TFTitulo.RetornarTipoApresentacao: Integer;
begin
  Result:=-1;
  if dm.TitulosTIPO_APRESENTACAO.AsString='B' then Result:=0;
  if dm.TitulosTIPO_APRESENTACAO.AsString='C' then Result:=1;
  if dm.TitulosTIPO_APRESENTACAO.AsString='I' then Result:=2;
end;

function TFTitulo.RetornarConvenio: Integer;
begin
  Result:=-1;
  if dm.TitulosCONVENIO.AsString='S' then Result:=0;
  if dm.TitulosCONVENIO.AsString='N' then Result:=1;
end;

procedure TFTitulo.cbConvExit(Sender: TObject);
begin
  if Trim(cbConv.Text) = 'S' then
    edRecibo.ReadOnly := True
  else
    edRecibo.ReadOnly := False;
end;

procedure TFTitulo.cbTipoAprChange(Sender: TObject);
begin
  if cbTipoApr.ItemIndex=0 then
  begin
      edDocApr.BoundLabel.Caption     :='CPF';
      dm.RxPortadorDOCUMENTO.EditMask :='999.999.999-99;0;_';
      dm.RxPortadorTIPO.AsString      :='F';
  end
  else
  begin
      edDocApr.BoundLabel.Caption     :='CNPJ';
      dm.RxPortadorDOCUMENTO.EditMask :='99.999.999/9999-99;0;_';
      dm.RxPortadorTIPO.AsString      :='J';
  end;
end;

procedure TFTitulo.edDocAprChange(Sender: TObject);
begin
  if GR.PegarNumeroTexto(edDocApr.Text)='' then
  begin
      ImAviso1.Visible:=True;
      ImOk1.Visible   :=False;
      Exit;
  end;

  if cbTipoApr.ItemIndex=0 then ImAviso1.Visible:=not PF.VerificarCPF(GR.PegarNumeroTexto(edDocApr.Text));
  if cbTipoApr.ItemIndex=1 then ImAviso1.Visible:=not PF.VerificarCNPJ(GR.PegarNumeroTexto(edDocApr.Text));

  ImOk1.Visible:=not ImAviso1.Visible;
end;

procedure TFTitulo.edDocDevChange(Sender: TObject);
begin
  if GR.PegarNumeroTexto(edDocDev.Text)='' then
  begin
      ImAviso2.Visible:=True;
      ImOk2.Visible   :=False;
      Exit;
  end;

  if cbTipoDev.ItemIndex=0 then ImAviso2.Visible:= not PF.VerificarCPF(GR.PegarNumeroTexto(edDocDev.Text));
  if cbTipoDev.ItemIndex=1 then ImAviso2.Visible:= not PF.VerificarCNPJ(GR.PegarNumeroTexto(edDocDev.Text));

  ImOk2.Visible:=not ImAviso2.Visible;
end;

procedure TFTitulo.sbDevedorClick(Sender: TObject);
begin
  if edDocDev.Text='' then
  begin
      GR.Aviso('INFORME O DOCUMENTO DO DEVEDOR!');
      edDocDev.SetFocus;
      Exit;
  end;

  if edDevedor.Text='' then
  begin
      GR.Aviso('INFORME O NOME DO DEVEDOR!');
      edDevedor.SetFocus;
      Exit;
  end;

  if dm.RxDevedor.IsEmpty then
  begin
    dm.RxDevedor.Append;
    dm.RxDevedorORDEM.AsInteger     :=1;
    dm.RxDevedorNOME.AsString       :=edDevedor.Text;
    dm.RxDevedorTIPO.AsString       :=cbTipoDev.Text;
    dm.RxDevedorDOCUMENTO.AsString  :=edDocDev.Text;
    dm.RxDevedorIGNORADO.AsString   :='N';
    dm.RxDevedorENDERECO.AsString   := edtEndDevedor.Text;
    dm.RxDevedorBAIRRO.AsString     := edtBairroDevedor.Text;
    dm.RxDevedorMUNICIPIO.AsString  := lcbMunDevedor.Text;
    dm.RxDevedorUF.AsString         := cbUFDevedor.Text;
    dm.RxDevedorCEP.AsString        := medCEPDevedor.Text;
    dm.RxDevedor.Post;
  end
  else
    dm.RxDevedor.Locate('ORDEM',1,[]);

  dm.RxDevedor.Edit;

  GR.CriarForm(TFCadastro,FCadastro);

  cbTipoDev.ItemIndex    := IfThen(dm.RxDevedorTIPO.AsString<>'',cbTipoDev.IndexOf(dm.RxDevedorTIPO.AsString),0);
  edDocDev.Text          := dm.RxDevedorDOCUMENTO.AsString;
  edDevedor.Text         := dm.RxDevedorNOME.AsString;
  edtEndDevedor.Text     := dm.RxDevedorENDERECO.AsString;
  edtBairroDevedor.Text  := dm.RxDevedorBAIRRO.AsString;
  lcbMunDevedor.KeyValue := dm.RxDevedorMUNICIPIO.AsString;
  cbUFDevedor.ItemIndex  := cbUFDevedor.IndexOf(dm.RxDevedorUF.AsString);
  medCEPDevedor.Text     := dm.RxDevedorCEP.AsString;
end;

procedure TFTitulo.cbTipoDevChange(Sender: TObject);
begin
  if cbTipoDev.ItemIndex=0 then
  begin
      edDocDev.BoundLabel.Caption :='CPF';
      edDocDev.EditMask           :='999.999.999-99;0;_';
  end
  else
  begin
      edDocDev.BoundLabel.Caption :='CNPJ';
      edDocDev.EditMask           :='99.999.999/9999-99;0;_';
  end;
end;

procedure TFTitulo.edApresentanteExit(Sender: TObject);
begin
  if edSacador.Text = '' then
    edSacador.Text := edApresentante.Text;

  if edCedente.Text = '' then
    edCedente.Text := edApresentante.Text;
end;

procedure TFTitulo.edCedenteExit(Sender: TObject);
begin
  if edCedente.Text='' then edCedente.Text:=edSacador.Text;
end;

function TFTitulo.RetornarTipoIntimacao: Integer;
begin
  Result:=-1;
  if dm.TitulosTIPO_INTIMACAO.AsString='P' then Result:=0;
  if dm.TitulosTIPO_INTIMACAO.AsString='E' then Result:=1;
  if dm.TitulosTIPO_INTIMACAO.AsString='C' then Result:=2;
end;

procedure TFTitulo.lkEspecieClick(Sender: TObject);
begin
  if dm.TiposCODIGO.AsInteger=10 then
    edBanco.Enabled:=True
      else
      begin
          edBanco.Clear;
          edBanco.Enabled:=False;
      end;
  edAgencia.Enabled:=edBanco.Enabled;
  edConta.Enabled  :=edBanco.Enabled; 
end;

procedure TFTitulo.FormCreate(Sender: TObject);
begin
  dm.Tipos.Close;
  dm.Tipos.Open;

  dm.Escreventes.Close;
  dm.Escreventes.Open;

  dm.Municipios.Close;
  dm.Municipios.Params[0].AsString := 'RJ';
  dm.Municipios.Open;
end;

procedure TFTitulo.sbLocalizarClick(Sender: TObject);
begin
  dm.Portadores.Close;
  GR.CriarForm(TFConsultaPortador,FConsultaPortador);

  if dm.vOkGeral then
  begin
      if dm.PortadoresCODIGO.AsString<>'' then
        PF.CarregarPortador('C',dm.PortadoresCODIGO.AsString,dm.PortadoresNOME.AsString,
                                dm.PortadoresTIPO.AsString,dm.PortadoresDOCUMENTO.AsString,dm.PortadoresCODIGO.AsString)
          else
            if dm.PortadoresDOCUMENTO.AsString<>'' then
              PF.CarregarPortador('D',dm.PortadoresDOCUMENTO.AsString,dm.PortadoresNOME.AsString,
                                      dm.PortadoresTIPO.AsString,dm.PortadoresDOCUMENTO.AsString,dm.PortadoresCODIGO.AsString)
              else
                PF.CarregarPortador('N',dm.PortadoresNOME.AsString,dm.PortadoresNOME.AsString,
                                        dm.PortadoresTIPO.AsString,dm.PortadoresDOCUMENTO.AsString,dm.PortadoresCODIGO.AsString);
      cbTipoAprChange(Sender);

      if edSacador.Text = '' then
        edSacador.Text := edApresentante.Text;

      if edCedente.Text = '' then
        edCedente.Text := edApresentante.Text;
  end
  else
    cbTipoApr.SetFocus;
end;

procedure TFTitulo.edDocAprExit(Sender: TObject);
begin
  if edDocApr.Text='' then Exit;

  dm.Portadores.Close;
  dm.Portadores.CommandText:='SELECT * from PORTADORES WHERE DOCUMENTO=:D';
  dm.Portadores.Params.ParamByName('D').AsString:=edDocApr.Text;
  dm.Portadores.Open;
  if not dm.Portadores.IsEmpty then
  begin
      if dm.PortadoresCODIGO.AsString<>'' then
        PF.CarregarPortador('C',dm.PortadoresCODIGO.AsString,dm.PortadoresNOME.AsString,
                                dm.PortadoresTIPO.AsString,dm.PortadoresDOCUMENTO.AsString,dm.PortadoresCODIGO.AsString)
          else
            if dm.PortadoresDOCUMENTO.AsString<>'' then
              PF.CarregarPortador('D',dm.PortadoresDOCUMENTO.AsString,dm.PortadoresNOME.AsString,
                                      dm.PortadoresTIPO.AsString,dm.PortadoresDOCUMENTO.AsString,dm.PortadoresCODIGO.AsString)
                else
                  PF.CarregarPortador('N',dm.PortadoresNOME.AsString,dm.PortadoresNOME.AsString,
                                          dm.PortadoresTIPO.AsString,dm.PortadoresDOCUMENTO.AsString,dm.PortadoresCODIGO.AsString);
      cbTipoAprChange(Sender);

      if edSacador.Text = '' then
        edSacador.Text := edApresentante.Text;

      if edCedente.Text = '' then
        edCedente.Text := edApresentante.Text;

      edSacador.SetFocus;
  end
  else
    sbPortadorClick(Sender);
end;

procedure TFTitulo.edDocAprClick(Sender: TObject);
begin
  edDocApr.SelectAll;
end;

procedure TFTitulo.edDocDevClick(Sender: TObject);
begin
  edDocDev.SelectAll;
end;

procedure TFTitulo.edDocDevExit(Sender: TObject);
var
  Q: TFDQuery;
begin
  if GR.PegarNumeroTexto(edDocDev.Text) = '' then
    Exit;

  Q := TFDQuery.Create(Nil);
  Q.Connection := dm.conSISTEMA;
  Q.SQL.Add('SELECT * FROM DEVEDORES WHERE DOCUMENTO = ' + QuotedStr(edDocDev.Text));
  Q.Open;

  if not Q.IsEmpty then
  begin
    dm.RxDevedorNOME.AsString          := Q.FieldByName('NOME').AsString;
    dm.RxDevedorTIPO.AsString          := Q.FieldByName('TIPO').AsString;
    dm.RxDevedorDOCUMENTO.AsString     := Q.FieldByName('DOCUMENTO').AsString;
    dm.RxDevedorIFP_DETRAN.AsString    := Q.FieldByName('IFP_DETRAN').AsString;
    dm.RxDevedorIDENTIDADE.AsString    := Q.FieldByName('IDENTIDADE').AsString;
    dm.RxDevedorORGAO.AsString         := Q.FieldByName('ORGAO').AsString;
    dm.RxDevedorENDERECO.AsString      := Q.FieldByName('ENDERECO').AsString;
    dm.RxDevedorBAIRRO.AsString        := Q.FieldByName('BAIRRO').AsString;
    dm.RxDevedorMUNICIPIO.AsString     := Q.FieldByName('MUNICIPIO').AsString;
    dm.RxDevedorUF.AsString            := Q.FieldByName('UF').AsString;
    dm.RxDevedorCEP.AsString           := Q.FieldByName('CEP').AsString;
    dm.RxDevedorIGNORADO.AsString      := Q.FieldByName('IGNORADO').AsString;
    dm.RxDevedorJUSTIFICATIVA.AsString := Q.FieldByName('JUSTIFICATIVA').AsString;

    if (Q.FieldByName('DT_EMISSAO').AsDateTime <> 0) and
      (not Q.FieldByName('DT_EMISSAO').IsNull) then
      dm.RxDevedorDT_EMISSAO.AsDateTime := Q.FieldByName('DT_EMISSAO').AsDateTime;
  end;

  Q.Close;
  Q.Free;
end;

procedure TFTitulo.edSacadorExit(Sender: TObject);
begin
  if edCedente.Text = '' then
    edCedente.Text := edSacador.Text;
end;

procedure TFTitulo.edCodigoExit(Sender: TObject);
begin
  if edCodigo.Text='' then Exit;

  dm.Portadores.Close;
  dm.Portadores.CommandText:='SELECT * FROM PORTADORES WHERE CODIGO=:C';
  dm.Portadores.Params.ParamByName('C').AsString:=edCodigo.Text;
  dm.Portadores.Open;
  if not dm.Portadores.IsEmpty then
  begin
      cbTipoApr.ItemIndex :=cbTipoApr.Items.IndexOf(dm.PortadoresTIPO.AsString);
      edDocApr.Text       :=dm.PortadoresDOCUMENTO.AsString;
      edApresentante.Text :=dm.PortadoresNOME.AsString;

      cbTipoAprChange(Sender);

      if edSacador.Text = '' then
        edSacador.Text := edApresentante.Text;

      if edCedente.Text = '' then
        edCedente.Text := edApresentante.Text;

      edSacador.SetFocus;
  end;

  dm.Portadores.Close;
  dm.Portadores.CommandText:='SELECT * FROM PORTADORES ORDER BY NOME';
end;

procedure TFTitulo.edLivroProtocoloKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFTitulo.btObsClick(Sender: TObject);
begin
  GR.CriarForm(TFObservacao,FObservacao);
end;

procedure TFTitulo.btProximoClick(Sender: TObject);
var
  Q: TFDQuery;
begin
  dm.Q1:=TFDQuery.Create(Nil);
  dm.Q1.Connection:=dm.conSISTEMA;
  dm.Q1.Close;
  dm.Q1.SQL.Clear;
  dm.Q1.SQL.Add('SELECT MIN(PROTOCOLO) FROM TITULOS WHERE PROTOCOLO>:P');
  dm.Q1.ParamByName('P').AsInteger:=StrToInt(edProtocoloD.Text);
  dm.Q1.Open;
  if not dm.Q1.IsEmpty then
  begin
      if dm.Q1.FieldByName('MIN').AsString<>'' then
      begin
          Q:=TFDQuery.Create(Nil);
          Q.Connection:=dm.conSISTEMA;
          Q.SQl.Add('SELECT ID_ATO FROM TITULOS WHERE PROTOCOLO='''+dm.Q1.FieldByName('MIN').AsString+'''');
          Q.Open;
          if not Q.IsEmpty then
          CarregarTitulo(Q.FieldByName('ID_ATO').AsInteger);
          Q.Close;
          Q.Free;
      end;
  end;
  dm.Q1.Close;
  dm.Q1.Free;
end;

procedure TFTitulo.btAnteriorClick(Sender: TObject);
var
  Q: TFDQuery;
begin
  dm.Q1:=TFDQuery.Create(nil);
  dm.Q1.Connection:=dm.conSISTEMA;
  dm.Q1.Close;
  dm.Q1.SQL.Clear;
  dm.Q1.SQL.Add('SELECT MAX(PROTOCOLO) FROM TITULOS WHERE PROTOCOLO<:P');
  dm.Q1.ParamByName('P').AsInteger:=StrToInt(edProtocoloD.Text);
  dm.Q1.Open;
  if not dm.Q1.IsEmpty then
  begin
      if dm.Q1.FieldByName('MAX').AsString<>'' then
      begin
          Q:=TFDQuery.Create(Nil);
          Q.Connection:=dm.conSISTEMA;
          Q.SQL.Add('SELECT ID_ATO FROM TITULOS WHERE PROTOCOLO='''+dm.Q1.FieldByName('MAX').AsString+'''');
          Q.Open;
          if not Q.IsEmpty then
          CarregarTitulo(Q.FieldByName('ID_ATO').AsInteger);
          Q.Close;
          Q.Free;
      end;
  end;
  dm.Q1.Close;
  dm.Q1.Free;
end;

procedure TFTitulo.btPesquisarClick(Sender: TObject);
var
  Protocolo: String;
begin
  InputQuery('Pesquisar','Protocolo',Protocolo);
  if Protocolo<>'' then
  begin
      dm.Q1:=TFDQuery.Create(nil);
      dm.Q1.Connection:=dm.conSISTEMA;
      dm.Q1.Close;
      dm.Q1.SQL.Clear;
      dm.Q1.SQL.Add('SELECT ID_ATO FROM TITULOS WHERE PROTOCOLO=:P');
      dm.Q1.ParamByName('P').AsInteger:=StrToInt(Protocolo);
      dm.Q1.Open;
      if not dm.Q1.IsEmpty then
      begin
          Visualizacao;
          CarregarTitulo(dm.Q1.FieldByName('ID_ATO').AsInteger);
      end
      else
      begin
          Fechado;
          GR.Aviso('Protocolo n�o encontrado.');
      end;
      dm.Q1.Close;
      dm.Q1.Free;
  end;
end;

procedure TFTitulo.btIncluirClick(Sender: TObject);
begin
  Inclusao;

  btLimparClick(Sender);

  dm.RxPortador.Close;
  dm.RxPortador.Open;
  dm.RxPortador.Append;
  dm.RxPortadorTIPO.AsString               := 'F';
  dm.RxPortadorCONVENIO.AsString           := 'N';
  dm.RxPortadorBANCO.AsString              := 'N';
  dm.RxPortadorFLG_PAGAANTECIPADO.AsString := 'N';

  dm.RxDevedor.Close;
  dm.RxDevedor.Open;
  dm.RxDevedor.Append;
  dm.RxDevedorTIPO.AsString       :='F';
  dm.RxDevedorIFP_DETRAN.AsString :='I';
  dm.RxDevedorIGNORADO.AsString   :='N';
  dm.RxCustas.Close;
  dm.RxCustas.Open;
  dm.vMotivo:='';

  cbEndosso.ItemIndex         :=0;
  cbTipoApr.ItemIndex         :=0;
  cbTipoApresentacao.ItemIndex:=0;
  edProtocoloD.Text           :=IntToStr(dm.ValorAtual('NPR','N'));
  edLivroProtocolo.Text       :=IntToStr(dm.ValorAtual('LPR','N'));
  edFolhaProtocolo.Text       :=IntToStr(dm.ValorAtual('FPR','N'));
  edDataProtocoloD.Date       :=Now;

  btnDesfazerExportacaoSA.Visible := False;

  cbTipoAprChange(Sender);
  cbTipoDevChange(Sender);

  edDataProtocoloD.SetFocus;
end;

procedure TFTitulo.btLimparClick(Sender: TObject);
begin
  edTotal.Clear;
  edtArquivoD.Clear;
  edtArquivoR.Clear;
  edtServentiaAgregada.Clear;
  lblServentiaAgregada.Caption := '';
  edBanco.Clear;
  edValor.Clear;
  edCodigo.Clear;
  edDocApr.Clear;
  edRecibo.Clear;
  edCCT.Clear;
  edNumero.Clear;
  edtNossoNumero.Clear;
  edCustas.Clear;
  edSacador.Clear;
  edCedente.Clear;
  edRegistro.Clear;
  edProtocoloD.Clear;
  edProtocoloSA.Clear;
  edDataEnvio.Clear;
  edPagamento.Clear;
  edDataTitulo.Clear;
  edSeloRegistro.Clear;
  edApresentante.Clear;
  edDataRegistro.Clear;
  edDataSustacao.Clear;
  edDataRetirada.Clear;
  edDataProtocoloD.Clear;
  edDataProtocoloSA.Clear;
  edDataIntimacao.Clear;
  edLivroRegistro.Clear;
  edFolhaRegistro.Clear;
  edDataPagamento.Clear;
  edLivroProtocolo.Clear;
  edFolhaProtocolo.Clear;

  if edDataVencimento.Visible then
    edDataVencimento.Clear;

  edSeloRegistro.Clear;
  edSeloSolucao.Clear;
  edAleatorioProtesto.Clear;
  edAleatorioSolucao.Clear;
  dm.RxDevedor.Close;
  cbTipoApr.ItemIndex           :=-1;
  cbEndosso.ItemIndex           :=-1;
  cbTipoApresentacao.ItemIndex  :=-1;
  cbTipoProtesto.ItemIndex      :=-1;
  cbTipoIntimacao.ItemIndex     :=-1;
  //cbConvenio.ItemIndex          :=-1;
  dm.RxPortador.Close;
  dm.RxPortador.Open;
  dm.RxDevedor.Close;
  dm.RxDevedor.Open;
  lkEscrevente.KeyValue         :=Null;
  lkEscreventeP.KeyValue        :=Null;
  lkEspecie.KeyValue            :=Null;
  lkEscrevente.KeyValue         :=Null;

  btnDesfazerExportacaoSA.Visible := False;
end;

procedure TFTitulo.btnCancelarDevClick(Sender: TObject);
begin
  if dm.RxDevedor.State in [dsInsert, dsEdit] then
  begin
    dm.RxDevedor.Cancel;

    cbTipoDev.ItemIndex    := 0;
    edDocDev.Clear;
    edDevedor.Clear;
    edtEndDevedor.Clear;
    edtBairroDevedor.Clear;
    lcbMunDevedor.KeyValue := '';
    cbUFDevedor.ItemIndex  := cbUFDevedor.IndexOf('RJ');
    medCEPDevedor.Text     := '';

    HasDesabBotoesDevedor(True);
  end;
end;

procedure TFTitulo.btnConfirmarDevClick(Sender: TObject);
begin
  if dm.RxDevedor.State in [dsInsert, dsEdit] then
  begin
    dm.RxDevedorNOME.AsString       := edDevedor.Text;
    dm.RxDevedorTIPO.AsString       := cbTipoDev.Text;
    dm.RxDevedorDOCUMENTO.AsString  := edDocDev.Text;
    dm.RxDevedorIGNORADO.AsString   := 'N';
    dm.RxDevedorENDERECO.AsString   := edtEndDevedor.Text;
    dm.RxDevedorBAIRRO.AsString     := edtBairroDevedor.Text;
    dm.RxDevedorMUNICIPIO.AsString  := lcbMunDevedor.Text;
    dm.RxDevedorUF.AsString         := cbUFDevedor.Text;
    dm.RxDevedorCEP.AsString        := medCEPDevedor.Text;
    dm.RxDevedor.Post;

    HasDesabBotoesDevedor(True);
  end;
end;

procedure TFTitulo.btnDesfazerExportacaoSAClick(Sender: TObject);
begin
  try
    dm.ServentiaAgregada.Close;
    dm.ServentiaAgregada.Open;

    if dm.ServentiaAgregada.Locate('CODIGO_DISTRIBUICAO',
                                   dm.Titulos.FieldByName('CARTORIO').AsInteger,
                                   []) then
    begin
      dm.ServentiaAgregada.Edit;
      dm.ServentiaAgregada.FieldByName('QTD_ULTIMO_SALDO').AsInteger   := (dm.ServentiaAgregada.FieldByName('QTD_ULTIMO_SALDO').AsInteger -
                                                                           1);
      dm.ServentiaAgregada.FieldByName('VALOR_ULTIMO_SALDO').AsFloat   := (dm.ServentiaAgregada.FieldByName('VALOR_ULTIMO_SALDO').AsFloat -
                                                                           dm.Titulos.FieldByName('TOTAL').AsFloat);
      dm.ServentiaAgregada.FieldByName('DATA_ULTIMO_SALDO').AsDateTime := Date;
      dm.ServentiaAgregada.Post;
    end;

    btAlterar.Click;

    if not (dm.Titulos.State = dsEdit) then
      dm.Titulos.Edit;

    dm.Titulos.FieldByName('FLG_SALDO').AsString     := 'N';
    dm.Titulos.FieldByName('ARQUIVO_CARTORIO').Value := Null;
    dm.Titulos.Post;

    Application.MessageBox('Exporta��o do T�tulo realizada com sucesso!',
                           'Sucesso!',
                           MB_OK);

    btSalvar.Click;
  except
    on E:Exception do
    begin
      Application.MessageBox(PChar('N�o foi poss�vel desfazer a Exporta��o desse T�tulo.'),
                             'Erro',
                             MB_OK + MB_ICONERROR);

      dm.ServentiaAgregada.Cancel;
      btCancelar.Click;
    end;
  end;
end;

procedure TFTitulo.btnEditarDevClick(Sender: TObject);
begin
  dm.RxDevedor.Edit;
  HasDesabBotoesDevedor(False);
end;

procedure TFTitulo.btnExcluirDevClick(Sender: TObject);
begin
  if dm.RxDevedor.RecordCount > 0 then
  begin
    dm.RxDevedor.Delete;
    HasDesabBotoesDevedor(False);
  end;
end;

procedure TFTitulo.btnIncluirDevClick(Sender: TObject);
begin
  dm.RxDevedor.Append;

  cbTipoDev.ItemIndex    := 0;
  edDocDev.Clear;
  edDevedor.Clear;
  edtEndDevedor.Clear;
  edtBairroDevedor.Clear;
  lcbMunDevedor.KeyValue := '';
  cbUFDevedor.ItemIndex  := cbUFDevedor.IndexOf('RJ');
  medCEPDevedor.Text     := '';

  HasDesabBotoesDevedor(False);
end;

procedure TFTitulo.btAlterarClick(Sender: TObject);
begin
  Alteracao;

  btnDesfazerExportacaoSA.Visible := False;

  edDataProtocoloD.SetFocus;
end;

procedure TFTitulo.btExcluirClick(Sender: TObject);
begin
  if GR.Pergunta('Confirma a Exclus�o do Protocolo N�: '+edProtocoloD.Text) then
  begin
      PF.ExcluirFD('TITULOS WHERE PROTOCOLO='+edProtocoloD.Text,dm.conSISTEMA);
      btLimparClick(Sender);
      Fechado;
      GR.Aviso('Protocolo Exclu�do.');
  end;
end;

procedure TFTitulo.Inclusao;
begin
  edNumero.MaxLength       := 11;
  edtNossoNumero.MaxLength := 15;

  dm.vTipo            := 'I';
  GbProtocolo.Enabled := True;
  GbTitulo.Enabled    := True;
  GbRegistro.Enabled  := True;
  GbPartes.Enabled    := True;

{  //Paticipantes
  cbTipoApr.ReadOnly        := False;
  edDocApr.ReadOnly         := False;
  ImOk1.Enabled             := True;
  edCodigo.ReadOnly         := False;
  edApresentante.ReadOnly   := False;
  sbPortador.Enabled        := True;
  sbLocalizar.Enabled       := True;
  edSacador.ReadOnly        := False;
  sbSacador.Enabled         := True;
  edCedente.ReadOnly        := False;
  cbTipoDev.ReadOnly        := False;
  edDocDev.ReadOnly         := False;
  ImOk2.Enabled             := True and ImOk2.Visible;
  ImAviso2.Enabled          := True and ImAviso2.Visible;
  edDevedor.ReadOnly        := False;
  sbDevedor.Enabled         := True;
  sbDevedores.Enabled       := True;
  edtEndDevedor.ReadOnly    := False;
  edtBairroDevedor.ReadOnly := False;
  lcbMunDevedor.ReadOnly    := False;
  cbUFDevedor.ReadOnly      := False;
  medCEPDevedor.ReadOnly    := False;
  dbgDevedores.ReadOnly     := False; }

  //Paticipantes
  cbTipoApr.Enabled        := True;
  edDocApr.Enabled         := True;
  ImOk1.Enabled            := True;
  edCodigo.Enabled         := True;
  edApresentante.Enabled   := True;
  sbPortador.Enabled       := True;
  sbLocalizar.Enabled      := True;
  edSacador.Enabled        := True;
  sbSacador.Enabled        := True;
  edCedente.Enabled        := True;
  cbTipoDev.Enabled        := True;
  edDocDev.Enabled         := True;
  ImOk2.Enabled            := True and ImOk2.Visible;
  ImAviso2.Enabled         := True and ImAviso2.Visible;
  edDevedor.Enabled        := True;
  sbDevedor.Enabled        := True;
  sbDevedores.Enabled      := True;
  edtEndDevedor.Enabled    := True;
  edtBairroDevedor.Enabled := True;
  lcbMunDevedor.Enabled    := True;
  cbUFDevedor.Enabled      := True;
  medCEPDevedor.Enabled    := True;
  dbgDevedores.Enabled     := True;

  HasDesabBotoesDevedor(True);

  btIncluir.Enabled       := False;
  btAlterar.Enabled       := False;
  btExcluir.Enabled       := False;
  btPesquisar.Enabled     := False;
  btAnterior.Enabled      := False;
  btProximo.Enabled       := False;
  btSalvar.Enabled        := True;
  btCancelar.Enabled      := True;
  btLimpar.Enabled        := True;
  ckFins.Enabled          := True;
  ckPostecipado.Enabled   := True;
  edSeloRegistro.ReadOnly := False;
  edSeloSolucao.ReadOnly  := False;
end;

procedure TFTitulo.Alteracao;
begin
  edNumero.MaxLength       := 11;
  edtNossoNumero.MaxLength := 15;

  dm.vTipo            := 'A';
  GbProtocolo.Enabled := True;
  GbTitulo.Enabled    := True;
  GbRegistro.Enabled  := True;
  GbPartes.Enabled    := True;

{  //Paticipantes
  cbTipoApr.ReadOnly        := False;
  edDocApr.ReadOnly         := False;
  ImOk1.Enabled             := True;
  edCodigo.ReadOnly         := False;
  edApresentante.ReadOnly   := False;
  sbPortador.Enabled        := True;
  sbLocalizar.Enabled       := True;
  edSacador.ReadOnly        := False;
  sbSacador.Enabled         := True;
  edCedente.ReadOnly        := False;
  cbTipoDev.ReadOnly        := False;
  edDocDev.ReadOnly         := False;
  ImOk2.Enabled             := True and ImOk2.Visible;
  ImAviso2.Enabled          := True and ImAviso2.Visible;
  edDevedor.ReadOnly        := False;
  sbDevedor.Enabled         := True;
  sbDevedores.Enabled       := True;
  edtEndDevedor.ReadOnly    := False;
  edtBairroDevedor.ReadOnly := False;
  lcbMunDevedor.ReadOnly    := False;
  cbUFDevedor.ReadOnly      := False;
  medCEPDevedor.ReadOnly    := False;
  dbgDevedores.ReadOnly     := False; }

  //Paticipantes
  cbTipoApr.Enabled        := True;
  edDocApr.Enabled         := True;
  ImOk1.Enabled            := True;
  edCodigo.Enabled         := True;
  edApresentante.Enabled   := True;
  sbPortador.Enabled       := True;
  sbLocalizar.Enabled      := True;
  edSacador.Enabled        := True;
  sbSacador.Enabled        := True;
  edCedente.Enabled        := True;
  cbTipoDev.Enabled        := True;
  edDocDev.Enabled         := True;
  ImOk2.Enabled            := True and ImOk2.Visible;
  ImAviso2.Enabled         := True and ImAviso2.Visible;
  edDevedor.Enabled        := True;
  sbDevedor.Enabled        := True;
  sbDevedores.Enabled      := True;
  edtEndDevedor.Enabled    := True;
  edtBairroDevedor.Enabled := True;
  lcbMunDevedor.Enabled    := True;
  cbUFDevedor.Enabled      := True;
  medCEPDevedor.Enabled    := True;
  dbgDevedores.Enabled     := True;

  HasDesabBotoesDevedor(True);

  btIncluir.Enabled       := False;
  btAlterar.Enabled       := False;
  btExcluir.Enabled       := False;
  btPesquisar.Enabled     := False;
  btAnterior.Enabled      := False;
  btProximo.Enabled       := False;
  btSalvar.Enabled        := True;
  btCancelar.Enabled      := True;
  btLimpar.Enabled        := True;
  ckFins.Enabled          := True;
  ckPostecipado.Enabled   := True;

  if edDataProtocoloD.Date < StrToDate('21/03/2014') then
  begin
    edSeloRegistro.ReadOnly := False;
    edSeloSolucao.ReadOnly  := False;
  end;
end;

procedure TFTitulo.Visualizacao;
begin
  edNumero.MaxLength       := 15;
  edtNossoNumero.MaxLength := 15;

  GbProtocolo.Enabled := False;
  GbTitulo.Enabled    := False;
  GbRegistro.Enabled  := False;
  GbPartes.Enabled    := True;

{  //Paticipantes
  cbTipoApr.ReadOnly        := True;
  edDocApr.ReadOnly         := True;
  ImOk1.Enabled             := False;
  edCodigo.ReadOnly         := True;
  edApresentante.ReadOnly   := True;
  sbPortador.Enabled        := False;
  sbLocalizar.Enabled       := False;
  edSacador.ReadOnly        := True;
  sbSacador.Enabled         := False;
  edCedente.ReadOnly        := True;
  cbTipoDev.ReadOnly        := True;
  edDocDev.ReadOnly         := True;
  ImOk2.Enabled             := False and ImOk2.Visible;
  ImAviso2.Enabled          := False and ImAviso2.Visible;
  edDevedor.ReadOnly        := True;
  sbDevedor.Enabled         := False;
  sbDevedores.Enabled       := False;
  edtEndDevedor.ReadOnly    := True;
  edtBairroDevedor.ReadOnly := True;
  lcbMunDevedor.ReadOnly    := True;
  cbUFDevedor.ReadOnly      := True;
  medCEPDevedor.ReadOnly    := True;
  dbgDevedores.ReadOnly     := True;
  btnIncluirDev.Enabled     := False;
  btnEditarDev.Enabled      := False;
  btnExcluirDev.Enabled     := False;
  btnConfirmarDev.Enabled   := False;
  btnCancelarDev.Enabled    := False;  }

  //Paticipantes
  cbTipoApr.Enabled        := False;
  edDocApr.Enabled         := False;
  ImOk1.Enabled            := False;
  edCodigo.Enabled         := False;
  edApresentante.Enabled   := False;
  sbPortador.Enabled       := False;
  sbLocalizar.Enabled      := False;
  edSacador.Enabled        := False;
  sbSacador.Enabled        := False;
  edCedente.Enabled        := False;
  cbTipoDev.Enabled        := False;
  edDocDev.Enabled         := False;
  ImOk2.Enabled            := False and ImOk2.Visible;
  ImAviso2.Enabled         := False and ImAviso2.Visible;
  edDevedor.Enabled        := False;
  sbDevedor.Enabled        := False;
  sbDevedores.Enabled      := False;
  edtEndDevedor.Enabled    := False;
  edtBairroDevedor.Enabled := False;
  lcbMunDevedor.Enabled    := False;
  cbUFDevedor.Enabled      := False;
  medCEPDevedor.Enabled    := False;
  dbgDevedores.Enabled     := True;
  btnIncluirDev.Enabled    := False;
  btnEditarDev.Enabled     := False;
  btnExcluirDev.Enabled    := False;
  btnConfirmarDev.Enabled  := False;
  btnCancelarDev.Enabled   := False;

  btIncluir.Enabled     := True;
  btAlterar.Enabled     := Trim(dm.TitulosTIPO_APRESENTACAO.AsString) = 'B';
  btExcluir.Enabled     := True;
  btPesquisar.Enabled   := True;
  btAnterior.Enabled    := True;
  btProximo.Enabled     := True;
  btSalvar.Enabled      := False;
  btCancelar.Enabled    := False;
  btLimpar.Enabled      := False;
end;

procedure TFTitulo.Fechado;
begin
  edNumero.MaxLength       := 15;
  edtNossoNumero.MaxLength := 15;

  GbProtocolo.Enabled := False;
  GbTitulo.Enabled    := False;
  GbRegistro.Enabled  := False;
  GbPartes.Enabled    := True;

{  //Paticipantes
  cbTipoApr.ReadOnly        := True;
  edDocApr.ReadOnly         := True;
  ImOk1.Enabled             := False;
  edCodigo.ReadOnly         := True;
  edApresentante.ReadOnly   := True;
  sbPortador.Enabled        := False;
  sbLocalizar.Enabled       := False;
  edSacador.ReadOnly        := True;
  sbSacador.Enabled         := False;
  edCedente.ReadOnly        := True;
  cbTipoDev.ReadOnly        := True;
  edDocDev.ReadOnly         := True;
  ImOk2.Enabled             := False and ImOk2.Visible;
  ImAviso2.Enabled          := False and ImAviso2.Visible;
  edDevedor.ReadOnly        := True;
  sbDevedor.Enabled         := False;
  sbDevedores.Enabled       := False;
  edtEndDevedor.ReadOnly    := True;
  edtBairroDevedor.ReadOnly := True;
  lcbMunDevedor.ReadOnly    := True;
  cbUFDevedor.ReadOnly      := True;
  medCEPDevedor.ReadOnly    := True;
  dbgDevedores.ReadOnly     := True;
  btnIncluirDev.Enabled     := False;
  btnEditarDev.Enabled      := False;
  btnExcluirDev.Enabled     := False;
  btnConfirmarDev.Enabled   := False;
  btnCancelarDev.Enabled    := False;  }

  //Paticipantes
  cbTipoApr.Enabled        := False;
  edDocApr.Enabled         := False;
  ImOk1.Enabled            := False;
  edCodigo.Enabled         := False;
  edApresentante.Enabled   := False;
  sbPortador.Enabled       := False;
  sbLocalizar.Enabled      := False;
  edSacador.Enabled        := False;
  sbSacador.Enabled        := False;
  edCedente.Enabled        := False;
  cbTipoDev.Enabled        := False;
  edDocDev.Enabled         := False;
  ImOk2.Enabled            := False and ImOk2.Visible;
  ImAviso2.Enabled         := False and ImAviso2.Visible;
  edDevedor.Enabled        := False;
  sbDevedor.Enabled        := False;
  sbDevedores.Enabled      := False;
  edtEndDevedor.Enabled    := False;
  edtBairroDevedor.Enabled := False;
  lcbMunDevedor.Enabled    := False;
  cbUFDevedor.Enabled      := False;
  medCEPDevedor.Enabled    := False;
  dbgDevedores.Enabled     := True;
  btnIncluirDev.Enabled    := False;
  btnEditarDev.Enabled     := False;
  btnExcluirDev.Enabled    := False;
  btnConfirmarDev.Enabled  := False;
  btnCancelarDev.Enabled   := False;

  btIncluir.Enabled     := True;
  btAlterar.Enabled     := False;
  btExcluir.Enabled     := False;
  btPesquisar.Enabled   := True;
  btAnterior.Enabled    := False;
  btProximo.Enabled     := False;
  btSalvar.Enabled      := False;
  btCancelar.Enabled    := False;
  btLimpar.Enabled      := False;
  ckFins.Enabled        := False;
  ckPostecipado.Enabled := False;
end;

procedure TFTitulo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.Escreventes.Close;
  dm.Tipos.Close;
end;

procedure TFTitulo.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if btSalvar.Enabled then
    if GR.Pergunta('EXISTE UMA OPERA��O EM ANDAMENTO!'+#13#13'DESEJA CANCELAR') then
      CanClose:=True
        else CanClose:=False;
end;

procedure TFTitulo.FormShow(Sender: TObject);
begin
  Application.ProcessMessages;

  if dm.vTipo = 'A' then
  begin
    if Trim(dm.TitulosTIPO_APRESENTACAO.AsString) = 'B' then
      btAlterar.SetFocus
    else
      btIncluir.SetFocus;
  end
  else
    btIncluir.SetFocus;

  lkEspecieClick(Sender);

  if Length(GR.PegarLetra(dm.TitulosSELO_REGISTRO.AsString))=3  then
  begin
      edSeloRegistro.ReadOnly :=False;
      edSeloRegistro.Enabled  :=True;
  end;

  if Length(GR.PegarLetra(dm.TitulosSELO_PAGAMENTO.AsString))=3 then
  begin
      edSeloSolucao.ReadOnly:=False;
      edSeloSolucao.Enabled :=True;
  end;

  edLivroProtocolo.Visible := False;
  edFolhaProtocolo.Visible := False;
  ImNovoCCT.Visible        := False;
  ImLimparCCT.Visible      := False;
  edCCT.Visible            := False;
  GbRegistro.Visible       := False;

  if dm.Titulos.State = dsBrowse then
  begin
    btnDesfazerExportacaoSA.Visible := (dm.Titulos.FieldByName('FLG_SALDO').AsString = 'S') and
                                       (dm.Titulos.FieldByName('TIPO_APRESENTACAO').AsString = 'B') and
                                       dm.Titulos.FieldByName('RETORNO_CARTORIO').IsNull;
  end
  else
    btnDesfazerExportacaoSA.Visible := False;

  dm.Devedores.Close;
  dm.Devedores.Params[0].AsInteger := 0;
  dm.Devedores.Open;
end;

procedure TFTitulo.HasDesabBotoesDevedor(Hab: Boolean);
begin
  cbTipoDev.Enabled        := not Hab;
  edDocDev.Enabled         := not Hab;
  edDevedor.Enabled        := not Hab;
  sbDevedor.Enabled        := not Hab;
  sbDevedores.Enabled      := not Hab;
  edtEndDevedor.Enabled    := not Hab;
  edtBairroDevedor.Enabled := not Hab;
  lcbMunDevedor.Enabled    := not Hab;
  cbUFDevedor.Enabled      := not Hab;
  medCEPDevedor.Enabled    := not Hab;

  btnIncluirDev.Enabled := Hab;
  btnEditarDev.Enabled  := Hab;
  btnExcluirDev.Enabled := Hab;

  btnConfirmarDev.Enabled := not Hab;
  btnCancelarDev.Enabled  := not Hab;
end;

procedure TFTitulo.ImTabelaClick(Sender: TObject);
begin
  dm.vApontamento:=0;

  Application.CreateForm(TFCustas,FCustas);
  FCustas.edApontamento.Visible:=False;
  FCustas.ShowModal;
  FCustas.Free;

  edCustas.Value:=dm.vTotal;
  edTotal.Value:=edSaldo.Value+edCustas.Value;
end;

procedure TFTitulo.sbPortadorClick(Sender: TObject);
begin
  ActiveControl:=Nil;
  GR.CriarForm(TFDadosPortador,FDadosPortador);
end;

function TFTitulo.RetornarTipoEndosso: Integer;
begin
  Result:=-1;
  if dm.TitulosTIPO_ENDOSSO.AsString='M' then Result:=0;
  if dm.TitulosTIPO_ENDOSSO.AsString='T' then Result:=1;
  if dm.TitulosTIPO_ENDOSSO.AsString='S' then Result:=2;
end;

procedure TFTitulo.cbTipoIntimacaoClick(Sender: TObject);
begin
  if cbTipoIntimacao.ItemIndex<>1 then
  begin
      edDataPublicacao.Clear;
      edDataPublicacao.Enabled:=False;
  end
  else
    edDataPublicacao.Enabled:=True;
end;

procedure TFTitulo.dsRXDevedorDataChange(Sender: TObject; Field: TField);
begin
  cbTipoDev.ItemIndex    := IfThen(dm.RxDevedorTIPO.AsString<>'',cbTipoDev.IndexOf(dm.RxDevedorTIPO.AsString),0);
  edDocDev.Text          := dm.RxDevedorDOCUMENTO.AsString;
  edDevedor.Text         := dm.RxDevedorNOME.AsString;
  edtEndDevedor.Text     := dm.RxDevedorENDERECO.AsString;
  edtBairroDevedor.Text  := dm.RxDevedorBAIRRO.AsString;
  lcbMunDevedor.KeyValue := dm.RxDevedorMUNICIPIO.AsString;
  cbUFDevedor.ItemIndex  := cbUFDevedor.IndexOf(dm.RxDevedorUF.AsString);
  medCEPDevedor.Text     := dm.RxDevedorCEP.AsString;
end;

procedure TFTitulo.cbTipoIntimacaoChange(Sender: TObject);
begin
  if cbTipoIntimacao.ItemIndex<>1 then
  begin
      edDataPublicacao.Clear;
      edDataPublicacao.Enabled:=False;
  end
  else
    edDataPublicacao.Enabled:=True;
end;

procedure TFTitulo.sbSacadorClick(Sender: TObject);
begin
  ActiveControl:=Nil;
  GR.CriarForm(TFSacador,FSacador);
end;

procedure TFTitulo.edProtocoloSAKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFTitulo.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFTitulo.lbStatusClick(Sender: TObject);
begin
  if dm.vTipo='A' then
    Application.CreateForm(TFStatus,FStatus);

  with FStatus do
  begin
    if dm.TitulosSTATUS.AsString = 'APONTADO'          then rgStatus.ItemIndex := 0;
    if dm.TitulosSTATUS.AsString = 'INTIMADO PESSOAL'  then rgStatus.ItemIndex := 1;
    if dm.TitulosSTATUS.AsString = 'INTIMADO EDITAL'   then rgStatus.ItemIndex := 2;
    if dm.TitulosSTATUS.AsString = 'INTIMADO CARTA'    then rgStatus.ItemIndex := 3;
    if dm.TitulosSTATUS.AsString = 'PROTESTADO'        then rgStatus.ItemIndex := 4;
    if dm.TitulosSTATUS.AsString = 'PAGO'              then rgStatus.ItemIndex := 5;
    if dm.TitulosSTATUS.AsString = 'CANCELADO'         then rgStatus.ItemIndex := 6;
    if dm.TitulosSTATUS.AsString = 'RETIRADO'          then rgStatus.ItemIndex := 7;
    if dm.TitulosSTATUS.AsString = 'SUSTADO'           then rgStatus.ItemIndex := 8;
    if dm.TitulosSTATUS.AsString = 'DEVOLVIDO'         then rgStatus.ItemIndex := 9;

    ShowModal;
    Free;

    if dm.vMudou then
    begin
      dm.Titulos.Edit;
      dm.TitulosSTATUS.AsString:=dm.vStatus;
      dm.Titulos.Post;

//      lbStatus.Caption:=dm.TitulosPROTOCOLO.AsString+' - '+dm.vStatus;

      if Trim(dm.TitulosSTATUS.AsString) = 'Aceito' then
        lbStatus.Caption                := '(D) ' + dm.TitulosPROTOCOLO.AsString + '  |  ' +
                                           '(SA) ' + IfThen((Trim(dm.TitulosPROTOCOLO_CARTORIO.AsString) = ''),
                                                            '0' ,
                                                            dm.TitulosPROTOCOLO_CARTORIO.AsString)
      else
        lbStatus.Caption                := '(D) ' + dm.TitulosPROTOCOLO.AsString + '  |  ' +
                                           '(SA) ' + IfThen((Trim(dm.TitulosPROTOCOLO_CARTORIO.AsString) = ''),
                                                            '0' ,
                                                            dm.TitulosPROTOCOLO_CARTORIO.AsString) +
                                           ' - ' +
                                           dm.vStatus;

      if dm.TitulosSTATUS.AsString = 'CANCELADO' then
        edDataPagamento.BoundLabel.Caption := 'Data Cancelamento'
      else
        edDataPagamento.BoundLabel.Caption := 'Data Pagamento';

      if dm.TitulosSTATUS.AsString = 'DEVOLVIDO' then
        edDataRetirada.BoundLabel.Caption := 'Data da Devolu��o'
      else
        edDataRetirada.BoundLabel.Caption := 'Data da Retirada';
    end;
  end;
end;

procedure TFTitulo.ImGerarClick(Sender: TObject);
begin
  if not dm.RxCustas.IsEmpty then
  repeat dm.RxCustas.Delete;
  until dm.RxCustas.IsEmpty;

  dm.qryFaixa.Close;
  dm.qryFaixa.ParamByName('C').AsString       :=dm.RxPortadorCONVENIO.AsString;
  dm.qryFaixa.ParamByName('OCULTO1').AsString :='N';
  dm.qryFaixa.ParamByName('OCULTO2').AsString :='N';
  dm.qryFaixa.ParamByName('VALOR1').AsFloat   :=edSaldo.Value;
  dm.qryFaixa.ParamByName('VALOR2').AsFloat   :=edSaldo.Value;
  dm.qryFaixa.Open;

  dm.Itens.Close;
  dm.Itens.Params[0].AsInteger:=dm.qryFaixaCOD.AsInteger;
  dm.Itens.Open;
  dm.Itens.First;

  dm.vEmolumentos:=0;
  while not dm.Itens.Eof do
  begin
      dm.RxCustas.Append;
      dm.RxCustasID_CUSTA.AsInteger :=dm.IdAtual('ID_CUSTA','S');
      dm.RxCustasTABELA.AsString    :=dm.ItensTAB.AsString;
      dm.RxCustasITEM.AsString      :=dm.ItensITEM.AsString;
      dm.RxCustasSUBITEM.AsString   :=dm.ItensSUB.AsString;
      dm.RxCustasVALOR.AsFloat      :=dm.ItensVALOR.AsFloat;
      dm.RxCustasQTD.AsString       :=dm.ItensQTD.AsString;
      dm.RxCustasDESCRICAO.AsString :=dm.ItensDESCR.AsString;
      dm.RxCustasTOTAL.AsFloat      :=dm.RxCustasVALOR.AsFloat*dm.RxCustasQTD.AsFloat;
      dm.RxCustas.Post;
      dm.vEmolumentos:=dm.vEmolumentos+dm.RxCustasTOTAL.AsFloat;
      dm.Itens.Next;
  end;

  dm.vFetj          :=GR.NoRound(dm.vEmolumentos*0.2,2);
  dm.vFundperj      :=GR.NoRound(dm.vEmolumentos*0.05,2);
  dm.vFunperj       :=GR.NoRound(dm.vEmolumentos*0.05,2);
  dm.vFunarpen      :=GR.NoRound(dm.vEmolumentos*0.04,2);
  dm.vPmcmv         :=GR.NoRound(dm.vEmolumentos*0.02,2);
  dm.vIss           :=GR.NoRound(dm.vEmolumentos*Gdm.vAliquotaISS,2);
  dm.vMutua         :=StrToFloat(dm.ValorParametro(26));
  dm.vAcoterj       :=StrToFloat(dm.ValorParametro(27));
  dm.vDistribuicao  :=StrToFloat(dm.ValorParametro(28));
  dm.vTotal         :=dm.vEmolumentos+dm.vFetj+dm.vFundperj+dm.vFunperj+dm.vFunarpen+dm.vPmcmv+dm.vIss+
                      dm.vMutua+dm.vAcoterj+dm.vDistribuicao+dm.vTarifa+dm.vAAR;
  edCustas.Value    :=dm.vTotal;
  edTotal.Value     :=edSaldo.Value+edCustas.Value;
end;

procedure TFTitulo.edLAEKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFTitulo.Image2Click(Sender: TObject);
begin
  dm.Codigos.Close;
  dm.Codigos.Params.ParamByName('OCULTO1').AsString:='N';
  dm.Codigos.Params.ParamByName('OCULTO2').AsString:='N';
  dm.Codigos.Open;
  GR.CriarForm(TFTabela,FTabela);
  dm.Codigos.Close;
end;

procedure TFTitulo.ImLimparCCTClick(Sender: TObject);
begin
  if edCCT.Text='' then Exit;

  if GR.PasswordInputBox('SEGURAN�A','SENHA')=dm.ServentiaCODIGO.AsString then
    if GR.Pergunta('LIMPAR CCT "'+GR.FormatarSelo(edCCT.Text)+'" DO PROTOCOLO N� '+edProtocoloD.Text) then
    begin
        if GR.Pergunta('DESEJA LIBERAR O CCT NO GERENCIAL') then
        GDM.LiberarSelo('PROTESTO',edCCT.Text,False);
        edCCT.Clear;
        if dm.vTipo='A' then
        begin
            dm.Titulos.Edit;
            dm.TitulosCCT.Clear;
        end;
        btSalvar.Click;
        GR.Aviso('INFORMA��ES ATUALIZADAS COM SUCESSO!');
    end;
end;

procedure TFTitulo.ImNovoCCTClick(Sender: TObject);
var
  vIdReservado: Integer;
begin
  if dm.vTipo='I' then Exit;

  if edCCT.Text<>'' then
  begin
      GR.Aviso('LIBERE ANTES O CCT UTILIZADO!');
      edCCT.SetFocus;
      Exit;
  end;

  dm.Titulos.Edit;

  {vIdReservado:=GR.ValorGeneratorDBX('ID_RESERVADO',Gdm.BDGerencial);
  dm.TitulosCCT.AsString:=Gdm.ProximoSelo(dm.SerieAtual,'PROTESTO',Self.Name,'S',
                          dm.TitulosLIVRO_PROTOCOLO.AsString,
                          GR.PegarNumero(dm.TitulosFOLHA_PROTOCOLO.AsString),
                          GR.PegarNumero(dm.TitulosFOLHA_PROTOCOLO.AsString),0,
                          'APONTAMENTO',dm.vNome,Now,vIdReservado);

  Gdm.BaixarSelo(dm.TitulosCCT.AsString,vIdReservado,'PROTESTO',Self.Name,dm.TitulosID_ATO.AsInteger,0,4,dm.TitulosDT_PROTOCOLO.AsDateTime,
                 dm.TitulosLIVRO_PROTOCOLO.AsString,GR.PegarNumero(dm.TitulosFOLHA_PROTOCOLO.AsString),
                 GR.PegarNumero(dm.TitulosFOLHA_PROTOCOLO.AsString),0,dm.TitulosPROTOCOLO.AsInteger,0,
                 dm.TitulosCODIGO.AsInteger,'APONTAMENTO',dm.vNome,dm.TitulosCONVENIO.AsString,dm.TitulosCOBRANCA.AsString,
                 dm.TitulosEMOLUMENTOS.AsFloat,dm.TitulosFETJ.AsFloat,dm.TitulosFUNDPERJ.AsFloat,dm.TitulosFUNPERJ.AsFloat,
                 dm.TitulosFUNARPEN.AsFloat,dm.TitulosPMCMV.AsFloat,dm.TitulosMUTUA.AsFloat,dm.TitulosACOTERJ.AsFloat,
                 dm.TitulosDISTRIBUICAO.AsFloat,0,0,dm.TitulosVALOR_AR.AsFloat,dm.TitulosTARIFA_BANCARIA.AsFloat,
                 dm.TitulosTOTAL.AsFloat,dm.TitulosISS.AsFloat);   }

  edCCT.Text:=dm.TitulosCCT.AsString;
  btSalvar.Click;
  GR.Aviso('INFORMA��ES ATUALIZADAS COM SUCESSO!');
end;

procedure TFTitulo.ImLimparSelo1Click(Sender: TObject);
begin
  if edSeloRegistro.Text='' then Exit;

  if GR.PasswordInputBox('SEGURAN�A','SENHA')=dm.ServentiaCODIGO.AsString then
    if GR.Pergunta('LIMPAR SELO "'+GR.SeloFormatado(edSeloRegistro.Text,edAleatorioProtesto.Text)+'" DO PROTOCOLO N� '+edProtocoloD.Text) then
    begin
        if GR.Pergunta('DESEJA LIBERAR O SELO NO GERENCIAL') then
        GDM.LiberarSelo('PROTESTO',edSeloRegistro.Text,False);
        edSeloRegistro.Clear;
        edAleatorioProtesto.Clear;
        if dm.vTipo='A' then
        begin
            dm.Titulos.Edit;
            dm.TitulosSELO_REGISTRO.Clear;
            dm.TitulosALEATORIO_PROTESTO.Clear;
        end;
        btSalvar.Click;
        GR.Aviso('INFORMA��ES ATUALIZADAS COM SUCESSO!');
    end;
end;

procedure TFTitulo.ImNovoSelo1Click(Sender: TObject);
var
  vIdReservado: Integer;
begin
  if dm.vTipo='I' then Exit;

  if dm.TitulosSTATUS.AsString<>'PROTESTADO' then
  begin
      GR.Aviso('T�TULO N�O PROTESTADO!');
      Exit;
  end;

  if edSeloRegistro.Text<>'' then
  begin
      GR.Aviso('LIBERE ANTES O SELO UTILIZADO!');
      edSeloRegistro.SetFocus;
      Exit;
  end;

  dm.Titulos.Edit;

  {vIdReservado:=GR.ValorGeneratorDBX('ID_RESERVADO',Gdm.BDGerencial);
  dm.TitulosSELO_REGISTRO.AsString:=Gdm.ProximoSelo(dm.SerieAtual,'PROTESTO',Self.Name,'N',
                                                    dm.TitulosLIVRO_PROTOCOLO.AsString,
                                                    GR.PegarNumero(dm.TitulosFOLHA_PROTOCOLO.AsString),
                                                    GR.PegarNumero(dm.TitulosFOLHA_PROTOCOLO.AsString),0,
                                                    'PROTESTO',dm.vNome,Now,vIdReservado);

  dm.TitulosALEATORIO_PROTESTO.AsString:=Gdm.vAleatorio;

  Gdm.BaixarSelo(dm.TitulosSELO_REGISTRO.AsString,vIdReservado,'PROTESTO',Self.Name,dm.TitulosID_ATO.AsInteger,0,4,
                 dm.TitulosDT_PROTOCOLO.AsDateTime,dm.TitulosLIVRO_PROTOCOLO.AsString,GR.PegarNumero(dm.TitulosFOLHA_PROTOCOLO.AsString),
                 GR.PegarNumero(dm.TitulosFOLHA_PROTOCOLO.AsString),0,dm.TitulosPROTOCOLO.AsInteger,0,dm.TitulosCODIGO.AsInteger,'PROTESTO',
                 dm.vNome,dm.TitulosCONVENIO.AsString,dm.TitulosCOBRANCA.AsString,dm.TitulosEMOLUMENTOS.AsFloat,dm.TitulosFETJ.AsFloat,
                 dm.TitulosFUNDPERJ.AsFloat,dm.TitulosFUNPERJ.AsFloat,dm.TitulosFUNARPEN.AsFloat,dm.TitulosPMCMV.AsFloat,
                 dm.TitulosMUTUA.AsFloat,dm.TitulosACOTERJ.AsFloat,dm.TitulosDISTRIBUICAO.AsFloat,0,0,dm.TitulosVALOR_AR.AsFloat,
                 dm.TitulosTARIFA_BANCARIA.AsFloat,dm.TitulosTOTAL.AsFloat,dm.TitulosISS.AsFloat);

  edSeloRegistro.Text:=dm.TitulosSELO_REGISTRO.AsString;
  edAleatorioProtesto.Text:=dm.TitulosALEATORIO_PROTESTO.AsString; }
  btSalvar.Click;
  GR.Aviso('INFORMA��ES ATUALIZADAS COM SUCESSO!');
end;

procedure TFTitulo.ImLimparSelo2Click(Sender: TObject);
begin
  if edSeloSolucao.Text='' then Exit;

  if GR.PasswordInputBox('SEGURAN�A','SENHA')=dm.ServentiaCODIGO.AsString then
    if GR.Pergunta('LIMPAR SELO "'+GR.SeloFormatado(edSeloSolucao.Text,edAleatorioProtesto.Text)+'" DO PROTOCOLO N� '+edProtocoloD.Text) then
    begin
        if GR.Pergunta('DESEJA LIBERAR O SELO NO GERENCIAL') then
        GDM.LiberarSelo('PROTESTO',edSeloSolucao.Text,False);
        edSeloSolucao.Clear;
        edAleatorioSolucao.Clear;
        if dm.vTipo='A' then
        begin
            dm.Titulos.Edit;
            dm.TitulosSELO_PAGAMENTO.Clear;
            dm.TitulosALEATORIO_SOLUCAO.Clear;
        end;
        btSalvar.Click;
        GR.Aviso('INFORMA��ES ATUALIZADAS COM SUCESSO!');
    end;
end;

procedure TFTitulo.ImNovoSelo2Click(Sender: TObject);
var
  vStatus: String;
  vIdReservado: Integer;
begin
  if dm.vTipo='I' then Exit;

  if (dm.TitulosSTATUS.AsString='APONTADO') or
     (dm.TitulosSTATUS.AsString='INTIMADO PESSOAL') or
     (dm.TitulosSTATUS.AsString='INTIMADO CARTA') or
     (dm.TitulosSTATUS.AsString='INTIMADO EDITAL') then
  begin
      GR.Aviso('T�TULO AINDA SEM SOLU��O!');
      Exit;
  end;

  if edSeloSolucao.Text<>'' then
  begin
      GR.Aviso('LIBERE ANTES O SELO UTILIZADO!');
      edSeloSolucao.SetFocus;
      Exit;
  end;

  if dm.TitulosSTATUS.AsString='CANCELADO' then
    vStatus:='CANCELAMENTO'
      else
        if dm.TitulosSTATUS.AsString='PAGO' then
          vStatus:='PAGAMENTO'
            else
            begin
                GR.Aviso('T�TULO SEM SOLU��O!');
                Exit;
            end;

  dm.Titulos.Edit;

  {vIdReservado:=GR.ValorGeneratorDBX('ID_RESERVADO',Gdm.BDGerencial);
  dm.TitulosSELO_PAGAMENTO.AsString:=Gdm.ProximoSelo(dm.SerieAtual,'PROTESTO',Self.Name,'N',
                                                     dm.TitulosLIVRO_PROTOCOLO.AsString,
                                                     GR.PegarNumero(dm.TitulosFOLHA_PROTOCOLO.AsString),
                                                     GR.PegarNumero(dm.TitulosFOLHA_PROTOCOLO.AsString),0,
                                                     'PROTESTO',dm.vNome,Now,vIdReservado);

  dm.TitulosALEATORIO_SOLUCAO.AsString:=Gdm.vAleatorio;

  Gdm.BaixarSelo(dm.TitulosSELO_PAGAMENTO.AsString,vIdReservado,'PROTESTO',Self.Name,dm.TitulosID_ATO.AsInteger,0,4,
                 dm.TitulosDT_PROTOCOLO.AsDateTime,dm.TitulosLIVRO_PROTOCOLO.AsString,GR.PegarNumero(dm.TitulosFOLHA_PROTOCOLO.AsString),
                 GR.PegarNumero(dm.TitulosFOLHA_PROTOCOLO.AsString),0,dm.TitulosPROTOCOLO.AsInteger,0,dm.TitulosCODIGO.AsInteger,
                 vStatus,dm.vNome,dm.TitulosCONVENIO.AsString,dm.TitulosCOBRANCA.AsString,dm.TitulosEMOLUMENTOS.AsFloat,
                 dm.TitulosFETJ.AsFloat,dm.TitulosFUNDPERJ.AsFloat,dm.TitulosFUNPERJ.AsFloat,dm.TitulosFUNARPEN.AsFloat,
                 dm.TitulosPMCMV.AsFloat,dm.TitulosMUTUA.AsFloat,dm.TitulosACOTERJ.AsFloat,dm.TitulosDISTRIBUICAO.AsFloat,0,0,
                 dm.TitulosVALOR_AR.AsFloat,dm.TitulosTARIFA_BANCARIA.AsFloat,dm.TitulosTOTAL.AsFloat,dm.TitulosISS.AsFloat);

  edSeloSolucao.Text:=dm.TitulosSELO_PAGAMENTO.AsString;
  edAleatorioSolucao.Text:=dm.TitulosALEATORIO_SOLUCAO.AsString; }
  btSalvar.Click;
  GR.Aviso('INFORMA��ES ATUALIZADAS COM SUCESSO!');
end;

procedure TFTitulo.ImAle1Click(Sender: TObject);
begin
  if dm.vTipo='I' then Exit;
  if edSeloRegistro.Text='' then Exit;
  edAleatorioProtesto.Text:=Gdm.BuscarAleatorio(edSeloRegistro.Text);
end;

procedure TFTitulo.ImAle2Click(Sender: TObject);
begin
  if dm.vTipo='I' then Exit;
  if edSeloSolucao.Text='' then Exit;
  edAleatorioSolucao.Text:=Gdm.BuscarAleatorio(edSeloSolucao.Text);
end;

procedure TFTitulo.sbDevedoresClick(Sender: TObject);
begin
  if edDocDev.Text='' then
  begin
      GR.Aviso('INFORME O DOCUMENTO DO DEVEDOR!');
      edDocDev.SetFocus;
      Exit;
  end;

  if edDevedor.Text='' then
  begin
      GR.Aviso('INFORME O NOME DO DEVEDOR!');
      edDevedor.SetFocus;
      Exit;
  end;

  if dm.RxDevedor.IsEmpty then
  begin
    dm.RxDevedor.Append;
    dm.RxDevedorORDEM.AsInteger     :=1;
    dm.RxDevedorNOME.AsString       :=edDevedor.Text;
    dm.RxDevedorTIPO.AsString       :=cbTipoDev.Text;
    dm.RxDevedorDOCUMENTO.AsString  :=edDocDev.Text;
    dm.RxDevedorIGNORADO.AsString   :='N';
    dm.RxDevedorENDERECO.AsString   := edtEndDevedor.Text;
    dm.RxDevedorBAIRRO.AsString     := edtBairroDevedor.Text;
    dm.RxDevedorMUNICIPIO.AsString  := lcbMunDevedor.Text;
    dm.RxDevedorUF.AsString         := cbUFDevedor.Text;
    dm.RxDevedorCEP.AsString        := medCEPDevedor.Text;
    dm.RxDevedor.Post;
  end;

  GR.CriarForm(TFDevedores,FDevedores);

  dm.RxDevedor.Locate('ORDEM',1,[]);
  cbTipoDev.ItemIndex    := IfThen(dm.RxDevedorTIPO.AsString<>'',cbTipoDev.IndexOf(dm.RxDevedorTIPO.AsString),0);
  edDocDev.Text          := dm.RxDevedorDOCUMENTO.AsString;
  edDevedor.Text         := dm.RxDevedorNOME.AsString;
  edtEndDevedor.Text     := dm.RxDevedorENDERECO.AsString;
  edtBairroDevedor.Text  := dm.RxDevedorBAIRRO.AsString;
  lcbMunDevedor.KeyValue := dm.RxDevedorMUNICIPIO.AsString;
  cbUFDevedor.ItemIndex  := cbUFDevedor.IndexOf(dm.RxDevedorUF.AsString);
  medCEPDevedor.Text     := dm.RxDevedorCEP.AsString;
end;

end.
