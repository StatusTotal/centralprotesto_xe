unit UConsultaCertidao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FMTBcd, StdCtrls, Buttons, sBitBtn, Grids, Wwdbigrd, Wwdbgrid,
  sEdit, sComboBox, ExtCtrls, sPanel, SqlExpr, Provider, DB, DBClient,
  Menus, Mask, sMaskEdit, sCustomComboEdit, sTooledit, sDBDateEdit,
  DBCtrls, sDBEdit, SimpleDS, sLabel, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFConsultaCertidao = class(TForm)
    PM: TPopupMenu;
    Excluir: TMenuItem;
    dsConsulta: TDataSource;
    P1: TsPanel;
    cbBuscar: TsComboBox;
    edPesquisar: TsEdit;
    P2: TsPanel;
    edData: TsDateEdit;
    edPedido: TsDateEdit;
    GridAtos: TwwDBGrid;
    btImprimir: TsBitBtn;
    edEntrega: TsDateEdit;
    sLabel1: TsLabel;
    qryConsulta: TFDQuery;
    qryConsultaID_CERTIDAO: TIntegerField;
    qryConsultaID_ATO: TIntegerField;
    qryConsultaORDEM: TIntegerField;
    qryConsultaDT_PEDIDO: TDateField;
    qryConsultaDT_ENTREGA: TDateField;
    qryConsultaDT_CERTIDAO: TDateField;
    qryConsultaTIPO_CERTIDAO: TStringField;
    qryConsultaNUMERO_TITULO: TStringField;
    qryConsultaANOS: TIntegerField;
    qryConsultaCONVENIO: TStringField;
    qryConsultaCODIGO: TIntegerField;
    qryConsultaSELO: TStringField;
    qryConsultaALEATORIO: TStringField;
    qryConsultaRECIBO: TIntegerField;
    qryConsultaREQUERIDO: TStringField;
    qryConsultaTIPO_REQUERIDO: TStringField;
    qryConsultaCPF_CNPJ_REQUERIDO: TStringField;
    qryConsultaREQUERENTE: TStringField;
    qryConsultaCPF_CNPJ_REQUERENTE: TStringField;
    qryConsultaTELEFONE_REQUERENTE: TStringField;
    qryConsultaRESULTADO: TStringField;
    qryConsultaFOLHAS: TIntegerField;
    qryConsultaREGISTROS: TIntegerField;
    qryConsultaCOBRANCA: TStringField;
    qryConsultaEMOLUMENTOS: TFloatField;
    qryConsultaFETJ: TFloatField;
    qryConsultaFUNDPERJ: TFloatField;
    qryConsultaFUNPERJ: TFloatField;
    qryConsultaFUNARPEN: TFloatField;
    qryConsultaPMCMV: TFloatField;
    qryConsultaISS: TFloatField;
    qryConsultaMUTUA: TFloatField;
    qryConsultaACOTERJ: TFloatField;
    qryConsultaDISTRIBUICAO: TFloatField;
    qryConsultaAPONTAMENTO: TFloatField;
    qryConsultaCAPA: TFloatField;
    qryConsultaTOTAL: TFloatField;
    qryConsultaEXRECIBO: TIntegerField;
    qryConsultaEXCOBRANCA: TStringField;
    qryConsultaEXEMOLUMENTOS: TFloatField;
    qryConsultaEXFETJ: TFloatField;
    qryConsultaEXFUNDPERJ: TFloatField;
    qryConsultaEXFUNPERJ: TFloatField;
    qryConsultaEXISS: TFloatField;
    qryConsultaEXFUNARPEN: TFloatField;
    qryConsultaEXTOTAL: TFloatField;
    qryConsultaCCT: TStringField;
    qryConsultaESCREVENTE: TStringField;
    qryConsultaRESPONSAVEL: TStringField;
    qryConsultaOBSERVACAO: TMemoField;
    qryConsultaENVIADO: TStringField;
    qryConsultaENVIADO_FLS: TStringField;
    qryConsultaQTD: TIntegerField;
    qryConsultaNomeCertidao: TStringField;
    qryConsultaExcedentes: TIntegerField;
    procedure edPesquisarKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ExcluirClick(Sender: TObject);
    procedure edDataKeyPress(Sender: TObject; var Key: Char);
    procedure edPedidoKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure btImprimirClick(Sender: TObject);
    procedure edEntregaKeyPress(Sender: TObject; var Key: Char);
    procedure qryConsultaAfterPost(DataSet: TDataSet);
    procedure qryConsultaCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FConsultaCertidao: TFConsultaCertidao;

implementation

uses UDM, UPF, UCertidao, UGeral, UGDM, UQuickCertidaoCancelamento1,
  UQuickPagamento1, Math;

{$R *.dfm}

procedure TFConsultaCertidao.edPesquisarKeyPress(Sender: TObject; var Key: Char);
var
  Query: String;
begin
  if key=#13 then
  begin
      if edPesquisar.Text='' then Exit;

      Query:='SELECT * FROM CERTIDOES WHERE ';

      qryConsulta.Close;

      {REQUERENTE}
      if cbBuscar.ItemIndex=0 then
      qryConsulta.SQL.Text:=Query+'REQUERENTE LIKE'+QuotedStr(edPesquisar.Text+'%')+' ORDER BY RECIBO';

      {REQUERIDO}
      if cbBuscar.ItemIndex=1 then
      qryConsulta.SQL.Text:=Query+'REQUERIDO LIKE'+QuotedStr(edPesquisar.Text+'%')+' ORDER BY RECIBO';

      {DOCUMENTO}
      if cbBuscar.ItemIndex=2 then
      qryConsulta.SQL.Text:=Query+'CPF_CNPJ_REQUERIDO='+QuotedStr(edPesquisar.Text)+' ORDER BY REQUERIDO';

      {RECIBO}
      if cbBuscar.ItemIndex=3 then
      qryConsulta.SQL.Text:=Query+'RECIBO='+QuotedStr(edPesquisar.Text)+' ORDER BY RECIBO';

      {SELO}
      if cbBuscar.ItemIndex=4 then
      qryConsulta.SQL.Text:=Query+'SELO='+QuotedStr(edPesquisar.Text)+' ORDER BY RECIBO';

      qryConsulta.Open;
  end;
end;

procedure TFConsultaCertidao.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFConsultaCertidao.qryConsultaAfterPost(DataSet: TDataSet);
begin
  qryConsulta.ApplyUpdates(0);
end;

procedure TFConsultaCertidao.qryConsultaCalcFields(DataSet: TDataSet);
begin
  if qryConsultaTIPO_CERTIDAO.AsString='N' then qryConsultaNomeCertidao.AsString:='NORMAL';
  if qryConsultaTIPO_CERTIDAO.AsString='E' then qryConsultaNomeCertidao.AsString:='ESPECIAL';
  if qryConsultaTIPO_CERTIDAO.AsString='C' then qryConsultaNomeCertidao.AsString:='CADASTRO';
  if qryConsultaTIPO_CERTIDAO.AsString='I' then qryConsultaNomeCertidao.AsString:='INTEIRO TEOR';
  if qryConsultaTIPO_CERTIDAO.AsString='R' then qryConsultaNomeCertidao.AsString:='REVALIDA��O (VISTO)';
  if qryConsultaTIPO_CERTIDAO.AsString='X' then qryConsultaNomeCertidao.AsString:='CANCELAMENTO';
  if qryConsultaTIPO_CERTIDAO.AsString='F' then qryConsultaNomeCertidao.AsString:='2� VIA ('+dm.CampoTitulos('STATUS',qryConsultaID_ATO.AsInteger)+')';
  qryConsultaExcedentes.AsInteger:=IfThen(qryConsultaFOLHAS.AsInteger=0,0,qryConsultaFOLHAS.AsInteger-1);
end;

procedure TFConsultaCertidao.ExcluirClick(Sender: TObject);
begin
  if qryConsulta.IsEmpty then Exit;

  if GR.PasswordInputBox('SEGURAN�A','SENHA')=dm.ServentiaCODIGO.AsString then
    if GR.Pergunta('CONFIRMA A EXCLUS�O') then
    begin
        if GR.Pergunta('DESEJA LIBERAR O SELO NO GERENCIAL') then
        Gdm.LiberarSelo('PROTESTO',qryConsultaSELO.AsString,False);
        qryConsulta.Delete;
        qryConsulta.ApplyUpdates(0);
    end;
end;

procedure TFConsultaCertidao.edDataKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
  begin
      qryConsulta.Close;
      qryConsulta.SQL.Text:='SELECT * FROM CERTIDOES WHERE DT_CERTIDAO='+QuotedStr(FormatDateTime('mm/dd/yyyy',edData.Date))+' ORDER BY RECIBO';
      qryConsulta.Open;
  end;
end;

procedure TFConsultaCertidao.edPedidoKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
  begin
      qryConsulta.Close;
      qryConsulta.SQL.Text:='SELECT * FROM CERTIDOES WHERE DT_PEDIDO='+QuotedStr(FormatDateTime('mm/dd/yyyy',edPedido.Date))+' ORDER BY RECIBO';
      qryConsulta.Open;
  end;
end;

procedure TFConsultaCertidao.FormCreate(Sender: TObject);
var
  Key: Char;
begin
  edData.Date:=Now;
  edPedido.Date:=Now;
  edEntrega.Date:=Now;

  Key:=#13;
  edDataKeyPress(Sender,Key);
end;

procedure TFConsultaCertidao.btImprimirClick(Sender: TObject);
begin
  {COLOQUEI VISIBLE FALSE PORQUE N�O TERMINEI A REIMPRESS�O DOS OUTROS TIPOS DE CERTID�ES}
  {OS ABAIXO EST�O FUNCIONANDO}
  if (qryConsultaNomeCertidao.AsString='2� VIA (CANCELADO)') or (qryConsultaNomeCertidao.AsString='2� VIA (PAGO)') then
  begin
      dm.Escreventes.Locate('CPF',qryConsultaESCREVENTE.AsString,[]);
      dm.vAssinatura:=dm.EscreventesNOME.AsString+' - '+dm.EscreventesQUALIFICACAO.AsString;
      if dm.EscreventesMATRICULA.AsString<>'' then
        dm.vMatricula:='Matr�cula: '+dm.EscreventesMATRICULA.AsString
          else dm.vMatricula:='';

      dm.Certidoes.Close;
      dm.Certidoes.Params[0].AsInteger:=qryConsultaID_CERTIDAO.AsInteger;
      dm.Certidoes.Open;

      dm.Titulos.Close;
      dm.Titulos.Params[0].AsInteger:=dm.CertidoesID_ATO.AsInteger;
      dm.Titulos.Open;

      if qryConsultaNomeCertidao.AsString='2� VIA (CANCELADO)' then
      begin
          Application.CreateForm(TFQuickCertidaoCancelamento1,FQuickCertidaoCancelamento1);
          FQuickCertidaoCancelamento1.Free;
      end
      else
      begin
          dm.v2Via:=GR.iif(dm.ServentiaCODIGO.AsInteger=1726,False,True);
          Application.CreateForm(TFQuickPagamento1,FQuickPagamento1);
          with FQuickPagamento1 do
          begin
              qryImprime.Close;
              qryImprime.SQL.Text:='SELECT * FROM TITULOS WHERE ID_ATO IN ('+dm.CertidoesID_ATO.AsString+') ORDER BY PROTOCOLO';
              qryImprime.Open;

              qrPagamento.Enabled :=False;
              lbProtocolo2.Enabled:=False;
              lbPortador2.Enabled :=False;
              qrDeclaro.Enabled   :=False;
              lbDocumento2.Enabled:=False;
              lbEspecie2.Enabled  :=False;
              lbNumero2.Enabled   :=False;
              lbValor.Enabled     :=False;
              lbDevedor2.Enabled  :=False;
              lbSacador2.Enabled  :=False;
              lbCedente2.Enabled  :=False;
              lbForma2.Enabled    :=False;
              lbSelo2.Enabled     :=False;
              lbData2.Enabled     :=False;
              QRShape3.Enabled    :=False;
              lbLinha.Enabled     :=False;

              Certidao.Preview;
              Free;
          end;
          dm.v2Via:=False;
      end;
  end;
end;

procedure TFConsultaCertidao.edEntregaKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
  begin
      qryConsulta.Close;
      qryConsulta.SQL.Text:='SELECT * FROM CERTIDOES WHERE DT_ENTREGA='+QuotedStr(FormatDateTime('mm/dd/yyyy',edEntrega.Date))+' ORDER BY RECIBO';
      qryConsulta.Open;
  end;
end;

end.
