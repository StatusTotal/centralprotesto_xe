object FExportarTxtRemBalcaoServ: TFExportarTxtRemBalcaoServ
  Left = 185
  Top = 115
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'ARQUIVOS DE REMESSA - T'#205'TULOS BALC'#195'O'
  ClientHeight = 523
  ClientWidth = 1041
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = PortadorAux
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 62
    Width = 1041
    Height = 442
    Align = alClient
    AutoSize = True
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Caption = 'Panel1'
    TabOrder = 1
    object dbgTitulos: TwwDBGrid
      Left = 2
      Top = 2
      Width = 1037
      Height = 438
      ControlType.Strings = (
        'Check;CheckBox;S;N'
        'SELECIONADO;CheckBox;True;False')
      Selected.Strings = (
        'SELECIONADO'#9'3'#9'*'#9'F'
        'CODIGO'#9'8'#9'C'#243'digo'#9'T'
        'APRESENTANTE'#9'34'#9'Portador'#9'T'
        'Protocolo'#9'13'#9'Protocolo'#9'T'
        'NUMERO_TITULO'#9'18'#9'N'#186' T'#237'tulo'#9'T'
        'STATUS'#9'18'#9'Situa'#231#227'o'#9'T'
        'AGENCIA_CEDENTE'#9'27'#9'Ag'#234'ncia Cedente'#9'T'
        'NOSSO_NUMERO'#9'18'#9'Nosso N'#250'mero'#9'T'
        'ARQUIVO_CARTORIO'#9'19'#9'Arquivo Remessa'#9'T')
      IniAttributes.Delimiter = ';;'
      IniAttributes.UnicodeIniFile = False
      TitleColor = clBtnFace
      FixedCols = 1
      ShowHorzScrollBar = True
      EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
      Align = alClient
      BorderStyle = bsNone
      DataSource = dsTitulos
      EditCalculated = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = []
      Options = [dgEditing, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgFixedProportionalResize]
      ParentFont = False
      PopupMenu = PM
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      TitleLines = 1
      TitleButtons = False
      UseTFields = False
      OnExit = dbgTitulosExit
      OnAddSelectColumn = dbgTitulosAddSelectColumn
      OnMouseMove = dbgTitulosMouseMove
      OnMouseUp = dbgTitulosMouseUp
      ExplicitLeft = 4
      ExplicitTop = 0
    end
  end
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 1041
    Height = 62
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'SCROLLSLIDERH'
    object RgTipo: TsRadioGroup
      Left = 2
      Top = -62
      Width = 140
      Height = 56
      Caption = '     Tipo de Arquivo      '
      ParentBackground = False
      TabOrder = 3
      Visible = False
      CaptionLayout = clTopCenter
      SkinData.SkinSection = 'HINT'
      CaptionSkin = 'HINT'
      ItemIndex = 0
      Items.Strings = (
        'Banco'
        'Distribuidor')
    end
    object btnVisualizar: TsBitBtn
      Left = 138
      Top = 8
      Width = 90
      Height = 48
      Cursor = crHandPoint
      Caption = 'Visualizar'
      Layout = blGlyphTop
      TabOrder = 1
      OnClick = btnVisualizarClick
      ImageIndex = 21
      Images = Gdm.Im16
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object btnExportar: TsBitBtn
      Left = 868
      Top = 8
      Width = 160
      Height = 48
      Cursor = crHandPoint
      Caption = 'Exportar para Serventias'
      Layout = blGlyphTop
      TabOrder = 2
      OnClick = btnExportarClick
      ImageIndex = 0
      Images = Gdm.Im16
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object dteDataEnt: TsDateEdit
      Left = 41
      Top = 34
      Width = 91
      Height = 22
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 0
      OnKeyDown = dteDataEntKeyDown
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Blend = 0
      DefaultToday = True
    end
  end
  object stbBarra: TsStatusBar
    Left = 0
    Top = 504
    Width = 1041
    Height = 19
    Panels = <
      item
        Text = 'Nenhum T'#237'tulo pequisado'
        Width = 100
      end>
    OnMouseMove = stbBarraMouseMove
    SkinData.SkinSection = 'STATUSBAR'
  end
  object dsTitulos: TDataSource
    DataSet = cdsTitulos
    Left = 952
    Top = 448
  end
  object cdsTitulos: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftDate
        Name = 'DATA_ENT'
        ParamType = ptInput
      end>
    ProviderName = 'dspTitulos'
    Left = 952
    Top = 400
    object cdsTitulosID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Required = True
    end
    object cdsTitulosCODIGO: TIntegerField
      Alignment = taCenter
      FieldName = 'CODIGO'
    end
    object cdsTitulosRECIBO: TIntegerField
      FieldName = 'RECIBO'
    end
    object cdsTitulosDT_ENTRADA: TDateField
      FieldName = 'DT_ENTRADA'
    end
    object cdsTitulosDT_PROTOCOLO: TDateField
      FieldName = 'DT_PROTOCOLO'
    end
    object cdsTitulosPROTOCOLO: TIntegerField
      Alignment = taCenter
      FieldName = 'PROTOCOLO'
    end
    object cdsTitulosLIVRO_PROTOCOLO: TIntegerField
      FieldName = 'LIVRO_PROTOCOLO'
    end
    object cdsTitulosFOLHA_PROTOCOLO: TStringField
      FieldName = 'FOLHA_PROTOCOLO'
      Size = 10
    end
    object cdsTitulosDT_PRAZO: TDateField
      FieldName = 'DT_PRAZO'
    end
    object cdsTitulosDT_REGISTRO: TDateField
      FieldName = 'DT_REGISTRO'
    end
    object cdsTitulosREGISTRO: TIntegerField
      FieldName = 'REGISTRO'
    end
    object cdsTitulosLIVRO_REGISTRO: TIntegerField
      FieldName = 'LIVRO_REGISTRO'
    end
    object cdsTitulosFOLHA_REGISTRO: TStringField
      FieldName = 'FOLHA_REGISTRO'
      Size = 10
    end
    object cdsTitulosSELO_REGISTRO: TStringField
      FieldName = 'SELO_REGISTRO'
      Size = 9
    end
    object cdsTitulosDT_PAGAMENTO: TDateField
      FieldName = 'DT_PAGAMENTO'
    end
    object cdsTitulosSELO_PAGAMENTO: TStringField
      FieldName = 'SELO_PAGAMENTO'
      Size = 9
    end
    object cdsTitulosRECIBO_PAGAMENTO: TIntegerField
      FieldName = 'RECIBO_PAGAMENTO'
    end
    object cdsTitulosCOBRANCA: TStringField
      FieldName = 'COBRANCA'
      FixedChar = True
      Size = 2
    end
    object cdsTitulosEMOLUMENTOS: TFloatField
      FieldName = 'EMOLUMENTOS'
    end
    object cdsTitulosFETJ: TFloatField
      FieldName = 'FETJ'
    end
    object cdsTitulosFUNDPERJ: TFloatField
      FieldName = 'FUNDPERJ'
    end
    object cdsTitulosFUNPERJ: TFloatField
      FieldName = 'FUNPERJ'
    end
    object cdsTitulosFUNARPEN: TFloatField
      FieldName = 'FUNARPEN'
    end
    object cdsTitulosPMCMV: TFloatField
      FieldName = 'PMCMV'
    end
    object cdsTitulosISS: TFloatField
      FieldName = 'ISS'
    end
    object cdsTitulosMUTUA: TFloatField
      FieldName = 'MUTUA'
    end
    object cdsTitulosDISTRIBUICAO: TFloatField
      FieldName = 'DISTRIBUICAO'
    end
    object cdsTitulosACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
    end
    object cdsTitulosTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
    object cdsTitulosTIPO_PROTESTO: TIntegerField
      FieldName = 'TIPO_PROTESTO'
    end
    object cdsTitulosTIPO_TITULO: TIntegerField
      FieldName = 'TIPO_TITULO'
    end
    object cdsTitulosNUMERO_TITULO: TStringField
      Alignment = taCenter
      FieldName = 'NUMERO_TITULO'
    end
    object cdsTitulosDT_TITULO: TDateField
      FieldName = 'DT_TITULO'
    end
    object cdsTitulosBANCO: TStringField
      FieldName = 'BANCO'
    end
    object cdsTitulosAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 10
    end
    object cdsTitulosCONTA: TStringField
      FieldName = 'CONTA'
      Size = 10
    end
    object cdsTitulosVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
    end
    object cdsTitulosSALDO_PROTESTO: TFloatField
      FieldName = 'SALDO_PROTESTO'
    end
    object cdsTitulosDT_VENCIMENTO: TDateField
      FieldName = 'DT_VENCIMENTO'
    end
    object cdsTitulosCONVENIO: TStringField
      FieldName = 'CONVENIO'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosTIPO_APRESENTACAO: TStringField
      FieldName = 'TIPO_APRESENTACAO'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosDT_ENVIO: TDateField
      FieldName = 'DT_ENVIO'
    end
    object cdsTitulosCODIGO_APRESENTANTE: TStringField
      FieldName = 'CODIGO_APRESENTANTE'
      Size = 10
    end
    object cdsTitulosTIPO_INTIMACAO: TStringField
      FieldName = 'TIPO_INTIMACAO'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosMOTIVO_INTIMACAO: TMemoField
      FieldName = 'MOTIVO_INTIMACAO'
      BlobType = ftMemo
      Size = 1
    end
    object cdsTitulosDT_INTIMACAO: TDateField
      FieldName = 'DT_INTIMACAO'
    end
    object cdsTitulosDT_PUBLICACAO: TDateField
      FieldName = 'DT_PUBLICACAO'
    end
    object cdsTitulosVALOR_PAGAMENTO: TFloatField
      FieldName = 'VALOR_PAGAMENTO'
    end
    object cdsTitulosAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Size = 100
    end
    object cdsTitulosCPF_CNPJ_APRESENTANTE: TStringField
      FieldName = 'CPF_CNPJ_APRESENTANTE'
      Size = 15
    end
    object cdsTitulosTIPO_APRESENTANTE: TStringField
      FieldName = 'TIPO_APRESENTANTE'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosCEDENTE: TStringField
      FieldName = 'CEDENTE'
      Size = 100
    end
    object cdsTitulosNOSSO_NUMERO: TStringField
      Alignment = taCenter
      FieldName = 'NOSSO_NUMERO'
      Size = 15
    end
    object cdsTitulosSACADOR: TStringField
      FieldName = 'SACADOR'
      Size = 100
    end
    object cdsTitulosDEVEDOR: TStringField
      FieldName = 'DEVEDOR'
      Size = 100
    end
    object cdsTitulosCPF_CNPJ_DEVEDOR: TStringField
      FieldName = 'CPF_CNPJ_DEVEDOR'
      Size = 15
    end
    object cdsTitulosTIPO_DEVEDOR: TStringField
      FieldName = 'TIPO_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosAGENCIA_CEDENTE: TStringField
      Alignment = taCenter
      FieldName = 'AGENCIA_CEDENTE'
      Size = 15
    end
    object cdsTitulosPRACA_PROTESTO: TStringField
      FieldName = 'PRACA_PROTESTO'
    end
    object cdsTitulosTIPO_ENDOSSO: TStringField
      FieldName = 'TIPO_ENDOSSO'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosACEITE: TStringField
      FieldName = 'ACEITE'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosCPF_ESCREVENTE: TStringField
      FieldName = 'CPF_ESCREVENTE'
      Size = 11
    end
    object cdsTitulosCPF_ESCREVENTE_PG: TStringField
      FieldName = 'CPF_ESCREVENTE_PG'
      Size = 11
    end
    object cdsTitulosOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      BlobType = ftMemo
      Size = 1
    end
    object cdsTitulosDT_SUSTADO: TDateField
      FieldName = 'DT_SUSTADO'
    end
    object cdsTitulosDT_RETIRADO: TDateField
      FieldName = 'DT_RETIRADO'
    end
    object cdsTitulosSTATUS: TStringField
      Alignment = taCenter
      FieldName = 'STATUS'
    end
    object cdsTitulosPROTESTADO: TStringField
      FieldName = 'PROTESTADO'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosENVIADO_APONTAMENTO: TStringField
      FieldName = 'ENVIADO_APONTAMENTO'
      OnGetText = cdsTitulosENVIADO_APONTAMENTOGetText
      OnSetText = cdsTitulosENVIADO_APONTAMENTOSetText
      FixedChar = True
      Size = 1
    end
    object cdsTitulosENVIADO_PROTESTO: TStringField
      FieldName = 'ENVIADO_PROTESTO'
      OnGetText = cdsTitulosENVIADO_PROTESTOGetText
      OnSetText = cdsTitulosENVIADO_PROTESTOSetText
      FixedChar = True
      Size = 1
    end
    object cdsTitulosENVIADO_PAGAMENTO: TStringField
      FieldName = 'ENVIADO_PAGAMENTO'
      OnGetText = cdsTitulosENVIADO_PAGAMENTOGetText
      OnSetText = cdsTitulosENVIADO_PAGAMENTOSetText
      FixedChar = True
      Size = 1
    end
    object cdsTitulosENVIADO_RETIRADO: TStringField
      FieldName = 'ENVIADO_RETIRADO'
      OnGetText = cdsTitulosENVIADO_RETIRADOGetText
      OnSetText = cdsTitulosENVIADO_RETIRADOSetText
      FixedChar = True
      Size = 1
    end
    object cdsTitulosENVIADO_SUSTADO: TStringField
      FieldName = 'ENVIADO_SUSTADO'
      OnGetText = cdsTitulosENVIADO_SUSTADOGetText
      OnSetText = cdsTitulosENVIADO_SUSTADOSetText
      FixedChar = True
      Size = 1
    end
    object cdsTitulosENVIADO_DEVOLVIDO: TStringField
      FieldName = 'ENVIADO_DEVOLVIDO'
      OnGetText = cdsTitulosENVIADO_DEVOLVIDOGetText
      OnSetText = cdsTitulosENVIADO_DEVOLVIDOSetText
      FixedChar = True
      Size = 1
    end
    object cdsTitulosEXPORTADO: TStringField
      FieldName = 'EXPORTADO'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosAVALISTA_DEVEDOR: TStringField
      FieldName = 'AVALISTA_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosFINS_FALIMENTARES: TStringField
      FieldName = 'FINS_FALIMENTARES'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosTARIFA_BANCARIA: TFloatField
      FieldName = 'TARIFA_BANCARIA'
    end
    object cdsTitulosFORMA_PAGAMENTO: TStringField
      FieldName = 'FORMA_PAGAMENTO'
      FixedChar = True
      Size = 2
    end
    object cdsTitulosNUMERO_PAGAMENTO: TStringField
      FieldName = 'NUMERO_PAGAMENTO'
      Size = 40
    end
    object cdsTitulosARQUIVO: TStringField
      FieldName = 'ARQUIVO'
    end
    object cdsTitulosRETORNO: TStringField
      FieldName = 'RETORNO'
    end
    object cdsTitulosDT_DEVOLVIDO: TDateField
      FieldName = 'DT_DEVOLVIDO'
    end
    object cdsTitulosCPF_CNPJ_SACADOR: TStringField
      FieldName = 'CPF_CNPJ_SACADOR'
      Size = 14
    end
    object cdsTitulosID_MSG: TIntegerField
      FieldName = 'ID_MSG'
    end
    object cdsTitulosVALOR_AR: TFloatField
      FieldName = 'VALOR_AR'
    end
    object cdsTitulosELETRONICO: TStringField
      FieldName = 'ELETRONICO'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosIRREGULARIDADE: TIntegerField
      FieldName = 'IRREGULARIDADE'
    end
    object cdsTitulosAVISTA: TStringField
      FieldName = 'AVISTA'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosSALDO_TITULO: TFloatField
      FieldName = 'SALDO_TITULO'
    end
    object cdsTitulosTIPO_SUSTACAO: TStringField
      FieldName = 'TIPO_SUSTACAO'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosRETORNO_PROTESTO: TStringField
      FieldName = 'RETORNO_PROTESTO'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosDT_RETORNO_PROTESTO: TDateField
      FieldName = 'DT_RETORNO_PROTESTO'
    end
    object cdsTitulosDT_DEFINITIVA: TDateField
      FieldName = 'DT_DEFINITIVA'
    end
    object cdsTitulosJUDICIAL: TStringField
      FieldName = 'JUDICIAL'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosALEATORIO_PROTESTO: TStringField
      FieldName = 'ALEATORIO_PROTESTO'
      Size = 3
    end
    object cdsTitulosALEATORIO_SOLUCAO: TStringField
      FieldName = 'ALEATORIO_SOLUCAO'
      Size = 3
    end
    object cdsTitulosCCT: TStringField
      FieldName = 'CCT'
      Size = 9
    end
    object cdsTitulosDETERMINACAO: TStringField
      FieldName = 'DETERMINACAO'
      Size = 100
    end
    object cdsTitulosANTIGO: TStringField
      FieldName = 'ANTIGO'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosLETRA: TStringField
      FieldName = 'LETRA'
      Size = 1
    end
    object cdsTitulosPROTOCOLO_CARTORIO: TIntegerField
      FieldName = 'PROTOCOLO_CARTORIO'
    end
    object cdsTitulosARQUIVO_CARTORIO: TStringField
      Alignment = taCenter
      FieldName = 'ARQUIVO_CARTORIO'
    end
    object cdsTitulosRETORNO_CARTORIO: TStringField
      FieldName = 'RETORNO_CARTORIO'
    end
    object cdsTitulosDT_DEVOLVIDO_CARTORIO: TDateField
      FieldName = 'DT_DEVOLVIDO_CARTORIO'
    end
    object cdsTitulosSELECIONADO: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'SELECIONADO'
    end
    object cdsTitulosCOD_IRREGULARIDADE: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'COD_IRREGULARIDADE'
    end
    object cdsTitulosSERVENTIA_AGREG: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'SERVENTIA_AGREG'
      Size = 2
    end
    object cdsTitulosSEQUENCIAL_ARQ: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'SEQUENCIAL_ARQ'
    end
    object cdsTitulosSEQ_BANCO: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'SEQ_BANCO'
    end
    object cdsTitulosCARTORIO: TIntegerField
      FieldName = 'CARTORIO'
    end
    object cdsTitulosDT_PROTOCOLO_CARTORIO: TDateField
      FieldName = 'DT_PROTOCOLO_CARTORIO'
    end
    object cdsTitulosDIAS_AVISTA: TIntegerField
      FieldName = 'DIAS_AVISTA'
    end
    object cdsTitulosFLG_SALDO: TStringField
      FieldName = 'FLG_SALDO'
      FixedChar = True
      Size = 1
    end
    object cdsTitulosID_ATO_1: TIntegerField
      FieldName = 'ID_ATO_1'
      ReadOnly = True
    end
    object cdsTitulosID_DEVEDOR: TIntegerField
      FieldName = 'ID_DEVEDOR'
      ReadOnly = True
    end
    object cdsTitulosORDEM: TIntegerField
      FieldName = 'ORDEM'
      ReadOnly = True
    end
    object cdsTitulosNOME: TStringField
      FieldName = 'NOME'
      ReadOnly = True
      Size = 100
    end
    object cdsTitulosTIPO: TStringField
      FieldName = 'TIPO'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object cdsTitulosDOCUMENTO: TStringField
      FieldName = 'DOCUMENTO'
      ReadOnly = True
      Size = 15
    end
    object cdsTitulosIFP_DETRAN: TStringField
      FieldName = 'IFP_DETRAN'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object cdsTitulosIDENTIDADE: TStringField
      FieldName = 'IDENTIDADE'
      ReadOnly = True
      Size = 25
    end
    object cdsTitulosDT_EMISSAO: TDateField
      FieldName = 'DT_EMISSAO'
      ReadOnly = True
    end
    object cdsTitulosORGAO: TStringField
      FieldName = 'ORGAO'
      ReadOnly = True
      Size = 70
    end
    object cdsTitulosCEP: TStringField
      FieldName = 'CEP'
      ReadOnly = True
      Size = 8
    end
    object cdsTitulosENDERECO: TStringField
      FieldName = 'ENDERECO'
      ReadOnly = True
      Size = 100
    end
    object cdsTitulosBAIRRO: TStringField
      FieldName = 'BAIRRO'
      ReadOnly = True
      Size = 100
    end
    object cdsTitulosMUNICIPIO: TStringField
      FieldName = 'MUNICIPIO'
      ReadOnly = True
      Size = 100
    end
    object cdsTitulosUF: TStringField
      FieldName = 'UF'
      ReadOnly = True
      FixedChar = True
      Size = 2
    end
    object cdsTitulosIGNORADO: TStringField
      FieldName = 'IGNORADO'
      ReadOnly = True
      FixedChar = True
      Size = 1
    end
    object cdsTitulosJUSTIFICATIVA: TMemoField
      FieldName = 'JUSTIFICATIVA'
      ReadOnly = True
      BlobType = ftMemo
    end
    object cdsTitulosTELEFONE: TStringField
      FieldName = 'TELEFONE'
      ReadOnly = True
      Size = 10
    end
    object cdsTitulosPOSTECIPADO: TStringField
      FieldName = 'POSTECIPADO'
      FixedChar = True
      Size = 1
    end
  end
  object dspTitulos: TDataSetProvider
    DataSet = qryTitulos
    Left = 952
    Top = 352
  end
  object cdsDuplicado: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'NUMERO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'CODIGO'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'VALOR'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'DATA'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'VENCIMENTO'
        ParamType = ptInput
      end>
    ProviderName = 'dspDuplicado'
    Left = 880
    Top = 398
  end
  object dsDuplicado: TDataSource
    DataSet = cdsDuplicado
    Left = 880
    Top = 448
  end
  object dspDuplicado: TDataSetProvider
    DataSet = qryDuplicado
    Left = 880
    Top = 350
  end
  object PM: TPopupMenu
    Left = 48
    Top = 104
    object MarcarTodos: TMenuItem
      Caption = 'Marcar todos'
      OnClick = MarcarTodosClick
    end
    object DesmarcarTodos: TMenuItem
      Caption = 'Desmarcar todos'
      OnClick = DesmarcarTodosClick
    end
  end
  object qryDuplicado: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT ID_ATO'
      '  FROM TITULOS'
      ' WHERE NUMERO_TITULO       = :NUMERO'
      '   AND CODIGO_APRESENTANTE = :CODIGO'
      '   AND VALOR_TITULO        = :VALOR'
      '   AND DT_TITULO           = :DATA'
      '   AND DT_VENCIMENTO       = :VENCIMENTO'
      'ORDER BY ID_ATO')
    Left = 880
    Top = 302
    ParamData = <
      item
        Name = 'NUMERO'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CODIGO'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'VALOR'
        DataType = ftFloat
        ParamType = ptInput
      end
      item
        Name = 'DATA'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'VENCIMENTO'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
  object qryTitulos: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT T.*,'
      '       D.*'
      '  FROM TITULOS T'
      ' INNER JOIN DEVEDORES D'
      '    ON T.ID_ATO = D.ID_ATO'
      ' WHERE T.TIPO_APRESENTACAO = '#39'B'#39
      '   AND T.STATUS = '#39'APONTADO'#39
      '   AND T.ARQUIVO_CARTORIO IS NULL'
      '   AND T.RETORNO_CARTORIO IS NULL'
      '   AND T.DT_ENTRADA = :DATA_ENT')
    Left = 952
    Top = 302
    ParamData = <
      item
        Position = 1
        Name = 'DATA_ENT'
        DataType = ftDate
        ParamType = ptInput
      end>
  end
end
