unit UQuickReciboSerasaBV;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls;

type
  TFQuickSerasaBV = class(TForm)
    Recibo: TQuickRep;
    Titulo: TQRBand;
    txNomeServentia: TQRDBText;
    txEndereco: TQRDBText;
    lbTitular1: TQRLabel;
    lbEmpresa1: TQRLabel;
    lbRecibo1: TQRLabel;
    QRDBText31: TQRDBText;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    lbEmolumentos1: TQRLabel;
    QRLabel6: TQRLabel;
    lbFetj1: TQRLabel;
    QRLabel8: TQRLabel;
    lbFundperj1: TQRLabel;
    QRLabel10: TQRLabel;
    lbFunperj1: TQRLabel;
    QRLabel14: TQRLabel;
    lbTotal1: TQRLabel;
    QRShape1: TQRShape;
    lbCidade1: TQRLabel;
    QRShape2: TQRShape;
    lbNome1: TQRLabel;
    lbQualificacao1: TQRLabel;
    lbDataEmissao1: TQRLabel;
    lbQtd1: TQRLabel;
    QRLabel1: TQRLabel;
    lbFunarpen1: TQRLabel;
    QRLabel5: TQRLabel;
    lbPMCMV1: TQRLabel;
    ChildBand1: TQRChildBand;
    Segundo: TQRChildBand;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    lbTitular2: TQRLabel;
    lbEmpresa2: TQRLabel;
    lbRecibo2: TQRLabel;
    QRDBText3: TQRDBText;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel15: TQRLabel;
    lbEmolumentos2: TQRLabel;
    QRLabel17: TQRLabel;
    lbFetj2: TQRLabel;
    QRLabel19: TQRLabel;
    lbFundperj2: TQRLabel;
    QRLabel21: TQRLabel;
    lbFunperj2: TQRLabel;
    QRLabel23: TQRLabel;
    lbCapa2: TQRLabel;
    QRLabel25: TQRLabel;
    lbTotal2: TQRLabel;
    QRShape3: TQRShape;
    lbCidade2: TQRLabel;
    QRShape4: TQRShape;
    lbNome2: TQRLabel;
    lbQualificacao2: TQRLabel;
    lbDataEmissao2: TQRLabel;
    lbQtd2: TQRLabel;
    QRLabel32: TQRLabel;
    lbFunarpen2: TQRLabel;
    QRLabel34: TQRLabel;
    lbPMCMV2: TQRLabel;
    lbIss: TQRLabel;
    lbISS1: TQRLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickSerasaBV: TFQuickSerasaBV;

implementation

uses UDM, DB, UGDM;

{$R *.dfm}

procedure TFQuickSerasaBV.FormCreate(Sender: TObject);
begin
   lbIss.Caption :='ISS('+FloatToStr(Gdm.vAliquotaISS*100)+'%):';
end;

end.
