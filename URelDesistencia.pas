unit URelDesistencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FMTBcd, StdCtrls, Buttons, sBitBtn, sEdit, sGroupBox, ExtCtrls,
  sPanel, DB, SqlExpr, DBClient, SimpleDS;

type
  TFRelDesistencia = class(TForm)
    P1: TsPanel;
    sGroupBox1: TsGroupBox;
    edProtocolo: TsEdit;
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    procedure btVisualizarClick(Sender: TObject);
    procedure edProtocoloKeyPress(Sender: TObject; var Key: Char);
    procedure btSairClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelDesistencia: TFRelDesistencia;

implementation

uses UQuickPagamento1, UDM, UPF, UQuickCancelado2, UQuickRetirado1, UGeral,
  QRCtrls;

{$R *.dfm}

procedure TFRelDesistencia.btVisualizarClick(Sender: TObject);
begin
  if edProtocolo.Text='' then
  begin
      edProtocolo.SetFocus;
      Exit;
  end;

  Application.CreateForm(TFQuickRetirado1,FQuickRetirado1);
  with FQuickRetirado1 do
  begin
      qryProtocolo.Close;
      qryProtocolo.Params[0].AsInteger:=StrToInt(edProtocolo.Text);
      qryProtocolo.Open;

      if qryProtocolo.IsEmpty then
      begin
          GR.Aviso('PROTOCOLO N�O ENCONTRADO!');
          edProtocolo.SetFocus;
          Exit;
          FQuickRetirado1.Free;
      end;

      if qryProtocoloRECIBO_PAGAMENTO.AsInteger=0 then
        if GR.Pergunta('GERAR N� DE RECIBO') then
        begin
            qryProtocolo.Edit;
            if StrToInt(dm.ValorParametro(14))<>0 then
            begin
                qryProtocoloRECIBO_PAGAMENTO.AsInteger:=StrToInt(dm.ValorParametro(14));
                dm.AtualizarParametro(14,IntToStr(qryProtocoloRECIBO_PAGAMENTO.AsInteger+1));
            end
            else
            begin
                qryProtocoloRECIBO_PAGAMENTO.AsInteger:=StrToInt(dm.ValorParametro(11));
                dm.AtualizarParametro(11,IntToStr(qryProtocoloRECIBO_PAGAMENTO.AsInteger+1));
            end;
            qryProtocolo.Post;
            qryProtocolo.ApplyUpdates(0);
        end;

      RE.Lines.Add('Declaramos, que o documento de d�vida, com as caracter�sticas abaixo discriminadas, foi DESISTIDO, de acordo com o que '+
                   'preceitua (Lei 9492/97, CAP. VII, Art. 16), conforme requerido.');

      if dm.ServentiaCODIGO.AsInteger=1430 then
        RE.Text:=PF.JstParagrafo(RE.Text,78)
          else PF.JustificarRichEdit(RE,True);

      PF.CarregarCustas(qryProtocoloID_ATO.AsInteger);
      dm.RxCustas.First;
      while not dm.RxCustas.Eof do
      begin
          M.Lines.Add(dm.RxCustasTABELA.AsString+'.'+dm.RxCustasITEM.AsString+'.'+dm.RxCustasSUBITEM.AsString+'   '+dm.RxCustasDESCRICAO.AsString+' x '+dm.RxCustasQTD.AsString+Format('%-1s %7.2f',[' =',dm.RxCustasTOTAL.AsFloat]));
          dm.RxCustas.Next;
      end;
      M.Lines.Add(Format('%-1s %7.2f',['Emolumentos =',qryProtocoloEMOLUMENTOS.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['FETJ =',qryProtocoloFETJ.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Fundperj =',qryProtocoloFUNDPERJ.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Funperj =',qryProtocoloFUNPERJ.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Funarpen =',qryProtocoloFUNARPEN.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Pmcmv =',qryProtocoloPMCMV.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Iss =',qryProtocoloISS.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['M�tua =',qryProtocoloMUTUA.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Acoterj =',qryProtocoloACOTERJ.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Total =',qryProtocoloTOTAL.AsFloat]));

      if qryProtocoloRECIBO_PAGAMENTO.AsInteger<>0 then
      begin
          qrRecibo.Enabled  :=True;
          lbRecibo.Enabled  :=True;
          qrRecibo2.Enabled :=True;
          lbRecibo2.Enabled :=True;
          lbRecibo.Caption  :=qryProtocoloRECIBO_PAGAMENTO.AsString;
          lbRecibo2.Caption :=lbRecibo.Caption;
      end
      else
      begin
          qrRecibo.Enabled  :=False;
          lbRecibo.Enabled  :=False;
          qrRecibo2.Enabled :=False;
          lbRecibo2.Enabled :=False;
      end;

      lbSeloEletronico.Caption:=GR.PegarLetra(qryProtocoloSELO_PAGAMENTO.AsString)+' '+
                                GR.Zeros(GR.PegarNumeroTexto(qryProtocoloSELO_PAGAMENTO.AsString),'D',5)+' '+
                                qryProtocoloALEATORIO_SOLUCAO.AsString;

      lbProtocolo.Caption   :=qryProtocoloPROTOCOLO.AsString;
      lbDevedor.Caption     :=qryProtocoloDEVEDOR.AsString;
      lbDocumento.Caption   :=GR.FormatarCPFCNPJ(qryProtocoloCPF_CNPJ_DEVEDOR.AsString,'S');
      lbTitulo.Caption      :=qryProtocoloNUMERO_TITULO.AsString;
      lbSacador.Caption     :=qryProtocoloSACADOR.AsString;
      lbCedente.Caption     :=qryProtocoloCEDENTE.AsString;
      lbMotivo.Caption      :=PF.RetornarIrregularidade(qryProtocoloIRREGULARIDADE.AsInteger);
      lbValor.Caption       :='R$ '+FloatToStrF(qryProtocoloVALOR_TITULO.AsFloat,ffNumber,7,2);
      lbVencimento.Caption  :=PF.FormatarData(qryProtocoloDT_VENCIMENTO.AsDateTime,'N');
      lbApresentante.Caption:=qryProtocoloAPRESENTANTE.AsString;
      lbPortador.Caption    :='Portador: '+qryProtocoloAPRESENTANTE.AsString;
      lbProtocolo2.Caption  :=lbProtocolo.Caption;
      lbDevedor2.Caption    :=lbDevedor.Caption;
      lbDocumento2.Caption  :=lbDocumento.Caption;
      lbTitulo2.Caption     :=lbTitulo.Caption;
      lbValor2.Caption      :=lbValor.Caption;
      lbSacador2.Caption    :=lbSacador.Caption;
      lbCedente2.Caption    :=lbCedente.Caption;
      lbData2.Caption       :=lbData.Caption;
      lbSelo.Caption        :=lbSeloEletronico.Caption;

      Desistencia.Preview;

      qryProtocolo.Close;
  end;
end;

procedure TFRelDesistencia.edProtocoloKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in ['0'..'9']) then key:=#0;
end;

procedure TFRelDesistencia.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFRelDesistencia.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

end.
