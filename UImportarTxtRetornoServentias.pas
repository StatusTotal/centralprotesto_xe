unit UImportarTxtRetornoServentias;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, sDBNavigator, ExtCtrls, sPanel,
  ComCtrls, sTreeView, acShellCtrls, {JvExStdCtrls, JvListBox, JvDriveCtrls, }
  FileCtrl, sListView, DB, RxMemDS, Grids, Wwdbigrd, Wwdbgrid, FMTBcd,
  DBClient, Provider, SqlExpr, StrUtils, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.Menus, sListBox;

type
  TFImportarTxtRetornoServentias = class(TForm)
    pnlPrincipal: TsPanel;
    btnProcessar: TsBitBtn;
    pnlContador: TsPanel;
    lblArquivosSelecionados: TLabel;
    //flbArquivosRetornoSAgreg: TJvFileListBox;
    lblTitulos: TLabel;
    lblArquivosRetornoSAgreg: TLabel;
    dsDetail: TDataSource;
    G1: TwwDBGrid;
    mdDetail: TRxMemoryData;
    dspTitulo: TDataSetProvider;
    cdsTitulo: TClientDataSet;
    dsTitulo: TDataSource;
    mdDetailCOD_CEDENTE: TStringField;
    mdDetailNOME_CEDENTE: TStringField;
    mdDetailNOME_SACADOR: TStringField;
    mdDetailDOC_SACADOR: TStringField;
    mdDetailEND_SACADOR: TStringField;
    mdDetailCEP_SACADOR: TIntegerField;
    mdDetailCID_SACADOR: TStringField;
    mdDetailUF_SACADOR: TStringField;
    mdDetailNOSSO_NUMERO: TStringField;
    mdDetailESPECIE_TITULO: TStringField;
    mdDetailNUM_TITULO: TStringField;
    mdDetailDATA_EMISSAO: TDateField;
    mdDetailDATA_VENCIMENTO: TDateField;
    mdDetailTIPO_MOEDA: TIntegerField;
    mdDetailVALOR_TITULO: TFloatField;
    mdDetailSALDO_TITULO: TFloatField;
    mdDetailPRACA_PROTESTO: TStringField;
    mdDetailTIPO_ENDOSSO: TStringField;
    mdDetailINFO_ACEITE: TStringField;
    mdDetailNUM_CONTROLE: TIntegerField;
    mdDetailNOME_DEVEDOR: TStringField;
    mdDetailTIPO_ID_DEVEDOR: TIntegerField;
    mdDetailDOC_DEVEDOR: TStringField;
    mdDetailEND_DEVEDOR: TStringField;
    mdDetailCEP_DEVEDOR: TIntegerField;
    mdDetailCID_DEVEDOR: TStringField;
    mdDetailUF_DEVEDOR: TStringField;
    mdDetailCOD_CARTORIO: TIntegerField;
    mdDetailPROTOCOLO_CARTORIO: TStringField;
    mdDetailTIPO_OCORRENCIA: TStringField;
    mdDetailDATA_PROTOCOLO: TDateField;
    mdDetailVALOR_CUSTAS_CART: TFloatField;
    mdDetailDECLARACAO_PORT: TStringField;
    mdDetailDATA_OCORRENCIA: TDateField;
    mdDetailCOD_IRREGULARIDADE: TStringField;
    mdDetailBAIRRO_DEVEDOR: TStringField;
    mdDetailVALOR_CUSTAS_DIST: TFloatField;
    mdDetailREGISTRO_DIST: TIntegerField;
    mdDetailVALOR_GRAVACAO: TFloatField;
    mdDetailNUM_OPERACAO: TIntegerField;
    mdDetailNUM_PARCELA: TIntegerField;
    mdDetailTIPO_LETRA: TIntegerField;
    mdDetailCOMPL_COD_IRREG: TStringField;
    mdDetailPROTESTO_FALENCIA: TStringField;
    mdDetailINSTRUMENTO_PROT: TStringField;
    mdDetailVALOR_DEMAIS_DESP: TFloatField;
    mdDetailCOMPL_REGISTRO: TStringField;
    mdDetailSEQ_REGISTRO: TIntegerField;
    mdDetailNOME_PORTADOR: TStringField;
    mdDetailSTATUS: TStringField;
    RE: TRichEdit;
    mdDetailNOME_ARQ_RET: TStringField;
    cdsTituloID_ATO: TIntegerField;
    cdsTituloCODIGO: TIntegerField;
    cdsTituloRECIBO: TIntegerField;
    cdsTituloDT_ENTRADA: TDateField;
    cdsTituloDT_PROTOCOLO: TDateField;
    cdsTituloPROTOCOLO: TIntegerField;
    cdsTituloLIVRO_PROTOCOLO: TIntegerField;
    cdsTituloFOLHA_PROTOCOLO: TStringField;
    cdsTituloDT_PRAZO: TDateField;
    cdsTituloDT_REGISTRO: TDateField;
    cdsTituloREGISTRO: TIntegerField;
    cdsTituloLIVRO_REGISTRO: TIntegerField;
    cdsTituloFOLHA_REGISTRO: TStringField;
    cdsTituloSELO_REGISTRO: TStringField;
    cdsTituloDT_PAGAMENTO: TDateField;
    cdsTituloSELO_PAGAMENTO: TStringField;
    cdsTituloRECIBO_PAGAMENTO: TIntegerField;
    cdsTituloCOBRANCA: TStringField;
    cdsTituloEMOLUMENTOS: TFloatField;
    cdsTituloFETJ: TFloatField;
    cdsTituloFUNDPERJ: TFloatField;
    cdsTituloFUNPERJ: TFloatField;
    cdsTituloFUNARPEN: TFloatField;
    cdsTituloPMCMV: TFloatField;
    cdsTituloISS: TFloatField;
    cdsTituloMUTUA: TFloatField;
    cdsTituloDISTRIBUICAO: TFloatField;
    cdsTituloACOTERJ: TFloatField;
    cdsTituloTOTAL: TFloatField;
    cdsTituloTIPO_PROTESTO: TIntegerField;
    cdsTituloTIPO_TITULO: TIntegerField;
    cdsTituloNUMERO_TITULO: TStringField;
    cdsTituloDT_TITULO: TDateField;
    cdsTituloBANCO: TStringField;
    cdsTituloAGENCIA: TStringField;
    cdsTituloCONTA: TStringField;
    cdsTituloVALOR_TITULO: TFloatField;
    cdsTituloSALDO_PROTESTO: TFloatField;
    cdsTituloDT_VENCIMENTO: TDateField;
    cdsTituloCONVENIO: TStringField;
    cdsTituloTIPO_APRESENTACAO: TStringField;
    cdsTituloDT_ENVIO: TDateField;
    cdsTituloCODIGO_APRESENTANTE: TStringField;
    cdsTituloTIPO_INTIMACAO: TStringField;
    cdsTituloMOTIVO_INTIMACAO: TMemoField;
    cdsTituloDT_INTIMACAO: TDateField;
    cdsTituloDT_PUBLICACAO: TDateField;
    cdsTituloVALOR_PAGAMENTO: TFloatField;
    cdsTituloAPRESENTANTE: TStringField;
    cdsTituloCPF_CNPJ_APRESENTANTE: TStringField;
    cdsTituloTIPO_APRESENTANTE: TStringField;
    cdsTituloCEDENTE: TStringField;
    cdsTituloNOSSO_NUMERO: TStringField;
    cdsTituloSACADOR: TStringField;
    cdsTituloDEVEDOR: TStringField;
    cdsTituloCPF_CNPJ_DEVEDOR: TStringField;
    cdsTituloTIPO_DEVEDOR: TStringField;
    cdsTituloAGENCIA_CEDENTE: TStringField;
    cdsTituloPRACA_PROTESTO: TStringField;
    cdsTituloTIPO_ENDOSSO: TStringField;
    cdsTituloACEITE: TStringField;
    cdsTituloCPF_ESCREVENTE: TStringField;
    cdsTituloCPF_ESCREVENTE_PG: TStringField;
    cdsTituloOBSERVACAO: TMemoField;
    cdsTituloDT_SUSTADO: TDateField;
    cdsTituloDT_RETIRADO: TDateField;
    cdsTituloSTATUS: TStringField;
    cdsTituloPROTESTADO: TStringField;
    cdsTituloENVIADO_APONTAMENTO: TStringField;
    cdsTituloENVIADO_PROTESTO: TStringField;
    cdsTituloENVIADO_PAGAMENTO: TStringField;
    cdsTituloENVIADO_RETIRADO: TStringField;
    cdsTituloENVIADO_SUSTADO: TStringField;
    cdsTituloENVIADO_DEVOLVIDO: TStringField;
    cdsTituloEXPORTADO: TStringField;
    cdsTituloAVALISTA_DEVEDOR: TStringField;
    cdsTituloFINS_FALIMENTARES: TStringField;
    cdsTituloTARIFA_BANCARIA: TFloatField;
    cdsTituloFORMA_PAGAMENTO: TStringField;
    cdsTituloNUMERO_PAGAMENTO: TStringField;
    cdsTituloARQUIVO: TStringField;
    cdsTituloRETORNO: TStringField;
    cdsTituloDT_DEVOLVIDO: TDateField;
    cdsTituloCPF_CNPJ_SACADOR: TStringField;
    cdsTituloID_MSG: TIntegerField;
    cdsTituloVALOR_AR: TFloatField;
    cdsTituloELETRONICO: TStringField;
    cdsTituloIRREGULARIDADE: TIntegerField;
    cdsTituloAVISTA: TStringField;
    cdsTituloSALDO_TITULO: TFloatField;
    cdsTituloTIPO_SUSTACAO: TStringField;
    cdsTituloRETORNO_PROTESTO: TStringField;
    cdsTituloDT_RETORNO_PROTESTO: TDateField;
    cdsTituloDT_DEFINITIVA: TDateField;
    cdsTituloJUDICIAL: TStringField;
    cdsTituloALEATORIO_PROTESTO: TStringField;
    cdsTituloALEATORIO_SOLUCAO: TStringField;
    cdsTituloCCT: TStringField;
    cdsTituloDETERMINACAO: TStringField;
    cdsTituloANTIGO: TStringField;
    cdsTituloLETRA: TStringField;
    cdsTituloPROTOCOLO_CARTORIO: TIntegerField;
    cdsTituloARQUIVO_CARTORIO: TStringField;
    cdsTituloRETORNO_CARTORIO: TStringField;
    cdsTituloDT_DEVOLVIDO_CARTORIO: TDateField;
    mdDetailSELO: TStringField;
    mdDetailALEATORIO: TStringField;
    mdDetailELETRONICO: TStringField;
    mdDetailTIPO_SUSTACAO: TStringField;
    mdDetailDATA_RET_PROT: TDateField;
    mdDetailVALOR_PAGAMENTO: TFloatField;
    mdDetailNUM_PAGAMENTO: TStringField;
    mdDetailTARIFA: TFloatField;
    mdDetailVALOR_AR: TFloatField;
    mdDetailDETERMINACAO: TStringField;
    mdDetailDATA_SUSTADO: TDateField;
    mdDetailNUM_ID_DEVEDOR: TStringField;
    mdDetailNUM_CONTRATO: TStringField;
    mdDetailPROTOCOLO_DISTRIBUIDOR: TStringField;
    mdDetailLIVRO: TStringField;
    mdDetailFOLHA: TStringField;
    mdDetailID_MSG: TStringField;
    mdDetailREGISTRO: TStringField;
    mdDetailRECIBO_PAGAMENTO: TStringField;
    mdDetailCPF_ESCREVENTE: TStringField;
    mdDetailDT_PROTOCOLO_CARTORIO: TDateField;
    cdsTituloCARTORIO: TIntegerField;
    cdsTituloDT_PROTOCOLO_CARTORIO: TDateField;
    qryTitulo: TFDQuery;
    mdDetailFLG_REIMPORTAR: TStringField;
    btnCarregar: TsBitBtn;
    mdDetailSelecionado: TBooleanField;
    PM: TPopupMenu;
    MarcarTodos: TMenuItem;
    DesmarcarTodos: TMenuItem;
    mdDetailFLG_EXISTE: TStringField;
    shpLegendaTitJaImportado: TShape;
    lblLegendaTitJaImportado: TLabel;
    lblLegendaTitNaoExistente: TLabel;
    shpLegendaTitNaoExistente: TShape;
    btnLimpar: TsBitBtn;
    mdDetailID_REGISTRO: TStringField;
    mdDetailCOD_PORTADOR: TStringField;
    qryPortadores: TFDQuery;
    dsPortadores: TDataSource;
    qryPortadoresID_PORTADOR: TIntegerField;
    qryPortadoresCODIGO: TStringField;
    qryPortadoresNOME: TStringField;
    qryPortadoresTIPO: TStringField;
    qryPortadoresDOCUMENTO: TStringField;
    qryPortadoresENDERECO: TStringField;
    qryPortadoresBANCO: TStringField;
    qryPortadoresCONVENIO: TStringField;
    qryPortadoresCONTA: TStringField;
    qryPortadoresOBSERVACAO: TMemoField;
    qryPortadoresAGENCIA: TStringField;
    qryPortadoresPRACA: TStringField;
    qryPortadoresCRA: TStringField;
    qryPortadoresSEQUENCIA: TIntegerField;
    qryPortadoresVALOR_DOC: TFloatField;
    qryPortadoresVALOR_TED: TFloatField;
    qryPortadoresNOMINAL: TStringField;
    qryPortadoresFORCA_LEI: TStringField;
    qryPortadoresESPECIE: TStringField;
    flbArquivosRetornoSAgreg: TFileListBox;
    mdDetailPOSTECIPADO: TStringField;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure btnProcessarClick(Sender: TObject);
    procedure btnCarregarClick(Sender: TObject);
    procedure MarcarTodosClick(Sender: TObject);
    procedure DesmarcarTodosClick(Sender: TObject);
    procedure G1Exit(Sender: TObject);
    procedure G1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure G1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure G1DrawDataCell(Sender: TObject; const Rect: TRect; Field: TField;
      State: TGridDrawState);
    procedure btnLimparClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }

    LstArqR: TListBox;

    iTotTitCar,
    iTotTitImp: Integer;

    function Texto(Linha,Inicio,Fim: Integer): String;
  public
    { Public declarations }
  end;

var
  FImportarTxtRetornoServentias: TFImportarTxtRetornoServentias;

implementation

uses UDM, UPF, UGDM, UGeral, UExportarTxt;

{$R *.dfm}

procedure TFImportarTxtRetornoServentias.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Self.Close;
end;

procedure TFImportarTxtRetornoServentias.FormShow(Sender: TObject);
begin
  btnProcessar.Enabled := False;
end;

procedure TFImportarTxtRetornoServentias.G1DrawDataCell(Sender: TObject;
  const Rect: TRect; Field: TField; State: TGridDrawState);
begin
  //Legenda
  if Field.DataSet.FieldValues['FLG_EXISTE'] = 'N' then  //Nao existe no banco de dados
    G1.Canvas.Font.Color := clGray
  else if Field.DataSet.FieldValues['FLG_REIMPORTAR'] = 'S' then  //Titulo ja foi retornado
    G1.Canvas.Font.Color := clMaroon
  else
    G1.Canvas.Font.Color := clWindowText;

  G1.Canvas.FillRect(Rect);
  G1.DefaultDrawDataCell(Rect, Field, State);
end;

procedure TFImportarTxtRetornoServentias.G1Exit(Sender: TObject);
begin
  if mdDetail.State in [dsEdit,dsInsert] then
    mdDetail.Post;
end;

procedure TFImportarTxtRetornoServentias.G1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if X in [0..28] then
    Screen.Cursor := crHandPoint
  else
    Screen.Cursor := crDefault;
end;

procedure TFImportarTxtRetornoServentias.G1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Screen.Cursor = crHandPoint then
  begin
    if mdDetail.IsEmpty then
      Exit;

    mdDetail.Edit;

    if mdDetailFLG_EXISTE.AsString = 'N' then
      mdDetailSelecionado.AsBoolean := False
    else
    begin
      if mdDetailSelecionado.AsBoolean = True then
        mdDetailSelecionado.AsBoolean := False
      else
        mdDetailSelecionado.AsBoolean := True;
    end;

    mdDetail.Post;
  end;
end;

procedure TFImportarTxtRetornoServentias.MarcarTodosClick(Sender: TObject);
var
  Posicao: Integer;
begin
  mdDetail.DisableControls;

  Posicao := mdDetail.RecNo;

  mdDetail.First;

  while not mdDetail.Eof do
  begin
    mdDetail.Edit;
    mdDetailSelecionado.AsBoolean := (mdDetailFLG_EXISTE.AsString = 'S');
    mdDetail.Post;
    mdDetail.Next;
  end;

  mdDetail.RecNo := Posicao;
  mdDetail.EnableControls;
end;

procedure TFImportarTxtRetornoServentias.FormCreate(Sender: TObject);
begin
  flbArquivosRetornoSAgreg.Directory := dm.ValorParametro(99);

  if flbArquivosRetornoSAgreg.Items.Count = 0 then
    flbArquivosRetornoSAgreg.Items.Add('');
end;

procedure TFImportarTxtRetornoServentias.btnCarregarClick(Sender: TObject);
var
  j, k, iQtd: Integer;
  sData, sProtNImp: String;
  dData: TDateTime;
  fsFormato: TFormatSettings;
  QryAux: TFDQuery;
begin
  sProtNImp  := '';

  iTotTitCar  := 0;
  j           := 0;
  k           := 0;
  iQtd        := 0;

  sData := '';
  dData := 0;
  fsFormato.ShortDateFormat := 'dd/mm;/yyyy';

  mdDetail.Close;
  mdDetail.Open;

  GR.CriarFDQuery(QryAux,'', dm.conSISTEMAAux);

  //Importa os dados de todos os arquivos
  for j := 0 to (flbArquivosRetornoSAgreg.Count - 1) do
  begin
    RE.Clear;

    if flbArquivosRetornoSAgreg.Selected[j] then
    begin
      //Importa os dados
      RE.Lines.LoadFromFile(flbArquivosRetornoSAgreg.FileName);

      for k := 0 to (RE.Lines.Count - 1) do
      begin
        if (Copy(RE.Lines[k], 1, 1) <> '0') and
          (Copy(RE.Lines[k], 1, 1) <> '9') then
        begin
          //Carga dos dados das linhas de Detail no Memory Data
          try
            qryPortadores.Close;
            qryPortadores.Params.ParamByName('CODIGO').Value := Texto(k, 2, 3);
            qryPortadores.Open;

            mdDetail.Append;

            mdDetailID_REGISTRO.AsString               := Texto(k, 1, 1);

            if Texto(k, 2, 3) <> '000' then
              mdDetailCOD_PORTADOR.AsString            := Texto(k, 2, 3);

            mdDetailCOD_CEDENTE.AsString               := Texto(k, 5, 15);
            mdDetailNOME_CEDENTE.AsString              := Texto(k, 20, 45);
            mdDetailNOME_SACADOR.AsString              := Texto(k, 65, 45);
            mdDetailDOC_SACADOR.AsString               := Texto(k, 110, 14);
            mdDetailEND_SACADOR.AsString               := Texto(k, 124, 45);

            if Texto(k, 169, 8) <> '00000000' then
              mdDetailCEP_SACADOR.AsInteger            := StrToInt(Texto(k, 169, 8));

            mdDetailCID_SACADOR.AsString               := Texto(k, 177, 20);
            mdDetailUF_SACADOR.AsString                := Texto(k, 197, 2);
            mdDetailNOSSO_NUMERO.AsString              := Texto(k, 199, 15);
            mdDetailESPECIE_TITULO.AsString            := Texto(k, 214, 3);
            mdDetailNUM_TITULO.AsString                := Texto(k, 217, 11);

            sData := (Texto(k, 228, 2) + '/' + Texto(k, 230, 2) + '/' + Texto(k, 232, 4));
            if TryStrToDate(sData, dData, fsFormato) then
              dData := StrToDate(Texto(k, 228, 2) + '/' + Texto(k, 230, 2) + '/' + Texto(k, 232, 4))
            else
              dData := 0;
            if dData <> 0 then
              mdDetailDATA_EMISSAO.AsDateTime          := dData;

            sData := (Texto(k, 236, 2) + '/' + Texto(k, 238, 2) + '/' + Texto(k, 244, 4));
            if TryStrToDate(sData, dData) then
              dData := StrToDate(Texto(k, 236, 2) + '/' + Texto(k, 238, 2) + '/' + Texto(k, 244, 4))
            else
              dData := 0;
            if dData <> 0 then
              mdDetailDATA_VENCIMENTO.AsDateTime       := dData;

            if (Texto(k, 244, 3) <> '000') and
              (Texto(k, 244, 3) <> '') then
              mdDetailTIPO_MOEDA.AsInteger             := StrToInt(Texto(k, 244, 3));

            if (Texto(k, 247, 14) <> '00000000000000') and
              (Texto(k, 247, 14) <> '') then
              mdDetailVALOR_TITULO.AsFloat             := PF.TextoParaFloat(Texto(k, 247, 14));

            if (Texto(k, 261, 14) <> '00000000000000') and
              (Texto(k, 261, 14) <> '') then
              mdDetailSALDO_TITULO.AsFloat             := PF.TextoParaFloat(Texto(k, 261, 14));

            mdDetailPRACA_PROTESTO.AsString            := Texto(k, 275, 20);
            mdDetailTIPO_ENDOSSO.AsString              := Texto(k, 295, 1);
            mdDetailINFO_ACEITE.AsString               := Texto(k, 296, 1);

            if (Texto(k, 297, 1) <> '0') and
              (Texto(k, 297, 1) <> '') then
              mdDetailNUM_CONTROLE.AsInteger           := StrToInt(Texto(k, 297, 1));

            mdDetailNOME_DEVEDOR.AsString              := Texto(k, 298, 45);

            if (Texto(k, 343, 3) <> '000') and
              (Texto(k, 343, 3) <> '') then
              mdDetailTIPO_ID_DEVEDOR.AsInteger        := StrToInt(Texto(k, 343, 3));

            mdDetailNUM_ID_DEVEDOR.AsString            := Texto(k, 346, 14);
            mdDetailDOC_DEVEDOR.AsString               := Texto(k, 360, 11);
            mdDetailEND_DEVEDOR.AsString               := Texto(k, 371, 45);

            if (Texto(k, 416, 8) <> '00000000') and
              (Texto(k, 416, 8) <> '') then
              mdDetailCEP_DEVEDOR.AsInteger            := StrToInt(Texto(k, 416, 8));

            mdDetailCID_DEVEDOR.AsString               := Texto(k, 424, 20);
            mdDetailUF_DEVEDOR.AsString                := Texto(k, 444, 2);

            if (Texto(k, 446, 2) <> '00') and
              (Texto(k, 446, 2) <> '') then
              mdDetailCOD_CARTORIO.AsInteger           := StrToInt(Texto(k, 446, 2));

            mdDetailTIPO_OCORRENCIA.AsString           := Texto(k, 458, 1);

            sData := (Texto(k, 459, 2) + '/' + Texto(k, 461, 2) + '/' + Texto(k, 463, 4));
            if TryStrToDate(sData, dData) then
              dData := StrToDate(Texto(k, 459, 2) + '/' + Texto(k, 461, 2) + '/' + Texto(k, 463, 4))
            else
              dData := 0;
            if dData <> 0 then
            begin
              mdDetailDATA_PROTOCOLO.AsDateTime        := dData;
              mdDetailDT_PROTOCOLO_CARTORIO.AsDateTime := dData;
            end;
            if (Texto(k, 467, 10) <> '0000000000') and
              (Texto(k, 467, 10) <> '') then
              mdDetailVALOR_CUSTAS_CART.AsFloat        := PF.TextoParaFloat(Texto(k, 467, 10));

            mdDetailDECLARACAO_PORT.AsString           := Texto(k, 477, 1);

            sData := (Texto(k, 478, 2) + '/' + Texto(k, 480, 2) + '/' + Texto(k, 482, 4));
            if TryStrToDate(sData, dData) then
              dData := StrToDate(Texto(k, 478, 2) + '/' + Texto(k, 480, 2) + '/' + Texto(k, 482, 4))
            else
              dData := 0;
            if dData <> 0 then
              mdDetailDATA_OCORRENCIA.AsDateTime       := dData;

            mdDetailCOD_IRREGULARIDADE.AsString        := Texto(k, 486, 2);
            mdDetailBAIRRO_DEVEDOR.AsString            := Texto(k, 488, 20);

            if (Texto(k, 508, 10) <> '0000000000') and
              (Texto(k, 508, 10) <> '') then
              mdDetailVALOR_CUSTAS_DIST.AsFloat        := PF.TextoParaFloat(Texto(k, 508, 10));

            if (Texto(k, 518, 6) <> '000000') and
              (Texto(k, 518, 6) <> '') then
              mdDetailREGISTRO_DIST.AsInteger          := StrToInt(Texto(k, 518, 6));

            if (Texto(k, 524, 10) <> '0000000000') and
              (Texto(k, 524, 10) <> '') then
              mdDetailVALOR_GRAVACAO.AsFloat           := PF.TextoParaFloat(Texto(k, 524, 10));

            if (Texto(k, 534, 5) <> '00000') and
              (Texto(k, 534, 5) <> '') then
              mdDetailNUM_OPERACAO.AsInteger           := StrToInt(Texto(k, 534, 5));

            mdDetailNUM_CONTRATO.AsString              := Texto(k, 539, 15);

            if (Texto(k, 554, 3) <> '000') and
              (Texto(k, 554, 3) <> '') then
              mdDetailNUM_PARCELA.AsInteger            := StrToInt(Texto(k, 554, 3));

            if (Texto(k, 557, 1) <> '0') and
              (Texto(k, 557, 1) <> '') then
              mdDetailTIPO_LETRA.AsInteger             := StrToInt(Texto(k, 557, 1));

            mdDetailCOMPL_COD_IRREG.AsString           := Texto(k, 558, 565);
            mdDetailPROTESTO_FALENCIA.AsString         := Texto(k, 566, 1);
            mdDetailPOSTECIPADO.AsString               := Texto(k, 567, 1);
            //mdDetailINSTRUMENTO_PROT.AsString          := Texto(k, 567, 1);

            if (Texto(k, 568, 10) <> '0000000000') and
              (Texto(k, 568, 10) <> '') then
              mdDetailVALOR_DEMAIS_DESP.AsFloat        := PF.TextoParaFloat(Texto(k, 568, 10));

            mdDetailCOMPL_REGISTRO.AsString            := Texto(k, 578, 19);

            if (Texto(k, 597, 4) <> '0000') and
              (Texto(k, 597, 4) <> '') then
              mdDetailSEQ_REGISTRO.AsInteger           := StrToInt(Texto(k, 597, 4));

            mdDetailPROTOCOLO_CARTORIO.AsString        := Texto(k, 448, 10);
            mdDetailPROTOCOLO_DISTRIBUIDOR.AsString    := Texto(k, 601, 10);

            mdDetailCPF_ESCREVENTE.AsString            := Texto(k, 611, 11);
            mdDetailLIVRO.AsString                     := Texto(k, 622, 10);
            mdDetailFOLHA.AsString                     := Texto(k, 632, 10);
            mdDetailSELO.AsString                      := Texto(k, 642, 9);
            mdDetailALEATORIO.AsString                 := Texto(k, 651, 3);
            mdDetailID_MSG.AsString                    := Texto(k, 654, 10);
            mdDetailREGISTRO.AsString                  := Texto(k, 664, 10);
            mdDetailELETRONICO.AsString                := Texto(k, 674, 1);
            mdDetailRECIBO_PAGAMENTO.AsString          := Texto(k, 675, 10);
            mdDetailTIPO_SUSTACAO.AsString             := Texto(k, 685, 1);

            sData := (Texto(k, 686, 2) + '/' + Texto(k, 688, 2) + '/' + Texto(k, 690, 4));
            if TryStrToDate(sData, dData) then
              dData := StrToDate(Texto(k, 686, 2) + '/' + Texto(k, 688, 2) + '/' + Texto(k, 690, 4))
            else
              dData := 0;
            if dData <> 0 then
              mdDetailDATA_RET_PROT.AsDateTime         := dData;

            if (Texto(k, 694, 14) <> '00000000000000') and
              (Texto(k, 694, 14) <> '') then
              mdDetailVALOR_PAGAMENTO.AsFloat          := PF.TextoParaFloat(Texto(k, 694, 14));

            mdDetailNUM_PAGAMENTO.AsString             := Texto(k, 708, 40);

            if (Texto(k, 748, 14) <> '00000000000000') and
              (Texto(k, 748, 14) <> '') then
              mdDetailTARIFA.AsFloat                   := PF.TextoParaFloat(Texto(k, 748, 14));

            if (Texto(k, 762, 14) <> '00000000000000') and
              (Texto(k, 762, 14) <> '') then
              mdDetailVALOR_AR.AsFloat                 := PF.TextoParaFloat(Texto(k, 762, 14));

            sData := (Texto(k, 776, 2) + '/' + Texto(k, 778, 2) + '/' + Texto(k, 780, 4));
            if TryStrToDate(sData, dData) then
              dData := StrToDate(Texto(k, 776, 2) + '/' + Texto(k, 778, 2) + '/' + Texto(k, 780, 4))
            else
              dData := 0;
            if dData <> 0 then
              mdDetailDATA_SUSTADO.AsDateTime          := dData;

            mdDetailDETERMINACAO.AsString              := Texto(k, 784, 100);
            mdDetailNOME_PORTADOR.AsString             := qryPortadoresNOME.AsString;
            mdDetailNOME_ARQ_RET.AsString              := Copy(ExtractFileName(flbArquivosRetornoSAgreg.FileName), 1, 20);

            case AnsiIndexStr(UpperCase(Texto(k, 458, 1)),
                              ['1', '2', '3', '4', '5', '6', '8', 'A','G']) of
              0 : mdDetailSTATUS.AsString := 'PAGO';
              1 : mdDetailSTATUS.AsString := 'PROTESTADO';
              2 : mdDetailSTATUS.AsString := 'RETIRADO';
              3 : mdDetailSTATUS.AsString := 'SUSTADO';
              4 : mdDetailSTATUS.AsString := 'DEVOLVIDO';
              5 : mdDetailSTATUS.AsString := 'DEVOLVIDO';
              6 : mdDetailSTATUS.AsString := 'ACEITO';
              7 : mdDetailSTATUS.AsString := 'CANCELADO';
              8 : mdDetailSTATUS.AsString := 'SUSTADO DEFINITIVO';
            end;

            QryAux.Close;
            QryAux.SQL.Clear;

            if Trim(Texto(k, 601, 10)) <> '' then
            begin
              QryAux.SQL.Text := 'SELECT ID_ATO, ' +
                                 '       RETORNO_CARTORIO, ' +
                                 '       NUMERO_TITULO ' +
                                 '  FROM TITULOS ' +
                                 ' WHERE PROTOCOLO = :PROTOCOLO';

              QryAux.Params.ParamByName('PROTOCOLO').AsString := Trim(Texto(k, 601, 10));
            end
            else
            begin
              QryAux.SQL.Text := 'SELECT ID_ATO, ' +
                                 '       RETORNO_CARTORIO, ' +
                                 '       NUMERO_TITULO ' +
                                 '  FROM TITULOS ' +
                                 ' WHERE RPAD(AGENCIA_CEDENTE, 15) = :AGENCIA_CEDENTE ' +
                                 '   AND LPAD(NOSSO_NUMERO, 15, ' + QuotedStr('0') + ') = :NOSSO_NUMERO';

              QryAux.Params.ParamByName('AGENCIA_CEDENTE').AsString := PF.Espaco(Texto(k, 5, 15), 'E', 15);
              QryAux.Params.ParamByName('NOSSO_NUMERO').AsString    := GR.Zeros(Texto(k, 199, 15), 'D', 15);
            end;

            QryAux.Open;

            if QryAux.RecordCount = 0 then
            begin
              if Trim(sProtNImp) = '' then
                sProtNImp := mdDetailPROTOCOLO_DISTRIBUIDOR.AsString
              else
                sProtNImp := (sProtNImp + ', ' + mdDetailPROTOCOLO_DISTRIBUIDOR.AsString);

              mdDetailSelecionado.AsBoolean   := False;
              mdDetailFLG_EXISTE.AsString     := 'N';
              mdDetailFLG_REIMPORTAR.AsString := 'N';
            end
            else
            begin
              mdDetailNUM_TITULO.AsString     := QryAux.FieldByName('NUMERO_TITULO').AsString;
              mdDetailSelecionado.AsBoolean   := (QryAux.FieldByName('RETORNO_CARTORIO').IsNull);
              mdDetailFLG_EXISTE.AsString     := 'S';
              mdDetailFLG_REIMPORTAR.AsString := IfThen(not QryAux.FieldByName('RETORNO_CARTORIO').IsNulL,
                                                        'S',
                                                        'N');
            end;

            mdDetail.Post;

            Inc(iQtd);
          except
            if Trim(sProtNImp) = '' then
              sProtNImp := mdDetailPROTOCOLO_DISTRIBUIDOR.AsString
            else
              sProtNImp := (sProtNImp + ', ' + mdDetailPROTOCOLO_DISTRIBUIDOR.AsString);
          end;
        end;
      end;

      iTotTitCar := (iTotTitCar + iQtd);
    end;

    if Trim(sProtNImp) <> '' then
    begin
      Application.MessageBox(PChar('Os seguintes Protocolos n�o foram encontrados cadastrados no Banco de Dados ' +
                                   'durante o carregamento do arquivo, ' +
                                   'portanto, n�o ficar�o habilitados para atualiza��o no Distribuidor: ' + #13#10 +
                                   sProtNImp + #13#10 +
                                   'Por favor, solicite novamente o arquivo � Serventia Agregada e ' +
                                   'tente import�-lo novamente. Caso o erro persista, entre em ' +
                                   'contato com o Suporte.' + #13#10 + #13#10 +
                                   'Os demais T�tulos foram carregados com sucesso!'), 'Aviso', MB_OK + MB_ICONWARNING);
    end;

    //Acerta a contagem de Titulos
    if iTotTitCar = 0 then
      lblTitulos.Caption := 'Nenhum T�tulo carregado'
    else if iTotTitCar = 1 then
      lblTitulos.Caption := '1 T�tulo carregado'
    else if iTotTitCar > 1 then
      lblTitulos.Caption := IntToStr(iTotTitCar) + ' T�tulos carregados';

    btnProcessar.Enabled := (iTotTitCar > 0);
  end;

  MarcarTodos.Click;

  FreeAndNil(QryAux);
end;

procedure TFImportarTxtRetornoServentias.btnLimparClick(Sender: TObject);
begin
  mdDetail.Close;
  btnProcessar.Enabled := False;
end;

procedure TFImportarTxtRetornoServentias.btnProcessarClick(Sender: TObject);
var
  IdMovimento, IdImportado, j: Integer;
  lOk: Boolean;
  cSaldoProt: Double;
  fsFormato: TFormatSettings;
  Dia, Mes, Ano: Word;
begin
  iTotTitImp  := 0;
  cSaldoProt  := 0;
  IdMovimento := 0;
  IdImportado := 0;

  LstArqR := TListBox.Create(Self);
  LstArqR.Parent  := Self;
  LstArqR.Visible := False;

  dm.VerificarExistenciaPastasSistema;

  DecodeDate(Date, Ano, Mes, Dia);

  fsFormato.ShortDateFormat := 'dd/mm;/yyyy';

  lOk := True;

  dm.Portadores.Close;
  dm.Portadores.Open;

  dm.ServentiaAgregada.Close;
  dm.ServentiaAgregada.Open;

  IdMovimento := dm.ProximoId('ID_MOVIMENTO',
                              'IDS',
                              1,
                              False);
  IdImportado := dm.ProximoId('ID_IMPORTADO',
                              'IDS',
                              1,
                              False);

  //Atualiza os dados dos Titulos
  try
    if dm.conSISTEMA.Connected then
      dm.conSISTEMA.StartTransaction;

    mdDetail.First;

    while not mdDetail.Eof do
    begin
      if mdDetailSelecionado.AsBoolean then
      begin
        cdsTitulo.Close;

        if Trim(mdDetailPROTOCOLO_DISTRIBUIDOR.AsString) <> '' then
        begin
          qryTitulo.SQL.Text := 'SELECT * ' +
                              '  FROM TITULOS ' +
                              ' WHERE PROTOCOLO = :PROTOCOLO';

          cdsTitulo.FetchParams;

          cdsTitulo.Params.ParamByName('PROTOCOLO').AsString := mdDetailPROTOCOLO_DISTRIBUIDOR.AsString;
        end
        else
        begin
          qryTitulo.SQL.Text := 'SELECT * ' +
                              '  FROM TITULOS ' +
                              ' WHERE RPAD(AGENCIA_CEDENTE, 15) = :AGENCIA_CEDENTE ' +
                              '   AND LPAD(NOSSO_NUMERO, 15, ' + QuotedStr('0') + ') = :NOSSO_NUMERO';

          cdsTitulo.FetchParams;

          cdsTitulo.Params.ParamByName('AGENCIA_CEDENTE').AsString := PF.Espaco(mdDetailCOD_CEDENTE.AsString, 'E', 15);
          cdsTitulo.Params.ParamByName('NOSSO_NUMERO').AsString    := GR.Zeros(mdDetailNOSSO_NUMERO.AsString, 'D', 15);
        end;

        cdsTitulo.Open;

        if cdsTitulo.RecordCount > 0 then
        begin
          { TITULO }
          mdDetail.Edit;
          mdDetailPROTOCOLO_DISTRIBUIDOR.AsString := cdsTituloPROTOCOLO.AsString;
          mdDetail.Post;

          cdsTitulo.Edit;

          if Trim(mdDetailFLG_REIMPORTAR.AsString) = 'N' then
          begin
            cdsTituloPROTOCOLO_CARTORIO.AsString      := mdDetailPROTOCOLO_CARTORIO.AsString;
            cdstituloDT_PROTOCOLO_CARTORIO.AsDateTime := mdDetailDT_PROTOCOLO_CARTORIO.AsDateTime;
          end;

          cdsTituloRETORNO_CARTORIO.AsString          := mdDetailNOME_ARQ_RET.AsString;
          cdsTituloDT_DEVOLVIDO_CARTORIO.AsDateTime   := Date;
          cdsTituloSTATUS.AsString                    := mdDetailSTATUS.AsString;

          case AnsiIndexStr(UpperCase(mdDetailSTATUS.AsString), ['PAGO', 'PROTESTADO',
                                                                 'RETIRADO', 'SUSTADO',
                                                                 'DEVOLVIDO', 'CANCELADO','SUSTADO DEFINITIVO']) of
            0: //PAGO
            begin
              if (Trim(mdDetailDATA_OCORRENCIA.AsString) <> '') and
                (mdDetailDATA_OCORRENCIA.AsDateTime > 0) then
              begin
                cdsTituloDT_REGISTRO.AsDateTime      := mdDetailDATA_OCORRENCIA.AsDateTime;
                cdsTituloDT_PAGAMENTO.AsDateTime     := mdDetailDATA_OCORRENCIA.AsDateTime;
              end;

              cdsTituloSELO_REGISTRO.AsString      := mdDetailSELO.AsString;
              cdsTituloALEATORIO_PROTESTO.AsString := mdDetailALEATORIO.AsString;
              cdsTituloCPF_ESCREVENTE.AsString     := mdDetailCPF_ESCREVENTE.AsString;

              if Trim(mdDetailVALOR_PAGAMENTO.AsString) <> '' then
                cdsTituloVALOR_PAGAMENTO.AsFloat     := mdDetailVALOR_PAGAMENTO.AsFloat;

              if mdDetailNUM_PAGAMENTO.AsInteger <> 0 then
                cdsTituloNUMERO_PAGAMENTO.AsString := mdDetailNUM_PAGAMENTO.AsString;

              cdsTituloTARIFA_BANCARIA.AsFloat     := mdDetailTARIFA.AsFloat;

              cSaldoProt := cdsTituloEMOLUMENTOS.AsFloat +
                            cdsTituloFETJ.AsFloat +
                            cdsTituloFUNDPERJ.AsFloat +
                            cdsTituloFUNPERJ.AsFloat +
                            cdsTituloFUNARPEN.AsFloat +
                            cdsTituloPMCMV.AsFloat +
                            cdsTituloISS.AsFloat +
                            cdsTituloMUTUA.AsFloat +
                            cdsTituloACOTERJ.AsFloat +
                            cdsTituloDISTRIBUICAO.AsFloat +
                            mdDetailVALOR_AR.AsFloat +
                            mdDetailTARIFA.AsFloat;

              if cSaldoProt > 0 then
              begin
                cdsTituloTOTAL.AsFloat          := cSaldoProt;
                cdsTituloSALDO_PROTESTO.AsFloat := mdDetailSALDO_TITULO.AsFloat + cSaldoProt;
              end;

              cdsTituloDT_SUSTADO.Clear;
              cdsTituloDT_RETIRADO.Clear;
            end;
            1: //PROTESTADO
            begin
              if (Trim(cdsTituloTIPO_SUSTACAO.AsString) = '') or
                (cdsTituloTIPO_SUSTACAO.IsNull) then
              begin  //Protestado
                cdsTituloPROTESTADO.AsString         := 'S';

                if Trim(mdDetailREGISTRO.AsString) <> '' then
                  cdsTituloREGISTRO.AsInteger        := mdDetailREGISTRO.AsInteger;

                if (Trim(mdDetailDATA_OCORRENCIA.AsString) <> '') and
                  (mdDetailDATA_OCORRENCIA.AsDateTime > 0) then
                  cdsTituloDT_REGISTRO.AsDateTime    := mdDetailDATA_OCORRENCIA.AsDateTime;

                if Trim(mdDetailLIVRO.AsString) <> '' then
                  cdsTituloLIVRO_REGISTRO.AsInteger    := mdDetailLIVRO.AsInteger;

                cdsTituloFOLHA_REGISTRO.AsString     := mdDetailFOLHA.AsString;
                cdsTituloSELO_REGISTRO.AsString      := mdDetailSELO.AsString;
                cdsTituloALEATORIO_PROTESTO.AsString := mdDetailALEATORIO.AsString;
                cdsTituloCPF_ESCREVENTE.AsString     := mdDetailCPF_ESCREVENTE.AsString;

                if Trim(mdDetailVALOR_PAGAMENTO.AsString) <> '' then
                  cdsTituloVALOR_PAGAMENTO.AsFloat     := mdDetailVALOR_PAGAMENTO.AsFloat;

                if Trim(mdDetailID_MSG.AsString) <> '' then
                  cdsTituloID_MSG.AsInteger          := mdDetailID_MSG.AsInteger;

                cdsTituloCPF_ESCREVENTE_PG.Clear;
                cdsTituloDT_PAGAMENTO.Clear;
                cdsTituloSELO_PAGAMENTO.Clear;
                cdsTituloDT_SUSTADO.Clear;
                cdsTituloDT_RETIRADO.Clear;
              end
              else  //Sustado
              begin
                if Trim(cdsTituloTIPO_SUSTACAO.AsString) = 'C' then
                begin
                  if Trim(mdDetailVALOR_PAGAMENTO.AsString) <> '' then
                    cdsTituloVALOR_PAGAMENTO.AsFloat        := mdDetailVALOR_PAGAMENTO.AsFloat;

                  cdsTituloCPF_ESCREVENTE.AsString        := mdDetailCPF_ESCREVENTE.AsString;
                  cdsTituloTIPO_SUSTACAO.AsString         := mdDetailTIPO_SUSTACAO.AsString;
                  cdsTituloJUDICIAL.AsString              := 'N';

                  if (Trim(mdDetailDATA_RET_PROT.AsString) <> '') and
                    (mdDetailDATA_RET_PROT.AsDateTime > 0) then
                    cdsTituloDT_RETORNO_PROTESTO.AsDateTime := mdDetailDATA_RET_PROT.AsDateTime;

                  cdsTituloDETERMINACAO.Clear;
                end;
              end;
            end;
            2: //RETIRADO
            begin
              if (Trim(mdDetailDATA_OCORRENCIA.AsString) <> '') and
                (mdDetailDATA_OCORRENCIA.AsDateTime > 0) then
                cdsTituloDT_RETIRADO.AsDateTime    := mdDetailDATA_OCORRENCIA.AsDateTime;

              cdsTituloSELO_REGISTRO.AsString      := mdDetailSELO.AsString;
              cdsTituloALEATORIO_PROTESTO.AsString := mdDetailALEATORIO.AsString;
              cdsTituloPROTESTADO.AsString         := 'N';

              if mdDetailVALOR_PAGAMENTO.AsInteger <> 0 then
                cdsTituloVALOR_PAGAMENTO.AsFloat   := mdDetailVALOR_PAGAMENTO.AsFloat;

              cdsTituloCPF_ESCREVENTE.AsString     := mdDetailCPF_ESCREVENTE.AsString;

              if mdDetailCOD_IRREGULARIDADE.AsInteger <> 0 then
                cdsTituloIRREGULARIDADE.AsInteger    := mdDetailCOD_IRREGULARIDADE.AsInteger;
            end;
            3: //SUSTADO
            begin
              if mdDetailVALOR_PAGAMENTO.AsInteger <> 0 then
                cdsTituloVALOR_PAGAMENTO.AsFloat    := mdDetailVALOR_PAGAMENTO.AsFloat;

              cdsTituloCPF_ESCREVENTE.AsString      := mdDetailCPF_ESCREVENTE.AsString;
              cdsTituloTIPO_SUSTACAO.AsString       := mdDetailTIPO_SUSTACAO.AsString;

              if Trim(mdDetailTIPO_SUSTACAO.AsString) = 'L' then
                cdsTituloJUDICIAL.AsString            := 'N'
              else if Trim(mdDetailTIPO_SUSTACAO.AsString) = 'D' then
                cdsTituloJUDICIAL.AsString            := 'S';

              if (Trim(mdDetailDETERMINACAO.AsString) = '') or
                (mdDetailDETERMINACAO.IsNull) then
                cdsTituloDETERMINACAO.Clear
              else
                cdsTituloDETERMINACAO.AsString      := mdDetailDETERMINACAO.AsString;

              if (mdDetailDATA_SUSTADO.AsDateTime = 0) or
                (mdDetailDATA_SUSTADO.IsNull) then
              begin
                cdsTituloDT_SUSTADO.Clear;
                cdsTituloDT_DEFINITIVA.Clear;
              end
              else
              begin
                if Trim(mdDetailTIPO_SUSTACAO.AsString) = 'L' then
                  cdsTituloDT_SUSTADO.AsDateTime    := mdDetailDATA_SUSTADO.AsDateTime
                else if Trim(mdDetailTIPO_SUSTACAO.AsString) = 'D' then
                  cdsTituloDT_DEFINITIVA.AsDateTime := mdDetailDATA_SUSTADO.AsDateTime;
              end;

              if (Trim(mdDetailSELO.AsString) = '') or
                (mdDetailSELO.IsNull) then
                cdsTituloSELO_PAGAMENTO.Clear
              else
                cdsTituloSELO_PAGAMENTO.AsString    := mdDetailSELO.AsString;

              if (Trim(mdDetailALEATORIO.AsString) = '') or
                (mdDetailALEATORIO.IsNull) then
                cdsTituloALEATORIO_SOLUCAO.Clear
              else
                cdsTituloALEATORIO_SOLUCAO.AsString := mdDetailALEATORIO.AsString;
            end;
            4: //DEVOLVIDO
            begin
              if (Trim(mdDetailDATA_OCORRENCIA.AsString) <> '') and
                (mdDetailDATA_OCORRENCIA.AsDateTime > 0) then
                cdsTituloDT_DEVOLVIDO.AsDateTime  := mdDetailDATA_OCORRENCIA.AsDateTime;

              cdsTituloPROTESTADO.AsString      := 'N';

              if mdDetailVALOR_PAGAMENTO.AsInteger <> 0 then
               cdsTituloVALOR_PAGAMENTO.AsFloat  := mdDetailVALOR_PAGAMENTO.AsFloat;

              cdsTituloCPF_ESCREVENTE.AsString  := mdDetailCPF_ESCREVENTE.AsString;

              if mdDetailCOD_IRREGULARIDADE.AsInteger <> 0 then
               cdsTituloIRREGULARIDADE.AsInteger := mdDetailCOD_IRREGULARIDADE.AsInteger;
            end;
            5: //CANCELADO
            begin
              if (Trim(mdDetailDATA_OCORRENCIA.AsString) <> '') and
                (mdDetailDATA_OCORRENCIA.AsDateTime > 0) then
                cdsTituloDT_PAGAMENTO.AsDateTime   := mdDetailDATA_OCORRENCIA.AsDateTime;

              cdsTituloSELO_REGISTRO.AsString      := mdDetailSELO.AsString;
              cdsTituloALEATORIO_PROTESTO.AsString := mdDetailALEATORIO.AsString;

              if mdDetailVALOR_PAGAMENTO.AsInteger <> 0 then
               cdsTituloVALOR_PAGAMENTO.AsFloat     := mdDetailVALOR_PAGAMENTO.AsFloat;

              cdsTituloCPF_ESCREVENTE.AsString     := mdDetailCPF_ESCREVENTE.AsString;

              if mdDetailRECIBO_PAGAMENTO.AsInteger <> 0 then
                cdsTituloRECIBO_PAGAMENTO.AsInteger := mdDetailRECIBO_PAGAMENTO.AsInteger;

              cdsTituloELETRONICO.AsString         := mdDetailELETRONICO.AsString;
              cdsTituloDT_SUSTADO.Clear;
              cdsTituloDT_RETIRADO.Clear;
            end;


          end;

          cdsTitulo.Post;
          cdsTitulo.ApplyUpdates(0);

          { MOVIMENTO }
          Inc(IdMovimento);

          dm.Movimento.Close;
          dm.Movimento.Params[0].AsInteger := cdsTituloID_ATO.AsInteger;
          dm.Movimento.Open;

          dm.Movimento.Append;
          dm.MovimentoID_MOVIMENTO.AsInteger := IdMovimento;
          dm.MovimentoID_ATO.AsInteger       := cdsTituloID_ATO.AsInteger;
          dm.MovimentoDATA.AsDateTime        := Now;
          dm.MovimentoHORA.AsDateTime        := Time;
          dm.MovimentoBAIXA.AsDateTime       := cdsTituloDT_PAGAMENTO.AsDateTime;
          dm.MovimentoESCREVENTE.AsString    := dm.vNome;
          dm.MovimentoSTATUS.AsString        := cdsTituloSTATUS.AsString;
          dm.Movimento.Post;

          GR.ExecutarSQLDAC('UPDATE IDS ' +
                           '   SET VALOR = ' + IntToStr(IdMovimento) +
                           ' WHERE CAMPO = ' + QuotedStr('ID_MOVIMENTO'),
                           dm.conSISTEMA);

          dm.Movimento.ApplyUpdates(0);

          { IMPORTADOS }
          dm.Importados.Close;
          dm.Importados.Params.ParamByName('ARQUIVO').AsString := mdDetailNOME_ARQ_RET.AsString;
          dm.Importados.Open;

          if dm.Importados.RecordCount = 0 then
          begin
            Inc(IdImportado);

            dm.Importados.Append;
            dm.ImportadosID_IMPORTADO.AsInteger := IdImportado;
            dm.ImportadosARQUIVO.AsString       := mdDetailNOME_ARQ_RET.AsString;
            dm.ImportadosDATA.AsDateTime        := Date;
            dm.ImportadosTITULOS.AsInteger      := 1;
            dm.ImportadosACEITOS.AsInteger      := 1;
            dm.ImportadosREJEITADOS.AsInteger   := 0;
            dm.Importados.Post;

            GR.ExecutarSQLDAC('UPDATE IDS ' +
                             '   SET VALOR = ' + IntToStr(IdImportado) +
                             ' WHERE CAMPO = ' + QuotedStr('ID_IMPORTADO'),
                             dm.conSISTEMA);

            dm.Importados.ApplyUpdates(0);
          end;

          iTotTitImp := (iTotTitImp + 1);

          if LstArqR.Items.IndexOf(mdDetailNOME_ARQ_RET.AsString) < 0 then
            LstArqR.Items.Add(mdDetailNOME_ARQ_RET.AsString);
        end;
      end;

      mdDetail.Next;
    end;

    if dm.conSISTEMA.InTransaction then
      dm.conSISTEMA.Commit;

    //Acerta a contagem de Titulos
    if iTotTitImp = 0 then
      lblTitulos.Caption := lblTitulos.Caption + ' | Nenhum T�tulo importado'
    else if iTotTitImp = 1 then
      lblTitulos.Caption := lblTitulos.Caption + ' | 1 T�tulo importado'
    else if iTotTitImp > 1 then
      lblTitulos.Caption := lblTitulos.Caption + ' | ' + IntToStr(iTotTitImp) + ' T�tulos importados';

    Application.MessageBox('T�tulos atualizados com sucesso!',
                           'Sucesso!',
                           MB_OK);
  except
    on E: Exception do
    begin
      if dm.conSISTEMA.InTransaction then
        dm.conSISTEMA.Rollback;

      lOk := False;

      Application.MessageBox(PChar('Erro na atualiza��o de T�tulos. Nenhum T�tulo foi atualizado.' + #13#10 +
                                   '(' + E.Message + ')'),
                             'Erro',
                             MB_OK + MB_ICONERROR);
    end;
  end;

  //Chama a tela para geracao do arquivo R de Retorno para o CRA
  if lOk then
  begin
    { Move os arquivo de Retorno das Serventias Agregadas da raiz da pasta Retorno_Agregadas para a pasta do dia }
    for j := 0 to LstArqR.Items.Count - 1 do
    begin
      if FileExists(PChar(dm.ValorParametro(99) +
                    IntToStr(Ano) + '\' +
                    GR.Zeros(IntToStr(Mes), 'D', 2) + '\' +
                    GR.Zeros(IntToStr(Dia), 'D', 2) + '\' +
                    LstArqR.Items.Strings[j])) then
        DeleteFile(PChar(dm.ValorParametro(99) +
                   IntToStr(Ano) + '\' +
                   GR.Zeros(IntToStr(Mes), 'D', 2) + '\' +
                   GR.Zeros(IntToStr(Dia), 'D', 2) + '\' +
                   LstArqR.Items.Strings[j]));

      if CopyFile(PChar(dm.ValorParametro(99) + LstArqR.Items.Strings[j]),
                  PChar(dm.ValorParametro(99) +
                        IntToStr(Ano) + '\' +
                        GR.Zeros(IntToStr(Mes), 'D', 2) + '\' +
                        GR.Zeros(IntToStr(Dia), 'D', 2) + '\' +
                        LstArqR.Items.Strings[j]),
                  True) then
        DeleteFile(PChar(dm.ValorParametro(99) + LstArqR.Items.Strings[j]));
    end;

    FreeAndNil(LstArqR);

    if Application.MessageBox('Deseja gerar o Arquivo de Retorno para o CRA?',
                              'Confirma��o',
                              MB_YESNO + MB_ICONQUESTION) = ID_YES then
      GR.CriarForm(TFExportarTxt, FExportarTxt);
  end;

  btnProcessar.Enabled := False;
end;

procedure TFImportarTxtRetornoServentias.DesmarcarTodosClick(Sender: TObject);
var
  Posicao: Integer;
begin
  mdDetail.DisableControls;

  Posicao := mdDetail.RecNo;

  mdDetail.First;

  while not mdDetail.Eof do
  begin
    mdDetail.Edit;
    mdDetailSelecionado.AsBoolean := False;
    mdDetail.Post;
    mdDetail.Next;
  end;

  mdDetail.RecNo := Posicao;
  mdDetail.EnableControls;
end;

function TFImportarTxtRetornoServentias.Texto(Linha, Inicio,
  Fim: Integer): String;
begin
  Result := Trim(Copy(RE.Lines[Linha], Inicio, Fim));
end;

end.
