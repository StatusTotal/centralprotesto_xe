unit URelIntimacao1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, ExtCtrls, sPanel, sEdit, sGroupBox,
  QuickRpt, QRCtrls, DB, RxMemDS, FMTBcd, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, sRadioButton, SqlExpr, Grids, Wwdbigrd,
  Wwdbgrid, DBClient, SimpleDS, Menus, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFRelIntimacao1 = class(TForm)
    P: TsPanel;
    GbProtocolo: TsGroupBox;
    edProtFinal: TsEdit;
    edProtInicial: TsEdit;
    Rb1: TsRadioButton;
    GbData: TsGroupBox;
    Rb2: TsRadioButton;
    edDataInicial: TsDateEdit;
    edDataFinal: TsDateEdit;
    dsProtocolo: TDataSource;
    GridAtos: TwwDBGrid;
    btFiltrar: TsBitBtn;
    btVisualizar: TsBitBtn;
    PM: TPopupMenu;
    Marcartodos1: TMenuItem;
    Desmarcartodos1: TMenuItem;
    qryProtocolo: TFDQuery;
    qryProtocoloID_ATO: TIntegerField;
    qryProtocoloPROTOCOLO: TIntegerField;
    qryProtocoloDT_PROTOCOLO: TDateField;
    qryProtocoloAPRESENTANTE: TStringField;
    qryProtocoloDEVEDOR: TStringField;
    qryProtocoloSTATUS: TStringField;
    qryProtocoloCheck: TBooleanField;
    procedure edProtInicialKeyPress(Sender: TObject; var Key: Char);
    procedure edProtFinalKeyPress(Sender: TObject; var Key: Char);
    procedure Rb1Click(Sender: TObject);
    procedure Rb2Click(Sender: TObject);
    procedure btFiltrarClick(Sender: TObject);
    procedure GridAtosFieldChanged(Sender: TObject; Field: TField);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GridAtosTitleButtonClick(Sender: TObject; AFieldName: String);
    procedure btVisualizarClick(Sender: TObject);
    procedure edDataInicialExit(Sender: TObject);
    procedure Marcartodos1Click(Sender: TObject);
    procedure Desmarcartodos1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelIntimacao1: TFRelIntimacao1;

implementation

uses UDM, UQuickIntimacao1, UPF, UQuickInstrumento1, UGeral, Math;

{$R *.dfm}

procedure TFRelIntimacao1.edProtInicialKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFRelIntimacao1.edProtFinalKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFRelIntimacao1.Rb1Click(Sender: TObject);
begin
  GbData.Enabled:=False;
  GbProtocolo.Enabled:=True;
  edDataInicial.Clear;
  edDataFinal.Clear;
  edProtInicial.SetFocus;
end;

procedure TFRelIntimacao1.Rb2Click(Sender: TObject);
begin
  GbData.Enabled:=True;
  GbProtocolo.Enabled:=False;
  edProtInicial.Clear;
  edProtFinal.Clear;
  edDataInicial.SetFocus;
end;

procedure TFRelIntimacao1.btFiltrarClick(Sender: TObject);
begin
  if Rb1.Checked then
  if edProtInicial.Text='' then
  begin
      edProtInicial.SetFocus;
      Exit;
  end;

  if edProtFinal.Text='' then edProtFinal.Text:=edProtInicial.Text;

  qryProtocolo.Close;
  if Rb1.Checked then
  begin
      qryProtocolo.SQL.Text:='SELECT ID_ATO,PROTOCOLO,DT_PROTOCOLO,APRESENTANTE,DEVEDOR,STATUS FROM TITULOS WHERE PROTOCOLO BETWEEN :P1 AND :P2';
      qryProtocolo.ParamByName('P1').AsInteger:=StrToInt(edProtInicial.Text);
      qryProtocolo.ParamByName('P2').AsInteger:=StrToInt(edProtFinal.Text);
  end
  else
  begin
      qryProtocolo.SQL.Text:='SELECT ID_ATO,PROTOCOLO,DT_PROTOCOLO,APRESENTANTE,DEVEDOR,STATUS FROM TITULOS WHERE DT_PROTOCOLO BETWEEN :D1 AND :D2 ';
      qryProtocolo.ParamByName('D1').AsDate:=edDataInicial.Date;
      qryProtocolo.ParamByName('D2').AsDate:=edDataFinal.Date;
  end;
  qryProtocolo.Open;
end;

procedure TFRelIntimacao1.GridAtosFieldChanged(Sender: TObject;
  Field: TField);
begin
  if qryProtocolo.State in [dsEdit,dsInsert] then qryProtocolo.Post;
end;

procedure TFRelIntimacao1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFRelIntimacao1.GridAtosTitleButtonClick(Sender: TObject;
  AFieldName: String);
begin
  qryProtocolo.IndexFieldNames:=AFieldName;
end;

procedure TFRelIntimacao1.btVisualizarClick(Sender: TObject);
var
  P: Pointer;
begin
  if qryProtocolo.IsEmpty then Exit;

  try
    PF.Aguarde(True);

    Application.CreateForm(TFQuickIntimacao1,FQuickIntimacao1);

    P:=qryProtocolo.GetBookmark;
    qryProtocolo.DisableControls;
    qryProtocolo.First;
    while not qryProtocolo.Eof do
    begin
        if qryProtocoloCheck.Value=True then
        FQuickIntimacao1.CarregarRX(qryProtocoloID_ATO.AsInteger);
        qryProtocolo.Next;
    end;

    if P<>Nil then
      if qryProtocolo.BookmarkValid(P) then
        qryProtocolo.GotoBookmark(P);

    qryProtocolo.EnableControls;

    FQuickIntimacao1.QkIntimacao.Preview;
    FQuickIntimacao1.Free;
  except
    on E:Exception do
    begin
        PF.Aguarde(False);
        GR.Aviso('Erro: '+E.Message);
    end;
  end;
end;

procedure TFRelIntimacao1.edDataInicialExit(Sender: TObject);
begin
  if edDataFinal.Date<edDataInicial.Date then
    edDataFinal.Date:=edDataInicial.Date;
end;

procedure TFRelIntimacao1.Marcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=qryProtocolo.RecNo;
  qryProtocolo.DisableControls;
  qryProtocolo.First;
  while not qryProtocolo.Eof do
  begin
      qryProtocolo.Edit;
      qryProtocoloCheck.AsBoolean:=True;
      qryProtocolo.Post;
      qryProtocolo.Next;
  end;
  qryProtocolo.RecNo:=Posicao;
  qryProtocolo.EnableControls;
end;

procedure TFRelIntimacao1.Desmarcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=qryProtocolo.RecNo;
  qryProtocolo.DisableControls;
  qryProtocolo.First;
  while not qryProtocolo.Eof do
  begin
      qryProtocolo.Edit;
      qryProtocoloCheck.AsBoolean:=False;
      qryProtocolo.Post;
      qryProtocolo.Next;
  end;
  qryProtocolo.RecNo:=Posicao;
  qryProtocolo.EnableControls;
end;

end.
