unit UQuickIndice1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, DB, DBClient, SimpleDS, RxMemDS,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFQuickIndice1 = class(TForm)
    Relatorio: TQuickRep;
    Cabecalho: TQRBand;
    txNomeServentia: TQRDBText;
    txEndereco: TQRDBText;
    lbCertidao: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    RX: TRxMemoryData;
    RXNome: TStringField;
    RXQualificacao: TStringField;
    RXLivro: TStringField;
    RXFolha: TStringField;
    QRBand1: TQRBand;
    ChildBand1: TQRChildBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    RXData: TDateField;
    QRLabel6: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    RXProtocolo: TStringField;
    QRLabel7: TQRLabel;
    QRDBText7: TQRDBText;
    RXSituacao: TStringField;
    lbFolha: TQRLabel;
    qryIndice: TFDQuery;
    qryIndiceID_ATO: TIntegerField;
    qryIndiceDT_PROTOCOLO: TDateField;
    qryIndicePROTOCOLO: TIntegerField;
    qryIndiceLIVRO_PROTOCOLO: TIntegerField;
    qryIndiceFOLHA_PROTOCOLO: TStringField;
    qryIndiceAPRESENTANTE: TStringField;
    qryIndiceCEDENTE: TStringField;
    qryIndiceSACADOR: TStringField;
    qryIndiceDEVEDOR: TStringField;
    qryIndiceLIVRO_REGISTRO: TIntegerField;
    qryIndiceFOLHA_REGISTRO: TStringField;
    qryIndiceDT_PAGAMENTO: TDateField;
    qryIndiceDT_REGISTRO: TDateField;
    qryIndiceSTATUS: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure RelatorioEndPage(Sender: TCustomQuickRep);
    procedure CabecalhoBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure RelatorioAfterPreview(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    vFolha,vInicial: Integer;
  end;

var
  FQuickIndice1: TFQuickIndice1;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFQuickIndice1.FormCreate(Sender: TObject);
begin
  qryIndice.Connection:=dm.conSISTEMA;
end;

procedure TFQuickIndice1.RelatorioEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickIndice1.CabecalhoBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  if vFolha<>0 then
  begin
      lbFolha.Caption:='Folha: '+IntToStr(vFolha);
      Inc(vFolha);
  end;
end;

procedure TFQuickIndice1.RelatorioAfterPreview(Sender: TObject);
begin
  vFolha:=vInicial;
end;

end.
