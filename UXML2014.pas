unit UXML2014;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, Grids, Wwdbigrd, Wwdbgrid, DB, DBCtrls,
  sDBLookupComboBox, StdCtrls, Buttons, sBitBtn, Mask, sMaskEdit, ShellAPI,
  sCustomComboEdit, sTooledit, sRadioButton, sCheckBox, sLabel, RxMemDS,
  SQLExpr,DateUtils, FMTBcd, Menus, sEdit, DBClient, sGroupBox, SimpleDS, sDBNavigator,
  Data.DBXInterBase, Data.DBXFirebird, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFXML2014 = class(TForm)
    Grid: TwwDBGrid;
    P1: TsPanel;
    ckEnviado: TsCheckBox;
    btLocalizar: TsBitBtn;
    dsEscreventes: TDataSource;
    dsAtos: TDataSource;
    PM: TPopupMenu;
    Marcartodos1: TMenuItem;
    Desmarcartodos1: TMenuItem;
    N1: TMenuItem;
    sPanel1: TsPanel;
    sLabel1: TsLabel;
    lbCaminho: TsLabel;
    MarcarTodosEnviado1: TMenuItem;
    DesmarcarTodosXML1: TMenuItem;
    cdsAtos: TClientDataSet;
    cdsAtosIDATO: TIntegerField;
    cdsAtosSTATUS: TStringField;
    cdsAtosXML: TBooleanField;
    cdsAtosENVIADO: TStringField;
    cdsAtosDATAPRATICA: TStringField;
    cdsAtosSELO: TStringField;
    cdsAtosALEATORIO: TStringField;
    cdsAtosNUMERORECIBO: TStringField;
    cdsAtosCCT: TStringField;
    cdsAtosTIPOTITULO: TStringField;
    cdsAtosNUMEROTITULO: TStringField;
    cdsAtosVALORTITULO: TStringField;
    cdsAtosSALDO: TStringField;
    cdsAtosDATAVENCIMENTO: TStringField;
    cdsAtosINDCONVENIO: TStringField;
    cdsAtosTIPOAPRESENTACAO: TStringField;
    cdsAtosNUMEROPROTOCOLO: TStringField;
    cdsAtosNUMEROGUIA: TStringField;
    cdsAtosFORMAPAGAMENTO: TStringField;
    cdsAtosDESCRICAOSUSTACAO: TStringField;
    cdsAtosNUMEROLIVRO: TStringField;
    cdsAtosNUMEROFOLHA: TStringField;
    cdsAtosTIPOCERTIDAO: TStringField;
    cdsAtosQTDPAGINAS: TStringField;
    cdsAtosRESULTADO: TStringField;
    cdsAtosNUMEROREGISTROS: TStringField;
    cdsAtosREVALIDADA: TStringField;
    cdsAtosQTDPAGEXCEDENTES: TStringField;
    cdsAtosNUMEROATO: TStringField;
    cdsAtosNOMEVERBAL: TStringField;
    cdsAtosNOMEDEVEDOR: TStringField;
    cdsAtosTIPODEVEDOR: TStringField;
    cdsAtosDOCUMENTODEVEDOR: TStringField;
    cdsAtosNOMESACADOR: TStringField;
    cdsAtosTIPOSACADOR: TStringField;
    cdsAtosDOCUMENTOSACADOR: TStringField;
    cdsAtosNOMEAPRESENTANTE: TStringField;
    cdsAtosTIPOAPRESENTANTE: TStringField;
    cdsAtosDOCUMENTOAPRESENTANTE: TStringField;
    cdsAtosNOMECEDENTE: TStringField;
    cdsAtosTIPOCOBRANCA: TStringField;
    cdsAtosEMOLUMENTOS: TStringField;
    cdsAtosFETJ: TStringField;
    cdsAtosFUNDPERJ: TStringField;
    cdsAtosFUNPERJ: TStringField;
    cdsAtosFUNARPEN: TStringField;
    cdsAtosPMCMV: TStringField;
    cdsAtosMUTUA: TStringField;
    cdsAtosACOTERJ: TStringField;
    cdsAtosDISTRIBUICAO: TStringField;
    cdsAtosTOTAL: TStringField;
    cdsAtosDATAPROTOCOLO: TStringField;
    cdsAtosDETERMINACAO: TStringField;
    cdsAtosSELOREGISTRO: TStringField;
    cdsAtosALEATORIOREGISTRO: TStringField;
    cdsAtosDATAREGISTRO: TStringField;
    cdsAtosINDCANCELAMENTO: TStringField;
    cdsAtosOPENTAG: TStringField;
    cdsAtosCLOSETAG: TStringField;
    edData: TsDateEdit;
    ckVisualizar: TsCheckBox;
    btGerar: TsBitBtn;
    ckAlteracao: TsCheckBox;
    cdsAtosCCTPROTOCOLO: TStringField;
    cdsAtosTAGCUSTAS: TBooleanField;
    cdsAtosIDREFERENCIA: TIntegerField;
    DBNavigator1: TDBNavigator;
    qryCancelamento: TFDQuery;
    qryFolhas: TFDQuery;
    qryCancelamentoID_CERTIDAO: TIntegerField;
    qryCancelamentoID_ATO: TIntegerField;
    qryCancelamentoORDEM: TIntegerField;
    qryCancelamentoDT_PEDIDO: TDateField;
    qryCancelamentoDT_ENTREGA: TDateField;
    qryCancelamentoDT_CERTIDAO: TDateField;
    qryCancelamentoTIPO_CERTIDAO: TStringField;
    qryCancelamentoNUMERO_TITULO: TStringField;
    qryCancelamentoANOS: TIntegerField;
    qryCancelamentoCONVENIO: TStringField;
    qryCancelamentoCODIGO: TIntegerField;
    qryCancelamentoSELO: TStringField;
    qryCancelamentoALEATORIO: TStringField;
    qryCancelamentoRECIBO: TIntegerField;
    qryCancelamentoREQUERIDO: TStringField;
    qryCancelamentoTIPO_REQUERIDO: TStringField;
    qryCancelamentoCPF_CNPJ_REQUERIDO: TStringField;
    qryCancelamentoREQUERENTE: TStringField;
    qryCancelamentoCPF_CNPJ_REQUERENTE: TStringField;
    qryCancelamentoTELEFONE_REQUERENTE: TStringField;
    qryCancelamentoRESULTADO: TStringField;
    qryCancelamentoFOLHAS: TIntegerField;
    qryCancelamentoREGISTROS: TIntegerField;
    qryCancelamentoCOBRANCA: TStringField;
    qryCancelamentoEMOLUMENTOS: TFloatField;
    qryCancelamentoFETJ: TFloatField;
    qryCancelamentoFUNDPERJ: TFloatField;
    qryCancelamentoFUNPERJ: TFloatField;
    qryCancelamentoFUNARPEN: TFloatField;
    qryCancelamentoPMCMV: TFloatField;
    qryCancelamentoISS: TFloatField;
    qryCancelamentoMUTUA: TFloatField;
    qryCancelamentoACOTERJ: TFloatField;
    qryCancelamentoDISTRIBUICAO: TFloatField;
    qryCancelamentoAPONTAMENTO: TFloatField;
    qryCancelamentoCAPA: TFloatField;
    qryCancelamentoTOTAL: TFloatField;
    qryCancelamentoEXRECIBO: TIntegerField;
    qryCancelamentoEXCOBRANCA: TStringField;
    qryCancelamentoEXEMOLUMENTOS: TFloatField;
    qryCancelamentoEXFETJ: TFloatField;
    qryCancelamentoEXFUNDPERJ: TFloatField;
    qryCancelamentoEXFUNPERJ: TFloatField;
    qryCancelamentoEXISS: TFloatField;
    qryCancelamentoEXFUNARPEN: TFloatField;
    qryCancelamentoEXTOTAL: TFloatField;
    qryCancelamentoCCT: TStringField;
    qryCancelamentoESCREVENTE: TStringField;
    qryCancelamentoRESPONSAVEL: TStringField;
    qryCancelamentoOBSERVACAO: TMemoField;
    qryCancelamentoENVIADO: TStringField;
    qryCancelamentoENVIADO_FLS: TStringField;
    qryCancelamentoQTD: TIntegerField;
    qryFolhasID_CERTIDAO: TIntegerField;
    qryFolhasDT_CERTIDAO: TDateField;
    qryFolhasDT_ENTREGA: TDateField;
    qryFolhasSELO: TStringField;
    qryFolhasALEATORIO: TStringField;
    qryFolhasFOLHAS: TIntegerField;
    qryFolhasREQUERENTE: TStringField;
    qryFolhasEXEMOLUMENTOS: TFloatField;
    qryFolhasEXFETJ: TFloatField;
    qryFolhasEXFUNDPERJ: TFloatField;
    qryFolhasEXFUNPERJ: TFloatField;
    qryFolhasEXFUNARPEN: TFloatField;
    qryFolhasEXTOTAL: TFloatField;
    qryFolhasCCT: TStringField;
    qryFolhasENVIADO_FLS: TStringField;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GridExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btLocalizarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure GridMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure Marcartodos1Click(Sender: TObject);
    procedure Desmarcartodos1Click(Sender: TObject);
    procedure CorrigirApresentante;
    procedure MarcarTodosEnviado1Click(Sender: TObject);
    procedure DesmarcarTodosXML1Click(Sender: TObject);
    procedure sPanel1MouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure P1MouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure btGerarClick(Sender: TObject);
    procedure GridMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure TagParticipantes;
    procedure TagEmolumentos;
    procedure lbCaminhoClick(Sender: TObject);
    function E(Tamanho: Integer): String;
    procedure edDataKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    F: TextFile;
    vMarcados: Integer;
  end;

var
  FXML2014: TFXML2014;

implementation

uses UDM, UPF, UPrincipal, UGeral, UGDM;

{$R *.dfm}

function TFXml2014.E(Tamanho: Integer): String;
var
  I: Integer;
begin
  Result:='';
  for I:=1 to Tamanho do
    Result:=Result+' ';
end;

procedure TFXML2014.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFXML2014.GridExit(Sender: TObject);
begin
  if cdsAtos.State in [dsEdit,dsInsert] then cdsAtos.Post;
end;

procedure TFXML2014.FormCreate(Sender: TObject);
begin
  dm.Escreventes.Close;
  dm.Escreventes.Open;

  dm.Escreventes.Filtered:=False;
  dm.Escreventes.Filter:='AUTORIZADO='+QuotedStr('S');
  dm.Escreventes.Filtered:=True;

  edData.Date:=Now;

  lbCaminho.Caption:=Gdm.PathMas+'ModuloApoioServico\Caixa de Sa�da\';
end;

procedure TFXML2014.btLocalizarClick(Sender: TObject);
var
  SQL: String;
  Q: TFDQuery;
  vIdReservado: Integer;
begin
  (**if edData.Date<StrToDate('10/03/2014') then
  begin
      GR.Aviso('A DATA DEVE SER A PARTIR DE 10/03/2014'+#13#13+'IN�CIO DO SELO ELETR�NICO');
      Exit;
  end;

  vMarcados:=0;

  cdsAtos.DisableControls;
  cdsAtos.Close;
  cdsAtos.CreateDataSet;
  cdsAtos.Open;
  cdsAtos.EmptyDataSet;

  {APONTADOS}
  SQL:='SELECT ID_ATO,DT_PROTOCOLO,PROTOCOLO,LIVRO_PROTOCOLO,FOLHA_PROTOCOLO,ENVIADO_APONTAMENTO,CCT,TIPO_TITULO,CODIGO,'+
       'NUMERO_TITULO,SALDO_TITULO,STATUS,DT_VENCIMENTO,TIPO_APRESENTACAO,VALOR_TITULO,CONVENIO,APRESENTANTE,CPF_CNPJ_APRESENTANTE,'+
       'CODIGO_APRESENTANTE,TIPO_APRESENTANTE,DEVEDOR,CPF_CNPJ_DEVEDOR,TIPO_DEVEDOR,SACADOR,CPF_CNPJ_SACADOR,'+
       'CEDENTE,COBRANCA,EMOLUMENTOS,FETJ,FUNDPERJ,FUNPERJ,FUNARPEN,PMCMV,ISS,MUTUA,ACOTERJ,DISTRIBUICAO,VALOR_AR,TARIFA_BANCARIA,TOTAL '+
       'FROM TITULOS WHERE DT_PROTOCOLO = '+QuotedStr(PF.FormatarData(edData.Date,'I'))+' AND ANTIGO<>'''+'S'+''' ORDER BY PROTOCOLO';

  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add(SQL);
  Q.Open;
  if not Q.IsEmpty then
  begin
      Q.First;
      while not Q.Eof do
      begin
          cdsAtos.Append;
          cdsAtosOPENTAG.AsString               :='<ProtocolizacaoTitulos';
          cdsAtosCLOSETAG.AsString              :='</ProtocolizacaoTitulos>';
          cdsAtosIDATO.AsInteger                :=Q.FieldByName('ID_ATO').AsInteger;
          cdsAtosSTATUS.AsString                :='APONTADO';
          cdsAtosXML.AsBoolean                  :=False;//Q.FieldByName('ENVIADO_APONTAMENTO').AsString='N';
          cdsAtosENVIADO.AsString               :='N';//Q.FieldByName('ENVIADO_APONTAMENTO').AsString;
          cdsAtosDATAPRATICA.AsString           :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_PROTOCOLO').AsDateTime);
          cdsAtosCCT.AsString                   :=Q.FieldByName('CCT').AsString;
          cdsAtosTIPOTITULO.AsString            :=Q.FieldByName('TIPO_TITULO').AsString;
          cdsAtosNUMEROTITULO.AsString          :=Q.FieldByName('NUMERO_TITULO').AsString;
          cdsAtosVALORTITULO.AsString           :=GR.ValorComPontoXML(Q.FieldByName('VALOR_TITULO').AsFloat);
          cdsAtosSALDO.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('SALDO_TITULO').AsFloat);
          cdsAtosDATAVENCIMENTO.AsString        :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_VENCIMENTO').AsDateTime);
          cdsAtosTIPOAPRESENTACAO.AsString      :=Q.FieldByName('TIPO_APRESENTACAO').AsString;
          cdsAtosNUMEROPROTOCOLO.AsString       :=Q.FieldByName('PROTOCOLO').AsString;
          cdsAtosNUMEROLIVRO.AsString           :=Q.FieldByName('LIVRO_PROTOCOLO').AsString;
          cdsAtosNUMEROFOLHA.AsString           :=Q.FieldByName('FOLHA_PROTOCOLO').AsString;
          cdsAtosNOMECEDENTE.AsString           :=Q.FieldByName('CEDENTE').AsString;
          cdsAtosNOMEAPRESENTANTE.AsString      :=Q.FieldByName('APRESENTANTE').AsString;
          cdsAtosTIPOAPRESENTANTE.AsString      :=Q.FieldByName('TIPO_APRESENTANTE').AsString;
          cdsAtosDOCUMENTOAPRESENTANTE.AsString :=Q.FieldByName('CPF_CNPJ_APRESENTANTE').AsString;
          cdsAtosNOMEDEVEDOR.AsString           :=Q.FieldByName('DEVEDOR').AsString;
          cdsAtosTIPODEVEDOR.AsString           :=Q.FieldByName('TIPO_DEVEDOR').AsString;
          cdsAtosDOCUMENTODEVEDOR.AsString      :=Q.FieldByName('CPF_CNPJ_DEVEDOR').AsString;
          cdsAtosNOMESACADOR.AsString           :=Q.FieldByName('SACADOR').AsString;
          cdsAtosDOCUMENTOSACADOR.AsString      :=Q.FieldByName('CPF_CNPJ_SACADOR').AsString;
          cdsAtosINDCONVENIO.AsString           :=Q.FieldByName('CONVENIO').AsString;
          cdsAtosEMOLUMENTOS.AsString           :=GR.ValorComPontoXML(Q.FieldByName('EMOLUMENTOS').AsFloat);
          cdsAtosFETJ.AsString                  :=GR.ValorComPontoXML(Q.FieldByName('FETJ').AsFloat);
          cdsAtosFUNDPERJ.AsString              :=GR.ValorComPontoXML(Q.FieldByName('FUNDPERJ').AsFloat);
          cdsAtosFUNPERJ.AsString               :=GR.ValorComPontoXML(Q.FieldByName('FUNPERJ').AsFloat);
          cdsAtosFUNARPEN.AsString              :=GR.ValorComPontoXML(Q.FieldByName('FUNARPEN').AsFloat);
          cdsAtosPMCMV.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('PMCMV').AsFloat);
          cdsAtosMUTUA.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('MUTUA').AsFloat);
          cdsAtosACOTERJ.AsString               :=GR.ValorComPontoXML(Q.FieldByName('ACOTERJ').AsFloat);
          cdsAtosDISTRIBUICAO.AsString          :=GR.ValorComPontoXML(Q.FieldByName('DISTRIBUICAO').AsFloat);
          cdsAtosTOTAL.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('EMOLUMENTOS').AsFloat+
                                                                      Q.FieldByName('FETJ').AsFloat+
                                                                      Q.FieldByName('FUNDPERJ').AsFloat+
                                                                      Q.FieldByName('FUNPERJ').AsFloat+
                                                                      Q.FieldByName('FUNARPEN').AsFloat+
                                                                      Q.FieldByName('PMCMV').AsFloat+
                                                                      Q.FieldByName('MUTUA').AsFloat+
                                                                      Q.FieldByName('ACOTERJ').AsFloat+
                                                                      Q.FieldByName('DISTRIBUICAO').AsFloat);

          if dm.ForcaDaLei(Q.FieldByName('CODIGO_APRESENTANTE').AsString) then
          begin
              cdsAtosTIPOCOBRANCA.AsString:='FL';
              cdsAtosTAGCUSTAS.AsBoolean:=False;
          end
          else
            if Q.FieldByName('CONVENIO').AsString='S' then
            begin
                cdsAtosTIPOCOBRANCA.AsString:='CV';
                cdsAtosTAGCUSTAS.AsBoolean:=False;
            end
            else
            begin
                cdsAtosTIPOCOBRANCA.AsString:='CC';
                cdsAtosTAGCUSTAS.AsBoolean:=True;
            end;

          if cdsAtosDOCUMENTOSACADOR.AsString='' then
            cdsAtosTIPOSACADOR.AsString:='F'
              else
                if Length(cdsAtosDOCUMENTOSACADOR.AsString)=11 then
                  cdsAtosTIPOSACADOR.AsString:='F'
                    else cdsAtosTIPOSACADOR.AsString:='J';

          if Q.FieldByName('DT_PROTOCOLO').AsDateTime>=StrToDate('10/03/2014') then
            if (cdsAtosCCT.AsString='') or (cdsAtosCCT.AsString='*') then
            begin
                vIdReservado:=GR.ValorGeneratorDBX('ID_RESERVADO',Gdm.BDGerencial);
                cdsAtosCCT.AsString:=Gdm.ProximoSelo(0,'PROTESTO',Self.Name,'S',cdsAtosNUMEROLIVRO.AsString,
                                                     cdsAtosNUMEROFOLHA.AsInteger,
                                                     cdsAtosNUMEROFOLHA.AsInteger,0,
                                                     'APONTAMENTO',
                                                     dm.vNome,
                                                     Q.FieldByName('DT_PROTOCOLO').AsDateTime,
                                                     vIdReservado);

                Gdm.BaixarSelo(cdsAtosCCT.AsString,vIdReservado,'PROTESTO',Self.Name,cdsAtosIDATO.AsInteger,
                               0,4,Q.FieldByName('DT_PROTOCOLO').AsDateTime,
                               Q.FieldByName('LIVRO_PROTOCOLO').AsString,GR.PegarNumero(Q.FieldByName('FOLHA_PROTOCOLO').AsString),
                               GR.PegarNumero(Q.FieldByName('FOLHA_PROTOCOLO').AsString),0,Q.FieldByName('PROTOCOLO').AsInteger,0,
                               Q.FieldByName('CODIGO').AsInteger,'APONTAMENTO',dm.vNome,Q.FieldByName('CONVENIO').AsString,
                               Q.FieldByName('COBRANCA').AsString,Q.FieldByName('EMOLUMENTOS').AsFloat,Q.FieldByName('FETJ').AsFloat,
                               Q.FieldByName('FUNDPERJ').AsFloat,Q.FieldByName('FUNPERJ').AsFloat,Q.FieldByName('FUNARPEN').AsFloat,
                               Q.FieldByName('PMCMV').AsFloat,Q.FieldByName('MUTUA').AsFloat,Q.FieldByName('ACOTERJ').AsFloat,
                               Q.FieldByName('DISTRIBUICAO').AsFloat,0,0,Q.FieldByName('VALOR_AR').AsFloat,Q.FieldByName('TARIFA_BANCARIA').AsFloat,
                               Q.FieldByName('TOTAL').AsFloat,Q.FieldByName('ISS').AsFloat);

                GR.ExecutarSQLFD('UPDATE TITULOS SET CCT='''+cdsAtosCCT.AsString+'''WHERE ID_ATO='+cdsAtosIDATO.AsString,dm.conSISTEMA);
            end;

          cdsAtos.Post;
          if cdsAtosXML.AsBoolean then Inc(vMarcados);
          Q.Next;
      end;
  end;

  {PROTESTADOS}
  SQL:='SELECT ID_ATO,DT_PROTOCOLO,PROTOCOLO,LIVRO_REGISTRO,FOLHA_REGISTRO,ENVIADO_PROTESTO,CCT,TIPO_TITULO,SELO_REGISTRO,'+
       'NUMERO_TITULO,SALDO_TITULO,STATUS,DT_VENCIMENTO,TIPO_APRESENTACAO,VALOR_TITULO,CONVENIO,COBRANCA,APRESENTANTE,'+
       'EMOLUMENTOS,FETJ,FUNDPERJ,FUNPERJ,FUNARPEN,PMCMV,MUTUA,ACOTERJ,DISTRIBUICAO,TOTAL,DT_REGISTRO,ALEATORIO_PROTESTO '+
       'FROM TITULOS WHERE DT_REGISTRO='+QuotedStr(PF.FormatarData(edData.Date,'I'))+' AND ANTIGO<>'''+'S'+''' ORDER BY PROTOCOLO';

  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add(SQL);
  Q.Open;
  if not Q.IsEmpty then
  begin
      Q.First;
      while not Q.Eof do
      begin
          cdsAtos.Append;
          cdsAtosOPENTAG.AsString               :='<Protesto';
          cdsAtosCLOSETAG.AsString              :='</Protesto>';
          cdsAtosIDATO.AsInteger                :=Q.FieldByName('ID_ATO').AsInteger;
          cdsAtosSTATUS.AsString                :='PROTESTADO';
          cdsAtosXML.AsBoolean                  :=False;//Q.FieldByName('ENVIADO_PROTESTO').AsString='N';
          cdsAtosENVIADO.AsString               :='N';//Q.FieldByName('ENVIADO_PROTESTO').AsString;
          cdsAtosDATAPRATICA.AsString           :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_REGISTRO').AsDateTime);
          cdsAtosDATAPROTOCOLO.AsString         :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_PROTOCOLO').AsDateTime);
          cdsAtosSELO.AsString                  :=Q.FieldByName('SELO_REGISTRO').AsString;
          cdsAtosALEATORIO.AsString             :=Q.FieldByName('ALEATORIO_PROTESTO').AsString;
          cdsAtosCCT.AsString                   :=Q.FieldByName('CCT').AsString;
          cdsAtosTIPOTITULO.AsString            :=Q.FieldByName('TIPO_TITULO').AsString;
          cdsAtosNUMEROTITULO.AsString          :=Q.FieldByName('NUMERO_TITULO').AsString;
          cdsAtosVALORTITULO.AsString           :=GR.ValorComPontoXML(Q.FieldByName('VALOR_TITULO').AsFloat);
          cdsAtosSALDO.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('SALDO_TITULO').AsFloat);
          cdsAtosDATAVENCIMENTO.AsString        :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_VENCIMENTO').AsDateTime);
          cdsAtosTIPOAPRESENTACAO.AsString      :=Q.FieldByName('TIPO_APRESENTACAO').AsString;
          cdsAtosNUMEROPROTOCOLO.AsString       :=Q.FieldByName('PROTOCOLO').AsString;
          cdsAtosNUMEROLIVRO.AsString           :=Q.FieldByName('LIVRO_REGISTRO').AsString;
          cdsAtosNUMEROFOLHA.AsString           :=Q.FieldByName('FOLHA_REGISTRO').AsString;
          cdsAtosNOMEAPRESENTANTE.AsString      :=Q.FieldByName('APRESENTANTE').AsString;
          cdsAtosSELO.AsString                  :=Q.FieldByName('SELO_REGISTRO').AsString;
          cdsAtosINDCONVENIO.AsString           :=Q.FieldByName('CONVENIO').AsString;
          cdsAtosEMOLUMENTOS.AsString           :=GR.ValorComPontoXML(Q.FieldByName('EMOLUMENTOS').AsFloat);
          cdsAtosFETJ.AsString                  :=GR.ValorComPontoXML(Q.FieldByName('FETJ').AsFloat);
          cdsAtosFUNDPERJ.AsString              :=GR.ValorComPontoXML(Q.FieldByName('FUNDPERJ').AsFloat);
          cdsAtosFUNPERJ.AsString               :=GR.ValorComPontoXML(Q.FieldByName('FUNPERJ').AsFloat);
          cdsAtosFUNARPEN.AsString              :=GR.ValorComPontoXML(Q.FieldByName('FUNARPEN').AsFloat);
          cdsAtosPMCMV.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('PMCMV').AsFloat);
          cdsAtosMUTUA.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('MUTUA').AsFloat);
          cdsAtosACOTERJ.AsString               :=GR.ValorComPontoXML(Q.FieldByName('ACOTERJ').AsFloat);
          cdsAtosDISTRIBUICAO.AsString          :=GR.ValorComPontoXML(Q.FieldByName('DISTRIBUICAO').AsFloat);
          cdsAtosTOTAL.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('EMOLUMENTOS').AsFloat+
                                                                      Q.FieldByName('FETJ').AsFloat+
                                                                      Q.FieldByName('FUNDPERJ').AsFloat+
                                                                      Q.FieldByName('FUNPERJ').AsFloat+
                                                                      Q.FieldByName('FUNARPEN').AsFloat+
                                                                      Q.FieldByName('PMCMV').AsFloat+
                                                                      Q.FieldByName('MUTUA').AsFloat+
                                                                      Q.FieldByName('ACOTERJ').AsFloat+
                                                                      Q.FieldByName('DISTRIBUICAO').AsFloat);

          if Q.FieldByName('CONVENIO').AsString='S' then
          begin
              cdsAtosTIPOCOBRANCA.AsString:='CV';
              cdsAtosTAGCUSTAS.AsBoolean:=False;
          end
          else
          begin
              cdsAtosTIPOCOBRANCA.AsString:='PZ';
              cdsAtosTAGCUSTAS.AsBoolean:=False;
          end;

          cdsAtos.Post;
          if cdsAtosXML.AsBoolean then Inc(vMarcados);
          Q.Next;
      end;
  end;

  {EMISS�O DE GUIAS}
  SQL:='SELECT B.ID_BOLETO,B.ID_ATO,B.DATA_BAIXA,B.CCT,B.NUMERO_DOCUMENTO,B.PROTOCOLO,B.DATA_PROTOCOLO,B.EMOLUMENTOS,B.FETJ,B.FUNDPERJ,B.FUNPERJ,'+
       'B.FUNARPEN,B.PMCMV,B.TOTAL,B.ENVIADO,T.CCT AS CCTPROTOCOLO,T.APRESENTANTE,T.LIVRO_PROTOCOLO,T.FOLHA_PROTOCOLO,T.CONVENIO '+
       'FROM BOLETOS B INNER JOIN TITULOS T ON B.ID_ATO=T.ID_ATO WHERE '+
       'PAGO='''+'S'+''' AND DATA_BAIXA='+QuotedStr(PF.FormatarData(edData.Date,'I'))+'ORDER BY ID_BOLETO';

  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add(SQL);
  Q.Open;
  if not Q.IsEmpty then
  begin
      Q.First;
      while not Q.Eof do
      begin
          cdsAtos.Append;
          cdsAtosOPENTAG.AsString               :='<EmissaoGuiaComunicacao';
          cdsAtosCLOSETAG.AsString              :='</EmissaoGuiaComunicacao>';
          cdsAtosIDATO.AsInteger                :=Q.FieldByName('ID_BOLETO').AsInteger;
          cdsAtosSTATUS.AsString                :='EMISS�O DE GUIA';
          cdsAtosXML.AsBoolean                  :=False;//Q.FieldByName('ENVIADO').AsString='N';
          cdsAtosENVIADO.AsString               :='N';//Q.FieldByName('ENVIADO').AsString;
          cdsAtosDATAPRATICA.AsString           :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DATA_BAIXA').AsDateTime);
          cdsAtosDATAPROTOCOLO.AsString         :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DATA_PROTOCOLO').AsDateTime);
          cdsAtosCCT.AsString                   :=Q.FieldByName('CCT').AsString;
          cdsAtosCCTPROTOCOLO.AsString          :=Q.FieldByName('CCTPROTOCOLO').AsString;
          cdsAtosNUMEROPROTOCOLO.AsString       :=Q.FieldByName('PROTOCOLO').AsString;
          cdsAtosNUMEROLIVRO.AsString           :=Q.FieldByName('LIVRO_PROTOCOLO').AsString;
          cdsAtosNUMEROFOLHA.AsString           :=Q.FieldByName('FOLHA_PROTOCOLO').AsString;
          cdsAtosNOMEAPRESENTANTE.AsString      :=Q.FieldByName('APRESENTANTE').AsString;
          cdsAtosEMOLUMENTOS.AsString           :=GR.ValorComPontoXML(Q.FieldByName('EMOLUMENTOS').AsFloat);
          cdsAtosFETJ.AsString                  :=GR.ValorComPontoXML(Q.FieldByName('FETJ').AsFloat);
          cdsAtosFUNDPERJ.AsString              :=GR.ValorComPontoXML(Q.FieldByName('FUNDPERJ').AsFloat);
          cdsAtosFUNPERJ.AsString               :=GR.ValorComPontoXML(Q.FieldByName('FUNPERJ').AsFloat);
          cdsAtosFUNARPEN.AsString              :=GR.ValorComPontoXML(Q.FieldByName('FUNARPEN').AsFloat);
          cdsAtosPMCMV.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('PMCMV').AsFloat);
          cdsAtosTOTAL.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('EMOLUMENTOS').AsFloat+
                                                                      Q.FieldByName('FETJ').AsFloat+
                                                                      Q.FieldByName('FUNDPERJ').AsFloat+
                                                                      Q.FieldByName('FUNPERJ').AsFloat+
                                                                      Q.FieldByName('FUNARPEN').AsFloat+
                                                                      Q.FieldByName('PMCMV').AsFloat);

          if Q.FieldByName('CONVENIO').AsString='S' then
          begin
              cdsAtosTIPOCOBRANCA.AsString:='PC';
              cdsAtosTAGCUSTAS.AsBoolean:=True;
          end
          else
          begin
              cdsAtosTIPOCOBRANCA.AsString:='CC';
              cdsAtosTAGCUSTAS.AsBoolean:=True;
          end;

          if (cdsAtosCCT.AsString='') or (cdsAtosCCT.AsString='*') then
          begin
              vIdReservado:=GR.ValorGeneratorDBX('ID_RESERVADO',Gdm.BDGerencial);
              cdsAtosCCT.AsString:=Gdm.ProximoSelo(0,'PROTESTO',Self.Name,'S',cdsAtosNUMEROLIVRO.AsString,
                                                   cdsAtosNUMEROFOLHA.AsInteger,
                                                   cdsAtosNUMEROFOLHA.AsInteger,0,
                                                   'EMISS�O DE GUIA',
                                                   dm.vNome,
                                                   Q.FieldByName('DATA_EMISSAO').AsDateTime,
                                                   vIdReservado);

              Gdm.BaixarSelo(cdsAtosCCT.AsString,vIdReservado,'PROTESTO',Self.Name,cdsAtosIDATO.AsInteger,Q.FieldByName('ID_ATO').AsInteger,4,
                             Q.FieldByName('DATA_BAIXA').AsDateTime,Q.FieldByName('LIVRO_PROTOCOLO').AsString,
                             GR.PegarNumero(Q.FieldByName('FOLHA_PROTOCOLO').AsString),GR.PegarNumero(Q.FieldByName('FOLHA_PROTOCOLO').AsString),
                             0,Q.FieldByName('PROTOCOLO').AsInteger,0,4028,'EMISS�O DE GUIA',dm.vNome,
                             Q.FieldByName('CONVENIO').AsString,'CC',Q.FieldByName('EMOLUMENTOS').AsFloat,
                             Q.FieldByName('FETJ').AsFloat,Q.FieldByName('FUNDPERJ').AsFloat,Q.FieldByName('FUNPERJ').AsFloat,
                             Q.FieldByName('FUNARPEN').AsFloat,Q.FieldByName('PMCMV').AsFloat,0,0,0,0,0,0,0,Q.FieldByName('TOTAL').AsFloat,0{ISS});

              GR.ExecutarSQLFD('UPDATE BOLETOS SET CCT='''+cdsAtosCCT.AsString+'''WHERE ID_BOLETO='+cdsAtosIDATO.AsString,dm.conSISTEMA);
          end;

          cdsAtos.Post;
          if cdsAtosXML.AsBoolean then Inc(vMarcados);
          Q.Next;
      end;
  end;

  {PAGOS}
  SQL:='SELECT ID_ATO,PROTOCOLO,LIVRO_PROTOCOLO,DT_PROTOCOLO,FOLHA_PROTOCOLO,DT_PAGAMENTO,SELO_PAGAMENTO,FORMA_PAGAMENTO,'+
       'ENVIADO_PAGAMENTO,TIPO_TITULO,NUMERO_TITULO,SALDO_TITULO,STATUS,DT_VENCIMENTO,TIPO_APRESENTACAO,VALOR_TITULO,CCT,'+
       'CONVENIO,COBRANCA,EMOLUMENTOS,FETJ,FUNDPERJ,FUNPERJ,FUNARPEN,PMCMV,MUTUA,ACOTERJ,DISTRIBUICAO,TOTAL,NUMERO_PAGAMENTO,'+
       'ALEATORIO_SOLUCAO,APRESENTANTE,CODIGO_APRESENTANTE FROM TITULOS WHERE DT_PAGAMENTO = '+
       QuotedStr(PF.FormatarData(edData.Date,'I'))+'AND STATUS='''+'PAGO'+'''ORDER BY PROTOCOLO';

  Q.Close;
  Q.SQL.Clear;
  Q.SQL.Add(SQL);
  Q.Open;
  if not Q.IsEmpty then
  begin
      Q.First;
      while not Q.Eof do
      begin
          cdsAtos.Append;
          cdsAtosOPENTAG.AsString               :='<Liquidacao';
          cdsAtosCLOSETAG.AsString              :='</Liquidacao>';
          cdsAtosIDATO.AsInteger                :=Q.FieldByName('ID_ATO').AsInteger;
          cdsAtosSTATUS.AsString                :='PAGO';
          cdsAtosXML.AsBoolean                  :=False;//Q.FieldByName('ENVIADO_PAGAMENTO').AsString='N';
          cdsAtosENVIADO.AsString               :='N';//Q.FieldByName('ENVIADO_PAGAMENTO').AsString;
          cdsAtosDATAPRATICA.AsString           :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_PAGAMENTO').AsDateTime);
          cdsAtosDATAPROTOCOLO.AsString         :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_PROTOCOLO').AsDateTime);
          cdsAtosSELO.AsString                  :=Q.FieldByName('SELO_PAGAMENTO').AsString;
          cdsAtosALEATORIO.AsString             :=Q.FieldByName('ALEATORIO_SOLUCAO').AsString;
          cdsAtosCCT.AsString                   :=Q.FieldByName('CCT').AsString;
          cdsAtosTIPOTITULO.AsString            :=Q.FieldByName('TIPO_TITULO').AsString;
          cdsAtosNUMEROTITULO.AsString          :=Q.FieldByName('NUMERO_TITULO').AsString;
          cdsAtosVALORTITULO.AsString           :=GR.ValorComPontoXML(Q.FieldByName('VALOR_TITULO').AsFloat);
          cdsAtosSALDO.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('SALDO_TITULO').AsFloat);
          cdsAtosDATAVENCIMENTO.AsString        :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_VENCIMENTO').AsDateTime);
          cdsAtosTIPOAPRESENTACAO.AsString      :=Q.FieldByName('TIPO_APRESENTACAO').AsString;
          cdsAtosNUMEROPROTOCOLO.AsString       :=Q.FieldByName('PROTOCOLO').AsString;
          cdsAtosNUMEROLIVRO.AsString           :=Q.FieldByName('LIVRO_PROTOCOLO').AsString;
          cdsAtosNUMEROFOLHA.AsString           :=Q.FieldByName('FOLHA_PROTOCOLO').AsString;
          cdsAtosNOMEAPRESENTANTE.AsString      :=Q.FieldByName('APRESENTANTE').AsString;
          cdsAtosINDCONVENIO.AsString           :=Q.FieldByName('CONVENIO').AsString;
          cdsAtosEMOLUMENTOS.AsString           :=GR.ValorComPontoXML(Q.FieldByName('EMOLUMENTOS').AsFloat);
          cdsAtosFETJ.AsString                  :=GR.ValorComPontoXML(Q.FieldByName('FETJ').AsFloat);
          cdsAtosFUNDPERJ.AsString              :=GR.ValorComPontoXML(Q.FieldByName('FUNDPERJ').AsFloat);
          cdsAtosFUNPERJ.AsString               :=GR.ValorComPontoXML(Q.FieldByName('FUNPERJ').AsFloat);
          cdsAtosFUNARPEN.AsString              :=GR.ValorComPontoXML(Q.FieldByName('FUNARPEN').AsFloat);
          cdsAtosPMCMV.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('PMCMV').AsFloat);
          cdsAtosMUTUA.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('MUTUA').AsFloat);
          cdsAtosACOTERJ.AsString               :=GR.ValorComPontoXML(Q.FieldByName('ACOTERJ').AsFloat);
          cdsAtosDISTRIBUICAO.AsString          :=GR.ValorComPontoXML(Q.FieldByName('DISTRIBUICAO').AsFloat);
          cdsAtosTOTAL.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('EMOLUMENTOS').AsFloat+
                                                                      Q.FieldByName('FETJ').AsFloat+
                                                                      Q.FieldByName('FUNDPERJ').AsFloat+
                                                                      Q.FieldByName('FUNPERJ').AsFloat+
                                                                      Q.FieldByName('FUNARPEN').AsFloat+
                                                                      Q.FieldByName('PMCMV').AsFloat+
                                                                      Q.FieldByName('MUTUA').AsFloat+
                                                                      Q.FieldByName('ACOTERJ').AsFloat+
                                                                      Q.FieldByName('DISTRIBUICAO').AsFloat);

          if dm.ForcaDaLei(Q.FieldByName('CODIGO_APRESENTANTE').AsString) then
          begin
              cdsAtosTIPOCOBRANCA.AsString:='PL';
              cdsAtosTAGCUSTAS.AsBoolean:=True;
          end
          else
            if Q.FieldByName('CONVENIO').AsString='S' then
            begin
                cdsAtosTIPOCOBRANCA.AsString:='PC';
                cdsAtosTAGCUSTAS.AsBoolean:=True;
            end
            else
            begin
                {SENTEN�A JUDICIAL}
                if cdsAtosTIPOTITULO.AsString='35' then
                begin
                    cdsAtosTIPOCOBRANCA.AsString:='PS';
                    cdsAtosTAGCUSTAS.AsBoolean:=True;
                end
                else
                begin
                    cdsAtosTIPOCOBRANCA.AsString:='PZ';
                    cdsAtosTAGCUSTAS.AsBoolean:=False;
                end;
            end;

          if Q.FieldByName('FORMA_PAGAMENTO').AsString='DH' then
            cdsAtosFORMAPAGAMENTO.AsString:='2'
              else
                if Q.FieldByName('FORMA_PAGAMENTO').AsString='CH' then
                  cdsAtosFORMAPAGAMENTO.AsString:='1'
                    else
                      if Q.FieldByName('FORMA_PAGAMENTO').AsString='BL' then
                        cdsAtosFORMAPAGAMENTO.AsString:='2'
                          else
                            if Q.FieldByName('FORMA_PAGAMENTO').AsString='DP' then
                              cdsAtosFORMAPAGAMENTO.AsString:='2'
                                else cdsAtosFORMAPAGAMENTO.AsString:='2';
          cdsAtos.Post;
          if cdsAtosXML.AsBoolean then Inc(vMarcados);
          Q.Next;
      end;
  end;

  {SUSTADOS}
  SQL:='SELECT ID_ATO,PROTOCOLO,LIVRO_PROTOCOLO,DT_PROTOCOLO,FOLHA_PROTOCOLO,DT_SUSTADO,SELO_PAGAMENTO,FORMA_PAGAMENTO,'+
       'ENVIADO_SUSTADO,TIPO_TITULO,NUMERO_TITULO,SALDO_TITULO,STATUS,DT_VENCIMENTO,TIPO_APRESENTACAO,VALOR_TITULO,CCT,'+
       'CONVENIO,COBRANCA,EMOLUMENTOS,FETJ,FUNDPERJ,FUNPERJ,FUNARPEN,PMCMV,MUTUA,ACOTERJ,DISTRIBUICAO,TOTAL,NUMERO_PAGAMENTO,'+
       'ALEATORIO_SOLUCAO,DETERMINACAO,APRESENTANTE,DT_DEFINITIVA FROM TITULOS WHERE (DT_DEFINITIVA = '+
       QuotedStr(PF.FormatarData(edData.Date,'I'))+') AND STATUS='''+'SUSTADO'+'''ORDER BY PROTOCOLO';

  Q.Close;
  Q.SQL.Clear;
  Q.SQL.Add(SQL);
  Q.Open;
  if not Q.IsEmpty then
  begin
      Q.First;
      while not Q.Eof do
      begin
          cdsAtos.Append;
          cdsAtosOPENTAG.AsString               :='<Sustacao';
          cdsAtosCLOSETAG.AsString              :='</Sustacao>';
          cdsAtosIDATO.AsInteger                :=Q.FieldByName('ID_ATO').AsInteger;
          cdsAtosSTATUS.AsString                :='SUSTADO';
          cdsAtosXML.AsBoolean                  :=False;//Q.FieldByName('ENVIADO_SUSTADO').AsString='N';
          cdsAtosENVIADO.AsString               :='N';//Q.FieldByName('ENVIADO_SUSTADO').AsString;

          if Q.FieldByName('DT_SUSTADO').AsDateTime<>0 then
            cdsAtosDATAPRATICA.AsString:=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_SUSTADO').AsDateTime)
              else cdsAtosDATAPRATICA.AsString:=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_DEFINITIVA').AsDateTime);

          cdsAtosDATAPROTOCOLO.AsString         :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_PROTOCOLO').AsDateTime);
          cdsAtosSELO.AsString                  :=Q.FieldByName('SELO_PAGAMENTO').AsString;
          cdsAtosALEATORIO.AsString             :=Q.FieldByName('ALEATORIO_SOLUCAO').AsString;
          cdsAtosCCT.AsString                   :=Q.FieldByName('CCT').AsString;
          cdsAtosTIPOTITULO.AsString            :=Q.FieldByName('TIPO_TITULO').AsString;
          cdsAtosNUMEROTITULO.AsString          :=Q.FieldByName('NUMERO_TITULO').AsString;
          cdsAtosVALORTITULO.AsString           :=GR.ValorComPontoXML(Q.FieldByName('VALOR_TITULO').AsFloat);
          cdsAtosSALDO.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('SALDO_TITULO').AsFloat);
          cdsAtosDATAVENCIMENTO.AsString        :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_VENCIMENTO').AsDateTime);
          cdsAtosTIPOAPRESENTACAO.AsString      :=Q.FieldByName('TIPO_APRESENTACAO').AsString;
          cdsAtosNUMEROPROTOCOLO.AsString       :=Q.FieldByName('PROTOCOLO').AsString;
          cdsAtosNUMEROLIVRO.AsString           :=Q.FieldByName('LIVRO_PROTOCOLO').AsString;
          cdsAtosNUMEROFOLHA.AsString           :=Q.FieldByName('FOLHA_PROTOCOLO').AsString;
          cdsAtosDETERMINACAO.AsString          :=Q.FieldByName('DETERMINACAO').AsString;
          cdsAtosNOMEAPRESENTANTE.AsString      :=Q.FieldByName('APRESENTANTE').AsString;
          cdsAtosINDCONVENIO.AsString           :=Q.FieldByName('CONVENIO').AsString;
          cdsAtosTIPOCOBRANCA.AsString          :='SC';
          cdsAtosTAGCUSTAS.AsBoolean            :=False;

          if Q.FieldByName('FORMA_PAGAMENTO').AsString='DH' then
            cdsAtosFORMAPAGAMENTO.AsString:='2'
              else
                if Q.FieldByName('FORMA_PAGAMENTO').AsString='CH' then
                  cdsAtosFORMAPAGAMENTO.AsString:='1'
                    else
                      if Q.FieldByName('FORMA_PAGAMENTO').AsString='BL' then
                        cdsAtosFORMAPAGAMENTO.AsString:='2'
                          else
                            if Q.FieldByName('FORMA_PAGAMENTO').AsString='DP' then
                              cdsAtosFORMAPAGAMENTO.AsString:='2';

          cdsAtos.Post;
          if cdsAtosXML.AsBoolean then Inc(vMarcados);
          Q.Next;
      end;
  end;

  {DESIST�NCIA=RETIRADOS}
  SQL:='SELECT ID_ATO,PROTOCOLO,LIVRO_PROTOCOLO,DT_PROTOCOLO,FOLHA_PROTOCOLO,DT_SUSTADO,SELO_PAGAMENTO,FORMA_PAGAMENTO,'+
       'ENVIADO_RETIRADO,TIPO_TITULO,NUMERO_TITULO,SALDO_TITULO,STATUS,DT_VENCIMENTO,TIPO_APRESENTACAO,VALOR_TITULO,CCT,'+
       'CONVENIO,COBRANCA,EMOLUMENTOS,FETJ,FUNDPERJ,FUNPERJ,FUNARPEN,PMCMV,MUTUA,ACOTERJ,DISTRIBUICAO,TOTAL,'+
       'ALEATORIO_SOLUCAO,APRESENTANTE,CODIGO_APRESENTANTE,DT_RETIRADO FROM TITULOS WHERE (DT_RETIRADO = '+
       QuotedStr(PF.FormatarData(edData.Date,'I'))+') AND STATUS='''+'RETIRADO'+'''ORDER BY PROTOCOLO';

  Q.Close;
  Q.SQL.Clear;
  Q.SQL.Add(SQL);
  Q.Open;
  if not Q.IsEmpty then
  begin
      Q.First;
      while not Q.Eof do
      begin
          cdsAtos.Append;
          cdsAtosOPENTAG.AsString               :='<Desistencia';
          cdsAtosCLOSETAG.AsString              :='</Desistencia>';
          cdsAtosIDATO.AsInteger                :=Q.FieldByName('ID_ATO').AsInteger;
          cdsAtosSTATUS.AsString                :='RETIRADO';
          cdsAtosXML.AsBoolean                  :=False;//Q.FieldByName('ENVIADO_RETIRADO').AsString='N';
          cdsAtosENVIADO.AsString               :='N';//Q.FieldByName('ENVIADO_RETIRADO').AsString;
          cdsAtosDATAPRATICA.AsString           :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_RETIRADO').AsDateTime);
          cdsAtosDATAPROTOCOLO.AsString         :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_PROTOCOLO').AsDateTime);
          cdsAtosSELO.AsString                  :=Q.FieldByName('SELO_PAGAMENTO').AsString;
          cdsAtosALEATORIO.AsString             :=Q.FieldByName('ALEATORIO_SOLUCAO').AsString;
          cdsAtosCCT.AsString                   :=Q.FieldByName('CCT').AsString;
          cdsAtosTIPOTITULO.AsString            :=Q.FieldByName('TIPO_TITULO').AsString;
          cdsAtosNUMEROTITULO.AsString          :=Q.FieldByName('NUMERO_TITULO').AsString;
          cdsAtosVALORTITULO.AsString           :=GR.ValorComPontoXML(Q.FieldByName('VALOR_TITULO').AsFloat);
          cdsAtosSALDO.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('SALDO_TITULO').AsFloat);
          cdsAtosDATAVENCIMENTO.AsString        :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_VENCIMENTO').AsDateTime);
          cdsAtosTIPOAPRESENTACAO.AsString      :=Q.FieldByName('TIPO_APRESENTACAO').AsString;
          cdsAtosNUMEROPROTOCOLO.AsString       :=Q.FieldByName('PROTOCOLO').AsString;
          cdsAtosNUMEROLIVRO.AsString           :=Q.FieldByName('LIVRO_PROTOCOLO').AsString;
          cdsAtosNUMEROFOLHA.AsString           :=Q.FieldByName('FOLHA_PROTOCOLO').AsString;
          cdsAtosNOMEAPRESENTANTE.AsString      :=Q.FieldByName('APRESENTANTE').AsString;
          cdsAtosINDCONVENIO.AsString           :=Q.FieldByName('CONVENIO').AsString;
          cdsAtosEMOLUMENTOS.AsString           :=GR.ValorComPontoXML(Q.FieldByName('EMOLUMENTOS').AsFloat);
          cdsAtosFETJ.AsString                  :=GR.ValorComPontoXML(Q.FieldByName('FETJ').AsFloat);
          cdsAtosFUNDPERJ.AsString              :=GR.ValorComPontoXML(Q.FieldByName('FUNDPERJ').AsFloat);
          cdsAtosFUNPERJ.AsString               :=GR.ValorComPontoXML(Q.FieldByName('FUNPERJ').AsFloat);
          cdsAtosFUNARPEN.AsString              :=GR.ValorComPontoXML(Q.FieldByName('FUNARPEN').AsFloat);
          cdsAtosPMCMV.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('PMCMV').AsFloat);
          cdsAtosMUTUA.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('MUTUA').AsFloat);
          cdsAtosACOTERJ.AsString               :=GR.ValorComPontoXML(Q.FieldByName('ACOTERJ').AsFloat);
          cdsAtosDISTRIBUICAO.AsString          :=GR.ValorComPontoXML(Q.FieldByName('DISTRIBUICAO').AsFloat);
          cdsAtosTOTAL.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('EMOLUMENTOS').AsFloat+
                                                                      Q.FieldByName('FETJ').AsFloat+
                                                                      Q.FieldByName('FUNDPERJ').AsFloat+
                                                                      Q.FieldByName('FUNPERJ').AsFloat+
                                                                      Q.FieldByName('FUNARPEN').AsFloat+
                                                                      Q.FieldByName('PMCMV').AsFloat+
                                                                      Q.FieldByName('MUTUA').AsFloat+
                                                                      Q.FieldByName('ACOTERJ').AsFloat+
                                                                      Q.FieldByName('DISTRIBUICAO').AsFloat);

          if dm.ForcaDaLei(Q.FieldByName('DISTRIBUICAO').AsString) then
          begin
              cdsAtosTIPOCOBRANCA.AsString:='FL';
              cdsAtosTAGCUSTAS.AsBoolean:=False;
          end
          else
            if Q.FieldByName('CONVENIO').AsString='S' then
            begin
                cdsAtosTIPOCOBRANCA.AsString:='PC';
                cdsAtosTAGCUSTAS.AsBoolean:=True;
            end
            else
            begin
                cdsAtosTIPOCOBRANCA.AsString:='PZ';
                cdsAtosTAGCUSTAS.AsBoolean:=False;
            end;

          if Q.FieldByName('FORMA_PAGAMENTO').AsString='DH' then
            cdsAtosFORMAPAGAMENTO.AsString:='2'
              else
                if Q.FieldByName('FORMA_PAGAMENTO').AsString='CH' then
                  cdsAtosFORMAPAGAMENTO.AsString:='1'
                    else
                      if Q.FieldByName('FORMA_PAGAMENTO').AsString='BL' then
                        cdsAtosFORMAPAGAMENTO.AsString:='2'
                          else
                            if Q.FieldByName('FORMA_PAGAMENTO').AsString='DP' then
                              cdsAtosFORMAPAGAMENTO.AsString:='2';

          cdsAtos.Post;
          if cdsAtosXML.AsBoolean then Inc(vMarcados);
          Q.Next;
      end;
  end;

  {CANCELADOS}
  SQL:='SELECT ID_ATO,DT_PROTOCOLO,PROTOCOLO,LIVRO_REGISTRO,FOLHA_REGISTRO,ENVIADO_PROTESTO,CCT,TIPO_TITULO,SELO_REGISTRO,'+
       'NUMERO_TITULO,SALDO_TITULO,STATUS,DT_VENCIMENTO,TIPO_APRESENTACAO,VALOR_TITULO,CONVENIO,COBRANCA,APRESENTANTE,'+
       'EMOLUMENTOS,FETJ,FUNDPERJ,FUNPERJ,FUNARPEN,PMCMV,MUTUA,ACOTERJ,DISTRIBUICAO,TOTAL,DT_REGISTRO,ALEATORIO_PROTESTO,'+
       'SELO_PAGAMENTO,ALEATORIO_SOLUCAO,ENVIADO_PAGAMENTO,DT_PAGAMENTO FROM TITULOS WHERE DT_PAGAMENTO = '+
       QuotedStr(PF.FormatarData(edData.Date,'I'))+' AND STATUS='''+'CANCELADO'+'''ORDER BY PROTOCOLO';

  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add(SQL);
  Q.Open;
  if not Q.IsEmpty then
  begin
      Q.First;
      while not Q.Eof do
      begin
          qryCancelamento.Close;
          qryCancelamento.Params[0].AsString:='X';
          qryCancelamento.Params[1].AsInteger:=Q.FieldByName('ID_ATO').AsInteger;
          qryCancelamento.Open;

          cdsAtos.Append;
          cdsAtosOPENTAG.AsString               :='<Cancelamento';
          cdsAtosCLOSETAG.AsString              :='</Cancelamento>';
          cdsAtosIDATO.AsInteger                :=Q.FieldByName('ID_ATO').AsInteger;
          cdsAtosSTATUS.AsString                :='CANCELADO';
          cdsAtosXML.AsBoolean                  :=False;//Q.FieldByName('ENVIADO_PAGAMENTO').AsString='N';
          cdsAtosENVIADO.AsString               :='N';//Q.FieldByName('ENVIADO_PAGAMENTO').AsString;
          cdsAtosDATAPRATICA.AsString           :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_PAGAMENTO').AsDateTime);
          cdsAtosDATAREGISTRO.AsString          :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_REGISTRO').AsDateTime);
          cdsAtosSELO.AsString                  :=Q.FieldByName('SELO_PAGAMENTO').AsString;
          cdsAtosALEATORIO.AsString             :=Q.FieldByName('ALEATORIO_SOLUCAO').AsString;
          cdsAtosSELOREGISTRO.AsString          :=Q.FieldByName('SELO_REGISTRO').AsString;
          cdsAtosALEATORIOREGISTRO.AsString     :=Q.FieldByName('ALEATORIO_SOLUCAO').AsString;
          cdsAtosCCT.AsString                   :=Q.FieldByName('CCT').AsString;
          cdsAtosTIPOTITULO.AsString            :=Q.FieldByName('TIPO_TITULO').AsString;
          cdsAtosNUMEROTITULO.AsString          :=Q.FieldByName('NUMERO_TITULO').AsString;
          cdsAtosVALORTITULO.AsString           :=GR.ValorComPontoXML(Q.FieldByName('VALOR_TITULO').AsFloat);
          cdsAtosSALDO.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('SALDO_TITULO').AsFloat);
          cdsAtosDATAVENCIMENTO.AsString        :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_VENCIMENTO').AsDateTime);
          cdsAtosTIPOAPRESENTACAO.AsString      :=Q.FieldByName('TIPO_APRESENTACAO').AsString;
          cdsAtosNUMEROPROTOCOLO.AsString       :=Q.FieldByName('PROTOCOLO').AsString;
          cdsAtosNUMEROLIVRO.AsString           :=Q.FieldByName('LIVRO_REGISTRO').AsString;
          cdsAtosNUMEROFOLHA.AsString           :=Q.FieldByName('FOLHA_REGISTRO').AsString;
          cdsAtosNOMEAPRESENTANTE.AsString      :=Q.FieldByName('APRESENTANTE').AsString;
          cdsAtosINDCONVENIO.AsString           :=Q.FieldByName('CONVENIO').AsString;
          cdsAtosIDREFERENCIA.AsInteger         :=qryCancelamentoID_CERTIDAO.AsInteger;

          if Q.FieldByName('CONVENIO').AsString='S' then
          begin
              if (qryCancelamentoCOBRANCA.AsString='CC') or (qryCancelamentoCOBRANCA.AsString='NH') then
              begin
                  cdsAtosTIPOCOBRANCA.AsString    :='PC';
                  cdsAtosINDCANCELAMENTO.AsString :='1';
                  cdsAtosTAGCUSTAS.AsBoolean      :=True;

                  {CUSTAS DO APONTAMENTO + CUSTAS DO CANCELAMENTO}
                  cdsAtosEMOLUMENTOS.AsString     :=GR.ValorComPontoXML(Q.FieldByName('EMOLUMENTOS').AsFloat+qryCancelamentoEMOLUMENTOS.AsFloat);
                  cdsAtosFETJ.AsString            :=GR.ValorComPontoXML(Q.FieldByName('FETJ').AsFloat+qryCancelamentoFETJ.AsFloat);
                  cdsAtosFUNDPERJ.AsString        :=GR.ValorComPontoXML(Q.FieldByName('FUNDPERJ').AsFloat+qryCancelamentoFUNDPERJ.AsFloat);
                  cdsAtosFUNPERJ.AsString         :=GR.ValorComPontoXML(Q.FieldByName('FUNPERJ').AsFloat+qryCancelamentoFUNPERJ.AsFloat);
                  cdsAtosFUNARPEN.AsString        :=GR.ValorComPontoXML(Q.FieldByName('FUNARPEN').AsFloat+qryCancelamentoFUNARPEN.AsFloat);
                  cdsAtosPMCMV.AsString           :=GR.ValorComPontoXML(Q.FieldByName('PMCMV').AsFloat+qryCancelamentoPMCMV.AsFloat);
                  cdsAtosMUTUA.AsString           :=GR.ValorComPontoXML(Q.FieldByName('MUTUA').AsFloat+qryCancelamentoMUTUA.AsFloat);
                  cdsAtosACOTERJ.AsString         :=GR.ValorComPontoXML(Q.FieldByName('ACOTERJ').AsFloat+qryCancelamentoACOTERJ.AsFloat);
                  cdsAtosDISTRIBUICAO.AsString    :=GR.ValorComPontoXML(Q.FieldByName('DISTRIBUICAO').AsFloat+qryCancelamentoDISTRIBUICAO.AsFloat);
                  cdsAtosTOTAL.AsString           :=GR.ValorComPontoXML(Q.FieldByName('TOTAL').AsFloat+qryCancelamentoTOTAL.AsFloat);
              end
              else
              begin
                  cdsAtosTIPOCOBRANCA.AsString    :=qryCancelamentoCOBRANCA.AsString;
                  cdsAtosINDCANCELAMENTO.AsString :='1';
                  cdsAtosTAGCUSTAS.AsBoolean      :=False;
              end;
          end
          else
          begin
              cdsAtosTIPOCOBRANCA.AsString:=qryCancelamentoCOBRANCA.AsString;
              cdsAtosINDCANCELAMENTO.AsString:='2';

              if (cdsAtosTIPOCOBRANCA.AsString='CC') or (cdsAtosTIPOCOBRANCA.AsString='NH') then
                cdsAtosTAGCUSTAS.AsBoolean:=True
                  else cdsAtosTAGCUSTAS.AsBoolean:=False;

              {CUSTAS DO CANCELAMENTO}
              cdsAtosEMOLUMENTOS.AsString      :=GR.ValorComPontoXML(qryCancelamentoEMOLUMENTOS.AsFloat);
              cdsAtosFETJ.AsString             :=GR.ValorComPontoXML(qryCancelamentoFETJ.AsFloat);
              cdsAtosFUNDPERJ.AsString         :=GR.ValorComPontoXML(qryCancelamentoFUNDPERJ.AsFloat);
              cdsAtosFUNPERJ.AsString          :=GR.ValorComPontoXML(qryCancelamentoFUNPERJ.AsFloat);
              cdsAtosFUNARPEN.AsString         :=GR.ValorComPontoXML(qryCancelamentoFUNARPEN.AsFloat);
              cdsAtosPMCMV.AsString            :=GR.ValorComPontoXML(qryCancelamentoPMCMV.AsFloat);
              cdsAtosMUTUA.AsString            :=GR.ValorComPontoXML(qryCancelamentoMUTUA.AsFloat);
              cdsAtosACOTERJ.AsString          :=GR.ValorComPontoXML(qryCancelamentoACOTERJ.AsFloat);
              cdsAtosDISTRIBUICAO.AsString     :=GR.ValorComPontoXML(qryCancelamentoDISTRIBUICAO.AsFloat);
              cdsAtosTOTAL.AsString            :=GR.ValorComPontoXML(qryCancelamentoTOTAL.AsFloat);
          end;

          cdsAtos.Post;
          if cdsAtosXML.AsBoolean then Inc(vMarcados);
          Q.Next;
      end;
  end;

  {CERTID�O DE PROTESTO: BUSCO PELA DATA DA CERTID�O NA TABELA DE CERTID�ES}
  SQL:='SELECT ID_CERTIDAO,DT_CERTIDAO,TIPO_CERTIDAO,ID_ATO,SELO,ENVIADO,ALEATORIO,RESULTADO,FOLHAS,REGISTROS,'+
       'CODIGO,EMOLUMENTOS,FETJ,FUNDPERJ,FUNPERJ,FUNARPEN,PMCMV,MUTUA,ACOTERJ,DISTRIBUICAO,TOTAL,'+
       'REQUERIDO,TIPO_REQUERIDO,CPF_CNPJ_REQUERIDO,RECIBO,COBRANCA '+
       'FROM CERTIDOES WHERE DT_CERTIDAO = '+QuotedStr(PF.FormatarData(edData.Date,'I'))+
       'AND TIPO_CERTIDAO<>'''+'X'+''' AND SELO <>'''' ORDER BY DT_CERTIDAO';

  Q.Close;
  Q.SQL.Clear;
  Q.SQL.Add(SQL);
  Q.Open;
  if not Q.IsEmpty then
  begin
      Q.First;
      while not Q.Eof do
      begin
          cdsAtos.Append;
          if Q.FieldByName('TIPO_CERTIDAO').AsString='I' then
          begin
              dm.Q1:=TFDQuery.Create(Nil);
              dm.Q1.Connection:=dm.conSISTEMA;
              dm.Q1.SQL.Add('SELECT PROTOCOLO,LIVRO_REGISTRO,FOLHA_REGISTRO,STATUS,DT_REGISTRO,DT_PAGAMENTO,'+
                            'SELO_REGISTRO,SELO_PAGAMENTO,ALEATORIO_PROTESTO,ALEATORIO_SOLUCAO FROM TITULOS WHERE ID_ATO=:ID_ATO');
              dm.Q1.Params[0].AsInteger:=Q.FieldByName('ID_ATO').AsInteger;
              dm.Q1.Open;

              cdsAtosNUMEROPROTOCOLO.AsInteger :=dm.Q1.FieldByName('PROTOCOLO').AsInteger;
              cdsAtosNUMEROLIVRO.AsString      :=dm.Q1.FieldByName('LIVRO_REGISTRO').AsString;
              cdsAtosNUMEROFOLHA.AsString      :=dm.Q1.FieldByName('FOLHA_REGISTRO').AsString;

              if dm.Q1.FieldByName('STATUS').AsString='PROTESTADO' then
              begin
                  cdsAtosDATAREGISTRO.AsString        :=dm.Q1.FieldByName('DT_REGISTRO').AsString;
                  cdsAtosSELOREGISTRO.AsString        :=dm.Q1.FieldByName('SELO_REGISTRO').AsString;
                  cdsAtosALEATORIOREGISTRO.AsString   :=dm.Q1.FieldByName('ALEATORIO_PROTESTO').AsString;
              end
              else
                if dm.Q1.FieldByName('STATUS').AsString='CANCELADO' then
                begin
                    cdsAtosDATAREGISTRO.AsString      :=dm.Q1.FieldByName('DT_PAGAMENTO').AsString;
                    cdsAtosSELOREGISTRO.AsString      :=dm.Q1.FieldByName('SELO_PAGAMENTO').AsString;
                    cdsAtosALEATORIOREGISTRO.AsString :=dm.Q1.FieldByName('ALEATORIO_SOLUCAO').AsString;
                end;

              dm.Q1.Close;
              dm.Q1.Free;
          end;

          cdsAtosOPENTAG.AsString               :='<Certidao';
          cdsAtosCLOSETAG.AsString              :='</Certidao>';
          cdsAtosIDATO.AsInteger                :=Q.FieldByName('ID_CERTIDAO').AsInteger;
          cdsAtosSTATUS.AsString                :='CERTID�O';
          cdsAtosXML.AsBoolean                  :=False;//Q.FieldByName('ENVIADO').AsString='N';
          cdsAtosENVIADO.AsString               :='N';//Q.FieldByName('ENVIADO').AsString;
          cdsAtosDATAPRATICA.AsString           :=FormatDateTime('dd/mm/yyyy',Q.FieldByName('DT_CERTIDAO').AsDateTime);
          cdsAtosSELO.AsString                  :=Q.FieldByName('SELO').AsString;
          cdsAtosALEATORIO.AsString             :=Q.FieldByName('ALEATORIO').AsString;
          cdsAtosTIPOCERTIDAO.AsString          :=Q.FieldByName('TIPO_CERTIDAO').AsString;
          cdsAtosQTDPAGINAS.AsString            :=Q.FieldByName('FOLHAS').AsString;
          cdsAtosRESULTADO.AsString             :=Q.FieldByName('RESULTADO').AsString;
          cdsAtosNUMEROREGISTROS.AsString       :=Q.FieldByName('REGISTROS').AsString;
          cdsAtosNOMEAPRESENTANTE.AsString      :=Q.FieldByName('REQUERIDO').AsString;
          cdsAtosTIPOAPRESENTANTE.AsString      :=Q.FieldByName('TIPO_REQUERIDO').AsString;
          cdsAtosDOCUMENTOAPRESENTANTE.AsString :=Q.FieldByName('CPF_CNPJ_REQUERIDO').AsString;
          cdsAtosTIPOCOBRANCA.AsString          :=Q.FieldByName('COBRANCA').AsString;
          cdsAtosEMOLUMENTOS.AsString           :=GR.ValorComPontoXML(Q.FieldByName('EMOLUMENTOS').AsFloat);
          cdsAtosFETJ.AsString                  :=GR.ValorComPontoXML(Q.FieldByName('FETJ').AsFloat);
          cdsAtosFUNDPERJ.AsString              :=GR.ValorComPontoXML(Q.FieldByName('FUNDPERJ').AsFloat);
          cdsAtosFUNPERJ.AsString               :=GR.ValorComPontoXML(Q.FieldByName('FUNPERJ').AsFloat);
          cdsAtosFUNARPEN.AsString              :=GR.ValorComPontoXML(Q.FieldByName('FUNARPEN').AsFloat);
          cdsAtosPMCMV.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('PMCMV').AsFloat);
          cdsAtosMUTUA.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('MUTUA').AsFloat);
          cdsAtosACOTERJ.AsString               :=GR.ValorComPontoXML(Q.FieldByName('ACOTERJ').AsFloat);
          cdsAtosDISTRIBUICAO.AsString          :=GR.ValorComPontoXML(Q.FieldByName('DISTRIBUICAO').AsFloat);
          cdsAtosTOTAL.AsString                 :=GR.ValorComPontoXML(Q.FieldByName('TOTAL').AsFloat);
          cdsAtosTAGCUSTAS.AsBoolean            :=True;

          if cdsAtosTIPOCERTIDAO.AsString='F' then
          cdsAtosTIPOCERTIDAO.AsString:='N';

          cdsAtos.Post;
          if cdsAtosXML.AsBoolean then Inc(vMarcados);
          Q.Next;
      end;
  end;
  Q.Close;
  Q.Free;

  {FOLHA ADICIONAL}
  qryFolhas.Close;
  qryFolhas.ParamByName('DT').AsDate    := edData.Date;
  qryFolhas.ParamByName('EN1').AsString := '*';
  qryFolhas.ParamByName('EN2').AsString := '*';
  qryFolhas.Open;

  if not qryFolhas.IsEmpty then
  begin
      qryFolhas.First;
      while not qryFolhas.Eof do
      begin
          cdsAtos.Append;
          cdsAtosOPENTAG.AsString               :='<FolhaAdicionalComplemento';
          cdsAtosCLOSETAG.AsString              :='</FolhaAdicionalComplemento>';
          cdsAtosIDATO.AsInteger                :=qryFolhasID_CERTIDAO.AsInteger;
          cdsAtosSTATUS.AsString                :='FOLHA EXCEDENTE';
          cdsAtosXML.AsBoolean                  :=False;//FolhasENVIADO_FLS.AsString='N';
          cdsAtosENVIADO.AsString               :='N';//FolhasENVIADO_FLS.AsString;
          cdsAtosDATAPRATICA.AsString           :=FormatDateTime('dd/mm/yyyy',qryFolhasDT_ENTREGA.AsDateTime);
          cdsAtosDATAREGISTRO.AsString          :=FormatDateTime('dd/mm/yyyy',qryFolhasDT_CERTIDAO.AsDateTime);
          cdsAtosSELO.AsString                  :=qryFolhasSELO.AsString;
          cdsAtosNOMEAPRESENTANTE.AsString      :=qryFolhasREQUERENTE.AsString;
          cdsAtosALEATORIO.AsString             :=qryFolhasALEATORIO.AsString;
          cdsAtosCCT.AsString                   :=qryFolhasCCT.AsString;
          cdsAtosNUMEROFOLHA.AsString           :=IntToStr(qryFolhasFOLHAS.AsInteger-1);
          cdsAtosEMOLUMENTOS.AsString           :=GR.ValorComPontoXML(qryFolhasEXEMOLUMENTOS.AsFloat);
          cdsAtosFETJ.AsString                  :=GR.ValorComPontoXML(qryFolhasEXFETJ.AsFloat);
          cdsAtosFUNDPERJ.AsString              :=GR.ValorComPontoXML(qryFolhasEXFUNDPERJ.AsFloat);
          cdsAtosFUNPERJ.AsString               :=GR.ValorComPontoXML(qryFolhasEXFUNPERJ.AsFloat);
          cdsAtosFUNARPEN.AsString              :=GR.ValorComPontoXML(qryFolhasEXFUNARPEN.AsFloat);
          cdsAtosTOTAL.AsString                 :=GR.ValorComPontoXML(qryFolhasEXTOTAL.AsFloat);
          cdsAtosTIPOCOBRANCA.AsString          :='CC';
          cdsAtosTAGCUSTAS.AsBoolean            :=True;
          cdsAtos.Post;
          if cdsAtosXML.AsBoolean then Inc(vMarcados);
          qryFolhas.Next;
      end;
  end;

  {INFORMA��O VERBAL}
  dm.qryVerbal.Close;
  dm.qryVerbal.ParamByName('DT').AsDate:=edData.Date;
  dm.qryVerbal.ParamByName('EN').AsString:='*';
  dm.qryVerbal.Open;

  if not dm.qryVerbal.IsEmpty then
  begin
      dm.qryVerbal.First;
      while not dm.qryVerbal.Eof do
      begin
          cdsAtos.Append;
          cdsAtosOPENTAG.AsString               :='<InformacaoVerbal';
          cdsAtosCLOSETAG.AsString              :='</InformacaoVerbal>';
          cdsAtosIDATO.AsInteger                :=dm.qryVerbalID_VERBAL.AsInteger;
          cdsAtosSTATUS.AsString                :='INFORMA��O VERBAL';
          cdsAtosXML.AsBoolean                  :=dm.qryVerbalENVIADO.AsString='N';
          cdsAtosENVIADO.AsString               :=dm.qryVerbalENVIADO.AsString;
          cdsAtosDATAPRATICA.AsString           :=FormatDateTime('dd/mm/yyyy',dm.qryVerbalDATA.AsDateTime);
          cdsAtosCCT.AsString                   :=dm.qryVerbalCCT.AsString;
          cdsAtosNOMEVERBAL.AsString            :=dm.qryVerbalNOME.AsString;
          cdsAtosNUMEROATO.AsString             :=dm.qryVerbalNUMERO.AsString;
          cdsAtosEMOLUMENTOS.AsString           :=GR.ValorComPontoXML(dm.qryVerbalEMOLUMENTOS.AsFloat);
          cdsAtosFETJ.AsString                  :=GR.ValorComPontoXML(dm.qryVerbalFETJ.AsFloat);
          cdsAtosFUNDPERJ.AsString              :=GR.ValorComPontoXML(dm.qryVerbalFUNDPERJ.AsFloat);
          cdsAtosFUNPERJ.AsString               :=GR.ValorComPontoXML(dm.qryVerbalFUNPERJ.AsFloat);
          cdsAtosFUNARPEN.AsString              :=GR.ValorComPontoXML(dm.qryVerbalFUNARPEN.AsFloat);
          cdsAtosTOTAL.AsString                 :=GR.ValorComPontoXML(dm.qryVerbalEMOLUMENTOS.AsFloat+
                                                                      dm.qryVerbalFETJ.AsFloat+
                                                                      dm.qryVerbalFUNDPERJ.AsFloat+
                                                                      dm.qryVerbalFUNPERJ.AsFloat+
                                                                      dm.qryVerbalFUNARPEN.AsFloat);
          cdsAtosTIPOCOBRANCA.AsString          :=dm.qryVerbalTIPO_COBRANCA.AsString;
          cdsAtosTAGCUSTAS.AsBoolean            :=True;

          cdsAtos.Post;
          if cdsAtosXML.AsBoolean then Inc(vMarcados);
          dm.qryVerbal.Next;
      end;
  end;

  cdsAtos.First;
  cdsAtos.EnableControls;
  Self.Caption:='XML - '+IntToStr(vMarcados)+GR.iif(vMarcados>1,' Selecionados',' Selecionado');     **)
end;

procedure TFXML2014.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.Escreventes.Close;
  dm.Escreventes.Filtered:=False;
end;

procedure TFXML2014.GridMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if (X>0) and (X<87) then
    Screen.Cursor:=crHandPoint
      else Screen.Cursor:=crDefault;
end;

procedure TFXML2014.Marcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  vMarcados:=0;
  cdsAtos.DisableControls;
  Posicao:=cdsAtos.RecNo;
  cdsAtos.First;
  while not cdsAtos.Eof do
  begin
      cdsAtos.Edit;
      cdsAtosXML.AsBoolean:=True;
      cdsAtos.Post;
      cdsAtos.Next;
      Inc(vMarcados);
  end;
  cdsAtos.RecNo:=Posicao;
  cdsAtos.EnableControls;
  Self.Caption:='XML - '+IntToStr(vMarcados)+GR.iif(vMarcados>1,' Selecionados',' Selecionado');
end;

procedure TFXML2014.Desmarcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  cdsAtos.DisableControls;
  Posicao:=cdsAtos.RecNo;
  cdsAtos.First;
  while not cdsAtos.Eof do
  begin
      cdsAtos.Edit;
      cdsAtosXML.AsBoolean:=False;
      cdsAtos.Post;
      cdsAtos.Next;
  end;
  cdsAtos.RecNo:=Posicao;
  cdsAtos.EnableControls;
  vMarcados:=0;
  Self.Caption:='XML - '+IntToStr(vMarcados)+GR.iif(vMarcados>1,' Selecionados',' Selecionado');
end;

procedure TFXML2014.CorrigirApresentante;
begin
  if (dm.TitulosCODIGO_APRESENTANTE.AsString='') and (dm.TitulosCPF_CNPJ_APRESENTANTE.AsString='') then
  begin
      PF.CarregarPortador('N',dm.TitulosAPRESENTANTE.AsString,dm.TitulosAPRESENTANTE.AsString,
                              dm.TitulosTIPO_APRESENTANTE.AsString,dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosCODIGO_APRESENTANTE.AsString);
      dm.Titulos.Edit;
      dm.TitulosCODIGO_APRESENTANTE.AsInteger :=dm.RxPortadorCODIGO.AsInteger;
      dm.TitulosCPF_CNPJ_APRESENTANTE.AsString:=dm.RxPortadorDOCUMENTO.AsString;
      dm.TitulosTIPO_APRESENTANTE.AsString    :=dm.RxPortadorTIPO.AsString;
      dm.Titulos.Post;
      dm.Titulos.ApplyUpdates(0);
  end;
end;

procedure TFXML2014.MarcarTodosEnviado1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  cdsAtos.DisableControls;
  Posicao:=cdsAtos.RecNo;
  cdsAtos.First;
  while not cdsAtos.Eof do
  begin
      cdsAtos.Edit;
      cdsAtosENVIADO.AsString:='S';
      cdsAtos.Post;
      cdsAtos.Next;
  end;
  cdsAtos.RecNo:=Posicao;
  cdsAtos.EnableControls;
end;

procedure TFXML2014.DesmarcarTodosXML1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  cdsAtos.DisableControls;
  Posicao:=cdsAtos.RecNo;
  cdsAtos.First;
  while not cdsAtos.Eof do
  begin
      cdsAtos.Edit;
      cdsAtosENVIADO.AsString:='N';
      cdsAtos.Post;
      cdsAtos.Next;
  end;
  cdsAtos.RecNo:=Posicao;
  cdsAtos.EnableControls;
end;

procedure TFXML2014.sPanel1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  Screen.Cursor:=crDefault;
end;

procedure TFXML2014.P1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  Screen.Cursor:=crDefault;
end;

procedure TFXML2014.GridMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Screen.Cursor=crHandPoint then
  begin
      if cdsAtos.IsEmpty then Exit;

      cdsAtos.Edit;

      if X in [0..38] then
      begin
          if cdsAtosXML.AsBoolean=True then
            cdsAtosXML.AsBoolean:=False
              else cdsAtosXML.AsBoolean:=True;
      end;

      if X in [39..87] then
      begin
          if cdsAtosENVIADO.AsString='S' then
            cdsAtosENVIADO.AsString:='N'
              else cdsAtosENVIADO.AsString:='S';
      end;

      cdsAtos.Post;
      if cdsAtosXML.AsBoolean then
        Inc(vMarcados)
          else Dec(vMarcados);
      Self.Caption:='XML - '+IntToStr(vMarcados)+GR.iif(vMarcados>1,' Selecionados',' Selecionado');
  end;
end;

procedure TFXML2014.FormShow(Sender: TObject);
begin
  edData.SetFocus;
end;

procedure TFXML2014.TagParticipantes;
begin
  GR.WXML(F,E(2)+'<Participantes>',True);

  {PESSOA F�SICA}
  {APRESENTANTE}
  if cdsAtosTIPOAPRESENTANTE.AsString='F' then
  begin
      GR.WXML(F,E(4)+'<PessoaFisica ',False);
      GR.WXML(F,'Tipo','34',3,True,False,True,False);
      GR.WXML(F,'Nome',cdsAtosNOMEAPRESENTANTE.AsString,250,True,True,False,False);
      GR.WXML(F,'CPF',cdsAtosDOCUMENTOAPRESENTANTE.AsString,14,False,True,True,False);
      GR.WXML(F,'/>',True);
  end;

  {DEVEDOR}
  if cdsAtosTIPODEVEDOR.AsString='F' then
  begin
      GR.WXML(F,E(4)+'<PessoaFisica ',False);
      GR.WXML(F,'Tipo','8',3,True,False,True,False);
      GR.WXML(F,'Nome',cdsAtosNOMEDEVEDOR.AsString,250,True,True,False,False);
      GR.WXML(F,'CPF',cdsAtosDOCUMENTODEVEDOR.AsString,14,False,True,True,False);
      GR.WXML(F,'/>',True);
  end;

  PF.CarregarDevedor(cdsAtosIDATO.AsInteger);
  if dm.RxDevedor.RecordCount>1 then
  begin
      while not dm.RxDevedor.Eof do
      begin
          if dm.RxDevedorDOCUMENTO.AsString<>cdsAtosDOCUMENTODEVEDOR.AsString then
          begin
              GR.WXML(F,E(4)+'<PessoaFisica ',False);
              GR.WXML(F,'Tipo','8',3,True,False,True,False);
              GR.WXML(F,'Nome',dm.RxDevedorNOME.AsString,250,True,True,False,False);
              GR.WXML(F,'CPF',dm.RxDevedorDOCUMENTO.AsString,14,False,True,True,False);
              GR.WXML(F,'/>',True);
          end;
          dm.RxDevedor.Next;
      end;
  end;
  dm.RxDevedor.Close;

  {SACADOR}
  if cdsAtosTIPOSACADOR.AsString='F' then
  begin
      GR.WXML(F,E(4)+'<PessoaFisica ',False);
      GR.WXML(F,'Tipo','35',3,True,False,True,False);
      GR.WXML(F,'Nome',cdsAtosNOMESACADOR.AsString,250,True,True,False,False);
      GR.WXML(F,'CPF',cdsAtosDOCUMENTOSACADOR.AsString,14,False,True,True,False);
      GR.WXML(F,'/>',True);
  end;

  {CEDENTE}
  if cdsAtosNOMECEDENTE.AsString<>'' then
  begin
      GR.WXML(F,E(4)+'<PessoaFisica ',False);
      GR.WXML(F,'Tipo','4',3,True,False,True,False);
      GR.WXML(F,'Nome',cdsAtosNOMECEDENTE.AsString,250,True,True,False,False);
      //GR.WXML(F,'CPF','N�O INFORMADO',14,False,True,True,False);
      GR.WXML(F,'/>',True);
  end;

  {PESSOA JUR�DICA}
  {APRESENTANTE}
  if cdsAtosTIPOAPRESENTANTE.AsString='J' then
  begin
      GR.WXML(F,E(4)+'<PessoaJuridica ',False);
      GR.WXML(F,'Tipo','34',3,True,False,True,False);
      GR.WXML(F,'Nome',cdsAtosNOMEAPRESENTANTE.AsString,250,True,True,False,False);
      GR.WXML(F,'CNPJ',cdsAtosDOCUMENTOAPRESENTANTE.AsString,18,False,True,True,False);
      GR.WXML(F,'/>',True);
  end;

  {DEVEDOR}
  if cdsAtosTIPODEVEDOR.AsString='J' then
  begin
      GR.WXML(F,E(4)+'<PessoaJuridica ',False);
      GR.WXML(F,'Tipo','8',3,True,False,True,False);
      GR.WXML(F,'Nome',cdsAtosNOMEDEVEDOR.AsString,250,True,True,False,False);
      GR.WXML(F,'CNPJ',cdsAtosDOCUMENTODEVEDOR.AsString,18,False,True,True,False);
      GR.WXML(F,'/>',True);
  end;

  {SACADOR}
  if cdsAtosTIPOSACADOR.AsString='J' then
  begin
      GR.WXML(F,E(4)+'<PessoaJuridica ',False);
      GR.WXML(F,'Tipo','35',3,True,False,True,False);
      GR.WXML(F,'Nome',cdsAtosNOMESACADOR.AsString,250,True,True,False,False);
      GR.WXML(F,'CNPJ',cdsAtosDOCUMENTOSACADOR.AsString,18,False,True,True,False);
      GR.WXML(F,'/>',True);
  end;

  GR.WXML(F,E(2)+'</Participantes>',True);
end;

procedure TFXML2014.btGerarClick(Sender: TObject);
var
  vArquivo,vTipo,vAmbiente: String;
begin
  if not cdsAtos.Active then Exit;

  if (not (cdsAtos.IsEmpty)) and (vMarcados=0) then
  begin
      GR.Aviso('NENHUM ATO SELECIONADO!');
      Exit;
  end;

  try
    if FileExists('C:\FNMASTOTAL') then
      vAmbiente:='HML'
        else vAmbiente:='PRD';

    if ckAlteracao.Checked then
      vTipo:='A'
        else vTipo:='I';

    if (cdsAtos.Active) and (cdsAtos.IsEmpty) then
      if GR.Pergunta('NENHUM MOVIMENTO ENCONTRADO NO DIA: '+edData.Text+'.'+#13#13+
                     'GERAR ARQUIVO DE JUSTIFICATIVA DE ATO N�O PRATICADO') then
      begin
          GR.GerarXMLVazio(F,lbCaminho.Caption,'ProtestoTitulos','PT',FormatDateTime('yyyymmdd',Now),FormatDateTime('hhmmss',Now),'1.05',
                           IntToStr(Gdm.IdRemessa),dm.ServentiaCODIGO.AsString,vTipo,'TOTAL CENTRAL PROTESTO',vAmbiente,edData.Text,
                           dm.ServentiaDESCRICAO.AsString,dm.ServentiaCIDADE.AsString,dm.ServentiaTABELIAO.AsString,
                           dm.vVersao,dm.vAtualizacao,FormatDateTime('dd/mm/yyyy',Now));
          Exit;
      end
      else
        Exit;

    vArquivo:=GR.CabecalhoXML(F,
                              dm.ServentiaCODIGO.AsString,
                              FormatDateTime('yyyymmdd',edData.Date),
                              FormatDateTime('hhmmss',Now),
                              'PT',
                              lbCaminho.Caption,
                              'ProtestoTitulos',
                              '1.05',
                              Gdm.IdRemessa,
                              vTipo,
                              'TOTAL CENTRAL PROTESTO',
                              vAmbiente,
                              dm.ServentiaDESCRICAO.AsString,
                              dm.ServentiaCIDADE.AsString,
                              dm.ServentiaTABELIAO.AsString,
                              dm.vVersao,
                              dm.vAtualizacao,
                              FormatDateTime('dd/mm/yyyy',Now));

    cdsAtos.DisableControls;
    cdsAtos.First;
    while not cdsAtos.Eof do
    begin
        if cdsAtosXML.AsBoolean then
        begin
            GR.WXML(F,cdsAtosOPENTAG.AsString+' ',False);
            GR.WXML(F,'DataPratica',cdsAtosDATAPRATICA.AsString,10,True,False,True,False);

            if (cdsAtosSTATUS.AsString='APONTADO')          or
               (cdsAtosSTATUS.AsString='EMISS�O DE GUIA')   or
               (cdsAtosSTATUS.AsString='INFORMA��O VERBAL') or
               (cdsAtosSTATUS.AsString='FOLHA EXCEDENTE')   then
              GR.WXML(F,'CCT',GR.SeloXML(cdsAtosCCT.AsString),9,False,True,True,False)
                else
                begin
                    GR.WXML(F,'Selo',GR.SeloXML(cdsAtosSELO.AsString),9,False,True,True,False);
                    GR.WXML(F,'Aleatorio',cdsAtosALEATORIO.AsString,3,False,True,True,False);
                end;

            GR.WXML(F,'NumeroRecibo',cdsAtosNUMERORECIBO.AsString,25,False,True,True,False);

            if cdsAtosSTATUS.AsString='APONTADO' then
            begin
                GR.WXML(F,'TipoTitulo',cdsAtosTIPOTITULO.AsString,2,True,True,True,False);
                GR.WXML(F,'NumeroTitulo',cdsAtosNUMEROTITULO.AsString,25,False,True,True,False);
                GR.WXML(F,'ValorTitulo',cdsAtosVALORTITULO.AsString,18,False,True,True,False);
                GR.WXML(F,'Saldo',cdsAtosSALDO.AsString,18,False,True,True,False);
                GR.WXML(F,'DataVencimento',cdsAtosDATAVENCIMENTO.AsString,10,False,False,True,False);
                GR.WXML(F,'IndConvenio',cdsAtosINDCONVENIO.AsString,1,False,True,True,False);
                GR.WXML(F,'TipoApresentacao',cdsAtosTIPOAPRESENTACAO.AsString,1,False,True,True,False);
                GR.WXML(F,'NumeroProtocolo',cdsAtosNUMEROPROTOCOLO.AsString,25,False,True,True,False);
            end;

            if cdsAtosSTATUS.AsString='PAGO' then
            GR.WXML(F,'FormaPagamento',cdsAtosFORMAPAGAMENTO.AsString,25,False,True,True,False);

            if cdsAtosSTATUS.AsString='SUSTADO' then
            GR.WXML(F,'DescricaoSustacao',cdsAtosDETERMINACAO.AsString,100,False,True,False,False);

            if cdsAtosSTATUS.AsString='PROTESTADO' then
            begin
                GR.WXML(F,'NumeroLivro',cdsAtosNUMEROLIVRO.AsString,25,False,True,False,False);
                GR.WXML(F,'NumeroFolha',cdsAtosNUMEROFOLHA.AsString,25,False,True,False,False);
            end;

            if (cdsAtosSTATUS.AsString='EMISS�O DE GUIA') then
            GR.WXML(F,'NumeroGuia',cdsAtosNUMEROGUIA.AsString,25,False,True,True,False);

            if (cdsAtosSTATUS.AsString='INFORMA��O VERBAL') then
            GR.WXML(F,'NumeroAto',cdsAtosNUMEROATO.AsString,25,False,True,True,False);

            if (cdsAtosSTATUS.AsString='FOLHA EXCEDENTE') then
            GR.WXML(F,'QtdPaginasExcedentes',cdsAtosNUMEROFOLHA.AsString,6,True,True,True,False);

            if cdsAtosSTATUS.AsString='CANCELADO' then
            GR.WXML(F,'IndCancelamento',cdsAtosINDCANCELAMENTO.AsString,1,False,True,False,False);

            if (cdsAtosSTATUS.AsString='CERTID�O') then
            begin
                GR.WXML(F,'TipoCertidao',cdsAtosTIPOCERTIDAO.AsString,1,True,True,True,False);
                GR.WXML(F,'QtdPaginas',cdsAtosQTDPAGINAS.AsString,6,False,True,True,False);
                GR.WXML(F,'Resultado',cdsAtosRESULTADO.AsString,1,False,True,True,False);
                GR.WXML(F,'NumeroRegistros',cdsAtosNUMEROREGISTROS.AsString,6,False,True,True,False);
            end;

            GR.WXML(F,'>',True);

            if (cdsAtosSTATUS.AsString='APONTADO') or (cdsAtosSTATUS.AsString='CERTID�O') then
            TagParticipantes;

            if (cdsAtosSTATUS.AsString='PAGO') or (cdsAtosSTATUS.AsString='SUSTADO') or
               (cdsAtosSTATUS.AsString='RETIRADO') or (cdsAtosSTATUS.AsString='PROTESTADO') then
            begin
                GR.WXML(F,E(2)+'<AtosVinculados>',True);
                GR.WXML(F,E(4)+'<AtoVinculado ',False);
                GR.WXML(F,'TipoVinculo','Z',1,True,True,True,False);
                GR.WXML(F,'CodigoServico',dm.ServentiaCODIGO.AsString,4,True,True,True,False);
                GR.WXML(F,'CCT',GR.SeloXML(cdsAtosCCT.AsString),9,False,True,True,False);
                GR.WXML(F,'DataPratica',cdsAtosDATAPROTOCOLO.AsString,10,False,False,True,False);
                GR.WXML(F,'NumeroProtocolo',cdsAtosNUMEROPROTOCOLO.AsString,25,False,True,True,False);
                GR.WXML(F,'/>',True);
                GR.WXML(F,E(2)+'</AtosVinculados>',True);
            end;

            if (cdsAtosSTATUS.AsString='EMISS�O DE GUIA') then
            begin
                GR.WXML(F,E(2)+'<AtosVinculados>',True);
                GR.WXML(F,E(4)+'<AtoVinculado ',False);
                GR.WXML(F,'TipoVinculo','Z',1,True,True,True,False);
                GR.WXML(F,'CodigoServico',dm.ServentiaCODIGO.AsString,4,True,True,True,False);
                GR.WXML(F,'CCT',GR.SeloXML(cdsAtosCCTPROTOCOLO.AsString),9,False,True,True,False);
                GR.WXML(F,'DataPratica',cdsAtosDATAPROTOCOLO.AsString,10,False,False,True,False);
                GR.WXML(F,'NumeroProtocolo',cdsAtosNUMEROPROTOCOLO.AsString,25,False,True,True,False);
                GR.WXML(F,'/>',True);
                GR.WXML(F,E(2)+'</AtosVinculados>',True);
            end;

            if (cdsAtosSTATUS.AsString='FOLHA EXCEDENTE') then
            begin
                GR.WXML(F,E(2)+'<AtosVinculados>',True);
                GR.WXML(F,E(4)+'<AtoVinculado ',False);
                GR.WXML(F,'TipoVinculo','C',1,True,True,True,False);
                GR.WXML(F,'CodigoServico',dm.ServentiaCODIGO.AsString,4,True,True,True,False);
                GR.WXML(F,'Selo',GR.SeloXML(cdsAtosSELO.AsString),9,False,True,True,False);
                GR.WXML(F,'Aleatorio',cdsAtosALEATORIO.AsString,3,False,True,True,False);
                GR.WXML(F,'TipoSelo','E',1,True,True,True,False);
                GR.WXML(F,'DataPratica',cdsAtosDATAREGISTRO.AsString,10,False,False,True,False);
                GR.WXML(F,'/>',True);
                GR.WXML(F,E(2)+'</AtosVinculados>',True);
            end;

            if (cdsAtosSTATUS.AsString='CANCELADO') then
            begin
                GR.WXML(F,E(2)+'<AtosVinculados>',True);
                GR.WXML(F,E(4)+'<AtoVinculado ',False);
                GR.WXML(F,'TipoVinculo','S',1,True,True,True,False);
                GR.WXML(F,'CodigoServico',dm.ServentiaCODIGO.AsString,4,True,True,True,False);
                GR.WXML(F,'Selo',GR.SeloXML(cdsAtosSELOREGISTRO.AsString),9,False,True,True,False);
                GR.WXML(F,'Aleatorio',cdsAtosALEATORIOREGISTRO.AsString,3,False,True,True,False);
                GR.WXML(F,'TipoSelo','E',1,True,True,True,False);
                GR.WXML(F,'DataPratica',cdsAtosDATAREGISTRO.AsString,10,False,False,True,False);
                GR.WXML(F,'NumeroProtocolo',cdsAtosNUMEROPROTOCOLO.AsString,25,False,True,True,False);
                GR.WXML(F,'/>',True);
                GR.WXML(F,E(2)+'</AtosVinculados>',True);
            end;

            if (cdsAtosSTATUS.AsString='CERTID�O') and (cdsAtosTIPOCERTIDAO.AsString='I') then
            begin
                GR.WXML(F,E(2)+'<AtosVinculados>',True);
                GR.WXML(F,E(4)+'<AtoVinculado ',False);
                GR.WXML(F,'TipoVinculo','E',1,True,True,True,False);
                GR.WXML(F,'CodigoServico',dm.ServentiaCODIGO.AsString,4,True,True,True,False);
                GR.WXML(F,'Selo',GR.SeloXML(cdsAtosSELOREGISTRO.AsString),9,False,True,True,False);
                GR.WXML(F,'Aleatorio',cdsAtosALEATORIOREGISTRO.AsString,3,False,True,True,False);
                GR.WXML(F,'TipoSelo','E',1,True,True,True,False);
                GR.WXML(F,'DataPratica',cdsAtosDATAREGISTRO.AsString,10,False,False,True,False);
                GR.WXML(F,'NumeroProtocolo',cdsAtosNUMEROPROTOCOLO.AsString,25,False,True,True,False);
                GR.WXML(F,'/>',True);
                GR.WXML(F,E(2)+'</AtosVinculados>',True);
            end;

            TagEmolumentos;

            GR.WXML(F,cdsAtosCLOSETAG.AsString,True);
            GR.WXML(F,'',True);
        end;
        cdsAtos.Next;
    end;

  GR.WXML(F,'</Remessa>',True);
  GR.WXML(F,'</ProtestoTitulos>',True);
  CloseFile(F);
  cdsAtos.Close;
  cdsAtos.EnableControls;
  if ckVisualizar.Checked then
  ShellExecute(Handle,'Open',PChar(vArquivo),Nil,Nil,SW_SHOWMAXIMIZED);
  GR.Aviso('ARQUIVO GERADO COM SUCESSO!');
  except
    on E: Exception do
    begin
        CloseFile(F);
        cdsAtos.EnableControls;
        Grid.SetFocus;
        GR.Aviso('ERRO NO SEGUINTE ATO: '+#13#13+'ID: '+cdsAtosIDATO.AsString+#13+
                                                 'LIVRO: '+cdsAtosNUMEROLIVRO.AsString+#13+
                                                 'FOLHAS: '+cdsAtosNUMEROFOLHA.AsString+#13+
                                                 'SELO: '+cdsAtosSELO.AsString+#13#13+'MENSAGEM: '+E.Message);
    end;
  end;
end;

procedure TFXML2014.TagEmolumentos;
begin
  GR.WXML(F,E(2)+'<Emolumentos ',False);
  GR.WXML(F,'TipoCobranca',cdsAtosTIPOCOBRANCA.AsString,2,True,False,False,False);

  if cdsAtosTAGCUSTAS.AsBoolean then
  begin
      GR.WXML(F,'ValorTotalEmolumentos',cdsAtosEMOLUMENTOS.AsString,10,False,False,False,False);
      GR.WXML(F,'FETJ',cdsAtosFETJ.AsString,10,False,False,False,False);
      GR.WXML(F,'FUNDPERJ',cdsAtosFUNDPERJ.AsString,10,False,False,False,False);
      GR.WXML(F,'FUNPERJ',cdsAtosFUNPERJ.AsString,10,False,False,False,False);
      GR.WXML(F,'FUNARPEN',cdsAtosFUNARPEN.AsString,10,False,False,False,False);
      GR.WXML(F,'RESSAG',cdsAtosPMCMV.AsString,10,False,False,False,False);
      GR.WXML(F,'ValorMutua',cdsAtosMUTUA.AsString,10,False,False,False,False);
      GR.WXML(F,'ValorAcoterj',cdsAtosACOTERJ.AsString,10,False,False,False,False);
      GR.WXML(F,'ValorDistribuidor',cdsAtosDISTRIBUICAO.AsString,10,False,False,False,False);
      GR.WXML(F,'>',True);

      if cdsAtosSTATUS.AsString='EMISS�O DE GUIA' then
      begin
          GR.WXML(F,E(6)+'<ItemEmolumento ',False);
          GR.WXML(F,'Ano',dm.ValorParametro(77),4,False,False,False,False);
          GR.WXML(F,'Tabela','16',8,True,False,False,False);
          GR.WXML(F,'Item','5',20,True,False,False,False);
          GR.WXML(F,'SubItem','*',20,False,False,False,False);
          GR.WXML(F,'Quantidade','1',4,True,False,False,False);
          GR.WXML(F,'/>',True);
      end
      else
      begin
          PF.CarregarCustas(cdsAtosIDATO.AsInteger);

          if (cdsAtosSTATUS.AsString='CANCELADO') then
          begin
              if (cdsAtosTIPOCOBRANCA.AsString='CV') then
              begin
                  dm.RxCustas.First;
                  while not dm.RxCustas.Eof do
                  begin
                      GR.WXML(F,E(6)+'<ItemEmolumento ',False);
                      GR.WXML(F,'Ano',dm.ValorParametro(77),4,False,False,False,False);
                      GR.WXML(F,'Tabela',dm.RxCustasTABELA.AsString,8,True,False,False,False);
                      GR.WXML(F,'Item',dm.RxCustasITEM.AsString,20,True,False,False,False);
                      GR.WXML(F,'SubItem',dm.RxCustasSUBITEM.AsString,20,False,False,False,False);
                      GR.WXML(F,'Quantidade',dm.RxCustasQTD.AsString,4,True,False,False,False);
                      GR.WXML(F,'/>',True);
                      dm.RxCustas.Next;
                  end;
                  PF.CarregarCustas(cdsAtosIDREFERENCIA.AsInteger);
                  dm.RxCustas.First;
                  while not dm.RxCustas.Eof do
                  begin
                      GR.WXML(F,E(6)+'<ItemEmolumento ',False);
                      GR.WXML(F,'Ano',dm.ValorParametro(77),4,False,False,False,False);
                      GR.WXML(F,'Tabela',dm.RxCustasTABELA.AsString,8,True,False,False,False);
                      GR.WXML(F,'Item',dm.RxCustasITEM.AsString,20,True,False,False,False);
                      GR.WXML(F,'SubItem',dm.RxCustasSUBITEM.AsString,20,False,False,False,False);
                      GR.WXML(F,'Quantidade',dm.RxCustasQTD.AsString,4,True,False,False,False);
                      GR.WXML(F,'/>',True);
                      dm.RxCustas.Next;
                  end;
              end
              else
              begin
                  PF.CarregarCustas(cdsAtosIDREFERENCIA.AsInteger);
                  dm.RxCustas.First;
                  while not dm.RxCustas.Eof do
                  begin
                      GR.WXML(F,E(6)+'<ItemEmolumento ',False);
                      GR.WXML(F,'Ano',dm.ValorParametro(77),4,False,False,False,False);
                      GR.WXML(F,'Tabela',dm.RxCustasTABELA.AsString,8,True,False,False,False);
                      GR.WXML(F,'Item',dm.RxCustasITEM.AsString,20,True,False,False,False);
                      GR.WXML(F,'SubItem',dm.RxCustasSUBITEM.AsString,20,False,False,False,False);
                      GR.WXML(F,'Quantidade',dm.RxCustasQTD.AsString,4,True,False,False,False);
                      GR.WXML(F,'/>',True);
                      dm.RxCustas.Next;
                  end;
              end;
          end
          else
          begin
              dm.RxCustas.First;
              while not dm.RxCustas.Eof do
              begin
                  if cdsAtosSTATUS.AsString='FOLHA EXCEDENTE' then
                  begin
                      if dm.RxCustasTABELA.AsString+dm.RxCustasITEM.AsString+dm.RxCustasSUBITEM.AsString='162*' then
                      begin
                          GR.WXML(F,E(6)+'<ItemEmolumento ',False);
                          GR.WXML(F,'Ano',dm.ValorParametro(77),4,False,False,False,False);
                          GR.WXML(F,'Tabela',dm.RxCustasTABELA.AsString,8,True,False,False,False);
                          GR.WXML(F,'Item',dm.RxCustasITEM.AsString,20,True,False,False,False);
                          GR.WXML(F,'SubItem',dm.RxCustasSUBITEM.AsString,20,False,False,False,False);
                          GR.WXML(F,'Quantidade',IntToStr(dm.RxCustasQTD.AsInteger-1),4,True,False,False,False);
                          GR.WXML(F,'/>',True);
                      end;
                  end
                  else
                  begin
                      GR.WXML(F,E(6)+'<ItemEmolumento ',False);
                      GR.WXML(F,'Ano',dm.ValorParametro(77),4,False,False,False,False);
                      GR.WXML(F,'Tabela',dm.RxCustasTABELA.AsString,8,True,False,False,False);
                      GR.WXML(F,'Item',dm.RxCustasITEM.AsString,20,True,False,False,False);
                      GR.WXML(F,'SubItem',dm.RxCustasSUBITEM.AsString,20,False,False,False,False);
                      GR.WXML(F,'Quantidade',dm.RxCustasQTD.AsString,4,True,False,False,False);
                      GR.WXML(F,'/>',True);
                  end;
                  dm.RxCustas.Next;
              end;
          end;
      end;
      GR.WXML(F,E(2)+'</Emolumentos>',True);
  end
  else
    GR.WXML(F,'/>',True);
end;

procedure TFXML2014.lbCaminhoClick(Sender: TObject);
begin
  GR.ShellOpen(lbCaminho.Caption);
end;

procedure TFXML2014.edDataKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then btLocalizar.SetFocus;
end;

end.
