unit UBoleto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, sEdit, sComboBox, sGroupBox,
  sCurrEdit, sCurrencyEdit, Mask, sMaskEdit, sCustomComboEdit, sTooledit,
  Buttons, sBitBtn, ACBrBoleto, ACBrBoletoFCQuickFr, ACBrBase, sMemo,
  ACBrBoletoFCFortesFr, SQLExpr;

type
  TFBoleto = class(TForm)
    P1: TsPanel;
    cbEspecie: TsComboBox;
    edMoeda: TsEdit;
    cbAceite: TsComboBox;
    edNosso: TsEdit;
    sGroupBox1: TsGroupBox;
    edI1: TsEdit;
    edI2: TsEdit;
    edI3: TsEdit;
    edI4: TsEdit;
    edI5: TsEdit;
    edEmissao: TsDateEdit;
    edNumero: TsEdit;
    edValor: TsCurrencyEdit;
    edVencimento: TsDateEdit;
    sGroupBox2: TsGroupBox;
    edNome: TsEdit;
    edDocumento: TsEdit;
    edEndereco: TsEdit;
    edBairro: TsEdit;
    edCidade: TsEdit;
    edUF: TsEdit;
    edCEP: TsEdit;
    btCancelar: TsBitBtn;
    btVisualizar: TsBitBtn;
    Boleto: TACBrBoleto;
    Quick: TACBrBoletoFCQuick;
    Fortes: TACBrBoletoFCFortes;
    btGerar: TsBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure btVisualizarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);
    procedure btGerarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }


  end;

var
  FBoleto: TFBoleto;

implementation

uses UDM, UPF, DB, UGeral, Math;

{$R *.dfm}

procedure TFBoleto.FormCreate(Sender: TObject);
begin
  Boleto.ACBrBoletoFC.DirLogo:=ExtractFilePath(Application.ExeName);
  dm.Tipos.Close;
  dm.Tipos.Open;
  cbEspecie.Items.Clear;
  while not dm.Tipos.Eof do
  begin
      if dm.TiposSIGLA.AsString<>'' then
      cbEspecie.Items.Add(dm.TiposSIGLA.AsString);
      dm.Tipos.Next;
  end;

  cbEspecie.ItemIndex           :=cbEspecie.IndexOf(PF.RetornarSigla(dm.TitulosTIPO_TITULO.AsInteger));
  edMoeda.Text                  :=dm.ValorParametro(56);
  cbAceite.ItemIndex            :=cbAceite.IndexOf(dm.TitulosACEITE.AsString);
  edI1.Text                     :=dm.ValorParametro(59);
  edI2.Text                     :=dm.ValorParametro(60);
  edI3.Text                     :=dm.ValorParametro(61);
  edI4.Text                     :=dm.ValorParametro(62);
  edI5.Text                     :=dm.ValorParametro(63);
  edNosso.Text                  :=dm.TitulosPROTOCOLO.AsString;
  edNumero.Text                 :=dm.TitulosPROTOCOLO.AsString;
  edValor.Value                 :=dm.TitulosSALDO_PROTESTO.AsFloat;

  if (dm.ServentiaCODIGO.AsInteger=1823) or (dm.ServentiaCODIGO.AsInteger=1866) or (dm.ServentiaCODIGO.AsInteger=1622) then
    edEmissao.Date:=Now
      else edEmissao.Date:=dm.TitulosDT_PROTOCOLO.AsDateTime;

  edVencimento.Date:=dm.TitulosDT_PRAZO.AsDateTime;

  if (dm.ServentiaCODIGO.AsInteger=1823) or (dm.ServentiaCODIGO.AsInteger=2369) or
     (dm.ServentiaCODIGO.AsInteger=1866) or (dm.ServentiaCODIGO.AsInteger=1622) then
  edVencimento.Date:=Now;

  edNome.Text                   :=dm.DevedoresNOME.AsString;
  edDocumento.Text              :=dm.DevedoresDOCUMENTO.AsString;
  edEndereco.Text               :=dm.DevedoresENDERECO.AsString;
  edCEP.Text                    :=dm.DevedoresCEP.AsString;
  edBairro.Text                 :=dm.DevedoresBAIRRO.AsString;
  edUF.Text                     :=dm.DevedoresUF.AsString;
  edCidade.Text                 :=dm.DevedoresMUNICIPIO.AsString;

  if cbAceite.Text='' then
  cbAceite.ItemIndex:=1;
end;

procedure TFBoleto.btVisualizarClick(Sender: TObject);
var
  Titulo: TACBrTitulo;
  Q: TSQLQuery;
begin
  if dm.ServentiaCODIGO.AsInteger=1823 then
  begin
      if (dm.TitulosSTATUS.AsString='PROTESTADO') or
         (dm.TitulosSTATUS.AsString='RETIRADO')   then
      begin
          GR.Aviso('T�tulo j� solucionado!');
          Exit;
      end;

      Q:=TSQLQuery.Create(Nil);
      Q.SQLConnection:=dm.Protesto;
      Q.SQL.Add('SELECT ID_BOLETO FROM BOLETOS WHERE DATA_EMISSAO=:DATA AND ID_ATO=:ID');
      Q.ParamByName('DATA').AsDate  :=edEmissao.Date;
      Q.ParamByName('ID').AsInteger :=dm.TitulosID_ATO.AsInteger;
      Q.Open;
      if not Q.IsEmpty then
      begin
          GR.Aviso('Boleto j� impresso para esta data!');
          Exit;
      end;
  end;

  if cbEspecie.Text='' then
  begin
      GR.Aviso('Informe a esp�cie do t�tulo!');
      cbEspecie.SetFocus;
      Exit;
  end;

  if edMoeda.Text='' then
  begin
      GR.Aviso('Informe a esp�cie da moeda!');
      edMoeda.SetFocus;
      Exit;
  end;

  if cbAceite.Text='' then
  begin
      GR.Aviso('Informe se o t�tulo foi aceite!');
      cbAceite.SetFocus;
      Exit;
  end;

  if edNosso.Text='' then
  begin
      GR.Aviso('Informe o nosso n�mero!');
      edNosso.SetFocus;
      Exit;
  end;

  if edNumero.Text='' then
  begin
      GR.Aviso('Informe o n�mero do documento!');
      edNumero.SetFocus;
      Exit;
  end;

  if edEmissao.Date=0 then
  begin
      GR.Aviso('Informe o data de emiss�o!');
      edEmissao.SetFocus;
      Exit;
  end;

  if edVencimento.Date=0 then
  begin
      GR.Aviso('Informe o data de vencimento!');
      edVencimento.SetFocus;
      Exit;
  end;

  if dm.ValorParametro(64)='' then dm.AtualizarParametro(64,'0');
  if dm.ValorParametro(65)='' then dm.AtualizarParametro(65,'0');
  if dm.ValorParametro(67)='' then dm.AtualizarParametro(67,'0');

  Boleto.ListadeBoletos.Clear;
  Titulo:=Boleto.CriarTituloNaLista;

  Boleto.Banco.Digito           :=StrToInt(dm.ValorParametro(67));
  Boleto.Banco.Nome             :=dm.ValorParametro(44);
  Boleto.Banco.Numero           :=StrToInt(dm.ValorParametro(66));
  Boleto.Cedente.Agencia        :=dm.ValorParametro(45);
  Boleto.Cedente.AgenciaDigito  :=dm.ValorParametro(64);
  Boleto.Cedente.CodigoCedente  :=dm.ValorParametro(68);
  Boleto.Cedente.Conta          :=dm.ValorParametro(46);
  Boleto.Cedente.ContaDigito    :=dm.ValorParametro(65);
  Boleto.Cedente.Nome           :=dm.ValorParametro(58);

  with Titulo do
  begin
      Vencimento                :=edVencimento.Date;
      DataDocumento             :=edEmissao.Date;
      NumeroDocumento           :=edNumero.Text;
      EspecieDoc                :=cbEspecie.Text;
      LocalPagamento            :=dm.ValorParametro(70);

      if cbAceite.ItemIndex=0 then
        Aceite:=atSim
          else Aceite:=atNao;

      DataProcessamento       :=Now;
      NossoNumero             :=edNosso.Text;
      Carteira                :=dm.ValorParametro(57);
      ValorDocumento          :=edValor.Value;
      Sacado.NomeSacado       :=edNome.Text;
      Sacado.CNPJCPF          :=GR.PegarNumeroTexto(edDocumento.Text);
      Sacado.Logradouro       :=edEndereco.Text;
      Sacado.Bairro           :=edBairro.Text;
      Sacado.CEP              :=GR.PegarNumeroTexto(edCEP.Text);
      Sacado.UF               :=edUF.Text;
      Sacado.Cidade           :=edCidade.Text;
      OcorrenciaOriginal.Tipo :=toRemessaBaixar;
      if edI1.Text<>'' then Mensagem.Add(edI1.Text);
      if edI2.Text<>'' then Mensagem.Add(edI2.Text);
      if edI3.Text<>'' then Mensagem.Add(edI3.Text);
      if edI4.Text<>'' then Mensagem.Add(edI4.Text);
      if edI5.Text<>'' then Mensagem.Add(edI5.Text);
  end;

  Boleto.Imprimir;

  if GR.Pergunta('Confirma a Emiss�o do Boleto') then
  begin
      dm.Boletos.Close;
      dm.Boletos.Params[0].AsInteger:=-1;
      dm.Boletos.Open;
      dm.Boletos.Append;
      dm.BoletosID_BOLETO.AsInteger         :=dm.IdAtual('ID_ATO','S');
      dm.BoletosID_ATO.AsInteger            :=dm.TitulosID_ATO.AsInteger;
      dm.BoletosID_DEVEDOR.AsInteger        :=dm.DevedoresID_DEVEDOR.AsInteger;
      dm.BoletosESPECIE_DOCUMENTO.AsString  :=cbEspecie.Text;
      dm.BoletosMOEDA.AsString              :=edMoeda.Text;
      if cbAceite.ItemIndex=0 then
        dm.BoletosACEITE.AsString:='S'
          else dm.BoletosACEITE.AsString:='N';
      dm.BoletosCARTEIRA.AsString           :=Titulo.Carteira;
      dm.BoletosNOSSO_NUMERO.AsString       :=Titulo.NossoNumero;
      dm.BoletosVALOR_MORA_JUROS.AsFloat    :=Titulo.ValorMoraJuros;
      dm.BoletosVALOR_DESCONTO.AsFloat      :=Titulo.ValorDesconto;
      dm.BoletosVALOR_ABATIMENTO.AsFloat    :=Titulo.ValorAbatimento;
      dm.BoletosPORCENTO_MULTA.AsFloat      :=Titulo.PercentualMulta;
      if Titulo.DataMoraJuros<>0 then
      dm.BoletosDATA_MULTA_JUROS.AsDateTime :=Titulo.DataMoraJuros;
      if Titulo.DataDesconto<>0 then
      dm.BoletosDATA_DESCONTO.AsDateTime    :=Titulo.DataDesconto;
      if Titulo.DataAbatimento<>0 then
      dm.BoletosDATA_ABATIMENTO.AsDateTime  :=Titulo.DataAbatimento;
      if Titulo.DataProtesto<>0 then
      dm.BoletosDATA_PROTESTO.AsDateTime    :=Titulo.DataProtesto;
      dm.BoletosNUMERO_DOCUMENTO.AsString   :=Titulo.NumeroDocumento;
      dm.BoletosVALOR.AsFloat               :=Titulo.ValorDocumento;
      if edEmissao.Date<>0 then
      dm.BoletosDATA_EMISSAO.AsDateTime     :=edEmissao.Date;
      if Titulo.Vencimento<>0 then
      dm.BoletosDATA_VENCIMENTO.AsDateTime  :=Titulo.Vencimento;
      dm.BoletosINSTRUCAO1.AsString         :=edI1.Text;
      dm.BoletosINSTRUCAO2.AsString         :=edI2.Text;
      dm.BoletosINSTRUCAO3.AsString         :=edI3.Text;
      dm.BoletosINSTRUCAO4.AsString         :=edI4.Text;
      dm.BoletosINSTRUCAO5.AsString         :=edI5.Text;
      if dm.TitulosDT_PROTOCOLO.AsDateTime<>0 then
      dm.BoletosDATA_PROTOCOLO.AsDateTime   :=dm.TitulosDT_PROTOCOLO.AsDateTime;
      dm.BoletosPROTOCOLO.AsInteger         :=dm.TitulosPROTOCOLO.AsInteger;
      dm.BoletosHORA.AsDateTime             :=Now;
      dm.BoletosEMISSOR.AsString            :=dm.vNome;
      dm.BoletosPAGO.AsString               :='N';

      dm.Adicional.Close;
      dm.Adicional.Params[0].AsInteger:=4028;
      dm.Adicional.Open;

      dm.BoletosEMOLUMENTOS.AsFloat         :=dm.AdicionalEMOL.AsFloat;
      dm.BoletosFETJ.AsFloat                :=dm.AdicionalFETJ.AsFloat;
      dm.BoletosFUNDPERJ.AsFloat            :=dm.AdicionalFUND.AsFloat;
      dm.BoletosFUNPERJ.AsFloat             :=dm.AdicionalFUNP.AsFloat;
      dm.BoletosFUNARPEN.AsFloat            :=dm.AdicionalFUNA.AsFloat;
      dm.BoletosTOTAL.AsFloat               :=dm.AdicionalEMOL.AsFloat+
                                              dm.AdicionalFETJ.AsFloat+
                                              dm.AdicionalFUND.AsFloat+
                                              dm.AdicionalFUNP.AsFloat+
                                              dm.AdicionalFUNA.AsFloat;
      dm.Boletos.Post;
      dm.Boletos.ApplyUpdates(0);

      dm.Movimento.Append;
      dm.MovimentoID_MOVIMENTO.AsInteger  :=dm.IdAtual('ID_MOVIMENTO','S');
      dm.MovimentoID_ATO.AsInteger        :=dm.TitulosID_ATO.AsInteger;
      dm.MovimentoDATA.AsDateTime         :=Now;
      dm.MovimentoHORA.AsDateTime         :=Time;
      dm.MovimentoBAIXA.AsDateTime        :=Now;
      dm.MovimentoESCREVENTE.AsString     :=dm.vNome;
      dm.MovimentoSTATUS.AsString         :='BOLETO IMPRESSO';
      dm.Movimento.Post;
      dm.Movimento.ApplyUpdates(0);

      if dm.ValorParametro(76)='S' then
        if GR.Pergunta('Deseja Exportar para PDF') then
         begin
             Fortes.NomeArquivo:=ExtractFilePath(Application.ExeName)+'\Boletos\'+Copy(edNome.Text,1,20)+' ('+FormatDateTime('dd-mm-yyyy_hh-mm',Now)+').pdf';
             Boleto.GerarPDF;
         end;

      Close;
  end;
end;

procedure TFBoleto.btCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TFBoleto.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFBoleto.btGerarClick(Sender: TObject);
var
  Titulo: TACBrTitulo;
begin
  if GR.Pergunta('Confirma a Emiss�o da Guia') then
  begin
      dm.Boletos.Close;
      dm.Boletos.Params[0].AsInteger:=-1;
      dm.Boletos.Open;
      dm.Boletos.Append;
      dm.BoletosID_BOLETO.AsInteger         :=dm.IdAtual('ID_ATO','S');
      dm.BoletosID_ATO.AsInteger            :=dm.TitulosID_ATO.AsInteger;
      dm.BoletosID_DEVEDOR.AsInteger        :=dm.DevedoresID_DEVEDOR.AsInteger;
      dm.BoletosESPECIE_DOCUMENTO.AsString  :=cbEspecie.Text;
      dm.BoletosMOEDA.AsString              :=edMoeda.Text;
      if cbAceite.ItemIndex=0 then
        dm.BoletosACEITE.AsString:='S'
          else dm.BoletosACEITE.AsString:='N';
      dm.BoletosCARTEIRA.AsString           :=dm.ValorParametro(57);
      dm.BoletosNOSSO_NUMERO.AsString       :=edNosso.Text;
      dm.BoletosVALOR_MORA_JUROS.AsFloat    :=Titulo.ValorMoraJuros;
      dm.BoletosVALOR_DESCONTO.AsFloat      :=Titulo.ValorDesconto;
      dm.BoletosVALOR_ABATIMENTO.AsFloat    :=Titulo.ValorAbatimento;
      dm.BoletosPORCENTO_MULTA.AsFloat      :=Titulo.PercentualMulta;
      if Titulo.DataMoraJuros<>0 then
      dm.BoletosDATA_MULTA_JUROS.AsDateTime :=Titulo.DataMoraJuros;
      if Titulo.DataDesconto<>0 then
      dm.BoletosDATA_DESCONTO.AsDateTime    :=Titulo.DataDesconto;
      if Titulo.DataAbatimento<>0 then
      dm.BoletosDATA_ABATIMENTO.AsDateTime  :=Titulo.DataAbatimento;
      if Titulo.DataProtesto<>0 then
      dm.BoletosDATA_PROTESTO.AsDateTime    :=Titulo.DataProtesto;
      dm.BoletosNUMERO_DOCUMENTO.AsString   :=edNumero.Text;
      dm.BoletosVALOR.AsFloat               :=edValor.Value;
      if edEmissao.Date<>0 then
      dm.BoletosDATA_EMISSAO.AsDateTime     :=edEmissao.Date;
      if edVencimento.Date<>0 then
      dm.BoletosDATA_VENCIMENTO.AsDateTime  :=edVencimento.Date;
      dm.BoletosINSTRUCAO1.AsString         :=edI1.Text;
      dm.BoletosINSTRUCAO2.AsString         :=edI2.Text;
      dm.BoletosINSTRUCAO3.AsString         :=edI3.Text;
      dm.BoletosINSTRUCAO4.AsString         :=edI4.Text;
      dm.BoletosINSTRUCAO5.AsString         :=edI5.Text;
      if dm.TitulosDT_PROTOCOLO.AsDateTime<>0 then
      dm.BoletosDATA_PROTOCOLO.AsDateTime   :=dm.TitulosDT_PROTOCOLO.AsDateTime;
      dm.BoletosPROTOCOLO.AsInteger         :=dm.TitulosPROTOCOLO.AsInteger;
      dm.BoletosHORA.AsDateTime             :=Now;
      dm.BoletosEMISSOR.AsString            :=dm.vNome;
      dm.BoletosPAGO.AsString               :='N';

      dm.Adicional.Close;
      dm.Adicional.Params[0].AsInteger:=4028;
      dm.Adicional.Open;

      dm.BoletosEMOLUMENTOS.AsFloat         :=dm.AdicionalEMOL.AsFloat;
      dm.BoletosFETJ.AsFloat                :=dm.AdicionalFETJ.AsFloat;
      dm.BoletosFUNDPERJ.AsFloat            :=dm.AdicionalFUND.AsFloat;
      dm.BoletosFUNPERJ.AsFloat             :=dm.AdicionalFUNP.AsFloat;
      dm.BoletosFUNARPEN.AsFloat            :=dm.AdicionalFUNA.AsFloat;
      dm.BoletosPMCMV.AsFloat               :=dm.AdicionalPMCMV.AsFloat;
      dm.BoletosTOTAL.AsFloat               :=dm.AdicionalEMOL.AsFloat+dm.AdicionalFETJ.AsFloat+
                                              dm.AdicionalFUND.AsFloat+dm.AdicionalFUNP.AsFloat+
                                              dm.AdicionalFUNA.AsFloat+dm.AdicionalPMCMV.AsFloat;

      dm.Boletos.Post;
      dm.Boletos.ApplyUpdates(0);
      GR.Aviso('Guia Gerada com Sucesso!');
      Close;
  end;
end;

procedure TFBoleto.FormShow(Sender: TObject);
begin
  if dm.ValorParametro(81)='cobNenhum' then
    if dm.ServentiaCODIGO.AsInteger=1823 then
      dm.AtualizarParametro(81,'cobBradesco')
        else
          if dm.ServentiaCODIGO.AsInteger=1866 then
            dm.AtualizarParametro(81,'cobItau')
              else
                if dm.ServentiaCODIGO.AsInteger=1415 then
                  dm.AtualizarParametro(81,'cobBradesco');

  if dm.ValorParametro(81)='cobNenhum'          then Boleto.Banco.TipoCobranca:=cobNenhum;
  if dm.ValorParametro(81)='cobBancoDoBrasil'   then Boleto.Banco.TipoCobranca:=cobBancoDoBrasil;
  if dm.ValorParametro(81)='cobSantander'       then Boleto.Banco.TipoCobranca:=cobSantander;
  if dm.ValorParametro(81)='cobCaixaEconomica'  then Boleto.Banco.TipoCobranca:=cobCaixaEconomica;
  if dm.ValorParametro(81)='cobCaixaSicob'      then Boleto.Banco.TipoCobranca:=cobCaixaSicob;
  if dm.ValorParametro(81)='cobBradesco'        then Boleto.Banco.TipoCobranca:=cobBradesco;
  if dm.ValorParametro(81)='cobItau'            then Boleto.Banco.TipoCobranca:=cobItau;
  if dm.ValorParametro(81)='cobBancoMercantil'  then Boleto.Banco.TipoCobranca:=cobBancoMercantil;
  if dm.ValorParametro(81)='cobSicred'          then Boleto.Banco.TipoCobranca:=cobSicred;
  if dm.ValorParametro(81)='cobBancoob'         then Boleto.Banco.TipoCobranca:=cobBancoob;
  if dm.ValorParametro(81)='cobBanrisul'        then Boleto.Banco.TipoCobranca:=cobBanrisul;
  if dm.ValorParametro(81)='cobBanestes'        then Boleto.Banco.TipoCobranca:=cobBanestes;
  if dm.ValorParametro(81)='cobHSBC'            then Boleto.Banco.TipoCobranca:=cobHSBC;
  if dm.ValorParametro(81)='cobBancoDoNordeste' then Boleto.Banco.TipoCobranca:=cobBancoDoNordeste;
  if dm.ValorParametro(81)='cobBRB'             then Boleto.Banco.TipoCobranca:=cobBRB;
end;

end.
