unit URelSustacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, ExtCtrls, sPanel, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, sGroupBox, sEdit, FMTBcd, DB, SqlExpr,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFRelSustacao = class(TForm)
    P1: TsPanel;
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    sGroupBox1: TsGroupBox;
    edInicial: TsEdit;
    edFinal: TsEdit;
    Protocolos: TFDQuery;
    ProtocolosDT_SUSTADO: TDateField;
    ProtocolosTIPO_SUSTACAO: TStringField;
    ProtocolosRETORNO_PROTESTO: TStringField;
    ProtocolosDT_RETORNO_PROTESTO: TDateField;
    ProtocolosDT_DEFINITIVA: TDateField;
    ProtocolosSELO_PAGAMENTO: TStringField;
    ProtocolosALEATORIO_SOLUCAO: TStringField;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSairClick(Sender: TObject);
    procedure edInicialKeyPress(Sender: TObject; var Key: Char);
    procedure edFinalKeyPress(Sender: TObject; var Key: Char);
    procedure btVisualizarClick(Sender: TObject);
    procedure edInicialExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelSustacao: TFRelSustacao;

implementation

uses UDM, UPF, UQuickSustacao1, UGeral;

{$R *.dfm}

procedure TFRelSustacao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFRelSustacao.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFRelSustacao.edInicialKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFRelSustacao.edFinalKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFRelSustacao.btVisualizarClick(Sender: TObject);
begin
  if edInicial.Text='' then Exit;

  Protocolos.Close;
  Protocolos.Params[0].AsInteger:=StrToInt(edInicial.Text);
  Protocolos.Params[1].AsInteger:=StrToInt(edFinal.Text);
  Protocolos.Open;

  if Protocolos.IsEmpty then
  begin
      GR.Aviso('Protocolo n�o encontrado.');
      Exit;
  end;

  PF.Aguarde(True);

  Application.CreateForm(TFQuickSustacao1,FQuickSustacao1);

  with FQuickSustacao1 do
  begin
      Protocolos.First;
      while not Protocolos.Eof do
      begin
          if ProtocolosTIPO_SUSTACAO.AsString='L' then
          lbCidade1.Caption:=dm.ServentiaCIDADE.AsString+' - RJ, '+FormatDateTime('dd "de" mmmm "de" yyyy.',ProtocolosDT_SUSTADO.AsDateTime);

          if ProtocolosTIPO_SUSTACAO.AsString='D' then
          lbCidade1.Caption:=dm.ServentiaCIDADE.AsString+' - RJ, '+FormatDateTime('dd "de" mmmm "de" yyyy.',ProtocolosDT_DEFINITIVA.AsDateTime);

          if ProtocolosTIPO_SUSTACAO.AsString='C' then
          lbCidade1.Caption:=dm.ServentiaCIDADE.AsString+' - RJ, '+FormatDateTime('dd "de" mmmm "de" yyyy.',ProtocolosDT_RETORNO_PROTESTO.AsDateTime);

          lbCidade2.Caption:=lbCidade1.Caption;
          lbCidade3.Caption:=lbCidade1.Caption;
          lbSeloEletronico.Caption:=GR.SeloFormatado(ProtocolosSELO_PAGAMENTO.AsString,ProtocolosALEATORIO_SOLUCAO.AsString);

          if ProtocolosTIPO_SUSTACAO.AsString='L' then
          begin
              lbAverbacao1.Enabled  :=True;
              lbLinha1.Enabled      :=True;
              lbLinha2.Enabled      :=True;
              lbLinha3.Enabled      :=True;
              lbLinha4.Enabled      :=True;
              lblinha5.Enabled      :=True;
              lbNomeJuiz1.Enabled   :=True;
              lbCidade1.Enabled     :=True;
              Cancelamento.Enabled  :=True;
              Definitiva.Enabled    :=False;
              Cancelamento.Enabled  :=False;
          end;

          if ProtocolosTIPO_SUSTACAO.AsString='D' then
          begin
              lbAverbacao1.Enabled  :=False;
              lbLinha1.Enabled      :=False;
              lbLinha2.Enabled      :=False;
              lbLinha3.Enabled      :=False;
              lbLinha4.Enabled      :=False;
              lblinha5.Enabled      :=False;
              lbNomeJuiz1.Enabled   :=False;
              lbCidade1.Enabled     :=False;
              Cancelamento.Enabled  :=False;
              Definitiva.Enabled    :=True;
          end;
                                  
          if ProtocolosTIPO_SUSTACAO.AsString='C' then
          begin
              lbAverbacao1.Enabled  :=False;
              lbLinha1.Enabled      :=False;
              lbLinha2.Enabled      :=False;
              lbLinha3.Enabled      :=False;
              lbLinha4.Enabled      :=False;
              lblinha5.Enabled      :=False;
              lbNomeJuiz1.Enabled   :=False;
              lbCidade1.Enabled     :=False;
              Definitiva.Enabled    :=False;
              Cancelamento.Enabled  :=True;
          end;
          Relatorio.Preview;
          Protocolos.Next;
      end;
      Free;
  end;
end;

procedure TFRelSustacao.edInicialExit(Sender: TObject);
begin
  if edFinal.Text='' then edFinal.Text:=edInicial.Text;
end;

end.
