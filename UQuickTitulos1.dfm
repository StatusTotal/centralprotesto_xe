�
 TFQUICKTITULOS1 0κ  TPF0TFQuickTitulos1FQuickTitulos1LeftJTop� CaptionFQuickTitulos1ClientHeight�ClientWidthyColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style OldCreateOrderScaledOnCreate
FormCreatePixelsPerInch`
TextHeight 	TQuickRep	QuickRep1LeftTop WidthcHeightShowingPreviewDataSet
qryTitulosFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' 	OnEndPageQuickRep1EndPageOptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.OrientationpoLandscapePage.PaperSizeA4Page.ContinuousPage.Values       �@      @�
@       �@      ��
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPreviewWidth�PreviewHeight�PrevInitialZoomqrZoomToFitPreviewDefaultSaveTypestQRPPreviewLeft 
PreviewTop  TQRBandQRBand1Left&Top&WidthHeight� Frame.DrawTop	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values      ��@      #�
@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageHeader TQRLabel
lbCartorioLeft�TopWidth� HeightSize.Values������j�@������*�	@UUUUUUU�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   Nome do CartórioColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbEnderecoLeft�Top Width8HeightSize.Values�������@      ��	@UUUUUUU�@������*�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption	   EndereçoColorclWhiteTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbTituloLeft�Top@Width4HeightSize.Values�������@UUUUUU_�	@UUUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption
   RelatórioColorclWhiteTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel
lbPortadorLeftTophWidth8HeightSize.Values�������@UUUUUUU�@UUUUUU��@������*�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionPortadorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsUnderline 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  
TQRSysData
QRSysData2Left�TophWidthTHeightSize.Values�������@UUUUUUm�
@UUUUUU��@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandColorclWhiteDataqrsPageNumberFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontText	   Página: Transparent	ExportAsexptTextFontSize   TQRChildBand
ChildBand1Left&Top� WidthHeightFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUUU�@      #�
@ PreCaluculateBandHeightKeepOnOnePage
ParentBandQRBand1
PrintOrdercboAfterParent TQRLabelQRLabel1LeftTopWidth.HeightSize.Values      ��@ XUUUUU�@ XUUUUU� @ �����j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	ProtocoloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel2LeftrTopWidth*HeightSize.Values      ��@      Ж@ XUUUUU� @      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionDevedorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel3Left`TopWidthFHeightSize.Values      ��@ `UUUU��@ XUUUUU� @ XUUUU5�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption   Valor do TítuloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel5Left�TopWidth@HeightSize.Values      ��@      ܐ	@ XUUUUU� @ XUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionEmolumentosColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel6LeftTopWidth*HeightSize.Values      ��@      ��	@UUUUUUU� @      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionSacadorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel7Left�TopWidth)HeightSize.Values      ��@ �����j�	@ XUUUUU� @ `UUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption   Nº TítuloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel8Left@TopWidth/HeightSize.Values      ��@ XUUUU��
@ XUUUUU� @ XUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	   Nosso NºColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbSituacaoLeft�TopWidth/HeightSize.Values      ��@ XUUUUk�
@ XUUUUU� @ XUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption
   SituaçãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   TQRBandQRBand2Left&Top� WidthHeightFrame.DrawLeft	Frame.DrawRight	AlignToBottomBeforePrintQRBand2BeforePrintTransparentBandForceNewColumnForceNewPageSize.ValuesVUUUUU��@      #�
@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail 	TQRDBText	QRDBText1LeftTopWidthaHeightSize.Values      ��@ XUUUUU�@ XUUUUU�@ �����R�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSet
qryTitulos	DataFieldDataProtocoloFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText2LeftrTopWidth� HeightSize.Values      ��@      Ж@ XUUUUU�@      ̘@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSet
qryTitulos	DataFieldDEVEDORFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText3Left`TopWidthFHeightSize.Values      ��@ `UUUU��@ XUUUUU�@ XUUUU5�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
qryTitulos	DataFieldVALOR_TITULOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText4Left�TopWidth@HeightSize.Values      ��@      ܐ	@ XUUUUU�@ XUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
qryTitulos	DataFieldTOTALFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText5LeftTopWidth� HeightSize.Values      ��@      ��	@UUUUUUU�@      �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSet
qryTitulos	DataFieldSACADORFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText6Left�TopWidthXHeightSize.Values      ��@ �����j�	@ XUUUUU�@ `UUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSet
qryTitulos	DataFieldNUMERO_TITULOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText7Left@TopWidthyHeightSize.Values      ��@ XUUUU��
@ XUUUUU�@ ������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSet
qryTitulos	DataFieldNOSSO_NUMEROFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
qrSituacaoLeft�TopWidthTHeightSize.Values      ��@ XUUUUk�
@ XUUUUU�@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSet
qryTitulos	DataFieldSTATUSFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize   TQRBandQRBand3Left&Top� WidthHeightFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values������j�@      #�
@ PreCaluculateBandHeightKeepOnOnePageBandType	rbSummary 
TQRSysData
QRSysData1LeftTopWidth� HeightSize.Values�������@UUUUUUU�@UUUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataqrsDetailCountFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontText   Quantidade de Títulos: Transparent	ExportAsexptTextFontSize  TQRExprQRExpr1LeftaTopWidthFHeightSize.Values ������@ �����~�@ XUUUUU�@ XUUUU5�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrintTransparent
ExpressionSum(Titulos.VALOR_TITULO)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr2Left�TopWidth@HeightSize.Values ������@      ܐ	@ XUUUUU�@ XUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrintTransparent
ExpressionSum(Titulos.TOTAL)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize    	TQuickRepTodosLeft&Top5WidthcHeightShowingPreviewDataSetqryTodosPortadoresFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' 	OnEndPageTodosEndPageOptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.OrientationpoLandscapePage.PaperSizeA4Page.ContinuousPage.Values       �@      @�
@       �@      ��
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPreviewWidth�PreviewHeight�PrevInitialZoomqrZoomToFitPreviewDefaultSaveTypestQRPPreviewLeft 
PreviewTop  TQRBandQRBand4Left&Top&WidthHeight� Frame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values      ��@      #�
@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageHeader TQRLabellbCartorio2Left�TopWidth� HeightSize.Values������j�@������*�	@��������@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   Nome do CartórioColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbEndereco2Left�TopCWidth/HeightSize.Values�������@UUUUUU]�	@UUUUUUE�@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption	   EndereçoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	lbTitulo2Left�Top`Width4HeightSize.Values�������@UUUUUU_�	@       �@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption
   RelatórioColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  
TQRSysData
QRSysData3Left�TophWidthTHeightSize.Values�������@UUUUUUm�
@UUUUUU��@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandColorclWhiteDataqrsPageNumberFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontText	   Página: Transparent	ExportAsexptTextFontSize  TQRLabelQRLabel4LefthTopWidthFHeightSize.Values�������@�������@      ��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption,   PROTESTO DE TÍTULOS E DOCUMENTOS DE DÍVIDAColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel
lbTabeliaoLeft�Top4Width)HeightSize.Values�������@UUUUUU[�	@UUUUUU��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption	   TabeliãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   TQRBandDetalheLeft&Top� WidthHeightFrame.DrawLeft	Frame.DrawRight	AlignToBottomBeforePrintQRBand2BeforePrintTransparentBandForceNewColumnForceNewPageSize.ValuesVUUUUU��@      #�
@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail 	TQRDBText	QRDBText8LeftTopWidthEHeightSize.Values      ��@������*�@UUUUUUU�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetqryTodosPortadores	DataField	PROTOCOLOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText9LeftRTopWidth� HeightSize.Values      ��@UUUUUU��@UUUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetqryTodosPortadores	DataFieldDEVEDORFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText10Left� TopWidthzHeightSize.Values      ��@�������@UUUUUUU�@UUUUUUe�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetqryTodosPortadores	DataFieldTipoDocumentoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText12Left�TopWidthDHeightSize.Values      ��@������>�	@UUUUUUU�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetqryTodosPortadores	DataFieldVALOR_TITULOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText13Left�TopWidth.HeightSize.Values      ��@TUUUUU��	@UUUUUUU�@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetqryTodosPortadores	DataFieldcFunperjFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText14LeftQTopWidth3HeightSize.Values      ��@      e�
@UUUUUUU�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetqryTodosPortadores	DataFieldcAcoterjFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText15Left�TopWidth7HeightSize.Values      ��@������(�
@UUUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetqryTodosPortadores	DataFieldcTotalFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText17Left'TopWidth'HeightSize.Values      ��@      s�
@UUUUUUU�@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetqryTodosPortadores	DataFieldcMutuaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText18LeftTTopWidth5HeightSize.Values      ��@UUUUUU�	@UUUUUUU�@������:�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetqryTodosPortadores	DataField	cFundperjFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText19Left$TopWidth,HeightSize.Values      ��@UUUUUU=�	@UUUUUUU�@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetqryTodosPortadores	DataFieldcFetjFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText20Left�TopWidth2HeightSize.Values      ��@�������	@UUUUUUU�@������J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetqryTodosPortadores	DataFieldcEmolumentosFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText11Left�TopWidthPHeightSize.Values      ��@�������
@UUUUUUU�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetqryTodosPortadores	DataFieldSTATUSFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText21Left�TopWidth;HeightSize.Values      ��@��������	@UUUUUUU�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetqryTodosPortadores	DataField	cFunarpenFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText22Left�TopWidth'HeightSize.Values      ��@      �	@UUUUUUU�@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetqryTodosPortadores	DataFieldcPmcmvFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText23Left|TopWidth"HeightSize.Values      ��@������Z�@UUUUUUU�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetqryTodosPortadores	DataFieldCODIGOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize   TQRBandFooterLeft&Top� WidthHeightFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomBeforePrintFooterBeforePrintTransparentBandForceNewColumnForceNewPageLinkBandDetalheSize.Values������j�@      #�
@ PreCaluculateBandHeightKeepOnOnePageBandTyperbGroupFooter TQRExprQRExpr3LeftQTopWidth3HeightSize.Values      ��@      e�
@��������@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cACOTERJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr4Left�TopWidth7HeightSize.Values      ��@������(�
@��������@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cTotal)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr5Left'TopWidth'HeightSize.Values      ��@      s�
@��������@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cMUTUA)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr6Left�TopWidth.HeightSize.Values      ��@TUUUUU��	@��������@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cFUNPERJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr7LeftTTopWidth5HeightSize.Values      ��@UUUUUU�	@��������@������:�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cFUNDPERJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr8Left$TopWidth,HeightSize.Values      ��@UUUUUU=�	@��������@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cFETJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr9Left�TopWidth2HeightSize.Values      ��@�������	@��������@������J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
Expression!Sum(TodosPortadores.cEMOLUMENTOS)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr10Left�TopWidthDHeightSize.Values      ��@������>�	@��������@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
Expression!Sum(TodosPortadores.VALOR_TITULO)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRLabellbQtdLeftTopWidth?HeightSize.Values      ��@������*�@��������@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption
QuantidadeColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRExprQRExpr19Left�TopWidth;HeightSize.Values      ��@��������	@��������@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cFUNARPEN)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr21Left�TopWidth'HeightSize.Values      ��@      �	@��������@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cPMCMV)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize   TQRGroupGrupoLeft&Top� WidthHeight'Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values      `�@      #�
@ PreCaluculateBandHeightKeepOnOnePage
ExpressionTodosPortadores.Apresentante
FooterBandFooterMasterTodosReprintOnNewPage TQRLabel	QRLabel12LeftTopWidth.HeightSize.Values      ��@������*�@������j�@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	ProtocoloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel13LeftRTopWidth*HeightSize.Values      ��@UUUUUU��@������j�@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionDevedorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText16LeftTopWidthhHeightSize.Values�������@������*�@       �@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhite	DataFieldAPRESENTANTEFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize
  TQRLabel	QRLabel11Left�TopWidth2HeightSize.Values      ��@�������	@������j�@������J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionEmol.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel14Left$TopWidth,HeightSize.Values      ��@UUUUUU=�	@������j�@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionFETJColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel15LeftTTopWidth5HeightSize.Values      ��@UUUUUU�	@������j�@������:�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionFUNDPERJColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel16Left�TopWidth.HeightSize.Values      ��@TUUUUU��	@������j�@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionFUNPERJColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel17Left'TopWidth'HeightSize.Values      ��@      s�
@������j�@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption   MÚTUAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel18LeftQTopWidth3HeightSize.Values      ��@      e�
@������j�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionACOTERJColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel19Left�TopWidth$HeightSize.Values      ��@      M�
@������j�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionTOTALColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel20Left� TopWidthQHeightSize.Values      ��@�������@������j�@      P�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionTipo DocumentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel21Left�TopWidthDHeightSize.Values      ��@������>�	@������j�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption
Valor Doc.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel22Left�TopWidth-HeightSize.Values      ��@�������
@������j�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption
   SituaçãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel9Left�TopWidth;HeightSize.Values      ��@��������	@������j�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionFUNARPENColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel10Left�TopWidth'HeightSize.Values      ��@      �	@������j�@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionPMCMVColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel23Left|TopWidth"HeightSize.Values      ��@������Z�@������j�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption   CódigoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   TQRBandQRBand5Left&Top� WidthHeight*Frame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomBeforePrintQRBand5BeforePrintColor��� TransparentBandForceNewColumnForceNewPageSize.Values     @�@      #�
@ PreCaluculateBandHeightKeepOnOnePageBandType	rbSummary TQRExprQRExpr11Left�TopWidthDHeightSize.Values      ��@������>�	@      ��@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrintTransparent	
Expression!Sum(TodosPortadores.VALOR_TITULO)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr12LeftQTopWidth3HeightSize.Values      ��@      e�
@      ��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cACOTERJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr13Left�TopWidth7HeightSize.Values      ��@������(�
@      ��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cTotal)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr14Left'TopWidth'HeightSize.Values      ��@      s�
@      ��@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cMUTUA)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr15Left�TopWidth.HeightSize.Values      ��@TUUUUU��	@      ��@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cFUNPERJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr16LeftTTopWidth5HeightSize.Values      ��@UUUUUU�	@      ��@������:�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cFUNDPERJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr17Left$TopWidth,HeightSize.Values      ��@UUUUUU=�	@      ��@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cFETJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr18Left�TopWidth2HeightSize.Values      ��@�������	@      ��@������J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
Expression!Sum(TodosPortadores.cEMOLUMENTOS)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRLabellbQgrLeftTopWidth?HeightSize.Values      ��@������*�@      ��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption
QuantidadeColor��� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRExprQRExpr20Left�TopWidth;HeightSize.Values      ��@��������	@      ��@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cFUNARPEN)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr22Left�TopWidth'HeightSize.Values      ��@      �	@      ��@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.cPMCMV)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize    TFDQuery
qryTitulosOnCalcFieldsqryTitulosCalcFields
Connectiondm.conSISTEMASQL.Stringscselect PROTOCOLO, DT_PROTOCOLO, TOTAL, NUMERO_TITULO, VALOR_TITULO, NOSSO_NUMERO, SACADOR, DEVEDOR,aSTATUS,CODIGO from TITULOS where CODIGO_APRESENTANTE=:CODIGO and DT_PROTOCOLO between :D1 and :D2 Left^TopV	ParamDataNameCODIGODataType	ftInteger	ParamTypeptInput NameD1DataType
ftDateTime	ParamTypeptInput NameD2DataType
ftDateTime	ParamTypeptInput   TIntegerFieldqryTitulosPROTOCOLO	FieldName	PROTOCOLOOrigin	PROTOCOLO  
TDateFieldqryTitulosDT_PROTOCOLO	FieldNameDT_PROTOCOLOOriginDT_PROTOCOLO  TFloatFieldqryTitulosTOTAL	FieldNameTOTALOriginTOTAL	OnGetTextqryTitulosTOTALGetText  TStringFieldqryTitulosNUMERO_TITULO	FieldNameNUMERO_TITULOOriginNUMERO_TITULO  TFloatFieldqryTitulosVALOR_TITULO	FieldNameVALOR_TITULOOriginVALOR_TITULO	OnGetTextqryTitulosVALOR_TITULOGetText  TStringFieldqryTitulosNOSSO_NUMERO	FieldNameNOSSO_NUMEROOriginNOSSO_NUMEROSize  TStringFieldqryTitulosSACADOR	FieldNameSACADOROriginSACADORSized  TStringFieldqryTitulosDEVEDOR	FieldNameDEVEDOROriginDEVEDORSized  TStringFieldqryTitulosSTATUS	FieldNameSTATUSOriginSTATUS  TIntegerFieldqryTitulosCODIGO	FieldNameCODIGOOriginCODIGO  TStringFieldqryTitulosDataProtocolo	FieldKindfkInternalCalc	FieldNameDataProtocoloSize   TFDQueryqryTodosPortadoresOnCalcFieldsqryTodosPortadoresCalcFields
Connectiondm.conSISTEMASQL.Stringsselect     APRESENTANTE,
PROTOCOLO,DEVEDOR,EMOLUMENTOS,FETJ,	FUNDPERJ,FUNPERJ,	FUNARPEN,PMCMV,MUTUA,ACOTERJ,DISTRIBUICAO,TOTAL,TIPO_TITULO,VALOR_TITULO,STATUS,	CONVENIO,CODIGO    3from TITULOS where DT_PROTOCOLO between :D1 and :D2    ORDER BY PROTOCOLO Left� TopV	ParamDataNameD1DataTypeftDate	ParamTypeptInput NameD2DataTypeftDate	ParamTypeptInput   TStringFieldqryTodosPortadoresAPRESENTANTE	FieldNameAPRESENTANTEOriginAPRESENTANTESized  TIntegerFieldqryTodosPortadoresPROTOCOLO	FieldName	PROTOCOLOOrigin	PROTOCOLO  TStringFieldqryTodosPortadoresDEVEDOR	FieldNameDEVEDOROriginDEVEDORSized  TFloatFieldqryTodosPortadoresEMOLUMENTOS	FieldNameEMOLUMENTOSOriginEMOLUMENTOS	OnGetText$qryTodosPortadoresEMOLUMENTOSGetText  TFloatFieldqryTodosPortadoresFETJ	FieldNameFETJOriginFETJ	OnGetTextqryTodosPortadoresFETJGetText  TFloatFieldqryTodosPortadoresFUNDPERJ	FieldNameFUNDPERJOriginFUNDPERJ	OnGetText!qryTodosPortadoresFUNDPERJGetText  TFloatFieldqryTodosPortadoresFUNPERJ	FieldNameFUNPERJOriginFUNPERJ	OnGetText qryTodosPortadoresFUNPERJGetText  TFloatFieldqryTodosPortadoresFUNARPEN	FieldNameFUNARPENOriginFUNARPEN	OnGetText!qryTodosPortadoresFUNARPENGetText  TFloatFieldqryTodosPortadoresPMCMV	FieldNamePMCMVOriginPMCMV	OnGetTextqryTodosPortadoresPMCMVGetText  TFloatFieldqryTodosPortadoresMUTUA	FieldNameMUTUAOriginMUTUA	OnGetTextqryTodosPortadoresMUTUAGetText  TFloatFieldqryTodosPortadoresACOTERJ	FieldNameACOTERJOriginACOTERJ  TFloatFieldqryTodosPortadoresDISTRIBUICAO	FieldNameDISTRIBUICAOOriginDISTRIBUICAO	OnGetText%qryTodosPortadoresDISTRIBUICAOGetText  TFloatFieldqryTodosPortadoresTOTAL	FieldNameTOTALOriginTOTAL	OnGetTextqryTodosPortadoresTOTALGetText  TIntegerFieldqryTodosPortadoresTIPO_TITULO	FieldNameTIPO_TITULOOriginTIPO_TITULO  TFloatFieldqryTodosPortadoresVALOR_TITULO	FieldNameVALOR_TITULOOriginVALOR_TITULO  TStringFieldqryTodosPortadoresSTATUS	FieldNameSTATUSOriginSTATUS  TStringFieldqryTodosPortadoresCONVENIO	FieldNameCONVENIOOriginCONVENIO	FixedChar	Size  TIntegerFieldqryTodosPortadoresCODIGO	FieldNameCODIGOOriginCODIGO  TStringFieldqryTodosPortadoresTipoDocumento	FieldKindfkInternalCalc	FieldNameTipoDocumentoSize(  TFloatFieldqryTodosPortadorescEmolumentos	FieldKindfkInternalCalc	FieldNamecEmolumentos	OnGetText%qryTodosPortadorescEmolumentosGetText  TFloatFieldqryTodosPortadorescFetj	FieldKindfkInternalCalc	FieldNamecFetj	OnGetTextqryTodosPortadorescFetjGetText  TFloatFieldqryTodosPortadorescFundperj	FieldKindfkInternalCalc	FieldName	cFundperj	OnGetText"qryTodosPortadorescFundperjGetText  TFloatFieldqryTodosPortadorescFunperj	FieldKindfkInternalCalc	FieldNamecFunperj	OnGetText!qryTodosPortadorescFunperjGetText  TFloatFieldqryTodosPortadorescFunarpen	FieldKindfkInternalCalc	FieldName	cFunarpen	OnGetText"qryTodosPortadorescFunarpenGetText  TFloatFieldqryTodosPortadorescPmcmv	FieldKindfkInternalCalc	FieldNamecPmcmv	OnGetTextqryTodosPortadorescPmcmvGetText  TFloatFieldqryTodosPortadorescMutua	FieldKindfkInternalCalc	FieldNamecMutua	OnGetTextqryTodosPortadorescMutuaGetText  TFloatFieldqryTodosPortadorescAcoterj	FieldKindfkInternalCalc	FieldNamecAcoterj	OnGetText!qryTodosPortadorescAcoterjGetText  TFloatFieldqryTodosPortadorescDistribuicao	FieldKindfkInternalCalc	FieldNamecDistribuicao	OnGetText&qryTodosPortadorescDistribuicaoGetText  TFloatFieldqryTodosPortadorescTotal	FieldKindfkInternalCalc	FieldNamecTotal	OnGetTextqryTodosPortadorescTotalGetText    