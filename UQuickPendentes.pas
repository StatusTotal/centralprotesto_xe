unit UQuickPendentes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QuickRpt, QRCtrls, ExtCtrls, DB, DBClient, SimpleDS,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFQuickPendentes = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    lbCartorio: TQRLabel;
    lbEndereco: TQRLabel;
    lbTitulo: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    lbPortador: TQRLabel;
    QRBand2: TQRBand;
    QRBand3: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRSysData1: TQRSysData;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRSysData2: TQRSysData;
    lbSituacao: TQRLabel;
    qrSituacao: TQRDBText;
    Todos: TQuickRep;
    QRBand4: TQRBand;
    lbCartorio2: TQRLabel;
    lbEndereco2: TQRLabel;
    lbTitulo2: TQRLabel;
    QRSysData3: TQRSysData;
    Detalhe: TQRBand;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    Footer: TQRBand;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    Grupo: TQRGroup;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRDBText16: TQRDBText;
    QRLabel11: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel22: TQRLabel;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    QRExpr7: TQRExpr;
    QRExpr8: TQRExpr;
    QRExpr9: TQRExpr;
    QRExpr10: TQRExpr;
    lbQtd: TQRLabel;
    QRLabel4: TQRLabel;
    lbTabeliao: TQRLabel;
    QRBand5: TQRBand;
    QRExpr11: TQRExpr;
    QRExpr12: TQRExpr;
    QRExpr13: TQRExpr;
    QRExpr14: TQRExpr;
    QRExpr15: TQRExpr;
    QRExpr16: TQRExpr;
    QRExpr17: TQRExpr;
    QRExpr18: TQRExpr;
    lbQgr: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText21: TQRDBText;
    QRLabel10: TQRLabel;
    QRExpr19: TQRExpr;
    QRExpr20: TQRExpr;
    QRDBText22: TQRDBText;
    QRExpr21: TQRExpr;
    QRExpr22: TQRExpr;
    Titulos: TFDQuery;
    TodosPortadores: TFDQuery;
    TodosPortadoresAPRESENTANTE: TStringField;
    TodosPortadoresPROTOCOLO: TIntegerField;
    TodosPortadoresDEVEDOR: TStringField;
    TodosPortadoresEMOLUMENTOS: TFloatField;
    TodosPortadoresFETJ: TFloatField;
    TodosPortadoresFUNDPERJ: TFloatField;
    TodosPortadoresFUNPERJ: TFloatField;
    TodosPortadoresFUNARPEN: TFloatField;
    TodosPortadoresPMCMV: TFloatField;
    TodosPortadoresMUTUA: TFloatField;
    TodosPortadoresACOTERJ: TFloatField;
    TodosPortadoresTOTAL: TFloatField;
    TodosPortadoresTIPO_TITULO: TIntegerField;
    TodosPortadoresVALOR_TITULO: TFloatField;
    TodosPortadoresSTATUS: TStringField;
    TodosPortadoresCONVENIO: TStringField;
    TitulosPROTOCOLO: TIntegerField;
    TitulosDT_PROTOCOLO: TDateField;
    TitulosTOTAL: TFloatField;
    TitulosNUMERO_TITULO: TStringField;
    TitulosVALOR_TITULO: TFloatField;
    TitulosNOSSO_NUMERO: TStringField;
    TitulosSACADOR: TStringField;
    TitulosDEVEDOR: TStringField;
    TitulosSTATUS: TStringField;
    TodosPortadoresTipoDocumento: TStringField;
    TitulosDataProtocolo: TStringField;
    procedure TitulosCalcFields(DataSet: TDataSet);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure TodosPortadoresCalcFields(DataSet: TDataSet);
    procedure QuickRep1EndPage(Sender: TCustomQuickRep);
    procedure TodosEndPage(Sender: TCustomQuickRep);
    procedure FooterBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    Qtd,Qgr: Integer;
  public
    { Public declarations }
  end;

var
  FQuickPendentes: TFQuickPendentes;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFQuickPendentes.TitulosCalcFields(DataSet: TDataSet);
begin
  TitulosDataProtocolo.AsString:=PF.FormatarData(TitulosDT_PROTOCOLO.AsDateTime,'N')+' - '+TitulosPROTOCOLO.AsString;
end;

procedure TFQuickPendentes.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if dm.ServentiaCODIGO.AsInteger=1208 then
    if TitulosSTATUS.AsString='APONTADO' then
    begin
        lbSituacao.Enabled:=False;
        qrSituacao.Enabled:=False;
    end
    else
    begin
        lbSituacao.Enabled:=True;
        qrSituacao.Enabled:=True;
    end;

  Inc(Qtd);
  Inc(Qgr);
end;

procedure TFQuickPendentes.FormCreate(Sender: TObject);
begin
  Qtd:=0;
  Qgr:=0;
  Titulos.Connection:=dm.conSISTEMA;
end;

procedure TFQuickPendentes.TodosPortadoresCalcFields(DataSet: TDataSet);
begin
  TodosPortadoresTipoDocumento.AsString:=PF.RetornarTitulo(TodosPortadoresTIPO_TITULO.AsInteger);
end;

procedure TFQuickPendentes.QuickRep1EndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickPendentes.TodosEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickPendentes.FooterBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbQtd.Caption:='Quantidade: '+IntToStr(Qtd);
  Qtd:=0;
end;

procedure TFQuickPendentes.QRBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbQgr.Caption:='Quantidade: '+IntToStr(Qgr);
  Qgr:=0;
end;

end.
