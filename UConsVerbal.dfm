object FConsVerbal: TFConsVerbal
  Left = 282
  Top = 275
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Informa'#231#227'o Verbal - Consulta'
  ClientHeight = 377
  ClientWidth = 682
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 15
  object btIncluir: TsBitBtn
    Left = 8
    Top = 329
    Width = 93
    Height = 38
    Cursor = crHandPoint
    Caption = 'Incluir'
    TabOrder = 0
    OnClick = btIncluirClick
    ImageIndex = 5
    Images = Gdm.Im24
    SkinData.SkinSection = 'BUTTON'
  end
  object btAlterar: TsBitBtn
    Left = 109
    Top = 329
    Width = 93
    Height = 38
    Cursor = crHandPoint
    Caption = 'Alterar'
    TabOrder = 1
    OnClick = btAlterarClick
    ImageIndex = 7
    Images = Gdm.Im24
    SkinData.SkinSection = 'BUTTON'
  end
  object btExcluir: TsBitBtn
    Left = 210
    Top = 329
    Width = 93
    Height = 38
    Cursor = crHandPoint
    Caption = 'Excluir'
    TabOrder = 2
    OnClick = btExcluirClick
    ImageIndex = 6
    Images = Gdm.Im24
    SkinData.SkinSection = 'BUTTON'
  end
  object P1: TsPanel
    Left = 8
    Top = 8
    Width = 667
    Height = 53
    TabOrder = 3
    SkinData.SkinSection = 'PANEL'
    object btLocalizar: TsBitBtn
      Left = 139
      Top = 7
      Width = 95
      Height = 38
      Cursor = crHandPoint
      Caption = 'Localizar'
      TabOrder = 0
      OnClick = btLocalizarClick
      ImageIndex = 8
      Images = Gdm.Im24
      SkinData.SkinSection = 'BUTTON'
    end
    object edData: TsDateEdit
      Left = 40
      Top = 17
      Width = 89
      Height = 22
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Blend = 0
    end
  end
  object Grid: TsDBGrid
    Left = 8
    Top = 67
    Width = 667
    Height = 253
    Cursor = crHandPoint
    Color = clWhite
    DataSource = dsVerbal
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 4
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Arial'
    TitleFont.Style = []
    OnDblClick = GridDblClick
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DATA'
        Title.Alignment = taCenter
        Title.Caption = 'Data'
        Width = 74
        Visible = True
      end
      item
        Alignment = taCenter
        Color = 8454016
        Expanded = False
        FieldName = 'CCT'
        Title.Alignment = taCenter
        Width = 87
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NUMERO'
        Title.Alignment = taCenter
        Title.Caption = 'N'#186' Recibo'
        Width = 124
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME'
        Title.Alignment = taCenter
        Title.Caption = 'Nome solicitado'
        Width = 270
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TOTAL'
        Title.Alignment = taCenter
        Title.Caption = 'Total'
        Width = 74
        Visible = True
      end>
  end
  object dsVerbal: TDataSource
    Left = 144
    Top = 232
  end
end
