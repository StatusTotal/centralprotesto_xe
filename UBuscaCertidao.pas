unit UBuscaCertidao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, sEdit, Grids, Wwdbigrd, Wwdbgrid,
  ExtCtrls, sPanel, FMTBcd, SqlExpr, Provider, DB, DBClient, sGroupBox,
  sComboBox, sRadioButton, sCheckBox, jpeg;

type
  TFBuscaCertidao = class(TForm)
    P2: TsPanel;
    Grid1: TwwDBGrid;
    edLocalizar: TsEdit;
    dsConsulta: TDataSource;
    Consulta: TClientDataSet;
    ConsultaDEVEDOR: TStringField;
    dspConsulta: TDataSetProvider;
    dtsConsulta: TSQLDataSet;
    dtsConsultaDEVEDOR: TStringField;
    ConsultaNome: TStringField;
    ConsultaDocumento: TStringField;
    dtsConsultaCPF_CNPJ_DEVEDOR: TStringField;
    ConsultaCPF_CNPJ_DEVEDOR: TStringField;
    btPedido: TsBitBtn;
    P3: TsPanel;
    Grid2: TwwDBGrid;
    dsProtocolo: TDataSource;
    Protocolo: TClientDataSet;
    dspProtocolo: TDataSetProvider;
    dtsProtocolo: TSQLDataSet;
    ProtocoloPROTOCOLO: TIntegerField;
    ProtocoloDT_PROTOCOLO: TDateField;
    ProtocoloLIVRO_REGISTRO: TIntegerField;
    ProtocoloFOLHA_REGISTRO: TStringField;
    ProtocoloNUMERO_TITULO: TStringField;
    ProtocoloAPRESENTANTE: TStringField;
    ProtocoloID_ATO: TIntegerField;
    P1: TsPanel;
    Rb3: TsRadioButton;
    Rb1: TsRadioButton;
    Rb2: TsRadioButton;
    ProtocoloSTATUS: TStringField;
    ProtocoloDT_REGISTRO: TDateField;
    btConsultar: TsBitBtn;
    sPanel1: TsPanel;
    PA: TsPanel;
    Image1: TImage;
    Memo1: TMemo;
    ProtocoloCONVENIO: TStringField;
    ProtocoloMUTUA: TFloatField;
    ProtocoloACOTERJ: TFloatField;
    ProtocoloDISTRIBUICAO: TFloatField;
    ProtocoloTOTAL: TFloatField;
    ProtocoloCODIGO: TIntegerField;
    ProtocoloTIPO_APRESENTACAO: TStringField;
    ProtocoloSALDO_TITULO: TFloatField;
    ProtocoloVALOR_TITULO: TFloatField;
    dtsProtocoloID_ATO: TIntegerField;
    dtsProtocoloCODIGO: TIntegerField;
    dtsProtocoloPROTOCOLO: TIntegerField;
    dtsProtocoloDT_PROTOCOLO: TDateField;
    dtsProtocoloLIVRO_REGISTRO: TIntegerField;
    dtsProtocoloFOLHA_REGISTRO: TStringField;
    dtsProtocoloNUMERO_TITULO: TStringField;
    dtsProtocoloAPRESENTANTE: TStringField;
    dtsProtocoloDT_REGISTRO: TDateField;
    dtsProtocoloSTATUS: TStringField;
    dtsProtocoloCONVENIO: TStringField;
    dtsProtocoloMUTUA: TFloatField;
    dtsProtocoloACOTERJ: TFloatField;
    dtsProtocoloDISTRIBUICAO: TFloatField;
    dtsProtocoloTOTAL: TFloatField;
    dtsProtocoloTIPO_APRESENTACAO: TStringField;
    dtsProtocoloSALDO_TITULO: TFloatField;
    dtsProtocoloVALOR_TITULO: TFloatField;
    cbFiltro: TsComboBox;
    Rb4: TsRadioButton;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ConsultaCalcFields(DataSet: TDataSet);
    procedure btPedidoClick(Sender: TObject);
    procedure dsConsultaDataChange(Sender: TObject; Field: TField);
    procedure Rb3Click(Sender: TObject);
    procedure Rb1Click(Sender: TObject);
    procedure Rb2Click(Sender: TObject);
    procedure Grid2DblClick(Sender: TObject);
    procedure btConsultarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbFiltroKeyPress(Sender: TObject; var Key: Char);
    procedure edLocalizarKeyPress(Sender: TObject; var Key: Char);
    procedure cbFiltroClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Rb4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FBuscaCertidao: TFBuscaCertidao;
    HintWnd: THintWindow;

implementation

uses UDM, UPF, UCertidao, UPedidoCertidao, DateUtils, UTitulo, UPrincipal, UGeral;

{$R *.dfm}

procedure TFBuscaCertidao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFBuscaCertidao.ConsultaCalcFields(DataSet: TDataSet);
begin
  if Length(ConsultaCPF_CNPJ_DEVEDOR.AsString)>11 then
    ConsultaDocumento.AsString:=PF.FormatarCNPJ(ConsultaCPF_CNPJ_DEVEDOR.AsString)
      else ConsultaDocumento.AsString:=PF.FormatarCPF(ConsultaCPF_CNPJ_DEVEDOR.AsString);

  ConsultaNome.AsString:=ConsultaDEVEDOR.AsString;
end;

procedure TFBuscaCertidao.btPedidoClick(Sender: TObject);
var
  Ano1,Ano2,Periodo: Integer;
begin
  if (cbFiltro.Text='N� do T�tulo') and (edLocalizar.Text='') then
  begin
      GR.Aviso('INFORME O N� DO T�TULO QUE DESEJA CONSULTAR!');
      edLocalizar.SetFocus;
      Exit;
  end;

  dm.Certidoes.Close;
  dm.Certidoes.Params[0].AsInteger:=-1;
  dm.Certidoes.Open;
  dm.Certidoes.Append;

  dm.CertidoesDT_PEDIDO.AsDateTime    :=Now;
  dm.CertidoesDT_ENTREGA.AsDateTime   :=PF.DataFinalFeriados(dm.CertidoesDT_PEDIDO.AsDateTime,StrToInt(dm.ValorParametro(12)));
  dm.CertidoesCODIGO.AsInteger        :=GR.iif(Rb2.Checked,4010,4022);
  dm.CertidoesNUMERO_TITULO.AsString  :=GR.iif(cbFiltro.Text='N� do T�tulo',edLocalizar.Text,'');
  dm.CertidoesCOBRANCA.AsString       :='CC';
  dm.CertidoesCONVENIO.AsString       :='N';
  dm.CertidoesANOS.AsInteger          :=StrToInt(dm.ValorParametro(30));
  dm.CertidoesCONVENIO.AsString       :=GR.iif((Protocolo.Active) and (not Protocolo.IsEmpty),ProtocoloCONVENIO.AsString,'N');
  dm.CertidoesENVIADO.AsString        :='N';


  if dm.CertidoesCONVENIO.AsString='S' then
    dm.vApontamento:=dm.MontarTabelaConvenio(ProtocoloCODIGO.AsInteger,
                                             ProtocoloID_ATO.AsInteger,
                                             GR.iif(ProtocoloSALDO_TITULO.AsFloat<>0,
                                                    ProtocoloSALDO_TITULO.AsFloat,
                                                    ProtocoloVALOR_TITULO.AsFloat))
    else
      dm.vApontamento:=0;

  if Consulta.IsEmpty then
  begin
      if (cbFiltro.ItemIndex=0) and (edLocalizar.Text<>'') then dm.CertidoesCPF_CNPJ_REQUERIDO.AsString:=edLocalizar.Text;
      if (cbFiltro.ItemIndex=1) and (edLocalizar.Text<>'') then dm.CertidoesREQUERIDO.AsString:=edLocalizar.Text;
  end
  else
  begin
      dm.CertidoesCPF_CNPJ_REQUERIDO.AsString:=GR.PegarNumeroTexto(ConsultaDocumento.AsString);
      dm.CertidoesREQUERIDO.AsString:=ConsultaNome.AsString;
  end;

  if Length(dm.CertidoesCPF_CNPJ_REQUERIDO.AsString)>11 then
    dm.CertidoesTIPO_REQUERIDO.AsString:='J'
      else dm.CertidoesTIPO_REQUERIDO.AsString:='F';

  if StrToInt(dm.ValorParametro(14))<>0 then
    dm.CertidoesRECIBO.AsInteger:=StrToInt(dm.ValorParametro(14))
      else dm.CertidoesRECIBO.AsInteger:=StrToInt(dm.ValorParametro(11));

  if Rb1.Checked then dm.CertidoesTIPO_CERTIDAO.AsString:='N';
  if Rb2.Checked then dm.CertidoesTIPO_CERTIDAO.AsString:='X';
  if Rb3.Checked then dm.CertidoesTIPO_CERTIDAO.AsString:='I';
  if Rb4.Checked then dm.CertidoesTIPO_CERTIDAO.AsString:='F';

  {SE FOR CANCELAMENTO � FEITA A CONSULTA DO PROTOCOLO (STATUS=PROTESTADO), ENT�O AMARRO A QUAL ATO EST� RELACIONADA A CERTID�O}
  if Rb2.Checked then
  begin
      if Protocolo.IsEmpty then
      begin
          GR.Aviso('NENHUM T�TULO INFORMADO!');
          Exit;
      end;
      dm.CertidoesID_ATO.AsInteger:=ProtocoloID_ATO.AsInteger;

      Ano1:=YearOf(ProtocoloDT_REGISTRO.AsDateTime);
      Ano2:=YearOf(Now);
      Periodo:=Ano2-Ano1;
      if Periodo<5 then Periodo:=5;
      dm.CertidoesANOS.AsInteger:=Periodo;
  end;

  {SE FOR INTEIRO TEOR � FEITA A CONSULTA DO PROTOCOLO, ENT�O AMARRO A QUAL ATO EST� RELACIONADA A CERTID�O}
  {SE ENCONTRAR ALGUM T�TULO VERIFICO O STATUS}
  {SE STATUS<>PROTESTADO ENT�O CERTID�O=NEGATIVA}
  if Rb3.Checked then
  begin
      if Protocolo.IsEmpty then
      begin
          GR.Aviso('NENHUM T�TULO INFORMADO!');
          Exit;
      end;

      dm.CertidoesID_ATO.AsInteger:=ProtocoloID_ATO.AsInteger;

      Ano1:=YearOf(ProtocoloDT_REGISTRO.AsDateTime);
      Ano2:=YearOf(Now);
      Periodo:=Ano2-Ano1;
      if Periodo<5 then Periodo:=5;
      dm.CertidoesANOS.AsInteger:=Periodo;
  end;

  {CERTID�O ESPEC�FICA}
  if Rb4.Checked then
  begin
      if Protocolo.IsEmpty then
      begin
          GR.Aviso('NENHUM T�TULO INFORMADO!');
          Exit;
      end;
      dm.CertidoesID_ATO.AsInteger    :=ProtocoloID_ATO.AsInteger;
      dm.CertidoesANOS.AsInteger      :=0;
      dm.CertidoesRESULTADO.AsString  :='P';
      dm.CertidoesFOLHAS.AsInteger    :=1;
      dm.CertidoesREGISTROS.AsInteger :=1;
  end;

  GR.CriarForm(TFPedidoCertidao,FPedidoCertidao);
  if dm.vOkGeral then Close;
end;

procedure TFBuscaCertidao.dsConsultaDataChange(Sender: TObject; Field: TField);
begin
  if (Rb2.Checked) or (Rb3.Checked) or (Rb4.Checked) then
  begin
      Protocolo.Close;
      Protocolo.Params[0].AsString:=ConsultaCPF_CNPJ_DEVEDOR.AsString;
      Protocolo.Open;

      Protocolo.Filtered:=False;
      if Rb2.Checked then
      begin
          Protocolo.Filtered:=False;
          Protocolo.Filter:='STATUS='+QuotedStr('PROTESTADO');
          Protocolo.Filtered:=True;
      end;

      if Rb3.Checked then
      begin
          Protocolo.Filtered:=False;
          Protocolo.Filter:='STATUS='+QuotedStr('PAGO')+' OR STATUS='+QuotedStr('CANCELADO')+' OR STATUS='+QuotedStr('PROTESTADO');
          Protocolo.Filtered:=True;
      end;

      if Rb4.Checked then
      begin
          Protocolo.Filtered:=False;
          Protocolo.Filter:='STATUS='+QuotedStr('PAGO')+' OR STATUS='+QuotedStr('CANCELADO');
          Protocolo.Filtered:=True;
      end;

      if cbFiltro.Text='Protocolo' then
      begin
          Protocolo.Filtered:=False;
          Protocolo.Filter:='PROTOCOLO='+QuotedStr(edLocalizar.Text);
          Protocolo.Filtered:=True;
      end;
  end;
end;

procedure TFBuscaCertidao.Rb1Click(Sender: TObject);
begin
  P3.Visible:=(Rb2.Checked) or (Rb3.Checked) or (Rb4.Checked);
  edLocalizar.SetFocus;

  cbFiltro.Items.Clear;
  cbFiltro.Items.Add('CPF/CNPJ');
  cbFiltro.Items.Add('Nome');
  cbFiltro.Items.Add('N� do T�tulo');
  cbFiltro.ItemIndex:=1;
end;

procedure TFBuscaCertidao.Rb2Click(Sender: TObject);
begin
  P3.Visible:=(Rb2.Checked) or (Rb3.Checked) or (Rb4.Checked);
  edLocalizar.SetFocus;

  cbFiltro.Items.Clear;
  cbFiltro.Items.Add('CPF/CNPJ');
  cbFiltro.Items.Add('Nome');
  cbFiltro.Items.Add('Protocolo');
  cbFiltro.ItemIndex:=1;
end;

procedure TFBuscaCertidao.Rb3Click(Sender: TObject);
begin
  P3.Visible:=(Rb2.Checked) or (Rb3.Checked) or (Rb4.Checked);
  edLocalizar.SetFocus;

  cbFiltro.Items.Clear;
  cbFiltro.Items.Add('CPF/CNPJ');
  cbFiltro.Items.Add('Nome');
  cbFiltro.Items.Add('Protocolo');
  cbFiltro.ItemIndex:=1;
end;

procedure TFBuscaCertidao.Grid2DblClick(Sender: TObject);
begin
  if Protocolo.IsEmpty then Exit;

  dm.vTipo:='A';

  Application.CreateForm(TFTitulo,FTitulo);
  FTitulo.CarregarTitulo(ProtocoloID_ATO.AsInteger);
  FTitulo.Visualizacao;
  FTitulo.ShowModal;
  FTitulo.Free;

  Protocolo.RefreshRecord;
end;

procedure TFBuscaCertidao.btConsultarClick(Sender: TObject);
var
  vQuery,vWhere: String;
begin
  if edLocalizar.Text='' then Exit;

  PF.Aguarde(True);
  vQuery:='SELECT DISTINCT DEVEDOR,CPF_CNPJ_DEVEDOR FROM TITULOS ';

  case cbFiltro.ItemIndex of
    0: vWhere:='WHERE CPF_CNPJ_DEVEDOR=:PARAMETRO';
    1: vWhere:='WHERE DEVEDOR LIKE:PARAMETRO';
    2: vWhere:=GR.iif(Rb1.Checked,'WHERE NUMERO_TITULO=:PARAMETRO','WHERE PROTOCOLO=:PARAMETRO');
  end;

  Consulta.Close;
  Consulta.CommandText:=vQuery+vWhere+' ORDER BY DEVEDOR';
  Consulta.Params.ParamByName('PARAMETRO').AsString:=GR.iif(cbFiltro.Text='Nome',edLocalizar.Text+'%',edLocalizar.Text);
  Consulta.Open;
  PF.Aguarde(False);
end;

procedure TFBuscaCertidao.FormCreate(Sender: TObject);
begin
  PA.Visible:=FPrincipal.ckAssistente.Checked;
  cbFiltro.ItemIndex:=1;
  cbFiltroClick(Sender);
end;

procedure TFBuscaCertidao.cbFiltroKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then edLocalizar.SetFocus;
end;

procedure TFBuscaCertidao.edLocalizarKeyPress(Sender: TObject; var Key: Char);
begin
  if cbFiltro.Text='Protocolo' then
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
  
  if key=#13 then btConsultar.Click;
end;

procedure TFBuscaCertidao.cbFiltroClick(Sender: TObject);
begin
  case cbFiltro.ItemIndex of
    0: edLocalizar.BoundLabel.Caption:='Informe o CPF/CNPJ do Devedor';
    1: edLocalizar.BoundLabel.Caption:='Informe o Nome do Devedor';
    2: edLocalizar.BoundLabel.Caption:=GR.iif(Rb1.Checked,'Informe o N� do T�tulo','Informe o Protocolo do T�tulo');
  end;
  edLocalizar.Clear;
end;

procedure TFBuscaCertidao.FormShow(Sender: TObject);
begin
  edLocalizar.SetFocus;
end;

procedure TFBuscaCertidao.Rb4Click(Sender: TObject);
begin
  P3.Visible:=(Rb2.Checked) or (Rb3.Checked) or (Rb4.Checked);
  edLocalizar.SetFocus;

  cbFiltro.Items.Clear;
  cbFiltro.Items.Add('CPF/CNPJ');
  cbFiltro.Items.Add('Nome');
  cbFiltro.Items.Add('Protocolo');
  cbFiltro.ItemIndex:=1;
end;

end.

