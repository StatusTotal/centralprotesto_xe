unit UStatus;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sGroupBox, ExtCtrls, sPanel, Buttons, sBitBtn;

type
  TFStatus = class(TForm)
    P1: TsPanel;
    rgStatus: TsRadioGroup;
    btSalvar: TsBitBtn;
    btCancelar: TsBitBtn;
    procedure btSalvarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FStatus: TFStatus;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFStatus.btSalvarClick(Sender: TObject);
begin
  dm.vMudou:=True;
  case rgStatus.ItemIndex of
    0: dm.vStatus:='APONTADO';
    1: dm.vStatus:='INTIMADO PESSOAL';
    2: dm.vStatus:='INTIMADO EDITAL';
    3: dm.vStatus:='INTIMADO CARTA';
    4: dm.vStatus:='PROTESTADO';
    5: dm.vStatus:='PAGO';
    6: dm.vStatus:='CANCELADO';
    7: dm.vStatus:='RETIRADO';
    8: dm.vStatus:='SUSTADO';
    9: dm.vStatus:='DEVOLVIDO';
  end;
  Close;
end;

procedure TFStatus.btCancelarClick(Sender: TObject);
begin
  dm.vMudou:=False;
  dm.vStatus:='';
  Close;
end;

end.
