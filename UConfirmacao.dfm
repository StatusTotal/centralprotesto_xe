object FConfirmacao: TFConfirmacao
  Left = 277
  Top = 202
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Confirma'#231#227'o'
  ClientHeight = 480
  ClientWidth = 773
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  Icon.Data = {
    0000010001002020020001000100300100001600000028000000200000004000
    0000010001000000000088000000000000000000000000000000000000000000
    0000FFFFFF000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
    FFFFFFFFFFFF}
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 15
  object P2: TsPanel
    Left = 0
    Top = 351
    Width = 773
    Height = 129
    Align = alBottom
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    SkinData.SkinSection = 'PANEL'
    object edDevedor: TsDBEdit
      Left = 16
      Top = 60
      Width = 305
      Height = 22
      DataField = 'DEVEDOR'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Devedor'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edDataTitulo: TsDBDateEdit
      Left = 16
      Top = 20
      Width = 98
      Height = 22
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 0
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data do T'#237'tulo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Blend = 0
      DataField = 'DT_TITULO'
      DataSource = dsRX
    end
    object edValor: TsDBEdit
      Left = 132
      Top = 20
      Width = 73
      Height = 22
      DataField = 'VALOR_TITULO'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Valor'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edEspecie: TsDBEdit
      Left = 344
      Top = 20
      Width = 409
      Height = 22
      DataField = 'Especie'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Esp'#233'cie'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edDataVencimento: TsDBDateEdit
      Left = 223
      Top = 20
      Width = 98
      Height = 22
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data do Vencimento'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Blend = 0
      DataField = 'DT_VENCIMENTO'
      DataSource = dsRX
    end
    object edDocumento: TsDBEdit
      Left = 344
      Top = 60
      Width = 153
      Height = 22
      DataField = 'CPF_CNPJ_DEVEDOR'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Documento'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edEndereco: TsDBEdit
      Left = 16
      Top = 99
      Width = 305
      Height = 22
      DataField = 'END_DEVEDOR'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Endere'#231'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edBairro: TsDBEdit
      Left = 344
      Top = 99
      Width = 153
      Height = 22
      DataField = 'BAIRRO_DEVEDOR'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Bairro'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edUF: TsDBEdit
      Left = 509
      Top = 99
      Width = 41
      Height = 22
      DataField = 'UF'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'UF'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edCidade: TsDBEdit
      Left = 560
      Top = 99
      Width = 193
      Height = 22
      DataField = 'CID_DEVEDOR'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Cidade'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edPraca: TsDBEdit
      Left = 509
      Top = 60
      Width = 244
      Height = 22
      DataField = 'PRACA'
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Pra'#231'a'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object PB: TsProgressBar
      Left = 1
      Top = 1
      Width = 771
      Height = 127
      Align = alClient
      TabOrder = 11
      Visible = False
      SkinData.SkinSection = 'MAINMENU'
    end
  end
  object GridAtos: TwwDBGrid
    Left = 0
    Top = 63
    Width = 773
    Height = 288
    ControlType.Strings = (
      'Check;CheckBox;True;False')
    Selected.Strings = (
      'Check'#9'3'#9'*'#9'F'
      'NUMERO_TITULO'#9'13'#9'N'#186' T'#237'tulo'#9'T'
      'Protocolo'#9'12'#9'Protocolo'#9'F'
      'DescIrreg'#9'76'#9'Irregularidade'#9'F'
      'STATUS'#9'17'#9'Status'#9'F')
    IniAttributes.Delimiter = ';;'
    IniAttributes.UnicodeIniFile = False
    TitleColor = clBtnFace
    FixedCols = 1
    ShowHorzScrollBar = True
    EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
    Align = alClient
    BorderStyle = bsNone
    DataSource = dsRX
    EditCalculated = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = []
    Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
    ParentFont = False
    PopupMenu = PM
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    OnExit = GridAtosExit
  end
  object M: TMemo
    Left = 72
    Top = 527
    Width = 769
    Height = 130
    BorderStyle = bsNone
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 773
    Height = 63
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    SkinData.SkinSection = 'SCROLLSLIDERH'
    object lbAviso: TLabel
      Left = 378
      Top = 7
      Width = 382
      Height = 50
      Cursor = crHandPoint
      AutoSize = False
      Caption = 'Seu arquivo de confirma'#231#227'o ser'#225' gerado na seguinte pasta:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
      WordWrap = True
      OnClick = lbAvisoClick
    end
    object btArquivo: TsBitBtn
      Left = 6
      Top = 6
      Width = 60
      Height = 52
      Cursor = crHandPoint
      Caption = 'Arquivo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Layout = blGlyphTop
      ParentFont = False
      TabOrder = 0
      OnClick = btArquivoClick
      Reflected = True
      ImageIndex = 4
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object sDBNavigator1: TsDBNavigator
      Left = 940
      Top = 5
      Width = 94
      Height = 52
      FullRepaint = False
      TabOrder = 3
      SkinData.SkinSection = 'ALPHACOMBOBOX'
      DataSource = dsRX
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
    end
    object btApontar: TsBitBtn
      Left = 165
      Top = 6
      Width = 79
      Height = 52
      Cursor = crHandPoint
      Hint = 'Gerar arquivo de confirma'#231#227'o'
      Caption = 'Confirma'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Layout = blGlyphTop
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btApontarClick
      Reflected = True
      ImageIndex = 10
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object RgTipo: TsRadioGroup
      Left = 1130
      Top = 2
      Width = 140
      Height = 56
      Caption = '  Tipo de Apresenta'#231#227'o  '
      Enabled = False
      ParentBackground = False
      TabOrder = 4
      Visible = False
      CaptionLayout = clTopCenter
      SkinData.SkinSection = 'HINT'
      CaptionSkin = 'HINT'
      ItemIndex = 1
      Items.Strings = (
        'Correio'
        'Internet')
    end
    object btExportar: TButton
      Left = 1038
      Top = 8
      Width = 89
      Height = 25
      Caption = 'Exportar'
      TabOrder = 5
      Visible = False
      OnClick = btExportarClick
    end
    object btRejeitar: TsBitBtn
      Left = 71
      Top = 6
      Width = 89
      Height = 52
      Cursor = crHandPoint
      Caption = 'Irregularidade'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Layout = blGlyphTop
      ParentFont = False
      TabOrder = 1
      OnClick = btRejeitarClick
      Reflected = True
      ImageIndex = 9
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object btExportarCRA: TButton
      Left = 1038
      Top = 32
      Width = 89
      Height = 25
      Caption = 'ExportarCRA'
      TabOrder = 6
      Visible = False
    end
  end
  object M2: TMemo
    Left = 64
    Top = 558
    Width = 769
    Height = 124
    BorderStyle = bsNone
    Color = 8454016
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 4
    Visible = False
  end
  object wwDBGrid1: TwwDBGrid
    Left = 560
    Top = 557
    Width = 769
    Height = 172
    Selected.Strings = (
      'Linha'#9'10000'#9'Linha')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow]
    IniAttributes.Delimiter = ';;'
    IniAttributes.UnicodeIniFile = False
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsEntrada
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    KeyOptions = []
    ParentFont = False
    TabOrder = 5
    TitleAlignment = taLeftJustify
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Arial'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = False
  end
  object RX: TRxMemoryData
    FieldDefs = <
      item
        Name = 'CEDENTE'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'SACADOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DEVEDOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'TIPO_DEVEDOR'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CPF_CNPJ_DEVEDOR'
        DataType = ftString
        Size = 14
      end
      item
        Name = 'RG_DEVEDOR'
        DataType = ftString
        Size = 25
      end
      item
        Name = 'END_DEVEDOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CEP_DEVEDOR'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'UF'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'CID_DEVEDOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'BAIRRO_DEVEDOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DT_TITULO'
        DataType = ftDate
      end
      item
        Name = 'DT_VENCIMENTO'
        DataType = ftDate
      end
      item
        Name = 'VALOR_TITULO'
        DataType = ftFloat
      end
      item
        Name = 'APRESENTANTE'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CODIGO_APRESENTANTE'
        DataType = ftInteger
      end
      item
        Name = 'TIPO_TITULO'
        DataType = ftInteger
      end
      item
        Name = 'NUMERO_TITULO'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SALDO_PROTESTO'
        DataType = ftFloat
      end
      item
        Name = 'PRACA'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ACEITE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ENDOSSO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'STATUS'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'OCORRENCIA'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NOSSO_NUMERO'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'AGENCIA'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'Check'
        DataType = ftBoolean
      end
      item
        Name = 'Protocolo'
        DataType = ftInteger
      end
      item
        Name = 'CUSTAS'
        DataType = ftFloat
      end
      item
        Name = 'Irregularidade'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'CPF_CNPJ_SACADOR'
        DataType = ftString
        Size = 14
      end
      item
        Name = 'END_SACADOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CEP_SACADOR'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'UF_SACADOR'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'BAIRRO_SACADOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'CID_SACADOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DataOcorrencia'
        DataType = ftDate
      end
      item
        Name = 'DescOcorrencia'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DescIrreg'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DT_PROTOCOLO'
        DataType = ftDate
      end>
    Left = 80
    Top = 216
    object RXCEDENTE: TStringField
      DisplayLabel = 'Cedente'
      DisplayWidth = 25
      FieldName = 'CEDENTE'
      Size = 100
    end
    object RXSACADOR: TStringField
      DisplayLabel = 'Sacador'
      DisplayWidth = 22
      FieldName = 'SACADOR'
      Size = 100
    end
    object RXDEVEDOR: TStringField
      DisplayLabel = 'Devedor'
      DisplayWidth = 28
      FieldName = 'DEVEDOR'
      Visible = False
      Size = 100
    end
    object RXTIPO_DEVEDOR: TStringField
      DisplayLabel = 'Tipo'
      DisplayWidth = 4
      FieldName = 'TIPO_DEVEDOR'
      Visible = False
      Size = 1
    end
    object RXCPF_CNPJ_DEVEDOR: TStringField
      DisplayLabel = 'Documento'
      DisplayWidth = 15
      FieldName = 'CPF_CNPJ_DEVEDOR'
      Visible = False
      Size = 14
    end
    object RXRG_DEVEDOR: TStringField
      DisplayLabel = 'RG'
      DisplayWidth = 12
      FieldName = 'RG_DEVEDOR'
      Visible = False
      Size = 25
    end
    object RXEND_DEVEDOR: TStringField
      DisplayLabel = 'Endere'#231'o'
      DisplayWidth = 28
      FieldName = 'END_DEVEDOR'
      Visible = False
      Size = 100
    end
    object RXCEP_DEVEDOR: TStringField
      DisplayLabel = 'CEP'
      DisplayWidth = 10
      FieldName = 'CEP_DEVEDOR'
      Visible = False
      EditMask = '99.999-999;0;_'
      Size = 8
    end
    object RXUF: TStringField
      DisplayWidth = 3
      FieldName = 'UF'
      Visible = False
      Size = 2
    end
    object RXCID_DEVEDOR: TStringField
      DisplayLabel = 'Cidade'
      DisplayWidth = 19
      FieldName = 'CID_DEVEDOR'
      Visible = False
      Size = 100
    end
    object RXBAIRRO_DEVEDOR: TStringField
      DisplayLabel = 'Bairro'
      DisplayWidth = 21
      FieldName = 'BAIRRO_DEVEDOR'
      Visible = False
      Size = 100
    end
    object RXEspecie: TStringField
      DisplayLabel = 'Esp'#233'cie'
      DisplayWidth = 18
      FieldKind = fkCalculated
      FieldName = 'Especie'
      Visible = False
      Size = 100
      Calculated = True
    end
    object RXDT_TITULO: TDateField
      DisplayLabel = 'Data'
      DisplayWidth = 10
      FieldName = 'DT_TITULO'
      Visible = False
    end
    object RXDT_VENCIMENTO: TDateField
      DisplayLabel = 'Vencimento'
      DisplayWidth = 10
      FieldName = 'DT_VENCIMENTO'
      Visible = False
    end
    object RXVALOR_TITULO: TFloatField
      DisplayLabel = 'Valor'
      DisplayWidth = 10
      FieldName = 'VALOR_TITULO'
      Visible = False
      DisplayFormat = '#####0.00'
    end
    object RXAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Visible = False
      Size = 100
    end
    object RXTIPO_TITULO: TIntegerField
      FieldName = 'TIPO_TITULO'
      Visible = False
    end
    object RXNUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
      Visible = False
    end
    object RXSALDO_PROTESTO: TFloatField
      FieldName = 'SALDO_PROTESTO'
      Visible = False
    end
    object RXPRACA: TStringField
      FieldName = 'PRACA'
      Visible = False
    end
    object RXDocumento: TStringField
      FieldKind = fkCalculated
      FieldName = 'Documento'
      Size = 18
      Calculated = True
    end
    object RXACEITE: TStringField
      FieldName = 'ACEITE'
      Size = 1
    end
    object RXENDOSSO: TStringField
      FieldName = 'ENDOSSO'
      Size = 1
    end
    object RXSTATUS: TStringField
      Alignment = taCenter
      FieldName = 'STATUS'
    end
    object RXOCORRENCIA: TStringField
      FieldName = 'OCORRENCIA'
      Size = 1
    end
    object RXNOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Size = 15
    end
    object RXAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 15
    end
    object RXCheck: TBooleanField
      FieldName = 'Check'
      OnChange = RXCheckChange
    end
    object RXProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object RXCUSTAS: TFloatField
      FieldName = 'CUSTAS'
    end
    object RXIrregularidade: TStringField
      FieldName = 'Irregularidade'
      Size = 2
    end
    object RXCPF_CNPJ_SACADOR: TStringField
      FieldName = 'CPF_CNPJ_SACADOR'
      Size = 14
    end
    object RXEND_SACADOR: TStringField
      FieldName = 'END_SACADOR'
      Size = 100
    end
    object RXCEP_SACADOR: TStringField
      FieldName = 'CEP_SACADOR'
      Size = 8
    end
    object RXUF_SACADOR: TStringField
      FieldName = 'UF_SACADOR'
      Size = 2
    end
    object RXBAIRRO_SACADOR: TStringField
      FieldName = 'BAIRRO_SACADOR'
      Size = 100
    end
    object RXCID_SACADOR: TStringField
      FieldName = 'CID_SACADOR'
      Size = 100
    end
    object RXDataOcorrencia: TDateField
      FieldName = 'DataOcorrencia'
    end
    object RXDescOcorrencia: TStringField
      FieldName = 'DescOcorrencia'
      Size = 100
    end
    object RXDescIrreg: TStringField
      FieldName = 'DescIrreg'
      Size = 100
    end
    object RXDT_PROTOCOLO: TDateField
      FieldName = 'DT_PROTOCOLO'
    end
    object RXCODIGO_APRESENTANTE: TStringField
      FieldName = 'CODIGO_APRESENTANTE'
      Size = 10
    end
  end
  object dsRX: TDataSource
    DataSet = RX
    OnDataChange = dsRXDataChange
    Left = 112
    Top = 216
  end
  object Opd: TsOpenDialog
    Left = 144
    Top = 216
  end
  object Entrada: TRxMemoryData
    FieldDefs = <
      item
        Name = 'Linha'
        DataType = ftMemo
      end>
    Left = 80
    Top = 160
    object EntradaLinha: TMemoField
      DisplayWidth = 10000
      FieldName = 'Linha'
      BlobType = ftMemo
    end
  end
  object dsEntrada: TDataSource
    DataSet = Entrada
    Left = 264
    Top = 216
  end
  object Saida: TRxMemoryData
    FieldDefs = <
      item
        Name = 'Linha'
        DataType = ftMemo
      end>
    Left = 144
    Top = 160
    object SaidaLinha: TMemoField
      DisplayWidth = 10
      FieldName = 'Linha'
      BlobType = ftMemo
    end
  end
  object dsSaida: TDataSource
    DataSet = Saida
    Left = 312
    Top = 216
  end
  object dsControle: TDataSource
    DataSet = dm.Controle
    Left = 360
    Top = 216
  end
  object RXBC: TRxMemoryData
    FieldDefs = <>
    Left = 464
    Top = 216
    object RXBCImportar: TMemoField
      FieldName = 'Importar'
      BlobType = ftMemo
    end
    object RXBCExportar: TMemoField
      FieldName = 'Exportar'
      BlobType = ftMemo
    end
    object RXBCCodigo: TStringField
      FieldName = 'Codigo'
      Size = 10
    end
  end
  object dsRXBC: TDataSource
    DataSet = RXBC
    Left = 512
    Top = 216
  end
  object RXEN: TRxMemoryData
    FieldDefs = <
      item
        Name = 'Linha'
        DataType = ftMemo
      end>
    OnFilterRecord = RXENFilterRecord
    Left = 560
    Top = 216
    object RXENLinha: TMemoField
      DisplayWidth = 10000
      FieldName = 'Linha'
      BlobType = ftMemo
    end
    object RXENCodigo: TStringField
      FieldName = 'Codigo'
      Size = 10
    end
  end
  object dsRXEN: TDataSource
    DataSet = RXEN
    Left = 606
    Top = 216
  end
  object Linhas: TRxMemoryData
    FieldDefs = <>
    Left = 464
    Top = 168
    object LinhasLinha: TMemoField
      FieldName = 'Linha'
      BlobType = ftMemo
    end
  end
  object Portadores: TRxMemoryData
    FieldDefs = <>
    Left = 512
    Top = 168
    object PortadoresCodigo: TStringField
      FieldName = 'Codigo'
      Size = 10
    end
    object PortadoresNome: TStringField
      FieldName = 'Nome'
      Size = 100
    end
    object PortadoresAgencia: TStringField
      FieldName = 'Agencia'
      Size = 6
    end
    object PortadoresPraca: TStringField
      FieldName = 'Praca'
      Size = 7
    end
  end
  object PM: TPopupMenu
    Left = 80
    Top = 112
    object MarcarTodos: TMenuItem
      Caption = 'Marcar Todos'
      OnClick = MarcarTodosClick
    end
    object DesmarcarTodos: TMenuItem
      Caption = 'Desmarcar Todos'
      OnClick = DesmarcarTodosClick
    end
  end
  object qryNumero: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select PROTOCOLO,DT_PROTOCOLO,DT_PAGAMENTO,'
      'DT_REGISTRO,DT_RETIRADO,DT_SUSTADO,STATUS,'
      'SALDO_PROTESTO,VALOR_TITULO,DT_DEVOLVIDO,'
      
        'CONVENIO,TOTAL,TARIFA_BANCARIA,VALOR_AR,DISTRIBUICAO from titulo' +
        's where '
      ''
      'NUMERO_TITULO=:NUMERO and '
      'CODIGO_APRESENTANTE=:CODIGO and'
      'DT_TITULO=:DT_TITULO and'
      'NOSSO_NUMERO=:NOSSO')
    Left = 272
    Top = 136
    ParamData = <
      item
        Name = 'NUMERO'
        DataType = ftString
        ParamType = ptInput
        Size = 20
      end
      item
        Name = 'CODIGO'
        DataType = ftString
        ParamType = ptInput
        Size = 10
      end
      item
        Name = 'DT_TITULO'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'NOSSO'
        DataType = ftString
        ParamType = ptInput
        Size = 15
      end>
    object qryNumeroPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object qryNumeroDT_PROTOCOLO: TDateField
      FieldName = 'DT_PROTOCOLO'
      Origin = 'DT_PROTOCOLO'
    end
    object qryNumeroDT_PAGAMENTO: TDateField
      FieldName = 'DT_PAGAMENTO'
      Origin = 'DT_PAGAMENTO'
    end
    object qryNumeroDT_REGISTRO: TDateField
      FieldName = 'DT_REGISTRO'
      Origin = 'DT_REGISTRO'
    end
    object qryNumeroDT_RETIRADO: TDateField
      FieldName = 'DT_RETIRADO'
      Origin = 'DT_RETIRADO'
    end
    object qryNumeroDT_SUSTADO: TDateField
      FieldName = 'DT_SUSTADO'
      Origin = 'DT_SUSTADO'
    end
    object qryNumeroSTATUS: TStringField
      FieldName = 'STATUS'
      Origin = 'STATUS'
    end
    object qryNumeroSALDO_PROTESTO: TFloatField
      FieldName = 'SALDO_PROTESTO'
      Origin = 'SALDO_PROTESTO'
    end
    object qryNumeroVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
      Origin = 'VALOR_TITULO'
    end
    object qryNumeroDT_DEVOLVIDO: TDateField
      FieldName = 'DT_DEVOLVIDO'
      Origin = 'DT_DEVOLVIDO'
    end
    object qryNumeroCONVENIO: TStringField
      FieldName = 'CONVENIO'
      Origin = 'CONVENIO'
      FixedChar = True
      Size = 1
    end
    object qryNumeroTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
    end
    object qryNumeroTARIFA_BANCARIA: TFloatField
      FieldName = 'TARIFA_BANCARIA'
      Origin = 'TARIFA_BANCARIA'
    end
    object qryNumeroVALOR_AR: TFloatField
      FieldName = 'VALOR_AR'
      Origin = 'VALOR_AR'
    end
    object qryNumeroDISTRIBUICAO: TFloatField
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
    end
  end
  object qryFaixa: TFDQuery
    Connection = dm.conCENTRAL
    SQL.Strings = (
      'select COD,EMOL,FETJ,FUND,FUNP,MUTUA,ACOTERJ,'
      'DISTRIB,TOT, TITULO from COD'
      ''
      'where '
      ''
      '(ATRIB=4) AND'
      ''
      '(CONVENIO=:C) AND'
      ''
      '(MINIMO <= :VALOR1 AND MAXIMO >= :VALOR2) '
      ''
      'group by COD,EMOL,FETJ,FUND,FUNP,MUTUA,ACOTERJ,'
      'DISTRIB,TOT, TITULO')
    Left = 344
    Top = 136
    ParamData = <
      item
        Name = 'C'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'VALOR1'
        DataType = ftFloat
        ParamType = ptInput
      end
      item
        Name = 'VALOR2'
        DataType = ftFloat
        ParamType = ptInput
      end>
    object qryFaixaCOD: TIntegerField
      FieldName = 'COD'
      Origin = 'COD'
    end
    object qryFaixaEMOL: TFloatField
      FieldName = 'EMOL'
      Origin = 'EMOL'
    end
    object qryFaixaFETJ: TFloatField
      FieldName = 'FETJ'
      Origin = 'FETJ'
    end
    object qryFaixaFUND: TFloatField
      FieldName = 'FUND'
      Origin = 'FUND'
    end
    object qryFaixaFUNP: TFloatField
      FieldName = 'FUNP'
      Origin = 'FUNP'
    end
    object qryFaixaMUTUA: TFloatField
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
    end
    object qryFaixaACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
    end
    object qryFaixaDISTRIB: TFloatField
      FieldName = 'DISTRIB'
      Origin = 'DISTRIB'
    end
    object qryFaixaTOT: TFloatField
      FieldName = 'TOT'
      Origin = 'TOT'
    end
    object qryFaixaTITULO: TStringField
      FieldName = 'TITULO'
      Origin = 'TITULO'
      Size = 50
    end
  end
end
