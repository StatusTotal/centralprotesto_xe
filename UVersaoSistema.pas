{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UVersaoSistema.pas
  Descricao:   Unit de processamento das informacoes de Atualizacao do Sistema
  Author   :   Cristina
  Date:        17-out-2017
  Last Update: 12-out-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UVersaoSistema;

interface

uses System.SysUtils, System.Classes, System.Variants, System.DateUtils,
  System.VarUtils, FireDAC.Comp.Client, Vcl.Forms, System.Generics.Collections,
  FireDAC.Stan.Param, System.StrUtils, Winapi.Windows;

type
  TVersaoSistema = class(TForm)

  private
    { Private declarations }
    procedure AtualizarTabelaCGJ(NovaVersaoT: String; NovaMutua, NovaAcoterj, NovaDistribuicao: Currency; AtuTabela: Boolean);
    procedure AtualizaTabelaCONCUS(AnoTabela: String);
    procedure AtualizaTabelaCOD;
    procedure AtualizaTabelaTABCUS;
    procedure GravarNovaVersaoSistema(NovaVersao: String; var Menssagem: String);

    function RetornarVersaoSistema: String;
    function RetornarVersaoTabela: String;
  public
    { Public declarations }
    procedure VerificarVersaoSistema(var Mensagem: String);
end;

var
  VS: TVersaoSistema;

implementation

{ TVersaoSistema }

uses UDM, UGDM, UGeral;

procedure TVersaoSistema.AtualizarTabelaCGJ(NovaVersaoT: String; NovaMutua,
  NovaAcoterj, NovaDistribuicao: Currency; AtuTabela: Boolean);
var
  QryAtuV: TFDQuery;
begin
  try
    GR.Processando(True,
                   'Atualiza��o da Tabela ' + NovaVersaoT,
                   True,
                   'Por favor, aguarde...');

    if Gdm.FDGerencial.Connected then
      Gdm.FDGerencial.StartTransaction;

    if dm.conCENTRAL.Connected then
      dm.conCENTRAL.StartTransaction;

    GR.CriarFDQuery(QryAtuV,'',dm.conSistema);

    if AtuTabela then
    begin
      AtualizaTabelaCONCUS(NovaVersaoT);

      AtualizaTabelaCOD;

      AtualizaTabelaTABCUS;

      //ANO_TABELA
      QryAtuV.Close;
      QryAtuV.SQL.Clear;
      QryAtuV.SQL.Text := 'UPDATE PARAMETROS ' +
                          '   SET VALOR = :VERSAO_TABELA ' +
                          ' WHERE ID_PARAMETRO = :ID_PARAMETRO' ;
      QryAtuV.Params.ParamByName('VERSAO_TABELA').Value := NovaVersaoT;
      QryAtuV.Params.ParamByName('ID_PARAMETRO').Value  := 77;
      QryAtuV.ExecSQL;
    end;

    //MUTUA
    QryAtuV.Close;
    QryAtuV.SQL.Clear;
    QryAtuV.SQL.Text := 'UPDATE PARAMETROS ' +
                        '   SET VALOR = :VERSAO_TABELA ' +
                        ' WHERE ID_PARAMETRO = :ID_PARAMETRO' ;
    QryAtuV.Params.ParamByName('VERSAO_TABELA').Value := NovaMutua;
    QryAtuV.Params.ParamByName('ID_PARAMETRO').Value  := 26;
    QryAtuV.ExecSQL;

    //ACOTERJ
    QryAtuV.Close;
    QryAtuV.SQL.Clear;
    QryAtuV.SQL.Text := 'UPDATE PARAMETROS ' +
                        '   SET VALOR = :VERSAO_TABELA ' +
                        ' WHERE ID_PARAMETRO = :ID_PARAMETRO' ;
    QryAtuV.Params.ParamByName('VERSAO_TABELA').Value := NovaAcoterj;
    QryAtuV.Params.ParamByName('ID_PARAMETRO').Value  := 27;
    QryAtuV.ExecSQL;

    //DISTRIBUICAO
    QryAtuV.Close;
    QryAtuV.SQL.Clear;
    QryAtuV.SQL.Text := 'UPDATE PARAMETROS ' +
                        '   SET VALOR = :VERSAO_TABELA ' +
                        ' WHERE ID_PARAMETRO = :ID_PARAMETRO' ;
    QryAtuV.Params.ParamByName('VERSAO_TABELA').Value := NovaDistribuicao;
    QryAtuV.Params.ParamByName('ID_PARAMETRO').Value  := 28;
    QryAtuV.ExecSQL;

    if Gdm.FDGERENCIAL.InTransaction then
      Gdm.FDGERENCIAL.Commit;

    if dm.conCENTRAL.InTransaction then
      dm.conCENTRAL.Commit;

    FreeAndNil(QryAtuV);

    GR.Processando(False);

    Application.MessageBox(PChar('Tabela ' + Trim(NovaVersaoT) + ' atualizada com sucesso!'),
                           PChar('TABELA ' + Trim(NovaVersaoT)),
                           MB_OK);
  except
    on E:Exception do
    begin
      if Gdm.FDGERENCIAL.InTransaction then
        Gdm.FDGERENCIAL.Rollback;

      if dm.conCENTRAL.InTransaction then
        dm.conCENTRAL.Rollback;

      FreeAndNil(QryAtuV);

      GR.Processando(False);

      Application.MessageBox(PChar('Erro na atualiza��o da Tabela ' + Trim(NovaVersaoT) + '. Por favor, tente novamente mais tarde.'),
                             PChar('TABELA ' + Trim(NovaVersaoT)),
                             MB_OK);
    end;
  end;
end;

procedure TVersaoSistema.AtualizaTabelaCOD;
var
  QryADICIONAL, QryCOD, QryAtu: TFDQuery;
begin
  GR.CriarFDQuery(QryADICIONAL,'',Gdm.FDGERENCIAL);
  GR.CriarFDQuery(QryCOD,'',dm.conCENTRAL);
  GR.CriarFDQuery(QryAtu,'',dm.conCENTRAL);

  QryADICIONAL.Close;
  QryADICIONAL.SQL.Clear;
  QryADICIONAL.SQL.Text := 'SELECT ATRIBUICAO, ' +
                           '       CODIGO, ' +
                           '       DESCRICAO, ' +
                           '       EMOLUMENTOS, ' +
                           '       FETJ, ' +
                           '       FUNDPERJ, ' +
                           '       FUNPERJ, ' +
                           '       FUNARPEN, ' +
                           '       PMCMV, ' +
                           '       ISS, ' +
                           '       MUTUA, ' +
                           '       ACOTERJ, ' +
                           '       DISTRIBUICAO, ' +
                           '       TOTAL, ' +
                           '       MINIMO, ' +
                           '       MAXIMO, ' +
                           '       CONVENIO, ' +
                           '       ATIVO ' +
                           '  FROM ADICIONAL ' +
                           ' WHERE ATRIBUICAO = 4';
  QryADICIONAL.Open;

  QryADICIONAL.First;

  while not QryADICIONAL.Eof do
  begin
    QryCOD.Close;
    QryCOD.SQL.Clear;

    QryCOD.SQL.Text := 'SELECT ATRIB, ' +
                       '       COD ' +
                       '  FROM COD ' +
                       ' WHERE ATRIB = :ATRIB ' +
                       '   AND COD = :COD';

    QryCOD.Params.ParamByName('ATRIB').Value    := QryADICIONAL.FieldByName('ATRIBUICAO').AsInteger;
    QryCOD.Params.ParamByName('COD').Value      := QryADICIONAL.FieldByName('CODIGO').AsInteger;
    QryCOD.Open;

    QryAtu.Close;
    QryAtu.SQL.Clear;

    if QryCOD.RecordCount = 0 then
    begin
      QryAtu.SQL.Text := 'INSERT INTO COD (ATRIB, COD, ATOS, EMOL, FETJ, FUND, ' +
                         '                 FUNP, FUNA, PMCMV, ISS, MUTUA, ACOTERJ, ' +
                         '                 DISTRIB, TOT, MINIMO, MAXIMO, DIA, ' +
                         '                 CONVENIO, OCULTO) ' +
                         '         VALUES (:ATRIB, :COD, :ATOS, :EMOL, :FETJ, :FUND, ' +
                         '                 :FUNP, :FUNA, :PMCMV, :ISS, :MUTUA, :ACOTERJ, ' +
                         '                 :DISTRIB, :TOT, :MINIMO, :MAXIMO, :DIA, ' +
                         '                 :CONVENIO, :OCULTO)';
    end
    else
    begin
      QryAtu.SQL.Text := 'UPDATE COD ' +
                         '   SET ATOS     = :ATOS, ' +
                         '       EMOL     = :EMOL, ' +
                         '       FETJ     = :FETJ, ' +
                         '       FUND     = :FUND, ' +
                         '       FUNP     = :FUNP, ' +
                         '       FUNA     = :FUNA, ' +
                         '       PMCMV    = :PMCMV, ' +
                         '       ISS      = :ISS, ' +
                         '       MUTUA    = :MUTUA, ' +
                         '       ACOTERJ  = :ACOTERJ, ' +
                         '       DISTRIB  = :DISTRIB, ' +
                         '       TOT      = :TOT, ' +
                         '       MINIMO   = :MINIMO, ' +
                         '       MAXIMO   = :MAXIMO, ' +
                         '       DIA      = :DIA, ' +
                         '       CONVENIO = :CONVENIO, ' +
                         '       OCULTO   = :OCULTO ' +
                         ' WHERE COD   = :COD ' +
                         '   AND ATRIB = :ATRIB';
    end;

    QryAtu.Params.ParamByName('ATRIB').Value    := QryADICIONAL.FieldByName('ATRIBUICAO').AsInteger;
    QryAtu.Params.ParamByName('COD').Value      := QryADICIONAL.FieldByName('CODIGO').AsInteger;
    QryAtu.Params.ParamByName('ATOS').Value     := QryADICIONAL.FieldByName('DESCRICAO').AsString;
    QryAtu.Params.ParamByName('EMOL').Value     := GR.NoRound(QryADICIONAL.FieldByName('EMOLUMENTOS').AsFloat, 2);
    QryAtu.Params.ParamByName('FETJ').Value     := GR.NoRound(QryADICIONAL.FieldByName('FETJ').AsFloat, 2);
    QryAtu.Params.ParamByName('FUND').Value     := GR.NoRound(QryADICIONAL.FieldByName('FUNDPERJ').AsFloat, 2);
    QryAtu.Params.ParamByName('FUNP').Value     := GR.NoRound(QryADICIONAL.FieldByName('FUNPERJ').AsFloat, 2);
    QryAtu.Params.ParamByName('FUNA').Value     := GR.NoRound(QryADICIONAL.FieldByName('FUNARPEN').AsFloat, 2);
    QryAtu.Params.ParamByName('PMCMV').Value    := GR.NoRound(QryADICIONAL.FieldByName('PMCMV').AsFloat, 2);
    QryAtu.Params.ParamByName('ISS').Value      := GR.NoRound(QryADICIONAL.FieldByName('ISS').AsFloat, 2);
    QryAtu.Params.ParamByName('MUTUA').Value    := GR.NoRound(QryADICIONAL.FieldByName('MUTUA').AsFloat, 2);
    QryAtu.Params.ParamByName('ACOTERJ').Value  := GR.NoRound(QryADICIONAL.FieldByName('ACOTERJ').AsFloat, 2);
    QryAtu.Params.ParamByName('DISTRIB').Value  := GR.NoRound(QryADICIONAL.FieldByName('DISTRIBUICAO').AsFloat, 2);
    QryAtu.Params.ParamByName('TOT').Value      := GR.NoRound(QryADICIONAL.FieldByName('TOTAL').AsFloat, 2);
    QryAtu.Params.ParamByName('MINIMO').Value   := GR.NoRound(QryADICIONAL.FieldByName('MINIMO').AsFloat, 2);
    QryAtu.Params.ParamByName('MAXIMO').Value   := GR.NoRound(QryADICIONAL.FieldByName('MAXIMO').AsFloat, 2);
    QryAtu.Params.ParamByName('DIA').Value      := 0;
    QryAtu.Params.ParamByName('CONVENIO').Value := QryADICIONAL.FieldByName('CONVENIO').AsString;
    QryAtu.Params.ParamByName('OCULTO').Value   := IfThen(Trim(QryADICIONAL.FieldByName('ATIVO').AsString) = 'S',
                                                          'N',
                                                          'S');

    QryAtu.ExecSQL;

    QryADICIONAL.Next;
  end;

  FreeAndNil(QryADICIONAL);
  FreeAndNil(QryCOD);
  FreeAndNil(QryAtu);
end;

procedure TVersaoSistema.AtualizaTabelaCONCUS(AnoTabela: String);
var
  QryCGJ, QryCONCUS, QryAtu: TFDQuery;
begin
  GR.CriarFDQuery(QryCGJ,'',Gdm.FDGERENCIAL);
  GR.CriarFDQuery(QryCONCUS,'',dm.conCENTRAL);
  GR.CriarFDQuery(QryAtu,'',dm.conCENTRAL);

  QryCGJ.Close;
  QryCGJ.SQL.Clear;
  QryCGJ.SQL.Text := 'SELECT ORDEM, ' +
                     '       TABELA, ' +
                     '       ITEM, ' +
                     '       SUBITEM, ' +
                     '       DESCRICAO, ' +
                     '       VALOR ' +
                     '  FROM CGJ ' +
                     ' WHERE ATRIBUICAO = 0 ' +
                     'UNION ALL ' +
                     'SELECT ORDEM, ' +
                     '       TABELA, ' +
                     '       ITEM, ' +
                     '       SUBITEM, ' +
                     '       DESCRICAO, ' +
                     '       VALOR ' +
                     '  FROM CGJ ' +
                     ' WHERE ATRIBUICAO = 4';
  QryCGJ.Open;

  QryCGJ.First;

  while not QryCGJ.Eof do
  begin
    //Verifica se existe, caso nao, inclui, caso sim, atualiza
    QryCONCUS.Close;
    QryCONCUS.SQL.Clear;

    QryCONCUS.SQL.Text := 'SELECT ORDEM ' +
                          '  FROM CONCUS ' +
                          ' WHERE ANO   = :ANO ' +
                          '   AND ORDEM = :ORDEM ' +
                          '   AND TAB   = :TAB ' +
                          '   AND ITEM  = :ITEM ' +
                          '   AND SUB   = :SUB ';

    QryCONCUS.Params.ParamByName('ANO').Value   := StrToInt(AnoTabela);
    QryCONCUS.Params.ParamByName('ORDEM').Value := QryCGJ.FieldByName('ORDEM').AsInteger;
    QryCONCUS.Params.ParamByName('TAB').Value   := QryCGJ.FieldByName('TABELA').AsString;
    QryCONCUS.Params.ParamByName('ITEM').Value  := QryCGJ.FieldByName('ITEM').AsString;
    QryCONCUS.Params.ParamByName('SUB').Value   := QryCGJ.FieldByName('SUBITEM').AsString;
    QryCONCUS.Open;

    QryAtu.Close;
    QryAtu.SQL.Clear;

    if QryCONCUS.RecordCount = 0 then  //Incluir
    begin
      QryAtu.SQL.Text := 'INSERT INTO CONCUS (ORDEM, ANO, VAI, TAB, ITEM, SUB, ' +
                         '                    DESCR, VALOR, TEXTO) ' +
                         '            VALUES (:ORDEM, :ANO, :VAI, :TAB, :ITEM, :SUB, ' +
                         '                    :DESCR, :VALOR, :TEXTO)';
    end
    else  //Alterar
    begin
      QryAtu.SQL.Text := 'UPDATE CONCUS ' +
                         '   SET DESCR = :DESCR, ' +
                         '       VALOR = :VALOR, ' +
                         '       TEXTO = :TEXTO, ' +
                         '       VAI   = :VAI ' +
                         ' WHERE ANO   = :ANO ' +
                         '   AND ORDEM = :ORDEM ' +
                         '   AND TAB   = :TAB ' +
                         '   AND ITEM  = :ITEM ' +
                         '   AND SUB   = :SUB ';
    end;

    QryAtu.Params.ParamByName('ORDEM').Value := QryCGJ.FieldByName('ORDEM').AsInteger;
    QryAtu.Params.ParamByName('ANO').Value   := Trim(AnoTabela);
    QryAtu.Params.ParamByName('VAI').Value   := 'S';
    QryAtu.Params.ParamByName('TAB').Value   := QryCGJ.FieldByName('TABELA').AsString;
    QryAtu.Params.ParamByName('ITEM').Value  := QryCGJ.FieldByName('ITEM').AsString;
    QryAtu.Params.ParamByName('SUB').Value   := QryCGJ.FieldByName('SUBITEM').AsString;
    QryAtu.Params.ParamByName('DESCR').Value := QryCGJ.FieldByName('DESCRICAO').AsString;
    QryAtu.Params.ParamByName('VALOR').Value := GR.NoRound(QryCGJ.FieldByName('VALOR').AsFloat, 2);
    QryAtu.Params.ParamByName('TEXTO').Value := QryCGJ.FieldByName('DESCRICAO').AsString;

    QryAtu.ExecSQL;

    QryCGJ.Next;
  end;

  FreeAndNil(QryCGJ);
  FreeAndNil(QryCONCUS);
  FreeAndNil(QryAtu);
end;

procedure TVersaoSistema.AtualizaTabelaTABCUS;
var
  QryITENS, QryTABCUS, QryAtu: TFDQuery;
  IdCus: Integer;
begin
  GR.CriarFDQuery(QryITENS,'',Gdm.FDGERENCIAL);
  GR.CriarFDQuery(QryTABCUS,'',dm.conCENTRAL);
  GR.CriarFDQuery(QryAtu,'',dm.conCENTRAL);

  IdCus := dm.ProximoId('ID_CUS',
                        'TABCUS',
                        2,
                        False);

  QryITENS.Close;
  QryITENS.SQL.Clear;
  QryITENS.SQL.Text := 'SELECT I.CODIGO, ' +
                       '       I.TABELA, ' +
                       '       I.ITEM, ' +
                       '       I.SUBITEM, ' +
                       '       I.DESCRICAO, ' +
                       '       I.VALOR, ' +
                       '       I.QTD, ' +
                       '       I.TOTAL ' +
                       '  FROM ITENS I ' +
                       ' INNER JOIN ADICIONAL A ' +
                       '    ON I.ID_ADICIONAL = A.ID_ADICIONAL ' +
                       ' WHERE A.ATRIBUICAO = 4';
  QryITENS.Open;

  QryITENS.First;

  while not QryITENS.Eof do
  begin
    QryTABCUS.Close;
    QryTABCUS.SQL.Clear;

    QryTABCUS.SQL.Text := 'SELECT ID_CUS ' +
                          '  FROM TABCUS ' +
                          ' WHERE COD = :COD ' +
                          '   AND TAB = :TAB ' +
                          '   AND ITEM = :ITEM ' +
                          '   AND SUB = :SUB ' +
                          '   AND DESCR = :DESCR ' +
                          '   AND VALOR = :VALOR ' +
                          '   AND QTD = :QTD ' +
                          '   AND TOTAL = :TOTAL';

    QryTABCUS.Params.ParamByName('COD').Value   := QryITENS.FieldByName('CODIGO').AsInteger;
    QryTABCUS.Params.ParamByName('TAB').Value   := QryITENS.FieldByName('TABELA').AsString;
    QryTABCUS.Params.ParamByName('ITEM').Value  := QryITENS.FieldByName('ITEM').AsString;
    QryTABCUS.Params.ParamByName('SUB').Value   := QryITENS.FieldByName('SUBITEM').AsString;
    QryTABCUS.Params.ParamByName('DESCR').Value := QryITENS.FieldByName('DESCRICAO').AsString;
    QryTABCUS.Params.ParamByName('VALOR').Value := QryITENS.FieldByName('VALOR').AsFloat;
    QryTABCUS.Params.ParamByName('QTD').Value   := QryITENS.FieldByName('QTD').AsString;
    QryTABCUS.Params.ParamByName('TOTAL').Value := QryITENS.FieldByName('TOTAL').AsFloat;
    QryTABCUS.Open;

    QryAtu.Close;
    QryAtu.SQL.Clear;

    if QryTABCUS.RecordCount = 0 then
    begin
      QryAtu.SQL.Text := 'INSERT INTO TABCUS (ID_CUS, COD, TAB, ITEM, SUB, DESCR, VALOR, QTD, TOTAL) ' +
                         '            VALUES (:ID_CUS, :COD, :TAB, :ITEM, :SUB, :DESCR, :VALOR, :QTD, :TOTAL)';

      Inc(IdCus);

      QryAtu.Params.ParamByName('ID_CUS').Value := IdCus;
    end
    else
    begin
      QryAtu.SQL.Text := 'UPDATE TABCUS ' +
                         '   SET COD   = :COD, ' +
                         '       TAB   = :TAB, ' +
                         '       ITEM  = :ITEM, ' +
                         '       SUB   = :SUB, ' +
                         '       DESCR = :DESCR, ' +
                         '       VALOR = :VALOR, ' +
                         '       QTD   = :QTD, ' +
                         '       TOTAL = :TOTAL ' +
                         ' WHERE ID_CUS = :ID_CUS';

      QryAtu.Params.ParamByName('ID_CUS').Value := QryTABCUS.FieldByName('ID_CUS').AsInteger;
    end;

    QryAtu.Params.ParamByName('COD').Value    := QryITENS.FieldByName('CODIGO').AsInteger;
    QryAtu.Params.ParamByName('TAB').Value    := QryITENS.FieldByName('TABELA').AsString;
    QryAtu.Params.ParamByName('ITEM').Value   := QryITENS.FieldByName('ITEM').AsString;
    QryAtu.Params.ParamByName('SUB').Value    := QryITENS.FieldByName('SUBITEM').AsString;
    QryAtu.Params.ParamByName('DESCR').Value  := QryITENS.FieldByName('DESCRICAO').AsString;
    QryAtu.Params.ParamByName('VALOR').Value  := GR.NoRound(QryITENS.FieldByName('VALOR').AsFloat, 2);
    QryAtu.Params.ParamByName('QTD').Value    := QryITENS.FieldByName('QTD').AsString;
    QryAtu.Params.ParamByName('TOTAL').Value  := GR.NoRound(QryITENS.FieldByName('TOTAL').AsFloat, 2);

    QryAtu.ExecSQL;

    QryITENS.Next;
  end;

  FreeAndNil(QryITENS);
  FreeAndNil(QryTABCUS);
  FreeAndNil(QryAtu);
end;

procedure TVersaoSistema.GravarNovaVersaoSistema(NovaVersao: String; var Menssagem: String);
var
  QryAtuV: TFDQuery;
  sData: String;
  iCont: Integer;
begin
  sData := '';

  GR.Processando(True,
                 'Atualiza��o do Sistema Central Protesto',
                 True,
                 'Por favor, aguarde...');

  Menssagem := '***************************************' +
               '******** RESUMO DA ATUALIZA��O ********' +
               '***************************************' + #13#10 + #13#10;

  GR.CriarFDQuery(QryAtuV,'',dm.conSISTEMA);

  if Trim(NovaVersao) = '1.0.0.2' then
  begin
    dm.CriarCampoFD('FLG_PAGAANTECIPADO',
                    'PORTADORES',
                    'CHAR(1) DEFAULT ' + QuotedStr('N'),
                    dm.conSISTEMA);

    dm.DMLUpdateFD('PORTADORES',
                   'FLG_PAGAANTECIPADO = ' + QuotedStr('N'),
                   dm.conSISTEMA);

    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('010'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('013'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('014'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('055'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('058'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('071'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('072'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('073'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('076'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('078'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('079'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('085'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('086'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('091'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('093'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('094'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('095'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('096'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('098'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('099'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('102'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('106'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('110'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('121'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('123'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('141'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('149'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('154'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('155'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('156'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('157'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('167'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('169'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('170'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('180'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('181'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('182'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('183'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('189'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('190'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('196'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('198'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('199'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('218'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('238'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('240'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('248'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('253'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('260'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('268'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('269'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('270'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('272'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('273'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('274'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('345'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('367'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('368'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('371'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('374'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('375'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('383'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('385'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('390'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('391'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('396'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('411'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('413'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('414'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('433'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('434'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('461'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('462'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('469'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('470'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('476'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('481'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('486'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('506'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('507'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('510'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('512'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('513'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('514'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('515'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('581'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('582'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('587'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('615'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('619'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('632'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('636'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('644'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('646'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('651'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('652'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('666'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('668'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('670'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('671'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('672'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('673'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('674'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('676'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('678'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('679'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('680'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('681'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('682'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('683'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('684'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('688'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('689'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('690'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('693'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('694'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('696'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('697'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('698'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('699'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('701'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('703'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('704'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('706'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('709'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('710'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('711'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('712'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('714'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('716'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('717'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('722'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('752'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('753'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('754'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('757'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('758'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('761'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('763'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('764'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('765'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('766'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('767'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('768'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('769'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('770'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('771'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('772'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('773'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('774'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('775'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('776'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('777'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('779'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('781'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('782'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('783'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('784'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('786'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('788'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('789'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('791'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('792'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('793'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('794'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('796'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('797'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('798'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('799'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('801'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('807'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('809'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('811'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('812'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('813'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('814'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('821'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('822'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('823'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('824'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('825'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('827'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('828'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('829'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('830'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('831'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('832'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('833'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('834'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('835'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('836'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('837'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('838'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('839'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('840'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('841'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('843'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('844'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('845'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('846'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('848'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('849'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('851'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('852'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('853'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('855'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('857'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('858'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('861'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('862'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('863'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('864'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('865'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('868'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('870'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('871'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('872'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('873'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('874'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('875'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('876'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('877'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('878'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('879'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('880'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('881'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('883'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('884'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('885'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('886'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('890'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('891'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('893'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('894'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('896'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('897'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('898'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('899'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('900'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('901'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('902'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('903'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('904'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('905'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('911'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('920'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('923'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('925'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('926'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('927'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('928'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('929'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('930'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('932'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('933'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('934'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('935'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('936'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('940'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('941'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('942'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('943'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('946'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('949'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('950'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('951'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('953'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('954'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('957'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('962'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('963'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('965'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('967'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('969'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('970'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('971'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('972'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('973'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('974'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('975'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('976'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('977'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('978'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('980'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('984'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('985'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('986'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('987'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('988'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('989'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('990'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('991'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('994'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('995'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('996'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('999'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A00'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A01'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A02'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A04'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A05'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A06'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A07'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A08'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A09'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A10'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A11'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A12'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A14'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A15'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A16'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A17'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A18'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A19'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A20'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A21'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A23'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A24'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A25'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A26'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A27'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A28'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A29'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A30'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A31'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A32'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A33'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A34'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A35'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('a36'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A37'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('a38'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A39'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A40'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A41'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A42'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A43'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A44'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A46'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A59'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A60'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A63'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A64'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A65'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A66'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A67'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A68'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A69'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A70'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A71'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A72'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A74'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A75'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A76'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A77'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A78'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A79'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A80'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A81'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A82'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A83'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A84'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A85'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A86'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A89'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A90'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A91'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A92'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A93'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A95'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A96'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A97'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A98'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('A99'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B01'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B03'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B04'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B05'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B07'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B08'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B10'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B11'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('b12'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B13'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B14'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B15'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B16'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B17'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B18'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B19'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B20'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B21'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B22'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B23'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B25'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B26'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B27'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B28'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B29'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B30'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B31'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B32'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B33'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B34'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B35'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B36'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B37'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B38'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B40'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B41'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B43'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B44'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B45'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B46'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B47'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B49'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B50'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B52'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B53'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B54'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B55'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B56'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B57'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B58'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B60'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B61'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B63'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B64'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B65'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B66'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B67'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B68'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B69'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B70'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B80'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B82'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B83'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B84'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B85'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B86'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B87'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B88'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B89'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B90'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B91'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B92'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B93'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B94'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B95'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B96'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B97'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B98'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('B99'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C01'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C03'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C04'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C06'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C07'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C08'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C09'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C10'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C11'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C12'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C13'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C14'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C16'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C17'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C18'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C19'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C20'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C21'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C22'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C23'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C24'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C26'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C27'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C28'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C29'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C30'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C31'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C32'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C33'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C34'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C35'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C36'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C37'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C38'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C39'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C40'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C41'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C42'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C43'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C44'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C45'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C46'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C47'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C48'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C49'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C50'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C51'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C52'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C53'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C54'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C55'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C56'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C57'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C58'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C59'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C60'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C61'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C62'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C63'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C64'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C65'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C66'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C67'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C68'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C69'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C70'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C71'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C72'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C73'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C74'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C75'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C76'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C77'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C78'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C79'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C80'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C81'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C82'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C83'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C84'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C85'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C86'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C87'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C88'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C89'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C90'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C91'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C92'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C93'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C94'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C95'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C96'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C97'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C98'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('C99'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D01'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D02'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D03'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D04'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D05'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D06'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D07'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D08'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D09'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D10'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D11'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D12'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D13'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D14'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D15'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D17'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D18'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D19'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D20'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D21'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D22'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D23'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D24'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D25'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D26'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D27'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D28'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D29'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D30'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D31'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D32'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D33'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D34'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D35'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D36'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D37'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D38'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D39'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D40'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D41'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D42'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D43'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D44'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D45'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D46'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D47'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D48'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D50'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D51'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D52'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D53'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D54'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D55'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D56'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D57'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D58'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D59'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D60'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D61'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D62'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D63'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D64'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D65'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D66'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D67'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D68'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D69'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D70'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D72'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D73'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D74'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D75'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D76'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D77'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D78'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D79'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D80'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D81'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D82'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D83'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D84'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D85'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D86'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D87'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D88'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D89'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D90'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D91'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D92'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D93'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D94'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D95'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D96'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D97'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D98'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('D99'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E01'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E02'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E03'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E04'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E05'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E06'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E07'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E08'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E09'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E10'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E11'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E12'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E13'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E14'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E15'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E16'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E18'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E20'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E21'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E22'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E23'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E24'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E25'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E26'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E27'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E28'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E29'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E30'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E31'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E32'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E33'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E34'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E35'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E36'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E37'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E39'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E40'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E41'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E42'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E43'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E44'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E45'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E47'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E48'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E49'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E50'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E51'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E52'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E53'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E54'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E55'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E56'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E57'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E58'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E59'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E60'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E61'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E62'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E63'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E64'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E65'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E66'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E67'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E68'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E69'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E70'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E71'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E72'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E73'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E74'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E75'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E76'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E77'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E78'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E79'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E80'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E81'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E82'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E83'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E84'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E85'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E86'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E87'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E88'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E89'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E90'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E91'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E92'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E93'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E94'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E95'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E96'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E97'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E98'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('E99'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F01'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F02'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F03'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F04'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F05'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F06'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F07'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F08'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F09'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F10'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F12'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F13'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F14'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F15'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F16'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F17'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F18'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F19'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F20'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F21'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F22'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F23'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F24'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F25'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F26'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F27'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F28'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F29'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F30'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F31'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F32'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F33'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F34'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F35'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F36'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F37'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F38'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F39'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F40'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F41'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F42'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F43'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F44'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F45'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F46'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F47'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F48'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F49'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F50'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F51'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F52'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F53'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F54'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F55'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F56'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F57'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F58'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F59'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F60'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F61'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F62'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F63'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F64'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F65'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F66'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F67'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F68'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F69'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F70'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F71'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F72'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F73'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F74'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F75'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F76'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F77'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F78'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F79'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F80'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F81'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F82'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F83'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F84'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F85'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F86'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F87'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F88'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F89'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F90'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F91'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F92'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F93'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F94'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F95'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F96'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F97'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F98'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('F99'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G01'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G02'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G03'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G04'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G05'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G06'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G07'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G08'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G09'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G10'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G11'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G12'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G13'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G14'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G15'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G16'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G17'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G18'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G19'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G20'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G21'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G22'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G23'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G24'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G25'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G26'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G27'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G28'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G29'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G30'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G31'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G32'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G33'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G34'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G35'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G36'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G37'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G38'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G39'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G40'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G41'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G42'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G43'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G45'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G47'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G48'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G49'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G50'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G51'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G52'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G53'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G54'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G55'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G56'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G57'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G58'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G59'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G60'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G61'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G62'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G63'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G64'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G65'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G66'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G67'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G68'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G69'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G70'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G71'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G72'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G74'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G75'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G76'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G77'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G78'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G79'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G80'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G81'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G82'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G83'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G84'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G85'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G86'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G87'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G88'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G89'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G90'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G91'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G92'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('S') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G93'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G94'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G95'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G96'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G97'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G98'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('G99'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H01'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H02'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H03'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H04'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H05'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H06'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H07'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H08'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H09'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H10'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H11'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H12'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H13'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H14'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H15'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H16'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H17'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H18'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H19'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H20'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H21'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H22'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H23'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H24'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H25'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H26'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H27'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H28'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H29'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H30'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H31'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H32'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H33'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H35'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H36'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H37'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H38'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H39'));
    dm.DMLUpdateFD('PORTADORES', 'FLG_PAGAANTECIPADO = ' + QuotedStr('N') + ', CONVENIO = ' + QuotedStr('S'), dm.conSISTEMA, 'CODIGO = ' + QuotedStr('H40'));

    sData := '27/10/2017';

    Menssagem := Menssagem + '- Inclu�da a condi��o de pagamento antecipado para os Portadores com Conv�nio';
  end;

  if Trim(NovaVersao) = '1.0.0.3' then
  begin
    iCont := -1;

    QryAtuV.Close;
    QryAtuV.SQL.Clear;
    QryAtuV.SQL.Text := 'SELECT * ' +
                        '  FROM IRREGULARIDADES ' +
                        ' WHERE CODIGO > 68';
    QryAtuV.Open;

    QryAtuV.First;

    while not QryAtuV.Eof do
    begin
      iCont := (iCont - 1);

      dm.DMLUpdateFD('IRREGULARIDADES', 'CODIGO = ' + IntToStr(iCont), dm.conSISTEMA, 'CODIGO = ' + QryAtuV.FieldByName('CODIGO').AsString);

      QryAtuV.Next;
    end;

    sData := '26/11/2017';

    Menssagem := Menssagem + '- Realizado acerto no cadastro de Irregularidades';
  end;

  if Trim(NovaVersao) = '1.0.0.4' then
  begin
    sData := '14/12/2017';

    Menssagem := Menssagem + '- Realizado acerto na importa��o de T�tulos com mais de um Devedor.' + #13#10 +
                             '- Inclu�da a exibi��o de todos os Devedores atrelados ao Ato na tela de cadastro/visualiza��o de T�tulo.' + #13#10 +
                             '- Inclu�da a exibi��o de todos os Devedores atrelados ao Ato na tela de Entrada Balc�o.';
  end;

  if Trim(NovaVersao) = '1.0.0.5' then
  begin
    sData := '15/12/2017 - 12:00';

    Menssagem := Menssagem + '- Realizado acerto na gera��o do arquivo de Confirma��o.';
  end;

  if Trim(NovaVersao) = '1.0.0.6' then
  begin
    sData := '15/12/2017 - 16:00';

    Menssagem := Menssagem + '- Realizado acerto na gera��o do arquivo de Remessa para as Serventias Agregadas.';
  end;

  if Trim(NovaVersao) = '1.0.0.7' then
  begin
    sData := '19/12/2017';

    Menssagem := Menssagem + '- Realizado acerto na gera��o do arquivo de Remessa para as Serventias Agregadas.';
  end;

  if Trim(NovaVersao) = '1.0.0.8' then
  begin
    sData := '21/12/2017 - 11:00';

    Menssagem := Menssagem + '- Altera��o na verifica��o dos T�tulos em Duplicidade durante a gera��o do arquivo de Remessa para as Serventias Agregadas.';
  end;

  if Trim(NovaVersao) = '1.0.0.9' then
  begin
    sData := '22/12/2017 - 13:00';

    Menssagem := Menssagem + '- Realizado acerto na ordena��o do Devedores durante a gera��o do arquivo de Remessa para as Serventias Agregadas.';
  end;

  if Trim(NovaVersao) = '1.0.0.10' then
  begin
    sData := '02/01/2018 - 12:00';

    Menssagem := Menssagem + '- Realizado acerto na atualiza��o dos Feriados do ano vigente.';
  end;

  if Trim(NovaVersao) = '1.0.0.11' then
  begin
    sData := '15/01/2018';

    Menssagem := Menssagem + '- Altera��o no filtro de pesquisa do Relat�rio de T�tulos Distribu�dos e no layout do respectivo relat�rio.';
  end;

  if Trim(NovaVersao) = '1.0.0.12' then
  begin
    sData := '16/01/2018 - 12:00';

    Menssagem := Menssagem + '- Acerto nos somat�rios do Relat�rio de T�tulos Distribu�dos.';
  end;

  if Trim(NovaVersao) = '1.0.0.13' then
  begin
    sData := '22/02/2018 - 15:00';

    Menssagem := Menssagem + '- Altera��o dos c�lculos envolvendo os valores de M�tua e Acoterj.';
  end;

  if Trim(NovaVersao) = '1.0.0.14' then
  begin
    sData := '01/03/2018';

    Menssagem := Menssagem + '- Acerto na Exporta��o de T�tulos para as serventias Agregadas.';
  end;

  if Trim(NovaVersao) = '1.0.0.15' then
  begin
    sData := '14/08/2018 - 11:20';

    Menssagem := Menssagem + '- Inclus�o de op��o para o usu�rio trocar o nome do Portador pelo nome do Cedente na impress�o da Ordem de Protesto.';
  end;

  if Trim(NovaVersao) = '1.0.0.16' then
  begin
    sData := '28/08/2018';

    Menssagem := Menssagem + '- Altera��o na gera��o de Ordem de Protesto para que a troca do nome do Portador pelo nome do Cedente na impress�o (quando o Portador for E19) passar a ser feita automaticamente.';
  end;

  if Trim(NovaVersao) = '1.0.0.17' then
  begin
    sData := '12/10/2018 - 23:00';

    Menssagem := Menssagem + '- Inclus�o da desmarca��o autom�tica de T�tulo quando informada Irregularidade no mesmo.';
  end;

  if Trim(NovaVersao) = '1.0.0.18' then
  begin
    sData := '01/01/2019 - 09:00';

    Menssagem := Menssagem + '- Atualiza��o da tabela 2019.';
  end;

  if Trim(NovaVersao) = '1.0.0.19' then
  begin
    sData := '27/11/2019 - 09:00';

    GR.CriarCampoDAC('POSTECIPADO','TITULOS','CHAR(1) DEFAULT ''''',dm.conSISTEMA,'USADO PARA CLAUSULA DE BARREIRA');

    Menssagem := Menssagem + '- Atualiza��o referente ao Provimento n�86.';
  end;

  if Trim(NovaVersao) = '1.0.0.20' then
  begin
    sData := '04/01/2021 - 09:00';

    Menssagem := Menssagem + '- Atualiza��o da tabela 2021.';
  end;

  //Atualiza o numero da Versao do Sistema na tabela VERSAO
  dm.DMLUpdateFD('VERSAO',
                 'VERSAO_SISTEMA = ' + QuotedStr(NovaVersao) + ', ' +
                 'DATA = ' + QuotedStr(IfThen(Length(sData) > 10,
                                              sData,
                                              sData + ' - 09:00')),
                 dm.conSISTEMA);

  dm.DMLSelectFD(QryAtuV, 'DATA', 'VERSAO', dm.conSISTEMA);

  dm.vAtualizacao := QryAtuV.FieldByName('DATA').AsString;

  FreeAndNil(QryAtuV);

  GR.Processando(False);
end;

function TVersaoSistema.RetornarVersaoSistema: String;
var
  Exe: String;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);

  Size := GetFileVersionInfoSize(PChar(Exe), Handle);

  if Size = 0 then
    RaiseLastOSError;

  SetLength(Buffer, Size);

  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;

  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;

  Result := Format('%d.%d.%d.%d',
                   [LongRec(FixedPtr.dwFileVersionMS).Hi,  //Major
                    LongRec(FixedPtr.dwFileVersionMS).Lo,  //Minor
                    LongRec(FixedPtr.dwFileVersionLS).Hi,  //Release
                    LongRec(FixedPtr.dwFileVersionLS).Lo]) //Build
end;

function TVersaoSistema.RetornarVersaoTabela: String;
//var
  //QryVersao: TFDQuery;
begin
  //GR.CriarFDQuery(QryVersao,'',Gdm.FDGERENCIAL);

  {QryVersao.Close;
  QryVersao.SQL.Clear;
  QryVersao.SQL.Text := 'SELECT ANO_TABELA, ' +
                        '       MUTUA, ' +
                        '       ACOTERJ, ' +
                        '       DISTRIBUICAO1 ' +
                        '  FROM PARAMETROS';
  QryVersao.Open;

  if QryVersao.RecordCount = 0 then
  begin
    Application.MessageBox('N�o foi encontrada uma tabela de Atos para atualizar. Por favor, entre em contato com o Suporte.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
    Result := '';
    Exit;
  end;

  if (Trim(QryVersao.FieldByName('ANO_TABELA').AsString) <> Trim(dm.ValorParametro(77))) or
    (StrToFloat(QryVersao.FieldByName('MUTUA').AsString) <> StrToFloat(dm.ValorParametro(26))) or
    (StrToFloat(QryVersao.FieldByName('ACOTERJ').AsString) <> StrToFloat(dm.ValorParametro(27))) or
    (StrToFloat(QryVersao.FieldByName('DISTRIBUICAO1').AsString) <> StrToFloat(dm.ValorParametro(28))) then
  begin
    if (Trim(QryVersao.FieldByName('ANO_TABELA').AsString) <> '') and
      (Trim(QryVersao.FieldByName('ANO_TABELA').AsString) <> '0') and
      (Trim(QryVersao.FieldByName('ANO_TABELA').AsString) <> Trim(dm.ValorParametro(77))) then
    begin
      if Application.MessageBox(PChar('Saiu a nova Tabela para ' + Trim(QryVersao.FieldByName('ANO_TABELA').AsString) + '!' + #13#10 +
                                      'Deseja realizar a Atualiza��o agora?'),
                                PChar('TABELA ' + Trim(QryVersao.FieldByName('ANO_TABELA').AsString)),
                                MB_YESNO + MB_ICONQUESTION) = ID_YES then
        AtualizarTabelaCGJ(QryVersao.FieldByName('ANO_TABELA').AsString,
                           QryVersao.FieldByName('MUTUA').AsCurrency,
                           QryVersao.FieldByName('ACOTERJ').AsCurrency,
                           QryVersao.FieldByName('DISTRIBUICAO1').AsCurrency,
                           True);
    end
    else
    begin
      AtualizarTabelaCGJ(QryVersao.FieldByName('ANO_TABELA').AsString,
                         QryVersao.FieldByName('MUTUA').AsCurrency,
                         QryVersao.FieldByName('ACOTERJ').AsCurrency,
                         QryVersao.FieldByName('DISTRIBUICAO1').AsCurrency,
                         False);
    end;
  end;

  Result := Trim(QryVersao.FieldByName('ANO_TABELA').AsString);}

  //FreeAndNil(QryVersao);
end;

procedure TVersaoSistema.VerificarVersaoSistema(var Mensagem: String);
var
  QryVersao: TFDQuery;
  sVersaoAtual: String;
begin
  dm.vVersaoTabela := RetornarVersaoTabela;
  sVersaoAtual     := RetornarVersaoSistema;

  //QryVersao := Gdm.CriarFDQuery(nil, dm.conSISTEMA);

  GR.CriarFDQuery(QryVersao,'',dm.conSISTEMA);

  QryVersao.Close;
  QryVersao.SQL.Clear;
  QryVersao.SQL.Text := 'SELECT * FROM VERSAO';
  QryVersao.Open;

  if (Trim(QryVersao.FieldByName('VERSAO_SISTEMA').AsString) <> Trim(sVersaoAtual)) or
    (QryVersao.RecordCount = 0) then
    GravarNovaVersaoSistema(sVersaoAtual, Mensagem)
  else
  begin
    dm.vAtualizacao := QryVersao.FieldByName('DATA').AsString;
    Mensagem        := '';
  end;

  FreeAndNil(QryVersao);
end;

end.
