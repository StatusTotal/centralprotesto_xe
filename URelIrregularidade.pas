unit URelIrregularidade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, ExtCtrls, sPanel, sEdit, sGroupBox,
  QuickRpt, QRCtrls, DB, RxMemDS, FMTBcd, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, sRadioButton, SqlExpr, Grids, Wwdbigrd,
  Wwdbgrid, DBClient, SimpleDS, Menus, DBCtrls, sDBLookupComboBox,
  sDBDateEdit, sLabel, acDBTextFX, Data.DBXFirebird, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFRelIrregularidade = class(TForm)
    P: TsPanel;
    GbProtocolo: TsGroupBox;
    edProtFinal: TsEdit;
    edProtInicial: TsEdit;
    Rb1: TsRadioButton;
    GbData: TsGroupBox;
    Rb2: TsRadioButton;
    edDataInicial: TsDateEdit;
    edDataFinal: TsDateEdit;
    dsProtocolo: TDataSource;
    GridAtos: TwwDBGrid;
    btFiltrar: TsBitBtn;
    btVisualizar: TsBitBtn;
    PM: TPopupMenu;
    Marcartodos1: TMenuItem;
    Desmarcartodos1: TMenuItem;
    qryProtocolo: TFDQuery;
    qryProtocoloID_ATO: TIntegerField;
    qryProtocoloPROTOCOLO: TIntegerField;
    qryProtocoloDT_PROTOCOLO: TDateField;
    qryProtocoloAPRESENTANTE: TStringField;
    qryProtocoloDEVEDOR: TStringField;
    qryProtocoloSTATUS: TStringField;
    qryProtocoloMOTIVO_INTIMACAO: TMemoField;
    qryProtocoloDT_PUBLICACAO: TDateField;
    qryProtocoloTIPO_INTIMACAO: TStringField;
    qryProtocoloDT_DEVOLVIDO: TDateField;
    qryProtocoloDT_RETIRADO: TDateField;
    qryProtocoloIRREGULARIDADE: TIntegerField;
    qryProtocoloCheck: TBooleanField;
    procedure edProtInicialKeyPress(Sender: TObject; var Key: Char);
    procedure edProtFinalKeyPress(Sender: TObject; var Key: Char);
    procedure Rb1Click(Sender: TObject);
    procedure Rb2Click(Sender: TObject);
    procedure btFiltrarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridAtosTitleButtonClick(Sender: TObject;
      AFieldName: String);
    procedure btVisualizarClick(Sender: TObject);
    procedure edDataInicialExit(Sender: TObject);
    procedure Marcartodos1Click(Sender: TObject);
    procedure Desmarcartodos1Click(Sender: TObject);
    procedure GridAtosMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure GridAtosMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelIrregularidade: TFRelIrregularidade;

implementation

uses UDM, UPF, Math, UDadosEdital, UAssinatura, UDevolvido, UQuickAviso1,
  UGeral;

{$R *.dfm}

procedure TFRelIrregularidade.edProtInicialKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFRelIrregularidade.edProtFinalKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFRelIrregularidade.Rb1Click(Sender: TObject);
begin
  GbData.Enabled:=False;
  GbProtocolo.Enabled:=True;
  edDataInicial.Clear;
  edDataFinal.Clear;
  edProtInicial.SetFocus;
end;

procedure TFRelIrregularidade.Rb2Click(Sender: TObject);
begin
  GbData.Enabled:=True;
  GbProtocolo.Enabled:=False;
  edProtInicial.Clear;
  edProtFinal.Clear;
  edDataInicial.SetFocus;
end;

procedure TFRelIrregularidade.btFiltrarClick(Sender: TObject);
begin
  if Rb1.Checked then
  if edProtInicial.Text='' then
  begin
      edProtInicial.SetFocus;
      Exit;
  end;

  if edProtFinal.Text='' then edProtFinal.Text:=edProtInicial.Text;

  qryProtocolo.Close;
  if Rb1.Checked then
  begin
      qryProtocolo.SQL.Text:='SELECT ID_ATO,PROTOCOLO,DT_PROTOCOLO,APRESENTANTE,DEVEDOR,STATUS,'+
                             'MOTIVO_INTIMACAO,DT_PUBLICACAO,TIPO_INTIMACAO,DT_DEVOLVIDO,'+
                             'DT_RETIRADO,IRREGULARIDADE FROM TITULOS WHERE PROTOCOLO BETWEEN :P1 AND :P2';
      qryProtocolo.ParamByName('P1').AsInteger:=StrToInt(edProtInicial.Text);
      qryProtocolo.ParamByName('P2').AsInteger:=StrToInt(edProtFinal.Text);
  end
  else
  begin
      qryProtocolo.SQL.Text:='SELECT ID_ATO,PROTOCOLO,DT_PROTOCOLO,APRESENTANTE,DEVEDOR,STATUS,'+
                             'MOTIVO_INTIMACAO,DT_PUBLICACAO,TIPO_INTIMACAO,DT_DEVOLVIDO,'+
                             'DT_RETIRADO,IRREGULARIDADE FROM TITULOS WHERE DT_PROTOCOLO BETWEEN :D1 AND :D2';
      qryProtocolo.ParamByName('D1').AsDate:=edDataInicial.Date;
      qryProtocolo.ParamByName('D2').AsDate:=edDataFinal.Date;
  end;
  qryProtocolo.Open;
end;

procedure TFRelIrregularidade.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFRelIrregularidade.GridAtosTitleButtonClick(Sender: TObject;
  AFieldName: String);
begin
  qryProtocolo.IndexFieldNames:=AFieldName;
end;

procedure TFRelIrregularidade.btVisualizarClick(Sender: TObject);
var
  Posicao: Integer;
begin
  if qryProtocolo.IsEmpty then Exit;

  try
    PF.Aguarde(True);
    Application.CreateForm(TFQuickAviso1,FQuickAviso1);

    with FQuickAviso1 do
    begin
        RX.Close;
        RX.Open;

        Posicao:=qryProtocolo.RecNo;
        qryProtocolo.DisableControls;
        qryProtocolo.First;
        while not qryProtocolo.Eof do
        begin
            if qryProtocoloCheck.Value=True then
            begin
                qryProtocolo.Edit;
                Application.CreateForm(TFDevolvido,FDevolvido);
                with FDevolvido do
                begin
                    Caption:='Irregularidade - Protocolo: '+qryProtocoloPROTOCOLO.AsString;
                    edData.BoundLabel.Caption:='Data';
                    lbMsg.Caption:='Informe abaixo a irregularidade do t�tulo';
                    ckRetirado.Visible:=True;
                    ckDevolvido.Visible:=True;
                    ShowModal;
                    if ckRetirado.Checked then
                    begin
                      if qryProtocolo.State <> dsEdit then
                        qryProtocolo.Edit;

                      qryProtocoloDT_DEVOLVIDO.Clear;
                      qryProtocoloDT_RETIRADO.AsDateTime:=edData.Date;
                      qryProtocoloSTATUS.AsString:='RETIRADO';
                    end;
                    if ckDevolvido.Checked then
                    begin
                      if qryProtocolo.State <> dsEdit then
                        qryProtocolo.Edit;

                      qryProtocoloDT_RETIRADO.Clear;
                      qryProtocoloDT_DEVOLVIDO.AsDateTime:=edData.Date;
                      qryProtocoloSTATUS.AsString:='DEVOLVIDO';
                    end;
                    Free;
                end;
                dm.Irregularidades.First;
                if not dm.IrregularidadesCheck.AsBoolean then
                  repeat dm.Irregularidades.Next;
                  until (dm.IrregularidadesCheck.AsBoolean) or (dm.Irregularidades.Eof);
                qryProtocoloIRREGULARIDADE.AsInteger:=dm.IrregularidadesCODIGO.AsInteger;
                qryProtocolo.Post;
                dm.Motivos.Close;
                dm.Titulos.Close;
                dm.Titulos.Params[0].AsInteger:=qryProtocoloID_ATO.AsInteger;
                dm.Titulos.Open;

                RX.Append;
                RXID_ATO.AsInteger          :=dm.TitulosID_ATO.AsInteger;
                RXTitulo.AsString           :=PF.RetornarTitulo(dm.TitulosTIPO_TITULO.AsInteger);
                RXApresentante.AsString     :=dm.TitulosAPRESENTANTE.AsString;
                RXSacador.AsString          :=dm.TitulosSACADOR.AsString;
                RXDevedor.AsString          :=dm.TitulosDEVEDOR.AsString;
                RXValor.AsFloat             :=dm.TitulosVALOR_TITULO.AsFloat;
                RXCustas.AsFloat            :=dm.TitulosTOTAL.AsFloat;
                RXEmol.AsFloat              :=dm.TitulosEMOLUMENTOS.AsFloat;
                RXFetj.AsFloat              :=dm.TitulosFETJ.AsFloat;
                RXFund.AsFloat              :=dm.TitulosFUNDPERJ.AsFloat;
                RXFunp.AsFloat              :=dm.TitulosFUNPERJ.AsFloat;
                RXMutua.AsFloat             :=dm.TitulosMUTUA.AsFloat;
                RXAcoterj.AsFloat           :=dm.TitulosACOTERJ.AsFloat;
                RXDist.AsFloat              :=dm.TitulosDISTRIBUICAO.AsFloat;
                RXTotal.AsFloat             :=dm.TitulosTOTAL.AsFloat;
                RXSaldo.AsFloat             :=dm.TitulosSALDO_PROTESTO.AsFloat;
                RXVencimento.AsDateTime     :=dm.TitulosDT_VENCIMENTO.AsDateTime;
                RXProtocolo.AsInteger       :=dm.TitulosPROTOCOLO.AsInteger;
                RXEspecie.AsString          :=dm.TitulosEspecie.AsString;

                if dm.TitulosCPF_CNPJ_DEVEDOR.AsString<>'' then
                  if dm.TitulosTIPO_DEVEDOR.AsString='F' then
                    RXCPF_CNPJ_Devedor.AsString:=PF.FormatarCPF(dm.TitulosCPF_CNPJ_DEVEDOR.AsString)
                      else
                        if dm.TitulosTIPO_DEVEDOR.AsString='J' then
                          RXCPF_CNPJ_Devedor.AsString:=PF.FormatarCNPJ(dm.TitulosCPF_CNPJ_DEVEDOR.AsString);

                RXTitulo.AsString           :=dm.TitulosNUMERO_TITULO.AsString;
                RXDt_Protocolo.AsDateTime   :=dm.TitulosDT_PROTOCOLO.AsDateTime;
                RXDt_Titulo.AsDateTime      :=dm.TitulosDT_TITULO.AsDateTime;

                PF.CarregarDevedor(dm.TitulosID_ATO.AsInteger);
                RXTipo.AsString    :=dm.RxDevedorTIPO.AsString; 
                RXEndereco.AsString:=dm.RxDevedorENDERECO.AsString+' '+dm.RxDevedorCEP.AsString+' '+dm.RxDevedorBAIRRO.AsString+' '+
                                     dm.RxDevedorMUNICIPIO.AsString+' '+dm.RxDevedorUF.AsString;

                RXDt_Intimacao.AsDateTime  :=dm.TitulosDT_INTIMACAO.AsDateTime;
                RXDt_Edital.AsDateTime     :=dm.TitulosDT_PUBLICACAO.AsDateTime;
                RXIrregularidade.AsString  :=PF.RetornarIrregularidade(qryProtocoloIRREGULARIDADE.AsInteger);
                RX.Post;
            end;
            qryProtocolo.Next;
        end;
        qryProtocolo.RecNo:=Posicao;
        qryProtocolo.EnableControls;

        if not RX.IsEmpty then
        begin
            GR.CriarForm(TFAssinatura,FAssinatura);
            Aviso.Preview;
        end
        else
          PF.Aguarde(False);

        Free;
    end;
  except
    on E:Exception do
    begin
        PF.Aguarde(False);
        GR.Aviso('Erro: '+E.Message);
    end;
  end;
end;

procedure TFRelIrregularidade.edDataInicialExit(Sender: TObject);
begin
  if edDataFinal.Date<edDataInicial.Date then
    edDataFinal.Date:=edDataInicial.Date;
end;

procedure TFRelIrregularidade.Marcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=qryProtocolo.RecNo;
  qryProtocolo.DisableControls;
  qryProtocolo.First;
  while not qryProtocolo.Eof do
  begin
      qryProtocolo.Edit;
      qryProtocoloCheck.AsBoolean:=True;
      qryProtocolo.Post;
      qryProtocolo.Next;
  end;
  qryProtocolo.RecNo:=Posicao;
  qryProtocolo.EnableControls;
end;

procedure TFRelIrregularidade.Desmarcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=qryProtocolo.RecNo;
  qryProtocolo.DisableControls;
  qryProtocolo.First;
  while not qryProtocolo.Eof do
  begin
      qryProtocolo.Edit;
      qryProtocoloCheck.AsBoolean:=False;
      qryProtocolo.Post;
      qryProtocolo.Next;
  end;
  qryProtocolo.RecNo:=Posicao;
  qryProtocolo.EnableControls;
end;

procedure TFRelIrregularidade.GridAtosMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if X in [12..51] then
    Screen.Cursor:=crHandPoint
      else Screen.Cursor:=crDefault;
end;

procedure TFRelIrregularidade.GridAtosMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Screen.Cursor=crHandPoint then
  begin
      if qryProtocolo.IsEmpty then Exit;
      qryProtocolo.Edit;
      if qryProtocoloCheck.AsBoolean then
        qryProtocoloCheck.AsBoolean:=False
          else qryProtocoloCheck.AsBoolean:=True;
      qryProtocolo.Post;
  end;
end;

end.
