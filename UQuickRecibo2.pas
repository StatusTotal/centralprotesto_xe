unit UQuickRecibo2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QuickRpt, QRCtrls, ExtCtrls, DB, RxMemDS, StdCtrls, ComCtrls;

type
  TFQuickRecibo2 = class(TForm)
    Recibo: TQuickRep;
    Titulo: TQRBand;
    lbNomeCartorio: TQRLabel;
    QRLabel2: TQRLabel;
    lbNomeTabeliao: TQRLabel;
    lbEndereco: TQRLabel;
    QRLabel1: TQRLabel;
    RT: TQRRichText;
    RX: TRxMemoryData;
    RXLivroProtocolo: TIntegerField;
    RXFolhaProtocolo: TStringField;
    RXStatus: TStringField;
    RXDataStatus: TDateField;
    RXDevedor: TStringField;
    RXDocumento: TStringField;
    RXCedente: TStringField;
    RXPortador: TStringField;
    RXSacador: TStringField;
    RXProtocolo: TIntegerField;
    RXValorTitulo: TFloatField;
    RXDataVencimento: TDateField;
    RXNumeroTitulo: TStringField;
    QRLabel3: TQRLabel;
    lbTipo: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    lbNomeDevedor: TQRLabel;
    lbCPFCNPJ: TQRLabel;
    lbNumeroTitulo: TQRLabel;
    RE: TRichEdit;
    lbNomeCedente: TQRLabel;
    lbNomePortador: TQRLabel;
    lbNomeSacador: TQRLabel;
    lbProtocolo: TQRLabel;
    lbValor: TQRLabel;
    lbDataVencimento: TQRLabel;
    lbReferido: TQRLabel;
    QRLabel12: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure TituloBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickRecibo2: TFQuickRecibo2;

implementation

uses UDM, UGeral;

{$R *.dfm}

procedure TFQuickRecibo2.FormCreate(Sender: TObject);
begin
  lbNomeCartorio.Caption  :=dm.ServentiaDESCRICAO.AsString;
  lbNomeTabeliao.Caption  :=dm.ServentiaTABELIAO.AsString;
  lbEndereco.Caption      :=dm.ServentiaENDERECO.AsString+' - '+dm.ServentiaCIDADE.AsString+' - '+dm.ServentiaUF.AsString+' - '+GR.FormatarTelefone(dm.ServentiaTELEFONE.AsString);
end;

procedure TFQuickRecibo2.TituloBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  RE.Lines.Clear;
  RE.Lines.Add('O t�tulo objeto do protocolo abaixo mencionado, constante do livro N� '+RXLivroProtocolo.AsString+' as folhas '+
               RXFolhaProtocolo.AsString+', distribu�do � este Cart�rio, foi '+RXStatus.AsString+' em '+FormatDateTime('dd/mm/yyyy.',RXDataStatus.AsDateTime));

  lbNomeDevedor.Caption     :=RXDevedor.AsString;
  lbCPFCNPJ.Caption         :=RXDocumento.AsString;
  lbNumeroTitulo.Caption    :=RXNumeroTitulo.AsString;
  lbNomeCedente.Caption     :=RXCedente.AsString;
  lbNomePortador.Caption    :=RXPortador.AsString;
  lbNomeSacador.Caption     :=RXSacador.AsString;
  lbProtocolo.Caption       :=RXProtocolo.AsString;
  lbValor.Caption           :=FloatToStrF(RXValorTitulo.AsFloat,ffNumber,15,2);
  if RXDataVencimento.AsDateTime<>0 then
  lbDataVencimento.Caption  :=FormatDateTime('dd/mm/yyyy',RXDataVencimento.AsDateTime);
  lbReferido.Caption        :='O referido, � verdade do que dou f�. '+dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',Now);

  if Length(RXDocumento.AsString)=14  then
  begin
      lbTipo.Caption:='CPF:';
      lbCPFCNPJ.Left:=79;
  end
  else
  begin
      lbTipo.Caption:='CNPJ:';
      lbCPFCNPJ.Left:=88;
  end;

  GR.JustificarParagrafo(RE.Text,80);
end;

end.
