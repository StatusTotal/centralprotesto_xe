�
 TFQUICKPENDENTES 0m�  TPF0TFQuickPendentesFQuickPendentesLeft� Top� CaptionFQuickPendentesClientHeight!ClientWidth�Color	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style OldCreateOrderScaledOnCreate
FormCreatePixelsPerInch`
TextHeight 	TQuickRep	QuickRep1LeftLTop� WidthcHeightShowingPreviewDataSetTitulosFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' 	OnEndPageQuickRep1EndPageOptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.OrientationpoLandscapePage.PaperSizeA4Page.ContinuousPage.Values       �@      @�
@       �@      ��
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPreviewWidth�PreviewHeight�PrevInitialZoomqrZoomToFitPreviewDefaultSaveTypestPDFPreviewLeft 
PreviewTop  TQRBandQRBand1Left&Top&WidthHeight� Frame.DrawTop	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values      ��@      #�
@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageHeader TQRLabel
lbCartorioLeft�TopWidth� HeightSize.Values������j�@������*�	@UUUUUUU�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   Nome do CartórioColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbEnderecoLeft�Top Width8HeightSize.Values�������@      ��	@UUUUUUU�@������*�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption	   EndereçoColorclWhiteTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbTituloLeft�Top@Width4HeightSize.Values�������@UUUUUU_�	@UUUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption
   RelatórioColorclWhiteTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel
lbPortadorLeftTophWidth8HeightSize.Values�������@UUUUUUU�@UUUUUU��@������*�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionPortadorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsUnderline 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  
TQRSysData
QRSysData2Left�TophWidthTHeightSize.Values�������@UUUUUUm�
@UUUUUU��@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandColorclWhiteDataqrsPageNumberFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontText	   Página: Transparent	ExportAsexptTextFontSize   TQRChildBand
ChildBand1Left&Top� WidthHeightFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUUU�@      #�
@ PreCaluculateBandHeightKeepOnOnePage
ParentBandQRBand1
PrintOrdercboAfterParent TQRLabelQRLabel1LeftTopWidth.HeightSize.Values      ��@ XUUUUU�@ XUUUUU� @ �����j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	ProtocoloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel2LeftrTopWidth*HeightSize.Values      ��@      Ж@ XUUUUU� @      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionDevedorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel3Left`TopWidthFHeightSize.Values      ��@ `UUUU��@ XUUUUU� @ XUUUU5�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption   Valor do TítuloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel5Left�TopWidth@HeightSize.Values      ��@      ܐ	@ XUUUUU� @ XUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionEmolumentosColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel6LeftTopWidth*HeightSize.Values      ��@      ��	@UUUUUUU� @      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionSacadorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel7Left�TopWidth)HeightSize.Values      ��@ �����j�	@ XUUUUU� @ `UUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption   Nº TítuloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel8Left@TopWidth/HeightSize.Values      ��@ XUUUU��
@ XUUUUU� @ XUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	   Nosso NºColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbSituacaoLeft�TopWidth/HeightSize.Values      ��@ XUUUUk�
@ XUUUUU� @ XUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption
   SituaçãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   TQRBandQRBand2Left&Top� WidthHeightFrame.DrawLeft	Frame.DrawRight	AlignToBottomBeforePrintQRBand2BeforePrintTransparentBandForceNewColumnForceNewPageSize.ValuesVUUUUU��@      #�
@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail 	TQRDBText	QRDBText1LeftTopWidthaHeightSize.Values      ��@ XUUUUU�@ XUUUUU�@ �����R�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetTitulos	DataFieldDataProtocoloFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText2LeftrTopWidth� HeightSize.Values      ��@      Ж@ XUUUUU�@      ̘@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetTitulos	DataFieldDEVEDORFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText3Left`TopWidthFHeightSize.Values      ��@ `UUUU��@ XUUUUU�@ XUUUU5�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetTitulos	DataFieldVALOR_TITULOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText4Left�TopWidth@HeightSize.Values      ��@      ܐ	@ XUUUUU�@ XUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetTitulos	DataFieldTOTALFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText5LeftTopWidth� HeightSize.Values      ��@      ��	@UUUUUUU�@      �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetTitulos	DataFieldSACADORFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText6Left�TopWidthXHeightSize.Values      ��@ �����j�	@ XUUUUU�@ `UUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetTitulos	DataFieldNUMERO_TITULOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText7Left@TopWidthyHeightSize.Values      ��@ XUUUU��
@ XUUUUU�@ ������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetTitulos	DataFieldNOSSO_NUMEROFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
qrSituacaoLeft�TopWidthTHeightSize.Values      ��@ XUUUUk�
@ XUUUUU�@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetTitulos	DataFieldSTATUSFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize   TQRBandQRBand3Left&Top� WidthHeightFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values������j�@      #�
@ PreCaluculateBandHeightKeepOnOnePageBandType	rbSummary 
TQRSysData
QRSysData1LeftTopWidth� HeightSize.Values�������@UUUUUUU�@UUUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataqrsDetailCountFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontText   Quantidade de Títulos: Transparent	ExportAsexptTextFontSize  TQRExprQRExpr1LeftaTopWidthFHeightSize.Values ������@ �����~�@ XUUUUU�@ XUUUU5�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrintTransparent
ExpressionSum(Titulos.VALOR_TITULO)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr2Left�TopWidth@HeightSize.Values ������@      ܐ	@ XUUUUU�@ XUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrintTransparent
ExpressionSum(Titulos.TOTAL)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize    	TQuickRepTodosLeft� TopxWidthcHeightShowingPreviewDataSetTodosPortadoresFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' 	OnEndPageTodosEndPageOptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.OrientationpoLandscapePage.PaperSizeA4Page.ContinuousPage.Values       �@      @�
@       �@      ��
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPreviewWidth�PreviewHeight�PrevInitialZoomqrZoomToFitPreviewDefaultSaveTypestPDFPreviewLeft 
PreviewTop  TQRBandQRBand4Left&Top&WidthHeight� Frame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values      ��@      #�
@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageHeader TQRLabellbCartorio2Left�TopWidth� HeightSize.Values������j�@������*�	@��������@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   Nome do CartórioColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbEndereco2Left�TopCWidth/HeightSize.Values�������@UUUUUU]�	@UUUUUUE�@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption	   EndereçoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	lbTitulo2Left�Top`Width4HeightSize.Values�������@UUUUUU_�	@       �@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption
   RelatórioColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  
TQRSysData
QRSysData3Left�TophWidthTHeightSize.Values�������@UUUUUUm�
@UUUUUU��@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandColorclWhiteDataqrsPageNumberFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontText	   Página: Transparent	ExportAsexptTextFontSize  TQRLabelQRLabel4LefthTopWidthFHeightSize.Values�������@�������@      ��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption,   PROTESTO DE TÍTULOS E DOCUMENTOS DE DÍVIDAColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel
lbTabeliaoLeft�Top4Width)HeightSize.Values�������@UUUUUU[�	@UUUUUU��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption	   TabeliãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   TQRBandDetalheLeft&Top� WidthHeightFrame.DrawLeft	Frame.DrawRight	AlignToBottomBeforePrintQRBand2BeforePrintTransparentBandForceNewColumnForceNewPageSize.ValuesVUUUUU��@      #�
@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail 	TQRDBText	QRDBText8LeftTopWidthEHeightSize.Values      ��@������*�@UUUUUUU�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetTodosPortadores	DataField	PROTOCOLOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText9LeftRTopWidth� HeightSize.Values      ��@UUUUUU��@UUUUUUU�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetTodosPortadores	DataFieldDEVEDORFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText10LeftTopWidth� HeightSize.Values      ��@      ��@UUUUUUU�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetTodosPortadores	DataFieldTipoDocumentoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText12Left�TopWidthDHeightSize.Values      ��@UUUUUU��	@UUUUUUU�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetTodosPortadores	DataFieldVALOR_TITULOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText13Left�TopWidth.HeightSize.Values      ��@������K�	@UUUUUUU�@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetTodosPortadores	DataFieldFUNPERJFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText14LeftOTopWidth3HeightSize.Values      ��@UUUUUU�
@UUUUUUU�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetTodosPortadores	DataFieldACOTERJFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText15Left�TopWidth7HeightSize.Values      ��@UUUUUU}�
@UUUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetTodosPortadores	DataFieldTOTALFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText17Left$TopWidth'HeightSize.Values      ��@      �
@UUUUUUU�@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetTodosPortadores	DataFieldMUTUAFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText18LeftVTopWidth5HeightSize.Values      ��@��������	@UUUUUUU�@������:�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetTodosPortadores	DataFieldFUNDPERJFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText19LeftTopWidth3HeightSize.Values      ��@      ��	@UUUUUUU�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetTodosPortadores	DataFieldFETJFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText20Left�TopWidth/HeightSize.Values      ��@      ��	@UUUUUUU�@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetTodosPortadores	DataFieldEMOLUMENTOSFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText11Left�TopWidthPHeightSize.Values      ��@�������
@UUUUUUU�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhite	DataFieldSTATUSFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText21Left�TopWidth'HeightSize.Values      ��@�������	@UUUUUUU�@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetTodosPortadores	DataFieldPMCMVFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText22Left�TopWidth9HeightSize.Values      ��@��������	@UUUUUUU�@      Ж@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetTodosPortadores	DataFieldFUNARPENFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize   TQRBandFooterLeft&Top� WidthHeightFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomBeforePrintFooterBeforePrintTransparentBandForceNewColumnForceNewPageLinkBandDetalheSize.Values������j�@      #�
@ PreCaluculateBandHeightKeepOnOnePageBandTyperbGroupFooter TQRExprQRExpr3LeftOTopWidth3HeightSize.Values      ��@UUUUUU�
@��������@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.ACOTERJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr4Left�TopWidth7HeightSize.Values      ��@UUUUUU}�
@��������@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.TOTAL)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr5Left$TopWidth'HeightSize.Values      ��@      �
@��������@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.MUTUA)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr6Left�TopWidth.HeightSize.Values      ��@������K�	@��������@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.FUNPERJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr7LeftVTopWidth5HeightSize.Values      ��@��������	@��������@������:�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.FUNDPERJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr8LeftTopWidth3HeightSize.Values      ��@      ��	@��������@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.FETJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr9Left�TopWidth/HeightSize.Values      ��@      ��	@��������@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
Expression Sum(TodosPortadores.EMOLUMENTOS)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr10Left�TopWidthDHeightSize.Values      ��@UUUUUU��	@��������@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
Expression!Sum(TodosPortadores.VALOR_TITULO)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRLabellbQtdLeftTopWidth?HeightSize.Values      ��@������*�@��������@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption
QuantidadeColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRExprQRExpr19Left�TopWidth9HeightSize.Values      ��@��������	@��������@      Ж@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.FUNARPEN)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr20Left�TopWidth'HeightSize.Values      ��@�������	@��������@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.PMCMV)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize   TQRGroupGrupoLeft&Top� WidthHeight'Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values      `�@      #�
@ PreCaluculateBandHeightKeepOnOnePage
ExpressionTodosPortadores.Apresentante
FooterBandFooterMasterTodosReprintOnNewPage TQRLabel	QRLabel12LeftTopWidth.HeightSize.Values      ��@������*�@������j�@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	ProtocoloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel13LeftRTopWidth*HeightSize.Values      ��@UUUUUU��@������j�@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionDevedorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText16LeftTopWidthhHeightSize.Values�������@������*�@       �@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhite	DataFieldAPRESENTANTEFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize
  TQRLabel	QRLabel11Left�TopWidth/HeightSize.Values      ��@      ��	@������j�@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionEmol.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel14LeftTopWidth3HeightSize.Values      ��@      ��	@������j�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionFETJ ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel15LeftVTopWidth5HeightSize.Values      ��@��������	@������j�@������:�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionFUNDPERJColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel16Left�TopWidth.HeightSize.Values      ��@������K�	@������j�@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionFUNPERJColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel17Left$TopWidth'HeightSize.Values      ��@      �
@������j�@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption   MÚTUAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel18LeftOTopWidth3HeightSize.Values      ��@UUUUUU�
@������j�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionACOTERJColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel19Left�TopWidth$HeightSize.Values      ��@��������
@������j�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionTOTALColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel20LeftTopWidthQHeightSize.Values      ��@      ��@������j�@      P�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionTipo DocumentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel21Left�TopWidthDHeightSize.Values      ��@UUUUUU��	@������j�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption
Valor Doc.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel22Left�TopWidth-HeightSize.Values      ��@�������
@������j�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption
   SituaçãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel9Left�TopWidth'HeightSize.Values      ��@�������	@������j�@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionPMCMVColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel10Left�TopWidth9HeightSize.Values      ��@��������	@������j�@      Ж@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionFUNARPENColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   TQRBandQRBand5Left&Top� WidthHeight*Frame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomBeforePrintQRBand5BeforePrintColor��� TransparentBandForceNewColumnForceNewPageSize.Values     @�@      #�
@ PreCaluculateBandHeightKeepOnOnePageBandType	rbSummary TQRExprQRExpr11Left�TopWidthDHeightSize.Values      ��@UUUUUU��	@      ��@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrintTransparent	
Expression!Sum(TodosPortadores.VALOR_TITULO)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr12LeftOTopWidth3HeightSize.Values      ��@UUUUUU�
@      ��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.ACOTERJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr13Left�TopWidth7HeightSize.Values      ��@UUUUUU}�
@      ��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.TOTAL)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr14Left$TopWidth'HeightSize.Values      ��@      �
@      ��@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.MUTUA)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr15Left�TopWidth.HeightSize.Values      ��@������K�	@      ��@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.FUNPERJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr16LeftVTopWidth5HeightSize.Values      ��@��������	@      ��@������:�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.FUNDPERJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr17LeftTopWidth3HeightSize.Values      ��@      ��	@      ��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.FETJ)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr18Left�TopWidth/HeightSize.Values      ��@      ��	@      ��@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
Expression Sum(TodosPortadores.EMOLUMENTOS)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRLabellbQgrLeftTopWidth?HeightSize.Values      ��@������*�@      ��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption
QuantidadeColor��� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRExprQRExpr21Left�TopWidth9HeightSize.Values      ��@��������	@      ��@      Ж@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.FUNARPEN)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRExprQRExpr22Left�TopWidth'HeightSize.Values      ��@�������	@      ��@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Color��� 
ParentFontResetAfterPrint	Transparent
ExpressionSum(TodosPortadores.PMCMV)Mask	#####0.00ExportAsexptText	WrapStyleBreakOnSpacesFontSize    TFDQueryTitulosOnCalcFieldsTitulosCalcFields
Connectiondm.conSISTEMASQL.Stringscselect PROTOCOLO, DT_PROTOCOLO, TOTAL, NUMERO_TITULO, VALOR_TITULO, NOSSO_NUMERO, SACADOR, DEVEDOR,ZSTATUS from TITULOS where CODIGO_APRESENTANTE=:CODIGO and DT_PROTOCOLO between :D1 and :D2 Left`Top 	ParamDataNameCODIGODataTypeftString	ParamTypeptInputSize
 NameD1DataTypeftDate	ParamTypeptInput NameD2DataTypeftDate	ParamTypeptInput   TIntegerFieldTitulosPROTOCOLO	FieldName	PROTOCOLOOrigin	PROTOCOLO  
TDateFieldTitulosDT_PROTOCOLO	FieldNameDT_PROTOCOLOOriginDT_PROTOCOLO  TFloatFieldTitulosTOTAL	FieldNameTOTALOriginTOTAL  TStringFieldTitulosNUMERO_TITULO	FieldNameNUMERO_TITULOOriginNUMERO_TITULO  TFloatFieldTitulosVALOR_TITULO	FieldNameVALOR_TITULOOriginVALOR_TITULO  TStringFieldTitulosNOSSO_NUMERO	FieldNameNOSSO_NUMEROOriginNOSSO_NUMEROSize  TStringFieldTitulosSACADOR	FieldNameSACADOROriginSACADORSized  TStringFieldTitulosDEVEDOR	FieldNameDEVEDOROriginDEVEDORSized  TStringFieldTitulosSTATUS	FieldNameSTATUSOriginSTATUS  TStringFieldTitulosDataProtocolo	FieldKindfkInternalCalc	FieldNameDataProtocoloSize   TFDQueryTodosPortadoresOnCalcFieldsTodosPortadoresCalcFields
Connectiondm.conSISTEMASQL.Stringsselect     APRESENTANTE,
PROTOCOLO,DEVEDOR,EMOLUMENTOS,FETJ,	FUNDPERJ,FUNPERJ,	FUNARPEN,PMCMV,MUTUA,ACOTERJ,TOTAL,TIPO_TITULO,VALOR_TITULO,STATUS,CONVENIO    3from TITULOS where DT_PROTOCOLO between :D1 and :D2    AND EXPORTADO='N'    order by PROTOCOLO Left� Top 	ParamDataNameD1DataTypeftDate	ParamTypeptInput NameD2DataTypeftDate	ParamTypeptInput   TStringFieldTodosPortadoresAPRESENTANTE	FieldNameAPRESENTANTEOriginAPRESENTANTESized  TIntegerFieldTodosPortadoresPROTOCOLO	FieldName	PROTOCOLOOrigin	PROTOCOLO  TStringFieldTodosPortadoresDEVEDOR	FieldNameDEVEDOROriginDEVEDORSized  TFloatFieldTodosPortadoresEMOLUMENTOS	FieldNameEMOLUMENTOSOriginEMOLUMENTOS  TFloatFieldTodosPortadoresFETJ	FieldNameFETJOriginFETJ  TFloatFieldTodosPortadoresFUNDPERJ	FieldNameFUNDPERJOriginFUNDPERJ  TFloatFieldTodosPortadoresFUNPERJ	FieldNameFUNPERJOriginFUNPERJ  TFloatFieldTodosPortadoresFUNARPEN	FieldNameFUNARPENOriginFUNARPEN  TFloatFieldTodosPortadoresPMCMV	FieldNamePMCMVOriginPMCMV  TFloatFieldTodosPortadoresMUTUA	FieldNameMUTUAOriginMUTUA  TFloatFieldTodosPortadoresACOTERJ	FieldNameACOTERJOriginACOTERJ  TFloatFieldTodosPortadoresTOTAL	FieldNameTOTALOriginTOTAL  TIntegerFieldTodosPortadoresTIPO_TITULO	FieldNameTIPO_TITULOOriginTIPO_TITULO  TFloatFieldTodosPortadoresVALOR_TITULO	FieldNameVALOR_TITULOOriginVALOR_TITULO  TStringFieldTodosPortadoresSTATUS	FieldNameSTATUSOriginSTATUS  TStringFieldTodosPortadoresCONVENIO	FieldNameCONVENIOOriginCONVENIO	FixedChar	Size  TStringFieldTodosPortadoresTipoDocumento	FieldKindfkInternalCalc	FieldNameTipoDocumentoSize(    