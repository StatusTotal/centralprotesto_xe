unit UQuickPagamento1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, DB, DBClient, SimpleDS,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFQuickPagamento1 = class(TForm)
    Certidao: TQuickRep;
    Detail: TQRBand;
    lbCidade: TQRLabel;
    lbTitular: TQRLabel;
    lbCartorio: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    lbDevedor: TQRLabel;
    lbDocumento: TQRLabel;
    lbValorPago: TQRLabel;
    lbForma: TQRLabel;
    QRLabel5: TQRLabel;
    lbEspecie: TQRLabel;
    lbNumero: TQRLabel;
    lbPortador: TQRLabel;
    lbSacador: TQRLabel;
    lbCedente: TQRLabel;
    lbAviso: TQRLabel;
    lbData: TQRLabel;
    sh1: TQRShape;
    lbNome1: TQRLabel;
    lbInfo1: TQRLabel;
    QRLabel3: TQRLabel;
    M: TQRMemo;
    QRLabel1: TQRLabel;
    lbProtocolo: TQRLabel;
    lbEndereco: TQRLabel;
    lbDados: TQRLabel;
    lbCNPJ: TQRLabel;
    qrPagamento: TQRLabel;
    lbPortador2: TQRLabel;
    qrDeclaro: TQRLabel;
    lbDevedor2: TQRLabel;
    lbDocumento2: TQRLabel;
    lbEspecie2: TQRLabel;
    lbNumero2: TQRLabel;
    lbValor: TQRLabel;
    lbSacador2: TQRLabel;
    lbCedente2: TQRLabel;
    lbForma2: TQRLabel;
    lbProtocolo2: TQRLabel;
    lbSelo2: TQRLabel;
    lbData2: TQRLabel;
    QRShape3: TQRShape;
    QRShape2: TQRShape;
    lbLinha: TQRLabel;
    lbValorTitulo: TQRLabel;
    lbCGJ1: TQRLabel;
    lbCGJ2: TQRLabel;
    lbCGJ3: TQRLabel;
    lbSeloEletronico: TQRLabel;
    lbCGJ4: TQRLabel;
    lbCGJ5: TQRLabel;
    lb2Via: TQRLabel;
    lbSaldoTitulo: TQRLabel;
    qrValorTitulo: TQRLabel;
    Certidao2: TQuickRep;
    Detail2: TQRBand;
    qrPagamento2: TQRLabel;
    lbPortador3: TQRLabel;
    qrDeclaro2: TQRLabel;
    lbDevedor3: TQRLabel;
    lbDocumento3: TQRLabel;
    lbEspecie3: TQRLabel;
    lbNumero3: TQRLabel;
    lbValor2: TQRLabel;
    lbSacador3: TQRLabel;
    lbCedente3: TQRLabel;
    lbForma3: TQRLabel;
    lbProtocolo3: TQRLabel;
    lbSelo3: TQRLabel;
    lbData3: TQRLabel;
    QRShape1: TQRShape;
    qrValortitulo2: TQRLabel;
    lbDataTitulo: TQRLabel;
    lbSelo: TQRLabel;
    qryImprime: TFDQuery;
    qryImprimeID_ATO: TIntegerField;
    qryImprimeCODIGO: TIntegerField;
    qryImprimeRECIBO: TIntegerField;
    qryImprimeDT_ENTRADA: TDateField;
    qryImprimeDT_PROTOCOLO: TDateField;
    qryImprimePROTOCOLO: TIntegerField;
    qryImprimeLIVRO_PROTOCOLO: TIntegerField;
    qryImprimeFOLHA_PROTOCOLO: TStringField;
    qryImprimeDT_PRAZO: TDateField;
    qryImprimeDT_REGISTRO: TDateField;
    qryImprimeREGISTRO: TIntegerField;
    qryImprimeLIVRO_REGISTRO: TIntegerField;
    qryImprimeFOLHA_REGISTRO: TStringField;
    qryImprimeSELO_REGISTRO: TStringField;
    qryImprimeDT_PAGAMENTO: TDateField;
    qryImprimeSELO_PAGAMENTO: TStringField;
    qryImprimeRECIBO_PAGAMENTO: TIntegerField;
    qryImprimeCOBRANCA: TStringField;
    qryImprimeEMOLUMENTOS: TFloatField;
    qryImprimeFETJ: TFloatField;
    qryImprimeFUNDPERJ: TFloatField;
    qryImprimeFUNPERJ: TFloatField;
    qryImprimeFUNARPEN: TFloatField;
    qryImprimePMCMV: TFloatField;
    qryImprimeISS: TFloatField;
    qryImprimeMUTUA: TFloatField;
    qryImprimeDISTRIBUICAO: TFloatField;
    qryImprimeACOTERJ: TFloatField;
    qryImprimeTOTAL: TFloatField;
    qryImprimeTIPO_PROTESTO: TIntegerField;
    qryImprimeTIPO_TITULO: TIntegerField;
    qryImprimeNUMERO_TITULO: TStringField;
    qryImprimeDT_TITULO: TDateField;
    qryImprimeBANCO: TStringField;
    qryImprimeAGENCIA: TStringField;
    qryImprimeCONTA: TStringField;
    qryImprimeVALOR_TITULO: TFloatField;
    qryImprimeSALDO_PROTESTO: TFloatField;
    qryImprimeDT_VENCIMENTO: TDateField;
    qryImprimeCONVENIO: TStringField;
    qryImprimeTIPO_APRESENTACAO: TStringField;
    qryImprimeDT_ENVIO: TDateField;
    qryImprimeCODIGO_APRESENTANTE: TStringField;
    qryImprimeTIPO_INTIMACAO: TStringField;
    qryImprimeMOTIVO_INTIMACAO: TMemoField;
    qryImprimeDT_INTIMACAO: TDateField;
    qryImprimeDT_PUBLICACAO: TDateField;
    qryImprimeVALOR_PAGAMENTO: TFloatField;
    qryImprimeAPRESENTANTE: TStringField;
    qryImprimeCPF_CNPJ_APRESENTANTE: TStringField;
    qryImprimeTIPO_APRESENTANTE: TStringField;
    qryImprimeCEDENTE: TStringField;
    qryImprimeNOSSO_NUMERO: TStringField;
    qryImprimeSACADOR: TStringField;
    qryImprimeDEVEDOR: TStringField;
    qryImprimeCPF_CNPJ_DEVEDOR: TStringField;
    qryImprimeTIPO_DEVEDOR: TStringField;
    qryImprimeAGENCIA_CEDENTE: TStringField;
    qryImprimePRACA_PROTESTO: TStringField;
    qryImprimeTIPO_ENDOSSO: TStringField;
    qryImprimeACEITE: TStringField;
    qryImprimeCPF_ESCREVENTE: TStringField;
    qryImprimeCPF_ESCREVENTE_PG: TStringField;
    qryImprimeOBSERVACAO: TMemoField;
    qryImprimeDT_SUSTADO: TDateField;
    qryImprimeDT_RETIRADO: TDateField;
    qryImprimeSTATUS: TStringField;
    qryImprimePROTESTADO: TStringField;
    qryImprimeENVIADO_APONTAMENTO: TStringField;
    qryImprimeENVIADO_PROTESTO: TStringField;
    qryImprimeENVIADO_PAGAMENTO: TStringField;
    qryImprimeENVIADO_RETIRADO: TStringField;
    qryImprimeENVIADO_SUSTADO: TStringField;
    qryImprimeENVIADO_DEVOLVIDO: TStringField;
    qryImprimeEXPORTADO: TStringField;
    qryImprimeAVALISTA_DEVEDOR: TStringField;
    qryImprimeFINS_FALIMENTARES: TStringField;
    qryImprimeTARIFA_BANCARIA: TFloatField;
    qryImprimeFORMA_PAGAMENTO: TStringField;
    qryImprimeNUMERO_PAGAMENTO: TStringField;
    qryImprimeARQUIVO: TStringField;
    qryImprimeRETORNO: TStringField;
    qryImprimeDT_DEVOLVIDO: TDateField;
    qryImprimeCPF_CNPJ_SACADOR: TStringField;
    qryImprimeID_MSG: TIntegerField;
    qryImprimeVALOR_AR: TFloatField;
    qryImprimeELETRONICO: TStringField;
    qryImprimeIRREGULARIDADE: TIntegerField;
    qryImprimeAVISTA: TStringField;
    qryImprimeSALDO_TITULO: TFloatField;
    qryImprimeTIPO_SUSTACAO: TStringField;
    qryImprimeRETORNO_PROTESTO: TStringField;
    qryImprimeDT_RETORNO_PROTESTO: TDateField;
    qryImprimeDT_DEFINITIVA: TDateField;
    qryImprimeJUDICIAL: TStringField;
    qryImprimeALEATORIO_PROTESTO: TStringField;
    qryImprimeALEATORIO_SOLUCAO: TStringField;
    qryImprimeCCT: TStringField;
    qryImprimeDETERMINACAO: TStringField;
    qryImprimeANTIGO: TStringField;
    qryImprimeLETRA: TStringField;
    qryImprimePROTOCOLO_CARTORIO: TIntegerField;
    qryImprimeARQUIVO_CARTORIO: TStringField;
    qryImprimeRETORNO_CARTORIO: TStringField;
    qryImprimeDT_DEVOLVIDO_CARTORIO: TDateField;
    qryImprimeCARTORIO: TIntegerField;
    qryImprimeDT_PROTOCOLO_CARTORIO: TDateField;
    procedure DetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickPagamento1: TFQuickPagamento1;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFQuickPagamento1.DetailBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbCartorio.Caption    :=dm.ServentiaDESCRICAO.AsString;
  lbCidade.Caption      :=dm.ServentiaCIDADE.AsString+' - Rio de Janeiro';
  lbTitular.Caption     :=dm.ServentiaTABELIAO.AsString+' - '+dm.ServentiaTITULO.AsString;
  lbEndereco.Caption    :=dm.ServentiaENDERECO.AsString;
  lbDados.Caption       :='Telefone: '+dm.ServentiaTELEFONE.AsString+' / E-mail: '+dm.ServentiaEMAIL.AsString;
  lbCNPJ.Caption        :='CNPJ: '+dm.ServentiaCNPJ.AsString;
  lbDevedor.Caption     :=qryImprimeDEVEDOR.AsString;

  if qryImprimeTIPO_DEVEDOR.AsString='F' then
    lbDocumento.Caption:='CPF: '+PF.FormatarCPF(qryImprimeCPF_CNPJ_DEVEDOR.AsString)
      else lbDocumento.Caption:='CNPJ: '+PF.FormatarCNPJ(qryImprimeCPF_CNPJ_DEVEDOR.AsString);

  lbValorTitulo.Caption :='Valor do T�tulo: R$ '+FloatToStrF(qryImprimeVALOR_TITULO.AsFloat,ffNumber,7,2);
  lbSaldoTitulo.Caption :='Saldo do T�tulo: R$ '+FloatToStrF(qryImprimeSALDO_TITULO.AsFloat,ffNumber,7,2);
  lbValorPago.Caption   :='A import�ncia de: R$ '+FloatToStrF(qryImprimeVALOR_PAGAMENTO.AsFloat,ffNumber,7,2);

  if (qryImprimeFORMA_PAGAMENTO.AsString='DH') or (qryImprimeFORMA_PAGAMENTO.AsString='') then
  begin
      lbForma.Caption:='Atrav�s de DINHEIRO em '+PF.FormatarData(qryImprimeDT_PAGAMENTO.AsDateTime,'N')+'. RECIBO N�: '+dm.CertidoesRECIBO.AsString;
      lbAviso.Caption:='Ficando assim o devedor quites com este recibo.';
  end;

  if qryImprimeFORMA_PAGAMENTO.AsString='CH' then
  begin
      if qryImprimeNUMERO_PAGAMENTO.AsString='' then
        lbForma.Caption:='Atrav�s de CHEQUE em '+PF.FormatarData(qryImprimeDT_PAGAMENTO.AsDateTime,'N')
          else lbForma.Caption:='Atrav�s de CHEQUE n� '+qryImprimeNUMERO_PAGAMENTO.AsString+' em '+PF.FormatarData(qryImprimeDT_PAGAMENTO.AsDateTime,'N');
      lbAviso.Caption:='Ficando assim o devedor quites com este recibo.';
  end;

  if qryImprimeFORMA_PAGAMENTO.AsString='BL' then
    if qryImprimeNUMERO_PAGAMENTO.AsString='' then
      lbForma.Caption:='Atrav�s de BOLETO em '+PF.FormatarData(qryImprimeDT_PAGAMENTO.AsDateTime,'N')
        else lbForma.Caption:='Atrav�s de BOLETO n� '+qryImprimeNUMERO_PAGAMENTO.AsString+' em '+PF.FormatarData(qryImprimeDT_PAGAMENTO.AsDateTime,'N');

  if qryImprimeFORMA_PAGAMENTO.AsString='DP' then
  begin
      lbForma.Caption:='Atrav�s de DEP�SITO em '+PF.FormatarData(qryImprimeDT_PAGAMENTO.AsDateTime,'N');
      lbAviso.Caption:='Ficando assim o devedor quites com este recibo.';
  end;

  lbEspecie.Caption         :='Ep�cie do T�tulo: '+PF.RetornarTitulo(qryImprimeTIPO_TITULO.AsInteger);
  lbNumero.Caption          :='N� do T�tulo: '+qryImprimeNUMERO_TITULO.AsString;
  lbDataTitulo.Caption      :='Data do T�tulo: '+FormatDateTime('dd/mm/yyyy',qryImprimeDT_TITULO.AsDateTime);
  lbSelo.Caption            :='Selo de Pagamento: '+qryImprimeSELO_PAGAMENTO.AsString;
  lbPortador.Caption        :='Portador: '+qryImprimeAPRESENTANTE.AsString;
  lbSacador.Caption         :='Sacador: '+qryImprimeSACADOR.AsString;
  lbCedente.Caption         :='Cedente: '+qryImprimeCEDENTE.AsString;
  lbProtocolo.Caption       :='Protocolo: '+qryImprimePROTOCOLO.AsString;

  if dm.v2Via then
  begin
      lbSeloEletronico.Caption:=GR.SeloFormatado(dm.CertidoesSELO.AsString,dm.CertidoesALEATORIO.AsString);
      lbData.Caption:=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',dm.CertidoesDT_CERTIDAO.AsDateTime);
  end
  else
  begin
      lbSeloEletronico.Caption:=GR.SeloFormatado(qryImprimeSELO_PAGAMENTO.AsString,qryImprimeALEATORIO_SOLUCAO.AsString);
      lbData.Caption:=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',qryImprimeDT_PAGAMENTO.AsDateTime);
  end;

  lbPortador2.Caption       :=lbPortador.Caption;
  lbProtocolo2.Caption      :=lbProtocolo.Caption;
  lbDevedor2.Caption        :=lbDevedor.Caption;
  lbDocumento2.Caption      :=lbDocumento.Caption;
  lbEspecie2.Caption        :=lbEspecie.Caption;
  lbNumero2.Caption         :=lbNumero.Caption;
  qrValorTitulo.Caption     :='Valor do T�tulo: '+FloatToStrF(qryImprimeVALOR_TITULO.AsFloat,ffNumber,7,2);

  lbValor.Caption:='Saldo do T�tulo: '+FloatToStrF(qryImprimeSALDO_TITULO.AsFloat,ffNumber,7,2)+' ** '+
                   'Custas: '+FloatToStrF(qryImprimeTOTAL.AsFloat-qryImprimeTARIFA_BANCARIA.AsFloat,ffNumber,7,2)+' ** '+
                   'Tarifa banc�ria: '+FloatToStrF(qryImprimeTARIFA_BANCARIA.AsFloat,ffNumber,7,2)+' ** '+
                   'Total: '+FloatToStrF(qryImprimeSALDO_PROTESTO.AsFloat,ffNumber,7,2)+' ** '+
                   'Valor Pago: '+FloatToStrF(qryImprimeVALOR_PAGAMENTO.AsFloat,ffNumber,7,2);

  lbSacador2.Caption    :=lbSacador.Caption;
  lbCedente2.Caption    :=lbCedente.Caption;
  lbForma2.Caption      :=lbForma.Caption;
  lbSelo2.Caption       :='Selo: '+lbSeloEletronico.Caption;
  lbData2.Caption       :=lbData.Caption;

  M.Lines.Clear;

  if dm.v2Via then
  begin
      PF.CarregarCustas(dm.CertidoesID_CERTIDAO.AsInteger);
      dm.RxCustas.First;
      while not dm.RxCustas.Eof do
      begin
          M.Lines.Add(dm.RxCustasTABELA.AsString+'.'+dm.RxCustasITEM.AsString+'.'+dm.RxCustasSUBITEM.AsString+'   '+dm.RxCustasDESCRICAO.AsString+' x '+dm.RxCustasQTD.AsString+Format('%-1s %7.2f',[' =',dm.RxCustasTOTAL.AsFloat]));
          dm.RxCustas.Next;
      end;
      M.Lines.Add(Format('%-1s %7.2f',['Emolumentos =',dm.CertidoesEMOLUMENTOS.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Fetj =',dm.CertidoesFETJ.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Fundperj =',dm.CertidoesFUNDPERJ.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Funperj =',dm.CertidoesFUNPERJ.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Funarpen =',dm.CertidoesFUNARPEN.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Iss =',dm.CertidoesISS.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Total =',dm.CertidoesTOTAL.AsFloat]));
  end
  else
  begin
      PF.CarregarCustas(qryImprimeID_ATO.AsInteger);
      dm.RxCustas.First;
      while not dm.RxCustas.Eof do
      begin
          M.Lines.Add(dm.RxCustasTABELA.AsString+'.'+dm.RxCustasITEM.AsString+'.'+dm.RxCustasSUBITEM.AsString+'   '+dm.RxCustasDESCRICAO.AsString+' x '+dm.RxCustasQTD.AsString+Format('%-1s %7.2f',[' =',dm.RxCustasTOTAL.AsFloat]));
          dm.RxCustas.Next;
      end;
      M.Lines.Add(Format('%-1s %7.2f',['Emolumentos =',qryImprimeEMOLUMENTOS.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Fetj =',qryImprimeFETJ.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Fundperj =',qryImprimeFUNDPERJ.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Funperj =',qryImprimeFUNPERJ.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Funarpen =',qryImprimeFUNARPEN.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Pmcmv =',qryImprimePMCMV.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Iss =',qryImprimeISS.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['M�tua =',qryImprimeMUTUA.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Acoterj =',qryImprimeACOTERJ.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Distribui��o =',qryImprimeDISTRIBUICAO.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Tarifa Banc�ria =',qryImprimeTARIFA_BANCARIA.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Correio (A.R.) =',qryImprimeVALOR_AR.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Total =',qryImprimeTOTAL.AsFloat]));
  end;

  lbNome1.Caption:=dm.vAssinatura;
  lbInfo1.Caption:=dm.vMatricula;

  if dm.vPergunta then
  begin
      lbPortador3.Enabled       := True;
      lbProtocolo3.Enabled      := True;
      lbDevedor3.Enabled        := True;
      lbDocumento3.Enabled      := True;
      lbEspecie3.Enabled        := True;
      lbNumero3.Enabled         := True;
      qrValortitulo2.Enabled    := True;
      lbValor2.Enabled          := True;
      lbSacador3.Enabled        := True;
      lbCedente3.Enabled        := True;
      lbForma3.Enabled          := True;
      lbSelo3.Enabled           := True;
      lbData3.Enabled           := True;
      lbPortador3.Caption       :=lbPortador.Caption;
      lbProtocolo3.Caption      :=lbProtocolo.Caption;
      lbDevedor3.Caption        :=lbDevedor.Caption;
      lbDocumento3.Caption      :=lbDocumento.Caption;
      lbEspecie3.Caption        :=lbEspecie.Caption;
      lbNumero3.Caption         :=lbNumero.Caption;
      qrValortitulo2.Caption    :=qrValorTitulo.Caption;
      lbValor2.Caption          :=lbValor.Caption;
      lbSacador3.Caption        :=lbSacador.Caption;
      lbCedente3.Caption        :=lbCedente.Caption;
      lbForma3.Caption          :=lbForma.Caption;
      lbSelo3.Caption           :=lbSelo.Caption;
      lbData3.Caption           :=lbData.Caption;
  end;
end;

procedure TFQuickPagamento1.FormCreate(Sender: TObject);
begin
  lb2Via.Enabled:=dm.v2Via;
  {PEDRO - 31/03/2016}
  if GR.Pergunta('DESEJA IMPRIMIR NA FOLHA DE SEGURAN�A') then
  begin
      if GR.Pergunta('DESEJA IMPRIMIR O COMPROVANTE DA CERTID�O DE PAGAMENTO') then
         dm.vPergunta := true
         else dm.vPergunta := False;

      qrPagamento.Enabled       :=False;
      lbPortador2.Enabled       :=False;
      lbProtocolo2.Enabled      :=False;
      qrDeclaro.Enabled         :=False;
      lbDevedor2.Enabled        :=False;
      lbDocumento2.Enabled      :=False;
      lbEspecie2.Enabled        :=False;
      lbNumero2.Enabled         :=False;
      qrValorTitulo.Enabled     :=False;
      lbValor.Enabled           :=False;
      lbSacador2.Enabled        :=False;
      lbCedente2.Enabled        :=False;
      lbForma2.Enabled          :=False;
      lbSelo2.Enabled           :=False;
      lbData2.Enabled           :=False;
      QRShape3.Enabled          :=False;
      lbLinha.Enabled           :=False;

      if dm.vFlTiracabecalho = 'S' then
      begin
         lbCartorio.Enabled   :=False;
         lbCidade.Enabled     :=False;
         lbTitular.Enabled    :=False;
         lbEndereco.Enabled   :=False;
         lbDados.Enabled      :=False;
         lbCNPJ.Enabled       :=False;
      end
      else
      begin
         lbCartorio.Enabled   :=True;
         lbCidade.Enabled     :=True;
         lbTitular.Enabled    :=True;
         lbEndereco.Enabled   :=True;
         lbDados.Enabled      :=True;
         lbCNPJ.Enabled       :=True;
      end;

      Certidao.Page.BottomMargin := StrToFloat(dm.vFlBottom);
      Certidao.Page.TopMargin    := StrToFloat(dm.vFlTop);
      Certidao.Page.LeftMargin   := StrToFloat(dm.vFlLeft);
      Certidao.Page.RightMargin  := StrToFloat(dm.vFlRight);

      if dm.vFlFolha ='A4' then
      begin
        Certidao.Page.Width := 210.0;
        Certidao.Page.Length:= 297.0;
      end;

      if dm.vFlFolha ='Legal' then
      begin
        Certidao.Page.Width := 215.9;
        Certidao.Page.Length:= 355.6;
      end;
  end;


end;

end.
