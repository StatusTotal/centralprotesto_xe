object FCertidao: TFCertidao
  Left = 294
  Top = 155
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Certid'#227'o'
  ClientHeight = 558
  ClientWidth = 775
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 14
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 775
    Height = 277
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'STATUSBAR'
    DesignSize = (
      775
      277)
    object Bv: TBevel
      Left = 125
      Top = 55
      Width = 643
      Height = 48
    end
    object lbInformacoes: TsLabel
      Left = 130
      Top = 60
      Width = 632
      Height = 38
      Alignment = taCenter
      AutoSize = False
      ParentFont = False
      Layout = tlCenter
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object Grid: TwwDBGrid
      Left = 12
      Top = 125
      Width = 756
      Height = 142
      Selected.Strings = (
        'DT_PROTOCOLO'#9'10'#9'Data'#9'F'
        'PROTOCOLO'#9'8'#9'Protocolo'#9'F'
        'LIVRO_PROTOCOLO'#9'5'#9'Livro'#9'F'
        'FOLHA_PROTOCOLO'#9'6'#9'Folha'#9'F'
        'DT_REGISTRO'#9'10'#9'Protesto'#9'F'
        'APRESENTANTE'#9'35'#9'Apresentante'#9'F'
        'STATUS'#9'19'#9'Status'#9'F'
        'VALOR_TITULO'#9'10'#9'Valor'#9'F'
        'Total'#9'9'#9'Custas'#9'F')
      IniAttributes.Delimiter = ';;'
      IniAttributes.UnicodeIniFile = False
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Anchors = [akLeft, akTop, akBottom]
      DataSource = dsConsulta
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = []
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      TitleLines = 1
      TitleButtons = False
      UseTFields = False
    end
    object btBusca: TsBitBtn
      Left = 12
      Top = 55
      Width = 97
      Height = 48
      Caption = 'Efetuar busca!'
      Layout = blGlyphTop
      TabOrder = 0
      OnClick = btBuscaClick
      ImageIndex = 4
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object P2: TsPanel
      Left = 1
      Top = 1
      Width = 773
      Height = 41
      Align = alTop
      TabOrder = 2
      SkinData.SkinSection = 'HINT'
      object txDT_PEDIDO: TsDBText
        Left = 116
        Top = 16
        Width = 77
        Height = 14
        Caption = 'txDT_PEDIDO'
        ParentFont = False
        ShowAccelChar = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 5059883
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        DataField = 'DT_PEDIDO'
        DataSource = dsCertidao
      end
      object sLabel1: TsLabel
        Left = 12
        Top = 16
        Width = 99
        Height = 14
        Caption = 'Data do Pedido:'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 5059883
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
      end
      object sLabel2: TsLabel
        Left = 271
        Top = 16
        Width = 51
        Height = 14
        Caption = 'Per'#237'odo:'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 5059883
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
      end
      object txANOS: TsDBText
        Left = 327
        Top = 16
        Width = 43
        Height = 14
        Caption = 'txANOS'
        ParentFont = False
        ShowAccelChar = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 5059883
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        DataField = 'Periodo'
        DataSource = dsCertidao
      end
      object sLabel3: TsLabel
        Left = 457
        Top = 16
        Width = 105
        Height = 14
        Caption = 'Tipo de Certid'#227'o:'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 5059883
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
      end
      object txNOME_CERTIDAO: TsDBText
        Left = 568
        Top = 16
        Width = 109
        Height = 14
        Caption = 'txNOME_CERTIDAO'
        ParentFont = False
        ShowAccelChar = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 5059883
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        DataField = 'NomeCertidao'
        DataSource = dsCertidao
      end
    end
  end
  object btSalvar: TsBitBtn
    Left = 596
    Top = 519
    Width = 80
    Height = 28
    Cursor = crHandPoint
    Caption = 'Salvar'
    TabOrder = 2
    OnClick = btSalvarClick
    ImageIndex = 0
    Images = Gdm.Im16
    SkinData.SkinSection = 'BUTTON'
  end
  object btCancelar: TsBitBtn
    Left = 682
    Top = 519
    Width = 88
    Height = 28
    Cursor = crHandPoint
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = btCancelarClick
    ImageIndex = 1
    Images = Gdm.Im16
    SkinData.SkinSection = 'BUTTON'
  end
  object ckConvenio: TsDBCheckBox
    Left = 948
    Top = 249
    Width = 82
    Height = 18
    Caption = 'Conv'#234'nio'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Visible = False
    ImgChecked = 0
    ImgUnchecked = 0
    SkinData.SkinSection = 'CHECKBOX'
    DataField = 'CONVENIO'
    DataSource = dsCertidao
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object GbCertidao: TsGroupBox
    Left = 12
    Top = 294
    Width = 758
    Height = 213
    Caption = 'C E R T I D '#195' O'
    TabOrder = 1
    CaptionLayout = clTopCenter
    SkinData.SkinSection = 'TOOLBAR'
    CaptionSkin = 'EDIT'
    object edFolhas: TsDBEdit
      Left = 354
      Top = 37
      Width = 37
      Height = 22
      Hint = 'N'#218'MERO DE FOLHAS'
      Color = clWhite
      DataField = 'FOLHAS'
      DataSource = dsCertidao
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnChange = edFolhasChange
      OnExit = edFolhasExit
      OnKeyDown = edFolhasKeyDown
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Folhas'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edDataCertidao: TsDBDateEdit
      Left = 7
      Top = 37
      Width = 97
      Height = 22
      Hint = 'DATA DA CERTID'#195'O'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data da Certid'#227'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DA CERTID'#195'O'
      GlyphMode.Blend = 0
      DataField = 'DT_CERTIDAO'
      DataSource = dsCertidao
    end
    object edSelo: TsDBEdit
      Left = 215
      Top = 37
      Width = 86
      Height = 22
      Hint = 'SELO UTILIZADO NA CERTID'#195'O'
      CharCase = ecUpperCase
      Color = 8454143
      DataField = 'SELO'
      DataSource = dsCertidao
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Selo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object lkEscrevente: TsDBLookupComboBox
      Left = 398
      Top = 37
      Width = 174
      Height = 22
      Hint = 'ESCREVENTE QUE REALIZOU A BUSCA'
      Color = clWhite
      DataField = 'ESCREVENTE'
      DataSource = dsCertidao
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyField = 'CPF'
      ListField = 'LOGIN'
      ListSource = dsEscreventes
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Escrevente'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
    end
    object MObservacao: TsDBMemo
      Left = 7
      Top = 78
      Width = 742
      Height = 50
      Color = clWhite
      DataField = 'OBSERVACAO'
      DataSource = dsCertidao
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 7
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Observa'#231#227'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
    end
    object lkResponsavel: TsDBLookupComboBox
      Left = 579
      Top = 37
      Width = 170
      Height = 22
      Hint = 'RESPONS'#193'VEL QUE ASSINA NA CERTID'#195'O'
      Color = clWhite
      DataField = 'RESPONSAVEL'
      DataSource = dsCertidao
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyField = 'CPF'
      ListField = 'LOGIN'
      ListSource = dsResponsavel
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Respons'#225'vel'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
    end
    object edEntrega: TsDBDateEdit
      Left = 111
      Top = 37
      Width = 97
      Height = 22
      Hint = 'DATA DE ENTREGA'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data da Entrega'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DE ENTREGA'
      GlyphMode.Blend = 0
      DataField = 'DT_ENTREGA'
      DataSource = dsCertidao
    end
    object edAleatorio: TsDBEdit
      Left = 307
      Top = 37
      Width = 41
      Height = 22
      Hint = 'ALEAT'#211'RIO'
      Color = 16777088
      DataField = 'ALEATORIO'
      DataSource = dsCertidao
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Layout = sclTopLeft
    end
    object GBExcedentes: TsGroupBox
      Left = 7
      Top = 134
      Width = 742
      Height = 71
      Caption = 'Folhas Excedentes'
      TabOrder = 8
      SkinData.SkinSection = 'GROUPBOX'
      object ImLimparCCT: TsImage
        Left = 715
        Top = 20
        Width = 16
        Height = 16
        Cursor = crHandPoint
        Hint = 'LIMPAR CCT'
        AutoSize = True
        Center = True
        ParentShowHint = False
        Picture.Data = {
          0B54504E474772617068696336040000424D3604000000000000360000002800
          0000100000001000000001002000000000000004000000000000000000000000
          000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF001D5FDE4E356BD9CA1F61
          E032FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001D5FDE4E5E81D2FDADA3B2FF4B73
          CFF61F61E032FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF001D5FDE4E688CDCFDCAC2C9FFBEB4BDFFB0A4
          B2FF577DD3F7ECF1FBCAF1F5FBCFFFFFFF24FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF001D5FDE4E7297E5FDE3DFE3FFD7D0D6FFCAC2C9FFC3B9
          C2FFEEEBF0FF7397DDFF517DD2FFF0F2F9E7FFFFFF24FFFFFF00FFFFFF00FFFF
          FF00FFFFFF001D5FDE4E779EEBFDFCFBFCFFF0EDEFFFE3DFE3FFDAD3D9FFF3F1
          F4FF789FE3FF2964CFFF255DC9FF517DD2FFF0F2F9E7FFFFFF24FFFFFF00FFFF
          FF00FFFFFF003F78E3CAF7FAFEFFFFFFFFFFFCFBFCFFF1EEF0FFF7F7F9FF7DA6
          E9FF316FD8FF265EC8FF265EC9FF255ECAFF517DD2FFF0F2F9E7FFFFFF24FFFF
          FF00FFFFFF002666E1346490E9F6FAFCFEFFFFFFFFFFFCFDFEFF83ADEFFF397B
          E1FF2963CCFF2962CCFF2962CCFF245CC7FF255ECAFF517DD2FFF0F2F9E7FFFF
          FF24FFFFFF00FFFFFF001F61E0326D97EAF7FCFDFFFF88B5F5FF4286EBFF2D67
          D0FF2C67D0FF2E6AD3FF265EC7FF2A65CFFF245CC6FF255ECAFF517DD2FFF0F2
          F9E7FFFFFF24FFFFFF00FFFFFF00EDF2FCCA8DBCFBFF4A92F4FF306CD3FF306C
          D4FF3371D8FF2861CAFF306ED6FF245CC6FF2A65CFFF245CC6FF255ECAFF537F
          D3FFFBFBFCAEFFFFFF00FFFFFF00F4F9FECF78B2FDFF4990F3FF3776DCFF3878
          DEFF2A63CCFF3776DDFF265EC7FF316ED7FF255DC6FF2962CCFF2B65CFFFB1C6
          ECFFFFFFFF7EFFFFFF00FFFFFF00FFFFFF24F3F6FEE778B2FDFF4D96F8FF306C
          D4FF3D7FE4FF2760C9FF3877DEFF275FC8FF2E6AD3FF3370D8FFB4C9EFFFD6E2
          F9AFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF24F3F6FEE778B2FDFF4E97
          F9FF2E69D1FF3E80E5FF2861CAFF3371D8FF3B7CE1FFB7CEF3FFD6E2F9AFFFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF24F3F6FEE778B2
          FDFF4E97F9FF2E6AD1FF3878DEFF4487EBFFBAD2F6FFD6E2F9AFFFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF24F3F6
          FEE778B2FDFF4D96F8FF4C93F4FFBDD6F9FFD6E2F9AFFFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF24F3F6FEE77AB3FDFFC0DAFDFFD6E2F9AFFFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF24FBFCFEAEFFFFFF7EFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00}
        ShowHint = True
        Transparent = True
        OnClick = ImLimparCCTClick
        SkinData.SkinSection = 'CHECKBOX'
      end
      object edExcedentes: TsCurrencyEdit
        Left = 128
        Top = 39
        Width = 42
        Height = 22
        AutoSize = False
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Qtd.'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Grayed = False
        GlyphMode.Blend = 0
        DisplayFormat = '0'
      end
      object edEmolumentos: TsDBEdit
        Left = 176
        Top = 39
        Width = 57
        Height = 22
        AutoSize = False
        Color = clWhite
        DataField = 'EXEMOLUMENTOS'
        DataSource = dsCertidao
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Emol.'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edFetj: TsDBEdit
        Left = 239
        Top = 39
        Width = 48
        Height = 22
        AutoSize = False
        Color = clWhite
        DataField = 'EXFETJ'
        DataSource = dsCertidao
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Fetj'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edFundperj: TsDBEdit
        Left = 293
        Top = 39
        Width = 50
        Height = 22
        AutoSize = False
        Color = clWhite
        DataField = 'EXFUNDPERJ'
        DataSource = dsCertidao
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Fundperj'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edFunperj: TsDBEdit
        Left = 349
        Top = 39
        Width = 50
        Height = 22
        AutoSize = False
        Color = clWhite
        DataField = 'EXFUNPERJ'
        DataSource = dsCertidao
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Funperj'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edTotal: TsDBEdit
        Left = 524
        Top = 39
        Width = 57
        Height = 22
        AutoSize = False
        Color = clWhite
        DataField = 'EXTOTAL'
        DataSource = dsCertidao
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 8
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Total'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edFunarpen: TsDBEdit
        Left = 405
        Top = 39
        Width = 57
        Height = 22
        AutoSize = False
        Color = clWhite
        DataField = 'EXFUNARPEN'
        DataSource = dsCertidao
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 6
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Funarpen'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edCCT: TsDBEdit
        Left = 650
        Top = 39
        Width = 83
        Height = 22
        Hint = 'CCT UTILIZADO NAS FOLHAS EXCEDENTES'
        AutoSize = False
        CharCase = ecUpperCase
        Color = 8454016
        DataField = 'CCT'
        DataSource = dsCertidao
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 10
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'CCT'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edRecibo: TsDBEdit
        Left = 587
        Top = 39
        Width = 57
        Height = 22
        AutoSize = False
        Color = clWhite
        DataField = 'EXRECIBO'
        DataSource = dsCertidao
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Recibo'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object edIss: TsDBEdit
        Left = 468
        Top = 39
        Width = 50
        Height = 22
        AutoSize = False
        Color = clWhite
        DataField = 'EXISS'
        DataSource = dsCertidao
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 7
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Iss'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
      end
      object cbTipoCobranca: TsDBComboBoxEx
        Left = 8
        Top = 39
        Width = 114
        Height = 22
        Cursor = crHandPoint
        Style = csDropDownList
        Color = clWhite
        DataField = 'EXCOBRANCA'
        DataSource = dsCertidao
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Items.Strings = (
          'Justi'#231'a Gratuita'
          'Com Cobran'#231'a'
          'Sem Cobran'#231'a'
          'NIHIL')
        ParentFont = False
        TabOrder = 0
        OnChange = cbTipoCobrancaChange
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Tipo de Cobran'#231'a'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        Values.Strings = (
          'JG'
          'CC'
          'SC'
          'NH')
      end
    end
  end
  object ckEmitir: TsCheckBox
    Left = 410
    Top = 524
    Width = 177
    Height = 20
    Caption = 'Salvar e Emitir Certid'#227'o'
    Checked = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    State = cbChecked
    TabOrder = 5
    ImgChecked = 0
    ImgUnchecked = 0
    SkinData.SkinSection = 'CHECKBOX'
  end
  object btRecibo: TsBitBtn
    Left = 12
    Top = 519
    Width = 197
    Height = 28
    Cursor = crHandPoint
    Caption = 'Recibo de Folhas Excedentes'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    OnClick = btReciboClick
    ImageIndex = 49
    Images = dm.Imagens
    SkinData.SkinSection = 'BUTTON'
  end
  object dsCertidao: TDataSource
    DataSet = dm.Certidoes
    Left = 952
    Top = 104
  end
  object dsEscreventes: TDataSource
    DataSet = dm.Escreventes
    Left = 1019
    Top = 104
  end
  object dsConsulta: TDataSource
    DataSet = Consulta
    Left = 320
    Top = 161
  end
  object Consulta: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'Documento'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'Data1'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Name = 'Data2'
        ParamType = ptInput
      end>
    ProviderName = 'dspConsulta'
    OnCalcFields = ConsultaCalcFields
    Left = 264
    Top = 161
    object ConsultaID_ATO: TIntegerField
      DisplayWidth = 10
      FieldName = 'ID_ATO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Visible = False
    end
    object ConsultaDT_PROTOCOLO: TDateField
      Alignment = taCenter
      DisplayLabel = 'Data'
      DisplayWidth = 10
      FieldName = 'DT_PROTOCOLO'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object ConsultaPROTOCOLO: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'Protocolo'
      DisplayWidth = 10
      FieldName = 'PROTOCOLO'
    end
    object ConsultaLIVRO_PROTOCOLO: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'Livro'
      DisplayWidth = 10
      FieldName = 'LIVRO_PROTOCOLO'
    end
    object ConsultaFOLHA_PROTOCOLO: TStringField
      Alignment = taCenter
      DisplayLabel = 'Folha'
      DisplayWidth = 10
      FieldName = 'FOLHA_PROTOCOLO'
      Size = 10
    end
    object ConsultaDT_REGISTRO: TDateField
      Alignment = taCenter
      DisplayLabel = 'Protesto'
      DisplayWidth = 10
      FieldName = 'DT_REGISTRO'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object ConsultaAPRESENTANTE: TStringField
      DisplayLabel = 'Apresentante'
      DisplayWidth = 100
      FieldName = 'APRESENTANTE'
      Size = 100
    end
    object ConsultaSTATUS: TStringField
      Alignment = taCenter
      DisplayLabel = 'Status'
      DisplayWidth = 20
      FieldName = 'STATUS'
    end
    object ConsultaTotal: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Total'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
      Calculated = True
    end
    object ConsultaCONVENIO: TStringField
      FieldName = 'CONVENIO'
      FixedChar = True
      Size = 1
    end
    object ConsultaSALDO_TITULO: TFloatField
      FieldName = 'SALDO_TITULO'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object ConsultaVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
  end
  object dspConsulta: TDataSetProvider
    DataSet = qryConsulta
    Options = [poAllowCommandText]
    Left = 208
    Top = 161
  end
  object dsResponsavel: TDataSource
    DataSet = dm.Responsavel
    Left = 1099
    Top = 104
  end
  object qryTitulo: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT '
      ''
      'ID_ATO,'
      'APRESENTANTE,'
      'CEDENTE,'
      'DEVEDOR,'
      'DT_REGISTRO,'
      'DT_TITULO,'
      'TIPO_ENDOSSO,'
      'TIPO_TITULO,'
      'LIVRO_REGISTRO,'
      'FOLHA_REGISTRO,'
      'PROTOCOLO,'
      'SACADOR,'
      'NUMERO_TITULO,'
      'VALOR_TITULO,'
      'DT_VENCIMENTO,'
      'CPF_CNPJ_SACADOR'
      ''
      'from TITULOS'
      ''
      'where ID_ATO=:ID_ATO'
      ''
      'order by PROTOCOLO')
    Left = 64
    Top = 160
    ParamData = <
      item
        Name = 'ID_ATO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryTituloID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryTituloAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Origin = 'APRESENTANTE'
      Size = 100
    end
    object qryTituloCEDENTE: TStringField
      FieldName = 'CEDENTE'
      Origin = 'CEDENTE'
      Size = 100
    end
    object qryTituloDEVEDOR: TStringField
      FieldName = 'DEVEDOR'
      Origin = 'DEVEDOR'
      Size = 100
    end
    object qryTituloDT_REGISTRO: TDateField
      FieldName = 'DT_REGISTRO'
      Origin = 'DT_REGISTRO'
    end
    object qryTituloDT_TITULO: TDateField
      FieldName = 'DT_TITULO'
      Origin = 'DT_TITULO'
    end
    object qryTituloTIPO_ENDOSSO: TStringField
      FieldName = 'TIPO_ENDOSSO'
      Origin = 'TIPO_ENDOSSO'
      FixedChar = True
      Size = 1
    end
    object qryTituloTIPO_TITULO: TIntegerField
      FieldName = 'TIPO_TITULO'
      Origin = 'TIPO_TITULO'
    end
    object qryTituloLIVRO_REGISTRO: TIntegerField
      FieldName = 'LIVRO_REGISTRO'
      Origin = 'LIVRO_REGISTRO'
    end
    object qryTituloFOLHA_REGISTRO: TStringField
      FieldName = 'FOLHA_REGISTRO'
      Origin = 'FOLHA_REGISTRO'
      Size = 10
    end
    object qryTituloPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object qryTituloSACADOR: TStringField
      FieldName = 'SACADOR'
      Origin = 'SACADOR'
      Size = 100
    end
    object qryTituloNUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
      Origin = 'NUMERO_TITULO'
    end
    object qryTituloVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
      Origin = 'VALOR_TITULO'
    end
    object qryTituloDT_VENCIMENTO: TDateField
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
    end
    object qryTituloCPF_CNPJ_SACADOR: TStringField
      FieldName = 'CPF_CNPJ_SACADOR'
      Origin = 'CPF_CNPJ_SACADOR'
      Size = 14
    end
  end
  object qryConsulta: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      ''
      'ID_ATO,'
      'LIVRO_PROTOCOLO,'
      'FOLHA_PROTOCOLO,'
      'PROTOCOLO,'
      'DT_PROTOCOLO,'
      'DT_REGISTRO,'
      'APRESENTANTE,'
      'STATUS,'
      'CONVENIO,'
      'SALDO_TITULO,'
      'VALOR_TITULO'
      ''
      'from TITULOS'
      ''
      'where CPF_CNPJ_DEVEDOR=:Documento and'
      ''
      'DT_REGISTRO between :Data1 and :Data2 and '
      ''
      'STATUS='#39'PROTESTADO'#39' '
      ''
      'order by PROTOCOLO')
    Left = 144
    Top = 160
    ParamData = <
      item
        Name = 'DOCUMENTO'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'DATA1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'DATA2'
        DataType = ftDate
        ParamType = ptInput
      end>
    object qryConsultaID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryConsultaLIVRO_PROTOCOLO: TIntegerField
      FieldName = 'LIVRO_PROTOCOLO'
      Origin = 'LIVRO_PROTOCOLO'
    end
    object qryConsultaFOLHA_PROTOCOLO: TStringField
      FieldName = 'FOLHA_PROTOCOLO'
      Origin = 'FOLHA_PROTOCOLO'
      Size = 10
    end
    object qryConsultaPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object qryConsultaDT_PROTOCOLO: TDateField
      FieldName = 'DT_PROTOCOLO'
      Origin = 'DT_PROTOCOLO'
    end
    object qryConsultaDT_REGISTRO: TDateField
      FieldName = 'DT_REGISTRO'
      Origin = 'DT_REGISTRO'
    end
    object qryConsultaAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Origin = 'APRESENTANTE'
      Size = 100
    end
    object qryConsultaSTATUS: TStringField
      FieldName = 'STATUS'
      Origin = 'STATUS'
    end
    object qryConsultaCONVENIO: TStringField
      FieldName = 'CONVENIO'
      Origin = 'CONVENIO'
      FixedChar = True
      Size = 1
    end
    object qryConsultaSALDO_TITULO: TFloatField
      FieldName = 'SALDO_TITULO'
      Origin = 'SALDO_TITULO'
    end
    object qryConsultaVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
      Origin = 'VALOR_TITULO'
    end
  end
end
