object FImportarTxtRetornoServentias: TFImportarTxtRetornoServentias
  Left = 277
  Top = 137
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Importa'#231#227'o de Retorno das Serventias'
  ClientHeight = 420
  ClientWidth = 893
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object pnlPrincipal: TsPanel
    Left = 0
    Top = 0
    Width = 893
    Height = 420
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    SkinData.SkinSection = 'SCROLLSLIDERH'
    object lblArquivosRetornoSAgreg: TLabel
      Left = 8
      Top = 8
      Width = 114
      Height = 15
      Caption = 'Arquivos de Retorno:'
    end
    object btnProcessar: TsBitBtn
      Left = 8
      Top = 351
      Width = 125
      Height = 42
      Cursor = crHandPoint
      Hint = 'Processar T'#237'tulos'
      Caption = 'Processar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnProcessarClick
      Reflected = True
      ImageIndex = 8
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object pnlContador: TsPanel
      Left = 0
      Top = 396
      Width = 893
      Height = 24
      Align = alBottom
      TabOrder = 1
      SkinData.SkinSection = 'SELECTION'
      object lblArquivosSelecionados: TLabel
        Left = 8
        Top = 5
        Width = 136
        Height = 14
        Cursor = crHandPoint
        Caption = '0 Arquivo(s) Selecionado(s)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object lblTitulos: TLabel
        Left = 836
        Top = 5
        Width = 48
        Height = 14
        Cursor = crHandPoint
        Alignment = taRightJustify
        Caption = '0 T'#237'tulo(s)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object shpLegendaTitJaImportado: TShape
        Left = 167
        Top = 5
        Width = 14
        Height = 14
        Brush.Color = clMaroon
      end
      object lblLegendaTitJaImportado: TLabel
        Left = 184
        Top = 5
        Width = 226
        Height = 14
        Caption = 'T'#237'tulo j'#225' importado em outro arquivo de Retorno'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object lblLegendaTitNaoExistente: TLabel
        Left = 444
        Top = 5
        Width = 176
        Height = 14
        Caption = 'T'#237'tulo n'#227'o existe no Banco de Dados'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object shpLegendaTitNaoExistente: TShape
        Left = 427
        Top = 5
        Width = 14
        Height = 14
        Brush.Color = clGray
      end
    end
    object G1: TwwDBGrid
      Left = 139
      Top = 10
      Width = 744
      Height = 385
      ControlType.Strings = (
        'Selecionado;CheckBox;True;False')
      Selected.Strings = (
        'Selecionado'#9'4'#9'*'#9'F'
        'PROTOCOLO_DISTRIBUIDOR'#9'14'#9'Protocolo Distrib.'#9'F'
        'PROTOCOLO_CARTORIO'#9'14'#9'Protocolo Cart.'#9'F'
        'NOME_PORTADOR'#9'23'#9'Apresentante'#9'F'
        'NUM_TITULO'#9'9'#9'N'#186' T'#237'tulo'#9'F'
        'COD_CEDENTE'#9'18'#9'Ag'#234'ncia Cedente'#9'F'
        'NOSSO_NUMERO'#9'15'#9'Nosso N'#250'mero'#9'F'
        'STATUS'#9'17'#9'Situa'#231#227'o'#9'F')
      IniAttributes.Delimiter = ';;'
      IniAttributes.UnicodeIniFile = False
      TitleColor = clBtnFace
      FixedCols = 1
      ShowHorzScrollBar = True
      EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
      BorderStyle = bsNone
      DataSource = dsDetail
      DefaultDrawing = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
      ParentFont = False
      PopupMenu = PM
      ReadOnly = True
      TabOrder = 2
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      TitleLines = 1
      TitleButtons = False
      UseTFields = False
      OnDrawDataCell = G1DrawDataCell
      OnExit = G1Exit
      OnMouseMove = G1MouseMove
      OnMouseUp = G1MouseUp
    end
    object RE: TRichEdit
      Left = 24
      Top = 568
      Width = 1321
      Height = 97
      BorderStyle = bsNone
      Color = 9699327
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssHorizontal
      TabOrder = 3
      WordWrap = False
      Zoom = 100
    end
    object btnCarregar: TsBitBtn
      Left = 8
      Top = 263
      Width = 125
      Height = 42
      Cursor = crHandPoint
      Hint = 'Carregar T'#237'tulos'
      Caption = 'Carregar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = btnCarregarClick
      Reflected = True
      ImageIndex = 35
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object btnLimpar: TsBitBtn
      Left = 8
      Top = 307
      Width = 125
      Height = 42
      Cursor = crHandPoint
      Hint = 'Limpar grid'
      Caption = 'Limpar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = btnLimparClick
      Reflected = True
      ImageIndex = 34
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object flbArquivosRetornoSAgreg: TFileListBox
    Left = 8
    Top = 29
    Width = 125
    Height = 228
    ItemHeight = 15
    TabOrder = 1
  end
  object dsDetail: TDataSource
    DataSet = mdDetail
    Left = 224
    Top = 344
  end
  object mdDetail: TRxMemoryData
    FieldDefs = <
      item
        Name = 'ID_REGISTRO'
        DataType = ftInteger
      end
      item
        Name = 'COD_PORTADOR'
        DataType = ftInteger
      end
      item
        Name = 'COD_CEDENTE'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'NOME_CEDENTE'
        DataType = ftString
        Size = 45
      end
      item
        Name = 'NOME_SACADOR'
        DataType = ftString
        Size = 45
      end
      item
        Name = 'DOC_SACADOR'
        DataType = ftString
        Size = 14
      end
      item
        Name = 'END_SACADOR'
        DataType = ftString
        Size = 45
      end
      item
        Name = 'CEP_SACADOR'
        DataType = ftInteger
      end
      item
        Name = 'CID_SACADOR'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'UF_SACADOR'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'NOSSO_NUMERO'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'ESPECIE_TITULO'
        DataType = ftString
        Size = 3
      end
      item
        Name = 'NUM_TITULO'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'DATA_EMISSAO'
        DataType = ftDate
      end
      item
        Name = 'DATA_VENCIMENTO'
        DataType = ftDate
      end
      item
        Name = 'TIPO_MOEDA'
        DataType = ftInteger
      end
      item
        Name = 'VALOR_TITULO'
        DataType = ftFloat
      end
      item
        Name = 'SALDO_TITULO'
        DataType = ftFloat
      end
      item
        Name = 'PRACA_PROTESTO'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TIPO_ENDOSSO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'INFO_ACEITE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NUM_CONTROLE'
        DataType = ftInteger
      end
      item
        Name = 'NOME_DEVEDOR'
        DataType = ftString
        Size = 45
      end
      item
        Name = 'TIPO_ID_DEVEDOR'
        DataType = ftInteger
      end
      item
        Name = 'NUM_ID_DEVEDOR'
        DataType = ftString
        Size = 14
      end
      item
        Name = 'DOC_DEVEDOR'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'END_DEVEDOR'
        DataType = ftString
        Size = 45
      end
      item
        Name = 'CEP_DEVEDOR'
        DataType = ftInteger
      end
      item
        Name = 'CID_DEVEDOR'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'UF_DEVEDOR'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'COD_CARTORIO'
        DataType = ftInteger
      end
      item
        Name = 'PROTOCOLO_CARTORIO'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'TIPO_OCORRENCIA'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DATA_PROTOCOLO'
        DataType = ftDate
      end
      item
        Name = 'VALOR_CUSTAS_CART'
        DataType = ftFloat
      end
      item
        Name = 'DECLARACAO_PORT'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DATA_OCORRENCIA'
        DataType = ftDate
      end
      item
        Name = 'COD_IRREGULARIDADE'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'BAIRRO_DEVEDOR'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'VALOR_CUSTAS_DIST'
        DataType = ftFloat
      end
      item
        Name = 'REGISTRO_DIST'
        DataType = ftInteger
      end
      item
        Name = 'VALOR_GRAVACAO'
        DataType = ftFloat
      end
      item
        Name = 'NUM_OPERACAO'
        DataType = ftInteger
      end
      item
        Name = 'NUM_CONTRATO'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'NUM_PARCELA'
        DataType = ftInteger
      end
      item
        Name = 'TIPO_LETRA'
        DataType = ftInteger
      end
      item
        Name = 'COMPL_COD_IRREG'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'PROTESTO_FALENCIA'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'INSTRUMENTO_PROT'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'VALOR_DEMAIS_DESP'
        DataType = ftFloat
      end
      item
        Name = 'COMPL_REGISTRO'
        DataType = ftString
        Size = 19
      end
      item
        Name = 'SEQ_REGISTRO'
        DataType = ftInteger
      end
      item
        Name = 'PROTOCOLO_DISTRIBUIDOR'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'NOME_PORTADOR'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'STATUS'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NOME_ARQ_RET'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CPF_ESCREVENTE'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'LIVRO'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'FOLHA'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'SELO'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'ALEATORIO'
        DataType = ftString
        Size = 3
      end
      item
        Name = 'ID_MSG'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'REGISTRO'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'ELETRONICO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'RECIBO_PAGAMENTO'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'TIPO_SUSTACAO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DATA_RET_PROT'
        DataType = ftDate
      end
      item
        Name = 'VALOR_PAGAMENTO'
        DataType = ftFloat
      end
      item
        Name = 'NUM_PAGAMENTO'
        DataType = ftString
        Size = 404
      end
      item
        Name = 'TARIFA'
        DataType = ftFloat
      end
      item
        Name = 'VALOR_AR'
        DataType = ftFloat
      end
      item
        Name = 'DATA_SUSTADO'
        DataType = ftDate
      end
      item
        Name = 'DETERMINACAO'
        DataType = ftString
        Size = 100
      end>
    Left = 224
    Top = 296
    object mdDetailID_REGISTRO: TStringField
      FieldName = 'ID_REGISTRO'
      Size = 1
    end
    object mdDetailCOD_PORTADOR: TStringField
      FieldName = 'COD_PORTADOR'
      Size = 3
    end
    object mdDetailCOD_CEDENTE: TStringField
      FieldName = 'COD_CEDENTE'
      Size = 15
    end
    object mdDetailNOME_CEDENTE: TStringField
      FieldName = 'NOME_CEDENTE'
      Size = 45
    end
    object mdDetailNOME_SACADOR: TStringField
      FieldName = 'NOME_SACADOR'
      Size = 45
    end
    object mdDetailDOC_SACADOR: TStringField
      FieldName = 'DOC_SACADOR'
      Size = 14
    end
    object mdDetailEND_SACADOR: TStringField
      FieldName = 'END_SACADOR'
      Size = 45
    end
    object mdDetailCEP_SACADOR: TIntegerField
      FieldName = 'CEP_SACADOR'
    end
    object mdDetailCID_SACADOR: TStringField
      FieldName = 'CID_SACADOR'
    end
    object mdDetailUF_SACADOR: TStringField
      FieldName = 'UF_SACADOR'
      Size = 2
    end
    object mdDetailNOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Size = 15
    end
    object mdDetailESPECIE_TITULO: TStringField
      FieldName = 'ESPECIE_TITULO'
      Size = 3
    end
    object mdDetailNUM_TITULO: TStringField
      FieldName = 'NUM_TITULO'
      Size = 11
    end
    object mdDetailDATA_EMISSAO: TDateField
      FieldName = 'DATA_EMISSAO'
    end
    object mdDetailDATA_VENCIMENTO: TDateField
      FieldName = 'DATA_VENCIMENTO'
    end
    object mdDetailTIPO_MOEDA: TIntegerField
      FieldName = 'TIPO_MOEDA'
    end
    object mdDetailVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
    end
    object mdDetailSALDO_TITULO: TFloatField
      FieldName = 'SALDO_TITULO'
    end
    object mdDetailPRACA_PROTESTO: TStringField
      FieldName = 'PRACA_PROTESTO'
    end
    object mdDetailTIPO_ENDOSSO: TStringField
      FieldName = 'TIPO_ENDOSSO'
      Size = 1
    end
    object mdDetailINFO_ACEITE: TStringField
      FieldName = 'INFO_ACEITE'
      Size = 1
    end
    object mdDetailNUM_CONTROLE: TIntegerField
      FieldName = 'NUM_CONTROLE'
    end
    object mdDetailNOME_DEVEDOR: TStringField
      FieldName = 'NOME_DEVEDOR'
      Size = 45
    end
    object mdDetailTIPO_ID_DEVEDOR: TIntegerField
      FieldName = 'TIPO_ID_DEVEDOR'
    end
    object mdDetailNUM_ID_DEVEDOR: TStringField
      FieldName = 'NUM_ID_DEVEDOR'
      Size = 14
    end
    object mdDetailDOC_DEVEDOR: TStringField
      FieldName = 'DOC_DEVEDOR'
      Size = 11
    end
    object mdDetailEND_DEVEDOR: TStringField
      FieldName = 'END_DEVEDOR'
      Size = 45
    end
    object mdDetailCEP_DEVEDOR: TIntegerField
      FieldName = 'CEP_DEVEDOR'
    end
    object mdDetailCID_DEVEDOR: TStringField
      FieldName = 'CID_DEVEDOR'
    end
    object mdDetailUF_DEVEDOR: TStringField
      FieldName = 'UF_DEVEDOR'
      Size = 2
    end
    object mdDetailCOD_CARTORIO: TIntegerField
      FieldName = 'COD_CARTORIO'
    end
    object mdDetailPROTOCOLO_CARTORIO: TStringField
      FieldName = 'PROTOCOLO_CARTORIO'
      Size = 10
    end
    object mdDetailTIPO_OCORRENCIA: TStringField
      FieldName = 'TIPO_OCORRENCIA'
      Size = 1
    end
    object mdDetailDATA_PROTOCOLO: TDateField
      FieldName = 'DATA_PROTOCOLO'
    end
    object mdDetailVALOR_CUSTAS_CART: TFloatField
      FieldName = 'VALOR_CUSTAS_CART'
    end
    object mdDetailDECLARACAO_PORT: TStringField
      FieldName = 'DECLARACAO_PORT'
      Size = 1
    end
    object mdDetailDATA_OCORRENCIA: TDateField
      FieldName = 'DATA_OCORRENCIA'
    end
    object mdDetailCOD_IRREGULARIDADE: TStringField
      FieldName = 'COD_IRREGULARIDADE'
      Size = 2
    end
    object mdDetailBAIRRO_DEVEDOR: TStringField
      FieldName = 'BAIRRO_DEVEDOR'
    end
    object mdDetailVALOR_CUSTAS_DIST: TFloatField
      FieldName = 'VALOR_CUSTAS_DIST'
    end
    object mdDetailREGISTRO_DIST: TIntegerField
      FieldName = 'REGISTRO_DIST'
    end
    object mdDetailVALOR_GRAVACAO: TFloatField
      FieldName = 'VALOR_GRAVACAO'
    end
    object mdDetailNUM_OPERACAO: TIntegerField
      FieldName = 'NUM_OPERACAO'
    end
    object mdDetailNUM_CONTRATO: TStringField
      FieldName = 'NUM_CONTRATO'
      Size = 15
    end
    object mdDetailNUM_PARCELA: TIntegerField
      FieldName = 'NUM_PARCELA'
    end
    object mdDetailTIPO_LETRA: TIntegerField
      FieldName = 'TIPO_LETRA'
    end
    object mdDetailCOMPL_COD_IRREG: TStringField
      FieldName = 'COMPL_COD_IRREG'
      Size = 8
    end
    object mdDetailPROTESTO_FALENCIA: TStringField
      FieldName = 'PROTESTO_FALENCIA'
      Size = 1
    end
    object mdDetailINSTRUMENTO_PROT: TStringField
      FieldName = 'INSTRUMENTO_PROT'
      Size = 1
    end
    object mdDetailVALOR_DEMAIS_DESP: TFloatField
      FieldName = 'VALOR_DEMAIS_DESP'
    end
    object mdDetailCOMPL_REGISTRO: TStringField
      FieldName = 'COMPL_REGISTRO'
      Size = 19
    end
    object mdDetailSEQ_REGISTRO: TIntegerField
      FieldName = 'SEQ_REGISTRO'
    end
    object mdDetailPROTOCOLO_DISTRIBUIDOR: TStringField
      FieldName = 'PROTOCOLO_DISTRIBUIDOR'
      Size = 10
    end
    object mdDetailNOME_PORTADOR: TStringField
      FieldName = 'NOME_PORTADOR'
      Size = 100
    end
    object mdDetailSTATUS: TStringField
      FieldName = 'STATUS'
    end
    object mdDetailNOME_ARQ_RET: TStringField
      FieldName = 'NOME_ARQ_RET'
    end
    object mdDetailCPF_ESCREVENTE: TStringField
      FieldName = 'CPF_ESCREVENTE'
      Size = 11
    end
    object mdDetailLIVRO: TStringField
      FieldName = 'LIVRO'
      Size = 10
    end
    object mdDetailFOLHA: TStringField
      FieldName = 'FOLHA'
      Size = 10
    end
    object mdDetailSELO: TStringField
      FieldName = 'SELO'
      Size = 9
    end
    object mdDetailALEATORIO: TStringField
      FieldName = 'ALEATORIO'
      Size = 3
    end
    object mdDetailID_MSG: TStringField
      FieldName = 'ID_MSG'
      Size = 10
    end
    object mdDetailREGISTRO: TStringField
      FieldName = 'REGISTRO'
      Size = 10
    end
    object mdDetailELETRONICO: TStringField
      FieldName = 'ELETRONICO'
      Size = 1
    end
    object mdDetailRECIBO_PAGAMENTO: TStringField
      FieldName = 'RECIBO_PAGAMENTO'
      Size = 10
    end
    object mdDetailTIPO_SUSTACAO: TStringField
      FieldName = 'TIPO_SUSTACAO'
      Size = 1
    end
    object mdDetailDATA_RET_PROT: TDateField
      FieldName = 'DATA_RET_PROT'
    end
    object mdDetailVALOR_PAGAMENTO: TFloatField
      FieldName = 'VALOR_PAGAMENTO'
    end
    object mdDetailNUM_PAGAMENTO: TStringField
      FieldName = 'NUM_PAGAMENTO'
      Size = 404
    end
    object mdDetailTARIFA: TFloatField
      FieldName = 'TARIFA'
    end
    object mdDetailVALOR_AR: TFloatField
      FieldName = 'VALOR_AR'
    end
    object mdDetailDATA_SUSTADO: TDateField
      FieldName = 'DATA_SUSTADO'
    end
    object mdDetailDETERMINACAO: TStringField
      FieldName = 'DETERMINACAO'
      Size = 100
    end
    object mdDetailDT_PROTOCOLO_CARTORIO: TDateField
      FieldName = 'DT_PROTOCOLO_CARTORIO'
    end
    object mdDetailFLG_REIMPORTAR: TStringField
      FieldName = 'FLG_REIMPORTAR'
      Size = 1
    end
    object mdDetailSelecionado: TBooleanField
      FieldName = 'Selecionado'
    end
    object mdDetailFLG_EXISTE: TStringField
      FieldName = 'FLG_EXISTE'
      Size = 1
    end
    object mdDetailPOSTECIPADO: TStringField
      FieldName = 'POSTECIPADO'
      Size = 1
    end
  end
  object dspTitulo: TDataSetProvider
    DataSet = qryTitulo
    Left = 720
    Top = 336
  end
  object cdsTitulo: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'PROTOCOLO'
        ParamType = ptInput
      end>
    ProviderName = 'dspTitulo'
    Left = 768
    Top = 336
    object cdsTituloID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Required = True
    end
    object cdsTituloCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
    object cdsTituloRECIBO: TIntegerField
      FieldName = 'RECIBO'
    end
    object cdsTituloDT_ENTRADA: TDateField
      FieldName = 'DT_ENTRADA'
    end
    object cdsTituloDT_PROTOCOLO: TDateField
      FieldName = 'DT_PROTOCOLO'
    end
    object cdsTituloPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
    end
    object cdsTituloLIVRO_PROTOCOLO: TIntegerField
      FieldName = 'LIVRO_PROTOCOLO'
    end
    object cdsTituloFOLHA_PROTOCOLO: TStringField
      FieldName = 'FOLHA_PROTOCOLO'
      Size = 10
    end
    object cdsTituloDT_PRAZO: TDateField
      FieldName = 'DT_PRAZO'
    end
    object cdsTituloDT_REGISTRO: TDateField
      FieldName = 'DT_REGISTRO'
    end
    object cdsTituloREGISTRO: TIntegerField
      FieldName = 'REGISTRO'
    end
    object cdsTituloLIVRO_REGISTRO: TIntegerField
      FieldName = 'LIVRO_REGISTRO'
    end
    object cdsTituloFOLHA_REGISTRO: TStringField
      FieldName = 'FOLHA_REGISTRO'
      Size = 10
    end
    object cdsTituloSELO_REGISTRO: TStringField
      FieldName = 'SELO_REGISTRO'
      Size = 9
    end
    object cdsTituloDT_PAGAMENTO: TDateField
      FieldName = 'DT_PAGAMENTO'
    end
    object cdsTituloSELO_PAGAMENTO: TStringField
      FieldName = 'SELO_PAGAMENTO'
      Size = 9
    end
    object cdsTituloRECIBO_PAGAMENTO: TIntegerField
      FieldName = 'RECIBO_PAGAMENTO'
    end
    object cdsTituloCOBRANCA: TStringField
      FieldName = 'COBRANCA'
      FixedChar = True
      Size = 2
    end
    object cdsTituloEMOLUMENTOS: TFloatField
      FieldName = 'EMOLUMENTOS'
    end
    object cdsTituloFETJ: TFloatField
      FieldName = 'FETJ'
    end
    object cdsTituloFUNDPERJ: TFloatField
      FieldName = 'FUNDPERJ'
    end
    object cdsTituloFUNPERJ: TFloatField
      FieldName = 'FUNPERJ'
    end
    object cdsTituloFUNARPEN: TFloatField
      FieldName = 'FUNARPEN'
    end
    object cdsTituloPMCMV: TFloatField
      FieldName = 'PMCMV'
    end
    object cdsTituloISS: TFloatField
      FieldName = 'ISS'
    end
    object cdsTituloMUTUA: TFloatField
      FieldName = 'MUTUA'
    end
    object cdsTituloDISTRIBUICAO: TFloatField
      FieldName = 'DISTRIBUICAO'
    end
    object cdsTituloACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
    end
    object cdsTituloTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
    object cdsTituloTIPO_PROTESTO: TIntegerField
      FieldName = 'TIPO_PROTESTO'
    end
    object cdsTituloTIPO_TITULO: TIntegerField
      FieldName = 'TIPO_TITULO'
    end
    object cdsTituloNUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
    end
    object cdsTituloDT_TITULO: TDateField
      FieldName = 'DT_TITULO'
    end
    object cdsTituloBANCO: TStringField
      FieldName = 'BANCO'
    end
    object cdsTituloAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Size = 10
    end
    object cdsTituloCONTA: TStringField
      FieldName = 'CONTA'
      Size = 10
    end
    object cdsTituloVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
    end
    object cdsTituloSALDO_PROTESTO: TFloatField
      FieldName = 'SALDO_PROTESTO'
    end
    object cdsTituloDT_VENCIMENTO: TDateField
      FieldName = 'DT_VENCIMENTO'
    end
    object cdsTituloCONVENIO: TStringField
      FieldName = 'CONVENIO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloTIPO_APRESENTACAO: TStringField
      FieldName = 'TIPO_APRESENTACAO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloDT_ENVIO: TDateField
      FieldName = 'DT_ENVIO'
    end
    object cdsTituloCODIGO_APRESENTANTE: TStringField
      FieldName = 'CODIGO_APRESENTANTE'
      Size = 10
    end
    object cdsTituloTIPO_INTIMACAO: TStringField
      FieldName = 'TIPO_INTIMACAO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloMOTIVO_INTIMACAO: TMemoField
      FieldName = 'MOTIVO_INTIMACAO'
      BlobType = ftMemo
      Size = 1
    end
    object cdsTituloDT_INTIMACAO: TDateField
      FieldName = 'DT_INTIMACAO'
    end
    object cdsTituloDT_PUBLICACAO: TDateField
      FieldName = 'DT_PUBLICACAO'
    end
    object cdsTituloVALOR_PAGAMENTO: TFloatField
      FieldName = 'VALOR_PAGAMENTO'
    end
    object cdsTituloAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Size = 100
    end
    object cdsTituloCPF_CNPJ_APRESENTANTE: TStringField
      FieldName = 'CPF_CNPJ_APRESENTANTE'
      Size = 15
    end
    object cdsTituloTIPO_APRESENTANTE: TStringField
      FieldName = 'TIPO_APRESENTANTE'
      FixedChar = True
      Size = 1
    end
    object cdsTituloCEDENTE: TStringField
      FieldName = 'CEDENTE'
      Size = 100
    end
    object cdsTituloNOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Size = 15
    end
    object cdsTituloSACADOR: TStringField
      FieldName = 'SACADOR'
      Size = 100
    end
    object cdsTituloDEVEDOR: TStringField
      FieldName = 'DEVEDOR'
      Size = 100
    end
    object cdsTituloCPF_CNPJ_DEVEDOR: TStringField
      FieldName = 'CPF_CNPJ_DEVEDOR'
      Size = 15
    end
    object cdsTituloTIPO_DEVEDOR: TStringField
      FieldName = 'TIPO_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object cdsTituloAGENCIA_CEDENTE: TStringField
      FieldName = 'AGENCIA_CEDENTE'
      Size = 15
    end
    object cdsTituloPRACA_PROTESTO: TStringField
      FieldName = 'PRACA_PROTESTO'
    end
    object cdsTituloTIPO_ENDOSSO: TStringField
      FieldName = 'TIPO_ENDOSSO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloACEITE: TStringField
      FieldName = 'ACEITE'
      FixedChar = True
      Size = 1
    end
    object cdsTituloCPF_ESCREVENTE: TStringField
      FieldName = 'CPF_ESCREVENTE'
      Size = 11
    end
    object cdsTituloCPF_ESCREVENTE_PG: TStringField
      FieldName = 'CPF_ESCREVENTE_PG'
      Size = 11
    end
    object cdsTituloOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      BlobType = ftMemo
      Size = 1
    end
    object cdsTituloDT_SUSTADO: TDateField
      FieldName = 'DT_SUSTADO'
    end
    object cdsTituloDT_RETIRADO: TDateField
      FieldName = 'DT_RETIRADO'
    end
    object cdsTituloSTATUS: TStringField
      FieldName = 'STATUS'
    end
    object cdsTituloPROTESTADO: TStringField
      FieldName = 'PROTESTADO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloENVIADO_APONTAMENTO: TStringField
      FieldName = 'ENVIADO_APONTAMENTO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloENVIADO_PROTESTO: TStringField
      FieldName = 'ENVIADO_PROTESTO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloENVIADO_PAGAMENTO: TStringField
      FieldName = 'ENVIADO_PAGAMENTO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloENVIADO_RETIRADO: TStringField
      FieldName = 'ENVIADO_RETIRADO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloENVIADO_SUSTADO: TStringField
      FieldName = 'ENVIADO_SUSTADO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloENVIADO_DEVOLVIDO: TStringField
      FieldName = 'ENVIADO_DEVOLVIDO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloEXPORTADO: TStringField
      FieldName = 'EXPORTADO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloAVALISTA_DEVEDOR: TStringField
      FieldName = 'AVALISTA_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object cdsTituloFINS_FALIMENTARES: TStringField
      FieldName = 'FINS_FALIMENTARES'
      FixedChar = True
      Size = 1
    end
    object cdsTituloTARIFA_BANCARIA: TFloatField
      FieldName = 'TARIFA_BANCARIA'
    end
    object cdsTituloFORMA_PAGAMENTO: TStringField
      FieldName = 'FORMA_PAGAMENTO'
      FixedChar = True
      Size = 2
    end
    object cdsTituloNUMERO_PAGAMENTO: TStringField
      FieldName = 'NUMERO_PAGAMENTO'
      Size = 40
    end
    object cdsTituloARQUIVO: TStringField
      FieldName = 'ARQUIVO'
    end
    object cdsTituloRETORNO: TStringField
      FieldName = 'RETORNO'
    end
    object cdsTituloDT_DEVOLVIDO: TDateField
      FieldName = 'DT_DEVOLVIDO'
    end
    object cdsTituloCPF_CNPJ_SACADOR: TStringField
      FieldName = 'CPF_CNPJ_SACADOR'
      Size = 14
    end
    object cdsTituloID_MSG: TIntegerField
      FieldName = 'ID_MSG'
    end
    object cdsTituloVALOR_AR: TFloatField
      FieldName = 'VALOR_AR'
    end
    object cdsTituloELETRONICO: TStringField
      FieldName = 'ELETRONICO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloIRREGULARIDADE: TIntegerField
      FieldName = 'IRREGULARIDADE'
    end
    object cdsTituloAVISTA: TStringField
      FieldName = 'AVISTA'
      FixedChar = True
      Size = 1
    end
    object cdsTituloSALDO_TITULO: TFloatField
      FieldName = 'SALDO_TITULO'
    end
    object cdsTituloTIPO_SUSTACAO: TStringField
      FieldName = 'TIPO_SUSTACAO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloRETORNO_PROTESTO: TStringField
      FieldName = 'RETORNO_PROTESTO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloDT_RETORNO_PROTESTO: TDateField
      FieldName = 'DT_RETORNO_PROTESTO'
    end
    object cdsTituloDT_DEFINITIVA: TDateField
      FieldName = 'DT_DEFINITIVA'
    end
    object cdsTituloJUDICIAL: TStringField
      FieldName = 'JUDICIAL'
      FixedChar = True
      Size = 1
    end
    object cdsTituloALEATORIO_PROTESTO: TStringField
      FieldName = 'ALEATORIO_PROTESTO'
      Size = 3
    end
    object cdsTituloALEATORIO_SOLUCAO: TStringField
      FieldName = 'ALEATORIO_SOLUCAO'
      Size = 3
    end
    object cdsTituloCCT: TStringField
      FieldName = 'CCT'
      Size = 9
    end
    object cdsTituloDETERMINACAO: TStringField
      FieldName = 'DETERMINACAO'
      Size = 100
    end
    object cdsTituloANTIGO: TStringField
      FieldName = 'ANTIGO'
      FixedChar = True
      Size = 1
    end
    object cdsTituloLETRA: TStringField
      FieldName = 'LETRA'
      Size = 1
    end
    object cdsTituloPROTOCOLO_CARTORIO: TIntegerField
      FieldName = 'PROTOCOLO_CARTORIO'
    end
    object cdsTituloARQUIVO_CARTORIO: TStringField
      FieldName = 'ARQUIVO_CARTORIO'
    end
    object cdsTituloRETORNO_CARTORIO: TStringField
      FieldName = 'RETORNO_CARTORIO'
    end
    object cdsTituloDT_DEVOLVIDO_CARTORIO: TDateField
      FieldName = 'DT_DEVOLVIDO_CARTORIO'
    end
    object cdsTituloCARTORIO: TIntegerField
      FieldName = 'CARTORIO'
    end
    object cdsTituloDT_PROTOCOLO_CARTORIO: TDateField
      FieldName = 'DT_PROTOCOLO_CARTORIO'
    end
  end
  object dsTitulo: TDataSource
    DataSet = cdsTitulo
    Left = 816
    Top = 336
  end
  object qryTitulo: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM TITULOS')
    Left = 672
    Top = 336
  end
  object PM: TPopupMenu
    Left = 816
    Top = 113
    object MarcarTodos: TMenuItem
      Caption = 'Marcar todos'
      OnClick = MarcarTodosClick
    end
    object DesmarcarTodos: TMenuItem
      Caption = 'Desmarcar todos'
      OnClick = DesmarcarTodosClick
    end
  end
  object qryPortadores: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM PORTADORES'
      ' WHERE CODIGO = :CODIGO'
      'ORDER BY NOME')
    Left = 472
    Top = 296
    ParamData = <
      item
        Name = 'CODIGO'
        DataType = ftString
        ParamType = ptInput
        Size = 10
      end>
    object qryPortadoresID_PORTADOR: TIntegerField
      FieldName = 'ID_PORTADOR'
      Origin = 'ID_PORTADOR'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryPortadoresCODIGO: TStringField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
      Size = 10
    end
    object qryPortadoresNOME: TStringField
      FieldName = 'NOME'
      Origin = 'NOME'
      Size = 100
    end
    object qryPortadoresTIPO: TStringField
      FieldName = 'TIPO'
      Origin = 'TIPO'
      FixedChar = True
      Size = 1
    end
    object qryPortadoresDOCUMENTO: TStringField
      FieldName = 'DOCUMENTO'
      Origin = 'DOCUMENTO'
      Size = 14
    end
    object qryPortadoresENDERECO: TStringField
      FieldName = 'ENDERECO'
      Origin = 'ENDERECO'
      Size = 100
    end
    object qryPortadoresBANCO: TStringField
      FieldName = 'BANCO'
      Origin = 'BANCO'
      FixedChar = True
      Size = 1
    end
    object qryPortadoresCONVENIO: TStringField
      FieldName = 'CONVENIO'
      Origin = 'CONVENIO'
      FixedChar = True
      Size = 1
    end
    object qryPortadoresCONTA: TStringField
      FieldName = 'CONTA'
      Origin = 'CONTA'
    end
    object qryPortadoresOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      BlobType = ftMemo
    end
    object qryPortadoresAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Origin = 'AGENCIA'
      Size = 6
    end
    object qryPortadoresPRACA: TStringField
      FieldName = 'PRACA'
      Origin = 'PRACA'
      Size = 7
    end
    object qryPortadoresCRA: TStringField
      FieldName = 'CRA'
      Origin = 'CRA'
      FixedChar = True
      Size = 1
    end
    object qryPortadoresSEQUENCIA: TIntegerField
      FieldName = 'SEQUENCIA'
      Origin = 'SEQUENCIA'
    end
    object qryPortadoresVALOR_DOC: TFloatField
      FieldName = 'VALOR_DOC'
      Origin = 'VALOR_DOC'
    end
    object qryPortadoresVALOR_TED: TFloatField
      FieldName = 'VALOR_TED'
      Origin = 'VALOR_TED'
    end
    object qryPortadoresNOMINAL: TStringField
      FieldName = 'NOMINAL'
      Origin = 'NOMINAL'
      FixedChar = True
      Size = 1
    end
    object qryPortadoresFORCA_LEI: TStringField
      FieldName = 'FORCA_LEI'
      Origin = 'FORCA_LEI'
      FixedChar = True
      Size = 1
    end
    object qryPortadoresESPECIE: TStringField
      FieldName = 'ESPECIE'
      Origin = 'ESPECIE'
      FixedChar = True
      Size = 1
    end
  end
  object dsPortadores: TDataSource
    DataSet = qryPortadores
    Left = 472
    Top = 344
  end
end
