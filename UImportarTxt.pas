{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UImportarTxt.pas
  Descricao:   Tela de importacao dos Arquivos de Remessa pelo Distribuidor
  Author:
  Date:        24-ago-2016
  Last Update: 12-out-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UImportarTxt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, DB, RxMemDS, Grids, Wwdbigrd,
  Wwdbgrid, ExtCtrls, sPanel, sLabel, sDialogs, Mask, DBCtrls, sDBEdit,
  sMaskEdit, sCustomComboEdit, sTooledit, sDBDateEdit, sDBNavigator,
  sDBText, sGroupBox, sDBRadioGroup, acDBTextFX, Menus, FMTBcd,
  SqlExpr, ClipBrd, DBGrids, RXDBCtrl, ComCtrls, acProgressBar, Gauges,
  jpeg, sSpeedButton, sEdit, sCheckBox, IniFiles, StrUtils, acImage,
  System.DateUtils, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  sDBCheckBox;

type
  TFImportarTxt = class(TForm)
    P1: TsPanel;
    GridAtos: TwwDBGrid;
    RX1: TRxMemoryData;
    dsRX: TDataSource;
    btArquivo: TsBitBtn;
    Opd: TsOpenDialog;
    P2: TsPanel;
    edDevedor: TsDBEdit;
    edDataTitulo: TsDBDateEdit;
    edValor: TsDBEdit;
    edEspecie: TsDBEdit;
    edDataVencimento: TsDBDateEdit;
    edDocumento: TsDBEdit;
    edEndereco: TsDBEdit;
    edBairro: TsDBEdit;
    edUF: TsDBEdit;
    edCidade: TsDBEdit;
    edPraca: TsDBEdit;
    sDBNavigator1: TsDBNavigator;
    btProcessar: TsBitBtn;
    PM: TPopupMenu;
    MarcarTodos: TMenuItem;
    DesmarcarTodos: TMenuItem;
    Entrada: TRxMemoryData;
    dsEntrada: TDataSource;
    EntradaLinha: TMemoField;
    Saida: TRxMemoryData;
    dsSaida: TDataSource;
    SaidaLinha: TMemoField;
    btRejeitar: TsBitBtn;
    edProtocolo: TsDBEdit;
    dsControle: TDataSource;
    btCriticas: TsBitBtn;
    edData: TsDateEdit;
    RXBC: TRxMemoryData;
    dsRXBC: TDataSource;
    RXEN: TRxMemoryData;
    dsRXEN: TDataSource;
    Linhas: TRxMemoryData;
    LinhasLinha: TMemoField;
    Portadores: TRxMemoryData;
    wwDBGrid1: TwwDBGrid;
    PA: TsPanel;

    Memo1: TMemo;
    PMotivo: TsPanel;
    sPanel2: TsPanel;
    lbAviso: TLabel;
    lbIrreguralidade: TsLabel;
    txMotivo: TsDBText;
    edSaldo: TsDBEdit;
    ckVGF: TsCheckBox;
    wwDBGrid2: TwwDBGrid;
    dsLinhas: TDataSource;
    wwDBGrid3: TwwDBGrid;
    PortadoresCodigo: TStringField;
    PortadoresNome: TStringField;
    PortadoresAgencia: TStringField;
    PortadoresPraca: TStringField;
    RX1CEDENTE: TStringField;
    RX1SACADOR: TStringField;
    RX1DEVEDOR: TStringField;
    RX1TIPO_DEVEDOR: TStringField;
    RX1CPF_CNPJ_DEVEDOR: TStringField;
    RX1RG_DEVEDOR: TStringField;
    RX1END_DEVEDOR: TStringField;
    RX1CEP_DEVEDOR: TStringField;
    RX1UF: TStringField;
    RX1CID_DEVEDOR: TStringField;
    RX1BAIRRO_DEVEDOR: TStringField;
    RX1DT_TITULO: TDateField;
    RX1DT_VENCIMENTO: TDateField;
    RX1VALOR_TITULO: TFloatField;
    RX1APRESENTANTE: TStringField;
    RX1CODIGO_APRESENTANTE: TStringField;
    RX1TIPO_TITULO: TIntegerField;
    RX1NUMERO_TITULO: TStringField;
    RX1SALDO_TITULO: TFloatField;
    RX1PRACA: TStringField;
    RX1ACEITE: TStringField;
    RX1ENDOSSO: TStringField;
    RX1STATUS: TStringField;
    RX1NOSSO_NUMERO: TStringField;
    RX1AGENCIA: TStringField;
    RX1CUSTAS: TFloatField;
    RX1CPF_CNPJ_SACADOR: TStringField;
    RX1END_SACADOR: TStringField;
    RX1CEP_SACADOR: TStringField;
    RX1UF_SACADOR: TStringField;
    RX1BAIRRO_SACADOR: TStringField;
    RX1CID_SACADOR: TStringField;
    RX1ESPECIE: TStringField;
    RX1Documento: TStringField;
    RX1Check: TBooleanField;
    RX1Protocolo: TIntegerField;
    RX1Irregularidade: TStringField;
    RX1MotivoRejeicao: TStringField;
    RX1CONVENIO: TBooleanField;
    RX1AVISTA: TStringField;
    RXENCodigo: TStringField;
    RXENLinha: TMemoField;
    RXBCCodigo: TStringField;
    RXBCImportar: TMemoField;
    RXBCExportar: TMemoField;
    RE: TRichEdit;
    MM: TMemo;
    RX1LETRA: TStringField;
    edCep: TsDBEdit;
    RX1FINS: TStringField;
    RX1NUMDEVEDOR: TStringField;
    RX1SERVENTIA_AGREG: TStringField;
    RX1SEQUENCIAL_ARQ: TStringField;
    RX1SEQ_BANCO: TIntegerField;
    RX1FLG_COOBRIGADO: TStringField;
    sImage1: TsImage;
    qryDuplicado: TFDQuery;
    qryConsultaProtocolo: TFDQuery;
    qryFaixa: TFDQuery;
    qryTabela: TFDQuery;
    qryTabelaORDEM: TIntegerField;
    qryTabelaANO: TIntegerField;
    qryTabelaVAI: TStringField;
    qryTabelaTAB: TStringField;
    qryTabelaITEM: TStringField;
    qryTabelaSUB: TStringField;
    qryTabelaDESCR: TStringField;
    qryTabelaVALOR: TFloatField;
    qryTabelaTEXTO: TStringField;
    qryFaixaCOD: TIntegerField;
    qryFaixaEMOL: TFloatField;
    qryFaixaFETJ: TFloatField;
    qryFaixaFUND: TFloatField;
    qryFaixaFUNP: TFloatField;
    qryFaixaMUTUA: TFloatField;
    qryFaixaACOTERJ: TFloatField;
    qryFaixaDISTRIB: TFloatField;
    qryFaixaTOT: TFloatField;
    qryFaixaTITULO: TStringField;
    qryDuplicadoID_ATO: TIntegerField;
    qryConsultaProtocoloID_ATO: TIntegerField;
    RX1DIAS_AVISTA: TIntegerField;
    RX1CARTORIO: TIntegerField;
    edtNossoNumero: TsDBEdit;
    edCedente: TsDBEdit;
    edSacador: TsDBEdit;
    PB: TsProgressBar;
    RX1POSTECIPADO: TStringField;
    RXFAKE: TFDMemTable;
    RXFAKECEDENTE: TStringField;
    RXFAKESACADOR: TStringField;
    RXFAKEDEVEDOR: TStringField;
    RXFAKETIPO_DEVEDOR: TStringField;
    RXFAKECPF_CNPJ_DEVEDOR: TStringField;
    RXFAKERG_DEVEDOR: TStringField;
    RXFAKEEND_DEVEDOR: TStringField;
    RXFAKECEP_DEVEDOR: TStringField;
    RXFAKEUF: TStringField;
    RXFAKECID_DEVEDOR: TStringField;
    RXFAKEBAIRRO_DEVEDOR: TStringField;
    RXFAKEDT_TITULO: TDateField;
    RXFAKEDT_VENCIMENTO: TDateField;
    RXFAKEVALOR_TITULO: TFloatField;
    RXFAKEAPRESENTANTE: TStringField;
    RXFAKECODIGO_APRESENTANTE: TStringField;
    RXFAKETIPO_TITULO: TIntegerField;
    RXFAKENUMERO_TITULO: TStringField;
    RXFAKESALDO_TITULO: TFloatField;
    RXFAKEPRACA: TStringField;
    RXFAKEACEITE: TStringField;
    RXFAKEENDOSSO: TStringField;
    RXFAKESTATUS: TStringField;
    RXFAKENOSSO_NUMERO: TStringField;
    RXFAKEAGENCIA: TStringField;
    RXFAKECUSTAS: TFloatField;
    RXFAKECPF_CNPJ_SACADOR: TStringField;
    RXFAKEEND_SACADOR: TStringField;
    RXFAKECEP_SACADOR: TStringField;
    RXFAKEUF_SACADOR: TStringField;
    RXFAKEBAIRRO_SACADOR: TStringField;
    RXFAKECID_SACADOR: TStringField;
    RXFAKEESPECIE: TStringField;
    RXFAKEDocumento: TStringField;
    RXFAKECheck: TBooleanField;
    RXFAKEProtocolo: TIntegerField;
    RXFAKEIrregularidade: TStringField;
    RXFAKEMotivoRejeicao: TStringField;
    RXFAKECONVENIO: TBooleanField;
    RXFAKEAVISTA: TStringField;
    RXFAKELETRA: TStringField;
    RXFAKEFINS: TStringField;
    RXFAKENUMDEVEDOR: TStringField;
    RXFAKESERVENTIA_AGREG: TStringField;
    RXFAKESEQUENCIAL_ARQ: TStringField;
    RXFAKESEQ_BANCO: TIntegerField;
    RXFAKEFLG_COOBRIGADO: TStringField;
    RXFAKEDIAS_AVISTA: TIntegerField;
    RXFAKECARTORIO: TIntegerField;
    RXFAKEPOSTECIPADO: TStringField;
    RX: TFDMemTable;
    RXCEDENTE: TStringField;
    RXSACADOR: TStringField;
    RXDEVEDOR: TStringField;
    RXTIPO_DEVEDOR: TStringField;
    RXCPF_CNPJ_DEVEDOR: TStringField;
    RXRG_DEVEDOR: TStringField;
    RXEND_DEVEDOR: TStringField;
    RXCEP_DEVEDOR: TStringField;
    RXUF: TStringField;
    RXCID_DEVEDOR: TStringField;
    RXBAIRRO_DEVEDOR: TStringField;
    RXDT_TITULO: TDateField;
    RXDT_VENCIMENTO: TDateField;
    RXVALOR_TITULO: TFloatField;
    RXAPRESENTANTE: TStringField;
    RXCODIGO_APRESENTANTE: TStringField;
    RXTIPO_TITULO: TIntegerField;
    RXNUMERO_TITULO: TStringField;
    RXSALDO_TITULO: TFloatField;
    RXPRACA: TStringField;
    RXACEITE: TStringField;
    RXENDOSSO: TStringField;
    RXSTATUS: TStringField;
    RXNOSSO_NUMERO: TStringField;
    RXAGENCIA: TStringField;
    RXCUSTAS: TFloatField;
    RXCPF_CNPJ_SACADOR: TStringField;
    RXEND_SACADOR: TStringField;
    RXCEP_SACADOR: TStringField;
    RXUF_SACADOR: TStringField;
    RXBAIRRO_SACADOR: TStringField;
    RXCID_SACADOR: TStringField;
    RXESPECIE: TStringField;
    RXDocumento: TStringField;
    RXCheck: TBooleanField;
    RXProtocolo: TIntegerField;
    RXIrregularidade: TStringField;
    RXMotivoRejeicao: TStringField;
    RXCONVENIO: TBooleanField;
    RXAVISTA: TStringField;
    RXLETRA: TStringField;
    RXFINS: TStringField;
    RXNUMDEVEDOR: TStringField;
    RXSERVENTIA_AGREG: TStringField;
    RXSEQUENCIAL_ARQ: TStringField;
    RXSEQ_BANCO: TIntegerField;
    RXFLG_COOBRIGADO: TStringField;
    RXDIAS_AVISTA: TIntegerField;
    RXCARTORIO: TIntegerField;
    RXPOSTECIPADO: TStringField;
    sDBCheckBox1: TsDBCheckBox;
    procedure btArquivoClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure dsRXDataChange(Sender: TObject; Field: TField);
    procedure btProcessarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GridAtosExit(Sender: TObject);
    procedure MarcarTodosClick(Sender: TObject);
    procedure DesmarcarTodosClick(Sender: TObject);
    procedure btRejeitarClick(Sender: TObject);
    procedure AtribuirProtocolos;
    procedure edProtocoloExit(Sender: TObject);
    procedure btCriticasClick(Sender: TObject);
    procedure GravarOrdem;
    procedure RXENFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure lbAvisoClick(Sender: TObject);
    procedure ChecarIrregularidade;
    procedure GridAtosMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure sPanel2MouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure PMotivoMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure GridAtosMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure VerificaMarcados(Check: Boolean; Num_Titulo, Nosso_Numero, Sequencial: String);

    function QtdMarcados: Integer;
    function  Maximo: Integer;
    function Texto(Linha,Inicio,Fim: Integer): String;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    lRejeitar: Boolean;

    LstArqB: TListBox;

    sNumTitulo: String;

    EMOLUMENTOS,
    FETJ,
    FUNDPERJ,
    FUNPERJ,
    FUNARPEN,
    PMCMV,
    ISS,
    MUTUA,
    ACOTERJ,
    DISTRIBUICAO,
    TOTAL: Double;
  public
    { Public declarations }
    Arquivo: String;
    vData: String; {DATA DOS TITULOS NO ARQUIVO DO GERENCIADO FINANCEIRO}
  end;

var
  FImportarTxt: TFImportarTxt;

implementation

uses UDM, UPF, UPortadores, UIrregularidades, UCriticas, UQuickOrdem1,
  UPrincipal, UGeral, UValores, UGDM;

{$R *.dfm}

procedure TFImportarTxt.btArquivoClick(Sender: TObject);
var
  I: Integer;
begin
  RX.Close;

  dm.iTotalTitulos     := 0;
  dm.iTotalBancos      := 0;
  dm.cTotalEmolumentos := 0;

  dm.NomeArqCRA := '';
  dm.ExtArqCRA  := '';

  opd.InitialDir := dm.ValorParametro(2);

  if opd.Execute then
  begin
    Arquivo := ExtractFileName(opd.FileName);
    Arquivo[1] := 'C';
    RE.Lines.LoadFromFile(opd.FileName);
  end
  else
    Exit;

  dm.ServentiaAgregada.Close;
  dm.ServentiaAgregada.Open;
  dm.ServentiaAgregada.Filtered := False;
  dm.ServentiaAgregada.Filter   := 'FLG_ATIVA = ' + QuotedStr('S');
  dm.ServentiaAgregada.Filtered := True;

  if dm.ServentiaAgregada.IsEmpty then
  begin
    GR.Aviso('FALTA CADASTRAR AS SERVENTIAS AGREGADAS!');
    Exit;
  end;

  dm.Importados.Close;
  dm.Importados.Params[0].AsString := Copy(ExtractFileName(opd.FileName), 1, 15);
  dm.Importados.Open;

  if not dm.Importados.IsEmpty then
  begin
    GR.Aviso('ARQUIVO J� IMPORTADO!');
    Exit;
  end
  else
  begin
    dm.NomeArqCRA := Copy(ExtractFileName(opd.FileName), 1, (Length(ExtractFileName(opd.FileName)) - (Length(ExtractFileExt(opd.FileName))) ));
    dm.ExtArqCRA  := Copy(ExtractFileExt(opd.FileName), 2, (Length(ExtractFileExt(opd.FileName)) - 1));
  end;

  Portadores.Close;
  Portadores.Open;
  for I:=0 to RE.Lines.Count-1 do
  begin
    if (Copy(RE.Lines[I],1,1)='0') then
    begin
      if Texto(I,2,3)='TAB' then
      begin
        Portadores.Append;
        PortadoresCodigo.AsString   :='TAB';
        PortadoresNome.AsString     :='APONTAMENTO CENPROT';
        PortadoresAgencia.AsString  :='';
        PortadoresPraca.AsString    :='';
        Portadores.Post;

        //vImportacaoCENPROT:=True;
      end
      else
      begin
        Portadores.Append;
        PortadoresCodigo.AsString   :=Texto(I,2,3);
        PortadoresNome.AsString     :=Trim(Copy(RE.Lines[I],5,40));
        PortadoresAgencia.AsString  :=(Texto(I,84,6));
        PortadoresPraca.AsString    :=(Texto(I,93,7));
        Portadores.Post;

        //vImportacaoCENPROT:=False;
      end;
    end;
  end;

  {for I := 0 to RE.Lines.Count - 1 do
  begin
    if Copy(RE.Lines[I], 1, 1) = '0' then
    begin
      //Portadores
      Portadores.Append;
      PortadoresCodigo.AsString  := Texto(I, 2, 3);
      PortadoresNome.AsString    := Trim(Copy(RE.Lines[I], 5, 40));
      PortadoresAgencia.AsString := (Texto(I, 84, 6));
      PortadoresPraca.AsString   := (Texto(I, 93, 7));
      Portadores.Post;
    end;
  end;}

  RXBC.Close;
  RXBC.Open;
  RXBC.Append;
  RXEN.Close;
  RXEN.Open;

  RX.DisableControls;
  RX.Close;
  RX.Open;

  Entrada.Close;
  Entrada.Open;

  dm.Criticas.Close;
  dm.Criticas.Open;

  dm.mdArquivoD.Close;
  dm.mdArquivoD.Open;

  lRejeitar  := False;
  sNumTitulo := '';

  for I:=0 to RE.Lines.Count-1 do
  begin
    Application.ProcessMessages;
    PF.Aguarde(True,'Aguarde: '+IntToStr(I)+'/'+IntToStr(RE.Lines.Count));

    if I = 0 then
      vData:=Texto(I,45,2)+'/'+Texto(I,47,2)+'/'+Texto(I,49,4);

(*      if Copy(RE.Lines[I], 1, 1) = '0' then
    begin
      {ARQUIVO D - Header}
      Inc(dm.iTotalBancos);

      dm.mdLinhaArqD.Append;
      dm.mdLinhaArqDID_REGISTRO.AsInteger     := StrToInt(Texto(I, 1, 1));
      dm.mdLinhaArqDNUM_COD_PORTADOR.AsString := GR.Zeros(Texto(I, 2, 3), 'D', 3);
      dm.mdLinhaArqDNOME_PORTADOR.AsString    := PF.Espaco(Texto(I, 5, 40), 'E', 40);
      dm.mdLinhaArqDDATA_MOVIMENTO.AsString   := GR.Zeros(Texto(I, 45, 8), 'D', 8);
      dm.mdLinhaArqDID_TRANS_REMET.AsString   := PF.Espaco(Texto(I, 53, 3), 'E', 3);
      dm.mdLinhaArqDID_TRANS_DESTIN.AsString  := PF.Espaco(Texto(I, 56, 3), 'E', 3);
      dm.mdLinhaArqDID_TRANS_TIPO.AsString    := PF.Espaco(Texto(I, 59, 3), 'E', 3);
      dm.mdLinhaArqDID_AGENCIA.AsString       := GR.Zeros(Texto(I, 84, 6), 'D', 6);
      dm.mdLinhaArqDVERSAO_LAYOUT.AsString    := GR.Zeros(Texto(I, 90, 3), 'D', 3);
      dm.mdLinhaArqDCOD_MUNIC_PRACA.AsString  := GR.Zeros(Texto(I, 93, 7), 'D', 7);
      dm.mdLinhaArqDCOMPL_REGISTRO.AsString   := PF.Espaco(Texto(I, 100, 497), 'E', 497);
      dm.mdLinhaArqDSEQ_BANCO.AsInteger       := dm.iTotalBancos;
      dm.mdLinhaArqD.Post;
    end;  *)

    Entrada.Append;
    EntradaLinha.AsString:=RE.Lines[I];
    Entrada.Post;

    if (I<>0) and (I<>RE.Lines.Count-1) and (Copy(RE.Lines[I],1,1)<>'0') and (Copy(RE.Lines[I],1,1)<>'9') then
    begin
      RX.Append;

      Portadores.Locate('Codigo',Texto(I,2,3),[]);

      {NOME PORTADOR}     RXAPRESENTANTE.AsString         := PortadoresNome.AsString;
      {C�DIGO PORTADOR}   RXCODIGO_APRESENTANTE.AsString  := Texto(I,2,3);
      {AG�NCIA CEDENTE}   RXAGENCIA.AsString              := Texto(I,5,15);
      {NOME CEDENTE}      RXCEDENTE.AsString              := Texto(I,20,45);
      {NOME SACADOR}      RXSACADOR.AsString              := Texto(I,65,45);
      {DOC. SACADOR}      RXCPF_CNPJ_SACADOR.AsString     := Texto(I,110,14);
      {END. SACADOR}      RXEND_SACADOR.AsString          := Texto(I,124,45);
      {CEP SACADOR}       RXCEP_SACADOR.AsString          := Texto(I,169,8);
      {CIDADE SACADOR}    RXCID_SACADOR.AsString          := Texto(I,177,20);
      {UF SACADOR}        RXUF_SACADOR.AsString           := Texto(I,197,2);
      {NOSSO NUMERO}      RXNOSSO_NUMERO.AsString         := Texto(I,199,15);
      {ESP�CIE}           RXTIPO_TITULO.AsInteger         := PF.RetornarEspecie(Texto(I,214,3));
      {N�M.T�TULO}        RXNUMERO_TITULO.AsString        := Trim(Copy(RE.Lines[I],217,11));

      {DT.T�TULO}         if (Texto(I,228,2) <> '00') and
                            (Texto(I,228,2) <> '99') then
                            RXDT_TITULO.AsDateTime := StrToDate(Texto(I,228,2) + '/' + Texto(I,230,2) + '/' + Texto(I,232,4));

      {DT.VENCIMENTO}     if (Texto(I,236,2) <> '00') and
                            (Texto(I,236,2) <> '99') and
                            (Texto(I,236,2) <> '') then
                            RXDT_VENCIMENTO.AsDateTime := StrToDate(Texto(I,236,2) + '/' + Texto(I,238,2) + '/' + Texto(I,240,4));

      {VALOR T�TULO}      if Texto(I,247,14) <> ''    then
                            RXVALOR_TITULO.AsFloat := PF.TextoParaFloat(Texto(I,247,14));

      {SALDO T�TULO}      if Texto(I,261,14) <> ''    then
                            RXSALDO_TITULO.AsFloat := PF.TextoParaFloat(Texto(I,261,14));

      {PRA�A}             RXPRACA.AsString                := Texto(I,275,20);

      {TIPO ENDOSSO}      if Texto(I,295,1) = ''      then
                            RXENDOSSO.AsString := 'S'
                          else
                            RXENDOSSO.AsString := Texto(I,295,1);

      {ACEITE}            RXACEITE.AsString               := Texto(I,296,1);
      {Pedro - 18/04/2016}
      {NUM DEVEDORES}     RXNUMDEVEDOR.AsString           := Texto(I,297,1);
          
      if (not RXNUMDEVEDOR.IsNull) and
        (RXNUMDEVEDOR.AsString <> '') then
        RXFLG_COOBRIGADO.AsString := IfThen((RXNUMDEVEDOR.AsInteger > 1), 'S', '');

      {DEVEDOR}           RXDEVEDOR.AsString              := Texto(I,298,45);

      {TIPO DEVEDOR}      if Texto(I, 343, 3) = '001' then
                            RXTIPO_DEVEDOR.AsString := 'J'
                          else
                            RXTIPO_DEVEDOR.AsString := 'F';

      {DOC. DEVEDOR}      if RXTIPO_DEVEDOR.AsString = 'J' then
                            RXCPF_CNPJ_DEVEDOR.AsString := Texto(I, 346, 14)
                          else
                            RXCPF_CNPJ_DEVEDOR.AsString := Texto(I, 349, 11);

      {RG DEVEDOR}        if Texto(I,360,11)<>'00000000000' then RXRG_DEVEDOR.AsString:=Texto(I,360,11);

      {END. DEVEDOR}      RXEND_DEVEDOR.AsString            :=Texto(I,371,45);
      {CEP DEVEDOR}       RXCEP_DEVEDOR.AsString            :=Texto(I,416,8);
      {CIDADE DEVEDOR}    RXCID_DEVEDOR.AsString            :=Texto(I,424,20);
      {UF}                RXUF.AsString                     :=Texto(I,444,2);
      {LETRA}             RXLETRA.AsString                  :=Texto(I,477,1);
      {BAIRRO DEVEDOR}    RXBAIRRO_DEVEDOR.AsString         :=Texto(I,488,20);
      {STATUS}            RXSTATUS.AsString                 :='Aceito';
      {DESCRI��O}         RXEspecie.AsString                :=PF.RetornarTitulo(RXTIPO_TITULO.AsInteger);
      {FINS FALIMENTARES} RXFINS.AsString                   :=IfThen(Texto(I,566,1)='','N','S');
      {POSTECIPADO}       RXPOSTECIPADO.AsString            :=Texto(I,567,1);
      if Length(RXCPF_CNPJ_DEVEDOR.AsString) > 11 then
        RXDocumento.AsString := PF.FormatarCNPJ(RXCPF_CNPJ_DEVEDOR.AsString)
      else
        RXDocumento.AsString := PF.FormatarCPF(RXCPF_CNPJ_DEVEDOR.AsString);

      if PF.RetornarEspecie(Texto(I, 214, 3)) in [19, 20, 21, 22, 23, 24, 27] then  //Protestos de Letras de Cambio ou Duplicatas
      begin
        if Texto(I, 236, 8) = '99999999' then
        begin
          RXAVISTA.AsString       := 'S';
          RXDIAS_AVISTA.AsInteger := 99
        end
        else if Texto(I, 236, 8) = '99990001' then
        begin
          RXAVISTA.AsString       := 'S';
          RXDIAS_AVISTA.AsInteger := 1
        end
        else if Texto(I, 236, 8) = '99990030' then
        begin
          RXAVISTA.AsString       := 'S';
          RXDIAS_AVISTA.AsInteger := 30
        end;
      end
      else
      begin
        if Texto(I, 236, 8) = '99999999' then  //A Vista
        begin
          RXAVISTA.AsString       := 'S';
          RXDIAS_AVISTA.AsInteger := 0;
        end
        else
          RXAVISTA.AsString := 'N';
      end;

//      if RXCODIGO_APRESENTANTE.AsString = '582' then
//        RXNUMERO_TITULO.AsString := RXNOSSO_NUMERO.AsString;

      RXNUMERO_TITULO.AsString := Trim(Copy(RE.Lines[I],217,11));

      {SEQUENCIAL DO ARQUIVO} RXSEQUENCIAL_ARQ.AsString     := GR.Zeros(Texto(I, 597, 4), 'D', 4);
      {SEQUENCIAL DO BANCO}   RXSEQ_BANCO.AsInteger         := dm.iTotalBancos;

      if sNumTitulo <> RXNUMERO_TITULO.AsString then
      begin
        sNumTitulo := RXNUMERO_TITULO.AsString;
        lRejeitar  := False;
      end
      else
      begin
        if RXNUMDEVEDOR.AsString = '1' then
          lRejeitar  := False;
      end;

      RXCheck.AsBoolean:=True;
      ChecarIrregularidade;
      RX.Post;

      VerificaMarcados(not lRejeitar, RXNUMERO_TITULO.AsString, RXNOSSO_NUMERO.AsString, RXSEQUENCIAL_ARQ.AsString);

      {PREENCHENDO O MEMORY DATA DO ARQUIVO D - Detail}
      dm.mdArquivoD.Append;
      dm.mdArquivoDARQUIVO_CRA.AsString          := ExtractFileName(opd.FileName);
      dm.mdArquivoDID_REGISTRO.AsString          := Texto(I, 1, 1);

      dm.mdArquivoDNUM_COD_PORTADOR.AsString     := GR.Zeros(Texto(I, 2,   3),   'D', 3);
      dm.mdArquivoDNOME_PORTADOR.AsString        := PortadoresNome.AsString;

      dm.mdArquivoDCOD_CEDENTE.AsString          := GR.Zeros(Texto(I, 5,   15),  'D', 15);
      dm.mdArquivoDNOME_CEDENTE.AsString         := PF.Espaco(Texto(I, 20,  45), 'E', 45);

      dm.mdArquivoDNOME_SACADOR.AsString         := PF.Espaco(Texto(I, 65,  45), 'E', 45);
      dm.mdArquivoDNUM_DOC_SACADOR.AsString      := GR.Zeros(Texto(I, 110, 14),  'D', 14);
      dm.mdArquivoDEND_SACADOR.AsString          := PF.Espaco(Texto(I, 124, 45), 'E', 45);
      dm.mdArquivoDCEP_SACADOR.AsString          := GR.Zeros(Texto(I, 169, 8),   'D', 8);
      dm.mdArquivoDCID_SACADOR.AsString          := PF.Espaco(Texto(I, 177, 20), 'E', 20);
      dm.mdArquivoDUF_SACADOR.AsString           := PF.Espaco(Texto(I, 197, 2),  'E', 2);

      dm.mdArquivoDNOSSO_NUMERO.AsString         := Texto(I,199,15);  //GR.Zeros(Texto(I, 199, 15),  'D', 15);
      dm.mdArquivoDESPECIE.AsString              := PF.Espaco(Texto(I, 214, 3),  'E', 3);

//      if Texto(I,2,3) = '582' then
//        dm.mdArquivoDNUM_TITULO.AsString         := Texto(I,199,15)
//      else
        dm.mdArquivoDNUM_TITULO.AsString         := Trim(Copy(RE.Lines[I],217,11));  //PF.Espaco(Texto(I, 217, 11), 'E', 11);

      dm.mdArquivoDDATA_EMISSAO.AsString         := GR.Zeros(Texto(I, 228, 8),   'D', 8);
      dm.mdArquivoDDATA_VENCIMENTO.AsString      := GR.Zeros(Texto(I, 236, 8),   'D', 8);
      dm.mdArquivoDTIPO_MOEDA.AsString           := GR.Zeros(Texto(I, 244, 3),   'D', 3);
      dm.mdArquivoDVALOR_TITULO.AsString         := GR.Zeros(Texto(I, 247, 14),  'D', 14);
      dm.mdArquivoDSALDO_TITULO.AsString         := GR.Zeros(Texto(I, 261, 14),  'D', 14);
      dm.mdArquivoDPRACA_PROTESTO.AsString       := PF.Espaco(Texto(I, 275, 20), 'E', 20);
      dm.mdArquivoDTIPO_ENDOSSO.AsString         := PF.Espaco(Texto(I, 295, 1),  'E', 1);
      dm.mdArquivoDINFO_ACEITE.AsString          := PF.Espaco(Texto(I, 296, 1),  'E', 1);

      dm.mdArquivoDNUM_CONTROLE_DEV.AsString     := GR.Zeros(Texto(I, 297, 1),   'D', 1);
      dm.mdArquivoDNOME_DEVEDOR.AsString         := PF.Espaco(Texto(I, 298, 45), 'E', 45);
      dm.mdArquivoDTIPO_ID_DEVEDOR.AsString      := GR.Zeros(Texto(I, 343, 3),   'D', 3);
      dm.mdArquivoDNUM_ID_DEVEDOR.AsString       := GR.Zeros(Texto(I, 346, 14),  'D', 14);
      dm.mdArquivoDNUM_DOC_DEVEDOR.AsString      := GR.Zeros(Texto(I, 360, 11),  'D', 11);
      dm.mdArquivoDEND_DEVEDOR.AsString          := PF.Espaco(Texto(I, 371, 45), 'E', 45);
      dm.mdArquivoDCEP_DEVEDOR.AsString          := GR.Zeros(Texto(I, 416, 8),   'D', 8);
      dm.mdArquivoDCID_DEVEDOR.AsString          := PF.Espaco(Texto(I, 424, 20), 'E', 20);
      dm.mdArquivoDUF_DEVEDOR.AsString           := PF.Espaco(Texto(I, 444, 2),  'E', 2);

      dm.mdArquivoDTIPO_OCORRENCIA.AsString      := PF.Espaco(' ',  'E', 1);
      dm.mdArquivoDDATA_PROTOCOLO.AsString       := GR.Zeros('0',  'D', 8);;
      dm.mdArquivoDVALOR_CUSTAS_CART.AsString    := GR.Zeros(Texto(I, 467, 10),  'D', 10);
      dm.mdArquivoDDECLARACAO_PORT.AsString      := PF.Espaco(Texto(I, 477, 1),  'E', 1);
      dm.mdArquivoDDATA_OCORRENCIA.AsString      := GR.Zeros(Texto(I, 478, 8),   'D', 8);
      dm.mdArquivoDCOD_IRREGULARIDADE.AsString   := PF.Espaco(Texto(I, 486, 2),  'E', 2);

      dm.mdArquivoDBAIRRO_DEVEDOR.AsString       := PF.Espaco(Texto(I, 488, 20), 'E', 20);

      dm.mdArquivoDVALOR_CUSTAS_DISTRIB.AsString := GR.Zeros(Texto(I, 508, 10),  'D', 10);
      dm.mdArquivoDREG_DISTRIBUICAO.AsString     := GR.Zeros(Texto(I, 518, 6),   'D', 6);
      dm.mdArquivoDVALOR_GRAVACAO.AsString       := GR.Zeros(Texto(I, 524, 10),  'D', 10);
      dm.mdArquivoDNUM_OPERACAO.AsString         := GR.Zeros(Texto(I, 534, 5),   'D', 5);
      dm.mdArquivoDNUM_CONTRATO.AsString         := GR.Zeros(Texto(I, 539, 15),  'D', 15);
      dm.mdArquivoDNUM_PARCELA.AsString          := GR.Zeros(Texto(I, 554, 3),   'D', 3);
      dm.mdArquivoDTIPO_LETRA_CAMBIO.AsString    := PF.Espaco(Texto(I, 557, 1),  'E', 1);
      dm.mdArquivoDCOMPL_COD_IRREG.AsString      := PF.Espaco(Texto(I, 558, 8),  'E', 8);
      dm.mdArquivoDPROTESTO_FALENCIA.AsString    := PF.Espaco(Texto(I, 566, 1),  'E', 1);
      dm.mdArquivoDINSTRUMENTO_PROT.AsString     := PF.Espaco(Texto(I, 567, 1),  'E', 1);
      dm.mdArquivoDVALOR_DDESPESAS.AsString      := GR.Zeros(Texto(I, 568, 10),  'D', 10);
      dm.mdArquivoDCOMPL_REGISTRO.AsString       := PF.Espaco(Texto(I, 578, 19), 'E', 19);
      dm.mdArquivoDSEQUENCIAL_REG.AsString       := GR.Zeros(Texto(I, 597, 4),   'D', 4);
      dm.mdArquivoDSEQ_BANCO.AsInteger           := dm.iTotalBancos;
      dm.mdArquivoD.Post;

      {ATUALIZANDO TABELA DE PORTADORES}
      dm.Portadores.Close;
      dm.Portadores.CommandText:='SELECT * FROM PORTADORES WHERE CODIGO = :CODIGO';
      dm.Portadores.Params.ParamByName('CODIGO').AsString := RXCODIGO_APRESENTANTE.AsString;
      dm.Portadores.Open;

      if dm.Portadores.IsEmpty then
      begin
        dm.Portadores.Append;
        dm.PortadoresCODIGO.AsString    :=RXCODIGO_APRESENTANTE.AsString;
        dm.PortadoresNOME.AsString      :=GR.iif(RXCODIGO_APRESENTANTE.AsString='TAB','APONTAMENTO CENPROT',RXAPRESENTANTE.AsString);
        dm.PortadoresTIPO.AsString      :='J';
        dm.PortadoresBANCO.AsString     :='S';
        dm.PortadoresCONVENIO.AsString  :=UpperCase(InputBox(dm.PortadoresNOME.AsString,'Conv�nio? (S/N)','N'));
        dm.PortadoresAGENCIA.AsString   :=PortadoresAgencia.AsString;
        dm.PortadoresPRACA.AsString     :=PortadoresPraca.AsString;
        dm.PortadoresCRA.AsString       :='S';
      end
      else
      begin
        dm.Portadores.Edit;
        dm.PortadoresAGENCIA.AsString :=(Texto(0,84,6));
        dm.PortadoresAGENCIA.AsString :=PortadoresAgencia.AsString;
        dm.PortadoresPRACA.AsString   :=PortadoresPraca.AsString;
        dm.PortadoresCRA.AsString     :='S';
      end;

      dm.Portadores.Post;
      dm.Portadores.ApplyUpdates(0);
    end;

    {AJUSTE CRA}
    RXEN.Append;
    RXENCodigo.AsString :=Texto(I,2,3);
    RXENLinha.Value     :=RE.Lines[I];
    RXEN.Post;

    RXBCCodigo.AsString :=Texto(I,2,3);
    RXBCImportar.Value  :=RXBCImportar.Value+#13+RE.Lines[I];

    if Copy(RE.Lines[I], 1, 1) = '9' then
    begin
      RXBCImportar.Value := Trim(RXBCImportar.Value);
      RXBC.Post;
      RXBC.Append;

(*        {ARQUIVO D - Trailer}
      dm.mdLinhaArqD.Append;
      dm.mdLinhaArqDID_REGISTRO.AsInteger     := StrToInt(Texto(I, 1, 1));
      dm.mdLinhaArqDNUM_COD_PORTADOR.AsString := GR.Zeros(Texto(I, 2, 3), 'D', 3);
      dm.mdLinhaArqDNOME_PORTADOR.AsString    := PF.Espaco(Texto(I, 5, 40), 'E', 40);
      dm.mdLinhaArqDDATA_MOVIMENTO.AsString   := GR.Zeros(Texto(I, 45, 8), 'D', 8);
      dm.mdLinhaArqDCOMPL_REGISTRO.AsString   := PF.Espaco(Texto(I, 76, 521), 'E', 521);
      dm.mdLinhaArqDSEQ_BANCO.AsInteger       := dm.iTotalBancos;
      dm.mdLinhaArqD.Post;  *)
    end;
  end;

  if dm.ServentiaCODIGO.AsInteger=1166 then MarcarTodos.Click;

  RX.First;
  RX.EnableControls;

  Self.Caption:='Importa��o: '+IntToStr(RX.RecordCount)+' T�tulos - Data do Arquivo: '+Texto(0,45,2)+'/'+Texto(0,47,2)+'/'+Texto(0,49,4);

  AtribuirProtocolos;

  PF.Aguarde(False);

  dm.Criticas.First;

  if not dm.Criticas.IsEmpty then
    GR.CriarForm(TFCriticas,FCriticas);
end;

procedure TFImportarTxt.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

function TFImportarTxt.Texto(Linha, Inicio, Fim: Integer): String;
begin
  Result:=Trim(Copy(RE.Lines[Linha],Inicio,Fim));
end;

procedure TFImportarTxt.dsRXDataChange(Sender: TObject; Field: TField);
begin
  if RX.IsEmpty then
    btProcessar.Enabled:=False
      else btProcessar.Enabled:=True;
  RXEN.Filtered:=False;
  RXEN.Filtered:=True;

  if RXCheck.AsBoolean then
    lbIrreguralidade.Caption:='T�TULO SEM IRREGULARIDADE'
      else lbIrreguralidade.Caption:='Irregularidade:';
end;

procedure TFImportarTxt.btProcessarClick(Sender: TObject);
var
  Posicao, Marcados, vIdReservado,
  vQtd, vProtocoloAtual, IdAto,
  IdImp, IdExp, IdCustas, IdDev, IdPort, IdSac, IdPraca: Integer;
  sValor, sMsgErro, sMsgRet: String;
  lErro: Boolean;
begin
  dm.Importados.Close;
  dm.Importados.Params[0].AsString := Copy(ExtractFileName(opd.FileName), 1, 15);
  dm.Importados.Open;

  if not dm.Importados.IsEmpty then
  begin
    GR.Aviso('ARQUIVO J� IMPORTADO!');
    Exit;
  end;

  sMsgErro := '';
  sValor   := '';

  IdAto    := 0;
  IdImp    := 0;
  IdExp    := 0;
  IdCustas := 0;
  IdDev    := 0;
  IdPort   := 0;
  IdSac    := 0;
  IdPraca  := 0;

  vQtd := QtdMarcados;

  RX.DisableControls;

  Posicao := RX.RecNo;
  Marcados := 0;

  RX.First;

  EMOLUMENTOS  := 0;
  FETJ         := 0;
  FUNDPERJ     := 0;
  FUNPERJ      := 0;
  FUNARPEN     := 0;
  PMCMV        := 0;
  ISS          := 0;
  MUTUA        := 0;
  ACOTERJ      := 0;
  DISTRIBUICAO := 0;
  TOTAL        := 0;
  
  vProtocoloAtual := 0;

  while not RX.Eof do
  begin
    if RXCheck.AsBoolean then
      Inc(Marcados);

    if (not RXCheck.AsBoolean) and (RXIrregularidade.AsString='') then
    begin
      RX.EnableControls;
      GR.Aviso('Informe a irregularidade do t�tulo!');
      Exit;
    end;

    qryConsultaProtocolo.Close;
    qryConsultaProtocolo.Params[0].AsInteger := RXProtocolo.AsInteger;
    qryConsultaProtocolo.Params[1].AsDate    := edData.Date;
    qryConsultaProtocolo.Open;

    if not qryConsultaProtocolo.IsEmpty then
    begin
      RX.EnableControls;
      GR.Aviso('Protocolo ' + RXProtocolo.AsString + ' j� existe!');
      Exit;
    end;

    RX.Next;
  end;
  
  RX.RecNo := Posicao;
  RX.EnableControls;

  if not GR.Pergunta('Confirma��o:' + #13#13 + 'Total de T�tulos: ' + IntToStr(RX.RecordCount) + #13#13 +
                     'T�tulos Aceitos: ' + IntToStr(Marcados) + #13#13 + 'T�tulos Rejeitados: ' + IntToStr(RX.RecordCount - Marcados)+
                     #13#13 + 'Processar') then
    Exit;

  try
    IdAto    := dm.ProximoId('ID_ATO',
                              'IDS',
                              1,
                              False);

    IdImp    := dm.ProximoId('ID_IMPORTADO',
                             'IDS',
                             1,
                             False);
    IdExp    := dm.ProximoId('ID_EXPORTADO',
                             'IDS',
                             1,
                             False);
    IdCustas := dm.ProximoId('ID_CUSTA',
                             'IDS',
                             1,
                             False);
    IdDev    := dm.ProximoId('ID_DEVEDOR',
                             'IDS',
                             1,
                             False);
    IdPort   := dm.ProximoId('ID_PORTADOR',
                             'IDS',
                             1,
                             False);
    IdSac    := dm.ProximoId('ID_SACADOR',
                             'IDS',
                             1,
                             False);
    IdPraca  := dm.ProximoId('ID_PRACA',
                             'IDS',
                             1,
                             False);

    if dm.conSISTEMA.Connected then
      dm.conSISTEMA.StartTransaction;

    PF.Aguarde(True);

    Inc(IdImp);

    dm.Importados.Close;
    dm.Importados.Params[0].AsString := '*#';
    dm.Importados.Open;
    dm.Importados.Append;
    dm.ImportadosID_IMPORTADO.AsInteger := IdImp;
    dm.ImportadosARQUIVO.AsString       := ExtractFileName(opd.FileName);
    dm.ImportadosDATA.AsDateTime        := Now;
    dm.ImportadosTITULOS.AsInteger      := RX.RecordCount;
    dm.ImportadosACEITOS.AsInteger      := Marcados;
    dm.ImportadosREJEITADOS.AsInteger   := Rx.RecordCount-Marcados;
    dm.Importados.Post;

    sMsgErro := 'ApplyUpdates dm.Importado';

    GR.ExecutarSQLDAC('UPDATE IDS ' +
                     '   SET VALOR = ' + IntToStr(IdImp) +
                     ' WHERE CAMPO = ' + QuotedStr('ID_IMPORTADO'), dm.conSISTEMA);

    dm.Importados.ApplyUpdates(0);

    sMsgErro := '';

    RX.DisableControls;
    Posicao:=RX.RecNo;
    RX.First;

    dm.Titulos.Close;
    dm.Titulos.Open;

    dm.Custas.Close;
    dm.Custas.Open;

    dm.Devedores.Close;
    dm.Devedores.Params[0].AsInteger := -1;
    dm.Devedores.Open;

    dm.Sacadores.Close;
    dm.Sacadores.Params[0].AsInteger := -1;
    dm.Sacadores.Open;

    Application.CreateForm(TFQuickOrdem1, FQuickOrdem1);
    FQuickOrdem1.RXOrdem.Close;
    FQuickOrdem1.RXOrdem.Open;

    PB.Max := RX.RecordCount;
    PB.Position := 0;
    PB.Visible := True;

    RXFAKE.Close;
    RXFAKE.Open;
    RXFAKE.CopyDataSet(RX,[coRestart,coAppend]);

    Rx.Filtered:=False;
    RX.Filter:='NUMDEVEDOR=1';
    Rx.Filtered:=True;

    RX.First;

    while not RX.Eof do
    begin
      Application.ProcessMessages;

      PF.Aguarde(True, 'Aguarde: ' + IntToStr(RX.RecNo) + '/' + IntToStr(RX.RecordCount));

      PB.Position := PB.Position + 1;

      vProtocoloAtual := RXProtocolo.AsInteger;

      if RXSTATUS.AsString = 'Aceito' then
      begin
        dm.Portadores.Close;
        dm.Portadores.CommandText := 'SELECT * FROM PORTADORES WHERE CODIGO = ' + QuotedStr(RXCODIGO_APRESENTANTE.AsString);
        dm.Portadores.Open;

        if Trim(dm.PortadoresCONVENIO.AsString) = '' then
        begin
          dm.Portadores.Edit;

          if GR.Pergunta('O Portador "'+dm.TitulosAPRESENTANTE.AsString+'" � conv�nio') then
            dm.PortadoresCONVENIO.AsString := 'S'
          else
            dm.PortadoresCONVENIO.AsString := 'N';

          dm.Portadores.Post;
          dm.Portadores.ApplyUpdates(0);
        end;

        dm.Portadores.Close;
        dm.Portadores.CommandText := 'SELECT * FROM PORTADORES WHERE CODIGO = ' + QuotedStr(RXCODIGO_APRESENTANTE.AsString);
        dm.Portadores.Open;

        qryFaixa.Close;
        qryFaixa.ParamByName('C').AsString       := dm.PortadoresCONVENIO.AsString;
        qryFaixa.ParamByName('OCULTO1').AsString := 'N';
        qryFaixa.ParamByName('OCULTO2').AsString := 'N';
        qryFaixa.ParamByName('VALOR1').AsFloat   := RXSALDO_TITULO.AsFloat;
        qryFaixa.ParamByName('VALOR2').AsFloat   := RXSALDO_TITULO.AsFloat;
        qryFaixa.Open;

        dm.Itens.Close;
        dm.Itens.Params[0].AsInteger := qryFaixaCOD.AsInteger;
        dm.Itens.Open;
        dm.Itens.First;

        Inc(IdAto);

        dm.Titulos.Append;

        dm.GerarValor('NPR');

        dm.TitulosID_ATO.AsInteger               := IdAto;
        dm.TitulosDT_PROTOCOLO.AsDateTime        := edData.Date;
        dm.TitulosDT_ENTRADA.AsDateTime          := edData.Date;
        dm.TitulosDT_PRAZO.AsDateTime            := PF.DataFinalFeriados(dm.TitulosDT_PROTOCOLO.AsDateTime, StrToInt(dm.ValorParametro(5)));
        dm.TitulosPROTOCOLO.AsInteger            := RXProtocolo.AsInteger;
        dm.TitulosTIPO_PROTESTO.AsInteger        := 1;
        dm.TitulosTIPO_TITULO.AsInteger          := RXTIPO_TITULO.AsInteger;
        dm.TitulosNUMERO_TITULO.AsString         := RXNUMERO_TITULO.AsString;
        dm.TitulosDT_TITULO.AsDateTime           := RXDT_TITULO.AsDateTime;
        dm.TitulosVALOR_TITULO.AsFloat           := RXVALOR_TITULO.AsFloat;

        if RXDT_VENCIMENTO.AsDateTime <> 0 then
          dm.TitulosDT_VENCIMENTO.AsDateTime     := RXDT_VENCIMENTO.AsDateTime;

        dm.TitulosAVISTA.AsString                := RXAVISTA.AsString;

        if not RXDIAS_AVISTA.IsNull then
          dm.TitulosDIAS_AVISTA.AsInteger        := RXDIAS_AVISTA.AsInteger;

        dm.TitulosFINS_FALIMENTARES.AsString     := RXFINS.AsString;
        dm.TitulosCONVENIO.AsString              := dm.PortadoresCONVENIO.AsString;
        dm.TitulosAPRESENTANTE.AsString          := dm.PortadoresNOME.AsString;
        dm.TitulosCPF_CNPJ_APRESENTANTE.AsString := dm.PortadoresDOCUMENTO.AsString;
        dm.TitulosTIPO_APRESENTANTE.AsString     := dm.PortadoresTIPO.AsString;

        if dm.PortadoresCODIGO.AsString <> '' then
          dm.TitulosCODIGO_APRESENTANTE.AsString := dm.PortadoresCODIGO.AsString;

        dm.TitulosCEDENTE.AsString               := RXCEDENTE.AsString;
        dm.TitulosNOSSO_NUMERO.AsString          := RXNOSSO_NUMERO.AsString;
        dm.TitulosSACADOR.AsString               := RXSACADOR.AsString;
        dm.TitulosCPF_CNPJ_SACADOR.AsString      := RXCPF_CNPJ_SACADOR.AsString;
        dm.TitulosDEVEDOR.AsString               := RXDEVEDOR.AsString;
        dm.TitulosTIPO_DEVEDOR.AsString          := RXTIPO_DEVEDOR.AsString;
        dm.TitulosCPF_CNPJ_DEVEDOR.AsString      := RXCPF_CNPJ_DEVEDOR.AsString;
        dm.TitulosAGENCIA_CEDENTE.AsString       := RXAGENCIA.AsString;
        dm.TitulosPRACA_PROTESTO.AsString        := RXPRACA.AsString;
        dm.TitulosSALDO_TITULO.AsFloat           := RXSALDO_TITULO.AsFloat;
        dm.TitulosLETRA.AsString                 := RXLETRA.AsString;
        dm.TitulosTIPO_ENDOSSO.AsString          := RXENDOSSO.AsString;
        dm.TitulosACEITE.AsString                := RXACEITE.AsString;
        dm.TitulosSTATUS.AsString                := 'Aceito';  //APONTADO

        if dm.PortadoresCodigo.AsString<>'TAB' then
          dm.TitulosTIPO_APRESENTACAO.AsString     := 'I'
        else
          dm.TitulosTIPO_APRESENTACAO.AsString     := 'B';

        dm.TitulosCODIGO.AsInteger               := qryFaixaCOD.AsInteger;

        dm.TitulosMUTUA.AsFloat                  := StrToFloat(dm.ValorParametro(26)); //qryFaixaMUTUA.AsFloat;
        dm.TitulosACOTERJ.AsFloat                := StrToFloat(dm.ValorParametro(27)); //qryFaixaACOTERJ.AsFloat;
        dm.TitulosDISTRIBUICAO.AsFloat           := qryFaixaDISTRIB.AsFloat;
        dm.TitulosPOSTECIPADO.AsString           := RXPOSTECIPADO.AsString.Trim;

        if (dm.PortadoresCONVENIO.AsString = 'N') and
          (dm.ValorParametro(35) <> '') then
          dm.TitulosVALOR_AR.AsFloat             := StrToFloat(dm.ValorParametro(35));

        dm.TitulosCPF_ESCREVENTE.AsString        := dm.vCPF;
        dm.TitulosCOBRANCA.AsString              := 'CC';
        dm.TitulosENVIADO_PROTESTO.AsString      := 'N';
        dm.TitulosENVIADO_PAGAMENTO.AsString     := 'N';
        dm.TitulosENVIADO_APONTAMENTO.AsString   := 'N';
        dm.TitulosENVIADO_RETIRADO.AsString      := 'N';
        dm.TitulosENVIADO_SUSTADO.AsString       := 'N';
        dm.TitulosENVIADO_DEVOLVIDO.AsString     := 'N';
        dm.TitulosARQUIVO.AsString               := ExtractFileName(opd.FileName);

        dm.Titulos.Post;

        Inc(IdPraca);

        if (RXPRACA.AsString <> dm.ValorParametro(6)) and
           (not dm.Pracas.Locate('NOME', RXPRACA.AsString, [loPartialKey, loCaseInsensitive])) then
        begin
          if GR.Pergunta('Cadastrar Pra�a de Protesto?' + #13#13 + RXPRACA.AsString) then
          begin
            dm.Pracas.Append;
            dm.PracasID_PRACA.AsInteger := IdPraca;
            dm.PracasNOME.AsString      := RXPRACA.AsString;
            dm.Pracas.Post;

            GR.ExecutarSQLDAC('UPDATE IDS ' +
                             '   SET VALOR = ' + IntToStr(IdPraca) +
                             ' WHERE CAMPO = ' + QuotedStr('ID_PRACA'), dm.conSISTEMA);

            sMsgErro := 'ApplyUpdates dm.Pracas';

            dm.Pracas.ApplyUpdates(0);

            sMsgErro := '';
          end;
        end;

        while not dm.Itens.Eof do
        begin
          Inc(IdCustas);

          dm.Custas.Append;

          dm.CustasID_CUSTA.AsInteger := IdCustas;
          dm.CustasID_ATO.AsInteger   := IdAto;
          dm.CustasTABELA.AsString    := dm.ItensTAB.AsString;
          dm.CustasITEM.AsString      := dm.ItensITEM.AsString;
          dm.CustasSUBITEM.AsString   := dm.ItensSUB.AsString;
          dm.CustasVALOR.AsFloat      := dm.ItensVALOR.AsFloat;
          dm.CustasQTD.AsString       := dm.ItensQTD.AsString;
          dm.CustasTOTAL.AsFloat      := dm.CustasVALOR.AsFloat * dm.CustasQTD.AsFloat;

          dm.Custas.Post;

          GR.ExecutarSQLDAC('UPDATE IDS ' +
                           '   SET VALOR = ' + IntToStr(IdCustas) +
                           ' WHERE CAMPO = ' + QuotedStr('ID_CUSTA'), dm.conSISTEMA);

          sMsgErro := 'ApplyUpdates dm.Custas';

          dm.Custas.ApplyUpdates(0);

          sMsgErro := '';

          dm.Titulos.Edit;
          dm.TitulosEMOLUMENTOS.AsFloat := dm.TitulosEMOLUMENTOS.AsFloat + dm.CustasTOTAL.AsFloat;
          dm.TitulosFETJ.AsFloat           := GR.NoRound(dm.TitulosEMOLUMENTOS.AsFloat * 0.2, 2);
          dm.TitulosFUNDPERJ.AsFloat       := GR.NoRound(dm.TitulosEMOLUMENTOS.AsFloat * 0.05, 2);
          dm.TitulosFUNPERJ.AsFloat        := GR.NoRound(dm.TitulosEMOLUMENTOS.AsFloat * 0.05, 2);
          dm.TitulosFUNARPEN.AsFloat       := GR.NoRound(dm.TitulosEMOLUMENTOS.AsFloat * 0.04, 2);
          dm.TitulosPMCMV.AsFloat          := GR.NoRound(dm.TitulosEMOLUMENTOS.AsFloat * 0.02, 2);
          dm.TitulosISS.AsFloat            := GR.NoRound(dm.TitulosEMOLUMENTOS.AsFloat * Gdm.vAliquotaISS, 2);
          dm.TitulosTOTAL.AsFloat          := dm.TitulosEMOLUMENTOS.AsFloat +
                                              dm.TitulosFETJ.AsFloat +
                                              dm.TitulosFUNDPERJ.AsFloat +
                                              dm.TitulosFUNPERJ.AsFloat +
                                              dm.TitulosFUNARPEN.AsFloat +
                                              dm.TitulosPMCMV.AsFloat +
                                              dm.TitulosISS.AsFloat +
                                              dm.TitulosMUTUA.AsFloat +
                                              dm.TitulosACOTERJ.AsFloat +
                                              dm.TitulosDISTRIBUICAO.AsFloat +
                                              StrToFloat(dm.ValorParametro(35));
          dm.TitulosSALDO_PROTESTO.AsFloat := RXSALDO_TITULO.AsFloat + dm.TitulosTOTAL.AsFloat;
          dm.Titulos.Post;

          dm.Itens.Next;
        end;

        if dm.TitulosCONVENIO.AsString = 'N' then
        begin
          RX.Edit;

          if dm.ValorParametro(83) = 'S' then
            RXCUSTAS.AsFloat := dm.TitulosTOTAL.AsFloat
          else
            RXCUSTAS.AsFloat := dm.TitulosTOTAL.AsFloat - StrToFloat(dm.ValorParametro(35));

          RX.Post;
        end;

        RX.Edit;
        RXCONVENIO.AsBoolean := dm.TitulosCONVENIO.AsString = 'S';
        RX.Post;

        EMOLUMENTOS  := EMOLUMENTOS + dm.TitulosEMOLUMENTOS.AsFloat;
        FETJ         := FETJ + dm.TitulosFETJ.AsFloat;
        FUNDPERJ     := FUNDPERJ + dm.TitulosFUNDPERJ.AsFloat;
        FUNPERJ      := FUNPERJ + dm.TitulosFUNPERJ.AsFloat;
        FUNARPEN     := FUNARPEN + dm.TitulosFUNARPEN.AsFloat;
        PMCMV        := PMCMV + dm.TitulosPMCMV.AsFloat;
        ISS          := ISS + dm.TitulosISS.AsFloat;
        MUTUA        := MUTUA + dm.TitulosMUTUA.AsFloat;
        ACOTERJ      := ACOTERJ + dm.TitulosACOTERJ.AsFloat;
        DISTRIBUICAO := DISTRIBUICAO + dm.TitulosDISTRIBUICAO.AsFloat;
        TOTAL        := TOTAL + dm.TitulosTOTAL.AsFloat;

        sMsgErro := 'ApplyUpdates dm.Titulos';

        GR.ExecutarSQLDAC('UPDATE IDS ' +
                         '   SET VALOR = ' + IntToStr(IdAto) +
                         ' WHERE CAMPO = ' + QuotedStr('ID_ATO'), dm.conSISTEMA);

        dm.Titulos.ApplyUpdates(0);

        {GRAVANDO DEVEDORES}

        RXFAKE.Filtered:=False;
        RXFAKE.Filter:='PROTOCOLO='+RXProtocolo.AsString;
        RXFAKE.Filtered:=True;

        RXFAKE.First;
        while not RXFAKE.Eof do
        begin
            Inc(IdDev);

            dm.Devedores.Append;
            dm.DevedoresID_DEVEDOR.AsInteger      := IdDev;
            dm.DevedoresID_ATO.AsInteger          := IdAto;
            dm.DevedoresORDEM.AsInteger           := RXFAKENUMDEVEDOR.AsInteger;
            dm.DevedoresNOME.AsString             := RXFAKEDEVEDOR.AsString;
            dm.DevedoresTIPO.AsString             := RXFAKETIPO_DEVEDOR.AsString;
            dm.DevedoresDOCUMENTO.AsString        := RXFAKECPF_CNPJ_DEVEDOR.AsString;
            dm.DevedoresIFP_DETRAN.AsString       := 'O';
            dm.DevedoresIDENTIDADE.AsString       := RXFAKERG_DEVEDOR.AsString;
            dm.DevedoresORGAO.AsString            := '';
            dm.DevedoresCEP.AsString              := GR.PegarNumeroTexto(RXFAKECEP_DEVEDOR.AsString);
            dm.DevedoresENDERECO.AsString         := RXFAKEEND_DEVEDOR.AsString;
            dm.DevedoresBAIRRO.AsString           := RXFAKEBAIRRO_DEVEDOR.AsString;
            dm.DevedoresMUNICIPIO.AsString        := RXFAKECID_DEVEDOR.AsString;
            dm.DevedoresUF.AsString               := RXFAKEUF.AsString;
            dm.DevedoresJUSTIFICATIVA.AsString    := '';
            dm.DevedoresIGNORADO.AsString         := 'N';

            dm.Devedores.Post;

            sMsgErro := 'ApplyUpdates dm.Devedores';

            GR.ExecutarSQLDAC('UPDATE IDS ' +
                             '   SET VALOR = ' + IntToStr(IdDev) +
                             ' WHERE CAMPO = ' + QuotedStr('ID_DEVEDOR'), dm.conSISTEMA);

            dm.Devedores.ApplyUpdates(0);

            sMsgErro := '';

            {GRAVANDO CUSTAS NO ARQUIVO D}
            if dm.mdArquivoD.Locate('NOSSO_NUMERO; SEQUENCIAL_REG; NUM_TITULO',
                                   VarArrayOf([RXNOSSO_NUMERO.AsString,  //GR.Zeros(RXNOSSO_NUMERO.AsString,  'D', 15),
                                               RXSEQUENCIAL_ARQ.AsString,
                                               Copy(Trim(RXNUMERO_TITULO.AsString), 1, 11)]),  //PF.Espaco(RXNUMERO_TITULO.AsString, 'E', 11)]),
                                   [loCaseInsensitive, loPartialKey]) then
            begin
              dm.mdArquivoD.Edit;
              dm.mdArquivoDID_ATO.AsInteger            := IdAto;
              dm.mdArquivoDNUM_PROTOCOLO_DIST.AsString := GR.Zeros(RXProtocolo.AsString, 'D', 10);

              sValor := FloatToStrF(GR.NoRound(dm.TitulosEMOLUMENTOS.AsFloat, 2), ffFixed, 12, 2);
              dm.mdArquivoDEMOLUMENTOS.AsString        := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

              sValor := FloatToStrF(GR.NoRound(dm.TitulosFETJ.AsFloat, 2), ffFixed, 12, 2);
              dm.mdArquivoDFETJ.AsString               := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

              sValor := FloatToStrF(GR.NoRound(dm.TitulosFUNDPERJ.AsFloat, 2), ffFixed, 12, 2);
              dm.mdArquivoDFUNDPERJ.AsString           := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

              sValor := FloatToStrF(GR.NoRound(dm.TitulosFUNPERJ.AsFloat, 2), ffFixed, 12, 2);
              dm.mdArquivoDFUNPERJ.AsString            := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

              sValor := FloatToStrF(GR.NoRound(dm.TitulosFUNARPEN.AsFloat, 2), ffFixed, 12, 2);
              dm.mdArquivoDFUNARPEN.AsString           := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

              sValor := FloatToStrF(GR.NoRound(dm.TitulosPMCMV.AsFloat, 2), ffFixed, 12, 2);
              dm.mdArquivoDPMCMV.AsString              := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

              sValor := FloatToStrF(GR.NoRound(dm.TitulosISS.AsFloat, 2), ffFixed, 12, 2);
              dm.mdArquivoDISS.AsString                := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

              sValor := FloatToStrF(GR.NoRound(dm.TitulosMUTUA.AsFloat, 2), ffFixed, 12, 2);
              dm.mdArquivoDMUTUA.AsString              := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

              sValor := FloatToStrF(GR.NoRound(dm.TitulosACOTERJ.AsFloat, 2), ffFixed, 12, 2);
              dm.mdArquivoDACOTERJ.AsString            := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

              sValor := FloatToStrF(GR.NoRound(dm.TitulosDISTRIBUICAO.AsFloat, 2), ffFixed, 12, 2);
              dm.mdArquivoDDISTRIBUICAO.AsString       := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

              sValor := FloatToStrF(GR.NoRound(dm.TitulosTOTAL.AsFloat, 2), ffFixed, 12, 2);
              dm.mdArquivoDTOTAL_CUSTAS.AsString       := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

              dm.mdArquivoD.Post;
            end;

            dm.iTotalTitulos := (dm.iTotalTitulos + 1);

            dm.cTotalEmolumentos := (dm.cTotalEmolumentos + GR.NoRound(dm.TitulosTOTAL.AsFloat, 2));

            RXFAKE.Next;
        end;
        RXFAKE.Filtered:=False;
            {end
            else
            RX.Prior;
          end;
        end;}

        {GRAVANDO O SACADOR}
        Inc(IdSac);

        dm.Sacadores.Append;
        dm.SacadoresID_SACADOR.AsInteger      := IdSac;
        dm.SacadoresID_ATO.AsInteger          := IdAto;
        dm.SacadoresNOME.AsString             := RXSACADOR.AsString;
        dm.SacadoresDOCUMENTO.AsString        := RXCPF_CNPJ_SACADOR.AsString;

        if Length(Trim(RXCPF_CNPJ_SACADOR.AsString)) = 11 then
          dm.SacadoresTIPO.AsString := 'F'
        else
        begin
          if Length(Trim(RXCPF_CNPJ_SACADOR.AsString)) = 14 then
            dm.SacadoresTIPO.AsString := 'J'
          else
          begin
            dm.SacadoresDOCUMENTO.Clear;
            dm.SacadoresTIPO.AsString := 'N';
          end;
        end;

        dm.SacadoresENDERECO.AsString         := RXEND_SACADOR.AsString;
        dm.SacadoresBAIRRO.AsString           := RXBAIRRO_SACADOR.AsString;
        dm.SacadoresCEP.AsString              := RXCEP_SACADOR.AsString;
        dm.SacadoresMUNICIPIO.AsString        := RXCID_SACADOR.AsString;
        dm.SacadoresUF.AsString               := RXUF_SACADOR.AsString;

        dm.Sacadores.Post;

        sMsgErro := 'ApplyUpdates dm.Sacadores';

        GR.ExecutarSQLDAC('UPDATE IDS ' +
                         '   SET VALOR = ' + IntToStr(IdSac) +
                         ' WHERE CAMPO = ' + QuotedStr('ID_SACADOR'), dm.conSISTEMA);

        dm.Sacadores.ApplyUpdates(0);

        sMsgErro := '';

//        GravarOrdem;

        {GRAVANDO CUSTAS NO ARQUIVO D}
{        if dm.mdArquivoD.Locate('NOSSO_NUMERO; SEQUENCIAL_REG; NUM_TITULO',
                               VarArrayOf([RXNOSSO_NUMERO.AsString,  //GR.Zeros(RXNOSSO_NUMERO.AsString,  'D', 15),
                                           RXSEQUENCIAL_ARQ.AsString,
                                           Copy(Trim(RXNUMERO_TITULO.AsString), 1, 11)]),  //PF.Espaco(RXNUMERO_TITULO.AsString, 'E', 11)]),
                               [loCaseInsensitive, loPartialKey]) then
        begin
          dm.mdArquivoD.Edit;
          dm.mdArquivoDID_ATO.AsInteger            := IdAto;
          dm.mdArquivoDNUM_PROTOCOLO_DIST.AsString := GR.Zeros(RXProtocolo.AsString, 'D', 10);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosEMOLUMENTOS.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDEMOLUMENTOS.AsString        := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosFETJ.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDFETJ.AsString               := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosFUNDPERJ.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDFUNDPERJ.AsString           := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosFUNPERJ.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDFUNPERJ.AsString            := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosFUNARPEN.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDFUNARPEN.AsString           := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosPMCMV.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDPMCMV.AsString              := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosISS.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDISS.AsString                := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosMUTUA.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDMUTUA.AsString              := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosACOTERJ.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDACOTERJ.AsString            := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosDISTRIBUICAO.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDDISTRIBUICAO.AsString       := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosTOTAL.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDTOTAL_CUSTAS.AsString       := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          dm.mdArquivoD.Post;
        end;

        dm.iTotalTitulos := (dm.iTotalTitulos + 1);

        dm.cTotalEmolumentos := (dm.cTotalEmolumentos + GR.NoRound(dm.TitulosTOTAL.AsFloat, 2));  }
      end
      else
      begin
        {RETIRANDO REGISTRO DO ARQUIVO D}
        dm.mdArquivoD.Locate('SEQUENCIAL_REG; SEQ_BANCO',
                             VarArrayOf([RXSEQUENCIAL_ARQ.AsString,
                                         RXSEQ_BANCO.AsInteger]),
                             [loCaseInsensitive, loPartialKey]);
        dm.mdArquivoD.Delete;
      end;

      RX.Next;
    end;

    RX.RecNo := Posicao;

    RX.EnableControls;

    if dm.conSISTEMA.InTransaction then
      dm.conSISTEMA.Commit;

    if dm.conSISTEMA.Connected then
      dm.conSISTEMA.StartTransaction;

    dm.DistribuirCartorios;

    dm.ExportarArquivoConfirmacao(ExtractFileName(opd.FileName), RX);

    PF.Aguarde(False);

    if dm.conSISTEMA.InTransaction then
      dm.conSISTEMA.Commit;

    if dm.ValorParametro(100) = 'S' then  //Geracao AUTOMATICA de Arquivo de Remessa D para Serventias Agregadas
    begin
      PF.Aguarde(True);

      lErro := False;

      sMsgRet := '';

      if LstArqB.Items.IndexOf(ExtractFileName(opd.FileName)) < 0 then
        LstArqB.Items.Add(ExtractFileName(opd.FileName));

      dm.ExportarCartorios(sMsgRet, lErro, True, LstArqB);

      if LstArqB.Items.IndexOf(ExtractFileName(opd.FileName)) > -1 then
        LstArqB.Items.Delete(LstArqB.Items.IndexOf(ExtractFileName(opd.FileName)));

      if lErro then
        Application.MessageBox(PChar(sMsgRet), 'Erro', MB_OK + MB_ICONERROR)
      else
      begin
        dm.EnviarRemessasCartorios;
        Application.MessageBox(PChar(sMsgRet), 'Aviso', MB_OK);
      end;

      PF.Aguarde(False);
    end;

    RX.Filtered:=False;

    RX.DisableControls;
    RX.First;

    FQuickOrdem1.iTotOrdems := 0;

    while not RX.Eof do
    begin
      if dm.mdArquivoD.Locate('NUM_PROTOCOLO_DIST', GR.Zeros(RXProtocolo.AsString, 'D', 10), []) then
      begin
        if dm.ServentiaAgregada.Locate('CODIGO_DISTRIBUICAO', dm.mdArquivoDCOD_CARTORIO.AsInteger, []) then
        begin
          RX.Edit;
          RXSERVENTIA_AGREG.AsString := dm.ServentiaAgregadaDESCRICAO.AsString;
          RXCARTORIO.AsInteger       := dm.ServentiaAgregadaCODIGO_DISTRIBUICAO.AsInteger;
          RX.Post;
        end;
      end;

      GravarOrdem;

      RX.Next;
    end;

    RX.EnableControls;

    if Application.MessageBox('Deseja imprimir Ordem de Protesto?',
                              'Confirma��o',
                              MB_YESNO + MB_ICONQUESTION) = ID_YES then
    begin
{      if Application.MessageBox('Deseja trocar o nome do Portador pelo nome do Cedente?',
                                'Impress�o',
                                MB_YESNO + MB_ICONQUESTION) = ID_YES then
        FQuickOrdem1.lTrocarPortadorCedente := True;
      else
        FQuickOrdem1.lTrocarPortadorCedente := False;

      if Application.MessageBox('Deseja incluir 2� via?',
                                'Impress�o',
                                MB_YESNO + MB_ICONQUESTION) = ID_YES then
        FQuickOrdem1.lSegundaVia := True
      else
        FQuickOrdem1.lSegundaVia := False;  }

      FQuickOrdem1.lSegundaVia := False;

      FQuickOrdem1.Ordem.Preview;
    end;

    FQuickOrdem1.Free;

{    RX.DisableControls;
    RX.First;

    while not RX.Eof do
    begin
      if dm.mdArquivoD.Locate('NUM_PROTOCOLO_DIST', GR.Zeros(RXProtocolo.AsString, 'D', 10), []) then
      begin
        if dm.ServentiaAgregada.Locate('CODIGO_DISTRIBUICAO', dm.mdArquivoDCOD_CARTORIO.AsInteger, []) then
        begin
          RX.Edit;
          RXSERVENTIA_AGREG.AsString := dm.ServentiaAgregadaDESCRICAO.AsString;
          RX.Post;
        end;
      end;

      RX.Next;
    end;

    RX.EnableControls;  }

    PB.Visible:=False;

    dm.Controle.Close;
    dm.Controle.Params[0].AsString:='NPR';
    dm.Controle.Open;

    Application.CreateForm(TFValores, FValores);

    FValores.lbEM.Caption := FloatToStrF(EMOLUMENTOS, ffNumber, 7, 2);
    FValores.lbFE.Caption := FloatToStrF(FETJ, ffNumber, 7, 2);
    FValores.lbFD.Caption := FloatToStrF(FUNDPERJ, ffNumber, 7, 2);
    FValores.lbFP.Caption := FloatToStrF(FUNPERJ, ffNumber, 7, 2);
    FValores.lbFN.Caption := FloatToStrF(FUNARPEN, ffNumber, 7, 2);
    FValores.lbPM.Caption := FloatToStrF(PMCMV, ffNumber, 7, 2);
    FValores.lbSS.Caption := FloatToStrF(ISS, ffNumber, 7, 2);
    FValores.lbMU.Caption := FloatToStrF(MUTUA, ffNumber, 7, 2);
    FValores.lbAC.Caption := FloatToStrF(ACOTERJ, ffNumber, 7, 2);
    FValores.lbDB.Caption := FloatToStrF(DISTRIBUICAO, ffNumber, 7, 2);
    FValores.lbTT.Caption := FloatToStrF(TOTAL, ffNumber, 7, 2);

    FValores.lbISS.Caption:='Iss ('+FloatToStr(Gdm.vTaxaIss)+'%): R$.....';

    FValores.ShowModal;
    FValores.Free;
  except
    on E: Exception do
    begin
      if dm.conSISTEMA.InTransaction then
        dm.conSISTEMA.Rollback;

      RX.Filtered:=False;

      PF.Aguarde(False);

      GR.Aviso('Erro: ' + sMsgErro + ' (' + E.Message + ')');
    end;
  end;
end;

procedure TFImportarTxt.FormCreate(Sender: TObject);
begin
  dm.vCalc:=False;
  dm.Pracas.Close;
  dm.Pracas.Open;
  PA.Visible:=FPrincipal.ckAssistente.Checked;
  lbAviso.Caption:='Seu arquivo de confirma��o ser� gerado na seguinte pasta: '+dm.ValorParametro(3);
  dm.Controle.Close;
  dm.Controle.Params[0].AsString:='NPR';
  dm.Controle.Open;

  LstArqB := TListBox.Create(Self);
  LstArqB.Parent  := Self;
  LstArqB.Visible := False;
end;

procedure TFImportarTxt.GridAtosExit(Sender: TObject);
begin
  if RX.State in [dsEdit,dsInsert] then
    RX.Post;
end;

procedure TFImportarTxt.MarcarTodosClick(Sender: TObject);
var
  Posicao: Integer;
begin
  RX.DisableControls;
  Posicao:=RX.RecNo;
  RX.First;
  while not RX.Eof do
  begin
      RX.Edit;
      RXSTATUS.AsString:='Aceito';
      RXCheck.AsBoolean:=True;
      RXIrregularidade.Clear;
      RX.Post;
      RX.Next;
  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;
  AtribuirProtocolos;
end;

procedure TFImportarTxt.DesmarcarTodosClick(Sender: TObject);
var
  Posicao: Integer;
begin
  RX.DisableControls;
  Posicao:=RX.RecNo;
  RX.First;
  while not RX.Eof do
  begin
      RX.Edit;
      RXSTATUS.AsString := 'Rejeitado';
      RXCheck.AsBoolean := False;

      if (RXIrregularidade.AsString = '') and
        (RXMotivoRejeicao.AsString = '') then
        RXMotivoRejeicao.AsString:='N�O INFORMADA!';
        
      RX.Post;
      RX.Next;
  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;
  AtribuirProtocolos;
end;

procedure TFImportarTxt.btRejeitarClick(Sender: TObject);
begin
  if RX.IsEmpty then
    Exit;

  dm.vOkGeral := False;

  GR.CriarForm(TFIrregularidades,FIrregularidades);

  if dm.vOkGeral then
  begin
    RX.Edit;
    RXIrregularidade.AsString := dm.vIrregularidade;
    RXMotivoRejeicao.AsString := '(' + dm.vIrregularidade + ') ' + dm.vMotivo;
    RXSTATUS.AsString := 'Rejeitado';
    RXCheck.AsBoolean := False;
    RX.Post;

    VerificaMarcados(RXCheck.AsBoolean, RXNUMERO_TITULO.AsString, RXNOSSO_NUMERO.AsString, RXSEQUENCIAL_ARQ.AsString);

    AtribuirProtocolos;
  end;

  dm.Irregularidades.Close;
end;

procedure TFImportarTxt.AtribuirProtocolos;
var
  Posicao,Protocolo: Integer;
  CTitulo,CCoobrigado: String;
begin
  Posicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  Protocolo:=dm.ControleVALOR.AsInteger-1;
  CTitulo:=RXNUMERO_TITULO.AsString;
  CCoobrigado:=RXNUMDEVEDOR.AsString;
  while not RX.Eof do
  begin
      RX.Edit;

      if RXCheck.AsBoolean  then
      begin
          if CCoobrigado <> '1' then
          begin
            if CTitulo <> RXNUMERO_TITULO.AsString then
            begin
                CTitulo:=RXNUMERO_TITULO.AsString;
                Inc(Protocolo);
                RXProtocolo.AsInteger:=Protocolo;
            end
            else
            begin
                RXProtocolo.AsInteger:=Protocolo;
            end;
          end
          else
          begin
              CTitulo:=RXNUMERO_TITULO.AsString;
              Inc(Protocolo);
              RXProtocolo.AsInteger:=Protocolo;
          end;

       end
       else RXProtocolo.Clear;

      RX.Post;

      RX.Next;
      CCoobrigado:=RXNUMDEVEDOR.AsString;

  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;

(*var
  Posicao,Protocolo: Integer;
  CTitulo: String;    
  CCooBrigado: String;
  lIncremetar: Boolean;
begin
  Posicao := RX.RecNo;

  RX.DisableControls;
  RX.First;

  Protocolo := dm.ControleVALOR.AsInteger;
  CTitulo := RXNUMERO_TITULO.AsString;
  lIncremetar := True;

  while not RX.Eof do
  begin
    RX.Edit;

    if RXCheck.AsBoolean  then
    begin
      if CTitulo <> RXNUMERO_TITULO.AsString then
      begin
        CTitulo := RXNUMERO_TITULO.AsString;
        Inc(Protocolo);
      end
      else
      begin
        if RXNUMDEVEDOR.AsString = '1' then
          Inc(Protocolo);
      end;

      RXProtocolo.AsInteger := Protocolo;
    end
    else
      RXProtocolo.Clear;

    RX.Post;
    RX.Next;
  end;

  RX.RecNo := Posicao;
  RX.EnableControls;  *)
end;

procedure TFImportarTxt.edProtocoloExit(Sender: TObject);
begin
  if dm.Controle.State in [dsEdit,dsInsert] then
  begin
      dm.Controle.Post;
      dm.Controle.ApplyUpdates(0);
  end;
end;

procedure TFImportarTxt.btCriticasClick(Sender: TObject);
var
  Posicao: Integer;
begin
  try
    Screen.Cursor:=crHourGlass;
    PF.Aguarde(True);

    dm.Criticas.Close;
    dm.Criticas.Open;

    RX.DisableControls;

    Posicao := RX.RecNo;
    RX.First;

    lRejeitar  := False;
    sNumTitulo := '';

    while not RX.Eof do
    begin
      if sNumTitulo <> RXNUMERO_TITULO.AsString then
      begin
        sNumTitulo := RXNUMERO_TITULO.AsString;
        lRejeitar  := False;
      end
      else
      begin
        if RXNUMDEVEDOR.AsString = '1' then
          lRejeitar  := False;
      end;

      Application.ProcessMessages;
      RX.Edit;
      RXCheck.AsBoolean:=True;
      if (edData.Date<RXDT_VENCIMENTO.AsDateTime) and (RXAVISTA.AsString='N') then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=1;
          dm.CriticasDescricao.AsString        :='Data da apresenta��o inferior � data de vencimento';
          dm.Criticas.Post;
          RX.Edit;
          RXIrregularidade.AsString:='1';
          RXMotivoRejeicao.AsString:='(1) Data da apresenta��o inferior � data de vencimento';
          RXCheck.AsBoolean:=False;
          lRejeitar := True;
      end;

      if RXCPF_CNPJ_DEVEDOR.AsString='' then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=7;
          dm.CriticasDescricao.AsString        :='CNPJ/CPF do sacado inv�lido/incorreto';
          dm.Criticas.Post;
          RX.Edit;
          RXIrregularidade.AsString:='7';
          RXMotivoRejeicao.AsString:='(7) CNPJ/CPF do sacado inv�lido/incorreto';
          RXCheck.AsBoolean:=False;
          lRejeitar := True;
      end
      else
        if RXTIPO_DEVEDOR.AsString='F' then
        begin
            if not PF.VerificarCPF(RXCPF_CNPJ_DEVEDOR.AsString) then
            begin
                dm.Criticas.Append;
                dm.CriticasID.AsInteger              :=RX.RecNo;
                dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
                dm.CriticasCodigo_Critica.AsInteger  :=7;
                dm.CriticasDescricao.AsString        :='CNPJ/CPF do sacado inv�lido/incorreto';
                dm.Criticas.Post;
                RX.Edit;
                RXIrregularidade.AsString:='7';
                RXMotivoRejeicao.AsString:='(7) CNPJ/CPF do sacado inv�lido/incorreto';
                RXCheck.AsBoolean:=False;
                lRejeitar := True;
            end;
        end
        else
        begin
            if not PF.VerificarCNPJ(RXCPF_CNPJ_DEVEDOR.AsString) then
            begin
                dm.Criticas.Append;
                dm.CriticasID.AsInteger              :=RX.RecNo;
                dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
                dm.CriticasCodigo_Critica.AsInteger  :=7;
                dm.CriticasDescricao.AsString        :='CNPJ/CPF do sacado inv�lido/incorreto';
                dm.Criticas.Post;
                RX.Edit;
                RXIrregularidade.AsString:='7';
                RXMotivoRejeicao.AsString:='(7) CNPJ/CPF do sacado inv�lido/incorreto';
                RXCheck.AsBoolean:=False;
                lRejeitar := True;
            end;
        end;

      if (RXPRACA.AsString<>dm.ValorParametro(6)) and
         (not dm.Pracas.Locate('NOME',RXPRACA.AsString,[loPartialKey,loCaseInsensitive])) then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=15;
          dm.CriticasDescricao.AsString        :='Pra�a de pagamento incompat�vel ('+RXPRACA.AsString+')';
          dm.Criticas.Post;
          RX.Edit;
          RXIrregularidade.AsString:='15';
          RXMotivoRejeicao.AsString:='(15) Pra�a de pagamento incompat�vel ('+RXPRACA.AsString+')';
          RXCheck.AsBoolean:=False;
          lRejeitar := True;
      end;

      if RXNUMERO_TITULO.AsString='' then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=16;
          dm.CriticasDescricao.AsString        :='Falta n�mero do t�tulo';
          dm.Criticas.Post;
          RX.Edit;
          RXIrregularidade.AsString:='16';
          RXMotivoRejeicao.AsString:='(16) Falta n�mero do t�tulo';
          RXCheck.AsBoolean:=False;
          lRejeitar := True;
      end;

      if RXDT_TITULO.AsDateTime=0 then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=18;
          dm.CriticasDescricao.AsString        :='Falta data de emiss�o do t�tulo';
          dm.Criticas.Post;
          RX.Edit;
          RXIrregularidade.AsString:='18';
          RXMotivoRejeicao.AsString:='(18) Falta data de emiss�o do t�tulo';
          RXCheck.AsBoolean:=False;
          lRejeitar := True;
      end;

      if (RXDT_TITULO.AsDateTime>RXDT_VENCIMENTO.AsDateTime) and (RXAVISTA.AsString='N') then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=20;
          dm.CriticasDescricao.AsString        :='Data de emiss�o posterior ao vencimento';
          dm.Criticas.Post;
          RX.Edit;
          RXIrregularidade.AsString:='20';
          RXMotivoRejeicao.AsString:='(20) Data de emiss�o posterior ao vencimento';
          RXCheck.AsBoolean:=False;
          lRejeitar := True;
      end;

      if RXDEVEDOR.AsString=RXSACADOR.AsString then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=28;
          dm.CriticasDescricao.AsString        :='Sacado e Sacador/Avalista s�o a mesma pessoa';
          dm.Criticas.Post;
          RX.Edit;
          RXIrregularidade.AsString:='28';
          RXMotivoRejeicao.AsString:='(28) Sacado e Sacador/Avalista s�o a mesma pessoa';
          RXCheck.AsBoolean:=False;
          lRejeitar := True;
      end;

      if (RXDT_VENCIMENTO.AsDateTime=0) and (RXAVISTA.AsString='N') then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=33;
          dm.CriticasDescricao.AsString        :='Falta data de vencimento do t�tulo';
          dm.Criticas.Post;
          RX.Edit;
          RXIrregularidade.AsString:='33';
          RXMotivoRejeicao.AsString:='(33) Falta data de vencimento do t�tulo';
          RXCheck.AsBoolean:=False;
          lRejeitar := True;
      end;

      {VERIFICANDO SE N�MERO DO T�TULO J� FOI APONTADO PARA ESSE PORTADOR}
      if RXCODIGO_APRESENTANTE.AsString<>'' then
      begin
          qryDuplicado.Close;
          qryDuplicado.Params[0].AsString  :=RXNUMERO_TITULO.AsString;
          qryDuplicado.Params[1].AsString  :=RXCODIGO_APRESENTANTE.AsString;
          qryDuplicado.Params[2].AsFloat   :=RXVALOR_TITULO.AsFloat;
          qryDuplicado.Params[3].AsDate    :=RXDT_TITULO.AsDateTime;
          qryDuplicado.Params[4].AsDate    :=RXDT_VENCIMENTO.AsDateTime;
          qryDuplicado.Open;
          if not qryDuplicado.IsEmpty then
          begin
              dm.Criticas.Append;
              dm.CriticasID.AsInteger:=RX.RecNo;
              dm.CriticasNumero_Titulo.AsString:=RXNUMERO_TITULO.AsString;
              dm.CriticasCodigo_Critica.AsInteger:=52;
              dm.CriticasDescricao.AsString:='T�tulo apresentado em duplicidade';
              dm.Criticas.Post;
              RX.Edit;
              RXIrregularidade.AsString:='52';
              RXMotivoRejeicao.AsString:='(52) T�tulo apresentado em duplicidade';
              RXCheck.AsBoolean:=False;
              lRejeitar := True;
          end;
      end;

      VerificaMarcados(not lRejeitar, RXNUMERO_TITULO.AsString, RXNOSSO_NUMERO.AsString, RXSEQUENCIAL_ARQ.AsString);

      RX.Next;
    end;

    RX.RecNo:=Posicao;
    RX.EnableControls;

    PF.Aguarde(False);
    Screen.Cursor:=crDefault;

    dm.Criticas.First;
    if not dm.Criticas.IsEmpty then
      GR.CriarForm(TFCriticas,FCriticas);
        //else GR.Aviso('Nenhuma irregularidade encontrada.');

    AtribuirProtocolos;
  except
    on E: Exception do
    begin
        PF.Aguarde(False);
        GR.Aviso('Erro de arquivo!'+#13#13+'Mensagem: '+E.Message);
        Close;
    end;
  end;
end;

procedure TFImportarTxt.GravarOrdem;
begin
  with FQuickOrdem1 do
  begin
    RXOrdem.Append;
    RXOrdemDevedor.AsString           := RXDEVEDOR.AsString;
    RXOrdemTipoDevedor.AsString       := RXTIPO_DEVEDOR.AsString;

    if RXTIPO_DEVEDOR.AsString='F' then
      RXOrdemCPF_CNPJ_Devedor.AsString := PF.FormatarCPF(RXCPF_CNPJ_DEVEDOR.AsString)
    else
      RXOrdemCPF_CNPJ_Devedor.AsString := PF.FormatarCNPJ(RXCPF_CNPJ_DEVEDOR.AsString);

    RXOrdemPraca.AsString             := RXPRACA.AsString;
    RXOrdemEnderecoDevedor.AsString   := RXEND_DEVEDOR.AsString+' '+RXBAIRRO_DEVEDOR.AsString+' '+RXCID_DEVEDOR.AsString+'-'+RXUF.AsString;
    RXOrdemSacador.AsString           := RXSACADOR.AsString;
    RXOrdemCedente.AsString           := RXCEDENTE.AsString;
    RXOrdemCodApresentante.AsString   := RXCODIGO_APRESENTANTE.AsString;
    RXOrdemApresentante.AsString      := RXAPRESENTANTE.AsString;
    RXOrdemEmissao.AsDateTime         := RXDT_TITULO.AsDateTime;

    if Trim(RXCODIGO_APRESENTANTE.AsString) = 'E19' then
      RXOrdemNomeApresentante.AsString := RXCEDENTE.AsString
    else
      RXOrdemNomeApresentante.AsString := RXAPRESENTANTE.AsString;

    if not RXDIAS_AVISTA.IsNull then
      RXOrdemVencimento.AsString := '� VISTA'
    else
      RXOrdemVencimento.AsDateTime := RXDT_VENCIMENTO.AsDateTime;

{    if RXSALDO_TITULO.AsFloat <> 0 then
      RXOrdemValor.AsFloat := RXSALDO_TITULO.AsFloat
    else
      RXOrdemValor.AsFloat := RXVALOR_TITULO.AsFloat;  }

    RXOrdemValor.AsFloat              := RXVALOR_TITULO.AsFloat;
    RXOrdemSaldo.AsFloat              := RXSALDO_TITULO.AsFloat;
    RXOrdemAgencia.AsString           := RXAGENCIA.AsString;
    RXOrdemEspecie.AsString           := PF.RetornarSigla(RXTIPO_TITULO.AsInteger);
    RXOrdemNumeroTitulo.AsString      := RXNUMERO_TITULO.AsString;

    if RXENDOSSO.AsString='M' then    RXOrdemEndosso.AsString:='Mandato';
    if RXENDOSSO.AsString='T' then    RXOrdemEndosso.AsString:='Translativo';
    if RXENDOSSO.AsString='S' then    RXOrdemEndosso.AsString:='Sem endosso';

    RXOrdemAgencia.AsString           := RXAGENCIA.AsString;
    RXOrdemAceite.AsString            := RXACEITE.AsString;
    RXOrdemNossoNumero.AsString       := RXNOSSO_NUMERO.AsString;
    RXOrdemProtocolo.AsInteger        := RXProtocolo.AsInteger;
    RXOrdemDataProtocolo.AsDateTime   := edData.Date;

    RXOrdemCustas.AsFloat             := RXCUSTAS.AsFloat;
    RXOrdemConvenio.AsBoolean         := RXCONVENIO.AsBoolean;
    RXOrdemLetra.AsString             := RXLETRA.AsString;
    RXOrdemCartorio.AsString          := RXCARTORIO.AsString;
    if RXPOSTECIPADO.AsString.Trim='P' then
     RXOrdemPOSTECIPADO.AsString       := 'Sim'
       else
         RXOrdemPOSTECIPADO.AsString       := 'N�o';
    RXOrdem.Post;

    Inc(iTotOrdems);
  end;
end;

procedure TFImportarTxt.RXENFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept:=RXENCodigo.AsString=RXCODIGO_APRESENTANTE.AsString;
end;

function TFImportarTxt.Maximo: Integer;
var
  X: Integer;
begin
  X:=0;
  RXEN.First;
  while not RXEN.Eof do
  begin
      Inc(X);
      RXEN.Next;
  end;
  Result:=X;
  RXEN.First;
end;

procedure TFImportarTxt.lbAvisoClick(Sender: TObject);
var
  Path: String;
begin
  Path:=dm.ValorParametro(3);
  InputQuery('Arquivo de Confirma��o','Diret�rio',Path);
  dm.AtualizarParametro(3,Path);
  lbAviso.Caption:='Seu arquivo de confirma��o ser� gerado na seguinte pasta: '+dm.ValorParametro(3);
  lbAviso.Refresh;
end;

function TFImportarTxt.QtdMarcados: Integer;
var
  P: Pointer;
begin
  Result:=0;
  P:=RX.GetBookmark;
  RX.DisableControls;
  RX.First;
  while not RX.Eof do
  begin
      Inc(Result);
      RX.Next;
  end;
  if RX.BookmarkValid(P) then
  RX.GotoBookmark(P);
  RX.EnableControls;
end;

procedure TFImportarTxt.ChecarIrregularidade;
begin
  if (edData.Date<RXDT_VENCIMENTO.AsDateTime) and (RXAVISTA.AsString='N') then
  begin
      dm.Criticas.Append;
      dm.CriticasID.AsInteger              :=RX.RecNo;
      dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
      dm.CriticasCodigo_Critica.AsInteger  :=1;
      dm.CriticasDescricao.AsString        :='Data da apresenta��o inferior � data de vencimento';
      dm.Criticas.Post;
      RXIrregularidade.AsString:='1';
      RXMotivoRejeicao.AsString:='(1) Data da apresenta��o inferior � data de vencimento';
      RXSTATUS.AsString:='Rejeitado';
      RXCheck.AsBoolean:=False;
      lRejeitar := True;
  end;

  if RXCPF_CNPJ_DEVEDOR.AsString='' then
  begin
      dm.Criticas.Append;
      dm.CriticasID.AsInteger              :=RX.RecNo;
      dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
      dm.CriticasCodigo_Critica.AsInteger  :=7;
      dm.CriticasDescricao.AsString        :='CNPJ/CPF do sacado inv�lido/incorreto';
      dm.Criticas.Post;
      RXIrregularidade.AsString:='7';
      RXMotivoRejeicao.AsString:='(7) CNPJ/CPF do sacado inv�lido/incorreto';
      RXSTATUS.AsString:='Rejeitado';
      RXCheck.AsBoolean:=False;
      lRejeitar := True;
  end
  else
    if RXTIPO_DEVEDOR.AsString='F' then
    begin
        if not PF.VerificarCPF(RXCPF_CNPJ_DEVEDOR.AsString) then
        begin
            dm.Criticas.Append;
            dm.CriticasID.AsInteger              :=RX.RecNo;
            dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
            dm.CriticasCodigo_Critica.AsInteger  :=7;
            dm.CriticasDescricao.AsString        :='CNPJ/CPF do sacado inv�lido/incorreto';
            dm.Criticas.Post;
            RXIrregularidade.AsString:='7';
            RXMotivoRejeicao.AsString:='(7) CNPJ/CPF do sacado inv�lido/incorreto';
            RXSTATUS.AsString:='Rejeitado';
            RXCheck.AsBoolean:=False;
            lRejeitar := True;
        end;
    end
    else
    begin
        if not PF.VerificarCNPJ(RXCPF_CNPJ_DEVEDOR.AsString) then
        begin
            dm.Criticas.Append;
            dm.CriticasID.AsInteger              :=RX.RecNo;
            dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
            dm.CriticasCodigo_Critica.AsInteger  :=7;
            dm.CriticasDescricao.AsString        :='CNPJ/CPF do sacado inv�lido/incorreto';
            dm.Criticas.Post;
            RXIrregularidade.AsString:='7';
            RXMotivoRejeicao.AsString:='(7) CNPJ/CPF do sacado inv�lido/incorreto';
            RXSTATUS.AsString:='Rejeitado';
            RXCheck.AsBoolean:=False;
            lRejeitar := True;
        end;
    end;

  if (RXPRACA.AsString<>dm.ValorParametro(6)) and
     (not dm.Pracas.Locate('NOME',RXPRACA.AsString,[loPartialKey,loCaseInsensitive])) then
  begin
      dm.Criticas.Append;
      dm.CriticasID.AsInteger              :=RX.RecNo;
      dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
      dm.CriticasCodigo_Critica.AsInteger  :=15;
      dm.CriticasDescricao.AsString        :='Pra�a de pagamento incompat�vel ('+RXPRACA.AsString+')';
      dm.Criticas.Post;
      RXIrregularidade.AsString:='15';
      RXMotivoRejeicao.AsString:='(15) Pra�a de pagamento incompat�vel ('+RXPRACA.AsString+')';
      RXSTATUS.AsString:='Rejeitado';
      RXCheck.AsBoolean:=False;
      lRejeitar := True;
  end;

  if RXNUMERO_TITULO.AsString='' then
  begin
      dm.Criticas.Append;
      dm.CriticasID.AsInteger              :=RX.RecNo;
      dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
      dm.CriticasCodigo_Critica.AsInteger  :=16;
      dm.CriticasDescricao.AsString        :='Falta n�mero do t�tulo';
      dm.Criticas.Post;
      RXIrregularidade.AsString:='16';
      RXMotivoRejeicao.AsString:='(16) Falta n�mero do t�tulo';
      RXSTATUS.AsString:='Rejeitado';
      RXCheck.AsBoolean:=False;
      lRejeitar := True;
  end;

  if RXDT_TITULO.AsDateTime=0 then
  begin
      dm.Criticas.Append;
      dm.CriticasID.AsInteger              :=RX.RecNo;
      dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
      dm.CriticasCodigo_Critica.AsInteger  :=18;
      dm.CriticasDescricao.AsString        :='Falta data de emiss�o do t�tulo';
      dm.Criticas.Post;
      RXIrregularidade.AsString:='18';
      RXMotivoRejeicao.AsString:='(18) Falta data de emiss�o do t�tulo';
      RXSTATUS.AsString:='Rejeitado';
      RXCheck.AsBoolean:=False;
      lRejeitar := True;
  end;

  if (RXDT_TITULO.AsDateTime>RXDT_VENCIMENTO.AsDateTime) and (RXAVISTA.AsString='N') then
  begin
      dm.Criticas.Append;
      dm.CriticasID.AsInteger              :=RX.RecNo;
      dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
      dm.CriticasCodigo_Critica.AsInteger  :=20;
      dm.CriticasDescricao.AsString        :='Data de emiss�o posterior ao vencimento';
      dm.Criticas.Post;
      RXIrregularidade.AsString:='20';
      RXMotivoRejeicao.AsString:='(20) Data de emiss�o posterior ao vencimento';
      RXSTATUS.AsString:='Rejeitado';
      RXCheck.AsBoolean:=False;
      lRejeitar := True;
  end;

  if RXDEVEDOR.AsString=RXSACADOR.AsString then
  begin
      dm.Criticas.Append;
      dm.CriticasID.AsInteger              :=RX.RecNo;
      dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
      dm.CriticasCodigo_Critica.AsInteger  :=28;
      dm.CriticasDescricao.AsString        :='Sacado e Sacador/Avalista s�o a mesma pessoa';
      dm.Criticas.Post;
      RXIrregularidade.AsString:='28';
      RXMotivoRejeicao.AsString:='(28) Sacado e Sacador/Avalista s�o a mesma pessoa';
      RXSTATUS.AsString:='Rejeitado';
      RXCheck.AsBoolean:=False;
      lRejeitar := True;
  end;

  if (RXDT_VENCIMENTO.AsDateTime=0) and (RXAVISTA.AsString='N') then
  begin
      dm.Criticas.Append;
      dm.CriticasID.AsInteger              :=RX.RecNo;
      dm.CriticasNumero_Titulo.AsString    :=RXNUMERO_TITULO.AsString;
      dm.CriticasCodigo_Critica.AsInteger  :=33;
      dm.CriticasDescricao.AsString        :='Falta data de vencimento do t�tulo';
      dm.Criticas.Post;
      RXIrregularidade.AsString:='33';
      RXMotivoRejeicao.AsString:='(33) Falta data de vencimento do t�tulo';
      RXSTATUS.AsString:='Rejeitado';
      RXCheck.AsBoolean:=False;
      lRejeitar := True;
  end;

  {VERIFICANDO SE N�MERO DO T�TULO J� FOI APONTADO PARA ESSE PORTADOR}
  if RXCODIGO_APRESENTANTE.AsString<>'' then
  begin
      qryDuplicado.Close;
      qryDuplicado.Params[0].AsString  :=RXNUMERO_TITULO.AsString;
      qryDuplicado.Params[1].AsString  :=RXCODIGO_APRESENTANTE.AsString;
      qryDuplicado.Params[2].AsFloat   :=RXVALOR_TITULO.AsFloat;
      qryDuplicado.Params[3].AsDate    :=RXDT_TITULO.AsDateTime;
      qryDuplicado.Params[4].AsDate    :=RXDT_VENCIMENTO.AsDateTime;
      qryDuplicado.Open;
      if not qryDuplicado.IsEmpty then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger:=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString:=RXNUMERO_TITULO.AsString;
          dm.CriticasCodigo_Critica.AsInteger:=52;
          dm.CriticasDescricao.AsString:='T�tulo apresentado em duplicidade';
          dm.Criticas.Post;
          RXIrregularidade.AsString:='52';
          RXMotivoRejeicao.AsString:='(52) T�tulo apresentado em duplicidade';
          RXSTATUS.AsString:='Rejeitado';
          RXCheck.AsBoolean:=False;
          lRejeitar := True;
      end;
  end;
end;

procedure TFImportarTxt.GridAtosMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if X in [0..28] then
    Screen.Cursor:=crHandPoint
      else Screen.Cursor:=crDefault;
end;

procedure TFImportarTxt.sPanel2MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  Screen.Cursor:=crDefault;
end;

procedure TFImportarTxt.PMotivoMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  Screen.Cursor:=crDefault;
end;

procedure TFImportarTxt.GridAtosMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Screen.Cursor=crHandPoint then
  begin
    if RX.IsEmpty then
      Exit;

    RX.Edit;

    if RXCheck.AsBoolean = True then
    begin
      RXSTATUS.AsString := 'Rejeitado';
      RXCheck.AsBoolean := False;

      if (RXIrregularidade.AsString = '') and
       (RXMotivoRejeicao.AsString = '') then
        RXMotivoRejeicao.AsString := 'N�O INFORMADA!';
    end
    else
    begin
      RXSTATUS.AsString:='Aceito';
      RXCheck.AsBoolean:=True;
      RXIrregularidade.Clear;
    end;

    RX.Post;

    VerificaMarcados(RXCheck.AsBoolean, RXNUMERO_TITULO.AsString, RXNOSSO_NUMERO.AsString, RXSEQUENCIAL_ARQ.AsString);

    AtribuirProtocolos;
  end;
end;

procedure TFImportarTxt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.ServentiaAgregada.Filtered := False;
  dm.vCalc:=True;

  FreeAndNil(LstArqB);
end;

procedure TFImportarTxt.VerificaMarcados(Check: Boolean;
  Num_Titulo, Nosso_Numero, Sequencial: String);
var
  P: TBookmark;
begin
  P := RX.GetBookmark;

  RX.DisableControls;
  RX.First;

  while not RX.Eof do
  begin
    if (RXNUMERO_TITULO.AsString = Num_Titulo) and
      (RXNOSSO_NUMERO.AsString = Nosso_Numero) and
      (RXSEQUENCIAL_ARQ.AsString = Sequencial) then
    begin
      RX.Edit;
      RXCheck.AsBoolean := Check;

      if RXCheck.AsBoolean = False then
      begin
        RXSTATUS.AsString := 'Rejeitado';

        if (RXIrregularidade.AsString = '') and
          (RXMotivoRejeicao.AsString = '') then
          RXMotivoRejeicao.AsString := 'N�O INFORMADA!';
      end
      else
      begin
        RXSTATUS.AsString := 'Aceito';
        RXIrregularidade.Clear;

        if (RXIrregularidade.AsString = '') and
          (RXMotivoRejeicao.AsString <> '') then
          RXMotivoRejeicao.AsString := ''
      end;

      RX.Post;
    end;

    RX.Next;
  end;

  RX.EnableControls;
  RX.GotoBookmark(P);
end;

end.



        {GRAVANDO CUSTAS NO ARQUIVO D}
        (*if dm.mdArquivoD.Locate('NOSSO_NUMERO; SEQUENCIAL_REG; NUM_TITULO',
                               VarArrayOf([RXNOSSO_NUMERO.AsString,  //GR.Zeros(RXNOSSO_NUMERO.AsString,  'D', 15),
                                           RXSEQUENCIAL_ARQ.AsString,
                                           Copy(Trim(RXNUMERO_TITULO.AsString), 1, 11)]),  //PF.Espaco(RXNUMERO_TITULO.AsString, 'E', 11)]),
                               [loCaseInsensitive, loPartialKey]) then
        begin
          dm.mdArquivoD.Edit;
          dm.mdArquivoDID_ATO.AsInteger            := IdAto;
          dm.mdArquivoDNUM_PROTOCOLO_DIST.AsString := GR.Zeros(RXProtocolo.AsString, 'D', 10);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosEMOLUMENTOS.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDEMOLUMENTOS.AsString        := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosFETJ.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDFETJ.AsString               := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosFUNDPERJ.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDFUNDPERJ.AsString           := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosFUNPERJ.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDFUNPERJ.AsString            := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosFUNARPEN.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDFUNARPEN.AsString           := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosPMCMV.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDPMCMV.AsString              := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosISS.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDISS.AsString                := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosMUTUA.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDMUTUA.AsString              := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosACOTERJ.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDACOTERJ.AsString            := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosDISTRIBUICAO.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDDISTRIBUICAO.AsString       := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          sValor := FloatToStrF(GR.NoRound(dm.TitulosTOTAL.AsFloat, 2), ffFixed, 12, 2);
          dm.mdArquivoDTOTAL_CUSTAS.AsString       := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 14);

          dm.mdArquivoD.Post;
        end;

        dm.iTotalTitulos := (dm.iTotalTitulos + 1);

        dm.cTotalEmolumentos := (dm.cTotalEmolumentos + GR.NoRound(dm.TitulosTOTAL.AsFloat, 2));

        sMsgErro := '';

        {GRAVANDO O DEVEDOR}
        Inc(IdDev);

        dm.Devedores.Append;

        dm.DevedoresID_DEVEDOR.AsInteger      := IdDev;
        dm.DevedoresID_ATO.AsInteger          := IdAto;
        dm.DevedoresORDEM.AsInteger           := RXNUMDEVEDOR.AsInteger;
        dm.DevedoresNOME.AsString             := RXDEVEDOR.AsString;
        dm.DevedoresTIPO.AsString             := RXTIPO_DEVEDOR.AsString;
        dm.DevedoresDOCUMENTO.AsString        := RXCPF_CNPJ_DEVEDOR.AsString;
        dm.DevedoresIFP_DETRAN.AsString       := 'O';
        dm.DevedoresIDENTIDADE.AsString       := RXRG_DEVEDOR.AsString;
        dm.DevedoresORGAO.AsString            := '';
        dm.DevedoresCEP.AsString              := GR.PegarNumeroTexto(RXCEP_DEVEDOR.AsString);
        dm.DevedoresENDERECO.AsString         := RXEND_DEVEDOR.AsString;
        dm.DevedoresBAIRRO.AsString           := RXBAIRRO_DEVEDOR.AsString;
        dm.DevedoresMUNICIPIO.AsString        := RXCID_DEVEDOR.AsString;
        dm.DevedoresUF.AsString               := RXUF.AsString;
        dm.DevedoresJUSTIFICATIVA.AsString    := '';
        dm.DevedoresIGNORADO.AsString         := 'N';

        dm.Devedores.Post;

        sMsgErro := 'ApplyUpdates dm.Devedores';

        GR.ExecutarSQLDAC('UPDATE IDS ' +
                         '   SET VALOR = ' + IntToStr(IdDev) +
                         ' WHERE CAMPO = ' + QuotedStr('ID_DEVEDOR'), dm.conSISTEMA);

        dm.Devedores.ApplyUpdates(0);

        sMsgErro := ''; *)

        {if not RX.Eof then
        begin
          RX.Next;

          if not RX.Eof then
          begin
            if RXProtocolo.AsInteger = vProtocoloAtual then
            begin }
