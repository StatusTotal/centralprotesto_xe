object FCertidao2Via: TFCertidao2Via
  Left = 435
  Top = 239
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'CERTID'#195'O NORMAL DE 2'#170' VIA'
  ClientHeight = 326
  ClientWidth = 389
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 15
  object ckEmitir: TsCheckBox
    Left = 6
    Top = 291
    Width = 177
    Height = 20
    Caption = 'Salvar e Emitir Certid'#227'o'
    Checked = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    State = cbChecked
    TabOrder = 0
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object GbCertidao: TsGroupBox
    Left = 7
    Top = 6
    Width = 372
    Height = 268
    Caption = 'C E R T I D '#195' O'
    TabOrder = 1
    CaptionLayout = clTopCenter
    SkinData.SkinSection = 'BARTITLE'
    CaptionSkin = 'EDIT'
    object sLabel1: TsLabel
      Left = 15
      Top = 24
      Width = 58
      Height = 15
      Alignment = taRightJustify
      Caption = 'Protocolo:'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object sLabel2: TsLabel
      Left = 33
      Top = 40
      Width = 40
      Height = 15
      Alignment = taRightJustify
      Caption = 'Status:'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object sBevel1: TsBevel
      Left = 15
      Top = 60
      Width = 338
      Height = 2
      Shape = bsFrame
    end
    object lbProtocolo: TsLabel
      Left = 77
      Top = 24
      Width = 35
      Height = 15
      Caption = '00000'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
    end
    object lbStatus: TsLabel
      Left = 77
      Top = 40
      Width = 35
      Height = 15
      Caption = 'Status'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
    end
    object edFolhas: TsDBEdit
      Left = 313
      Top = 81
      Width = 40
      Height = 22
      Color = 16777088
      DataField = 'ALEATORIO'
      DataSource = dsCertidao
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Layout = sclTopLeft
    end
    object edDataCertidao: TsDBDateEdit
      Left = 15
      Top = 81
      Width = 97
      Height = 22
      Hint = 'DATA DA CERTID'#195'O'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data da Certid'#227'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DA CERTID'#195'O'
      DataField = 'DT_CERTIDAO'
      DataSource = dsCertidao
    end
    object edSelo: TsDBEdit
      Left = 223
      Top = 81
      Width = 82
      Height = 22
      Hint = 'SELO UTILIZADO NA CERTID'#195'O'
      CharCase = ecUpperCase
      Color = 8454143
      DataField = 'SELO'
      DataSource = dsCertidao
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Selo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object lkEscrevente: TsDBLookupComboBox
      Left = 15
      Top = 129
      Width = 338
      Height = 22
      Hint = 'ESCREVENTE QUE REALIZOU A BUSCA'
      Color = clWhite
      DataField = 'ESCREVENTE'
      DataSource = dsCertidao
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyField = 'CPF'
      ListField = 'LOGIN'
      ListSource = dsEscreventes
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Escrevente'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
    end
    object MObservacao: TsDBMemo
      Left = 15
      Top = 178
      Width = 338
      Height = 73
      Color = clWhite
      DataField = 'OBSERVACAO'
      DataSource = dsCertidao
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 5
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Observa'#231#227'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
    end
    object edEntrega: TsDBDateEdit
      Left = 119
      Top = 81
      Width = 97
      Height = 22
      Hint = 'DATA DE ENTREGA'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data da Entrega'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DE ENTREGA'
      DataField = 'DT_ENTREGA'
      DataSource = dsCertidao
    end
  end
  object btSalvar: TsBitBtn
    Left = 204
    Top = 287
    Width = 80
    Height = 28
    Cursor = crHandPoint
    Caption = 'Salvar'
    TabOrder = 2
    OnClick = btSalvarClick
    ImageIndex = 3
    Images = dm.Imagens
    SkinData.SkinSection = 'BUTTON'
  end
  object btCancelar: TsBitBtn
    Left = 291
    Top = 287
    Width = 88
    Height = 28
    Cursor = crHandPoint
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = btCancelarClick
    ImageIndex = 5
    Images = dm.Imagens
    SkinData.SkinSection = 'BUTTON'
  end
  object dsCertidao: TDataSource
    DataSet = dm.Certidoes
    Left = 240
    Top = 200
  end
  object dsEscreventes: TDataSource
    DataSet = dm.Escreventes
    Left = 163
    Top = 200
  end
end
