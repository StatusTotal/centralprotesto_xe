object FRelDiario: TFRelDiario
  Left = 646
  Top = 369
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Movimento Di'#225'rio'
  ClientHeight = 169
  ClientWidth = 275
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 18
  object sPanel1: TsPanel
    Left = 8
    Top = 8
    Width = 258
    Height = 119
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object edInicio: TsDateEdit
      Left = 16
      Top = 32
      Width = 105
      Height = 26
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 0
      Text = '  /  /    '
      OnChange = edInicioChange
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'In'#237'cio'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -16
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      PopupHeight = 180
      PopupWidth = 220
    end
    object edFim: TsDateEdit
      Left = 136
      Top = 32
      Width = 105
      Height = 26
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Fim'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -16
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      PopupHeight = 180
      PopupWidth = 220
    end
  end
  object btVisualizar: TsBitBtn
    Left = 93
    Top = 85
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Caption = 'Visualizar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = btVisualizarClick
    SkinData.SkinSection = 'BUTTON'
  end
  object btSair: TsBitBtn
    Left = 174
    Top = 85
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Caption = 'Sair'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = btSairClick
    SkinData.SkinSection = 'BUTTON'
  end
  object Relatorio: TQuickRep
    Left = 106
    Top = 328
    Width = 898
    Height = 635
    ShowingPreview = False
    DataSet = RX
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnEndPage = RelatorioEndPage
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poLandscape
    Page.PaperSize = A4
    Page.Continuous = False
    Page.Values = (
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = False
    PrinterSettings.CustomBinCode = 0
    PrinterSettings.ExtendedDuplex = 0
    PrinterSettings.UseCustomPaperCode = False
    PrinterSettings.CustomPaperCode = 0
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.MemoryLimit = 1000000
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 0
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 80
    PrevFormStyle = fsNormal
    PreviewInitialState = wsMaximized
    PreviewWidth = 500
    PreviewHeight = 500
    PrevInitialZoom = qrZoomToWidth
    PreviewDefaultSaveType = stQRP
    PreviewLeft = 0
    PreviewTop = 0
    object QRBand1: TQRBand
      Left = 30
      Top = 30
      Width = 838
      Height = 75
      Frame.DrawTop = True
      Frame.DrawLeft = True
      Frame.DrawRight = True
      AlignToBottom = False
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        248.046875000000000000
        2771.510416666667000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbTitle
      object QRLabel1: TQRLabel
        Left = 8
        Top = 60
        Width = 34
        Height = 13
        Size.Values = (
          42.994791666666670000
          26.458333333333330000
          198.437500000000000000
          112.447916666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Servi'#231'o'
        Color = clWhite
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRLabel2: TQRLabel
        Left = 220
        Top = 60
        Width = 32
        Height = 13
        Size.Values = (
          42.994791666666670000
          727.604166666666700000
          198.437500000000000000
          105.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Grat.'
        Color = clWhite
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRLabel3: TQRLabel
        Left = 257
        Top = 60
        Width = 38
        Height = 13
        Size.Values = (
          42.994791666666670000
          849.973958333333300000
          198.437500000000000000
          125.677083333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'N. Grat.'
        Color = clWhite
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRLabel4: TQRLabel
        Left = 301
        Top = 60
        Width = 65
        Height = 13
        Size.Values = (
          42.994791666666670000
          995.494791666666700000
          198.437500000000000000
          214.973958333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Emolumentos'
        Color = clWhite
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRLabel5: TQRLabel
        Left = 371
        Top = 60
        Width = 40
        Height = 13
        Size.Values = (
          42.994791666666670000
          1227.005208333333000000
          198.437500000000000000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'FETJ'
        Color = clWhite
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRLabel6: TQRLabel
        Left = 415
        Top = 60
        Width = 55
        Height = 13
        Size.Values = (
          42.994791666666670000
          1372.526041666667000000
          198.437500000000000000
          181.901041666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'FUNDPERJ'
        Color = clWhite
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRLabel7: TQRLabel
        Left = 474
        Top = 60
        Width = 48
        Height = 13
        Size.Values = (
          42.994791666666670000
          1567.656250000000000000
          198.437500000000000000
          158.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'FUNPERJ'
        Color = clWhite
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRLabel8: TQRLabel
        Left = 685
        Top = 60
        Width = 46
        Height = 13
        Size.Values = (
          42.994791666666670000
          2265.494791666667000000
          198.437500000000000000
          152.135416666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'M'#250'tua'
        Color = clWhite
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRLabel9: TQRLabel
        Left = 736
        Top = 60
        Width = 34
        Height = 13
        Size.Values = (
          42.994791666666670000
          2434.166666666667000000
          198.437500000000000000
          112.447916666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Acoterj'
        Color = clWhite
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRLabel11: TQRLabel
        Left = 808
        Top = 60
        Width = 23
        Height = 13
        Size.Values = (
          42.994791666666670000
          2672.291666666667000000
          198.437500000000000000
          76.067708333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Total'
        Color = clWhite
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRShape1: TQRShape
        Left = 0
        Top = 56
        Width = 837
        Height = 1
        Size.Values = (
          3.307291666666667000
          0.000000000000000000
          185.208333333333300000
          2768.203125000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object lbTitulo: TQRLabel
        Left = 355
        Top = 16
        Width = 127
        Height = 28
        Size.Values = (
          92.604166666666670000
          1174.088541666667000000
          52.916666666666670000
          420.026041666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'PROTESTO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -29
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 22
      end
      object QRLabel12: TQRLabel
        Left = 526
        Top = 60
        Width = 58
        Height = 13
        Size.Values = (
          42.994791666666670000
          1739.635416666667000000
          198.437500000000000000
          191.822916666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'FUNARPEN'
        Color = clWhite
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRLabel10: TQRLabel
        Left = 601
        Top = 60
        Width = 40
        Height = 13
        Size.Values = (
          42.994791666666670000
          1987.682291666667000000
          198.437500000000000000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'PMCMV'
        Color = clWhite
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRLabel13: TQRLabel
        Left = 663
        Top = 60
        Width = 18
        Height = 13
        Size.Values = (
          42.994791666666670000
          2192.734375000000000000
          198.437500000000000000
          59.531250000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'ISS'
        Color = clWhite
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
    end
    object QRBand2: TQRBand
      Left = 30
      Top = 105
      Width = 838
      Height = 20
      Frame.DrawTop = True
      Frame.DrawBottom = True
      Frame.DrawLeft = True
      Frame.DrawRight = True
      AlignToBottom = False
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        66.145833333333330000
        2771.510416666667000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object QRDBText1: TQRDBText
        Left = 8
        Top = 4
        Width = 209
        Height = 13
        Size.Values = (
          42.994791666666670000
          26.458333333333330000
          13.229166666666670000
          691.223958333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Servico'
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRDBText2: TQRDBText
        Left = 220
        Top = 4
        Width = 32
        Height = 13
        Size.Values = (
          42.994791666666670000
          727.604166666666700000
          13.229166666666670000
          105.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Gratuitos'
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRDBText3: TQRDBText
        Left = 257
        Top = 4
        Width = 38
        Height = 13
        Size.Values = (
          42.994791666666670000
          849.973958333333300000
          13.229166666666670000
          125.677083333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'NaoGratuitos'
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRDBText4: TQRDBText
        Left = 301
        Top = 4
        Width = 65
        Height = 13
        Size.Values = (
          42.994791666666670000
          995.494791666666700000
          13.229166666666670000
          214.973958333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Emolumentos'
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRDBText5: TQRDBText
        Left = 371
        Top = 4
        Width = 40
        Height = 13
        Size.Values = (
          42.994791666666670000
          1227.005208333333000000
          13.229166666666670000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Fetj'
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRDBText6: TQRDBText
        Left = 415
        Top = 4
        Width = 55
        Height = 13
        Size.Values = (
          42.994791666666670000
          1372.526041666667000000
          13.229166666666670000
          181.901041666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Fundperj'
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRDBText7: TQRDBText
        Left = 474
        Top = 4
        Width = 48
        Height = 13
        Size.Values = (
          42.994791666666670000
          1567.656250000000000000
          13.229166666666670000
          158.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Funperj'
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRDBText8: TQRDBText
        Left = 685
        Top = 4
        Width = 46
        Height = 13
        Size.Values = (
          42.994791666666670000
          2265.494791666667000000
          13.229166666666670000
          152.135416666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Mutua'
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRDBText9: TQRDBText
        Left = 736
        Top = 4
        Width = 34
        Height = 13
        Size.Values = (
          42.994791666666670000
          2434.166666666667000000
          13.229166666666670000
          112.447916666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Acoterj'
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRDBText11: TQRDBText
        Left = 808
        Top = 4
        Width = 23
        Height = 13
        Size.Values = (
          42.994791666666670000
          2672.291666666667000000
          13.229166666666670000
          76.067708333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'Total'
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRDBText12: TQRDBText
        Left = 526
        Top = 4
        Width = 58
        Height = 13
        Size.Values = (
          42.994791666666670000
          1739.635416666667000000
          13.229166666666670000
          191.822916666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Funarpen'
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRDBText10: TQRDBText
        Left = 588
        Top = 4
        Width = 53
        Height = 13
        Size.Values = (
          42.994791666666670000
          1944.687500000000000000
          13.229166666666670000
          175.286458333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'PMCMV'
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRDBText13: TQRDBText
        Left = 646
        Top = 4
        Width = 35
        Height = 13
        Size.Values = (
          42.994791666666670000
          2136.510416666667000000
          13.229166666666670000
          115.755208333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Iss'
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
    end
    object QRBand3: TQRBand
      Left = 30
      Top = 125
      Width = 838
      Height = 32
      AlignToBottom = False
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        105.833333333333300000
        2771.510416666667000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbSummary
      object QRExpr1: TQRExpr
        Left = 221
        Top = 8
        Width = 32
        Height = 13
        Size.Values = (
          42.994791666666670000
          730.911458333333300000
          26.458333333333330000
          105.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Color = clWhite
        ParentFont = False
        ResetAfterPrint = False
        Transparent = True
        Expression = 'Sum(RX.Gratuitos)'
        Mask = '#####0'
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRExpr2: TQRExpr
        Left = 258
        Top = 8
        Width = 38
        Height = 13
        Size.Values = (
          42.994791666666670000
          853.281250000000000000
          26.458333333333330000
          125.677083333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Color = clWhite
        ParentFont = False
        ResetAfterPrint = False
        Transparent = True
        Expression = 'Sum(RX.NaoGratuitos)'
        Mask = '#####0'
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRExpr3: TQRExpr
        Left = 302
        Top = 8
        Width = 65
        Height = 13
        Size.Values = (
          42.994791666666670000
          998.802083333333300000
          26.458333333333330000
          214.973958333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Color = clWhite
        ParentFont = False
        ResetAfterPrint = False
        Transparent = True
        Expression = 'Sum(RX.Emolumentos)'
        Mask = '###,##0.00'
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRExpr4: TQRExpr
        Left = 372
        Top = 8
        Width = 40
        Height = 13
        Size.Values = (
          42.994791666666670000
          1230.312500000000000000
          26.458333333333330000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Color = clWhite
        ParentFont = False
        ResetAfterPrint = False
        Transparent = True
        Expression = 'Sum(RX.Fetj)'
        Mask = '###,##0.00'
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRExpr5: TQRExpr
        Left = 416
        Top = 8
        Width = 55
        Height = 13
        Size.Values = (
          42.994791666666670000
          1375.833333333333000000
          26.458333333333330000
          181.901041666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Color = clWhite
        ParentFont = False
        ResetAfterPrint = False
        Transparent = True
        Expression = 'Sum(RX.Fundperj)'
        Mask = '###,##0.00'
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRExpr6: TQRExpr
        Left = 475
        Top = 8
        Width = 48
        Height = 13
        Size.Values = (
          42.994791666666670000
          1570.963541666667000000
          26.458333333333330000
          158.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Color = clWhite
        ParentFont = False
        ResetAfterPrint = False
        Transparent = True
        Expression = 'Sum(RX.Funperj)'
        Mask = '###,##0.00'
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRExpr7: TQRExpr
        Left = 685
        Top = 8
        Width = 46
        Height = 13
        Size.Values = (
          42.994791666666670000
          2265.494791666667000000
          26.458333333333330000
          152.135416666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Color = clWhite
        ParentFont = False
        ResetAfterPrint = False
        Transparent = True
        Expression = 'Sum(RX.Mutua)'
        Mask = '###,##0.00'
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRExpr8: TQRExpr
        Left = 736
        Top = 8
        Width = 34
        Height = 13
        Size.Values = (
          42.994791666666670000
          2434.166666666667000000
          26.458333333333330000
          112.447916666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Color = clWhite
        ParentFont = False
        ResetAfterPrint = False
        Transparent = True
        Expression = 'Sum(RX.Acoterj)'
        Mask = '###,##0.00'
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRExpr10: TQRExpr
        Left = 776
        Top = 8
        Width = 55
        Height = 13
        Size.Values = (
          42.994791666666670000
          2566.458333333333000000
          26.458333333333330000
          181.901041666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Color = clWhite
        ParentFont = False
        ResetAfterPrint = False
        Transparent = True
        Expression = 'Sum(RX.Total)'
        Mask = '###,##0.00'
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRExpr11: TQRExpr
        Left = 527
        Top = 8
        Width = 58
        Height = 13
        Size.Values = (
          42.994791666666670000
          1742.942708333333000000
          26.458333333333330000
          191.822916666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Color = clWhite
        ParentFont = False
        ResetAfterPrint = False
        Transparent = True
        Expression = 'Sum(RX.Funarpen)'
        Mask = '###,##0.00'
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRExpr9: TQRExpr
        Left = 589
        Top = 9
        Width = 53
        Height = 13
        Size.Values = (
          42.994791666666670000
          1947.994791666667000000
          29.765625000000000000
          175.286458333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Color = clWhite
        ParentFont = False
        ResetAfterPrint = False
        Transparent = True
        Expression = 'Sum(RX.PMCMV)'
        Mask = '###,##0.00'
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRExpr12: TQRExpr
        Left = 646
        Top = 9
        Width = 36
        Height = 13
        Size.Values = (
          42.994791666666670000
          2136.510416666667000000
          29.765625000000000000
          119.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Color = clWhite
        ParentFont = False
        ResetAfterPrint = False
        Transparent = True
        Expression = 'Sum(RX.Iss)'
        Mask = '###,##0.00'
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
    end
  end
  object Barra: TsProgressBar
    Left = 8
    Top = 137
    Width = 258
    Height = 23
    TabOrder = 4
    SkinData.SkinSection = 'GAUGE'
  end
  object RX: TRxMemoryData
    FieldDefs = <>
    Left = 286
    Top = 32
    object RXServico: TStringField
      FieldName = 'Servico'
      Size = 50
    end
    object RXGratuitos: TIntegerField
      FieldName = 'Gratuitos'
      DisplayFormat = '#####0'
      EditFormat = '#####0'
    end
    object RXNaoGratuitos: TIntegerField
      FieldName = 'NaoGratuitos'
      DisplayFormat = '#####0'
      EditFormat = '#####0'
    end
    object RXEmolumentos: TFloatField
      FieldName = 'Emolumentos'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXFetj: TFloatField
      FieldName = 'Fetj'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXFundperj: TFloatField
      FieldName = 'Fundperj'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXFunperj: TFloatField
      FieldName = 'Funperj'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXMutua: TFloatField
      FieldName = 'Mutua'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXAcoterj: TFloatField
      FieldName = 'Acoterj'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXDistribuicao: TFloatField
      FieldName = 'Distribuicao'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXTotal: TFloatField
      FieldName = 'Total'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXFunarpen: TFloatField
      FieldName = 'Funarpen'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXPMCMV: TFloatField
      FieldName = 'PMCMV'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXIss: TFloatField
      FieldName = 'Iss'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
  end
  object Cancelados: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select Sum(C.EMOLUMENTOS) as Emolumentos,SUM(C.FETJ) as Fetj,'
      
        'Sum(C.Fundperj) as Fundperj,Sum(C.Funarpen) as Funarpen,Sum(C.PM' +
        'CMV) as PMCMV,Sum(C.MUTUA) as Mutua, Sum(C.Acoterj) as Acoterj, ' +
        'Sum(C.Distribuicao) as Distribuicao,Sum(C.Iss) as ISS,'
      'Sum(C.Total) as Total from TITULOS T, CERTIDOES C'
      ''
      'where C.DT_PEDIDO between :D1 and :D2'
      ''
      'and T.Cobranca=:COBRANCA'
      ''
      'and T.CONVENIO=:CONVENIO AND T.ID_ATO=C.ID_ATO')
    Left = 385
    Top = 32
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object CanceladosEMOLUMENTOS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosFETJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FETJ'
      Origin = 'FETJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosFUNDPERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosFUNARPEN: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosPMCMV: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosMUTUA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosACOTERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosDISTRIBUICAO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosISS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ISS'
      Origin = 'ISS'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosTOTAL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object Apontados: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select Sum(EMOLUMENTOS) as Emolumentos,SUM(FETJ) as Fetj,'
      
        'Sum(Fundperj) as Fundperj,Sum(Funarpen) as Funarpen,Sum(PMCMV) a' +
        's PMCMV,Sum(MUTUA) as Mutua, Sum(Acoterj) as Acoterj, Sum(Distri' +
        'buicao) as Distribuicao, Sum(Iss) as ISS,Sum(Total) as Total fro' +
        'm TITULOS'
      ''
      'where CODIGO=:CODIGO and DT_PROTOCOLO between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA'
      ''
      'and CONVENIO=:CONVENIO')
    Left = 385
    Top = 88
    ParamData = <
      item
        Name = 'CODIGO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object ApontadosEMOLUMENTOS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosFETJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FETJ'
      Origin = 'FETJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosFUNDPERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosFUNARPEN: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosPMCMV: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosMUTUA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosACOTERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosDISTRIBUICAO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosISS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ISS'
      Origin = 'ISS'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosTOTAL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object CanceladosConvenio: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select Sum(EMOLUMENTOS) as Emolumentos,SUM(FETJ) as Fetj,'
      
        'Sum(Fundperj) as Fundperj,Sum(Funarpen) as Funarpen,Sum(PMCMV) a' +
        's PMCMV,Sum(MUTUA) as Mutua, Sum(Acoterj) as Acoterj, Sum(Distri' +
        'buicao) as Distribuicao,Sum(Iss) as ISS,'
      
        '(Sum(Total)-Sum(Coalesce(Apontamento,0))) as Total from CERTIDOE' +
        'S'
      ''
      'where DT_PEDIDO between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA'
      ''
      'and CONVENIO='#39'S'#39' and TIPO_CERTIDAO='#39'X'#39)
    Left = 385
    Top = 144
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end>
    object CanceladosConvenioEMOLUMENTOS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosConvenioFETJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FETJ'
      Origin = 'FETJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosConvenioFUNDPERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosConvenioFUNARPEN: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosConvenioPMCMV: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosConvenioMUTUA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosConvenioACOTERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosConvenioDISTRIBUICAO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosConvenioISS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ISS'
      Origin = 'ISS'
      ProviderFlags = []
      ReadOnly = True
    end
    object CanceladosConvenioTOTAL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object Serasa: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select Sum(EMOLUMENTOS) as Emolumentos,SUM(FETJ) as Fetj,'
      
        'Sum(Fundperj) as Fundperj,Sum(Funarpen) as Funarpen,Sum(PMCMV) a' +
        's PMCMV,Sum(MUTUA) as Mutua, Sum(Acoterj) as Acoterj, Sum(Distri' +
        'buicao) as Distribuicao,Sum(Iss) as Iss,Sum(Total) as Total from' +
        ' CERTIDOES'
      ''
      'where DT_CERTIDAO between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA and '
      ''
      '(CODIGO=4011)'
      ''
      'and CONVENIO=:CONVENIO')
    Left = 385
    Top = 200
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object SerasaEMOLUMENTOS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
      ProviderFlags = []
      ReadOnly = True
    end
    object SerasaFETJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FETJ'
      Origin = 'FETJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object SerasaFUNDPERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object SerasaFUNARPEN: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
      ProviderFlags = []
      ReadOnly = True
    end
    object SerasaPMCMV: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
      ProviderFlags = []
      ReadOnly = True
    end
    object SerasaMUTUA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
      ProviderFlags = []
      ReadOnly = True
    end
    object SerasaACOTERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object SerasaDISTRIBUICAO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
      ProviderFlags = []
      ReadOnly = True
    end
    object SerasaISS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ISS'
      Origin = 'ISS'
      ProviderFlags = []
      ReadOnly = True
    end
    object SerasaTOTAL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object ApontadosConvenios: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select'
      ''
      'T.COBRANCA,'
      'COUNT(T.COBRANCA) as Qtd,'
      'Sum(T.EMOLUMENTOS) as Emolumentos,'
      'Sum(T.FETJ) as Fetj,'
      'Sum(T.FUNDPERJ) as Fundperj,'
      'Sum(T.FUNPERJ) as Funperj,'
      'Sum(T.FUNARPEN) as Funarpen,'
      'Sum(T.PMCMV) as Pmcmv,'
      'Sum(T.MUTUA) as Mutua,'
      'Sum(T.ACOTERJ) as Acoterj,'
      'Sum(T.DISTRIBUICAO) as Distribuicao,'
      'Sum(T.ISS) as Iss,'
      'Sum(T.TOTAL) as Total'
      ''
      'from TITULOS T, CERTIDOES C'
      ''
      'where'
      ''
      'T.ID_ATO=C.ID_ATO and C.DT_PEDIDO between :D1 and :D2 '
      'AND C.CONVENIO='#39'S'#39' AND C.TIPO_CERTIDAO='#39'X'#39
      ''
      'GROUP BY T.COBRANCA')
    Left = 385
    Top = 256
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end>
    object ApontadosConveniosCOBRANCA: TStringField
      FieldName = 'COBRANCA'
      Origin = 'COBRANCA'
      FixedChar = True
      Size = 2
    end
    object ApontadosConveniosQTD: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'QTD'
      Origin = 'QTD'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosConveniosEMOLUMENTOS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosConveniosFETJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FETJ'
      Origin = 'FETJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosConveniosFUNDPERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosConveniosFUNPERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNPERJ'
      Origin = 'FUNPERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosConveniosFUNARPEN: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosConveniosPMCMV: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosConveniosMUTUA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosConveniosACOTERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosConveniosDISTRIBUICAO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosConveniosISS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ISS'
      Origin = 'ISS'
      ProviderFlags = []
      ReadOnly = True
    end
    object ApontadosConveniosTOTAL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object QCancelados: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select Count(ID_ATO) as QTD from CERTIDOES'
      ''
      'where DT_PEDIDO between :D1 and :D2'
      ''
      
        'and Cobranca=:COBRANCA and CONVENIO=:CONVENIO and TIPO_CERTIDAO=' +
        #39'X'#39)
    Left = 479
    Top = 32
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object QCanceladosQTD: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'QTD'
      Origin = 'QTD'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object CApontados: TFDQuery
    Connection = dm.conCENTRAL
    SQL.Strings = (
      'SELECT COD,TITULO FROM COD WHERE '
      ''
      'COD BETWEEN :C1 AND :C2')
    Left = 479
    Top = 88
    ParamData = <
      item
        Name = 'C1'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'C2'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object CApontadosCOD: TIntegerField
      FieldName = 'COD'
      Origin = 'COD'
    end
    object CApontadosTITULO: TStringField
      FieldName = 'TITULO'
      Origin = 'TITULO'
      Size = 50
    end
  end
  object Folhas: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      'Sum(Coalesce(EXEMOLUMENTOS,0)) as EMOLUMENTOS,'
      'Sum(Coalesce(EXFETJ,0)) as FETJ,'
      'Sum(Coalesce(EXFUNDPERJ,0)) as FUNDPERJ,'
      'Sum(Coalesce(EXFUNARPEN,0)) as FUNARPEN,'
      'Sum(Coalesce(EXISS,0)) as ISS,'
      'Sum(Coalesce(EXTOTAL,0)) as Total from CERTIDOES'
      ''
      'where DT_ENTREGA between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA and '
      ''
      '(TIPO_CERTIDAO='#39'N'#39' or TIPO_CERTIDAO='#39'I'#39')'
      ''
      'and CONVENIO=:CONVENIO and FOLHAS>1')
    Left = 479
    Top = 144
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object FolhasEMOLUMENTOS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
      ProviderFlags = []
      ReadOnly = True
    end
    object FolhasFETJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FETJ'
      Origin = 'FETJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object FolhasFUNDPERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object FolhasFUNARPEN: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
      ProviderFlags = []
      ReadOnly = True
    end
    object FolhasISS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ISS'
      Origin = 'ISS'
      ProviderFlags = []
      ReadOnly = True
    end
    object FolhasTOTAL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object Pagos: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      ''
      'Sum(EMOLUMENTOS) as Emolumentos,SUM(FETJ) as Fetj,'
      
        'Sum(Fundperj) as Fundperj,Sum(Funarpen) as Funarpen,Sum(PMCMV) a' +
        's PMCMV,Sum(MUTUA) as Mutua, Sum(Acoterj) as Acoterj, Sum(Distri' +
        'buicao) as Distribuicao,Sum(Iss) as Iss, Sum(Tarifa_Bancaria) as' +
        ' Tarifa,'
      'Sum(Total) as Total from TITULOS'
      ''
      'where DT_PAGAMENTO between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA and STATUS='#39'PAGO'#39
      ''
      'and CONVENIO=:CONVENIO')
    Left = 479
    Top = 200
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object PagosEMOLUMENTOS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
      ProviderFlags = []
      ReadOnly = True
    end
    object PagosFETJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FETJ'
      Origin = 'FETJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object PagosFUNDPERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object PagosFUNARPEN: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
      ProviderFlags = []
      ReadOnly = True
    end
    object PagosPMCMV: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
      ProviderFlags = []
      ReadOnly = True
    end
    object PagosMUTUA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
      ProviderFlags = []
      ReadOnly = True
    end
    object PagosACOTERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object PagosDISTRIBUICAO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
      ProviderFlags = []
      ReadOnly = True
    end
    object PagosISS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ISS'
      Origin = 'ISS'
      ProviderFlags = []
      ReadOnly = True
    end
    object PagosTARIFA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TARIFA'
      Origin = 'TARIFA'
      ProviderFlags = []
      ReadOnly = True
    end
    object PagosTOTAL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object Protestos: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      ''
      'Sum(EMOLUMENTOS) as Emolumentos,SUM(FETJ) as Fetj,'
      
        'Sum(Fundperj) as Fundperj,Sum(Funarpen) as Funarpen,Sum(PMCMV) a' +
        's PMCMV,Sum(MUTUA) as Mutua, Sum(Acoterj) as Acoterj, Sum(Distri' +
        'buicao) as Distribuicao,Sum(Iss) as Iss,'
      'Sum(Total) as Total from TITULOS'
      ''
      'where DT_REGISTRO between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA and PROTESTADO='#39'S'#39
      ''
      'and CONVENIO=:CONVENIO')
    Left = 479
    Top = 256
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object ProtestosEMOLUMENTOS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
      ProviderFlags = []
      ReadOnly = True
    end
    object ProtestosFETJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FETJ'
      Origin = 'FETJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object ProtestosFUNDPERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object ProtestosFUNARPEN: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
      ProviderFlags = []
      ReadOnly = True
    end
    object ProtestosPMCMV: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
      ProviderFlags = []
      ReadOnly = True
    end
    object ProtestosMUTUA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
      ProviderFlags = []
      ReadOnly = True
    end
    object ProtestosACOTERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object ProtestosDISTRIBUICAO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
      ProviderFlags = []
      ReadOnly = True
    end
    object ProtestosISS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ISS'
      Origin = 'ISS'
      ProviderFlags = []
      ReadOnly = True
    end
    object ProtestosTOTAL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object Desistencias: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      ''
      'Sum(EMOLUMENTOS) as Emolumentos,SUM(FETJ) as Fetj,'
      
        'Sum(Fundperj) as Fundperj,Sum(Funarpen) as Funarpen,Sum(PMCMV) a' +
        's PMCMV,Sum(MUTUA) as Mutua, Sum(Acoterj) as Acoterj, Sum(Distri' +
        'buicao) as Distribuicao,Sum(Iss) as Iss,'
      'Sum(Total) as Total from TITULOS'
      ''
      'where DT_RETIRADO between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA and STATUS='#39'RETIRADO'#39
      ''
      'and CONVENIO=:CONVENIO')
    Left = 560
    Top = 32
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object DesistenciasEMOLUMENTOS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
      ProviderFlags = []
      ReadOnly = True
    end
    object DesistenciasFETJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FETJ'
      Origin = 'FETJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object DesistenciasFUNDPERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object DesistenciasFUNARPEN: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
      ProviderFlags = []
      ReadOnly = True
    end
    object DesistenciasPMCMV: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
      ProviderFlags = []
      ReadOnly = True
    end
    object DesistenciasMUTUA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
      ProviderFlags = []
      ReadOnly = True
    end
    object DesistenciasACOTERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object DesistenciasDISTRIBUICAO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
      ProviderFlags = []
      ReadOnly = True
    end
    object DesistenciasISS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ISS'
      Origin = 'ISS'
      ProviderFlags = []
      ReadOnly = True
    end
    object DesistenciasTOTAL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object CertidaoN: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      'Sum(Coalesce(EMOLUMENTOS,0)) as EMOLUMENTOS,'
      'Sum(Coalesce(FETJ,0)) as FETJ,'
      'Sum(Coalesce(FUNDPERJ,0)) as FUNDPERJ,'
      'Sum(Coalesce(FUNARPEN,0)) as FUNARPEN,'
      'Sum(Coalesce(PMCMV,0)) as PMCMV,'
      'Sum(Coalesce(MUTUA,0)) as MUTUA,'
      'Sum(Coalesce(ACOTERJ,0)) as ACOTERJ,'
      'Sum(Coalesce(DISTRIBUICAO,0)) as DISTRIBUICAO,'
      'Sum(Coalesce(ISS,0)) as ISS,'
      'Sum(Coalesce(TOTAL,0)) as Total from CERTIDOES'
      ''
      'where DT_PEDIDO between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA and '
      ''
      '(TIPO_CERTIDAO='#39'N'#39')'
      ''
      'and CONVENIO=:CONVENIO')
    Left = 560
    Top = 88
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object CertidaoNEMOLUMENTOS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoNFETJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FETJ'
      Origin = 'FETJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoNFUNDPERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoNFUNARPEN: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoNPMCMV: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoNMUTUA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoNACOTERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoNDISTRIBUICAO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoNISS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ISS'
      Origin = 'ISS'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoNTOTAL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object CertidaoI: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      'Sum(Coalesce(EMOLUMENTOS,0)) as EMOLUMENTOS,'
      'Sum(Coalesce(FETJ,0)) as FETJ,'
      'Sum(Coalesce(FUNDPERJ,0)) as FUNDPERJ,'
      'Sum(Coalesce(FUNARPEN,0)) as FUNARPEN,'
      'Sum(Coalesce(PMCMV,0)) as PMCMV,'
      'Sum(Coalesce(MUTUA,0)) as MUTUA,'
      'Sum(Coalesce(ACOTERJ,0)) as ACOTERJ,'
      'Sum(Coalesce(DISTRIBUICAO,0)) as DISTRIBUICAO,'
      'Sum(Coalesce(ISS,0)) as ISS,'
      'Sum(Coalesce(TOTAL,0)) as Total from CERTIDOES'
      ''
      'where DT_PEDIDO between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA and '
      ''
      '(TIPO_CERTIDAO='#39'I'#39')'
      ''
      'and CONVENIO=:CONVENIO')
    Left = 560
    Top = 144
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object CertidaoIEMOLUMENTOS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoIFETJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FETJ'
      Origin = 'FETJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoIFUNDPERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoIFUNARPEN: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoIPMCMV: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoIMUTUA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoIACOTERJ: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoIDISTRIBUICAO: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoIISS: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'ISS'
      Origin = 'ISS'
      ProviderFlags = []
      ReadOnly = True
    end
    object CertidaoITOTAL: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object QApontados: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select Count(ID_ATO) as QTD from TITULOS'
      ''
      'where CODIGO=:CODIGO and DT_PROTOCOLO between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA'
      ''
      'and CONVENIO=:CONVENIO')
    Left = 667
    Top = 32
    ParamData = <
      item
        Name = 'CODIGO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object QApontadosQTD: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'QTD'
      Origin = 'QTD'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object QDesistencia: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select Count(ID_ATO) as QTD from TITULOS'
      ''
      'where DT_RETIRADO between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA and STATUS='#39'RETIRADO'#39
      ''
      'and CONVENIO=:CONVENIO')
    Left = 667
    Top = 88
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object QDesistenciaQTD: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'QTD'
      Origin = 'QTD'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object QPagos: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select Count(ID_ATO) as QTD from TITULOS'
      ''
      'where DT_PAGAMENTO between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA and STATUS='#39'PAGO'#39
      ''
      'and CONVENIO=:CONVENIO')
    Left = 667
    Top = 144
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object QPagosQTD: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'QTD'
      Origin = 'QTD'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object QCertidaoN: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select Count(ID_CERTIDAO) as QTD from CERTIDOES'
      ''
      'where DT_PEDIDO between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA and '
      ''
      '(TIPO_CERTIDAO='#39'N'#39')'
      ''
      'and CONVENIO=:CONVENIO')
    Left = 667
    Top = 200
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object QCertidaoNQTD: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'QTD'
      Origin = 'QTD'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object QCertidaoI: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select Count(ID_CERTIDAO) as QTD from CERTIDOES'
      ''
      'where DT_PEDIDO between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA and '
      ''
      '(TIPO_CERTIDAO='#39'I'#39')'
      ''
      'and CONVENIO=:CONVENIO')
    Left = 667
    Top = 256
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object QCertidaoIQTD: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'QTD'
      Origin = 'QTD'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object QGuias: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      
        'select Count(ID_ATO) as QTD,SUM(TARIFA_BANCARIA) as Guia from TI' +
        'TULOS'
      ''
      'where '
      ''
      'DT_PAGAMENTO between :D1 and :D2'
      ''
      'and TARIFA_BANCARIA>0'
      ''
      'and Cobranca=:COBRANCA'
      ''
      'and CONVENIO=:CONVENIO'
      ''
      'and STATUS='#39'PAGO'#39)
    Left = 744
    Top = 32
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object QGuiasQTD: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'QTD'
      Origin = 'QTD'
      ProviderFlags = []
      ReadOnly = True
    end
    object QGuiasGUIA: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'GUIA'
      Origin = 'GUIA'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object QProtestos: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select Count(ID_ATO) as QTD from TITULOS'
      ''
      'where DT_REGISTRO between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA and PROTESTADO='#39'S'#39
      ''
      'and CONVENIO=:CONVENIO')
    Left = 744
    Top = 88
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object QProtestosQTD: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'QTD'
      Origin = 'QTD'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object QSerasa: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select Count(ID_CERTIDAO) as QTD from CERTIDOES'
      ''
      'where DT_CERTIDAO between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA and '
      ''
      'CODIGO=4011'
      ''
      'and CONVENIO=:CONVENIO')
    Left = 744
    Top = 144
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object QSerasaQTD: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'QTD'
      Origin = 'QTD'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object QFolhas: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select Sum(FOLHAS-1) as QTD from CERTIDOES'
      ''
      'where DT_ENTREGA between :D1 and :D2'
      ''
      'and Cobranca=:COBRANCA and '
      ''
      '(TIPO_CERTIDAO='#39'N'#39' or TIPO_CERTIDAO='#39'I'#39')'
      ''
      'and CONVENIO=:CONVENIO'
      ''
      'and FOLHAS>1')
    Left = 744
    Top = 200
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'COBRANCA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object QFolhasQTD: TLargeintField
      AutoGenerateValue = arDefault
      FieldName = 'QTD'
      Origin = 'QTD'
      ProviderFlags = []
      ReadOnly = True
    end
  end
end
