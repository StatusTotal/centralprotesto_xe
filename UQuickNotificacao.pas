unit UQuickNotificacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QuickRpt, QRCtrls, ExtCtrls, DB, RxMemDS, StdCtrls, ComCtrls;

type
  TFQuickNotificacao = class(TForm)
    Notificacao: TQuickRep;
    QRBand1: TQRBand;
    lbCartorio: TQRLabel;
    QRLabel2: TQRLabel;
    lbTabeliao: TQRLabel;
    lbEndereco: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    Detalhe: TQRBand;
    lbApresentante: TQRLabel;
    lbDevedor: TQRLabel;
    lbDocumento: TQRLabel;
    lbTitulo: TQRLabel;
    lbEmissao: TQRLabel;
    lbVencimento: TQRLabel;
    lbValor: TQRLabel;
    lbProtocolo: TQRLabel;
    QRLabel1: TQRLabel;
    RT: TQRRichText;
    RX: TRxMemoryData;
    RE: TRichEdit;
    RXID_ATO: TIntegerField;
    RXPROTOCOLO: TIntegerField;
    RXDT_PROTOCOLO: TDateField;
    RXAPRESENTANTE: TStringField;
    RXDEVEDOR: TStringField;
    RXSTATUS: TStringField;
    RXCPF_CNPJ_DEVEDOR: TStringField;
    RXNUMERO_TITULO: TStringField;
    RXTIPO_TITULO: TIntegerField;
    RXDT_TITULO: TDateField;
    RXDT_VENCIMENTO: TDateField;
    RXTOTAL: TFloatField;
    RXDT_PUBLICACAO: TDateField;
    QRBand2: TQRBand;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    lbData: TQRLabel;
    procedure DetalheBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure NotificacaoEndPage(Sender: TCustomQuickRep);
    procedure Negrito(Texto: String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickNotificacao: TFQuickNotificacao;

implementation

uses UPF, UDM;

{$R *.dfm}

procedure TFQuickNotificacao.Negrito(Texto: String);
var
  posi:integer;
  posf:integer;
begin
    posi:=0;
    posf:=RE.FindText(Texto,posi,Length(RE.Text),[stWholeWord]);
    while posF >= 0 do
    begin
        SetFocus;
        RE.SelStart:=posf;
        RE.SelLength:=Length(Texto);
        RE.SelStart:=posf;
        RE.SelLength:=Length(Texto);
        RE.SelAttributes.Style:=[fsBold];
        posi:=posf+length(Texto)+1;
        posf:=RE.FindText(Texto,posi,length(RE.Text),[stWholeWord]);
    end;
end;

procedure TFQuickNotificacao.DetalheBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  lbApresentante.Caption:='Apresentante/Credor/Protestador: '+RXAPRESENTANTE.AsString;
  lbDevedor.Caption     :='Devedor: '+RXDEVEDOR.AsString;
  lbDocumento.Caption   :='CPF/CNPJ: '+RXCPF_CNPJ_DEVEDOR.AsString;
  lbTitulo.Caption      :='T�tulo/documento de d�vida: '+PF.RetornarSigla(RXTIPO_TITULO.AsInteger)+' - '+RXNUMERO_TITULO.AsString;
  lbEmissao.Caption     :='Data da Emiss�o: '+FormatDateTime('dd/mm/yyyy',RXDT_TITULO.AsDateTime);
  lbVencimento.Caption  :='Data do Vencimento: '+FormatDateTime('dd/mm/yyyy',RXDT_VENCIMENTO.AsDateTime);
  lbValor.Caption       :='Valor da D�vida: '+FloatToStrF(RXTOTAL.AsFloat,ffNumber,7,2);
  lbProtocolo.Caption   :='Protocolo: '+RXPROTOCOLO.AsString;

  RE.Lines.Clear;
  RE.Lines.Text:='tendo em vista ser residente ou domiciliado fora da compet�ncia territorial deste tabelionato, V.Sa. ser� intimado(a) '+
               'exclusivamente mediante edital, cuja publica��o se dar� no dia '+FormatDateTime('dd/mm/yyyy',RXDT_PUBLICACAO.AsDateTime)+
               ', atrav�s somente da sua afixa��o neste Servi�o, nos termos do par�grafo �nico, do art. 992 da Consolida��o Normativa da '+
               'Corregedoria Geral da Justi�a do Estado do Rio de Janeiro.'+#13#13+'    No primeiro dia �til subsequente � publica��o, na '+
               'aus�ncia de aceite ou eventual pagamento volunt�rio consoante requerido pelo apresentande/credor/protestador, ser� '+
               'registrado o correspondente protesto (art. 21, caput, da Lei Federal n� 9.492/97), independentemente do motivo alegado '+
               'para a recusa do pagamento ou aceite (art. 977, � 2� da Consolida��o Normativa da corregedoria Geral da Justi�a do Estado '+
               'do Rio de Janeiro) bem como ser� encaminhada informa��es aos cadastros vinculados � prote��o ao cr�dito, nos termos dos arts. '+
               '29 e 30 da mesma Lei e da determina��o imposta a esta Serventia pelo art. 1.007 da Consolida��o Normativa da Corregedoria '+
               'Geral da Justi�a do Estado do Rio de Janeiro. (Ver http://www.tj.rj.gov.br/cgj/legis/CNCGJ-Extrajudicial.pdf).'+#13#13+
               '    Tratando-se de requerimento de protesto por falta de aceite, a finalidade da ulterior intima��o por edital ser� a manifesta��o '+
               'de V.Sa. no sentido de lan�ar o seu aceite, pessoalmente, no t�tulo ora indicado ou justificar eventual recusa em aceit�-lo ou pag�-lo '+
               ', que, neste �ltimo caso, n�o impedir� a lavratura do protesto de acordo com o artigo 20 da Lei Federal n� 9.492/1.997.'+#13#13+
               '    Outrossim, fica consignado que havendo justa causa em n�o aceitar ou pagar poder� V.Sa. contar com a medida da susta��o judicial '+
               'prevista no art. 17 da Lei Federal n� 9.492/97.';
  PF.JustificarRichEdit(RE,True);
end;

procedure TFQuickNotificacao.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbData.Caption:=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy',Now);
end;

procedure TFQuickNotificacao.FormCreate(Sender: TObject);
begin
  lbCartorio.Caption:=dm.ServentiaDESCRICAO.AsString;
  lbTabeliao.Caption:=dm.ServentiaTABELIAO.AsString;
  lbEndereco.Caption:=dm.ServentiaENDERECO.AsString+' - '+dm.ServentiaCIDADE.AsString+' - RJ - '+PF.FormatarTelefone(dm.ServentiaTELEFONE.AsString)
end;

procedure TFQuickNotificacao.NotificacaoEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

end.
