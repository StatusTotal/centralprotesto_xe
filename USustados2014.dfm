�
 TFSUSTADO2014 0�  TPF0TFSustado2014FSustado2014Left\Top7BorderIconsbiSystemMenu BorderStylebsDialogCaptionSustadoClientHeightClientWidthColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
KeyPreview	OldCreateOrderPositionpoMainFormCenterOnClose	FormClosePixelsPerInch`
TextHeight TsPanelP1Left Top WidthHeight� AlignalTopTabOrder SkinData.SkinSectionPANEL TsLabellbAleatorioLeft1Top� WidthFHeightAutoSize
ParentFontFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold   
TsDateEditedDataLeft� Top� WidthiHeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	MaxLength

ParentFontTabOrderBoundLabel.Active	BoundLabel.ParentFontBoundLabel.Caption   Data da SustaçãoBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameArialBoundLabel.Font.StylefsBold SkinData.SkinSectionEDITGlyphMode.GrayedGlyphMode.Blend DefaultToday	  TsEditedSeloLeft� Top� WidthiHeightHintSELOCharCaseecUpperCaseColorclWhiteEnabledFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	MaxLength
ParentFontParentShowHintReadOnly	ShowHint	TabOrderSkinData.SkinSectionEDITBoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionSeloBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameArialBoundLabel.Font.StylefsBold   TsRadioGroupRgTipoLeftTopWidthfHeight-CursorcrHandPointCaptionTipoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ParentBackground
ParentFontTabOrder OnClickRgTipoClickSkinData.SkinSectionBARTITLECaptionSkinPAGECONTROLColumns	ItemIndex Items.StringsLiminar
DefinitivaCancelamento   
TsCheckBox
ckImprimirLeftMTop� Width� HeightCursorcrHandPointCaption   Imprimir Carimbo de AverbaçãoChecked	Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontState	cbCheckedTabOrder
ImgChecked ImgUnchecked SkinData.SkinSectionCHECKBOX  TsMemo	MJudicialLeftTopNWidthfHeight+ColorclWhiteEnabledFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	MaxLengthd
ParentFont
ScrollBars
ssVerticalTabOrderBoundLabel.Active	BoundLabel.EnabledAlways	BoundLabel.ParentFontBoundLabel.Caption,   Identificação da determinação definitivaBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.ColorclWindowTextBoundLabel.Font.Height�BoundLabel.Font.NameArialBoundLabel.Font.Style BoundLabel.Layout
sclTopLeftSkinData.SkinSectionEDIT   TsBitBtnbtOkLeftcTop� WidthYHeightCursorcrHandPointCaptionOkTabOrderOnClick	btOkClick
ImageIndexImages
dm.ImagensSkinData.SkinSectionBUTTON  TsBitBtn
btCancelarLeft� Top� WidthYHeightCursorcrHandPointCaptionCancelarTabOrderOnClickbtCancelarClick
ImageIndexImages
dm.ImagensSkinData.SkinSectionBUTTON   