unit UConfirmacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, DB, RxMemDS, Grids, Wwdbigrd,
  Wwdbgrid, ExtCtrls, sPanel, sLabel, sDialogs, Mask, DBCtrls, sDBEdit,
  sMaskEdit, sCustomComboEdit, sTooledit, sDBDateEdit, sDBNavigator,
  sDBText, sGroupBox, sDBRadioGroup, acDBTextFX, Menus, FMTBcd,
  SqlExpr, ClipBrd, DBGrids, RXDBCtrl, ComCtrls, acProgressBar, Gauges,
  jpeg, sSpeedButton, sEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFConfirmacao = class(TForm)
    P1: TsPanel;
    GridAtos: TwwDBGrid;
    RX: TRxMemoryData;
    dsRX: TDataSource;
    btArquivo: TsBitBtn;
    M: TMemo;
    RXRG_DEVEDOR: TStringField;
    RXEND_DEVEDOR: TStringField;
    RXCEP_DEVEDOR: TStringField;
    RXCID_DEVEDOR: TStringField;
    RXUF: TStringField;
    RXBAIRRO_DEVEDOR: TStringField;
    Opd: TsOpenDialog;
    RXEspecie: TStringField;
    RXAPRESENTANTE: TStringField;
    RXCEDENTE: TStringField;
    RXSACADOR: TStringField;
    RXTIPO_TITULO: TIntegerField;
    RXNUMERO_TITULO: TStringField;
    RXDT_TITULO: TDateField;
    RXDT_VENCIMENTO: TDateField;
    RXVALOR_TITULO: TFloatField;
    RXSALDO_PROTESTO: TFloatField;
    RXDEVEDOR: TStringField;
    RXTIPO_DEVEDOR: TStringField;
    RXCPF_CNPJ_DEVEDOR: TStringField;
    P2: TsPanel;
    edDevedor: TsDBEdit;
    edDataTitulo: TsDBDateEdit;
    edValor: TsDBEdit;
    edEspecie: TsDBEdit;
    edDataVencimento: TsDBDateEdit;
    edDocumento: TsDBEdit;
    edEndereco: TsDBEdit;
    edBairro: TsDBEdit;
    edUF: TsDBEdit;
    edCidade: TsDBEdit;
    RXPRACA: TStringField;
    edPraca: TsDBEdit;
    sDBNavigator1: TsDBNavigator;
    RXDocumento: TStringField;
    RXACEITE: TStringField;
    RXENDOSSO: TStringField;
    RXSTATUS: TStringField;
    btApontar: TsBitBtn;
    RXOCORRENCIA: TStringField;
    RXNOSSO_NUMERO: TStringField;
    RgTipo: TsRadioGroup;
    RXAGENCIA: TStringField;
    RXCheck: TBooleanField;
    btExportar: TButton;
    M2: TMemo;
    RXProtocolo: TIntegerField;
    Entrada: TRxMemoryData;
    dsEntrada: TDataSource;
    EntradaLinha: TMemoField;
    Saida: TRxMemoryData;
    dsSaida: TDataSource;
    SaidaLinha: TMemoField;
    RXCUSTAS: TFloatField;
    RXIrregularidade: TStringField;
    btRejeitar: TsBitBtn;
    dsControle: TDataSource;
    RXCPF_CNPJ_SACADOR: TStringField;
    RXEND_SACADOR: TStringField;
    RXCEP_SACADOR: TStringField;
    RXUF_SACADOR: TStringField;
    RXBAIRRO_SACADOR: TStringField;
    RXCID_SACADOR: TStringField;
    RXBC: TRxMemoryData;
    dsRXBC: TDataSource;
    RXBCImportar: TMemoField;
    RXBCExportar: TMemoField;
    RXEN: TRxMemoryData;
    RXENLinha: TMemoField;
    dsRXEN: TDataSource;
    btExportarCRA: TButton;
    Linhas: TRxMemoryData;
    LinhasLinha: TMemoField;
    Portadores: TRxMemoryData;
    PortadoresNome: TStringField;
    wwDBGrid1: TwwDBGrid;
    PB: TsProgressBar;
    lbAviso: TLabel;
    PortadoresAgencia: TStringField;
    PortadoresPraca: TStringField;
    RXDataOcorrencia: TDateField;
    RXDescOcorrencia: TStringField;
    RXDescIrreg: TStringField;
    RXDT_PROTOCOLO: TDateField;
    PM: TPopupMenu;
    MarcarTodos: TMenuItem;
    DesmarcarTodos: TMenuItem;
    PortadoresCodigo: TStringField;
    RXCODIGO_APRESENTANTE: TStringField;
    RXENCodigo: TStringField;
    RXBCCodigo: TStringField;
    qryNumero: TFDQuery;
    qryFaixa: TFDQuery;
    qryNumeroPROTOCOLO: TIntegerField;
    qryNumeroDT_PROTOCOLO: TDateField;
    qryNumeroDT_PAGAMENTO: TDateField;
    qryNumeroDT_REGISTRO: TDateField;
    qryNumeroDT_RETIRADO: TDateField;
    qryNumeroDT_SUSTADO: TDateField;
    qryNumeroSTATUS: TStringField;
    qryNumeroSALDO_PROTESTO: TFloatField;
    qryNumeroVALOR_TITULO: TFloatField;
    qryNumeroDT_DEVOLVIDO: TDateField;
    qryNumeroCONVENIO: TStringField;
    qryNumeroTOTAL: TFloatField;
    qryNumeroTARIFA_BANCARIA: TFloatField;
    qryNumeroVALOR_AR: TFloatField;
    qryNumeroDISTRIBUICAO: TFloatField;
    qryFaixaCOD: TIntegerField;
    qryFaixaEMOL: TFloatField;
    qryFaixaFETJ: TFloatField;
    qryFaixaFUND: TFloatField;
    qryFaixaFUNP: TFloatField;
    qryFaixaMUTUA: TFloatField;
    qryFaixaACOTERJ: TFloatField;
    qryFaixaDISTRIB: TFloatField;
    qryFaixaTOT: TFloatField;
    qryFaixaTITULO: TStringField;
    procedure btArquivoClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    function Texto(Linha,Inicio,Fim: Integer): String;
    procedure dsRXDataChange(Sender: TObject; Field: TField);
    procedure btApontarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GridAtosExit(Sender: TObject);
    procedure btExportarClick(Sender: TObject);
    procedure btRejeitarClick(Sender: TObject);
    procedure RXENFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    function  Maximo: Integer;
    procedure lbAvisoClick(Sender: TObject);
    procedure RXCheckChange(Sender: TField);
    procedure MarcarTodosClick(Sender: TObject);
    procedure DesmarcarTodosClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Arquivo,Convenio: String;
  end;

var
  FConfirmacao: TFConfirmacao;

implementation

uses UDM, UPF, UPortadores, UIrregularidades, UCriticas, UQuickOrdem1, UPrincipal,
  UOcorrencias, UGeral;

{$R *.dfm}

procedure TFConfirmacao.btArquivoClick(Sender: TObject);
var
  I: Integer;
begin
  Opd.InitialDir:=dm.ValorParametro(2);

  if Opd.Execute then
  begin
      if (ExtractFileName(Opd.FileName)[1])<>'B' then
      begin
          GR.Aviso('Arquivo Inv�lido!');
          Exit;
      end;
      Arquivo:=ExtractFileName(opd.FileName);
      Arquivo[1]:='C';
      M.Lines.LoadFromFile(opd.FileName);
      Convenio:=InputBox(dm.PortadoresNOME.AsString,'Conv�nio? (S/N)','N');
  end
  else Exit;

  Portadores.Close;
  Portadores.Open;
  for I:=0 to M.Lines.Count-1 do
  begin
      if (Copy(M.Lines[I],1,1)='0') then
      begin
          Portadores.Append;
          PortadoresCodigo.AsString   :=Texto(I,2,3);
          PortadoresNome.AsString     :=Trim(Copy(M.Lines[I],5,40));
          PortadoresAgencia.AsString  :=(Texto(I,84,6));
          PortadoresPraca.AsString    :=(Texto(I,93,7));
          Portadores.Post;
      end;
  end;

  RX.DisableControls;
  RX.Close;
  RX.Open;

  Entrada.Close;
  Entrada.Open;

  for I:=0 to M.Lines.Count-1 do
  begin
      Entrada.Append;
      EntradaLinha.AsString:=M.Lines[I];
      Entrada.Post;

      if (I<>0) and (I<>M.Lines.Count-1) and (Copy(M.Lines[I],1,1)<>'0') and (Copy(M.Lines[I],1,1)<>'9') then
      begin
          RX.Append;
          Portadores.Locate('Codigo',Texto(I,2,3),[]);
          {NOME PORTADOR}   RXAPRESENTANTE.AsString     :=PortadoresNome.AsString;
          {C�DIGO PORTADOR} if Texto(I,2,3)<>''then     RXCODIGO_APRESENTANTE.AsString:=Texto(I,2,3);
          {AG�NCIA CEDENTE} RXAGENCIA.AsString          :=Texto(I,5,15);
          {NOME CEDENTE}    RXCEDENTE.AsString          :=Texto(I,20,45);
          {NOME SACADOR}    RXSACADOR.AsString          :=Texto(I,65,45);
          {DOC. SACADOR}    RXCPF_CNPJ_SACADOR.AsString :=Texto(I,110,14);
          {END. SACADOR}    RXEND_SACADOR.AsString      :=Texto(I,124,45);
          {CEP SACADOR}     RXCEP_SACADOR.AsString      :=Texto(I,169,8);
          {CIDADE SACADOR}  RXCID_SACADOR.AsString      :=Texto(I,177,20);
          {UF SACADOR}      RXUF_SACADOR.AsString       :=Texto(I,197,2);
          {NOSSO NUMERO}    RXNOSSO_NUMERO.AsString     :=Texto(I,199,15);
          {ESP�CIE}         RXTIPO_TITULO.AsInteger     :=PF.RetornarEspecie(Texto(I,214,3));
          {N�M.T�TULO}      RXNUMERO_TITULO.AsString    :=Trim(Copy(M.Lines[I],217,11));
          {DT.T�TULO}       if (Texto(I,228,2)<>'00') and (Texto(I,228,2)<>'99') then RXDT_TITULO.AsDateTime      :=StrToDate(Texto(I,228,2)+'/'+Texto(I,230,2)+'/'+Texto(I,232,4));
          {DT.VENCIMENTO}   if (Texto(I,236,2)<>'00') and (Texto(I,236,2)<>'99') then RXDT_VENCIMENTO.AsDateTime  :=StrToDate(Texto(I,236,2)+'/'+Texto(I,238,2)+'/'+Texto(I,240,4));
          {VALOR T�TULO}    if Texto(I,247,14)<>'' then RXVALOR_TITULO.AsFloat:=PF.TextoParaFloat(Texto(I,247,14));
          {SALDO T�TULO}    if Texto(I,261,14)<>'' then RXSALDO_PROTESTO.AsFloat:=PF.TextoParaFloat(Texto(I,261,14));
          {PRA�A}           RXPRACA.AsString            :=Texto(I,275,20);
          {TIPO ENDOSSO}    if Texto(I,295,1)='' then   RXENDOSSO.AsString:='S' else RXENDOSSO.AsString:=Texto(I,295,1);
          {ACEITE}          RXACEITE.AsString           :=Texto(I,296,1);
          {DEVEDOR}         RXDEVEDOR.AsString          :=Texto(I,298,45);
          {TIPO DEVEDOR}    if Texto(I,343,3)='001'then RXTIPO_DEVEDOR.AsString:='J' else RXTIPO_DEVEDOR.AsString:='F';
          {DOC. DEVEDOR}    if RXTIPO_DEVEDOR.AsString='J' then RXCPF_CNPJ_DEVEDOR.AsString :=Texto(I,346,14) else RXCPF_CNPJ_DEVEDOR.AsString:=Texto(I,349,11);
          {RG DEVEDOR}      if Texto(I,360,11)<>'00000000000' then RXRG_DEVEDOR.AsString:=Texto(I,360,11);
          {END. DEVEDOR}    RXEND_DEVEDOR.AsString      :=Texto(I,371,45);
          {CEP DEVEDOR}     RXCEP_DEVEDOR.AsString      :=Texto(I,416,8);
          {CIDADE DEVEDOR}  RXCID_DEVEDOR.AsString      :=Texto(I,424,20);
          {UF}              RXUF.AsString               :=Texto(I,444,2);
          {BAIRRO DEVEDOR}  RXBAIRRO_DEVEDOR.AsString   :=Texto(I,488,20);
          {STATUS}          RXSTATUS.AsString           :='';
                            RXSTATUS.AsString           :='Rejeitado';

                            RXEspecie.AsString          :=PF.RetornarTitulo(RXTIPO_TITULO.AsInteger);
                            if Length(RXCPF_CNPJ_DEVEDOR.AsString)>11 then
                              RXDocumento.AsString:=PF.FormatarCNPJ(RXCPF_CNPJ_DEVEDOR.AsString)
                                else RXDocumento.AsString:=PF.FormatarCPF(RXCPF_CNPJ_DEVEDOR.AsString);

                            if RXCODIGO_APRESENTANTE.AsString='582' then
                              RXNUMERO_TITULO.AsString:=RXNOSSO_NUMERO.AsString;

                            qryNumero.Close;
                            qryNumero.Params[0].AsString   :=RXNUMERO_TITULO.AsString;
                            qryNumero.Params[1].AsString   :=RXCODIGO_APRESENTANTE.AsString;
                            qryNumero.Params[2].AsDate     :=RXDT_TITULO.AsDateTime;
                            qryNumero.Params[3].AsString   :=RXNOSSO_NUMERO.AsString;
                            qryNumero.Open;
                            RXProtocolo.AsInteger       :=qryNumeroPROTOCOLO.AsInteger;
                            RXDT_PROTOCOLO.AsDateTime   :=qryNumeroDT_PROTOCOLO.AsDateTime;
                            if (dm.ServentiaCODIGO.AsInteger=1866) or (dm.ServentiaCODIGO.AsInteger=1208) or (dm.ServentiaCODIGO.AsInteger=8247) then
                              RXCUSTAS.AsFloat:=qryNumeroTOTAL.AsFloat
                                else RXCUSTAS.AsFloat:=qryNumeroTOTAL.AsFloat-qryNumeroVALOR_AR.AsFloat-qryNumeroTARIFA_BANCARIA.AsFloat;
          RX.Post;
      end;
  end;

  {AJUSTE PARA O CRA}
  RXBC.Close;
  RXBC.Open;
  RXBC.Append;
  RXEN.Close;
  RXEN.Open;
  for I:=0 to M.Lines.Count-1 do
  begin
      RXEN.Append;
      RXENCodigo.AsString :=Texto(I,2,3);
      RXENLinha.Value     :=M.Lines[I];
      RXEN.Post;

      RXBCCodigo.AsString :=Texto(I,2,3);
      RXBCImportar.Value  :=RXBCImportar.Value+#13+M.Lines[I];
      if Copy(M.Lines[I],1,1)='9' then
      begin
          RXBCImportar.Value:=Trim(RXBCImportar.Value);
          RXBC.Post;
          RXBC.Append;
      end;
  end;

  RX.First;
  RX.EnableControls;
end;

procedure TFConfirmacao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

function TFConfirmacao.Texto(Linha, Inicio, Fim: Integer): String;
begin
  Result:=Trim(Copy(M.Lines[Linha],Inicio,Fim));
end;

procedure TFConfirmacao.dsRXDataChange(Sender: TObject; Field: TField);
begin
  if RX.IsEmpty then
    btApontar.Enabled:=False
      else btApontar.Enabled:=True;
  RXEN.Filtered:=False;
  RXEN.Filtered:=True;
end;

procedure TFConfirmacao.FormCreate(Sender: TObject);
begin
  lbAviso.Caption:='Seu arquivo de confirma��o ser� gerado na seguinte pasta:'+#13+dm.ValorParametro(3)+#13+
  'Clique aqui para alterar!';
end;

procedure TFConfirmacao.GridAtosExit(Sender: TObject);
begin
  if RX.State in [dsEdit,dsInsert] then RX.Post;
end;

procedure TFConfirmacao.btExportarClick(Sender: TObject);
var
  I,J,U: Integer;
  Data,Codigo: String;
begin
  M2.Lines.Clear;

  Saida.Close;
  Saida.Open;
  Entrada.First;
  U:=Entrada.RecordCount;
  for I := 0 to Entrada.RecordCount -1 do
  begin
      if Entrada.RecNo=1 then
      begin
          Saida.Append;
          for J := 0 to Length(EntradaLinha.AsString) do
            if Length(SaidaLinha.AsString)=53 then
              SaidaLinha.AsString:=SaidaLinha.AsString+'S'
                else if Length(SaidaLinha.AsString)=54 then
                  SaidaLinha.AsString:=SaidaLinha.AsString+'D'
                    else if Length(SaidaLinha.AsString)=55 then
                      SaidaLinha.AsString:=SaidaLinha.AsString+'T'
                        else if Length(SaidaLinha.AsString)=56 then
                          SaidaLinha.AsString:=SaidaLinha.AsString+'B'
                            else if Length(SaidaLinha.AsString)=57 then
                              SaidaLinha.AsString:=SaidaLinha.AsString+'F'
                                else if Length(SaidaLinha.AsString)=58 then
                                  SaidaLinha.AsString:=SaidaLinha.AsString+'O'
                                    else if Length(SaidaLinha.AsString)=59 then
                                      SaidaLinha.AsString:=SaidaLinha.AsString+'C'
                                        else if Length(SaidaLinha.AsString)=60 then
                                          SaidaLinha.AsString:=SaidaLinha.AsString+'R'
                                            else if Length(SaidaLinha.AsString)=61 then
                                              SaidaLinha.AsString:=SaidaLinha.AsString+'T'
                                                else SaidaLinha.AsString:=SaidaLinha.AsString+EntradaLinha.AsString[J];

          SaidaLinha.AsString:=Copy(SaidaLinha.AsString,2,Length(SaidaLinha.AsString));
          Saida.Post;
          M2.Lines.Add(SaidaLinha.AsString);
      end
      else
      begin
          if I<(U-1) then
          begin
              RX.RecNo:=Entrada.RecNo-1;

              if RXDataOcorrencia.AsDateTime=0 then
                Data:='00000000'
                  else Data:=FormatDateTime('ddmmyyyy',RXDataOcorrencia.AsDateTime);

              if RXIrregularidade.AsString<>'' then
                Codigo:=PF.Espaco(RXIrregularidade.AsString,'E',2)
                  else Codigo:='  ';

              Saida.Append;
              for J := 0 to Length(EntradaLinha.AsString) do
                if J=446 then
                  SaidaLinha.AsString:=SaidaLinha.AsString+dm.ValorParametro(23)
                    else
                      if J=448 then
                        SaidaLinha.AsString:=SaidaLinha.AsString+PF.Zeros(RXProtocolo.AsString,'D',10)
                          else
                            if J=458 then
                              SaidaLinha.AsString:=SaidaLinha.AsString+' '
                                else
                                  if J=459 then
                                    SaidaLinha.AsString:=SaidaLinha.AsString+FormatDateTime('ddmmyyyy',Now)
                                      else
                                        if J=467 then
                                          SaidaLinha.AsString:=SaidaLinha.AsString+PF.Zeros(GR.PegarNumeroTexto(FloatToStrF(RXCUSTAS.AsFloat,ffNumber,7,2)),'D',10)
                                            else
                                              if J=478 then
                                                SaidaLinha.AsString:=SaidaLinha.AsString+Data
                                                  else
                                                    if J=486 then
                                                      SaidaLinha.AsString:=SaidaLinha.AsString+Codigo
                                                        else
                                                          if (J<446) or (J>487) or (J=477) then
                                                            SaidaLinha.AsString:=SaidaLinha.AsString+EntradaLinha.AsString[J];

              SaidaLinha.AsString:=Copy(SaidaLinha.AsString,2,Length(SaidaLinha.AsString));
              Saida.Post;
              M2.Lines.Add(SaidaLinha.AsString);
          end;
      end;
      Entrada.Next;
  end;
  M2.Lines.Add(EntradaLinha.AsString);
  M2.Lines.SaveToFile(dm.ValorParametro(3)+Arquivo);
end;

procedure TFConfirmacao.btRejeitarClick(Sender: TObject);
begin
  if RX.IsEmpty then Exit;
  dm.vOkGeral:=False;
  GR.CriarForm(TFIrregularidades,FIrregularidades);

  if dm.vOkGeral then
  begin
      RX.Edit;
      RXIrregularidade.AsString :=dm.IrregularidadesCODIGO.AsString;
      RXDescIrreg.AsString      :=dm.IrregularidadesMOTIVO.AsString;
      RX.Post;
  end;
  dm.Irregularidades.Close;
end;

procedure TFConfirmacao.RXENFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept:=RXENCodigo.AsInteger=RXCODIGO_APRESENTANTE.AsInteger;
end;

function TFConfirmacao.Maximo: Integer;
var
  X: Integer;
begin
  X:=0;
  RXEN.First;
  while not RXEN.Eof do
  begin
      Inc(X);
      RXEN.Next;
  end;
  Result:=X;
  RXEN.First;
end;

procedure TFConfirmacao.lbAvisoClick(Sender: TObject);
var
  Path: String;
begin
  Path:=dm.ValorParametro(3);
  InputQuery('Arquivo de Confirma��o','Diret�rio',Path);
  dm.AtualizarParametro(3,Path);
  lbAviso.Caption:='Seu arquivo de confirma��o ser� gerado na seguinte pasta:'+#13+dm.ValorParametro(3)+#13+
  'Clique aqui para alterar!';
end;

procedure TFConfirmacao.btApontarClick(Sender: TObject);
var
  I,J,U: Integer;
  Data,Codigo,Ocorrencia,DataProtocolo: String;
begin
  PB.Visible  :=True;
  PB.Max      :=RX.RecordCount;
  PB.Position :=0;

  M2.Lines.Clear;

  Saida.Close;
  Saida.Open;

  //RX.DisableControls;
  RX.First;
  RXEN.Filtered:=False;
  RXEN.Filtered:=True;
  RXEN.First;
  Linhas.Close;
  Linhas.Open;
  while not RXEN.Eof do
  begin
      Linhas.Append;
      LinhasLinha.Value:=RXENLinha.Value;
      Linhas.Post;
      RXEN.Next;
  end;
  Linhas.First;
  U:=Linhas.RecordCount;
  while not RX.Eof do
  begin
      Application.ProcessMessages;
      PB.Position:=PB.Position+1;
      for I := 0 to Linhas.RecordCount -1 do
      begin
          if Linhas.RecNo=1 then
          begin
              Saida.Append;
              for J := 0 to Length(LinhasLinha.AsString) do
                if Length(SaidaLinha.AsString)=53 then
                  SaidaLinha.AsString:=SaidaLinha.AsString+'S'
                    else if Length(SaidaLinha.AsString)=54 then
                      SaidaLinha.AsString:=SaidaLinha.AsString+'D'
                        else if Length(SaidaLinha.AsString)=55 then
                          SaidaLinha.AsString:=SaidaLinha.AsString+'T'
                            else if Length(SaidaLinha.AsString)=56 then
                              SaidaLinha.AsString:=SaidaLinha.AsString+'B'
                                else if Length(SaidaLinha.AsString)=57 then
                                  SaidaLinha.AsString:=SaidaLinha.AsString+'F'
                                    else if Length(SaidaLinha.AsString)=58 then
                                      SaidaLinha.AsString:=SaidaLinha.AsString+'O'
                                        else if Length(SaidaLinha.AsString)=59 then
                                          SaidaLinha.AsString:=SaidaLinha.AsString+'C'
                                            else if Length(SaidaLinha.AsString)=60 then
                                              SaidaLinha.AsString:=SaidaLinha.AsString+'R'
                                                else if Length(SaidaLinha.AsString)=61 then
                                                  SaidaLinha.AsString:=SaidaLinha.AsString+'T'
                                                    else SaidaLinha.AsString:=SaidaLinha.AsString+LinhasLinha.AsString[J];

              SaidaLinha.AsString:=Copy(SaidaLinha.AsString,2,Length(SaidaLinha.AsString));
              Saida.Post;
              M2.Lines.Add(SaidaLinha.AsString);
              Linhas.Next;
          end
          else
          begin
              if I<(U-1) then
              begin
                  if RXDT_PROTOCOLO.AsDateTime<>0 then
                    DataProtocolo:=FormatDateTime('ddmmyyyy',RXDT_PROTOCOLO.AsDateTime)
                      else DataProtocolo:='00000000';
                      
                  if RXOCORRENCIA.AsString='' then
                    Ocorrencia:=' '
                      else Ocorrencia:=RXOCORRENCIA.AsString;

                  if RXDataOcorrencia.AsDateTime=0 then
                    Data:='00000000'
                      else Data:=FormatDateTime('ddmmyyyy',RXDataOcorrencia.AsDateTime);

                  if RXIrregularidade.AsString<>'' then
                    Codigo:=PF.Espaco(RXIrregularidade.AsString,'E',2)
                      else Codigo:='  ';

                  Saida.Append;
                  for J := 0 to Length(LinhasLinha.AsString) do
                    if J=446 then
                      SaidaLinha.AsString:=SaidaLinha.AsString+dm.ValorParametro(23)
                        else
                          if J=448 then
                            SaidaLinha.AsString:=SaidaLinha.AsString+PF.Zeros(RXProtocolo.AsString,'D',10)
                              else
                                if J=458 then
                                  SaidaLinha.AsString:=SaidaLinha.AsString+Ocorrencia
                                    else
                                      if J=459 then
                                        SaidaLinha.AsString:=SaidaLinha.AsString+DataProtocolo
                                          else
                                            if J=467 then
                                              SaidaLinha.AsString:=SaidaLinha.AsString+PF.Zeros(GR.PegarNumeroTexto(FloatToStrF(RXCUSTAS.AsFloat,ffNumber,7,2)),'D',10)
                                                else
                                                  if J=478 then
                                                    SaidaLinha.AsString:=SaidaLinha.AsString+Data
                                                      else
                                                        if J=486 then
                                                          SaidaLinha.AsString:=SaidaLinha.AsString+Codigo
                                                            else
                                                              if (J<446) or (J>487) or (J=477) then
                                                                SaidaLinha.AsString:=SaidaLinha.AsString+LinhasLinha.AsString[J];

                  SaidaLinha.AsString:=Copy(SaidaLinha.AsString,2,Length(SaidaLinha.AsString));
                  Saida.Post;
                  M2.Lines.Add(SaidaLinha.AsString);
                  RX.Next;
                  Linhas.Next;
              end
              else
              begin
                  M2.Lines.Add(LinhasLinha.AsString);
                  RXEN.Filtered:=False;
                  RXEN.Filtered:=True;
                  RXEN.First;
                  Linhas.Close;
                  Linhas.Open;
                  while not RXEN.Eof do
                  begin
                      Linhas.Append;
                      LinhasLinha.Value:=RXENLinha.Value;
                      Linhas.Post;
                      RXEN.Next;
                  end;
                  Linhas.First;
                  U:=Linhas.RecordCount;
              end;
          end;
      end;
  end;

  M2.Lines.SaveToFile(dm.ValorParametro(3)+Arquivo);

  PF.Aguarde(False);

  GR.Aviso('Confirma��o gerada com sucesso.');
  RX.Close;
  //RX.EnableControls;
  PB.Visible:=False;
end;

procedure TFConfirmacao.RXCheckChange(Sender: TField);
begin
  RX.Edit;
  if RXCheck.AsBoolean=True then
  begin
      RXSTATUS.AsString:='Aceito';
      RXIrregularidade.Clear;
  end
  else RXSTATUS.AsString:='Rejeitado';
end;

procedure TFConfirmacao.MarcarTodosClick(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  while not RX.Eof do
  begin
      RX.Edit;
      RXCheck.AsBoolean:=True;
      RX.Post;
      RX.Next;
  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;
end;

procedure TFConfirmacao.DesmarcarTodosClick(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  while not RX.Eof do
  begin
      RX.Edit;
      RXCheck.AsBoolean:=False;
      RX.Post;
      RX.Next;
  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;
end;

end.



