unit UAssinatura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, Buttons, sBitBtn, DBCtrls,
  sDBLookupComboBox, DB, DBClient, SimpleDS, sBevel, sCheckBox, sEdit,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFAssinatura = class(TForm)
    sPanel1: TsPanel;
    btContinuar: TsBitBtn;
    lkEscreventes: TsDBLookupComboBox;
    dsEscreventes: TDataSource;
    ckTabeliao: TsCheckBox;
    sBevel1: TsBevel;
    ckMatricula: TsCheckBox;
    edTitulo: TsEdit;
    qryEscreventes: TFDQuery;
    qryEscreventesID_ESCREVENTE: TIntegerField;
    qryEscreventesNOME: TStringField;
    qryEscreventesCPF: TStringField;
    qryEscreventesQUALIFICACAO: TStringField;
    qryEscreventesLOGIN: TStringField;
    qryEscreventesSENHA: TStringField;
    qryEscreventesAUTORIZADO: TStringField;
    qryEscreventesMATRICULA: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure ckTabeliaoClick(Sender: TObject);
    procedure btContinuarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAssinatura: TFAssinatura;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFAssinatura.FormCreate(Sender: TObject);
begin
  qryEscreventes.Connection := dm.conSISTEMA;
  qryEscreventes.Close;
  qryEscreventes.Open;

  edTitulo.Text := dm.ServentiaTITULO.AsString;

  if dm.ServentiaCODIGO.AsInteger = 1823 then
  begin
    ckTabeliao.Checked := False;
    ckTabeliaoClick(Sender);
    lkEscreventes.KeyValue := PF.IDEscrevente(dm.vCPF);
  end;
end;

procedure TFAssinatura.ckTabeliaoClick(Sender: TObject);
begin
  if ckTabeliao.Checked then
  begin
    lkEscreventes.Enabled := False;
    qryEscreventes.Close;
  end
  else
  begin
    lkEscreventes.Enabled := True;
    qryEscreventes.Open;
  end;

  ckMatricula.Enabled := ckTabeliao.Checked;
  edTitulo.Enabled    := ckTabeliao.Checked;

  if not ckMatricula.Enabled then
    ckMatricula.Checked:=False;
end;

procedure TFAssinatura.btContinuarClick(Sender: TObject);
begin
  Close;
end;

procedure TFAssinatura.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if ckTabeliao.Checked then
  begin
    dm.vAssinatura := dm.ServentiaTABELIAO.AsString;

    if ckMatricula.Checked then
      dm.vMatricula := dm.ServentiaMATRICULA.AsString
    else
      dm.vMatricula := '';

    if edTitulo.Text <> '' then
    dm.vAssinatura := dm.vAssinatura + ' - ' + edTitulo.Text;
  end
  else
  begin
    if lkEscreventes.Text <> '' then
    begin
      dm.vAssinatura := lkEscreventes.Text;
      dm.vMatricula  := qryEscreventesQUALIFICACAO.AsString;
    end
    else
    begin
      dm.vAssinatura := '';
      dm.vMatricula  := '';
    end;
  end;
end;

procedure TFAssinatura.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
    Perform(WM_NEXTDLGCTL, 0, 0);
end;

procedure TFAssinatura.FormShow(Sender: TObject);
begin
  btContinuar.SetFocus;
end;

end.
