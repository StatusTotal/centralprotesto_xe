�
 TFRELRETORNO 0  TPF0TFRelRetornoFRelRetornoLeft�TopBorderIconsbiSystemMenu BorderStylebsDialogCaptionArquivo de RetornoClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
KeyPreview	OldCreateOrderPositionpoMainFormCenterOnCreate
FormCreate	OnKeyDownFormKeyDownPixelsPerInch`
TextHeight TsBitBtnbtVisualizarLeftTopgWidth[HeightCursorcrHandPointCaption
VisualizarFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderOnClickbtVisualizarClick
ImageIndex Images
dm.ImagensSkinData.SkinSectionBUTTON  TsBitBtnbtSairLeft� TopgWidthKHeightCursorcrHandPointCaptionSairFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderOnClickbtSairClick
ImageIndexImages
dm.ImagensSkinData.SkinSectionBUTTON  TsPanelP1Left Top Width�HeightaAlignalTopTabOrder SkinData.SkinSectionPANEL 
TsGroupBox
sGroupBox1Left
Top6Width�HeightICaption   PeríodoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderSkinData.SkinSectionGROUPBOX 
TsDateEditedInicioLeftYTopWidthoHeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontTabOrder BoundLabel.Active	BoundLabel.ParentFontBoundLabel.Caption   InícioBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style SkinData.SkinSectionEDITGlyphMode.Blend GlyphMode.GrayedDefaultToday	  
TsDateEditedFimLeft)TopWidthpHeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontTabOrderBoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionFimBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style SkinData.SkinSectionEDITGlyphMode.Blend GlyphMode.GrayedDefaultToday	   TsDBLookupComboBox
lkPortadorLeft
TopWidth�HeightColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style KeyFieldID_PORTADOR	ListFieldNOME
ListSource
dsPortador
ParentFontTabOrder BoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionPortadorBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style BoundLabel.Layout
sclTopLeftSkinData.SkinSectionCOMBOBOX  	TwwDBGridGridLeftTop� Width�Height� ControlType.StringsCheck;CheckBox;True;False Selected.StringsCheck	3	#	FProtocolo	9	Protocolo	F   NossoNumero	18	Nosso Número	FCustas	12	Custas	F   Situacao	18	Situação	F IniAttributes.Delimiter;;IniAttributes.UnicodeIniFile
TitleColor	clBtnFace	FixedColsShowHorzScrollBar	AlignalBottom
DataSourcedsRXOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit
dgWordWrapdgProportionalColResize 	PopupMenuPMReadOnly	TabOrderTitleAlignmenttaCenterTitleFont.CharsetANSI_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameTahomaTitleFont.Style 
TitleLinesTitleButtons
UseTFieldsOnMouseMoveGridMouseMove	OnMouseUpGridMouseUp   TsBitBtnsBitBtn1LeftfTopgWidth[HeightCursorcrHandPointCaptionImprimirFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderOnClicksBitBtn1Click
ImageIndexImages
dm.ImagensSkinData.SkinSectionBUTTON  TDataSource
dsPortadorDataSetdm.PortadoresLeft� Top�   TDataSourcedsRXDataSetRXLeftPTop�   TRxMemoryDataRX	FieldDefsNameCheckDataType	ftBoolean Name	ProtocoloDataType	ftInteger NameNossoNumeroDataTypeftStringSize NameCustasDataTypeftFloat NameSituacaoDataTypeftStringSize NameDataDataTypeftDate NameEditalDataTypeftFloat NameRetornoDataTypeftStringSize NameValorTituloDataTypeftFloat  OnFilterRecordRXFilterRecordLeft Top�  TBooleanFieldRXCheckDisplayLabel#DisplayWidth	FieldNameCheck  TIntegerFieldRXProtocoloDisplayWidth	FieldName	Protocolo  TStringFieldRXNossoNumeroDisplayLabel   Nosso NúmeroDisplayWidth	FieldNameNossoNumeroSize  TFloatFieldRXCustasDisplayWidth	FieldNameCustasDisplayFormat	#####0.00
EditFormat	#####0.00  TStringField
RXSituacaoDisplayLabel
   SituaçãoDisplayWidth	FieldNameSituacao  
TDateFieldRXData	FieldNameDataVisibleDisplayFormat
DD/MM/YYYY  TFloatFieldRXEdital	FieldNameEditalVisibleDisplayFormat	#####0.00
EditFormat	#####0.00  TStringField	RXRetorno	FieldNameRetornoVisible  TFloatFieldRXValorTitulo	FieldNameValorTituloVisibleDisplayFormat	#####0.00
EditFormat	#####0.00   
TPopupMenuPMLeft� Top�  	TMenuItemMarcartodos1CaptionMarcar todosOnClickMarcartodos1Click  	TMenuItemDesmarcartodos1CaptionDesmarcar todosOnClickDesmarcartodos1Click   TFDQueryTitulos
Connectiondm.conSISTEMASQL.Strings�select ID_ATO, PROTOCOLO, DT_REGISTRO, DT_PAGAMENTO, TOTAL, NOSSO_NUMERO, STATUS, DT_RETIRADO, DT_SUSTADO, ARQUIVO,VALOR_TITULO,TARIFA_BANCARIA from TITULOS Left� Top�  TIntegerFieldTitulosID_ATO	FieldNameID_ATOOriginID_ATOProviderFlags
pfInUpdate	pfInWherepfInKey Required	  TIntegerFieldTitulosPROTOCOLO	FieldName	PROTOCOLOOrigin	PROTOCOLO  
TDateFieldTitulosDT_REGISTRO	FieldNameDT_REGISTROOriginDT_REGISTRO  
TDateFieldTitulosDT_PAGAMENTO	FieldNameDT_PAGAMENTOOriginDT_PAGAMENTO  TFloatFieldTitulosTOTAL	FieldNameTOTALOriginTOTAL  TStringFieldTitulosNOSSO_NUMERO	FieldNameNOSSO_NUMEROOriginNOSSO_NUMEROSize  TStringFieldTitulosSTATUS	FieldNameSTATUSOriginSTATUS  
TDateFieldTitulosDT_RETIRADO	FieldNameDT_RETIRADOOriginDT_RETIRADO  
TDateFieldTitulosDT_SUSTADO	FieldName
DT_SUSTADOOrigin
DT_SUSTADO  TStringFieldTitulosARQUIVO	FieldNameARQUIVOOriginARQUIVO  TFloatFieldTitulosVALOR_TITULO	FieldNameVALOR_TITULOOriginVALOR_TITULO  TFloatFieldTitulosTARIFA_BANCARIA	FieldNameTARIFA_BANCARIAOriginTARIFA_BANCARIA  TBooleanFieldTitulosCheck	FieldKindfkInternalCalc	FieldNameCheck    