unit UProtocolar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FMTBcd, Mask, sMaskEdit, sCustomComboEdit, sTooledit, StdCtrls,
  sRadioButton, sEdit, sComboBox, Grids, Wwdbigrd, Wwdbgrid, sMemo,
  Buttons, sBitBtn, ExtCtrls, sPanel, Menus, DBClient, Provider, DB,
  SqlExpr, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFProtocolar = class(TForm)
    dsTitulos: TDataSource;
    Timer: TTimer;
    dspConsulta: TDataSetProvider;
    Consulta: TClientDataSet;
    dsConsulta: TDataSource;
    P2: TsPanel;
    btAlterar: TsBitBtn;
    btExcluir: TsBitBtn;
    btRecibo: TsBitBtn;
    P3: TsPanel;
    lbRecibo: TLabel;
    lbTipo: TLabel;
    lbTitulo: TLabel;
    lbEmissao: TLabel;
    lbVencimento: TLabel;
    lbValor: TLabel;
    lbCustas: TLabel;
    lbSaldo: TLabel;
    lbSacador: TLabel;
    lbCedente: TLabel;
    lbStatus: TLabel;
    lbDataEntrada: TLabel;
    GridAtos: TwwDBGrid;
    P1: TsPanel;
    cbBuscar: TsComboBox;
    edPesquisar: TsEdit;
    edData: TsDateEdit;
    ConsultaProtocolar: TStringField;
    edProtocolo: TsEdit;
    edDataProtocolo: TsDateEdit;
    PM: TPopupMenu;
    MarcarTodos1: TMenuItem;
    DesmarcarTodos1: TMenuItem;
    N1: TMenuItem;
    Custas1: TMenuItem;
    qryConsulta: TFDQuery;
    ConsultaID_ATO: TIntegerField;
    ConsultaDT_ENTRADA: TDateField;
    ConsultaRECIBO: TIntegerField;
    ConsultaAPRESENTANTE: TStringField;
    ConsultaDEVEDOR: TStringField;
    ConsultaSTATUS: TStringField;
    procedure dsConsultaDataChange(Sender: TObject; Field: TField);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edPesquisarKeyPress(Sender: TObject; var Key: Char);
    procedure cbBuscarChange(Sender: TObject);
    procedure edDataKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btReciboClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure GridAtosTitleButtonClick(Sender: TObject; AFieldName: String);
    procedure MarcarTodos;
    procedure DesmarcarTodos;
    procedure ConsultaAfterOpen(DataSet: TDataSet);
    procedure MarcarTodos1Click(Sender: TObject);
    procedure DesmarcarTodos1Click(Sender: TObject);
    procedure GridAtosMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormClick(Sender: TObject);
    procedure Custas1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FProtocolar: TFProtocolar;

implementation

uses UDM, UPF, UQuickRecibo1, UCustas, UGeral;

{$R *.dfm}

procedure TFProtocolar.dsConsultaDataChange(Sender: TObject; Field: TField);
var
  Intimacao,Data: String;
begin
  P3.Visible:=not Consulta.IsEmpty;

  dm.Titulos.Close;
  dm.Titulos.Params[0].AsInteger:=ConsultaID_ATO.AsInteger;
  dm.Titulos.Open;

  if dm.TitulosSTATUS.AsString='ENTRADA'          then Data:=PF.FormatarData(dm.TitulosDT_ENTRADA.AsDateTime,'N');
  if dm.TitulosSTATUS.AsString='APONTADO'         then Data:=PF.FormatarData(dm.TitulosDT_PROTOCOLO.AsDateTime,'N');
  if dm.TitulosSTATUS.AsString='ENVIAR INTIMA��O' then Data:=PF.FormatarData(dm.TitulosDT_ENVIO.AsDateTime,'N');
  if dm.TitulosSTATUS.AsString='INTIMADO'         then Data:=PF.FormatarData(dm.TitulosDT_INTIMACAO.AsDateTime,'N');
  if dm.TitulosSTATUS.AsString='PROTESTADO'       then Data:=PF.FormatarData(dm.TitulosDT_REGISTRO.AsDateTime,'N');
  if dm.TitulosSTATUS.AsString='PAGO'             then Data:=PF.FormatarData(dm.TitulosDT_PAGAMENTO.AsDateTime,'N');
  if dm.TitulosSTATUS.AsString='RETIRADO'         then Data:=PF.FormatarData(dm.TitulosDT_RETIRADO.AsDateTime,'N');
  if dm.TitulosSTATUS.AsString='SUSTADO'          then Data:=PF.FormatarData(dm.TitulosDT_SUSTADO.AsDateTime,'N');

  if dm.TitulosTIPO_INTIMACAO.AsString='P' then Intimacao:='Portador';
  if dm.TitulosTIPO_INTIMACAO.AsString='E' then Intimacao:='Edital ('+dm.TitulosMOTIVO_INTIMACAO.AsString+')';
  if dm.TitulosTIPO_INTIMACAO.AsString='C' then Intimacao:='Carta';

  lbStatus.Caption      :=dm.TitulosSTATUS.AsString+' EM '+Data;
  lbRecibo.Caption      :='Recibo: '+dm.TitulosRECIBO.AsString;
  lbDataEntrada.Caption :='Data: '+PF.FormatarData(dm.TitulosDT_ENTRADA.AsDateTime,'N');
  lbTipo.Caption        :='Esp�cie: '+PF.RetornarTitulo(dm.TitulosTIPO_TITULO.AsInteger);
  lbTitulo.Caption      :='T�tulo: '+dm.TitulosNUMERO_TITULO.AsString;
  lbEmissao.Caption     :='Emiss�o: '+PF.FormatarData(dm.TitulosDT_TITULO.AsDateTime,'N');
  lbVencimento.Caption  :='Vencimento: '+PF.FormatarData(dm.TitulosDT_VENCIMENTO.AsDateTime,'N');
  lbValor.Caption       :='Valor: '+FloatToStrF(dm.TitulosVALOR_TITULO.AsFloat,ffNumber,7,2);
  lbCustas.Caption      :='Custas: '+FloatToStrF(dm.TitulosTOTAL.AsFloat,ffNumber,7,2);
  lbSaldo.Caption       :='Saldo: '+FloatToStrF(dm.TitulosSALDO_PROTESTO.AsFloat,ffNumber,7,2);
  lbSacador.Caption     :='Sacador: '+dm.TitulosSACADOR.AsString;
  lbCedente.Caption     :='Cedente: '+dm.TitulosCEDENTE.AsString;
end;

procedure TFProtocolar.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFProtocolar.edPesquisarKeyPress(Sender: TObject; var Key: Char);
var
  Query: String;
begin
  if cbBuscar.ItemIndex in [0] then
    if not (key in ['0'..'9',#8,#13]) then key:=#0;

  if key=#13 then
  begin
      if edPesquisar.Text='' then Exit;

      Query:='SELECT ID_ATO,DT_ENTRADA,RECIBO,APRESENTANTE,DEVEDOR,STATUS FROM TITULOS WHERE ';

      Consulta.Close;

      {RECIBO}
      if cbBuscar.ItemIndex=0 then
      begin
          Consulta.CommandText:=Query+'RECIBO = :P AND STATUS='''+'ENTRADA'+''' AND ANTIGO<>'''+'S'+''' ORDER BY RECIBO';
          Consulta.Params.ParamByName('P').AsInteger:=StrToInt(edPesquisar.Text);
      end;

      {PORTADOR}
      if cbBuscar.ItemIndex=1 then
      begin
          Consulta.CommandText:=Query+'APRESENTANTE LIKE :P AND STATUS='''+'ENTRADA'+''' AND ANTIGO<>'''+'S'+''' ORDER BY RECIBO';
          Consulta.Params.ParamByName('P').AsString:=edPesquisar.Text+'%';
      end;

      {DEVEDOR}
      if cbBuscar.ItemIndex=2 then
      begin
          Consulta.CommandText:=Query+'DEVEDOR LIKE :P AND STATUS='''+'ENTRADA'+''' AND ANTIGO<>'''+'S'+''' ORDER BY RECIBO';
          Consulta.Params.ParamByName('P').AsString:=edPesquisar.Text+'%';
      end;

      {DOCUMENTO DEVEDOR}
      if cbBuscar.ItemIndex=3 then
      begin
          Consulta.CommandText:=Query+'CPF_CNPJ_DEVEDOR =:P AND STATUS='''+'ENTRADA'+''' AND ANTIGO<>'''+'S'+''' ORDER BY RECIBO';
          Consulta.Params.ParamByName('P').AsString:=edPesquisar.Text;
      end;

      {SACADOR}
      if cbBuscar.ItemIndex=4 then
      begin
          Consulta.CommandText:=Query+'SACADOR LIKE :P AND STATUS='''+'ENTRADA'+''' AND ANTIGO<>'''+'S'+''' ORDER BY RECIBO';
          Consulta.Params.ParamByName('P').AsString:=edPesquisar.Text+'%';
      end;

      {CEDENTE}
      if cbBuscar.ItemIndex=5 then
      begin
          Consulta.CommandText:=Query+'CEDENTE LIKE=:P AND STATUS='''+'ENTRADA'+''' AND ANTIGO<>'''+'S'+''' ORDER BY RECIBO';
          Consulta.Params.ParamByName('P').AsString:=edPesquisar.Text+'%';
      end;

      {N�MERO DO T�TULO}
      if cbBuscar.ItemIndex=6 then
      begin
          Consulta.CommandText:=Query+'NUMERO_TITULO =:P AND STATUS='''+'ENTRADA'+''' AND ANTIGO<>'''+'S'+''' ORDER BY RECIBO';
          Consulta.Params.ParamByName('P').AsString:=edPesquisar.Text;
      end;

      Consulta.Open;

      Application.ProcessMessages;
  end;
end;

procedure TFProtocolar.cbBuscarChange(Sender: TObject);
var
  Key: Char;
begin
  if cbBuscar.ItemIndex=7 then
  begin
      edPesquisar.Clear;
      edPesquisar.Enabled:=False;
      edData.Enabled:=True;
      Key:=#13;
      edDataKeyPress(Sender,Key);
  end
  else
  begin
      edPesquisar.Enabled:=True;
      edData.Enabled:=False;
  end;
end;

procedure TFProtocolar.edDataKeyPress(Sender: TObject; var Key: Char);
var
  Query: String;
begin
  if key=#13 then
  begin
      Query:='SELECT ID_ATO,DT_ENTRADA,RECIBO,APRESENTANTE,DEVEDOR,STATUS FROM TITULOS WHERE ';
      Consulta.Close;
      Consulta.CommandText:=Query+'DT_ENTRADA=:P AND STATUS='''+'ENTRADA'+''' ORDER BY RECIBO';
      Consulta.Params.ParamByName('P').AsDate:=edData.Date;
      Consulta.Open;
  end;
end;

procedure TFProtocolar.FormCreate(Sender: TObject);
var
  Key: Char;
begin
  Application.ProcessMessages;
  Key:=#13;
  edDataKeyPress(Sender,Key);
  edProtocolo.Text:=IntToStr(dm.ValorAtual('NPR','N'));
end;

procedure TFProtocolar.TimerTimer(Sender: TObject);
begin
  lbStatus.Font.Color     :=clRed;
  lbValor.Font.Color      :=clRed;
  lbCustas.Font.Color     :=clRed;
  lbSaldo.Font.Color      :=clRed;
  lbRecibo.Font.Color     :=clBlue;
  lbDataEntrada.Font.Color:=clBlue;
end;

procedure TFProtocolar.btAlterarClick(Sender: TObject);
var
  Posicao: Integer;
begin
  if Consulta.IsEmpty then Exit;

  if not GR.Pergunta('Confirma o apontamento') then Exit;

  Posicao:=Consulta.RecNo;
  Consulta.DisableControls;
  Consulta.First;
  while not Consulta.Eof do
  begin
      if ConsultaProtocolar.AsString='S' then
      begin
          dm.Titulos.Close;
          dm.Titulos.Params[0].AsInteger:=ConsultaID_ATO.AsInteger;
          dm.Titulos.Open;
          dm.Titulos.Edit;
          dm.TitulosDT_PROTOCOLO.AsDateTime   :=edDataProtocolo.Date;
          dm.TitulosDT_PRAZO.AsDateTime       :=PF.DataFinalFeriados(dm.TitulosDT_PROTOCOLO.AsDateTime,StrToInt(dm.ValorParametro(5)));
          dm.TitulosPROTOCOLO.AsInteger       :=dm.ValorAtual('NPR','S');
          dm.TitulosLIVRO_PROTOCOLO.AsInteger :=dm.ValorAtual('LPR','N');
          dm.TitulosFOLHA_PROTOCOLO.AsString  :=IntToStr(dm.ValorAtual('FPR','N'));
          dm.TitulosSTATUS.AsString           :='APONTADO';
          dm.Titulos.Post;
          dm.Titulos.ApplyUpdates(0);

          dm.Movimento.Close;
          dm.Movimento.Open;
          dm.Movimento.Append;
          dm.MovimentoID_MOVIMENTO.AsInteger  :=dm.IdAtual('ID_MOVIMENTO','S');
          dm.MovimentoID_ATO.AsInteger        :=dm.TitulosID_ATO.AsInteger;
          dm.MovimentoDATA.AsDateTime         :=Now;
          dm.MovimentoHORA.AsDateTime         :=Time;
          dm.MovimentoBAIXA.AsDateTime        :=dm.TitulosDT_PROTOCOLO.AsDateTime;
          dm.MovimentoESCREVENTE.AsString     :=dm.vNome;
          dm.MovimentoSTATUS.AsString         :=dm.TitulosSTATUS.AsString;
          dm.Movimento.Post;
          dm.Movimento.ApplyUpdates(0);
      end;
      Consulta.Next;
  end;
  Consulta.RecNo:=Posicao;
  Consulta.EnableControls;

  Consulta.Close;
  P3.Visible:=False;
  edProtocolo.Text:=IntToStr(dm.ValorAtual('NPR','N'));
  GR.Aviso('Apontamento efetuado com sucesso.');
end;

procedure TFProtocolar.btReciboClick(Sender: TObject);
begin
  if Consulta.IsEmpty then Exit;

  PF.Aguarde(True);

  dm.Titulos.Close;
  dm.Titulos.Params[0].AsInteger:=ConsultaID_ATO.AsInteger;
  dm.Titulos.Open;

  PF.CarregarCustas(dm.TitulosID_ATO.AsInteger);

  Application.CreateForm(TFQuickRecibo1,FQuickRecibo1);

  dm.Titulos.Close;
end;

procedure TFProtocolar.btExcluirClick(Sender: TObject);
var
  Id: String;
begin
  if Consulta.IsEmpty then Exit;

  if GR.Pergunta('Confirma a exclus�o') then
  begin
      Id:=ConsultaID_ATO.AsString;

      Consulta.Delete;
      Consulta.ApplyUpdates(0);

      PF.ExcluirFD('CUSTAS WHERE ID_ATO='+QuotedStr(Id),dm.conSISTEMA);
  end;
end;

procedure TFProtocolar.GridAtosTitleButtonClick(Sender: TObject;
  AFieldName: String);
begin
  Consulta.IndexFieldNames:=AFieldName;
end;

procedure TFProtocolar.MarcarTodos;
var
  Posicao: Integer;
begin
  Posicao:=Consulta.RecNo;
  Consulta.DisableControls;
  Consulta.First;
  while not Consulta.Eof do
  begin
      Consulta.Edit;
      ConsultaProtocolar.AsString:='S';
      Consulta.Post;
      Consulta.Next;
  end;
  Consulta.RecNo:=Posicao;
  Consulta.EnableControls;
end;

procedure TFProtocolar.ConsultaAfterOpen(DataSet: TDataSet);
begin
  MarcarTodos;
end;

procedure TFProtocolar.DesmarcarTodos;
var
  Posicao: Integer;
begin
  Posicao:=Consulta.RecNo;
  Consulta.DisableControls;
  Consulta.First;
  while not Consulta.Eof do
  begin
      Consulta.Edit;
      ConsultaProtocolar.AsString:='N';
      Consulta.Post;
      Consulta.Next;
  end;
  Consulta.RecNo:=Posicao;
  Consulta.EnableControls;
end;

procedure TFProtocolar.MarcarTodos1Click(Sender: TObject);
begin
  MarcarTodos;
end;

procedure TFProtocolar.DesmarcarTodos1Click(Sender: TObject);
begin
  DesmarcarTodos;
end;

procedure TFProtocolar.GridAtosMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if (X>0) and (X<24) then
    Screen.Cursor:=crHandPoint
      else Screen.Cursor:=crDefault;
end;

procedure TFProtocolar.FormClick(Sender: TObject);
begin
  if ActiveControl=GridAtos then
    if Screen.Cursor=-21 then
    begin
        if Consulta.IsEmpty then Exit;

        Consulta.Edit;
        if ConsultaProtocolar.AsString='S' then
          ConsultaProtocolar.AsString:='N'
            else ConsultaProtocolar.AsString:='N';
        Consulta.Post;
    end;
end;

procedure TFProtocolar.Custas1Click(Sender: TObject);
begin
  dm.Titulos.Close;
  dm.Titulos.Params[0].AsInteger:=ConsultaID_ATO.AsInteger;
  dm.Titulos.Open;
  dm.Titulos.Edit;
  dm.vEmolumentos   :=dm.TitulosEMOLUMENTOS.AsFloat;
  dm.vFetj          :=dm.TitulosFETJ.AsFloat;
  dm.vFundperj      :=dm.TitulosFUNDPERJ.AsFloat;
  dm.vFunperj       :=dm.TitulosFUNPERJ.AsFloat;
  dm.vMutua         :=dm.TitulosMUTUA.AsFloat;
  dm.vAcoterj       :=dm.TitulosACOTERJ.AsFloat;
  dm.vDistribuicao  :=dm.TitulosDISTRIBUICAO.AsFloat;
  dm.vTotal         :=dm.TitulosTOTAL.AsFloat;
  PF.CarregarCustas(dm.TitulosID_ATO.AsInteger);
  dm.Tabela.Close;
  dm.Tabela.Open;
  GR.CriarForm(TFCustas,FCustas);
  dm.TitulosEMOLUMENTOS.AsFloat             :=dm.vEmolumentos;
  dm.TitulosFETJ.AsFloat                    :=dm.vFetj;
  dm.TitulosFUNDPERJ.AsFloat                :=dm.vFundperj;
  dm.TitulosFUNPERJ.AsFloat                 :=dm.vFunperj;
  dm.TitulosMUTUA.AsFloat                   :=dm.vMutua;
  dm.TitulosACOTERJ.AsFloat                 :=dm.vAcoterj;
  dm.TitulosDISTRIBUICAO.AsFloat            :=dm.vDistribuicao;
  dm.TitulosTOTAL.AsFloat                   :=dm.vTotal;
  {GRAVANDO AS CUSTAS}
  if not dm.RxCustas.IsEmpty then
  begin
      dm.RxCustas.First;
      while not dm.RxCustas.Eof do
      begin
          dm.Custas.Close;
          dm.Custas.Params[0].AsInteger:=dm.RxCustasID_CUSTA.AsInteger;
          dm.Custas.Open;
          if dm.Custas.IsEmpty then
          begin
              dm.Custas.Append;
              dm.CustasID_CUSTA.AsInteger :=dm.IdAtual('ID_CUSTA','S');
          end
          else
            dm.Custas.Edit;

          dm.CustasID_ATO.AsInteger     :=dm.TitulosID_ATO.AsInteger;
          dm.CustasTABELA.AsString      :=dm.RxCustasTABELA.AsString;
          dm.CustasITEM.AsString        :=dm.RxCustasITEM.AsString;
          dm.CustasSUBITEM.AsString     :=dm.RxCustasSUBITEM.AsString;
          dm.CustasVALOR.AsFloat        :=dm.RxCustasVALOR.AsFloat;
          dm.CustasQTD.AsInteger        :=dm.RxCustasQTD.AsInteger;
          dm.CustasTOTAL.AsFloat        :=dm.RxCustasTOTAL.AsFloat;
          dm.Custas.Post;
          if dm.Custas.ApplyUpdates(0)>0 then
          Raise Exception.Create('ApplyUpdates dm.Custas');

          dm.RxCustas.Next;
      end;
  end;
  dm.Titulos.Post;
  dm.Titulos.ApplyUpdates(0);
  Consulta.RefreshRecord;
end;

end.
