object FRelNotificacao: TFRelNotificacao
  Left = 323
  Top = 202
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Notifica'#231#227'o'
  ClientHeight = 466
  ClientWidth = 742
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 14
  object P: TsPanel
    Left = 0
    Top = 0
    Width = 742
    Height = 84
    Align = alTop
    TabOrder = 0
    OnMouseMove = PMouseMove
    SkinData.SkinSection = 'PANEL'
    object GbProtocolo: TsGroupBox
      Left = 17
      Top = 14
      Width = 244
      Height = 61
      Caption = 'Protocolo'
      TabOrder = 0
      SkinData.SkinSection = 'MAINMENU'
      CaptionSkin = 'COMBOBOX'
      object edProtFinal: TsEdit
        Left = 154
        Top = 25
        Width = 78
        Height = 22
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnKeyPress = edProtFinalKeyPress
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Final'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
      end
      object edProtInicial: TsEdit
        Left = 42
        Top = 25
        Width = 78
        Height = 22
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnKeyPress = edProtInicialKeyPress
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Inicial'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
      end
    end
    object Rb1: TsRadioButton
      Left = 6
      Top = 6
      Width = 17
      Height = 13
      Cursor = crHandPoint
      Checked = True
      TabOrder = 3
      TabStop = True
      OnClick = Rb1Click
      SkinData.SkinSection = 'RADIOBUTTON'
    end
    object GbData: TsGroupBox
      Left = 283
      Top = 14
      Width = 277
      Height = 61
      Caption = 'Data'
      Enabled = False
      TabOrder = 1
      SkinData.SkinSection = 'MAINMENU'
      CaptionSkin = 'COMBOBOX'
      object edDataInicial: TsDateEdit
        Left = 42
        Top = 25
        Width = 92
        Height = 22
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
        OnExit = edDataInicialExit
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Inicial'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        DefaultToday = True
      end
      object edDataFinal: TsDateEdit
        Left = 170
        Top = 25
        Width = 92
        Height = 22
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Final'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        DefaultToday = True
      end
    end
    object Rb2: TsRadioButton
      Left = 272
      Top = 6
      Width = 17
      Height = 13
      Cursor = crHandPoint
      TabOrder = 4
      OnClick = Rb2Click
      SkinData.SkinSection = 'RADIOBUTTON'
    end
    object btFiltrar: TsBitBtn
      Left = 566
      Top = 22
      Width = 75
      Height = 53
      Cursor = crHandPoint
      Caption = 'Filtrar'
      TabOrder = 2
      OnClick = btFiltrarClick
      ImageIndex = 0
      Images = dm.Imagens
      Reflected = True
      SkinData.SkinSection = 'MAINMENU'
    end
    object btVisualizar: TsBitBtn
      Left = 647
      Top = 22
      Width = 77
      Height = 53
      Cursor = crHandPoint
      Caption = 'Visualizar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = btVisualizarClick
      ImageIndex = 4
      Images = dm.Imagens
      SkinData.SkinSection = 'MAINMENU'
    end
  end
  object GridAtos: TwwDBGrid
    Left = 0
    Top = 84
    Width = 742
    Height = 382
    ControlType.Strings = (
      'Check;CheckBox;True;False')
    Selected.Strings = (
      'Check'#9'5'#9'*'#9'F'
      'PROTOCOLO'#9'10'#9'Protocolo'#9'T'
      'DT_PROTOCOLO'#9'10'#9'Data'#9'T'
      'APRESENTANTE'#9'26'#9'Apresentante'#9'T'
      'DEVEDOR'#9'28'#9'Devedor'#9'T'
      'STATUS'#9'18'#9'Status'#9'F')
    IniAttributes.Delimiter = ';;'
    IniAttributes.UnicodeIniFile = False
    TitleColor = clBtnFace
    FixedCols = 1
    ShowHorzScrollBar = False
    EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm, ecoDisableDateTimePicker]
    Align = alClient
    BorderStyle = bsNone
    DataSource = dsProtocolo
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = []
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
    ParentFont = False
    PopupMenu = PM
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = False
    UseTFields = False
    OnMouseMove = GridAtosMouseMove
    OnFieldChanged = GridAtosFieldChanged
  end
  object dsProtocolo: TDataSource
    DataSet = qryProtocolo
    Left = 64
    Top = 288
  end
  object dsEscreventes: TDataSource
    DataSet = dm.Escreventes
    Left = 136
    Top = 240
  end
  object PM: TPopupMenu
    Left = 136
    Top = 288
    object Marcartodos1: TMenuItem
      Caption = 'Marcar todos'
      OnClick = Marcartodos1Click
    end
    object Desmarcartodos1: TMenuItem
      Caption = 'Desmarcar todos'
      OnClick = Desmarcartodos1Click
    end
  end
  object qryProtocolo: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      'ID_ATO,'
      'PROTOCOLO,'
      'DT_PROTOCOLO,'
      'APRESENTANTE,'
      'DEVEDOR,'
      'CPF_CNPJ_DEVEDOR,'
      'NUMERO_TITULO,'
      'TIPO_TITULO,'
      'DT_TITULO,'
      'DT_VENCIMENTO,'
      'TOTAL,'
      'STATUS,'
      'DT_PUBLICACAO,'
      'TIPO_DEVEDOR,'
      'VALOR_TITULO'
      ''
      ''
      'from TITULOS')
    Left = 64
    Top = 240
    object qryProtocoloCheck: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'Check'
    end
    object qryProtocoloID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryProtocoloPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object qryProtocoloDT_PROTOCOLO: TDateField
      FieldName = 'DT_PROTOCOLO'
      Origin = 'DT_PROTOCOLO'
    end
    object qryProtocoloAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Origin = 'APRESENTANTE'
      Size = 100
    end
    object qryProtocoloDEVEDOR: TStringField
      FieldName = 'DEVEDOR'
      Origin = 'DEVEDOR'
      Size = 100
    end
    object qryProtocoloCPF_CNPJ_DEVEDOR: TStringField
      FieldName = 'CPF_CNPJ_DEVEDOR'
      Origin = 'CPF_CNPJ_DEVEDOR'
      Size = 15
    end
    object qryProtocoloNUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
      Origin = 'NUMERO_TITULO'
    end
    object qryProtocoloTIPO_TITULO: TIntegerField
      FieldName = 'TIPO_TITULO'
      Origin = 'TIPO_TITULO'
    end
    object qryProtocoloDT_TITULO: TDateField
      FieldName = 'DT_TITULO'
      Origin = 'DT_TITULO'
    end
    object qryProtocoloDT_VENCIMENTO: TDateField
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
    end
    object qryProtocoloTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
    end
    object qryProtocoloSTATUS: TStringField
      FieldName = 'STATUS'
      Origin = 'STATUS'
    end
    object qryProtocoloDT_PUBLICACAO: TDateField
      FieldName = 'DT_PUBLICACAO'
      Origin = 'DT_PUBLICACAO'
    end
    object qryProtocoloTIPO_DEVEDOR: TStringField
      FieldName = 'TIPO_DEVEDOR'
      Origin = 'TIPO_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object qryProtocoloVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
      Origin = 'VALOR_TITULO'
    end
  end
end
