object FQuickIntimacao1: TFQuickIntimacao1
  Left = 428
  Top = 122
  Caption = 'Intima'#231#227'o 1'
  ClientHeight = 664
  ClientWidth = 943
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object QkIntimacao: TQuickRep
    Left = 83
    Top = 299
    Width = 794
    Height = 1123
    ShowingPreview = False
    DataSet = RX
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnEndPage = QkIntimacaoEndPage
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = Custom
    Page.Continuous = False
    Page.Values = (
      150.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = False
    PrinterSettings.CustomBinCode = 0
    PrinterSettings.ExtendedDuplex = 0
    PrinterSettings.UseCustomPaperCode = True
    PrinterSettings.CustomPaperCode = 0
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.MemoryLimit = 1000000
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 0
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 100
    PrevFormStyle = fsNormal
    PreviewInitialState = wsMaximized
    PreviewWidth = 500
    PreviewHeight = 500
    PrevInitialZoom = qrZoomToWidth
    PreviewDefaultSaveType = stQRP
    PreviewLeft = 0
    PreviewTop = 0
    object Cabecalho: TQRBand
      Left = 38
      Top = 38
      Width = 718
      Height = 119
      Frame.DrawTop = True
      Frame.DrawBottom = True
      Frame.DrawLeft = True
      Frame.DrawRight = True
      AlignToBottom = False
      BeforePrint = CabecalhoBeforePrint
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        314.854166666666700000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbPageHeader
      object QRLabel1: TQRLabel
        Left = 594
        Top = 26
        Width = 75
        Height = 19
        Size.Values = (
          50.270833333333330000
          1571.625000000000000000
          68.791666666666670000
          198.437500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Intima'#231#227'o'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 12
      end
      object lbTexto1: TQRLabel
        Left = 24
        Top = 64
        Width = 543
        Height = 17
        Size.Values = (
          44.979166666666670000
          63.500000000000000000
          169.333333333333300000
          1436.687500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 
          'Comunico-lhe(s) que est'#225'('#227'o) em meu Cart'#243'rio, para ser (em) Prot' +
          'estado(s) o(s) T'#237'tulo(s) abaixo relacionado(s).'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbTexto2: TQRLabel
        Left = 24
        Top = 84
        Width = 649
        Height = 31
        Size.Values = (
          82.020833333333330000
          63.500000000000000000
          222.250000000000000000
          1717.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = True
        Caption = 
          'Intimo-o(s) por este meio '#224' vir(em) pag'#225'-lo(s), ou dar-me raz'#245'es' +
          ' por que n'#227'o o faz(em), ficando desde j'#225' notificado(s) do respec' +
          'tivo protesto DENTRO DO PRAZO DE 03 (TR'#202'S) DIAS '#218'TEIS CONTADOS D' +
          'E [DATA] conforme lei n'#186' 9.492 de 10 de Setembro de 1997.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object txNomeServentia: TQRLabel
        Left = 24
        Top = 3
        Width = 641
        Height = 17
        Size.Values = (
          44.979166666666670000
          63.500000000000000000
          7.937500000000000000
          1695.979166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'txNomeServentia'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object lbTabeliao: TQRLabel
        Left = 24
        Top = 23
        Width = 49
        Height = 17
        Size.Values = (
          44.979166666666670000
          63.500000000000000000
          60.854166666666670000
          129.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Tabeliao'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object lbAssociado: TQRLabel
        Left = 24
        Top = 42
        Width = 131
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          63.500000000000000000
          111.125000000000000000
          346.604166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Associado ao IEPTB/RJ'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
    end
    object Detalhe: TQRBand
      Left = 38
      Top = 157
      Width = 718
      Height = 916
      Frame.DrawBottom = True
      Frame.DrawLeft = True
      Frame.DrawRight = True
      AlignToBottom = False
      BeforePrint = DetalheBeforePrint
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = True
      Size.Values = (
        2423.583333333333000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object lbDatas: TQRLabel
        Left = 24
        Top = 715
        Width = 433
        Height = 16
        Size.Values = (
          42.333333333333330000
          63.500000000000000000
          1891.770833333333000000
          1145.645833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Entrada e Intima'#231#227'o:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRDBText16: TQRDBText
        Left = 101
        Top = 760
        Width = 404
        Height = 15
        Size.Values = (
          39.687500000000000000
          267.229166666666700000
          2010.833333333333000000
          1068.916666666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Apresentante'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbCNPJ: TQRLabel
        Left = 507
        Top = 760
        Width = 166
        Height = 16
        Size.Values = (
          42.333333333333330000
          1341.437500000000000000
          2010.833333333333000000
          439.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'lbCNPJ'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel18: TQRLabel
        Left = 43
        Top = 760
        Width = 54
        Height = 16
        Size.Values = (
          42.333333333333330000
          113.770833333333300000
          2010.833333333333000000
          142.875000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Portador:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object ShFins: TQRShape
        Left = 480
        Top = 50
        Width = 143
        Height = 17
        Size.Values = (
          44.979166666666670000
          1270.000000000000000000
          132.291666666666700000
          378.354166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Pen.Width = 3
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object lbPrazo: TQRLabel
        Left = 502
        Top = 34
        Width = 92
        Height = 15
        Size.Values = (
          39.687500000000000000
          1328.208333333333000000
          89.958333333333330000
          243.416666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'DATA DO PRAZO:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbHora: TQRLabel
        Left = 24
        Top = 423
        Width = 328
        Height = 16
        Size.Values = (
          42.333333333333330000
          63.500000000000000000
          1119.187500000000000000
          867.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = '- Hor'#225'rio de funcionamento do Cart'#243'rio: 09:00H '#224's 17:30H.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object lbAviso: TQRLabel
        Left = 409
        Top = 423
        Width = 258
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          1082.145833333333000000
          1119.187500000000000000
          682.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = '* J'#225' inclu'#237'do o valor de R$ 17,46 referente a Certid'#227'o.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrLinha2: TQRLabel
        Left = 0
        Top = 529
        Width = 717
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          0.000000000000000000
          1399.645833333333000000
          1897.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ' +
          '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ' +
          '- - - - - - - - - - - - - - - - - - - - - - - - -'
        Color = clWhite
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRLabel4: TQRLabel
        Left = 24
        Top = 2
        Width = 50
        Height = 17
        Size.Values = (
          44.979166666666670000
          63.500000000000000000
          5.291666666666667000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Ilmo Sr(s).'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel5: TQRLabel
        Left = 45
        Top = 70
        Width = 29
        Height = 17
        Size.Values = (
          44.979166666666670000
          119.062500000000000000
          185.208333333333300000
          76.729166666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'T'#237'tulo:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel6: TQRLabel
        Left = 29
        Top = 108
        Width = 45
        Height = 17
        Size.Values = (
          44.979166666666670000
          76.729166666666670000
          285.750000000000000000
          119.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Portador:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel7: TQRLabel
        Left = 30
        Top = 127
        Width = 44
        Height = 17
        Size.Values = (
          44.979166666666670000
          79.375000000000000000
          336.020833333333300000
          116.416666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Cedente:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel8: TQRLabel
        Left = 505
        Top = 108
        Width = 89
        Height = 17
        Size.Values = (
          44.979166666666670000
          1336.145833333333000000
          285.750000000000000000
          235.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Valor do T'#237'tulo R$:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel9: TQRLabel
        Left = 533
        Top = 89
        Width = 61
        Height = 17
        Size.Values = (
          44.979166666666670000
          1410.229166666667000000
          235.479166666666700000
          161.395833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Vencimento:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel10: TQRLabel
        Left = 522
        Top = 2
        Width = 72
        Height = 17
        Size.Values = (
          44.979166666666670000
          1381.125000000000000000
          5.291666666666667000
          190.500000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'PROTOCOLO:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel11: TQRLabel
        Left = 29
        Top = 146
        Width = 45
        Height = 17
        Size.Values = (
          44.979166666666670000
          76.729166666666670000
          386.291666666666700000
          119.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Sacador:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbTexto3: TQRLabel
        Left = 24
        Top = 278
        Width = 641
        Height = 60
        Size.Values = (
          158.750000000000000000
          63.500000000000000000
          735.541666666666700000
          1695.979166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          'Buscando evitar futuros transtornos, assim como o protesto do t'#237 +
          'tulo listado acima, solicitamos que seja providenciado o pagamen' +
          'to atrav'#233's de boleto banc'#225'rio em dinheiro ou atrav'#233's de cheque a' +
          'dministrativo (de qualquer banco), no valor de R$ [VALOR] nomina' +
          'l a [BANCO]'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbTexto4: TQRLabel
        Left = 24
        Top = 340
        Width = 641
        Height = 30
        Size.Values = (
          79.375000000000000000
          63.500000000000000000
          899.583333333333300000
          1695.979166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          'ATEN'#199#195'O: Para pagamento em dinheiro, '#233' necess'#225'rio o pr'#233'vio compa' +
          'recimento ao cart'#243'rio para a retirada da guia de pagamento. Ap'#243's' +
          ' o pagamento, retornar ao cart'#243'rio no mesmo dia para a quita'#231#227'o ' +
          'do t'#237'tulo. '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object Sh14: TQRShape
        Left = 25
        Top = 243
        Width = 317
        Height = 1
        Size.Values = (
          2.645833333333333000
          66.145833333333330000
          642.937500000000000000
          838.729166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object lbAssinatura: TQRLabel
        Left = 121
        Top = 245
        Width = 125
        Height = 17
        Size.Values = (
          44.979166666666670000
          320.145833333333300000
          648.229166666666700000
          330.729166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        Caption = 'TABELI'#195'O DE PROTESTO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText4: TQRDBText
        Left = 77
        Top = 70
        Width = 400
        Height = 17
        Size.Values = (
          44.979166666666670000
          203.729166666666700000
          185.208333333333300000
          1058.333333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Titulo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText5: TQRDBText
        Left = 77
        Top = 108
        Width = 400
        Height = 17
        Size.Values = (
          44.979166666666670000
          203.729166666666700000
          285.750000000000000000
          1058.333333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Apresentante'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText6: TQRDBText
        Left = 77
        Top = 127
        Width = 400
        Height = 17
        Size.Values = (
          44.979166666666670000
          203.729166666666700000
          336.020833333333300000
          1058.333333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Credor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText7: TQRDBText
        Left = 77
        Top = 146
        Width = 400
        Height = 17
        Size.Values = (
          44.979166666666670000
          203.729166666666700000
          386.291666666666700000
          1058.333333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Sacador'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrCustas: TQRLabel
        Left = 540
        Top = 127
        Width = 54
        Height = 17
        Size.Values = (
          44.979166666666670000
          1428.750000000000000000
          336.020833333333300000
          142.875000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Custas R$:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbChequeAdm: TQRLabel
        Left = 294
        Top = 165
        Width = 300
        Height = 17
        Size.Values = (
          44.979166666666670000
          777.875000000000000000
          436.562500000000000000
          793.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Valor para pagamento em Cheque Administrativo R$:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRDBText8: TQRDBText
        Left = 597
        Top = 2
        Width = 54
        Height = 17
        Size.Values = (
          44.979166666666670000
          1579.562500000000000000
          5.291666666666667000
          142.875000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'Protocolo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText9: TQRDBText
        Left = 597
        Top = 108
        Width = 27
        Height = 17
        Size.Values = (
          44.979166666666670000
          1579.562500000000000000
          285.750000000000000000
          71.437500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'Valor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText10: TQRDBText
        Left = 597
        Top = 127
        Width = 35
        Height = 17
        Size.Values = (
          44.979166666666670000
          1579.562500000000000000
          336.020833333333300000
          92.604166666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'Custas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorAdm: TQRDBText
        Left = 597
        Top = 165
        Width = 74
        Height = 17
        Size.Values = (
          44.979166666666670000
          1579.562500000000000000
          436.562500000000000000
          195.791666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'ValorCheque'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRDBText12: TQRDBText
        Left = 77
        Top = 2
        Width = 400
        Height = 17
        Size.Values = (
          44.979166666666670000
          203.729166666666700000
          5.291666666666667000
          1058.333333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Devedor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrVencimento: TQRDBText
        Left = 597
        Top = 89
        Width = 58
        Height = 17
        Size.Values = (
          44.979166666666670000
          1579.562500000000000000
          235.479166666666700000
          153.458333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'Vencimento'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrrLinha2: TQRLabel
        Left = 0
        Top = 629
        Width = 717
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          0.000000000000000000
          1664.229166666667000000
          1897.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ' +
          '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ' +
          '- - - - - - - - - - - - - - - - - - - - - - - - - '
        Color = clWhite
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrrTesoura2: TQRImage
        Left = 0
        Top = 620
        Width = 18
        Height = 20
        Enabled = False
        Size.Values = (
          52.916666666666670000
          0.000000000000000000
          1640.416666666667000000
          47.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        AutoSize = True
        Picture.Data = {
          07544269746D617066010000424D660100000000000076000000280000001400
          0000140000000100040000000000F0000000120B0000120B0000100000000000
          0000000000000000800000800000008080008000000080008000808000008080
          8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
          FF00FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFF1111FFFFFFFFF0FF0FFFFFFF1FF
          F1FFFFFFF0FF0FFFF111F1FFFF1FFFFF0FF0FFFFF1FFF11FFF11FFF0FF0FFFFF
          F1FFFF1111111F0FF0FFFFFFF11FFFFFF11111FF0FFFFFFFFF11FFFFFFF1F1F0
          FFFFFFFFFFFFFFFFF11111FF0FFFFFFFFFFFFF1111111F0FF0FFFFFFFFFFF11F
          FF11FFF0FF0FFFFFFF11F1FFFF1FFFFF0FF0FFFFF11FF1FFF1FFFFFFF0FF0FFF
          F1FFFF111FFFFFFFFF0FF0FFF1FFFFFFFFFFFFFFFFF000FFFF11FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        Stretch = True
      end
      object QRDBText1: TQRDBText
        Left = 24
        Top = 647
        Width = 78
        Height = 17
        Size.Values = (
          44.979166666666670000
          63.500000000000000000
          1711.854166666667000000
          206.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = dm.Serventia
        DataField = 'DESCRICAO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRDBText2: TQRDBText
        Left = 24
        Top = 663
        Width = 55
        Height = 17
        Size.Values = (
          44.979166666666670000
          63.500000000000000000
          1754.187500000000000000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = dm.Serventia
        DataField = 'ENDERECO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText3: TQRDBText
        Left = 24
        Top = 680
        Width = 52
        Height = 17
        Size.Values = (
          44.979166666666670000
          63.500000000000000000
          1799.166666666667000000
          137.583333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = dm.Serventia
        DataField = 'TELEFONE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape1: TQRShape
        Left = 19
        Top = 714
        Width = 665
        Height = 1
        Size.Values = (
          2.645833333333333000
          50.270833333333330000
          1889.125000000000000000
          1759.479166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape2: TQRShape
        Left = 19
        Top = 644
        Width = 666
        Height = 1
        Size.Values = (
          2.645833333333333000
          50.270833333333330000
          1703.916666666667000000
          1762.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape3: TQRShape
        Left = 19
        Top = 774
        Width = 665
        Height = 1
        Size.Values = (
          2.645833333333333000
          50.270833333333330000
          2047.875000000000000000
          1759.479166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape4: TQRShape
        Left = 19
        Top = 799
        Width = 666
        Height = 1
        Size.Values = (
          2.645833333333333000
          50.270833333333330000
          2114.020833333333000000
          1762.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape5: TQRShape
        Left = 438
        Top = 646
        Width = 1
        Height = 69
        Size.Values = (
          182.562500000000000000
          1158.875000000000000000
          1709.208333333333000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape6: TQRShape
        Left = 683
        Top = 646
        Width = 1
        Height = 154
        Size.Values = (
          407.458333333333300000
          1807.104166666667000000
          1709.208333333333000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object lbComprovante: TQRLabel
        Left = 442
        Top = 647
        Width = 189
        Height = 16
        Size.Values = (
          42.333333333333330000
          1169.458333333333000000
          1711.854166666667000000
          500.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Comprovante de entrega pessoal'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel15: TQRLabel
        Left = 442
        Top = 663
        Width = 59
        Height = 17
        Size.Values = (
          44.979166666666670000
          1169.458333333333000000
          1754.187500000000000000
          156.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Protocolo:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel17: TQRLabel
        Left = 24
        Top = 730
        Width = 73
        Height = 16
        Size.Values = (
          42.333333333333330000
          63.500000000000000000
          1931.458333333333000000
          193.145833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Destinat'#225'rio:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel19: TQRLabel
        Left = 39
        Top = 745
        Width = 58
        Height = 16
        Size.Values = (
          42.333333333333330000
          103.187500000000000000
          1971.145833333333000000
          153.458333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Endere'#231'o:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel20: TQRLabel
        Left = 21
        Top = 780
        Width = 161
        Height = 17
        Size.Values = (
          44.979166666666670000
          55.562500000000000000
          2063.750000000000000000
          425.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Recebido em ___/___/_____'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel21: TQRLabel
        Left = 187
        Top = 780
        Width = 66
        Height = 17
        Size.Values = (
          44.979166666666670000
          494.770833333333300000
          2063.750000000000000000
          174.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Assinatura:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRShape8: TQRShape
        Left = 256
        Top = 792
        Width = 237
        Height = 1
        Size.Values = (
          2.645833333333333000
          677.333333333333300000
          2095.500000000000000000
          627.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape9: TQRShape
        Left = 19
        Top = 802
        Width = 15
        Height = 15
        Size.Values = (
          39.687500000000000000
          50.270833333333330000
          2121.958333333333000000
          39.687500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape10: TQRShape
        Left = 19
        Top = 819
        Width = 15
        Height = 15
        Size.Values = (
          39.687500000000000000
          50.270833333333330000
          2166.937500000000000000
          39.687500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel23: TQRLabel
        Left = 39
        Top = 802
        Width = 97
        Height = 17
        Size.Values = (
          44.979166666666670000
          103.187500000000000000
          2121.958333333333000000
          256.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Mudou de endere'#231'o'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel24: TQRLabel
        Left = 39
        Top = 819
        Width = 105
        Height = 17
        Size.Values = (
          44.979166666666670000
          103.187500000000000000
          2166.937500000000000000
          277.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Endere'#231'o Insuficiente'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape11: TQRShape
        Left = 155
        Top = 802
        Width = 15
        Height = 15
        Size.Values = (
          39.687500000000000000
          410.104166666666700000
          2121.958333333333000000
          39.687500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel25: TQRLabel
        Left = 175
        Top = 802
        Width = 70
        Height = 17
        Size.Values = (
          44.979166666666670000
          463.020833333333300000
          2121.958333333333000000
          185.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Desconhecido'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape12: TQRShape
        Left = 155
        Top = 819
        Width = 15
        Height = 15
        Size.Values = (
          39.687500000000000000
          410.104166666666700000
          2166.937500000000000000
          39.687500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel26: TQRLabel
        Left = 175
        Top = 819
        Width = 42
        Height = 17
        Size.Values = (
          44.979166666666670000
          463.020833333333300000
          2166.937500000000000000
          111.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Ausente'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape13: TQRShape
        Left = 256
        Top = 802
        Width = 15
        Height = 15
        Size.Values = (
          39.687500000000000000
          677.333333333333300000
          2121.958333333333000000
          39.687500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel27: TQRLabel
        Left = 276
        Top = 802
        Width = 50
        Height = 17
        Size.Values = (
          44.979166666666670000
          730.250000000000000000
          2121.958333333333000000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Recusado'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape14: TQRShape
        Left = 256
        Top = 819
        Width = 15
        Height = 15
        Size.Values = (
          39.687500000000000000
          677.333333333333300000
          2166.937500000000000000
          39.687500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel28: TQRLabel
        Left = 276
        Top = 819
        Width = 93
        Height = 17
        Size.Values = (
          44.979166666666670000
          730.250000000000000000
          2166.937500000000000000
          246.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'N'#250'mero inexistente'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel29: TQRLabel
        Left = 480
        Top = 815
        Width = 113
        Height = 17
        Size.Values = (
          44.979166666666670000
          1270.000000000000000000
          2156.354166666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Data ___/___/_____'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel30: TQRLabel
        Left = 480
        Top = 835
        Width = 77
        Height = 17
        Size.Values = (
          44.979166666666670000
          1270.000000000000000000
          2209.270833333333000000
          203.729166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Respons'#225'vel:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRShape15: TQRShape
        Left = 559
        Top = 847
        Width = 124
        Height = 1
        Size.Values = (
          2.645833333333333000
          1479.020833333333000000
          2241.020833333333000000
          328.083333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape16: TQRShape
        Left = 19
        Top = 646
        Width = 1
        Height = 154
        Size.Values = (
          407.458333333333300000
          50.270833333333330000
          1709.208333333333000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRDBText14: TQRDBText
        Left = 101
        Top = 730
        Width = 404
        Height = 15
        Size.Values = (
          39.687500000000000000
          267.229166666666700000
          1931.458333333333000000
          1068.916666666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Devedor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText15: TQRDBText
        Left = 101
        Top = 745
        Width = 572
        Height = 15
        Size.Values = (
          39.687500000000000000
          267.229166666666700000
          1971.145833333333000000
          1513.416666666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Endereco'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText17: TQRDBText
        Left = 505
        Top = 663
        Width = 56
        Height = 17
        Size.Values = (
          44.979166666666670000
          1336.145833333333000000
          1754.187500000000000000
          148.166666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'Protocolo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRDBText19: TQRDBText
        Left = 77
        Top = 18
        Width = 108
        Height = 17
        Size.Values = (
          44.979166666666670000
          203.729166666666700000
          47.625000000000000000
          285.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'CPF_CNPJ_Devedor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel3: TQRLabel
        Left = 30
        Top = 89
        Width = 44
        Height = 17
        Size.Values = (
          44.979166666666670000
          79.375000000000000000
          235.479166666666700000
          116.416666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'N'#186' T'#237'tulo:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText20: TQRDBText
        Left = 77
        Top = 89
        Width = 400
        Height = 17
        Size.Values = (
          44.979166666666670000
          203.729166666666700000
          235.479166666666700000
          1058.333333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'NumeroTitulo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel12: TQRLabel
        Left = 550
        Top = 70
        Width = 44
        Height = 17
        Size.Values = (
          44.979166666666670000
          1455.208333333333000000
          185.208333333333300000
          116.416666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Emiss'#227'o:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText21: TQRDBText
        Left = 597
        Top = 70
        Width = 41
        Height = 17
        Size.Values = (
          44.979166666666670000
          1579.562500000000000000
          185.208333333333300000
          108.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'Emissao'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText22: TQRDBText
        Left = 597
        Top = 18
        Width = 77
        Height = 17
        Size.Values = (
          44.979166666666670000
          1579.562500000000000000
          47.625000000000000000
          203.729166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'DataProtocolo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel13: TQRLabel
        Left = 491
        Top = 18
        Width = 103
        Height = 17
        Size.Values = (
          44.979166666666670000
          1299.104166666667000000
          47.625000000000000000
          272.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'DATA DE ENTRADA:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText23: TQRDBText
        Left = 77
        Top = 34
        Width = 400
        Height = 17
        Size.Values = (
          44.979166666666670000
          203.729166666666700000
          89.958333333333330000
          1058.333333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Endereco'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape17: TQRShape
        Left = 571
        Top = 792
        Width = 110
        Height = 1
        Size.Values = (
          2.645833333333333000
          1510.770833333333000000
          2095.500000000000000000
          291.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel34: TQRLabel
        Left = 498
        Top = 780
        Width = 69
        Height = 17
        Size.Values = (
          44.979166666666670000
          1317.625000000000000000
          2063.750000000000000000
          182.562500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Documento:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object lbTarifa: TQRLabel
        Left = 501
        Top = 184
        Width = 93
        Height = 17
        Size.Values = (
          44.979166666666670000
          1325.562500000000000000
          486.833333333333300000
          246.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Tarifa banc'#225'ria R$:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrTarifa: TQRDBText
        Left = 597
        Top = 184
        Width = 29
        Height = 17
        Size.Values = (
          44.979166666666670000
          1579.562500000000000000
          486.833333333333300000
          76.729166666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'Tarifa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText18: TQRDBText
        Left = 19
        Top = 865
        Width = 665
        Height = 17
        Size.Values = (
          44.979166666666670000
          50.270833333333330000
          2288.645833333333000000
          1759.479166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Observacao'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object lbObs: TQRLabel
        Left = 19
        Top = 853
        Width = 70
        Height = 15
        Size.Values = (
          39.687500000000000000
          50.270833333333330000
          2256.895833333333000000
          185.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Observa'#231#245'es:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object MMensagem: TQRMemo
        Left = 19
        Top = 883
        Width = 665
        Height = 17
        Size.Values = (
          44.979166666666670000
          50.270833333333330000
          2336.270833333333000000
          1759.479166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 8
      end
      object qrSaldo: TQRLabel
        Left = 442
        Top = 680
        Width = 86
        Height = 17
        Size.Values = (
          44.979166666666670000
          1169.458333333333000000
          1799.166666666667000000
          227.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Saldo Devedor:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrValorDeposito: TQRDBText
        Left = 609
        Top = 681
        Width = 66
        Height = 17
        Size.Values = (
          44.979166666666670000
          1611.312500000000000000
          1801.812500000000000000
          174.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'ValorDeposito'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDeposito: TQRLabel
        Left = 529
        Top = 680
        Width = 77
        Height = 17
        Size.Values = (
          44.979166666666670000
          1399.645833333333000000
          1799.166666666667000000
          203.729166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = '(Dep'#243'sito) R$'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrCheque: TQRLabel
        Left = 442
        Top = 697
        Width = 155
        Height = 17
        Size.Values = (
          44.979166666666670000
          1169.458333333333000000
          1844.145833333333000000
          410.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = '(Cheque Administrativo) R$'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrValorCheque: TQRDBText
        Left = 609
        Top = 697
        Width = 66
        Height = 15
        Size.Values = (
          39.687500000000000000
          1611.312500000000000000
          1844.145833333333000000
          174.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'ValorCheque'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel14: TQRLabel
        Left = 42
        Top = 165
        Width = 32
        Height = 17
        Size.Values = (
          44.979166666666670000
          111.125000000000000000
          436.562500000000000000
          84.666666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Pra'#231'a:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText13: TQRDBText
        Left = 77
        Top = 165
        Width = 29
        Height = 17
        Size.Values = (
          44.979166666666670000
          203.729166666666700000
          436.562500000000000000
          76.729166666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'Praca'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel35: TQRLabel
        Left = 518
        Top = 146
        Width = 76
        Height = 17
        Size.Values = (
          44.979166666666670000
          1370.541666666667000000
          386.291666666666700000
          201.083333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Distribui'#231#227'o R$:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText27: TQRDBText
        Left = 597
        Top = 146
        Width = 57
        Height = 17
        Size.Values = (
          44.979166666666670000
          1579.562500000000000000
          386.291666666666700000
          150.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'Distribuicao'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbBoleto: TQRLabel
        Left = 325
        Top = 203
        Width = 269
        Height = 17
        Size.Values = (
          44.979166666666670000
          859.895833333333300000
          537.104166666666700000
          711.729166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Valor para pagamento com Boleto Banc'#225'rio R$:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrBoleto: TQRDBText
        Left = 597
        Top = 203
        Width = 81
        Height = 17
        Size.Values = (
          44.979166666666670000
          1579.562500000000000000
          537.104166666666700000
          214.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'ValorDeposito'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object sh1: TQRShape
        Left = 19
        Top = 442
        Width = 665
        Height = 92
        Enabled = False
        Size.Values = (
          243.416666666666700000
          50.270833333333330000
          1169.458333333333000000
          1759.479166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrTesoura2: TQRImage
        Left = 0
        Top = 529
        Width = 18
        Height = 20
        Enabled = False
        Size.Values = (
          52.916666666666670000
          0.000000000000000000
          1399.645833333333000000
          47.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        AutoSize = True
        Picture.Data = {
          07544269746D617066010000424D660100000000000076000000280000001400
          0000140000000100040000000000F0000000120B0000120B0000100000000000
          0000000000000000800000800000008080008000000080008000808000008080
          8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
          FF00FFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFF000FFFFFFF1111FFFFFFFFF0FF0FFFFFFF1FF
          F1FFFFFFF0FF0FFFF111F1FFFF1FFFFF0FF0FFFFF1FFF11FFF11FFF0FF0FFFFF
          F1FFFF1111111F0FF0FFFFFFF11FFFFFF11111FF0FFFFFFFFF11FFFFFFF1F1F0
          FFFFFFFFFFFFFFFFF11111FF0FFFFFFFFFFFFF1111111F0FF0FFFFFFFFFFF11F
          FF11FFF0FF0FFFFFFF11F1FFFF1FFFFF0FF0FFFFF11FF1FFF1FFFFFFF0FF0FFF
          F1FFFF111FFFFFFFFF0FF0FFF1FFFFFFFFFFFFFFFFF000FFFF11FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        Stretch = True
      end
      object sh2: TQRShape
        Left = 19
        Top = 460
        Width = 665
        Height = 1
        Enabled = False
        Size.Values = (
          2.645833333333333000
          50.270833333333330000
          1217.083333333333000000
          1759.479166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrGuia: TQRLabel
        Left = 477
        Top = 444
        Width = 203
        Height = 16
        Enabled = False
        Size.Values = (
          42.333333333333330000
          1262.062500000000000000
          1174.750000000000000000
          537.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        Caption = 'Guia de Dep'#243'sito em Conta Corrente'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object sh12: TQRShape
        Left = 19
        Top = 480
        Width = 665
        Height = 1
        Enabled = False
        Size.Values = (
          2.645833333333333000
          50.270833333333330000
          1270.000000000000000000
          1759.479166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object sh3: TQRShape
        Left = 146
        Top = 460
        Width = 1
        Height = 74
        Enabled = False
        Size.Values = (
          195.791666666666700000
          386.291666666666700000
          1217.083333333333000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrFavorecido: TQRLabel
        Left = 27
        Top = 463
        Width = 116
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          71.437500000000000000
          1225.020833333333000000
          306.916666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome do Favorecido'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrServentia: TQRDBText
        Left = 150
        Top = 463
        Width = 319
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          396.875000000000000000
          1225.020833333333000000
          844.020833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = dm.Serventia
        DataField = 'DESCRICAO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object sh4: TQRShape
        Left = 474
        Top = 442
        Width = 1
        Height = 92
        Enabled = False
        Size.Values = (
          243.416666666666700000
          1254.125000000000000000
          1169.458333333333000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrValido: TQRLabel
        Left = 486
        Top = 463
        Width = 175
        Height = 16
        Enabled = False
        Size.Values = (
          42.333333333333330000
          1285.875000000000000000
          1225.020833333333000000
          463.020833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'N'#195'O '#201' V'#193'LIDO NO CAIXA R'#193'PIDO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrAgencia: TQRLabel
        Left = 94
        Top = 482
        Width = 49
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          248.708333333333300000
          1275.291666666667000000
          129.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ag'#234'ncia'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrConta: TQRLabel
        Left = 238
        Top = 482
        Width = 51
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          629.708333333333300000
          1275.291666666667000000
          134.937500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'N'#186' Conta'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrData: TQRLabel
        Left = 376
        Top = 482
        Width = 94
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          994.833333333333300000
          1275.291666666667000000
          248.708333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Data de Emiss'#227'o'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrTipo: TQRLabel
        Left = 435
        Top = 500
        Width = 37
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          1150.937500000000000000
          1322.916666666667000000
          97.895833333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'CNPJ'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object sh13: TQRShape
        Left = 19
        Top = 498
        Width = 666
        Height = 1
        Enabled = False
        Size.Values = (
          2.645833333333333000
          50.270833333333330000
          1317.625000000000000000
          1762.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrDepositado: TQRLabel
        Left = 55
        Top = 500
        Width = 88
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          145.520833333333300000
          1322.916666666667000000
          232.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Depositado por'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDevedor: TQRDBText
        Left = 150
        Top = 500
        Width = 275
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          396.875000000000000000
          1322.916666666667000000
          727.604166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Devedor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object sh5: TQRShape
        Left = 235
        Top = 480
        Width = 1
        Height = 18
        Enabled = False
        Size.Values = (
          47.625000000000000000
          621.770833333333300000
          1270.000000000000000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object sh7: TQRShape
        Left = 371
        Top = 480
        Width = 1
        Height = 18
        Enabled = False
        Size.Values = (
          47.625000000000000000
          981.604166666666700000
          1270.000000000000000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object sh6: TQRShape
        Left = 292
        Top = 480
        Width = 1
        Height = 19
        Enabled = False
        Size.Values = (
          50.270833333333330000
          772.583333333333300000
          1270.000000000000000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrDocumento: TQRDBText
        Left = 477
        Top = 500
        Width = 203
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          1262.062500000000000000
          1322.916666666667000000
          537.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'CPF_CNPJ_Devedor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object sh9: TQRShape
        Left = 428
        Top = 498
        Width = 1
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          1132.416666666667000000
          1317.625000000000000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object sh8: TQRShape
        Left = 19
        Top = 515
        Width = 665
        Height = 1
        Enabled = False
        Size.Values = (
          2.645833333333333000
          50.270833333333330000
          1362.604166666667000000
          1759.479166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrEmissao: TQRLabel
        Left = 477
        Top = 482
        Width = 69
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          1262.062500000000000000
          1275.291666666667000000
          182.562500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrEmissao'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object sh11: TQRShape
        Left = 549
        Top = 480
        Width = 1
        Height = 19
        Enabled = False
        Size.Values = (
          50.270833333333330000
          1452.562500000000000000
          1270.000000000000000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrProtocolo: TQRLabel
        Left = 552
        Top = 482
        Width = 57
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          1460.500000000000000000
          1275.291666666667000000
          150.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Protocolo'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object sh10: TQRShape
        Left = 613
        Top = 480
        Width = 1
        Height = 19
        Enabled = False
        Size.Values = (
          50.270833333333330000
          1621.895833333333000000
          1270.000000000000000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrNumProtocolo: TQRDBText
        Left = 617
        Top = 482
        Width = 64
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          1632.479166666667000000
          1275.291666666667000000
          169.333333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Protocolo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrNumConta: TQRLabel
        Left = 296
        Top = 482
        Width = 72
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          783.166666666666700000
          1275.291666666667000000
          190.500000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNumConta'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrNumAgencia: TQRLabel
        Left = 150
        Top = 482
        Width = 83
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          396.875000000000000000
          1275.291666666667000000
          219.604166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNumAgencia'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDinheiro: TQRLabel
        Left = 53
        Top = 517
        Width = 90
        Height = 16
        Enabled = False
        Size.Values = (
          42.333333333333330000
          140.229166666666700000
          1367.895833333333000000
          238.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Em Dinheiro R$'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValor: TQRDBText
        Left = 150
        Top = 517
        Width = 79
        Height = 16
        Enabled = False
        Size.Values = (
          42.333333333333330000
          396.875000000000000000
          1367.895833333333000000
          209.020833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'ValorDeposito'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrBanco: TQRLabel
        Left = 21
        Top = 444
        Width = 39
        Height = 16
        Enabled = False
        Size.Values = (
          42.333333333333330000
          55.562500000000000000
          1174.750000000000000000
          103.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'BANCO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText29: TQRDBText
        Left = 77
        Top = 51
        Width = 22
        Height = 17
        Size.Values = (
          44.979166666666670000
          203.729166666666700000
          134.937500000000000000
          58.208333333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'CEP'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbBanco: TQRLabel
        Left = 611
        Top = 222
        Width = 44
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          1616.604166666667000000
          587.375000000000000000
          116.416666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'lbBanco'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbAgencia: TQRLabel
        Left = 600
        Top = 242
        Width = 55
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          1587.500000000000000000
          640.291666666666700000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'lbAgencia'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbConta: TQRLabel
        Left = 612
        Top = 262
        Width = 43
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          1619.250000000000000000
          693.208333333333300000
          113.770833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'lbConta'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbConvenio: TQRLabel
        Left = 24
        Top = 374
        Width = 397
        Height = 16
        Size.Values = (
          42.333333333333330000
          63.500000000000000000
          989.541666666666700000
          1050.395833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 
          '- ATO NORMATIVO CONJUNTO N'#186' 11/2010 TJERJ (ANTIGO 05/2005 - CONV' +
          #202'NIO)'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrPrazo: TQRDBText
        Left = 597
        Top = 34
        Width = 55
        Height = 15
        Size.Values = (
          39.687500000000000000
          1579.562500000000000000
          89.958333333333330000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'DataPrazo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbLimite: TQRLabel
        Left = 24
        Top = 392
        Width = 641
        Height = 29
        Enabled = False
        Size.Values = (
          76.729166666666670000
          63.500000000000000000
          1037.166666666667000000
          1695.979166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          '- Se esta intima'#231#227'o for recebida na data limite para o pagamento' +
          ' em Cart'#243'rio, ou depois desta, o pagamento dever'#225' ser feito no p' +
          'rimeiro dia '#250'til seguinte (art. 13 da Lei 9492/97).'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbMatricula: TQRLabel
        Left = 177
        Top = 260
        Width = 13
        Height = 17
        Size.Values = (
          44.979166666666670000
          468.312500000000000000
          687.916666666666700000
          34.395833333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        Caption = '    '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object ssh1: TQRShape
        Left = 19
        Top = 551
        Width = 664
        Height = 92
        Enabled = False
        Size.Values = (
          243.416666666666700000
          50.270833333333330000
          1457.854166666667000000
          1756.833333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object ssh2: TQRShape
        Left = 19
        Top = 569
        Width = 664
        Height = 1
        Enabled = False
        Size.Values = (
          2.645833333333333000
          50.270833333333330000
          1505.479166666667000000
          1756.833333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrrGuia: TQRLabel
        Left = 477
        Top = 553
        Width = 203
        Height = 16
        Enabled = False
        Size.Values = (
          42.333333333333330000
          1262.062500000000000000
          1463.145833333333000000
          537.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        Caption = 'Guia de Dep'#243'sito em Conta Corrente'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object ssh12: TQRShape
        Left = 19
        Top = 589
        Width = 663
        Height = 1
        Enabled = False
        Size.Values = (
          2.645833333333333000
          50.270833333333330000
          1558.395833333333000000
          1754.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object ssh3: TQRShape
        Left = 146
        Top = 569
        Width = 1
        Height = 74
        Enabled = False
        Size.Values = (
          195.791666666666700000
          386.291666666666700000
          1505.479166666667000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrrFavorecido: TQRLabel
        Left = 27
        Top = 572
        Width = 116
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          71.437500000000000000
          1513.416666666667000000
          306.916666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome do Favorecido'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrrServentia: TQRDBText
        Left = 150
        Top = 572
        Width = 323
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          396.875000000000000000
          1513.416666666667000000
          854.604166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = dm.Serventia
        DataField = 'DESCRICAO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object ssh4: TQRShape
        Left = 475
        Top = 551
        Width = 1
        Height = 92
        Enabled = False
        Size.Values = (
          243.416666666666700000
          1256.770833333333000000
          1457.854166666667000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrrValido: TQRLabel
        Left = 486
        Top = 572
        Width = 175
        Height = 16
        Enabled = False
        Size.Values = (
          42.333333333333330000
          1285.875000000000000000
          1513.416666666667000000
          463.020833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'N'#195'O '#201' V'#193'LIDO NO CAIXA R'#193'PIDO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrrAgencia: TQRLabel
        Left = 94
        Top = 591
        Width = 49
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          248.708333333333300000
          1563.687500000000000000
          129.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ag'#234'ncia'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrrConta: TQRLabel
        Left = 233
        Top = 591
        Width = 51
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          616.479166666666700000
          1563.687500000000000000
          134.937500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'N'#186' Conta'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrrData: TQRLabel
        Left = 358
        Top = 591
        Width = 99
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          947.208333333333300000
          1563.687500000000000000
          261.937500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Data de Emiss'#227'o'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrrTipo: TQRLabel
        Left = 436
        Top = 609
        Width = 37
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          1153.583333333333000000
          1611.312500000000000000
          97.895833333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'CNPJ'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object ssh13: TQRShape
        Left = 19
        Top = 607
        Width = 664
        Height = 1
        Enabled = False
        Size.Values = (
          2.645833333333333000
          50.270833333333330000
          1606.020833333333000000
          1756.833333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrrDepositado: TQRLabel
        Left = 55
        Top = 609
        Width = 88
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          145.520833333333300000
          1611.312500000000000000
          232.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Depositado por'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrrDevedor: TQRDBText
        Left = 150
        Top = 609
        Width = 275
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          396.875000000000000000
          1611.312500000000000000
          727.604166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Devedor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object ssh5: TQRShape
        Left = 229
        Top = 589
        Width = 1
        Height = 18
        Enabled = False
        Size.Values = (
          47.625000000000000000
          605.895833333333300000
          1558.395833333333000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object ssh7: TQRShape
        Left = 358
        Top = 589
        Width = 1
        Height = 18
        Enabled = False
        Size.Values = (
          47.625000000000000000
          947.208333333333300000
          1558.395833333333000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object ssh6: TQRShape
        Left = 287
        Top = 589
        Width = 1
        Height = 19
        Enabled = False
        Size.Values = (
          50.270833333333330000
          759.354166666666700000
          1558.395833333333000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrrDocumento: TQRDBText
        Left = 477
        Top = 609
        Width = 203
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          1262.062500000000000000
          1611.312500000000000000
          537.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'CPF_CNPJ_Devedor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object ssh9: TQRShape
        Left = 430
        Top = 607
        Width = 1
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          1137.708333333333000000
          1606.020833333333000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object ssh8: TQRShape
        Left = 19
        Top = 624
        Width = 664
        Height = 1
        Enabled = False
        Size.Values = (
          2.645833333333333000
          50.270833333333330000
          1651.000000000000000000
          1756.833333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrrEmissao: TQRLabel
        Left = 477
        Top = 591
        Width = 69
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          1262.062500000000000000
          1563.687500000000000000
          182.562500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrEmissao'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object ssh11: TQRShape
        Left = 549
        Top = 589
        Width = 1
        Height = 19
        Enabled = False
        Size.Values = (
          50.270833333333330000
          1452.562500000000000000
          1558.395833333333000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrrProtocolo: TQRLabel
        Left = 552
        Top = 591
        Width = 57
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          1460.500000000000000000
          1563.687500000000000000
          150.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Protocolo'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object ssh10: TQRShape
        Left = 613
        Top = 589
        Width = 1
        Height = 19
        Enabled = False
        Size.Values = (
          50.270833333333330000
          1621.895833333333000000
          1558.395833333333000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrrNumProtocolo: TQRDBText
        Left = 616
        Top = 591
        Width = 64
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          1629.833333333333000000
          1563.687500000000000000
          169.333333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Protocolo'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrrNumConta: TQRLabel
        Left = 293
        Top = 591
        Width = 62
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          775.229166666666700000
          1563.687500000000000000
          164.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNumConta'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrrNumAgencia: TQRLabel
        Left = 150
        Top = 591
        Width = 75
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          396.875000000000000000
          1563.687500000000000000
          198.437500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNumAgencia'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrrDinheiro: TQRLabel
        Left = 53
        Top = 626
        Width = 90
        Height = 16
        Enabled = False
        Size.Values = (
          42.333333333333330000
          140.229166666666700000
          1656.291666666667000000
          238.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Em Dinheiro R$'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrrValor: TQRDBText
        Left = 150
        Top = 626
        Width = 79
        Height = 16
        Enabled = False
        Size.Values = (
          42.333333333333330000
          396.875000000000000000
          1656.291666666667000000
          209.020833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = RX
        DataField = 'ValorDeposito'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrrBanco: TQRLabel
        Left = 21
        Top = 553
        Width = 39
        Height = 16
        Enabled = False
        Size.Values = (
          42.333333333333330000
          55.562500000000000000
          1463.145833333333000000
          103.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'BANCO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel2: TQRLabel
        Left = 396
        Top = 802
        Width = 39
        Height = 17
        Size.Values = (
          44.979166666666670000
          1047.750000000000000000
          2121.958333333333000000
          103.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Faleceu'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape18: TQRShape
        Left = 376
        Top = 802
        Width = 15
        Height = 15
        Size.Values = (
          39.687500000000000000
          994.833333333333300000
          2121.958333333333000000
          39.687500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRDBText11: TQRDBText
        Left = 597
        Top = 52
        Width = 24
        Height = 15
        Size.Values = (
          39.687500000000000000
          1579.562500000000000000
          137.583333333333300000
          63.500000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = RX
        DataField = 'Fins'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel22: TQRLabel
        Left = 396
        Top = 819
        Width = 43
        Height = 17
        Size.Values = (
          44.979166666666670000
          1047.750000000000000000
          2166.937500000000000000
          113.770833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Fechado'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape19: TQRShape
        Left = 376
        Top = 819
        Width = 15
        Height = 15
        Size.Values = (
          39.687500000000000000
          994.833333333333300000
          2166.937500000000000000
          39.687500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape20: TQRShape
        Left = 19
        Top = 836
        Width = 15
        Height = 15
        Size.Values = (
          39.687500000000000000
          50.270833333333330000
          2211.916666666667000000
          39.687500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel32: TQRLabel
        Left = 39
        Top = 836
        Width = 271
        Height = 17
        Size.Values = (
          44.979166666666670000
          103.187500000000000000
          2211.916666666667000000
          717.020833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Resid'#234'ncia fora da compet'#234'ncia territorial do tabelionato'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel33: TQRLabel
        Left = 396
        Top = 836
        Width = 34
        Height = 17
        Size.Values = (
          44.979166666666670000
          1047.750000000000000000
          2211.916666666667000000
          89.958333333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Outros'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape21: TQRShape
        Left = 376
        Top = 836
        Width = 15
        Height = 15
        Size.Values = (
          39.687500000000000000
          994.833333333333300000
          2211.916666666667000000
          39.687500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object lbPGNF: TQRLabel
        Left = 24
        Top = 565
        Width = 657
        Height = 73
        Enabled = False
        Size.Values = (
          193.145833333333300000
          63.500000000000000000
          1494.895833333333000000
          1738.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          'N'#227'o sendo efetuado o pagamento dentro do prazo indicado, o t'#237'tul' +
          'o ser'#225' protestado. Para cancelamento do protesto, o interessado ' +
          'dever'#225' acessar e orientar-se no endere'#231'o eletr'#244'nico da PGNF abai' +
          'xo e fazer o seguinte: A) Efetuar o pagamento do t'#237'tulo por meio' +
          ' de Documento de Arrecada'#231#227'o de Receitas Federais - DARF perante' +
          ' a rede banc'#225'ria; B) Dirigir-se ao cart'#243'rio, ap'#243's 6 dias '#250'teis d' +
          'o pagamento do DARF, para requerer o cancelamento do protesto e ' +
          'efetuar o pagamento dos emolumentos e demais despesas. Para obte' +
          'r outras informa'#231#245'es, acesse o site eletr'#244'nico da PGNF (www.pgnf' +
          '.gov.br).'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel16: TQRLabel
        Left = 482
        Top = 52
        Width = 112
        Height = 15
        Size.Values = (
          39.687500000000000000
          1275.291666666667000000
          137.583333333333300000
          296.333333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'FINS FALIMENTARES:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrCNPJ: TQRLabel
        Left = 507
        Top = 730
        Width = 166
        Height = 16
        Size.Values = (
          42.333333333333330000
          1341.437500000000000000
          1931.458333333333000000
          439.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'lbCNPJ'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
    end
  end
  object RX: TRxMemoryData
    FieldDefs = <>
    Left = 856
    Top = 151
    object RXTitulo: TStringField
      FieldName = 'Titulo'
      Size = 100
    end
    object RXApresentante: TStringField
      FieldName = 'Apresentante'
      Size = 100
    end
    object RXCredor: TStringField
      FieldName = 'Credor'
      Size = 100
    end
    object RXSacador: TStringField
      FieldName = 'Sacador'
      Size = 100
    end
    object RXDevedor: TStringField
      FieldName = 'Devedor'
      Size = 100
    end
    object RXValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object RXProtocolo: TStringField
      FieldName = 'Protocolo'
    end
    object RXIntimacao: TDateField
      FieldName = 'Intimacao'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object RXValorDeposito: TFloatField
      FieldName = 'ValorDeposito'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXCustas: TFloatField
      FieldName = 'Custas'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXEndereco: TStringField
      FieldName = 'Endereco'
      Size = 200
    end
    object RXCPF_CNPJ_Devedor: TStringField
      FieldName = 'CPF_CNPJ_Devedor'
    end
    object RXNumeroTitulo: TStringField
      FieldName = 'NumeroTitulo'
      Size = 30
    end
    object RXEmissao: TDateField
      FieldName = 'Emissao'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object RXDataProtocolo: TDateField
      FieldName = 'DataProtocolo'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object RXTarifa: TFloatField
      FieldName = 'Tarifa'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXObservacao: TStringField
      FieldName = 'Observacao'
      Size = 255
    end
    object RXDataIntimacao: TDateField
      FieldName = 'DataIntimacao'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object RXTipoIntimacao: TStringField
      FieldName = 'TipoIntimacao'
    end
    object RXMotivo: TMemoField
      FieldName = 'Motivo'
      BlobType = ftMemo
    end
    object RXValorCheque: TFloatField
      FieldName = 'ValorCheque'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXPraca: TStringField
      FieldName = 'Praca'
    end
    object RXDistribuicao: TFloatField
      FieldName = 'Distribuicao'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXSaldo: TFloatField
      FieldName = 'Saldo'
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
    end
    object RXCEP: TStringField
      FieldName = 'CEP'
    end
    object RXConvenio: TStringField
      FieldName = 'Convenio'
      Size = 1
    end
    object RXDataPrazo: TDateField
      FieldName = 'DataPrazo'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object RXCPF_CNPJ_Apresentante: TStringField
      FieldName = 'CPF_CNPJ_Apresentante'
      Size = 18
    end
    object RXFins: TStringField
      FieldName = 'Fins'
      Size = 3
    end
    object RXNominal: TStringField
      FieldName = 'Nominal'
      Size = 100
    end
    object RXCodigoApresentante: TStringField
      FieldName = 'CodigoApresentante'
      Size = 10
    end
  end
end
