- Criar a tabela de Serventias Agregadas

CREATE TABLE SERVENTIA_AGREGADA (
    ID_SERVENTIA_AGREGADA  INTEGER NOT NULL,
    CODIGO                 INTEGER,
    DESCRICAO              VARCHAR(100),
    CODIGO_DISTRIBUICAO    INTEGER,
    VALOR_ULTIMO_SALDO     DOUBLE PRECISION,
    DATA_ULTIMO_SALDO      DATE,
    FLG_ATIVA              CHAR(1),
    DATA_INATIVACAO        DATE
);

ALTER TABLE SERVENTIA_AGREGADA ADD CONSTRAINT PK_SERVENTIA_AGREGADA PRIMARY KEY (ID_SERVENTIA_AGREGADA);


- Alterar o tamanho da coluna CAMPO da tabela IDS de VARCHAR(20) para VARCHAR(30)


- Incluir o campo ID_SERVENTIA_AGREGADA na tabela IDS

INSERT INTO IDS (CAMPO, VALOR) VALUES ('ID_SERVENTIA_AGREGADA', 1);


- Alterar a tabela TITULOS para incluir os campos:

PROTOCOLO_CARTORIO (INTEGER)
ARQUIVO_CARTORIO (VARCHAR(20))
RETORNO_CARTORIO (VARCHAR(20))
DT_DEVOLVIDO_CARTORIO (DATE)

ALTER TABLE TITULOS ADD PROTOCOLO_CARTORIO INTEGER;
ALTER TABLE TITULOS ADD ARQUIVO_CARTORIO VARCHAR(20);
ALTER TABLE TITULOS ADD RETORNO_CARTORIO VARCHAR(20);
ALTER TABLE TITULOS ADD DT_DEVOLVIDO_CARTORIO DATE;


- Criar a tabela de Exportações de Arquivos para os Cartorios

CREATE TABLE EXPORTADO (
    ID_EXPORTADO       INTEGER NOT NULL,
    NOME_ARQUIVO_IMP   VARCHAR(20),  --TABELA: IMPORTADOS
    COD_SERV_AGREGADA  INTEGER,  --TABELA: SERVENTIA_AGREGADA
    NOME_ARQUIVO_EXP   VARCHAR(20),
    DATA_EXPORTACAO    DATE,
    QTD_TITULOS        INTEGER
);

ALTER TABLE EXPORTADO ADD CONSTRAINT PK_EXPORTADO PRIMARY KEY (ID_EXPORTADO);


- Incluir o campo ID_EXPORTADO na tabela IDS

INSERT INTO IDS (CAMPO, VALOR) VALUES ('ID_EXPORTADO', 1);


- Incluir o parâmetro para o Caminho de Exportação do Arquivo "D" para as Serventias Agregadas:

INSERT INTO PARAMETROS (ID_PARAMETRO, DESCRICAO, VALOR)
                VALUES ((SELECT (MAX(ID_PARAMETRO) + 1) AS ID_PARAMETRO FROM PARAMETROS),
                        'CAMINHO PARA EXPORTAR OS ARQ. DE REMESSA DAS SERV. AGREGADAS',
                        '\\CRISTINA\TOTAL\PROTESTO\ARQUIVOS\REMESSAS_AGREGADAS\');


- Incluir o parâmetro para o identificar prioridade de distribuição de Remessa:

INSERT INTO PARAMETROS (ID_PARAMETRO, DESCRICAO, VALOR)
                VALUES ((SELECT (MAX(ID_PARAMETRO) + 1) AS ID_PARAMETRO FROM PARAMETROS),
                        'USAR QUANTIDADE COMO PARÂMETRO DE DISTRIBUIÇÃO DE REMESSAS',
		        'S');


- Inclusão do parâmetro para o Caminho de Importação do Arquivo "R" proveniente das Serventias Agregadas:

INSERT INTO PARAMETROS (ID_PARAMETRO, DESCRICAO, VALOR)
                VALUES ((SELECT (MAX(ID_PARAMETRO) + 1) AS ID_PARAMETRO FROM PARAMETROS),
                        'CAMINHO PARA IMPORTAR OS ARQ. DE RETORNO DAS SERV. AGREGADAS',
                        '\\CRISTINA\TOTAL\PROTESTO\ARQUIVOS\RETORNO_AGREGADAS\');


- Alteração do tamanho da coluna ARQUIVO da tabela IMPORTADOS de VARCHAR(15) para VARCHAR(20)