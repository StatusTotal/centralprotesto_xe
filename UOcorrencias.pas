unit UOcorrencias;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, Wwdbigrd, Wwdbgrid, DB;

type
  TFOcorrencias = class(TForm)
    dsOcorrencias: TDataSource;
    Grid: TwwDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
    procedure GridKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FOcorrencias: TFOcorrencias;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFOcorrencias.FormCreate(Sender: TObject);
begin
  dm.Ocorrencias.Close;
  dm.Ocorrencias.Open;
end;

procedure TFOcorrencias.GridDblClick(Sender: TObject);
begin
  dm.vOkGeral:=True;
  Close;
end;

procedure TFOcorrencias.GridKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
  begin
      dm.vOkGeral:=True;
      Close;
  end;
end;

procedure TFOcorrencias.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

end.
