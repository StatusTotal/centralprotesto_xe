{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UParametros.pas
  Descricao:   Tela de configuracoes de Parametros utilizados em todo o sistema
  Author   :
  Date:        15-ago-2016
  Last Update: 22-jun-2017 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UParametros;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, sEdit, sCheckBox, Buttons, sBitBtn,
  sSpeedButton, sDialogs, Mask, sMaskEdit, sCustomComboEdit, sCurrEdit,
  sCurrencyEdit, sMemo, sLabel, ComCtrls, sPageControl, sGroupBox, IniFiles,
  sBevel, ShellAPI, sComboBox, Menus, acImage, Math, StrUtils, DBCtrls,
  sDBLookupComboBox, sRadioButton, DB, DBClient, SimpleDS, sSpinEdit,
  Data.DBXFirebird, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFParametros = class(TForm)
    Svd: TsSaveDialog;
    PC: TsPageControl;
    sTabSheet1: TsTabSheet;
    sTabSheet2: TsTabSheet;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sSpeedButton6: TsSpeedButton;
    ed1: TsEdit;
    ed2: TsEdit;
    ed3: TsEdit;
    ed4: TsEdit;
    ed9: TsEdit;
    ed17: TsEdit;
    sBitBtn2: TsBitBtn;
    btOk: TsBitBtn;
    m24: TsMemo;
    lbChar1: TsLabel;
    sTabSheet3: TsTabSheet;
    ed5: TsEdit;
    ed6: TsEdit;
    ed7: TsEdit;
    ed11: TsEdit;
    ed12: TsEdit;
    ed14: TsEdit;
    ed19: TsCurrencyEdit;
    ed18: TsEdit;
    ed23: TsEdit;
    ed25: TsEdit;
    ck8: TsCheckBox;
    ck13: TsCheckBox;
    ed15: TsEdit;
    sb15: TsSpeedButton;
    ck16: TsCheckBox;
    ed26: TsCurrencyEdit;
    ed27: TsCurrencyEdit;
    ed28: TsCurrencyEdit;
    ck22: TsCheckBox;
    ck29: TsCheckBox;
    ed30: TsEdit;
    m31: TsMemo;
    lbChar2: TsLabel;
    ed32: TsEdit;
    ck33: TsCheckBox;
    ed34: TsMaskEdit;
    ed35: TsCurrencyEdit;
    ed36: TsEdit;
    ed20: TsEdit;
    sSpeedButton7: TsSpeedButton;
    ed38: TsEdit;
    sSpeedButton8: TsSpeedButton;
    ed39: TsEdit;
    sSpeedButton9: TsSpeedButton;
    sSpeedButton10: TsSpeedButton;
    ed41: TsEdit;
    sTabSheet4: TsTabSheet;
    sGroupBox1: TsGroupBox;
    edEspacoRecibo: TsEdit;
    edLocalRecibo: TsEdit;
    btTestarRecibo: TsBitBtn;
    sGroupBox2: TsGroupBox;
    edEspacoEtiqueta: TsEdit;
    edLocalEtiqueta: TsEdit;
    btTestarEtiqueta: TsBitBtn;
    ck42: TsCheckBox;
    ck43: TsCheckBox;
    ck47: TsCheckBox;
    cb50: TsComboBox;
    ck51: TsCheckBox;
    ck52: TsCheckBox;
    ck53: TsCheckBox;
    sTabSheet5: TsTabSheet;
    ed66: TsEdit;
    ed67: TsEdit;
    sLabel3: TsLabel;
    ed57: TsEdit;
    ed58: TsEdit;
    ed59: TsEdit;
    ed60: TsEdit;
    ed61: TsEdit;
    ed62: TsEdit;
    ed63: TsEdit;
    ed56: TsEdit;
    ed68: TsEdit;
    ck69: TsCheckBox;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    ed44: TsEdit;
    ed45: TsEdit;
    ed46: TsEdit;
    ed64: TsEdit;
    ed65: TsEdit;
    ed70: TsEdit;
    ed72: TsEdit;
    cb73: TsComboBox;
    ck76: TsCheckBox;
    ed79: TsEdit;
    sSpeedButton11: TsSpeedButton;
    ed80: TsEdit;
    sSpeedButton12: TsSpeedButton;
    PM: TPopupMenu;
    R1: TMenuItem;
    ck83: TsCheckBox;
    cb81: TsComboBox;
    cb84: TsComboBox;
    ck85: TsCheckBox;
    ed86: TsCurrencyEdit;
    tbAssinaturas: TsTabSheet;
    dsEscreventes: TDataSource;
    sPanel1: TsPanel;
    PNLogado: TsPanel;
    RBLogado: TsRadioButton;
    edLogado: TsEdit;
    PNPersonalizado: TsPanel;
    RBPersonalizado: TsRadioButton;
    edPersonalizado: TsEdit;
    sPanel3: TsPanel;
    ckMatricula: TsCheckBox;
    RBTabeliao: TsRadioButton;
    edTabeliao: TsEdit;
    edTitulo: TsEdit;
    PNDigitalizada: TsPanel;
    Shape1: TShape;
    Imagem: TsImage;
    RBDigitalizada: TsRadioButton;
    edTituloAssinatura: TsEdit;
    sPanel2: TsPanel;
    sCheckBox2: TsCheckBox;
    RBEscrevente: TsRadioButton;
    lkEscreventes: TsDBLookupComboBox;
    sPanel4: TsPanel;
    lbNomeTabeliao: TLabel;
    Shape3: TShape;
    lbTituloTabeliao: TLabel;
    Label1: TLabel;
    sPanel5: TsPanel;
    lbNomeEscrevente: TLabel;
    Shape2: TShape;
    lbQualificacao: TLabel;
    sPanel6: TsPanel;
    lbLogado: TLabel;
    Shape4: TShape;
    lbQualificacaoLogado: TLabel;
    sPanel7: TsPanel;
    Shape5: TShape;
    lbPersonalizado: TLabel;
    sPanel8: TsPanel;
    Shape6: TShape;
    Label2: TLabel;
    sImage1: TsImage;
    ck88: TsCheckBox;
    ed89: TsEdit;
    ck90: TsCheckBox;
    sTabSheet6: TsTabSheet;
    sGroupBox3: TsGroupBox;
    edCabecalho: TsDecimalSpinEdit;
    edRodape: TsDecimalSpinEdit;
    edDireita: TsDecimalSpinEdit;
    edEsquerda: TsDecimalSpinEdit;
    cbfolha: TsComboBox;
    sBitBtn1: TsBitBtn;
    cbTiraCabecalho: TsComboBox;
    edtArqRemServAgreg: TsEdit;
    btnArqServAgregadas: TsSpeedButton;
    chbQtdDistribTit: TsCheckBox;
    edtArqRetServAgreg: TsEdit;
    sSpeedButton13: TsSpeedButton;
    chbGeracaoRemAuto: TsCheckBox;
    edtPathModelosRelatorios: TsEdit;
    btnPathModelossRelatorios: TsSpeedButton;
    qryEscreventes: TFDQuery;
    qryEscreventesID_ESCREVENTE: TIntegerField;
    qryEscreventesNOME: TStringField;
    qryEscreventesCPF: TStringField;
    qryEscreventesQUALIFICACAO: TStringField;
    qryEscreventesLOGIN: TStringField;
    qryEscreventesSENHA: TStringField;
    qryEscreventesAUTORIZADO: TStringField;
    qryEscreventesMATRICULA: TStringField;
    ck102: TsCheckBox;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure sSpeedButton1Click(Sender: TObject);
    procedure sSpeedButton2Click(Sender: TObject);
    procedure sSpeedButton3Click(Sender: TObject);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sSpeedButton5Click(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure ck13Click(Sender: TObject);
    procedure ck22Click(Sender: TObject);
    procedure m24Change(Sender: TObject);
    function Caracteres(Texto: String): Integer;
    procedure m31Change(Sender: TObject);
    procedure sSpeedButton7Click(Sender: TObject);
    procedure sSpeedButton8Click(Sender: TObject);
    procedure sSpeedButton9Click(Sender: TObject);
    procedure ed39Exit(Sender: TObject);
    procedure ed38Exit(Sender: TObject);
    procedure ed20Exit(Sender: TObject);
    procedure ed9Exit(Sender: TObject);
    procedure ed4Exit(Sender: TObject);
    procedure ed3Exit(Sender: TObject);
    procedure ed2Exit(Sender: TObject);
    procedure ed1Exit(Sender: TObject);
    procedure sSpeedButton10Click(Sender: TObject);
    procedure btTestarReciboClick(Sender: TObject);
    procedure ed41Exit(Sender: TObject);
    procedure btTestarEtiquetaClick(Sender: TObject);
    procedure ck69Click(Sender: TObject);
    procedure sSpeedButton11Click(Sender: TObject);
    procedure ed79Exit(Sender: TObject);
    procedure ed80Exit(Sender: TObject);
    procedure sSpeedButton12Click(Sender: TObject);
    procedure R1Click(Sender: TObject);
    procedure cb81DblClick(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure edtArqRemServAgregExit(Sender: TObject);
    procedure btnArqServAgregadasClick(Sender: TObject);
    procedure edtArqRetServAgregExit(Sender: TObject);
    procedure sSpeedButton13Click(Sender: TObject);
    procedure edtPathModelosRelatoriosExit(Sender: TObject);
    procedure btnPathModelossRelatoriosClick(Sender: TObject);
  private
    { Private declarations }
    I: TIniFile;
  public
    { Public declarations }
  end;

var
  FParametros: TFParametros;

implementation

uses UDM, UPrincipal, UPF, UGeral, UGDM;

{$R *.dfm}

procedure TFParametros.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFParametros.FormCreate(Sender: TObject);
begin
  ck69Click(Sender);
  if dm.ServentiaCODIGO.AsInteger=1208 then
  ed11.BoundLabel.Caption:='N� Pedido';
  PC.ActivePageIndex:=0;
  ed1.Text      :=dm.ValorParametro(1);
  ed2.Text      :=dm.ValorParametro(2);
  ed3.Text      :=dm.ValorParametro(3);
  ed4.Text      :=dm.ValorParametro(4);
  ed5.Text      :=dm.ValorParametro(5);
  ed6.Text      :=dm.ValorParametro(6);
  ed7.Text      :=dm.ValorParametro(7);
  ck8.Checked   :=dm.ValorParametro(8)='S';
  ed9.Text      :=dm.ValorParametro(9);
  ed11.Text     :=dm.ValorParametro(11);
  ed12.Text     :=dm.ValorParametro(12);
  ck13.Checked  :=dm.ValorParametro(13)='S';
  ck13Click(Sender);
  ed14.Text     :=dm.ValorParametro(14);
  ed15.Text     :=dm.ValorParametro(15);
  ck16.Checked  :=dm.ValorParametro(16)='S';
  ed17.Text     :=dm.ValorParametro(17);
  ed18.Text     :=dm.ValorParametro(18);
  ed19.Value    :=StrToFloat(dm.ValorParametro(19));
  ed20.Text     :=dm.ValorParametro(20);
  ck22.Checked  :=dm.ValorParametro(22)='S';
  ck22Click(Sender);
  ed23.Text     :=dm.ValorParametro(23);
  m24.Text      :=dm.ValorParametro(24);
  m24Change(Sender);
  ed25.Text     :=dm.ValorParametro(25);
  ed26.Value    :=StrToFloat(dm.ValorParametro(26));
  ed27.Value    :=StrToFloat(dm.ValorParametro(27));
  ed28.Value    :=StrToFloat(dm.ValorParametro(28));
  ck29.Checked  :=dm.ValorParametro(29)='S';
  ed30.Text     :=dm.ValorParametro(30);
  m31.Text      :=dm.ValorParametro(31);
  m31Change(Sender);
  ed32.Text     :=dm.ValorParametro(32);
  ck33.Checked  :=dm.ValorParametro(33)='S';
  ed34.Text     :=dm.ValorParametro(34);
  ed35.Text     :=dm.ValorParametro(35);
  ed36.Text     :=dm.ValorParametro(36);
  ed38.Text     :=dm.ValorParametro(38);
  ed39.Text     :=dm.ValorParametro(39);
  ed41.Text     :=dm.ValorParametro(41);
  ck42.Checked  :=dm.ValorParametro(42)='S';
  ck43.Checked  :=dm.ValorParametro(43)='S';
  ed44.Text     :=dm.ValorParametro(44);
  ed45.Text     :=dm.ValorParametro(45);
  ed46.Text     :=dm.ValorParametro(46);
  ck47.Checked  :=dm.ValorParametro(47)='S';
  cb50.ItemIndex:=cb50.Items.IndexOf(dm.ValorParametro(50));
  ck51.Checked  :=dm.ValorParametro(51)='S';
  ck52.Checked  :=dm.ValorParametro(52)='S';
  ck53.Checked  :=dm.ValorParametro(53)='S';
  ed56.Text     :=dm.ValorParametro(56);
  ed57.Text     :=dm.ValorParametro(57);
  ed58.Text     :=dm.ValorParametro(58);
  ed59.Text     :=dm.ValorParametro(59);
  ed60.Text     :=dm.ValorParametro(60);
  ed61.Text     :=dm.ValorParametro(61);
  ed62.Text     :=dm.ValorParametro(62);
  ed63.Text     :=dm.ValorParametro(63);
  ed64.Text     :=dm.ValorParametro(64);
  ed65.Text     :=dm.ValorParametro(65);
  ed66.Text     :=dm.ValorParametro(66);
  ed67.Text     :=dm.ValorParametro(67);
  ed68.Text     :=dm.ValorParametro(68);
  ck69.Checked  :=dm.ValorParametro(69)='S';
  ed70.Text     :=dm.ValorParametro(70);
  ed72.Text     :=dm.ValorParametro(72);
  cb73.ItemIndex:=cb73.IndexOf(dm.ValorParametro(73));
  ck76.Checked  :=dm.ValorParametro(76)='S';
  ed79.Text     :=dm.ValorParametro(79);
  ed80.Text     :=dm.ValorParametro(80);
  cb81.ItemIndex:=cb81.Items.IndexOf(dm.ValorParametro(81));
  ck83.Checked  :=dm.ValorParametro(83)='S';
  cb84.ItemIndex:=IfThen(dm.ValorParametro(84)='S',0,1);
  ck85.Checked  :=GR.iif(dm.ValorParametro(85)='S',True,False);
  ed86.Value    :=StrToInt(dm.ValorParametro(86));
  ck88.Checked  :=GR.iif(dm.ValorParametro(88)='S',True,False);
  ed89.Text     :=dm.ValorParametro(89);
  ck90.Checked  :=GR.iif(dm.ValorParametro(90)='S',True,False);
  ck102.Checked  :=GR.iif(dm.ValorParametro(102)='S',True,False);
  {PEDRO - 28/03/2016}
  edCabecalho.Text          := dm.ValorParametro(91);
  edRodape.Text             := dm.ValorParametro(92);
  edEsquerda.Text           := dm.ValorParametro(93);
  edDireita.Text            := dm.ValorParametro(94);
  cbfolha.ItemIndex         := StrToInt(dm.ValorParametro(95));
  cbTiraCabecalho.ItemIndex := StrToInt(dm.ValorParametro(96));
  {FIM}
  edtArqRemServAgreg.Text       := dm.ValorParametro(97);
  chbQtdDistribTit.Checked      := dm.ValorParametro(98) = 'S';
  edtArqRetServAgreg.Text       := dm.ValorParametro(99);
  chbGeracaoRemAuto.Checked     := dm.ValorParametro(100) = 'S';
  edtPathModelosRelatorios.Text := dm.ValorParametro(101);

  I:=TIniFile.Create(ExtractFilePath(Application.ExeName)+'Par.ini');
  edLocalRecibo.Text    :=I.ReadString('IMPRESSORAS','RECIBO','');
  edLocalEtiqueta.Text  :=I.ReadString('IMPRESSORAS','ETIQUETA','');
  edEspacoRecibo.Text   :=I.ReadString('ESPACOFINAL','RECIBO','');
  edEspacoEtiqueta.Text :=I.ReadString('ESPACOFINAL','ETIQUETA','');
  I.Free;
end;

procedure TFParametros.sSpeedButton1Click(Sender: TObject);
begin
  Svd.FileName  :='LOCAL PARA SALVAR O ARQUIVO XML';
  Svd.InitialDir:=ed1.Text;
  if Svd.Execute then ed1.Text:=ExtractFilePath(Svd.FileName);
end;

procedure TFParametros.sSpeedButton2Click(Sender: TObject);
begin
  Svd.FileName  :='IMPORTAR FEBRABAN';
  Svd.InitialDir:=ed2.Text;
  if Svd.Execute then ed2.Text:=ExtractFilePath(Svd.FileName);
end;

procedure TFParametros.sSpeedButton3Click(Sender: TObject);
begin
  Svd.FileName  :='ARQUIVOS DE CONFIRMA��O';
  Svd.InitialDir:=ed3.Text;
  if Svd.Execute then ed3.Text:=ExtractFilePath(Svd.FileName);
end;

procedure TFParametros.sSpeedButton4Click(Sender: TObject);
begin
  Svd.FileName  :='ARQUIVOS DE RETORNO';
  Svd.InitialDir:=ed4.Text;
  if Svd.Execute then ed4.Text:=ExtractFilePath(Svd.FileName);
end;

procedure TFParametros.sSpeedButton5Click(Sender: TObject);
begin
  Svd.FileName  :='LOCAL DAS IMAGENS';
  Svd.InitialDir:=ed9.Text;
  if Svd.Execute then ed9.Text:=ExtractFilePath(Svd.FileName);
end;

procedure TFParametros.btnPathModelossRelatoriosClick(Sender: TObject);
begin
  Svd.FileName   := 'ARQUIVOS EM PDF DE RELAT�RIOS';
  Svd.InitialDir := edtPathModelosRelatorios.Text;

  if Svd.Execute then
    edtPathModelosRelatorios.Text := ExtractFilePath(Svd.FileName);
end;

procedure TFParametros.btOkClick(Sender: TObject);
begin
  dm.AtualizarParametro(1,ed1.Text);
  dm.AtualizarParametro(2,ed2.Text);
  dm.AtualizarParametro(3,ed3.Text);
  dm.AtualizarParametro(4,ed4.Text);
  dm.AtualizarParametro(5,ed5.Text);
  dm.AtualizarParametro(6,ed6.Text);
  dm.AtualizarParametro(7,ed7.Text);

  if ck8.Checked then
    dm.AtualizarParametro(8,'S')
      else dm.AtualizarParametro(8,'N');

  dm.AtualizarParametro(9,ed9.Text);
  dm.AtualizarParametro(11,ed11.Text);
  dm.AtualizarParametro(12,ed12.Text);

  if ck13.Checked then
    dm.AtualizarParametro(13,'S')
      else dm.AtualizarParametro(13,'N');

  dm.AtualizarParametro(14,ed14.Text);
  dm.AtualizarParametro(15,ed15.Text);

  if ck16.Checked then
    dm.AtualizarParametro(16,'S')
      else dm.AtualizarParametro(16,'N');

  dm.AtualizarParametro(17,ed17.Text);
  dm.AtualizarParametro(18,ed18.Text);
  dm.AtualizarParametro(19,ed19.Text);
  dm.AtualizarParametro(20,ed20.Text);

  if ck22.Checked then
    dm.AtualizarParametro(22,'S')
      else dm.AtualizarParametro(22,'N');

  dm.AtualizarParametro(23,ed23.Text);
  dm.AtualizarParametro(24,m24.Text);
  dm.AtualizarParametro(25,ed25.Text);
  dm.AtualizarParametro(26,ed26.Text);
  dm.AtualizarParametro(27,ed27.Text);
  dm.AtualizarParametro(28,ed28.Text);

  if ck29.Checked then
    dm.AtualizarParametro(29,'S')
      else dm.AtualizarParametro(29,'N');

  dm.AtualizarParametro(30,ed30.Text);
  dm.AtualizarParametro(31,m31.Text);
  dm.AtualizarParametro(32,ed32.Text);

  if ck33.Checked then
    dm.AtualizarParametro(33,'S')
      else dm.AtualizarParametro(33,'N');

  dm.AtualizarParametro(34,ed34.Text);
  dm.AtualizarParametro(35,ed35.Text);
  dm.AtualizarParametro(36,ed36.Text);
  dm.AtualizarParametro(38,ed38.Text);
  dm.AtualizarParametro(39,ed39.Text);
  dm.AtualizarParametro(41,ed41.Text);

  if ck42.Checked then
    dm.AtualizarParametro(42,'S')
      else dm.AtualizarParametro(42,'N');
      
  if ck43.Checked then
    dm.AtualizarParametro(43,'S')
      else dm.AtualizarParametro(43,'N');

  dm.AtualizarParametro(44,ed44.Text);
  dm.AtualizarParametro(45,ed45.Text);
  dm.AtualizarParametro(46,ed46.Text);

  if ck47.Checked then
    dm.AtualizarParametro(47,'S')
      else dm.AtualizarParametro(47,'N');

  dm.AtualizarParametro(50,cb50.Text);

  if ck51.Checked then
    dm.AtualizarParametro(51,'S')
      else dm.AtualizarParametro(51,'N');

  if ck52.Checked then
    dm.AtualizarParametro(52,'S')
      else dm.AtualizarParametro(52,'N');

  if ck53.Checked then
    dm.AtualizarParametro(53,'S')
      else dm.AtualizarParametro(53,'N');

  dm.AtualizarParametro(56,ed56.Text);
  dm.AtualizarParametro(57,ed57.Text);
  dm.AtualizarParametro(58,ed58.Text);
  dm.AtualizarParametro(59,ed59.Text);
  dm.AtualizarParametro(60,ed60.Text);
  dm.AtualizarParametro(61,ed61.Text);
  dm.AtualizarParametro(62,ed62.Text);
  dm.AtualizarParametro(63,ed63.Text);
  dm.AtualizarParametro(64,ed64.Text);
  dm.AtualizarParametro(65,ed65.Text);
  dm.AtualizarParametro(66,ed66.Text);
  dm.AtualizarParametro(67,ed67.Text);
  dm.AtualizarParametro(68,ed68.Text);

  if ck69.Checked then
    dm.AtualizarParametro(69,'S')
      else dm.AtualizarParametro(69,'N');

  dm.AtualizarParametro(70,ed70.Text);
  dm.AtualizarParametro(72,ed72.Text);
  dm.AtualizarParametro(73,cb73.Text);

  if ck76.Checked then
    dm.AtualizarParametro(76,'S')
      else dm.AtualizarParametro(76,'N');

  dm.AtualizarParametro(79,ed79.Text);
  dm.AtualizarParametro(80,ed80.Text);
  dm.AtualizarParametro(81,cb81.Text);
  dm.AtualizarParametro(83,IfThen(ck83.Checked,'S','N'));
  dm.AtualizarParametro(84,IfThen(cb84.ItemIndex=0,'S','N'));
  dm.AtualizarParametro(85,IfThen(ck85.Checked,'S','N'));
  dm.AtualizarParametro(86,ed86.Text);
  dm.AtualizarParametro(88,IfThen(ck88.Checked,'S','N'));
  dm.AtualizarParametro(89,ed89.Text);
  dm.AtualizarParametro(90,IfThen(ck90.Checked,'S','N'));
  dm.AtualizarParametro(102,IfThen(ck102.Checked,'S','N'));
  {PEDRO - 28/03/2016}
  dm.AtualizarParametro(91,edCabecalho.Text);
  dm.AtualizarParametro(92,edRodape.Text);
  dm.AtualizarParametro(93,edEsquerda.Text);
  dm.AtualizarParametro(94,edDireita.Text);
  dm.AtualizarParametro(95,IntToStr(cbfolha.ItemIndex));
  dm.AtualizarParametro(96,IntToStr(cbTiraCabecalho.ItemIndex));

  dm.vFlTop    := dm.ValorParametro(91);
  dm.vFlBottom := dm.ValorParametro(92);
  dm.vFlLeft   := dm.ValorParametro(93);
  dm.vFlRight  := dm.ValorParametro(94);

  if dm.ValorParametro(95) = '0' then
     dm.vFlFolha  := 'A4'
     else dm.vFlFolha  := 'Legal';

  if dm.ValorParametro(96) = '0' then
     dm.vFlTiracabecalho := 'S'
     else dm.vFlTiracabecalho := 'N';
  {FIM}

  dm.AtualizarParametro(97, edtArqRemServAgreg.Text);

  if chbQtdDistribTit.Checked then
    dm.AtualizarParametro(98,'S')
  else
    dm.AtualizarParametro(98,'N');

  dm.AtualizarParametro(99, edtArqRetServAgreg.Text); 

  if chbGeracaoRemAuto.Checked then
    dm.AtualizarParametro(100,'S')
  else
    dm.AtualizarParametro(100,'N');

  dm.AtualizarParametro(101, edtPathModelosRelatorios.Text);
  
  I:=TIniFile.Create(ExtractFilePath(Application.ExeName)+'Par.ini');
  I.WriteString('IMPRESSORAS','RECIBO',edLocalRecibo.Text);
  I.WriteString('IMPRESSORAS','ETIQUETA',edLocalEtiqueta.Text);
  I.WriteString('ESPACOFINAL','RECIBO',edEspacoRecibo.Text);
  I.WriteString('ESPACOFINAL','ETIQUETA',edEspacoEtiqueta.Text);
  I.Free;

  Close;
end;

procedure TFParametros.sBitBtn2Click(Sender: TObject);
begin
  Close;
end;

procedure TFParametros.ck13Click(Sender: TObject);
begin
  ed15.Visible:=ck13.Checked;
  sb15.Visible:=ck13.Checked;
end;

procedure TFParametros.ck22Click(Sender: TObject);
begin
  ed18.Enabled:=ck22.Checked;
end;

function TFParametros.Caracteres(Texto: String): Integer;
begin
    Result:=100-Length(Texto);
end;

procedure TFParametros.m24Change(Sender: TObject);
begin
  lbChar1.Caption:=IntToStr(Caracteres(m24.Text))+' restantes';
end;

procedure TFParametros.m31Change(Sender: TObject);
begin
  lbChar2.Caption:=IntToStr(Caracteres(m31.Text))+' restantes';
end;

procedure TFParametros.sSpeedButton7Click(Sender: TObject);
begin
  Svd.FileName  :='LOCAL PARA SALVAR A EXPORTA��O PARA O SERASA';
  Svd.InitialDir:=ed20.Text;
  if Svd.Execute then ed20.Text:=ExtractFilePath(Svd.FileName);
end;

procedure TFParametros.sSpeedButton8Click(Sender: TObject);
begin
  Svd.FileName  :='LOCAL PARA SALVAR O ARQUIVO';
  Svd.InitialDir:=ed38.Text;
  if Svd.Execute then ed38.Text:=ExtractFilePath(Svd.FileName);
end;

procedure TFParametros.sSpeedButton9Click(Sender: TObject);
begin
  Svd.FileName  :='LOCAL PARA SALVAR O ARQUIVO';
  Svd.InitialDir:=ed39.Text;
  if Svd.Execute then ed39.Text:=ExtractFilePath(Svd.FileName);
end;

procedure TFParametros.ed39Exit(Sender: TObject);
begin
  if ed39.Text[Length(ed39.Text)]<>'\' then ed39.Text:=ed39.Text+'\';
end;

procedure TFParametros.ed38Exit(Sender: TObject);
begin
  if ed38.Text[Length(ed39.Text)]<>'\' then ed38.Text:=ed38.Text+'\';
end;

procedure TFParametros.ed20Exit(Sender: TObject);
begin
  if ed20.Text[Length(ed20.Text)]<>'\' then ed20.Text:=ed20.Text+'\';
end;

procedure TFParametros.ed9Exit(Sender: TObject);
begin
  if ed9.Text[Length(ed9.Text)]<>'\' then ed9.Text:=ed9.Text+'\';
end;

procedure TFParametros.ed4Exit(Sender: TObject);
begin
  if ed4.Text[Length(ed4.Text)]<>'\' then ed4.Text:=ed4.Text+'\';
end;

procedure TFParametros.ed3Exit(Sender: TObject);
begin
  if ed3.Text[Length(ed3.Text)]<>'\' then ed3.Text:=ed3.Text+'\';
end;

procedure TFParametros.ed2Exit(Sender: TObject);
begin
  if ed2.Text[Length(ed2.Text)]<>'\' then ed2.Text:=ed2.Text+'\';
end;

procedure TFParametros.ed1Exit(Sender: TObject);
begin
  if ed1.Text[Length(ed1.Text)]<>'\' then ed1.Text:=ed1.Text+'\';
end;

procedure TFParametros.sSpeedButton10Click(Sender: TObject);
begin
  Svd.FileName  :='LOCAL PARA SALVAR A EXPORTA��O PARA O BANCO UNIFICADO';
  Svd.InitialDir:=ed41.Text;
  if Svd.Execute then ed41.Text:=ExtractFilePath(Svd.FileName);
end;

procedure TFParametros.btTestarReciboClick(Sender: TObject);
var
  F: TextFile;
  P: String;
  X: Integer;
begin
  try
    PF.Aguarde(True);
    I:=TIniFile.Create(ExtractFilePath(Application.ExeName)+'Par.ini');
    P:=edLocalRecibo.Text;
    AssignFile(F,P);
    Rewrite(F);
    Writeln(F,'***************************************');
    Writeln(F,'            TOTAL SISTEMAS');
    Writeln(F,'   PROTESTO DE TITULOS E DOCUMENTOS');
    Writeln(F,PF.RemoverAcento(dm.vVersao));
    Writeln(F,'  IMPRESSORA CONFIGURADA COM SUCESSO!');
    Writeln(F,'***************************************');

    for X:= 1 to StrToInt(edEspacoRecibo.Text) do
    Writeln(F,' ');
    CloseFile(F);

    PF.Aguarde(False);
    GR.Aviso('Impressora configurada com sucesso!');
  except
    on E: Exception do
    begin
        PF.Aguarde(False);
        GR.Aviso('Impressora n�o configurada!'+#13#13+'Erro: '+E.Message);
    end;
  end;
end;

procedure TFParametros.ed41Exit(Sender: TObject);
begin
  if ed41.Text[Length(ed41.Text)]<>'\' then ed41.Text:=ed41.Text+'\';
end;

procedure TFParametros.btTestarEtiquetaClick(Sender: TObject);
var
  F: TextFile;
  P: String;
begin
  try
    PF.Aguarde(True);
    I:=TIniFile.Create(ExtractFilePath(Application.ExeName)+'Par.ini');
    P:=edLocalEtiqueta.Text;
    AssignFile(F,P);
    Rewrite(F);
    Writeln(F,'***************************************');
    Writeln(F,'            TOTAL SISTEMAS');
    Writeln(F,'   PROTESTO DE TITULOS E DOCUMENTOS');
    Writeln(F,PF.RemoverAcento(dm.vVersao));
    Writeln(F,'  IMPRESSORA CONFIGURADA COM SUCESSO!');
    Writeln(F,'***************************************');
    Writeln(F,'');
    Writeln(F,'');
    Writeln(F,'');
    Writeln(F,'');
    Writeln(F,'');
    CloseFile(F);
    PF.Aguarde(False);
    GR.Aviso('Impressora configurada com sucesso!');
  except
    on E: Exception do
    begin
        PF.Aguarde(False);
        GR.Aviso('Impressora n�o configurada!'+#13#13+'Erro: '+E.Message);
    end;
  end;
end;

procedure TFParametros.ck69Click(Sender: TObject);
begin
  PC.Pages[4].TabVisible:=ck69.Checked;
end;

procedure TFParametros.sSpeedButton11Click(Sender: TObject);
begin
  Svd.FileName  :='LOCAL PARA SALVAR A EXPORTA��O PARA O BOAVISTA';
  Svd.InitialDir:=ed79.Text;
  if Svd.Execute then ed79.Text:=ExtractFilePath(Svd.FileName);
end;

procedure TFParametros.ed79Exit(Sender: TObject);
begin
  if ed79.Text<>'' then
  if ed79.Text[Length(ed79.Text)]<>'\' then ed79.Text:=ed79.Text+'\';
end;

procedure TFParametros.ed80Exit(Sender: TObject);
begin
  if ed80.Text[Length(ed80.Text)]<>'\' then ed80.Text:=ed80.Text+'\';
end;

procedure TFParametros.sSpeedButton12Click(Sender: TObject);
begin
  Svd.FileName  :='LOCAL PARA SALVAR A EXPORTA��O PARA O LIVRO ADICIONAL';
  Svd.InitialDir:=ed80.Text;
  if Svd.Execute then ed80.Text:=ExtractFilePath(Svd.FileName);
end;

procedure TFParametros.R1Click(Sender: TObject);
begin
  if GR.PasswordInputBox('SEGURAN�A','SENHA')='PADRAO' then
  begin
      ed1.Text:='\\'+GR.ComputerName+'\TOTAL\CENTRAL PROTESTO\ARQUIVOS\XML\';
      ed2.Text:='\\'+GR.ComputerName+'\TOTAL\CENTRAL PROTESTO\ARQUIVOS\REMESSAS\';
      edtArqRemServAgreg.Text := '\\' + GR.ComputerName + '\TOTAL\CENTRAL PROTESTO\ARQUIVOS\REMESSAS_AGREGADAS\';
      ed3.Text:='\\'+GR.ComputerName+'\TOTAL\CENTRAL PROTESTO\ARQUIVOS\CONFIRMA��O\';
      edtArqRetServAgreg.Text := '\\' + GR.ComputerName + '\TOTAL\CENTRAL PROTESTO\ARQUIVOS\RETORNO_AGREGADAS\';
      ed4.Text:='\\'+GR.ComputerName+'\TOTAL\CENTRAL PROTESTO\ARQUIVOS\RETORNO\';
      ed9.Text:='\\'+GR.ComputerName+'\TOTAL\CENTRAL PROTESTO\ARQUIVOS\IMAGENS\';
      ed20.Text:='\\'+GR.ComputerName+'\TOTAL\CENTRAL PROTESTO\ARQUIVOS\SERASA\';
      ed79.Text:='\\'+GR.ComputerName+'\TOTAL\CENTRAL PROTESTO\ARQUIVOS\BOAVISTA\';
      ed41.Text:='\\'+GR.ComputerName+'\TOTAL\CENTRAL PROTESTO\ARQUIVOS\ONLINE\';
      ed80.Text:='\\'+GR.ComputerName+'\TOTAL\CENTRAL PROTESTO\ARQUIVOS\LAE\';
      edtPathModelosRelatorios.Text := '\\' + GR.ComputerName + '\TOTAL\CENTRAL PROTESTO\ARQUIVOS\RELATORIOS\';
  end;
end;

procedure TFParametros.cb81DblClick(Sender: TObject);
begin
  if GR.PasswordInputBox('SEGURAN�A','SENHA')=FormatDateTime('HHMM',Now) then
  cb81.ReadOnly:=False;
end;

procedure TFParametros.sBitBtn1Click(Sender: TObject);
begin

  if gr.Pergunta('Deseja substituir os valores atuais da folha pelos valores padr�o') then
  begin
     edCabecalho.Value := 15.00;
     edRodape.Value    := 10.00;
     edEsquerda.Value  := 20.00;
     edDireita.Value   := 15.00;
  end;

end;

procedure TFParametros.edtArqRemServAgregExit(Sender: TObject);
begin
  if edtArqRemServAgreg.Text[Length(edtArqRemServAgreg.Text)] <> '\' then
    edtArqRemServAgreg.Text := edtArqRemServAgreg.Text + '\';
end;

procedure TFParametros.btnArqServAgregadasClick(Sender: TObject);
begin
  Svd.FileName   :='ARQUIVOS DE REMESSAS DE SERVENTIAS AGREGADAS';
  Svd.InitialDir := edtArqRemServAgreg.Text;
  
  if Svd.Execute then
    edtArqRemServAgreg.Text := ExtractFilePath(Svd.FileName);
end;

procedure TFParametros.edtArqRetServAgregExit(Sender: TObject);
begin
  if edtArqRetServAgreg.Text[Length(edtArqRetServAgreg.Text)] <> '\' then
    edtArqRetServAgreg.Text := edtArqRetServAgreg.Text + '\';
end;

procedure TFParametros.edtPathModelosRelatoriosExit(Sender: TObject);
begin
  if edtPathModelosRelatorios.Text[Length(edtPathModelosRelatorios.Text)] <> '\' then
    edtPathModelosRelatorios.Text := edtPathModelosRelatorios.Text + '\';
end;

procedure TFParametros.sSpeedButton13Click(Sender: TObject);
begin
  Svd.FileName   :='ARQUIVOS DE RETORNO DE SERVENTIAS AGREGADAS';
  Svd.InitialDir := edtArqRetServAgreg.Text;
  
  if Svd.Execute then
    edtArqRetServAgreg.Text := ExtractFilePath(Svd.FileName);
end;

end.
