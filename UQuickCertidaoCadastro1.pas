unit UQuickCertidaoCadastro1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, DB, RxMemDS, StdCtrls, ComCtrls;

type
  TFQuickCertidaoCadastro1 = class(TForm)
    RX: TRxMemoryData;
    RXTitulo: TStringField;
    RXApresentante: TStringField;
    RXDevedor: TStringField;
    RXValor: TFloatField;
    RXVencimento: TDateField;
    RXProtocolo: TStringField;
    RXDt_Titulo: TDateField;
    RXEspecie: TStringField;
    Certidao1: TQuickRep;
    Cabecalho: TQRBand;
    txNomeServentia: TQRDBText;
    txEndereco: TQRDBText;
    lbCertidao: TQRLabel;
    lbTitular: TQRLabel;
    lbTexto1: TQRLabel;
    RXSacador: TStringField;
    RXCedente: TStringField;
    RXDt_Protesto: TDateField;
    RXEndosso: TStringField;
    RXLivro: TStringField;
    RXFolha: TStringField;
    Summary: TQRBand;
    lbEscrevente: TQRLabel;
    QRLabel17: TQRLabel;
    lbCidade: TQRLabel;
    lbAssinatura: TQRLabel;
    QRShape5: TQRShape;
    M1: TQRMemo;
    QRBand1: TQRBand;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    RE: TRichEdit;
    QRE: TQRRichText;
    sysPagina: TQRSysData;
    QRDBText17: TQRDBText;
    lbAviso: TQRLabel;
    lbRecibo: TQRLabel;
    qrTabeliao: TQRLabel;
    qrAssinatura: TQRShape;
    QRLabel1: TQRLabel;
    lbCGJ1: TQRLabel;
    lbCGJ2: TQRLabel;
    lbCGJ3: TQRLabel;
    lbSeloEletronico: TQRLabel;
    lbCGJ4: TQRLabel;
    lbCGJ5: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure DetalheAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure CabecalhoBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure Certidao1EndPage(Sender: TCustomQuickRep);
  private
    { Private declarations }
  public
    { Public declarations }
    Registro: Integer;
  end;

var
  FQuickCertidaoCadastro1: TFQuickCertidaoCadastro1;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFQuickCertidaoCadastro1.FormCreate(Sender: TObject);
var
  Cobranca: String;
begin
  {PEDRO - 29/03/2016}
  if GR.Pergunta('DESEJA IMPRIMIR NA FOLHA DE SEGURAN�A') then
  begin
      if dm.vFlTiracabecalho = 'S' then
      begin
        txNomeServentia.Enabled := False;
        txEndereco.Enabled      := False;
        QRDBText14.Enabled      := False;
        QRDBText15.Enabled      := False;
        QRDBText16.Enabled      := False;
        QRDBText17.Enabled      := False;
      end
      else
      begin
        txNomeServentia.Enabled := True;
        txEndereco.Enabled      := True;
        QRDBText14.Enabled      := True;
        QRDBText15.Enabled      := True;
        QRDBText16.Enabled      := True;
        QRDBText17.Enabled      := True;
      end;

      Certidao1.Frame.DrawTop   := False;
      Certidao1.Frame.DrawBottom:= False;
      Certidao1.Frame.DrawLeft  := False;
      Certidao1.Frame.DrawRight := False;

      Certidao1.Page.BottomMargin := StrToFloat(dm.vFlBottom);
      Certidao1.Page.TopMargin    := StrToFloat(dm.vFlTop);
      Certidao1.Page.LeftMargin   := StrToFloat(dm.vFlLeft);
      Certidao1.Page.RightMargin  := StrToFloat(dm.vFlRight);

      if dm.vFlFolha ='A4' then
      begin
        Certidao1.Page.Width := 210.0;
        Certidao1.Page.Length:= 297.0;
      end;

      if dm.vFlFolha ='Legal' then
      begin
        Certidao1.Page.Width := 215.9;
        Certidao1.Page.Length:= 355.6;
      end;
      {FIM}
  end;

  if dm.ServentiaCODIGO.AsInteger=2353 then
  begin
      qrAssinatura.Enabled:=True;
      qrTabeliao.Enabled  :=True;
      qrTabeliao.Caption  :=dm.ServentiaTABELIAO.AsString;  
  end;

  lbTitular.Caption:=dm.ServentiaTABELIAO.AsString;
  lbTexto1.Caption :=StringReplace(lbTexto1.Caption,'[T�tulo]',dm.ValorParametro(7),[rfReplaceAll, rfIgnoreCase]);
  lbTexto1.Caption :=StringReplace(lbTexto1.Caption,'[Cart�rio]',dm.ServentiaDESCRICAO.AsString,[rfReplaceAll, rfIgnoreCase]);
  if (dm.vDestino='S') or (dm.vDestino='s') then
    dm.vDestino:='SERASA S/A'
      else dm.vDestino:='BOA VISTA SERVI�OS S.A.';

  RE.Lines.Add('CERTIFICA a requerimento formulado de '+dm.vDestino+', que revendo em seu poder e Cart�rio, no per�odo de '+
               dm.vDataInicial+' a '+dm.vDataFinal+', os livros de REGISTROS DE PROTESTOS DE T�TULOS e outros DOCUMENTOS DE D�VIDA,'+
               ' deles, depois da busca procedida, em conformidade com a Lei 9492/97, Art 29; que a rela��o de protestos e'+
               ' cancelamentos enviados via ARQUIVO MAGN�TICO contendo ('+dm.vMarcados{IntToStr(dm.vDescontarMarcados)}+') arquivos no total, dos quais ('+
               {IntToStr(dm.vDescontarProtestados)}dm.vProtestados+')'+
               ' registros s�o referentes aos Protestos tirados e ('+{IntToStr(dm.vDescontarCancelados)}dm.vCancelados+
               ') registros s�o referentes aos Cancelamentos de Protesto.'+
               ' S�o de nossa inteira responsabilidade.###################################################################################'+
               '##########################################################################################################################'+
               '##########################################################################################################################'+
               '##########################################################################################.');

  PF.JustificarRichEdit(RE,True);

  lbRecibo.Caption          :='Recibo n�: '+dm.vRecibo;
  lbSeloEletronico.Caption  :=GR.SeloFormatado(dm.CertidoesSELO.AsString,dm.CertidoesALEATORIO.AsString);
  lbEscrevente.Caption      :='Eu,__________________, '+dm.vNomeCompleto+', '+PF.QualificacaoEscrevente(dm.vCPF)+', efetuei a busca.';
  lbCidade.Caption          :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',dm.CertidoesDT_CERTIDAO.AsDateTime);
  lbAssinatura.Caption      :=dm.vAssinatura;
  lbAviso.Caption           :='Esta Certid�o � gerada de forma eletr�nica, qualquer forma de rasura ou complementa��o a tornar� como inv�lida, '+
                              'ou como princ�pio de fraude. (Art. 559 - Resolu��o 01/2000- CGJERJ). Esta Certid�o � composta por ('+dm.vFolhas+') p�gina(s) '+
                              '- V�lida somente se aposta de selo de fiscaliza��o.';

  dm.RxCustas.First;
  while not dm.RxCustas.Eof do
  begin
      M1.Lines.Add(Format('%12s %1s %7.2f',[dm.RxCustasTABELA.AsString+'.'+dm.RxCustasITEM.AsString+'.'+dm.RxCustasSUBITEM.AsString+'   '+
                                            dm.RxCustasDESCRICAO.AsString+':',' R$ ',dm.RxCustasTOTAL.AsFloat]));
      dm.RxCustas.Next;
  end;

  M1.Lines.Add('');
  M1.Lines.Add(Format('%12s %1s %7.2f',['Emolumentos:',' R$ ',dm.CertidoesEMOLUMENTOS.AsFloat]));
  M1.Lines.Add(Format('%12s %1s %7.2f',['       Fetj:',' R$ ',dm.CertidoesFETJ.AsFloat]));
  M1.Lines.Add(Format('%12s %1s %7.2f',['   Fundperj:',' R$ ',dm.CertidoesFUNDPERJ.AsFloat]));
  M1.Lines.Add(Format('%12s %1s %7.2f',['    Funperj:',' R$ ',dm.CertidoesFUNPERJ.AsFloat]));
  M1.Lines.Add(Format('%12s %1s %7.2f',['   Funarpen:',' R$ ',dm.CertidoesFUNARPEN.AsFloat]));
  M1.Lines.Add(Format('%12s %1s %7.2f',['      Pmcmv:',' R$ ',dm.CertidoesPMCMV.AsFloat]));
  M1.Lines.Add(Format('%12s %1s %7.2f',['        Iss:',' R$ ',dm.CertidoesISS.AsFloat]));
  M1.Lines.Add(Format('%12s %1s %7.2f',['      Total:',' R$ ',dm.CertidoesTOTAL.AsFloat]));

  if dm.CertidoesCOBRANCA.AsString='CC' then Cobranca:='';
  if dm.CertidoesCOBRANCA.AsString='SC' then Cobranca:='SEM COBRAN�A';
  if dm.CertidoesCOBRANCA.AsString='JG' then Cobranca:='JUSTI�A GRATUITA';
  if dm.CertidoesCOBRANCA.AsString='NH' then Cobranca:='NIHIL';

  M1.Lines.Add('');
  M1.Lines.Add(Cobranca);
  Certidao1.Preview;
end;

procedure TFQuickCertidaoCadastro1.DetalheAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  if dm.ValorParametro(22)='N' then Exit;
  Inc(Registro);
  if Registro=StrToInt(dm.ValorParametro(18)) then
  begin
      Certidao1.NewPage;
      Registro:=0;
  end;
end;

procedure TFQuickCertidaoCadastro1.CabecalhoBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Registro:=0;
end;

procedure TFQuickCertidaoCadastro1.Certidao1EndPage(
  Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

end.
