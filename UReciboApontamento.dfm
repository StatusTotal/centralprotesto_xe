object FReciboApontamento: TFReciboApontamento
  Left = 362
  Top = 231
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Recibo de Apontamentos'
  ClientHeight = 150
  ClientWidth = 359
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 15
  object btVisualizar: TsBitBtn
    Left = 150
    Top = 104
    Width = 91
    Height = 28
    Cursor = crHandPoint
    Caption = 'Visualizar'
    TabOrder = 1
    OnClick = btVisualizarClick
    ImageIndex = 4
    Images = dm.Imagens
    SkinData.SkinSection = 'BUTTON'
  end
  object btSair: TsBitBtn
    Left = 248
    Top = 104
    Width = 75
    Height = 28
    Cursor = crHandPoint
    Caption = 'Sair'
    TabOrder = 2
    OnClick = btSairClick
    ImageIndex = 7
    Images = dm.Imagens
    SkinData.SkinSection = 'BUTTON'
  end
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 359
    Height = 89
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object edInicio: TsDateEdit
      Left = 64
      Top = 36
      Width = 98
      Height = 24
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 0
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'In'#237'cio'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -16
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      DefaultToday = True
    end
    object edFim: TsDateEdit
      Left = 224
      Top = 36
      Width = 98
      Height = 24
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Fim'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -16
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      DefaultToday = True
    end
  end
end
