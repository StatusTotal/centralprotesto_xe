�
 TFDESISTENCIA 0�  TPF0TFDesistenciaFDesistenciaLeftbTop� BorderIconsbiSystemMenu BorderStylebsDialogCaption   DesistênciaClientHeight�ClientWidthLColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
KeyPreview	OldCreateOrderPositionpoMainFormCenterOnClose	FormCloseOnCreate
FormCreateOnMouseMoveFormMouseMoveOnShowFormShowPixelsPerInch`
TextHeight TsPanelP1Left Top WidthLHeight-AlignalTopTabOrder SkinData.SkinSectionPANEL TsBevelsBevel1Left5TopWidthHeight  TsLabellbMsgLeft:TopWidthHeightCaption2   Informe abaixo o motivo da desistência do título
ParentFontFont.CharsetANSI_CHARSET
Font.Color+5M Font.Height�	Font.NameArial
Font.Style   
TsDateEditedDataLeft)TopWidthWHeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 	MaxLength

ParentFontTabOrder BoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionDataBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameArialBoundLabel.Font.StylefsBold SkinData.SkinSectionEDITGlyphMode.GrayedGlyphMode.Blend DefaultToday	  TsEditedSeloLeft� TopWidth]HeightHintSELOAutoSizeCharCaseecUpperCaseColor��� Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	MaxLength
ParentFontParentShowHintShowHint	TabOrderSkinData.CustomColor	SkinData.SkinSectionEDITBoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionSeloBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameArialBoundLabel.Font.StylefsBold   TsEditedAleatorioLeftTopWidth,HeightHintSELOAutoSizeCharCaseecUpperCaseColor��� Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 	MaxLength
ParentFontParentShowHintShowHint	TabOrderSkinData.CustomColor	SkinData.SkinSectionEDITBoundLabel.ParentFontBoundLabel.Font.CharsetDEFAULT_CHARSETBoundLabel.Font.ColorclWindowTextBoundLabel.Font.Height�BoundLabel.Font.NameMS Sans SerifBoundLabel.Font.Style    TsBitBtnbtOkLeft�Top�WidthKHeightCursorcrHandPointCaptionOkTabOrderOnClick	btOkClick
ImageIndexImages
dm.ImagensSkinData.SkinSectionBUTTON  TsBitBtn
btCancelarLeft�Top�WidthYHeightCursorcrHandPointCaptionCancelarTabOrderOnClickbtCancelarClick
ImageIndexImages
dm.ImagensSkinData.SkinSectionBUTTON  	TwwDBGridGridLeft Top-WidthLHeightjControlType.StringsCheck;CheckBox;True;False Selected.StringsCheck	4	#	FMOTIVO	74	Tabela de Motivos	F IniAttributes.Delimiter;;IniAttributes.UnicodeIniFile
TitleColor	clBtnFace	FixedColsShowHorzScrollBar	AlignalTopBorderStylebsNone
DataSourcedsIrregularidadesOptions	dgEditingdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit
dgWordWrapdgProportionalColResize TabOrderTitleAlignmenttaCenterTitleFont.CharsetANSI_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameArialTitleFont.Style 
TitleLinesTitleButtons
UseTFieldsOnMouseMoveGridMouseMove	OnMouseUpGridMouseUp  TDataSourcedsIrregularidadesDataSetdm.IrregularidadesLeft�Toph  TDataSource
dsConsultaDataSetFBaixas2014.ConsultaLeft0Toph   