unit UCertidao2Via;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sBevel, StdCtrls, sLabel, DB, Buttons, sBitBtn,
  DBCtrls, sDBMemo, sDBLookupComboBox, Mask, sMaskEdit, sCustomComboEdit,
  sToolEdit, sDBDateEdit, sDBEdit, sGroupBox, sCheckBox;

type
  TFCertidao2Via = class(TForm)
    ckEmitir: TsCheckBox;
    GbCertidao: TsGroupBox;
    edFolhas: TsDBEdit;
    edDataCertidao: TsDBDateEdit;
    edSelo: TsDBEdit;
    lkEscrevente: TsDBLookupComboBox;
    MObservacao: TsDBMemo;
    edEntrega: TsDBDateEdit;
    btSalvar: TsBitBtn;
    btCancelar: TsBitBtn;
    dsCertidao: TDataSource;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sBevel1: TsBevel;
    lbProtocolo: TsLabel;
    lbStatus: TsLabel;
    dsEscreventes: TDataSource;
    procedure btSalvarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCertidao2Via: TFCertidao2Via;

implementation

uses UDM, UGDM, UGeral, UQuickCertidaoCancelamento1, UQuickPagamento1;

{$R *.dfm}

procedure TFCertidao2Via.btSalvarClick(Sender: TObject);
begin
  if edDataCertidao.Date=0 then
  begin
      GR.Aviso('INFORME A DATA DA CERTID�O!');
      edDataCertidao.SetFocus;
      Exit;
  end;

  if lkEscrevente.Text='' then
  begin
      GR.Aviso('INFORME O ESCREVENTE!');
      lkEscrevente.SetFocus;
      Exit;
  end;

  if dm.ValorParametro(72)<>'0' then
    if GR.Pergunta('Gerar N� de Ordem') then
    begin
        dm.CertidoesORDEM.AsInteger:=StrToInt(dm.ValorParametro(72));
        dm.AtualizarParametro(72,IntToStr(dm.CertidoesORDEM.AsInteger+1));
    end;

  dm.CertidoesRESPONSAVEL.AsString:=dm.CertidoesESCREVENTE.AsString;
  dm.Certidoes.Post;
  dm.Certidoes.ApplyUpdates(0);

  if dm.vIdReservado<>0 then
    Gdm.BaixarSelo(dm.CertidoesSELO.AsString,
                   dm.vIdReservado,
                   'PROTESTO',
                   Self.Name,
                   dm.CertidoesID_CERTIDAO.AsInteger,
                   dm.CertidoesID_ATO.AsInteger,
                   4,
                   dm.CertidoesDT_CERTIDAO.AsDateTime,
                   '',0,0,0,
                   StrToInt(lbProtocolo.Caption),
                   dm.CertidoesRECIBO.AsInteger,
                   dm.CertidoesCODIGO.AsInteger,
                   'CERTID�O 2� VIA ('+lbStatus.Caption+')',
                   lkEscrevente.Text,
                   dm.CertidoesCONVENIO.AsString,
                   dm.CertidoesCOBRANCA.AsString,
                   dm.CertidoesEMOLUMENTOS.AsFloat,
                   dm.CertidoesFETJ.AsFloat,
                   dm.CertidoesFUNDPERJ.AsFloat,
                   dm.CertidoesFUNPERJ.AsFloat,
                   dm.CertidoesFUNARPEN.AsFloat,
                   dm.CertidoesPMCMV.AsFloat,
                   0,0,0,0,0,0,0,
                   dm.CertidoesTOTAL.AsFloat,
                   dm.CertidoesISS.AsFloat);

  if ckEmitir.Checked then
  begin
      dm.vAssinatura:=dm.EscreventesNOME.AsString+' - '+dm.EscreventesQUALIFICACAO.AsString;
      if dm.EscreventesMATRICULA.AsString<>'' then
        dm.vMatricula:='Matr�cula: '+dm.EscreventesMATRICULA.AsString
          else dm.vMatricula:='';

      dm.Titulos.Close;
      dm.Titulos.Params[0].AsInteger:=dm.CertidoesID_ATO.AsInteger;
      dm.Titulos.Open;

      dm.v2Via:=GR.iif(dm.ServentiaCODIGO.AsInteger=1726,False,True);

      if lbStatus.Caption='CANCELADO' then
      begin
          Application.CreateForm(TFQuickCertidaoCancelamento1,FQuickCertidaoCancelamento1);
          FQuickCertidaoCancelamento1.Free;
      end
      else
      begin
          Application.CreateForm(TFQuickPagamento1,FQuickPagamento1);
          with FQuickPagamento1 do
          begin
              qryImprime.Close;
              qryImprime.SQL.Text:='SELECT * FROM TITULOS WHERE ID_ATO IN ('+dm.CertidoesID_ATO.AsString+') ORDER BY PROTOCOLO';
              qryImprime.Open;

              qrPagamento.Enabled   :=False;
              lbProtocolo2.Enabled  :=False;
              lbPortador2.Enabled   :=False;
              qrDeclaro.Enabled     :=False;
              lbDocumento2.Enabled  :=False;
              lbEspecie2.Enabled    :=False;
              lbNumero2.Enabled     :=False;
              lbValor.Enabled       :=False;
              lbValorTitulo.Enabled :=False;
              lbDevedor2.Enabled    :=False;
              lbSacador2.Enabled    :=False;
              lbCedente2.Enabled    :=False;
              lbForma2.Enabled      :=False;
              lbSelo2.Enabled       :=False;
              lbData2.Enabled       :=False;
              QRShape3.Enabled      :=False;
              lbLinha.Enabled       :=False;

              Certidao.Preview;
              
              if dm.vPergunta then
                Certidao2.Preview;
              
              Free;
          end;
          dm.v2Via:=False;
      end;

      dm.Titulos.Close;
  end;
  Close;
end;

procedure TFCertidao2Via.FormCreate(Sender: TObject);
begin
  lbProtocolo.Caption:=IntToStr(dm.CampoTitulos('PROTOCOLO',dm.CertidoesID_ATO.AsInteger));
  lbStatus.Caption:=dm.CampoTitulos('STATUS',dm.CertidoesID_ATO.AsInteger);
end;

procedure TFCertidao2Via.btCancelarClick(Sender: TObject);
begin
  dm.Certidoes.Cancel;
  Gdm.LiberarSelo('PROTESTO',dm.CertidoesSELO.AsString,False,-1,dm.vIdReservado);
  Close;
end;

procedure TFCertidao2Via.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if dm.Certidoes.State<>dsBrowse then
  btCancelar.Click;
end;

procedure TFCertidao2Via.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then btCancelar.Click;
end;

end.
