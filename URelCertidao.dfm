object FRelCertidao: TFRelCertidao
  Left = 254
  Top = 183
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Rela'#231#227'o de Certid'#245'es'
  ClientHeight = 167
  ClientWidth = 224
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 15
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 224
    Height = 167
    Align = alClient
    TabOrder = 0
    SkinData.SkinSection = 'SELECTION'
    ExplicitWidth = 230
    object edInicio: TsDateEdit
      Left = 76
      Top = 21
      Width = 111
      Height = 26
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 0
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'In'#237'cio'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -16
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
    end
    object edFim: TsDateEdit
      Left = 76
      Top = 64
      Width = 111
      Height = 26
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Fim'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -16
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
    end
    object btVisualizar: TsBitBtn
      Left = 32
      Top = 122
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Caption = 'Visualizar'
      TabOrder = 2
      OnClick = btVisualizarClick
      SkinData.SkinSection = 'BUTTON'
    end
    object btSair: TsBitBtn
      Left = 112
      Top = 122
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Caption = 'Sair'
      TabOrder = 3
      OnClick = btSairClick
      SkinData.SkinSection = 'BUTTON'
    end
    object Relatorio: TQuickRep
      Left = 344
      Top = 56
      Width = 794
      Height = 1123
      ShowingPreview = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      Functions.Strings = (
        'PAGENUMBER'
        'COLUMNNUMBER'
        'REPORTTITLE')
      Functions.DATA = (
        '0'
        '0'
        #39#39)
      Options = [FirstPageHeader, LastPageFooter]
      Page.Columns = 1
      Page.Orientation = poPortrait
      Page.PaperSize = A4
      Page.Continuous = False
      Page.Values = (
        100.000000000000000000
        2970.000000000000000000
        100.000000000000000000
        2100.000000000000000000
        100.000000000000000000
        100.000000000000000000
        0.000000000000000000)
      PrinterSettings.Copies = 1
      PrinterSettings.OutputBin = Auto
      PrinterSettings.Duplex = False
      PrinterSettings.FirstPage = 0
      PrinterSettings.LastPage = 0
      PrinterSettings.UseStandardprinter = False
      PrinterSettings.UseCustomBinCode = False
      PrinterSettings.CustomBinCode = 0
      PrinterSettings.ExtendedDuplex = 0
      PrinterSettings.UseCustomPaperCode = False
      PrinterSettings.CustomPaperCode = 0
      PrinterSettings.PrintMetaFile = False
      PrinterSettings.MemoryLimit = 1000000
      PrinterSettings.PrintQuality = 0
      PrinterSettings.Collate = 0
      PrinterSettings.ColorOption = 0
      PrintIfEmpty = True
      SnapToGrid = True
      Units = MM
      Zoom = 100
      PrevFormStyle = fsNormal
      PreviewInitialState = wsMaximized
      PreviewWidth = 500
      PreviewHeight = 500
      PrevInitialZoom = qrZoomToWidth
      PreviewDefaultSaveType = stQRP
      PreviewLeft = 0
      PreviewTop = 0
      object Cabecalho: TQRBand
        Left = 38
        Top = 38
        Width = 718
        Height = 123
        Frame.DrawTop = True
        Frame.DrawBottom = True
        Frame.DrawLeft = True
        Frame.DrawRight = True
        AlignToBottom = False
        BeforePrint = CabecalhoBeforePrint
        TransparentBand = False
        ForceNewColumn = False
        ForceNewPage = False
        Size.Values = (
          325.437500000000000000
          1899.708333333333000000)
        PreCaluculateBandHeight = False
        KeepOnOnePage = False
        BandType = rbPageHeader
        object QRLabel1: TQRLabel
          Left = 288
          Top = 100
          Width = 142
          Height = 20
          Size.Values = (
            52.916666666666670000
            762.000000000000000000
            264.583333333333300000
            375.708333333333300000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = True
          Caption = 'RELA'#199#195'O DE CERTID'#213'ES'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 9
        end
        object lbTabeliao: TQRLabel
          Left = 338
          Top = 31
          Width = 41
          Height = 15
          Size.Values = (
            39.687500000000000000
            894.291666666666700000
            82.020833333333330000
            108.479166666666700000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = True
          Caption = 'Tabeliao'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 8
        end
        object lbCartorio: TQRLabel
          Left = 320
          Top = 4
          Width = 78
          Height = 24
          Size.Values = (
            63.500000000000000000
            846.666666666666700000
            10.583333333333330000
            206.375000000000000000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = True
          Caption = 'Cartorio'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsItalic]
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 14
        end
        object QRLabel16: TQRLabel
          Left = 215
          Top = 46
          Width = 288
          Height = 18
          Size.Values = (
            47.625000000000000000
            568.854166666666700000
            121.708333333333300000
            762.000000000000000000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = True
          Caption = 'PROTESTO DE T'#205'TULOS E DOCUMENTOS DE D'#205'VIDA'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 9
        end
        object lbEndereco: TQRLabel
          Left = 335
          Top = 62
          Width = 47
          Height = 18
          Size.Values = (
            47.625000000000000000
            886.354166666666700000
            164.041666666666700000
            124.354166666666700000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = True
          Caption = 'Endere'#231'o'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 8
        end
        object lbTelefone: TQRLabel
          Left = 337
          Top = 75
          Width = 43
          Height = 18
          Size.Values = (
            47.625000000000000000
            891.645833333333300000
            198.437500000000000000
            113.770833333333300000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = True
          Caption = 'Telefone'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 8
        end
      end
      object QRBand1: TQRBand
        Left = 38
        Top = 183
        Width = 718
        Height = 21
        Frame.DrawBottom = True
        Frame.DrawLeft = True
        Frame.DrawRight = True
        AlignToBottom = False
        TransparentBand = False
        ForceNewColumn = False
        ForceNewPage = False
        Size.Values = (
          55.562500000000000000
          1899.708333333333000000)
        PreCaluculateBandHeight = False
        KeepOnOnePage = False
        BandType = rbDetail
        object QRDBText1: TQRDBText
          Left = 9
          Top = 3
          Width = 71
          Height = 15
          Size.Values = (
            39.687500000000000000
            23.812500000000000000
            7.937500000000000000
            187.854166666666700000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = False
          Color = clWhite
          DataField = 'DT_CERTIDAO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          FullJustify = False
          MaxBreakChars = 0
          VerticalAlignment = tlTop
          FontSize = 8
        end
        object QRDBText2: TQRDBText
          Left = 121
          Top = 3
          Width = 38
          Height = 15
          Size.Values = (
            39.687500000000000000
            320.145833333333300000
            7.937500000000000000
            100.541666666666700000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = False
          Color = clWhite
          DataField = 'RECIBO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          FullJustify = False
          MaxBreakChars = 0
          VerticalAlignment = tlTop
          FontSize = 8
        end
        object QRDBText3: TQRDBText
          Left = 199
          Top = 3
          Width = 65
          Height = 15
          Size.Values = (
            39.687500000000000000
            526.520833333333300000
            7.937500000000000000
            171.979166666666700000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = False
          Color = clWhite
          DataField = 'SeloAleatorio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          FullJustify = False
          MaxBreakChars = 0
          VerticalAlignment = tlTop
          FontSize = 8
        end
        object QRDBText4: TQRDBText
          Left = 302
          Top = 3
          Width = 61
          Height = 15
          Size.Values = (
            39.687500000000000000
            799.041666666666700000
            7.937500000000000000
            161.395833333333300000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = False
          Color = clWhite
          DataField = 'TipoCertidao'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          FullJustify = False
          MaxBreakChars = 0
          VerticalAlignment = tlTop
          FontSize = 8
        end
        object QRDBText5: TQRDBText
          Left = 400
          Top = 3
          Width = 311
          Height = 15
          Size.Values = (
            39.687500000000000000
            1058.333333333333000000
            7.937500000000000000
            822.854166666666700000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taLeftJustify
          AlignToBand = False
          AutoSize = False
          Color = clWhite
          DataField = 'REQUERENTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          FullJustify = False
          MaxBreakChars = 0
          VerticalAlignment = tlTop
          FontSize = 8
        end
      end
      object ChildBand1: TQRChildBand
        Left = 38
        Top = 161
        Width = 718
        Height = 22
        Frame.DrawBottom = True
        Frame.DrawLeft = True
        Frame.DrawRight = True
        AlignToBottom = False
        TransparentBand = False
        ForceNewColumn = False
        ForceNewPage = False
        Size.Values = (
          58.208333333333330000
          1899.708333333333000000)
        PreCaluculateBandHeight = False
        KeepOnOnePage = False
        ParentBand = Cabecalho
        PrintOrder = cboAfterParent
        object QRLabel2: TQRLabel
          Left = 32
          Top = 3
          Width = 27
          Height = 16
          Size.Values = (
            42.333333333333330000
            84.666666666666670000
            7.937500000000000000
            71.437500000000000000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taLeftJustify
          AlignToBand = False
          Caption = 'Data'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = False
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 9
        end
        object QRLabel3: TQRLabel
          Left = 120
          Top = 3
          Width = 40
          Height = 16
          Size.Values = (
            42.333333333333330000
            317.500000000000000000
            7.937500000000000000
            105.833333333333300000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taLeftJustify
          AlignToBand = False
          Caption = 'Recibo'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = False
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 9
        end
        object QRLabel4: TQRLabel
          Left = 218
          Top = 3
          Width = 26
          Height = 16
          Size.Values = (
            42.333333333333330000
            576.791666666666700000
            7.937500000000000000
            68.791666666666670000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taLeftJustify
          AlignToBand = False
          Caption = 'Selo'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = False
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 9
        end
        object QRLabel5: TQRLabel
          Left = 319
          Top = 3
          Width = 25
          Height = 16
          Size.Values = (
            42.333333333333330000
            844.020833333333300000
            7.937500000000000000
            66.145833333333330000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taLeftJustify
          AlignToBand = False
          Caption = 'Tipo'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = False
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 9
        end
        object QRLabel6: TQRLabel
          Left = 400
          Top = 3
          Width = 67
          Height = 16
          Size.Values = (
            42.333333333333330000
            1058.333333333333000000
            7.937500000000000000
            177.270833333333300000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taLeftJustify
          AlignToBand = False
          Caption = 'Requerente'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = False
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 9
        end
      end
    end
  end
  object Certidoes: TFDQuery
    OnCalcFields = FDQuery1CalcFields
    Connection = dm.conSISTEMA
    SQL.Strings = (
      
        'select ID_CERTIDAO, ID_ATO, DT_CERTIDAO, TIPO_CERTIDAO, SELO, AL' +
        'EATORIO, REQUERENTE from CERTIDOES'
      ''
      'where DT_CERTIDAO between :D1 and :D2'
      ''
      'order by DT_CERTIDAO,RECIBO')
    Left = 256
    Top = 24
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end>
    object CertidoesID_CERTIDAO: TIntegerField
      FieldName = 'ID_CERTIDAO'
      Origin = 'ID_CERTIDAO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object CertidoesID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
    end
    object CertidoesDT_CERTIDAO: TDateField
      FieldName = 'DT_CERTIDAO'
      Origin = 'DT_CERTIDAO'
    end
    object CertidoesTIPO_CERTIDAO: TStringField
      FieldName = 'TIPO_CERTIDAO'
      Origin = 'TIPO_CERTIDAO'
      FixedChar = True
      Size = 1
    end
    object CertidoesSELO: TStringField
      FieldName = 'SELO'
      Origin = 'SELO'
      Size = 9
    end
    object CertidoesALEATORIO: TStringField
      FieldName = 'ALEATORIO'
      Origin = 'ALEATORIO'
      Size = 3
    end
    object CertidoesREQUERENTE: TStringField
      FieldName = 'REQUERENTE'
      Origin = 'REQUERENTE'
      Size = 100
    end
    object CertidoesSeloAleatorio: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'SeloAleatorio'
      Size = 15
    end
    object CertidoesTipoCertidao: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'TipoCertidao'
      Size = 40
    end
  end
end
