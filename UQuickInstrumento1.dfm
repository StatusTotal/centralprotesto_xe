object FQuickInstrumento1: TFQuickInstrumento1
  Left = 531
  Top = 182
  Caption = 'Instrumento'
  ClientHeight = 640
  ClientWidth = 946
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 14
  object RE4: TRichEdit
    Left = 20
    Top = 493
    Width = 614
    Height = 25
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Zoom = 100
  end
  object RE3: TRichEdit
    Left = 20
    Top = 467
    Width = 630
    Height = 25
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Zoom = 100
  end
  object RE2: TRichEdit
    Left = 20
    Top = 376
    Width = 630
    Height = 25
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Zoom = 100
  end
  object RE1: TRichEdit
    Left = 20
    Top = 417
    Width = 630
    Height = 25
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Zoom = 100
  end
  object qkInstrumento: TQuickRep
    Left = 96
    Top = -470
    Width = 794
    Height = 1123
    Frame.DrawTop = True
    Frame.DrawBottom = True
    Frame.DrawLeft = True
    Frame.DrawRight = True
    ShowingPreview = False
    DataSet = RX
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnEndPage = qkInstrumentoEndPage
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = Custom
    Page.Continuous = False
    Page.Values = (
      150.000000000000000000
      2970.000000000000000000
      150.000000000000000000
      2100.000000000000000000
      250.000000000000000000
      200.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = False
    PrinterSettings.CustomBinCode = 0
    PrinterSettings.ExtendedDuplex = 0
    PrinterSettings.UseCustomPaperCode = True
    PrinterSettings.CustomPaperCode = 0
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.MemoryLimit = 1000000
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 0
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 100
    PrevFormStyle = fsNormal
    PreviewInitialState = wsMaximized
    PreviewWidth = 500
    PreviewHeight = 500
    PrevInitialZoom = qrZoomToWidth
    PreviewDefaultSaveType = stQRP
    PreviewLeft = 0
    PreviewTop = 0
    object Titulo: TQRBand
      Left = 94
      Top = 57
      Width = 624
      Height = 66
      AlignToBottom = False
      BeforePrint = TituloBeforePrint
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        174.625000000000000000
        1651.000000000000000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbPageHeader
      object qrSite: TQRDBText
        Left = 23
        Top = 48
        Width = 577
        Height = 15
        Size.Values = (
          39.687500000000000000
          60.854166666666670000
          127.000000000000000000
          1526.645833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        AutoSize = False
        Color = clWhite
        DataSet = dm.Serventia
        DataField = 'SITE'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrEnderecoServentia: TQRDBText
        Left = 23
        Top = 34
        Width = 577
        Height = 15
        Size.Values = (
          39.687500000000000000
          60.854166666666670000
          89.958333333333330000
          1526.645833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        AutoSize = False
        Color = clWhite
        DataSet = dm.Serventia
        DataField = 'ENDERECO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrTitulo: TQRLabel
        Left = 23
        Top = 19
        Width = 577
        Height = 16
        Size.Values = (
          42.333333333333340000
          60.854166666666670000
          50.270833333333330000
          1526.645833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        AutoSize = False
        Caption = 'qrTitulo'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrServentia: TQRLabel
        Left = 23
        Top = 1
        Width = 577
        Height = 19
        Size.Values = (
          50.270833333333330000
          60.854166666666670000
          2.645833333333333000
          1526.645833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        AutoSize = False
        Caption = 'qrServentia'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 12
      end
    end
    object Detalhes: TQRBand
      Left = 94
      Top = 148
      Width = 624
      Height = 559
      AlignToBottom = False
      BeforePrint = DetalhesBeforePrint
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = True
      Size.Values = (
        1479.020833333333000000
        1651.000000000000000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object qrApresentante: TQRLabel
        Left = 4
        Top = 145
        Width = 594
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          383.645833333333300000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Apresentante:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrNatureza: TQRLabel
        Left = 4
        Top = 177
        Width = 594
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          468.312500000000000000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Natureza:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrNumero: TQRLabel
        Left = 4
        Top = 193
        Width = 594
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          510.645833333333300000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'N'#186' do T'#237'tulo:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrEndosso: TQRLabel
        Left = 4
        Top = 241
        Width = 594
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          637.645833333333300000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Endosso:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrDevedor: TQRLabel
        Left = 4
        Top = 257
        Width = 594
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          679.979166666666700000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Devedor:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrDocumento: TQRLabel
        Left = 4
        Top = 273
        Width = 594
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          722.312500000000000000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'CPF/CNPJ:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrIntimado: TQRLabel
        Left = 4
        Top = 419
        Width = 594
        Height = 32
        Size.Values = (
          84.666666666666670000
          10.583333333333330000
          1108.604166666667000000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Intima'#231#227'o:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrSacador: TQRLabel
        Left = 4
        Top = 371
        Width = 594
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          981.604166666666700000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Sacador:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrCedente: TQRLabel
        Left = 4
        Top = 403
        Width = 594
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          1066.270833333333000000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Cedente:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrValor: TQRLabel
        Left = 4
        Top = 209
        Width = 594
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          552.979166666666700000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Valor:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrEmissao: TQRLabel
        Left = 4
        Top = 225
        Width = 594
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          595.312500000000000000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Emiss'#227'o:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object RT2: TQRRichText
        Left = 4
        Top = 482
        Width = 594
        Height = 76
        Size.Values = (
          201.083333333333300000
          10.583333333333330000
          1275.291666666667000000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AutoStretch = False
        Color = clWindow
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = []
        HiresExport = False
        Transparent = False
        YIncrement = 50
        ParentRichEdit = RE2
      end
      object qrMotivo: TQRLabel
        Left = 4
        Top = 128
        Width = 313
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          338.666666666666700000
          828.145833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = #8226'Motivo do Protesto: Falta de Pagamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsUnderline]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrEnderecoSacador: TQRLabel
        Left = 4
        Top = 387
        Width = 594
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          1023.937500000000000000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Endere'#231'o:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrEndereco: TQRLabel
        Left = 4
        Top = 289
        Width = 594
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          764.645833333333300000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Endere'#231'o:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrEnderecoAp: TQRLabel
        Left = 4
        Top = 161
        Width = 594
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          425.979166666666700000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Endere'#231'o:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object RT1: TQRRichText
        Left = 4
        Top = 38
        Width = 594
        Height = 91
        Size.Values = (
          240.770833333333300000
          10.583333333333330000
          100.541666666666700000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AutoStretch = True
        Color = clWindow
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        HiresExport = False
        Transparent = False
        YIncrement = 50
        ParentRichEdit = RE1
      end
      object qrLivroFolha: TQRLabel
        Left = 419
        Top = 4
        Width = 177
        Height = 17
        Size.Values = (
          44.979166666666670000
          1108.604166666667000000
          10.583333333333330000
          468.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'LIVRO: 00    FOLHA: 00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrProtocolo: TQRLabel
        Left = 4
        Top = 4
        Width = 161
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          10.583333333333330000
          425.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'PROTOCOLO N'#186': 000000'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrDataProtocolo: TQRLabel
        Left = 4
        Top = 20
        Width = 233
        Height = 17
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          52.916666666666670000
          616.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'DATA DO PROTOCOLO: 00/00/0000'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrDataProtesto: TQRLabel
        Left = 371
        Top = 20
        Width = 225
        Height = 17
        Size.Values = (
          44.979166666666670000
          981.604166666666700000
          52.916666666666670000
          595.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'DATA DO PROTESTO: 00/00/0000'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object lbEndereco: TQRLabel
        Left = 4
        Top = 321
        Width = 594
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          849.312500000000000000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Endere'#231'o:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object RT4: TQRRichText
        Left = 4
        Top = 451
        Width = 594
        Height = 32
        Size.Values = (
          84.666666666666670000
          10.583333333333330000
          1193.270833333333000000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AutoStretch = False
        Color = clWindow
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = []
        HiresExport = False
        Transparent = False
        YIncrement = 50
        ParentRichEdit = RE4
      end
      object lbCoDevedor2: TQRLabel
        Left = 4
        Top = 337
        Width = 411
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          891.645833333333300000
          1087.437500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Co-Devedor:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object lbDocumento2: TQRLabel
        Left = 416
        Top = 337
        Width = 182
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          1100.666666666667000000
          891.645833333333300000
          481.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'CNPJ: 00.000.000/0000-00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object lbCoDevedor: TQRLabel
        Left = 4
        Top = 305
        Width = 411
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          806.979166666666700000
          1087.437500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Co-Devedor:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object lbDocumento: TQRLabel
        Left = 416
        Top = 305
        Width = 182
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          1100.666666666667000000
          806.979166666666700000
          481.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'CNPJ: 00.000.000/0000-00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object lbEndereco2: TQRLabel
        Left = 4
        Top = 354
        Width = 594
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          936.625000000000000000
          1571.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = #8226'Endere'#231'o:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
    end
    object Child: TQRChildBand
      Left = 94
      Top = 707
      Width = 624
      Height = 37
      AlignToBottom = False
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        97.895833333333330000
        1651.000000000000000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      ParentBand = Detalhes
      PrintOrder = cboAfterParent
      object RT3: TQRRichText
        Left = 4
        Top = 1
        Width = 618
        Height = 36
        Size.Values = (
          95.250000000000000000
          10.583333333333330000
          2.645833333333333000
          1635.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AutoStretch = False
        Color = clWindow
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        HiresExport = False
        Transparent = False
        YIncrement = 50
        ParentRichEdit = RE3
      end
    end
    object ChildBand: TQRChildBand
      Left = 94
      Top = 744
      Width = 624
      Height = 321
      AlignToBottom = False
      BeforePrint = ChildBandBeforePrint
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        849.312500000000000000
        1651.000000000000000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      ParentBand = Child
      PrintOrder = cboAfterParent
      object Shape: TQRShape
        Left = 2
        Top = 16
        Width = 441
        Height = 1
        Size.Values = (
          2.645833333333333000
          5.291666666666667000
          42.333333333333330000
          1166.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object lbConvenio: TQRLabel
        Left = 2
        Top = 1
        Width = 397
        Height = 15
        Size.Values = (
          39.687500000000000000
          5.291666666666667000
          2.645833333333333000
          1050.395833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 
          '* ATO NORMATIVO CONJUNTO N'#186' 11/2010 TJERJ (ANTIGO 05/2005 - CONV' +
          #202'NIO)'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object shEscrevente: TQRShape
        Left = 176
        Top = 122
        Width = 273
        Height = 8
        Size.Values = (
          21.166666666666670000
          465.666666666666700000
          322.791666666666700000
          722.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qrFuncionario: TQRLabel
        Left = 268
        Top = 127
        Width = 87
        Height = 15
        Size.Values = (
          39.687500000000000000
          709.083333333333300000
          336.020833333333400000
          230.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'Nome Funcion'#225'rio'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrQualificacao: TQRLabel
        Left = 285
        Top = 141
        Width = 53
        Height = 13
        Size.Values = (
          34.395833333333340000
          754.062500000000000000
          373.062500000000000000
          140.229166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'Qualifica'#231#227'o'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrTestemunho: TQRLabel
        Left = 166
        Top = 98
        Width = 292
        Height = 17
        Size.Values = (
          44.979166666666670000
          439.208333333333400000
          259.291666666666700000
          772.583333333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'Cidade, 00 de MMMM de AAAA. Em testemunho da verdade.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrEM: TQRLabel
        Left = 5
        Top = 21
        Width = 85
        Height = 15
        Size.Values = (
          39.687500000000000000
          13.229166666666670000
          55.562500000000000000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Emolumentos:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrFE: TQRLabel
        Left = 5
        Top = 35
        Width = 85
        Height = 15
        Size.Values = (
          39.687500000000000000
          13.229166666666670000
          92.604166666666670000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Fetj.......:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrFD: TQRLabel
        Left = 5
        Top = 49
        Width = 85
        Height = 15
        Size.Values = (
          39.687500000000000000
          13.229166666666670000
          129.645833333333300000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Fundperj...:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrFP: TQRLabel
        Left = 5
        Top = 63
        Width = 85
        Height = 15
        Size.Values = (
          39.687500000000000000
          13.229166666666670000
          166.687500000000000000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Funperj....:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrMU: TQRLabel
        Left = 148
        Top = 49
        Width = 64
        Height = 15
        Size.Values = (
          39.687500000000000000
          391.583333333333300000
          129.645833333333300000
          169.333333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'M'#250'tua...:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrTT: TQRLabel
        Left = 269
        Top = 49
        Width = 120
        Height = 15
        Size.Values = (
          39.687500000000000000
          711.729166666666700000
          129.645833333333300000
          317.500000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Total...........:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrEmolumentos: TQRLabel
        Left = 91
        Top = 21
        Width = 50
        Height = 15
        Size.Values = (
          39.687500000000000000
          240.770833333333300000
          55.562500000000000000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '0,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrFetj: TQRLabel
        Left = 91
        Top = 35
        Width = 50
        Height = 15
        Size.Values = (
          39.687500000000000000
          240.770833333333300000
          92.604166666666670000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '0,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrFundperj: TQRLabel
        Left = 91
        Top = 49
        Width = 50
        Height = 15
        Size.Values = (
          39.687500000000000000
          240.770833333333300000
          129.645833333333300000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '0,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrFunperj: TQRLabel
        Left = 91
        Top = 63
        Width = 50
        Height = 15
        Size.Values = (
          39.687500000000000000
          240.770833333333300000
          166.687500000000000000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '0,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrMutua: TQRLabel
        Left = 213
        Top = 49
        Width = 50
        Height = 15
        Size.Values = (
          39.687500000000000000
          563.562500000000000000
          129.645833333333300000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '0,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrTotal: TQRLabel
        Left = 390
        Top = 49
        Width = 50
        Height = 15
        Size.Values = (
          39.687500000000000000
          1031.875000000000000000
          129.645833333333300000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '0,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDI: TQRLabel
        Left = 148
        Top = 76
        Width = 64
        Height = 15
        Size.Values = (
          39.687500000000000000
          391.583333333333300000
          201.083333333333300000
          169.333333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Distrib.:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDistribuicao: TQRLabel
        Left = 213
        Top = 76
        Width = 50
        Height = 15
        Size.Values = (
          39.687500000000000000
          563.562500000000000000
          201.083333333333300000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '0,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrTB: TQRLabel
        Left = 269
        Top = 21
        Width = 120
        Height = 15
        Size.Values = (
          39.687500000000000000
          711.729166666666700000
          55.562500000000000000
          317.500000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Tarifa Banc'#225'ria.:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrTarifa: TQRLabel
        Left = 390
        Top = 21
        Width = 50
        Height = 15
        Size.Values = (
          39.687500000000000000
          1031.875000000000000000
          55.562500000000000000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '0,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrCO: TQRLabel
        Left = 269
        Top = 35
        Width = 120
        Height = 15
        Size.Values = (
          39.687500000000000000
          711.729166666666700000
          92.604166666666670000
          317.500000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Correios........:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrCorreio: TQRLabel
        Left = 390
        Top = 35
        Width = 50
        Height = 15
        Size.Values = (
          39.687500000000000000
          1031.875000000000000000
          92.604166666666670000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '0,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrFN: TQRLabel
        Left = 5
        Top = 76
        Width = 85
        Height = 15
        Size.Values = (
          39.687500000000000000
          13.229166666666670000
          201.083333333333300000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Funarpen...:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrFunarpen: TQRLabel
        Left = 91
        Top = 76
        Width = 50
        Height = 15
        Size.Values = (
          39.687500000000000000
          240.770833333333300000
          201.083333333333300000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '0,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape1: TQRShape
        Left = 145
        Top = 16
        Width = 1
        Height = 77
        Size.Values = (
          203.729166666666700000
          383.645833333333300000
          42.333333333333330000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrPM: TQRLabel
        Left = 148
        Top = 21
        Width = 64
        Height = 15
        Size.Values = (
          39.687500000000000000
          391.583333333333300000
          55.562500000000000000
          169.333333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Pmcmv...:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrPmcmv: TQRLabel
        Left = 213
        Top = 21
        Width = 50
        Height = 15
        Size.Values = (
          39.687500000000000000
          563.562500000000000000
          55.562500000000000000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '0,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrAC: TQRLabel
        Left = 148
        Top = 63
        Width = 64
        Height = 15
        Size.Values = (
          39.687500000000000000
          391.583333333333300000
          166.687500000000000000
          169.333333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Acoterj.:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrAcoterj: TQRLabel
        Left = 213
        Top = 63
        Width = 50
        Height = 15
        Size.Values = (
          39.687500000000000000
          563.562500000000000000
          166.687500000000000000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '0,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbCGJ1: TQRLabel
        Left = 479
        Top = 18
        Width = 104
        Height = 17
        Size.Values = (
          44.979166666666670000
          1267.354166666667000000
          47.625000000000000000
          275.166666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        Caption = 'Poder Judici'#225'rio - TJERJ'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object lbCGJ2: TQRLabel
        Left = 468
        Top = 29
        Width = 125
        Height = 17
        Size.Values = (
          44.979166666666670000
          1238.250000000000000000
          76.729166666666670000
          330.729166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        Caption = 'Corregedoria Geral da Justi'#231'a'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object lbCGJ3: TQRLabel
        Left = 466
        Top = 40
        Width = 130
        Height = 17
        Size.Values = (
          44.979166666666670000
          1232.958333333333000000
          105.833333333333300000
          343.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        Caption = 'Selo de Fiscaliza'#231#227'o Eletr'#244'nico'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object lbSeloEletronico: TQRLabel
        Left = 488
        Top = 53
        Width = 86
        Height = 17
        Size.Values = (
          44.979166666666670000
          1291.166666666667000000
          140.229166666666700000
          227.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        Caption = 'LLLL 00000 XXX'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbCGJ4: TQRLabel
        Left = 463
        Top = 66
        Width = 136
        Height = 17
        Size.Values = (
          44.979166666666670000
          1225.020833333333000000
          174.625000000000000000
          359.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        Caption = 'Consulte a validade do selo em:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object lbCGJ5: TQRLabel
        Left = 461
        Top = 77
        Width = 140
        Height = 17
        Size.Values = (
          44.979166666666670000
          1219.729166666667000000
          203.729166666666700000
          370.416666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        Caption = 'https://www3.tjrj.jus.br/sitepublico'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrAviso1: TQRLabel
        Left = 43
        Top = 154
        Width = 538
        Height = 40
        Size.Values = (
          105.833333333333300000
          113.770833333333300000
          407.458333333333400000
          1423.458333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        AutoSize = False
        Caption = 
          'CREDOR - Para sua pr'#243'pria SEGURAN'#199'A - No CANCELAMENTO deste T'#237'tu' +
          'lo devem ser observadas as disposi'#231#245'es do Art. 26 da Lei 9492/97' +
          ', e Provimento 21/2000 da CGJ/RJ, resalvando que toda CARTA DE A' +
          'NUENCIA tem que estar com FIRMA RECONHECIDA, c'#243'pia do contrato s' +
          'ocial ou da '#250'ltima altera'#231#227'o social AUTENTICADOS, se o credor fo' +
          'r de fora do ESTADO DO RJ, tamb'#233'm deve ser apresentado o SINAL P' +
          #218'BLICO do cart'#243'rio que reconheceu a firma do documento, e PRINCI' +
          'PALMENTE anexar o documento de d'#237'vida apresentado para protesto.' +
          ' Assim voc'#234' evita atrasos e embara'#231'os nos procedimentos.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrAviso2: TQRLabel
        Left = 70
        Top = 195
        Width = 484
        Height = 20
        Size.Values = (
          52.916666666666670000
          185.208333333333300000
          515.937500000000000000
          1280.583333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        AutoSize = False
        Caption = 
          'Este Registro '#233' gerado de forma eletr'#244'nica, qualquer forma de ra' +
          'sura ou complementa'#231#227'o o tornar'#225' como inv'#225'lido, ou como princ'#237'pi' +
          'o de fraude. (Art. 559 - Resolu'#231#227'o 01/2000 - CGJERJ) - V'#225'lida so' +
          'mente se aposta de selo de fiscaliza'#231#227'o.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRShape2: TQRShape
        Left = 2
        Top = 17
        Width = 1
        Height = 76
        Size.Values = (
          201.083333333333300000
          5.291666666666667000
          44.979166666666670000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel1: TQRLabel
        Left = 148
        Top = 35
        Width = 64
        Height = 15
        Size.Values = (
          39.687500000000000000
          391.583333333333300000
          92.604166666666670000
          169.333333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Iss.....:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrIss: TQRLabel
        Left = 213
        Top = 35
        Width = 50
        Height = 15
        Size.Values = (
          39.687500000000000000
          563.562500000000000000
          92.604166666666670000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '0,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape3: TQRShape
        Left = 2
        Top = 93
        Width = 441
        Height = 1
        Size.Values = (
          2.645833333333333000
          5.291666666666667000
          246.062500000000000000
          1166.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape5: TQRShape
        Left = 266
        Top = 16
        Width = 1
        Height = 77
        Size.Values = (
          203.729166666666700000
          703.791666666666700000
          42.333333333333330000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape6: TQRShape
        Left = 442
        Top = 16
        Width = 1
        Height = 77
        Size.Values = (
          203.729166666666700000
          1169.458333333333000000
          42.333333333333330000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
    end
    object ChildBand2: TQRChildBand
      Left = 94
      Top = 123
      Width = 624
      Height = 25
      AlignToBottom = False
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        66.145833333333340000
        1651.000000000000000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      ParentBand = Titulo
      PrintOrder = cboAfterParent
      object qrInstrumento: TQRLabel
        Left = -119
        Top = 0
        Width = 254
        Height = 23
        Size.Values = (
          60.854166666666670000
          489.479166666666700000
          0.000000000000000000
          672.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'INSTRUMENTO DE PROTESTO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 14
      end
    end
  end
  object dsServentia: TDataSource
    DataSet = dm.Serventia
    Left = 32
    Top = 78
  end
  object RX: TRxMemoryData
    FieldDefs = <
      item
        Name = 'Titulo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Apresentante'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Credor'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Sacador'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Devedor'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Valor'
        DataType = ftFloat
      end
      item
        Name = 'Vencimento'
        DataType = ftDate
      end
      item
        Name = 'Protocolo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Intimacao'
        DataType = ftDate
      end
      item
        Name = 'Saldo'
        DataType = ftFloat
      end
      item
        Name = 'Custas'
        DataType = ftFloat
      end
      item
        Name = 'Endereco'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'Livro'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Folha'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Numero'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Endosso'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Tipo'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Documento'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Intimado'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Motivo'
        DataType = ftString
        Size = 250
      end
      item
        Name = 'Cedente'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Dt_Titulo'
        DataType = ftDate
      end
      item
        Name = 'Emolumentos'
        DataType = ftFloat
      end
      item
        Name = 'Fetj'
        DataType = ftFloat
      end
      item
        Name = 'Fundperj'
        DataType = ftFloat
      end
      item
        Name = 'Funperj'
        DataType = ftFloat
      end
      item
        Name = 'Mutua'
        DataType = ftFloat
      end
      item
        Name = 'Acoterj'
        DataType = ftFloat
      end
      item
        Name = 'Total'
        DataType = ftFloat
      end
      item
        Name = 'Selo'
        DataType = ftString
        Size = 8
      end>
    Left = 32
    Top = 24
    object RXTitulo: TStringField
      FieldName = 'Titulo'
      Size = 100
    end
    object RXApresentante: TStringField
      FieldName = 'Apresentante'
      Size = 100
    end
    object RXCredor: TStringField
      FieldName = 'Credor'
      Size = 100
    end
    object RXSacador: TStringField
      FieldName = 'Sacador'
      Size = 100
    end
    object RXDevedor: TStringField
      FieldName = 'Devedor'
      Size = 100
    end
    object RXValor: TFloatField
      DisplayLabel = '#####0.00'
      FieldName = 'Valor'
      DisplayFormat = '#####0.00'
    end
    object RXVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object RXProtocolo: TStringField
      FieldName = 'Protocolo'
    end
    object RXIntimacao: TDateField
      FieldName = 'Intimacao'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object RXSaldoProtesto: TFloatField
      DisplayLabel = '#####0.00'
      FieldName = 'SaldoProtesto'
      DisplayFormat = '#####0.00'
    end
    object RXCustas: TFloatField
      DisplayLabel = '#####0.00'
      FieldName = 'Custas'
      DisplayFormat = '#####0.00'
    end
    object RXEndereco: TStringField
      FieldName = 'Endereco'
      Size = 200
    end
    object RXLivro: TStringField
      FieldName = 'Livro'
      Size = 10
    end
    object RXFolha: TStringField
      FieldName = 'Folha'
      Size = 10
    end
    object RXNumero: TStringField
      FieldName = 'Numero'
    end
    object RXEndosso: TStringField
      FieldName = 'Endosso'
      Size = 100
    end
    object RXTipo: TStringField
      FieldName = 'Tipo'
      Size = 1
    end
    object RXDocumento: TStringField
      FieldName = 'Documento'
    end
    object RXIntimado: TStringField
      FieldName = 'Intimado'
    end
    object RXMotivo: TStringField
      FieldName = 'Motivo'
      Size = 400
    end
    object RXCedente: TStringField
      FieldName = 'Cedente'
      Size = 100
    end
    object RXDt_Titulo: TDateField
      FieldName = 'Dt_Titulo'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object RXEmolumentos: TFloatField
      FieldName = 'Emolumentos'
    end
    object RXFetj: TFloatField
      FieldName = 'Fetj'
    end
    object RXFundperj: TFloatField
      FieldName = 'Fundperj'
    end
    object RXFunperj: TFloatField
      FieldName = 'Funperj'
    end
    object RXPmcmv: TFloatField
      FieldName = 'Pmcmv'
    end
    object RXIss: TFloatField
      FieldName = 'Iss'
    end
    object RXMutua: TFloatField
      FieldName = 'Mutua'
    end
    object RXAcoterj: TFloatField
      FieldName = 'Acoterj'
    end
    object RXTotal: TFloatField
      FieldName = 'Total'
    end
    object RXSaldoTitulo: TFloatField
      DefaultExpression = '#####0.00'
      FieldName = 'SaldoTitulo'
      DisplayFormat = '#####0.00'
    end
    object RXSelo: TStringField
      FieldName = 'Selo'
      Size = 9
    end
    object RXEnderecoAp: TStringField
      FieldName = 'EnderecoAp'
      Size = 100
    end
    object RXDt_Protocolo: TDateField
      FieldName = 'Dt_Protocolo'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object RXDt_Registro: TDateField
      FieldName = 'Dt_Registro'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object RXTipoProtesto: TIntegerField
      FieldName = 'TipoProtesto'
    end
    object RXObservacao: TMemoField
      FieldName = 'Observacao'
      BlobType = ftMemo
    end
    object RXPraca: TStringField
      FieldName = 'Praca'
      Size = 60
    end
    object RXTermo: TIntegerField
      FieldName = 'Termo'
    end
    object RXEnderecoSa: TStringField
      FieldName = 'EnderecoSa'
      Size = 100
    end
    object RXDt_Publicacao: TDateField
      FieldName = 'Dt_Publicacao'
    end
    object RXDt_Intimacao: TDateField
      FieldName = 'Dt_Intimacao'
    end
    object RXMensagem: TStringField
      FieldName = 'Mensagem'
      Size = 200
    end
    object RXTarifa: TFloatField
      FieldName = 'Tarifa'
    end
    object RXAR: TFloatField
      FieldName = 'AR'
    end
    object RXDistribuicao: TFloatField
      FieldName = 'Distribuicao'
    end
    object RXConvenio: TStringField
      FieldName = 'Convenio'
      Size = 1
    end
    object RXFunarpen: TFloatField
      FieldName = 'Funarpen'
    end
    object RXAleatorio: TStringField
      FieldName = 'Aleatorio'
      Size = 3
    end
    object RXFins: TStringField
      FieldName = 'Fins'
      Size = 3
    end
    object RXNossoNumero: TStringField
      FieldName = 'NossoNumero'
      Size = 15
    end
    object RXDt_Certidao: TDateField
      FieldName = 'Dt_Certidao'
    end
    object RXIdAto: TIntegerField
      FieldName = 'IdAto'
    end
    object RXStatus: TStringField
      FieldName = 'Status'
    end
    object RXSeloCancelamento: TStringField
      FieldName = 'SeloCancelamento'
      Size = 14
    end
    object RXDt_Cancelamento: TDateField
      FieldName = 'Dt_Cancelamento'
    end
    object RXSeloProtesto: TStringField
      FieldName = 'SeloProtesto'
      Size = 14
    end
  end
end
