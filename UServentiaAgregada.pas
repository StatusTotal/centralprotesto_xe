{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UServentiaAgregada.pas
  Descricao:   Tela de cadastro de Serventias Agregadas
  Author   :   Cristina
  Date:        24-ago-2016
  Last Update: 21-jun-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UServentiaAgregada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, DB,
  Dialogs, StdCtrls, Mask, DBCtrls, ExtCtrls, Buttons, Menus, DBClient,
  sCheckBox, sDBCheckBox, sToolEdit, sDBDateEdit, sMaskEdit,
  sCustomComboEdit, sCurrEdit, sDBCalcEdit, sBitBtn, sDBEdit, sPanel,
  Grids, Wwdbigrd, Wwdbgrid, sDBLookupComboBox, sDBComboBox;

type
  TFServentiaAgregada = class(TForm)
    P1: TsPanel;
    edtCodigo: TsDBEdit;
    dsServentiaAgregada: TDataSource;
    edtDescricao: TsDBEdit;
    btOk: TsBitBtn;
    btCancelar: TsBitBtn;
    edtCodigoDistribuicao: TsDBEdit;
    cedValorUltimoSaldo: TsDBCalcEdit;
    dteDataUltimoSaldo: TsDBDateEdit;
    Grid: TwwDBGrid;
    btIncluir: TsBitBtn;
    btAlterar: TsBitBtn;
    btExcluir: TsBitBtn;
    btnSalvarServ: TsBitBtn;
    btnCancelarServ: TsBitBtn;
    edtQtdUltimoSaldo: TsDBEdit;
    chbAtiva: TsDBCheckBox;
    chbUsaProtesto: TsDBCheckBox;
    edtEndereco: TsDBEdit;
    lcbCidade: TsDBLookupComboBox;
    edtCep: TsDBEdit;
    edtTelefone1: TsDBEdit;
    edtEmail1: TsDBEdit;
    edtTitular: TsDBEdit;
    edtTelefone2: TsDBEdit;
    edtEmail2: TsDBEdit;
    cbUF: TsDBComboBox;
    dsMunicipios: TDataSource;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btOkClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btnSalvarServClick(Sender: TObject);
    procedure btnCancelarServClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure chbAtivaExit(Sender: TObject);
    procedure lcbCidadeClick(Sender: TObject);
  private
    { Private declarations }

    IdServAgreg: Integer;

    Modificado: Boolean;

    procedure HabDesabComponentes(Hab: Boolean);
  public
    { Public declarations }
  end;

var
  FServentiaAgregada: TFServentiaAgregada;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFServentiaAgregada.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_ESCAPE then
    Close;

  if key = VK_RETURN then
    Perform(WM_NEXTDLGCTL, 0, 0);
end;

procedure TFServentiaAgregada.btOkClick(Sender: TObject);
begin
  dm.ServentiaAgregada.ApplyUpdates(0);
  Close;
end;

procedure TFServentiaAgregada.chbAtivaExit(Sender: TObject);
begin
  if dm.ServentiaAgregada.State in [dsInsert, dsEdit] then
  begin
    if chbAtiva.Checked then
      dm.ServentiaAgregadaDATA_INATIVACAO.Clear
    else
      dm.ServentiaAgregadaDATA_INATIVACAO.AsDateTime := Date;
  end;
end;

procedure TFServentiaAgregada.btCancelarClick(Sender: TObject);
begin
  if Modificado then
  begin
    if Application.MessageBox('As inclus�es/altera��es realizadas ser�o perdidas. Deseja prosseguir?',
                              'Confima��o',
                              MB_YESNO + MB_ICONQUESTION) = ID_YES then
    begin
      dm.ServentiaAgregada.Cancel;
      Close;
    end;
  end
  else
  begin
    dm.ServentiaAgregada.Cancel;
    Close;
  end;
end;

procedure TFServentiaAgregada.btAlterarClick(Sender: TObject);
begin
  if PF.PasswordInputBox('Seguran�a', 'Senha') <> FormatDateTime('HHMM', Now) then
    Exit;

  HabDesabComponentes(True);
end;

procedure TFServentiaAgregada.FormShow(Sender: TObject);
begin
  if FileExists('C:\FNMASTOTAL') then
  begin
    edtCodigo.MaxLength             := 8;
    edtCodigoDistribuicao.MaxLength := 2;
  end;

  HabDesabComponentes(False);
end;

procedure TFServentiaAgregada.btExcluirClick(Sender: TObject);
begin
  if dm.ServentiaAgregada.RecordCount > 0 then
  begin
    dm.ServentiaAgregada.Delete;
    Modificado := True;
    btOk.Font.Style := [fsBold];
  end;
end;

procedure TFServentiaAgregada.btIncluirClick(Sender: TObject);
begin
  Inc(IdServAgreg);

  dm.ServentiaAgregada.Append;
  dm.ServentiaAgregadaFLG_USAPROTESTO.AsString := 'S';
  dm.ServentiaAgregadaFLG_ATIVA.AsString := 'S';
  chbUsaProtesto.State := cbChecked;
  chbAtiva.State := cbChecked;
  dm.ServentiaAgregadaVALOR_ULTIMO_SALDO.AsFloat := 0;
  dm.ServentiaAgregadaQTD_ULTIMO_SALDO.AsInteger := 0;
  dm.ServentiaAgregadaDATA_ULTIMO_SALDO.Clear;
  dm.ServentiaAgregadaDATA_INATIVACAO.Clear;
  HabDesabComponentes(True);
end;

procedure TFServentiaAgregada.HabDesabComponentes(Hab: Boolean);
begin

  btIncluir.Enabled := not Hab;
  btAlterar.Enabled := (not Hab) and
                       (dm.ServentiaAgregada.RecordCount > 0);
  btExcluir.Enabled := (not Hab) and
                       (dm.ServentiaAgregada.RecordCount > 0);

  edtCodigo.Enabled             := Hab;
  edtDescricao.Enabled          := Hab;
  edtEndereco.Enabled           := Hab;
  lcbCidade.Enabled             := Hab;
  cbUF.Enabled                  := Hab;
  edtCep.Enabled                := Hab;
  edtTitular.Enabled            := Hab;
  edtTelefone1.Enabled          := Hab;
  edtTelefone2.Enabled          := Hab;
  edtEmail1.Enabled             := Hab;
  edtEmail2.Enabled             := Hab;
  edtCodigoDistribuicao.Enabled := Hab;
  chbUsaProtesto.Enabled        := Hab;
  chbAtiva.Enabled              := Hab;

  btnSalvarServ.Visible   := Hab;
  btnCancelarServ.Visible := Hab;

  btOk.Visible       := (not Hab) and
                        (dm.ServentiaAgregada.RecordCount > 0);
  btCancelar.Visible := (not Hab) and
                        (dm.ServentiaAgregada.RecordCount > 0);
end;

procedure TFServentiaAgregada.lcbCidadeClick(Sender: TObject);
begin
  if dm.ServentiaAgregada.State in [dsEdit,dsInsert] then
    dm.ServentiaAgregadaCIDADE_LOGRADOURO.AsString := dm.MunicipiosCIDADE.AsString;
end;

procedure TFServentiaAgregada.btnSalvarServClick(Sender: TObject);
var
  lIncluirNovo: Boolean;
begin
  lIncluirNovo := False;

  if dm.ServentiaAgregada.State in [dsInsert, dsEdit] then
  begin
    if dm.ServentiaAgregada.State = dsInsert then
    begin
      dm.ServentiaAgregadaID_SERVENTIA_AGREGADA.AsInteger := dm.IdAtual('ID_SERVENTIA_AGREGADA', 'S');
      
      if Application.MessageBox('Deseja incluir outra Serventia Agregada?',
                                'Confirma��o',
                                MB_YESNO + MB_ICONQUESTION) = ID_YES then
        lIncluirNovo := True;
    end;

    dm.ServentiaAgregada.Post;

    Modificado := True;
    btOk.Font.Style := [fsBold];
  end;

  HabDesabComponentes(False);

  if lIncluirNovo then
    btIncluir.Click;
end;

procedure TFServentiaAgregada.btnCancelarServClick(Sender: TObject);
begin
  if dm.ServentiaAgregada.State in [dsInsert, dsEdit] then
    dm.ServentiaAgregada.Cancel;    

  HabDesabComponentes(False);
end;

procedure TFServentiaAgregada.FormCreate(Sender: TObject);
begin
  Modificado := False;

  dm.Municipios.Close;
  dm.Municipios.Params[0].AsString := 'RJ';
  dm.Municipios.Open;

  dm.ServentiaAgregada.Close;
  dm.ServentiaAgregada.Open;

  IdServAgreg := dm.ServentiaAgregada.RecordCount;
end;

end.
