unit URelNotificacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, ExtCtrls, sPanel, sEdit, sGroupBox,
  QuickRpt, QRCtrls, DB, RxMemDS, FMTBcd, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, sRadioButton, SqlExpr, Grids, Wwdbigrd,
  Wwdbgrid, DBClient, SimpleDS, DBCtrls, sDBLookupComboBox, Menus, Inifiles,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFRelNotificacao = class(TForm)
    P: TsPanel;
    GbProtocolo: TsGroupBox;
    edProtFinal: TsEdit;
    edProtInicial: TsEdit;
    Rb1: TsRadioButton;
    GbData: TsGroupBox;
    Rb2: TsRadioButton;
    edDataInicial: TsDateEdit;
    edDataFinal: TsDateEdit;
    dsProtocolo: TDataSource;
    GridAtos: TwwDBGrid;
    btFiltrar: TsBitBtn;
    dsEscreventes: TDataSource;
    PM: TPopupMenu;
    Marcartodos1: TMenuItem;
    Desmarcartodos1: TMenuItem;
    btVisualizar: TsBitBtn;
    qryProtocolo: TFDQuery;
    qryProtocoloCheck: TBooleanField;
    qryProtocoloID_ATO: TIntegerField;
    qryProtocoloPROTOCOLO: TIntegerField;
    qryProtocoloDT_PROTOCOLO: TDateField;
    qryProtocoloAPRESENTANTE: TStringField;
    qryProtocoloDEVEDOR: TStringField;
    qryProtocoloCPF_CNPJ_DEVEDOR: TStringField;
    qryProtocoloNUMERO_TITULO: TStringField;
    qryProtocoloTIPO_TITULO: TIntegerField;
    qryProtocoloDT_TITULO: TDateField;
    qryProtocoloDT_VENCIMENTO: TDateField;
    qryProtocoloTOTAL: TFloatField;
    qryProtocoloSTATUS: TStringField;
    qryProtocoloDT_PUBLICACAO: TDateField;
    qryProtocoloTIPO_DEVEDOR: TStringField;
    qryProtocoloVALOR_TITULO: TFloatField;
    procedure edProtInicialKeyPress(Sender: TObject; var Key: Char);
    procedure edProtFinalKeyPress(Sender: TObject; var Key: Char);
    procedure Rb1Click(Sender: TObject);
    procedure Rb2Click(Sender: TObject);
    procedure btFiltrarClick(Sender: TObject);
    procedure GridAtosFieldChanged(Sender: TObject; Field: TField);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btVisualizarClick(Sender: TObject);
    procedure edDataInicialExit(Sender: TObject);
    function Marcados: Integer;
    procedure Marcartodos1Click(Sender: TObject);
    procedure Desmarcartodos1Click(Sender: TObject);
    procedure GridAtosMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelNotificacao: TFRelNotificacao;

implementation

uses UDM, UPF, UQuickNotificacao, UGeral;

{$R *.dfm}

function TFRelNotificacao.Marcados: Integer;
var
  Posicao: Integer;
begin
  Result:=0;
  qryProtocolo.DisableControls;
  Posicao:=qryProtocolo.RecNo;
  qryProtocolo.First;
  while not qryProtocolo.Eof do
  begin
      if qryProtocoloCheck.AsBoolean then Result:=Result+1;
      if Result>1 then Break;
      qryProtocolo.Next;
  end;
  qryProtocolo.RecNo:=Posicao;
  qryProtocolo.EnableControls;
end;

procedure TFRelNotificacao.edProtInicialKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFRelNotificacao.edProtFinalKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFRelNotificacao.Rb1Click(Sender: TObject);
begin
  GbData.Enabled:=False;
  GbProtocolo.Enabled:=True;
  edDataInicial.Clear;
  edDataFinal.Clear;
  edProtInicial.SetFocus;
end;

procedure TFRelNotificacao.Rb2Click(Sender: TObject);
begin
  GbData.Enabled:=True;
  GbProtocolo.Enabled:=False;
  edProtInicial.Clear;
  edProtFinal.Clear;
  edDataInicial.SetFocus;
end;

procedure TFRelNotificacao.btFiltrarClick(Sender: TObject);
begin
  if Rb1.Checked then
    if edProtInicial.Text='' then
    begin
        edProtInicial.SetFocus;
        Exit;
    end;

  if edProtFinal.Text='' then edProtFinal.Text:=edProtInicial.Text;

  qryProtocolo.Close;
  if Rb1.Checked then
  begin
      qryProtocolo.SQL.Text:='SELECT ID_ATO,PROTOCOLO,DT_PROTOCOLO,APRESENTANTE,DEVEDOR,CPF_CNPJ_DEVEDOR,NUMERO_TITULO,TIPO_TITULO,'+
                             'DT_TITULO,DT_VENCIMENTO,TOTAL,STATUS,DT_PUBLICACAO,TIPO_DEVEDOR,VALOR_TITULO FROM TITULOS WHERE PROTOCOLO '+
                             'BETWEEN :P1 AND :P2 ORDER BY PROTOCOLO';
      qryProtocolo.ParamByName('P1').AsInteger:=StrToInt(edProtInicial.Text);
      qryProtocolo.ParamByName('P2').AsInteger:=StrToInt(edProtFinal.Text);
  end
  else
  begin
      qryProtocolo.SQL.Text:='SELECT ID_ATO,PROTOCOLO,DT_PROTOCOLO,APRESENTANTE,DEVEDOR,CPF_CNPJ_DEVEDOR,NUMERO_TITULO,TIPO_TITULO,'+
                             'DT_TITULO,DT_VENCIMENTO,TOTAL,STATUS,DT_PUBLICACAO,TIPO_DEVEDOR,VALOR_TITULO FROM TITULOS WHERE DT_PROTOCOLO '+
                             'BETWEEN :D1 AND :D2 ORDER BY PROTOCOLO';
      qryProtocolo.ParamByName('D1').AsDate:=edDataInicial.Date;
      qryProtocolo.ParamByName('D2').AsDate:=edDataFinal.Date;
  end;
  qryProtocolo.Open;
end;

procedure TFRelNotificacao.GridAtosFieldChanged(Sender: TObject;
  Field: TField);
begin
  if qryProtocolo.State in [dsEdit,dsInsert] then
    qryProtocolo.Post;
end;

procedure TFRelNotificacao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFRelNotificacao.btVisualizarClick(Sender: TObject);
var
  Posicao: Integer;
  F: TextFile;
  I: TIniFile;
  P: String;
begin
  if qryProtocolo.IsEmpty then
    Exit;

  if Marcados = 0 then
    Exit;

  try
    PF.Aguarde(True);
    Application.CreateForm(TFQuickNotificacao,FQuickNotificacao);

    with FQuickNotificacao do
    begin
        RX.Close;
        RX.Open;

        Posicao:=qryProtocolo.RecNo;
        qryProtocolo.DisableControls;
        qryProtocolo.First;
        while not qryProtocolo.Eof do
        begin
            if qryProtocoloCheck.Value=True then
            begin
                RX.Append;
                RXPROTOCOLO.AsInteger       :=qryProtocoloPROTOCOLO.AsInteger;
                RXDT_PROTOCOLO.AsDateTime   :=qryProtocoloDT_PROTOCOLO.AsDateTime;
                RXAPRESENTANTE.AsString     :=qryProtocoloAPRESENTANTE.AsString;
                RXDEVEDOR.AsString          :=qryProtocoloDEVEDOR.AsString;
                RXSTATUS.AsString           :=qryProtocoloSTATUS.AsString;
                if qryProtocoloTIPO_DEVEDOR.AsString='F' then
                  RXCPF_CNPJ_DEVEDOR.AsString:=PF.FormatarCPF(qryProtocoloCPF_CNPJ_DEVEDOR.AsString)
                    else RXCPF_CNPJ_DEVEDOR.AsString:=PF.FormatarCNPJ(qryProtocoloCPF_CNPJ_DEVEDOR.AsString);
                RXNUMERO_TITULO.AsString    :=qryProtocoloNUMERO_TITULO.AsString;
                RXTIPO_TITULO.AsInteger     :=qryProtocoloTIPO_TITULO.AsInteger;
                if qryProtocoloDT_TITULO.AsDateTime<>0 then
                RXDT_TITULO.AsDateTime      :=qryProtocoloDT_TITULO.AsDateTime;
                if qryProtocoloDT_VENCIMENTO.AsDateTime<>0 then
                RXDT_VENCIMENTO.AsDateTime  :=qryProtocoloDT_VENCIMENTO.AsDateTime;
                RXTOTAL.AsFloat             :=qryProtocoloVALOR_TITULO.AsFloat;
                if qryProtocoloDT_PUBLICACAO.AsDateTime<>0 then
                RXDT_PUBLICACAO.AsDateTime  :=qryProtocoloDT_PUBLICACAO.AsDateTime;
                RX.Post;
            end;
            qryProtocolo.Next;
        end;
        Notificacao.Preview;
        Free;

        if GR.Pergunta('Imprimir etiquetas') then
        begin
            I:=TIniFile.Create(ExtractFilePath(Application.ExeName)+'Par.ini');
            P:=I.ReadString('IMPRESSORAS','ETIQUETA','');
            AssignFile(F,P);
            I.Free;
            qryProtocolo.First;
            while not qryProtocolo.Eof do
            begin
                PF.CarregarDevedor(qryProtocoloID_ATO.AsInteger);
                Rewrite(F);
                Writeln(F,'------------------------------------------------------------');
                Writeln(F,'    Nome: '+Copy(PF.RemoverAcento(dm.RxDevedorNOME.AsString),1,50));
                Writeln(F,'Endereco: '+Copy(PF.RemoverAcento(dm.RxDevedorENDERECO.AsString),1,50));
                Writeln(F,'  Cidade: '+PF.RemoverAcento(dm.RxDevedorMUNICIPIO.AsString)+' - '+dm.RxDevedorUF.AsString);
                Writeln(F,'     CEP: '+dm.RxDevedorCEP.AsString);
                Writeln(F,'------------------------------------------------------------');
                CloseFile(F);
                qryProtocolo.Next;
            end;
        end;
        qryProtocolo.RecNo:=Posicao;
        qryProtocolo.EnableControls;
    end;
  except
    on E:Exception do
    begin
        PF.Aguarde(False);
        GR.Aviso('Erro: '+E.Message);
    end;
  end;
end;

procedure TFRelNotificacao.edDataInicialExit(Sender: TObject);
begin
  if edDataFinal.Date<edDataInicial.Date then
    edDataFinal.Date:=edDataInicial.Date;
end;

procedure TFRelNotificacao.Marcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=qryProtocolo.RecNo;
  qryProtocolo.DisableControls;
  qryProtocolo.First;
  while not qryProtocolo.Eof do
  begin
      qryProtocolo.Edit;
      qryProtocoloCheck.AsBoolean:=True;
      qryProtocolo.Post;
      qryProtocolo.Next;
  end;
  qryProtocolo.RecNo:=Posicao;
  qryProtocolo.EnableControls;
end;

procedure TFRelNotificacao.Desmarcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=qryProtocolo.RecNo;
  qryProtocolo.DisableControls;
  qryProtocolo.First;
  while not qryProtocolo.Eof do
  begin
      qryProtocolo.Edit;
      qryProtocoloCheck.AsBoolean:=False;
      qryProtocolo.Post;
      qryProtocolo.Next;
  end;
  qryProtocolo.RecNo:=Posicao;
  qryProtocolo.EnableControls;
end;

procedure TFRelNotificacao.GridAtosMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if X in [11..51] then
    Screen.Cursor:=crHandPoint
      else Screen.Cursor:=crDefault;
end;

procedure TFRelNotificacao.PMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  Screen.Cursor:=crDefault;
end;

end.
