unit URelQtds;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, Mask, sMaskEdit, sCustomComboEdit, sTooledit,
  ExtCtrls, sPanel, Buttons, sBitBtn, DB, DBCtrls, sDBLookupComboBox, SQLExpr,
  sCheckBox, FMTBcd, sLabel, RxMemDS, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFRelQtds = class(TForm)
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    P1: TsPanel;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    ck1: TsCheckBox;
    ck2: TsCheckBox;
    ck3: TsCheckBox;
    btExportar: TsBitBtn;
    lbQ1: TsLabel;
    lbQ2: TsLabel;
    lbQ3: TsLabel;
    RX: TRxMemoryData;
    RXRelatorio: TStringField;
    RXQtd: TIntegerField;
    RXDataInicio: TDateField;
    RXDataFim: TDateField;
    Qtd: TFDQuery;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btSairClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btVisualizarClick(Sender: TObject);
    procedure btExportarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelQtds: TFRelQtds;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFRelQtds.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm.Escreventes.Close;
end;

procedure TFRelQtds.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFRelQtds.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFRelQtds.btVisualizarClick(Sender: TObject);
begin
  if (not ck1.Checked) and (not ck2.Checked) and (not ck3.Checked) then Exit; 
  lbQ1.Caption:='0';
  lbQ2.Caption:='0';
  lbQ3.Caption:='0';
  if ck1.Checked then
  begin
      Qtd.Close;
      Qtd.SQL.Clear;
      Qtd.SQL.Add('SELECT COUNT(ID_ATO) AS QTD FROM TITULOS WHERE DT_PROTOCOLO BETWEEN :D1 AND :D2');
      Qtd.Params.ParamByName('D1').AsDate:=edInicio.Date;
      Qtd.Params.ParamByName('D2').AsDate:=edFim.Date;
      Qtd.Open;
      lbQ1.Caption:=IntToStr(Qtd.FieldByName('QTD').AsInteger);
  end;
  if ck2.Checked then
  begin
      Qtd.Close;
      Qtd.SQL.Clear;
      Qtd.SQL.Add('SELECT COUNT(ID_ATO) AS QTD FROM TITULOS WHERE DT_REGISTRO BETWEEN :D1 AND :D2 AND PROTESTADO='''+'S'+'''');
      Qtd.Params.ParamByName('D1').AsDate:=edInicio.Date;
      Qtd.Params.ParamByName('D2').AsDate:=edFim.Date;
      Qtd.Open;
      lbQ2.Caption:=IntToStr(Qtd.FieldByName('QTD').AsInteger);
  end;
  if ck3.Checked then
  begin
      Qtd.Close;
      Qtd.SQL.Clear;
      Qtd.SQL.Add('SELECT COUNT(ID_ATO) AS QTD FROM TITULOS WHERE DT_PAGAMENTO BETWEEN :D1 AND :D2');
      Qtd.Params.ParamByName('D1').AsDate:=edInicio.Date;
      Qtd.Params.ParamByName('D2').AsDate:=edFim.Date;
      Qtd.Open;
      lbQ3.Caption:=IntToStr(Qtd.FieldByName('QTD').AsInteger);
  end;
end;

procedure TFRelQtds.btExportarClick(Sender: TObject);
begin
  RX.Close;
  RX.Open;
  RX.Append;
  RXRelatorio.AsString    :='T�tulos apresentados no per�odo';
  RXQtd.AsInteger         :=StrToInt(lbQ1.Caption);
  RXDataInicio.AsDateTime :=edInicio.Date;
  RXDataFim.AsDateTime    :=edFim.Date;
  RX.Post;
  RX.Append;
  RXRelatorio.AsString    :='T�tulos protestados';
  RXQtd.AsInteger         :=StrToInt(lbQ2.Caption);
  RXDataInicio.AsDateTime :=edInicio.Date;
  RXDataFim.AsDateTime    :=edFim.Date;
  RX.Post;
  RX.Append;
  RXRelatorio.AsString    :='T�tulos pagos/cancelados at� 20/04/2011';
  RXQtd.AsInteger         :=StrToInt(lbQ3.Caption);
  RXDataInicio.AsDateTime :=edInicio.Date;
  RXDataFim.AsDateTime    :=edFim.Date;
  RX.Post;
  PF.ExportarParaExcel(RX,ExtractFilePath(Application.ExeName),'RelatorioFundos.csv');
end;

end.
