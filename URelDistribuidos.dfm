object FRelDistribuidos: TFRelDistribuidos
  Left = 362
  Top = 231
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'T'#237'tulos Distribu'#237'dos'
  ClientHeight = 266
  ClientWidth = 473
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object btVisualizar: TsBitBtn
    Left = 292
    Top = 233
    Width = 91
    Height = 28
    Cursor = crHandPoint
    Caption = 'Visualizar'
    TabOrder = 0
    OnClick = btVisualizarClick
    ImageIndex = 4
    Images = dm.Imagens
    SkinData.SkinSection = 'BUTTON'
  end
  object btSair: TsBitBtn
    Left = 390
    Top = 233
    Width = 75
    Height = 28
    Cursor = crHandPoint
    Caption = 'Sair'
    TabOrder = 1
    OnClick = btSairClick
    ImageIndex = 7
    Images = dm.Imagens
    SkinData.SkinSection = 'BUTTON'
  end
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 473
    Height = 227
    Align = alTop
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object gbServentiaA: TsGroupBox
      Left = 5
      Top = 116
      Width = 463
      Height = 105
      Caption = 'Serventia Agregada'
      TabOrder = 1
      object lcbServentiaA: TsDBLookupComboBox
        Left = 97
        Top = 73
        Width = 357
        Height = 22
        Color = clWhite
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyField = 'CODIGO_DISTRIBUICAO'
        ListField = 'DESCRICAO'
        ListSource = dsServAg
        ParentFont = False
        TabOrder = 2
      end
      object rbTodas: TsRadioButton
        Left = 10
        Top = 28
        Width = 58
        Height = 18
        Caption = 'Todas'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = rbTodasClick
        OnExit = rbTodasExit
      end
      object rbEspecifica: TsRadioButton
        Left = 10
        Top = 75
        Width = 81
        Height = 18
        Caption = 'Espec'#237'fica:'
        TabOrder = 1
        OnClick = rbEspecificaClick
        OnExit = rbEspecificaExit
      end
    end
    object gbDataPeriodo: TsGroupBox
      Left = 5
      Top = 5
      Width = 463
      Height = 105
      Caption = 'Data/Per'#237'odo'
      TabOrder = 0
      object sLabel1: TsLabel
        Left = 198
        Top = 76
        Width = 6
        Height = 14
        Caption = 'a'
      end
      object dtePeriodoIni: TsDateEdit
        Left = 88
        Top = 73
        Width = 98
        Height = 24
        AutoSize = False
        Color = clWhite
        Enabled = False
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 3
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'In'#237'cio'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        DefaultToday = True
      end
      object dtePeriodoFim: TsDateEdit
        Left = 216
        Top = 73
        Width = 98
        Height = 24
        AutoSize = False
        Color = clWhite
        Enabled = False
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 4
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Fim'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        DefaultToday = True
      end
      object rbData: TsRadioButton
        Left = 10
        Top = 28
        Width = 54
        Height = 18
        Caption = 'Data:'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = rbDataClick
        OnExit = rbDataExit
      end
      object rbPeriodo: TsRadioButton
        Left = 12
        Top = 75
        Width = 70
        Height = 18
        Caption = 'Per'#237'odo:'
        TabOrder = 2
        OnClick = rbPeriodoClick
        OnExit = rbPeriodoExit
      end
      object dteData: TsDateEdit
        Left = 88
        Top = 26
        Width = 98
        Height = 24
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Data'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        DefaultToday = True
      end
    end
  end
  object qryServAg: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT CODIGO_DISTRIBUICAO,'
      '       DESCRICAO'
      '  FROM SERVENTIA_AGREGADA'
      'ORDER BY DESCRICAO')
    Left = 248
    Top = 136
  end
  object dsServAg: TDataSource
    DataSet = qryServAg
    Left = 301
    Top = 136
  end
end
