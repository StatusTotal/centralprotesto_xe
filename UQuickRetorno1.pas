unit UQuickRetorno1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBClient, SimpleDS, QuickRpt, QRCtrls, ExtCtrls, RxMemDS;

type
  TFQuickRetorno1 = class(TForm)
    Relatorio: TQuickRep;
    QRBand1: TQRBand;
    lbData: TQRLabel;
    lbCartorio: TQRLabel;
    lbBanco: TQRLabel;
    QRLabel1: TQRLabel;
    QRBand2: TQRBand;
    ChildBand1: TQRChildBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    lbArquivo: TQRLabel;
    lbTotal: TQRLabel;
    QRLabel8: TQRLabel;
    QRSysData1: TQRSysData;
    procedure RelatorioEndPage(Sender: TCustomQuickRep);
  private
    { Private declarations }
    
  public
    { Public declarations }
  end;

var
  FQuickRetorno1: TFQuickRetorno1;

implementation

uses UDM, UPF, URelRetorno;

{$R *.dfm}

procedure TFQuickRetorno1.RelatorioEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

end.
