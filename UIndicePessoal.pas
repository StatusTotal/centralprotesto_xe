unit UIndicePessoal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sGroupBox, Buttons, sBitBtn, sEdit, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, ExtCtrls, sPanel, sCurrEdit, sCurrencyEdit;

type
  TFIndicePessoal = class(TForm)
    P1: TsPanel;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    RgTipo: TsRadioGroup;
    RgStatus: TsRadioGroup;
    edFolha: TsCurrencyEdit;
    procedure btSairClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btVisualizarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FIndicePessoal: TFIndicePessoal;

implementation

uses UQuickIndice1, DB, UPF;

{$R *.dfm}

procedure TFIndicePessoal.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFIndicePessoal.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFIndicePessoal.btVisualizarClick(Sender: TObject);
begin
  PF.Aguarde(True);
  Application.CreateForm(TFQuickIndice1,FQuickIndice1);
  with FQuickIndice1 do
  begin
      if edFolha.Value<>0 then
      begin
          vFolha:=edFolha.AsInteger;
          vInicial:=vFolha;
          lbFolha.Enabled:=True;
      end
      else
        lbFolha.Enabled:=False;

      qryIndice.Close;
      if RgStatus.ItemIndex=0 then
        qryIndice.SQL.Text:='SELECT ID_ATO,DT_PROTOCOLO,PROTOCOLO,LIVRO_PROTOCOLO,FOLHA_PROTOCOLO,APRESENTANTE,LIVRO_REGISTRO,'+
                                       'FOLHA_REGISTRO,DT_REGISTRO,DT_PAGAMENTO,CEDENTE,SACADOR,DEVEDOR,STATUS FROM TITULOS WHERE ((DT_REGISTRO BETWEEN :D1 AND :D2) OR (DT_PAGAMENTO BETWEEN :D1 AND :D2)) AND ('+
                                       'STATUS='''+'PROTESTADO'+''' OR STATUS='''+'CANCELADO'+''')'
      else if RgStatus.ItemIndex=1 then
        qryIndice.SQL.Text:='SELECT ID_ATO,DT_PROTOCOLO,PROTOCOLO,LIVRO_PROTOCOLO,FOLHA_PROTOCOLO,APRESENTANTE,LIVRO_REGISTRO,'+
                                       'FOLHA_REGISTRO,DT_REGISTRO,DT_PAGAMENTO,CEDENTE,SACADOR,DEVEDOR,STATUS FROM TITULOS WHERE DT_PAGAMENTO BETWEEN :D1 AND :D2 AND '+
                                       'STATUS='''+'CANCELADO'+''''
      else
        qryIndice.SQL.Text:='SELECT ID_ATO,DT_PROTOCOLO,PROTOCOLO,LIVRO_PROTOCOLO,FOLHA_PROTOCOLO,APRESENTANTE,LIVRO_REGISTRO,'+
                                       'FOLHA_REGISTRO,DT_REGISTRO,DT_PAGAMENTO,CEDENTE,SACADOR,DEVEDOR,STATUS FROM TITULOS WHERE DT_REGISTRO BETWEEN :D1 AND :D2 AND '+
                                       'STATUS='''+'PROTESTADO'+'''';

      qryIndice.ParamByName('D1').AsDate:=edInicio.Date;
      qryIndice.ParamByName('D2').AsDate:=edFim.Date;
      qryIndice.Open;
      qryIndice.First;

      RX.Close;
      RX.Open;
      while not qryIndice.Eof do
      begin
          {TODOS}
          if RgTipo.ItemIndex=0 then
          begin
              {APRESENTANTE}
              if qryIndiceAPRESENTANTE.AsString<>'' then
              begin
                  RX.Append;
                  RXNome.AsString           :=qryIndiceAPRESENTANTE.AsString;
                  RXQualificacao.AsString   :='Portador';
                  RXLivro.AsString          :=qryIndiceLIVRO_REGISTRO.AsString;
                  RXFolha.AsString          :=qryIndiceFOLHA_REGISTRO.AsString;
                  RXProtocolo.AsString      :=qryIndicePROTOCOLO.AsString;
                  RXSituacao.AsString       :=qryIndiceSTATUS.AsString;
                  if qryIndiceSTATUS.AsString='CANCELADO' then
                    RXData.AsDateTime:=qryIndiceDT_PAGAMENTO.AsDateTime
                      else RXData.AsDateTime:=qryIndiceDT_REGISTRO.AsDateTime;
                  RX.Post;
              end;
              {DEVEDOR}
              if qryIndiceDEVEDOR.AsString<>'' then
              begin
                  RX.Append;
                  RXNome.AsString           :=qryIndiceDEVEDOR.AsString;
                  RXQualificacao.AsString   :='Devedor';
                  RXLivro.AsString          :=qryIndiceLIVRO_REGISTRO.AsString;
                  RXFolha.AsString          :=qryIndiceFOLHA_REGISTRO.AsString;
                  RXProtocolo.AsString      :=qryIndicePROTOCOLO.AsString;
                  RXSituacao.AsString       :=qryIndiceSTATUS.AsString;
                  if qryIndiceSTATUS.AsString='CANCELADO' then
                    RXData.AsDateTime:=qryIndiceDT_PAGAMENTO.AsDateTime
                      else RXData.AsDateTime:=qryIndiceDT_REGISTRO.AsDateTime;
                  RX.Post;
              end;
              {SACADOR}
              if qryIndiceSACADOR.AsString<>'' then
              begin
                  RX.Append;
                  RXNome.AsString           :=qryIndiceSACADOR.AsString;
                  RXQualificacao.AsString   :='Sacador';
                  RXLivro.AsString          :=qryIndiceLIVRO_REGISTRO.AsString;
                  RXFolha.AsString          :=qryIndiceFOLHA_REGISTRO.AsString;
                  RXProtocolo.AsString      :=qryIndicePROTOCOLO.AsString;
                  RXSituacao.AsString       :=qryIndiceSTATUS.AsString;
                  if qryIndiceSTATUS.AsString='CANCELADO' then
                    RXData.AsDateTime:=qryIndiceDT_PAGAMENTO.AsDateTime
                      else RXData.AsDateTime:=qryIndiceDT_REGISTRO.AsDateTime;
                  RX.Post;
              end;
              {CEDENTE}
              if qryIndiceCEDENTE.AsString<>'' then
              begin
                  RX.Append;
                  RXNome.AsString           :=qryIndiceCEDENTE.AsString;
                  RXQualificacao.AsString   :='Cedente';
                  RXLivro.AsString          :=qryIndiceLIVRO_REGISTRO.AsString;
                  RXFolha.AsString          :=qryIndiceFOLHA_REGISTRO.AsString;
                  RXProtocolo.AsString      :=qryIndicePROTOCOLO.AsString;
                  RXSituacao.AsString       :=qryIndiceSTATUS.AsString;
                  if qryIndiceSTATUS.AsString='CANCELADO' then
                    RXData.AsDateTime:=qryIndiceDT_PAGAMENTO.AsDateTime
                      else RXData.AsDateTime:=qryIndiceDT_REGISTRO.AsDateTime;
                  RX.Post;
              end;
          end;

          {PORTADOR}
          if RgTipo.ItemIndex=1 then
          begin
              if qryIndiceAPRESENTANTE.AsString<>'' then
              begin
                  RX.Append;
                  RXNome.AsString           :=qryIndiceAPRESENTANTE.AsString;
                  RXQualificacao.AsString   :='Portador';
                  RXLivro.AsString          :=qryIndiceLIVRO_REGISTRO.AsString;
                  RXFolha.AsString          :=qryIndiceFOLHA_REGISTRO.AsString;
                  RXProtocolo.AsString      :=qryIndicePROTOCOLO.AsString;
                  RXSituacao.AsString       :=qryIndiceSTATUS.AsString;
                  if qryIndiceSTATUS.AsString='CANCELADO' then
                    RXData.AsDateTime:=qryIndiceDT_PAGAMENTO.AsDateTime
                      else RXData.AsDateTime:=qryIndiceDT_REGISTRO.AsDateTime;
                  RX.Post;
              end;
          end;

          {DEVEDOR}
          if RgTipo.ItemIndex=2 then
          begin
              if qryIndiceDEVEDOR.AsString<>'' then
              begin
                  RX.Append;
                  RXNome.AsString           :=qryIndiceDEVEDOR.AsString;
                  RXQualificacao.AsString   :='Devedor';
                  RXLivro.AsString          :=qryIndiceLIVRO_REGISTRO.AsString;
                  RXFolha.AsString          :=qryIndiceFOLHA_REGISTRO.AsString;
                  RXProtocolo.AsString      :=qryIndicePROTOCOLO.AsString;
                  RXSituacao.AsString       :=qryIndiceSTATUS.AsString;
                  if qryIndiceSTATUS.AsString='CANCELADO' then
                    RXData.AsDateTime:=qryIndiceDT_PAGAMENTO.AsDateTime
                      else RXData.AsDateTime:=qryIndiceDT_REGISTRO.AsDateTime;
                  RX.Post;
              end;
          end;

          {SACADOR}
          if RgTipo.ItemIndex=3 then
          begin
              if qryIndiceSACADOR.AsString<>'' then
              begin
                  RX.Append;
                  RXNome.AsString           :=qryIndiceSACADOR.AsString;
                  RXQualificacao.AsString   :='Sacador';
                  RXLivro.AsString          :=qryIndiceLIVRO_REGISTRO.AsString;
                  RXFolha.AsString          :=qryIndiceFOLHA_REGISTRO.AsString;
                  RXProtocolo.AsString      :=qryIndicePROTOCOLO.AsString;
                  RXSituacao.AsString       :=qryIndiceSTATUS.AsString;
                  if qryIndiceSTATUS.AsString='CANCELADO' then
                    RXData.AsDateTime:=qryIndiceDT_PAGAMENTO.AsDateTime
                      else RXData.AsDateTime:=qryIndiceDT_REGISTRO.AsDateTime;
                  RX.Post;
              end;
          end;

          {CEDENTE}
          if RgTipo.ItemIndex=4 then
          begin
              if qryIndiceCEDENTE.AsString<>'' then
              begin
                  RX.Append;
                  RXNome.AsString           :=qryIndiceCEDENTE.AsString;
                  RXQualificacao.AsString   :='Cedente';
                  RXLivro.AsString          :=qryIndiceLIVRO_REGISTRO.AsString;
                  RXFolha.AsString          :=qryIndiceFOLHA_REGISTRO.AsString;
                  RXProtocolo.AsString      :=qryIndicePROTOCOLO.AsString;
                  RXSituacao.AsString       :=qryIndiceSTATUS.AsString;
                  if qryIndiceSTATUS.AsString='CANCELADO' then
                    RXData.AsDateTime:=qryIndiceDT_PAGAMENTO.AsDateTime
                      else RXData.AsDateTime:=qryIndiceDT_REGISTRO.AsDateTime;
                  RX.Post;
              end;
          end;

          qryIndice.Next;
      end;
      RX.SortOnFields('NOME');
      Relatorio.Preview;
      Free;
  end;
end;

end.
