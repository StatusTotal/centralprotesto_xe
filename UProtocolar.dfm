�
 TFPROTOCOLAR 0�%  TPF0TFProtocolarFProtocolarLeft"Top� BorderStylebsDialogCaption   Entradas de TítulosClientHeight)ClientWidthColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
KeyPreview	OldCreateOrderPositionpoMainFormCenterOnClick	FormClickOnCreate
FormCreate	OnKeyDownFormKeyDownPixelsPerInch`
TextHeight TsPanelP2Left Top�WidthHeight+AlignalBottomTabOrder SkinData.SkinSection
TOOLBUTTON TsBitBtn	btAlterarLeft�TopWidth[Height$CursorcrHandPointCaptionApontarFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrder OnClickbtAlterarClick
ImageIndexImages
dm.ImagensSkinData.SkinSectionBUTTON  TsBitBtn	btExcluirLeftATopWidthMHeight$CursorcrHandPointCaptionExcluirFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderOnClickbtExcluirClick
ImageIndex	Images
dm.ImagensSkinData.SkinSectionBUTTON  TsBitBtnbtReciboLeft�TopWidthwHeight$CursorcrHandPointCaption   2ª Via ReciboFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
Glyph.Data
�   �   BM�       v   (               p                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333      30wwwwww0������0������3      330����330����330����330����330����330����330    333333333
ParentFontTabOrderOnClickbtReciboClickSkinData.SkinSectionBUTTON  TsEditedProtocoloLeftJTop
WidthXHeightColor��� Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.StylefsBold 
ParentFontTabOrderSkinData.SkinSectionMAINMENUBoundLabel.Active	BoundLabel.ParentFontBoundLabel.Caption	ProtocoloBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.StylefsBold   
TsDateEditedDataProtocoloLeftITop
WidthhHeightAutoSizeColor��� EditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.StylefsBold 	MaxLength

ParentFontTabOrderBoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionData do ApontamentoBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.StylefsBold SkinData.SkinSectionMAINMENUGlyphMode.Blend GlyphMode.GrayedDefaultToday	   TsPanelP3Left TopTWidthHeight� AlignalClientFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderVisibleSkinData.SkinSectionTOOLBAR TLabellbReciboLeft&Top?Width� HeightAutoSizeCaptionRecibo: 10000Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold 
ParentFontTransparent	  TLabellbTipoLeft&Top+WidthMHeightCaption?   Espécie: Duplicata de Prestação de Serviços por IndicaçãoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTransparent	  TLabellbTituloLeft1TopTWidth� HeightAutoSizeCaption   Título: 009098878877Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTransparent	  TLabel	lbEmissaoLeft?TopTWidthzHeightCaption   Emissão: 00/00/0000Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTransparent	  TLabellbVencimentoLeft8TopTWidth� HeightCaptionVencimento: 00/00/0000Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTransparent	  TLabellbValorLeft3TophWidth#HeightCaptionValor:Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTransparent	  TLabellbCustasLeftHTophWidth+HeightCaptionCustas:Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTransparent	  TLabellbSaldoLeft[TophWidth%HeightCaptionSaldo:Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTransparent	  TLabel	lbSacadorLeft"Top{Width4HeightCaptionSacador:Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTransparent	  TLabel	lbCedenteLeft"Top� Width4HeightCaptionCedente:Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTransparent	  TLabellbStatusLeftTop
Width�Height	AlignmenttaCenterAutoSizeCaptionA.R. EM 00/00/0000Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsUnderline 
ParentFontTransparent	  TLabellbDataEntradaLeftOTop?WidthxHeightCaptionData: 00/00/0000Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold 
ParentFontTransparent	   	TwwDBGridGridAtosLeft Top3WidthHeight!ControlType.StringsProtocolar;CheckBox;S;N Selected.StringsProtocolar	3	*	FDT_ENTRADA	11	Entrada	FRECIBO	13	Recibo	FAPRESENTANTE	37	Portador	FDEVEDOR	41	Devedor	F IniAttributes.Delimiter;;IniAttributes.UnicodeIniFile
TitleColor	clBtnFace	FixedColsShowHorzScrollBar	EditControlOptionsecoCheckboxSingleClickecoSearchOwnerFormecoDisableDateTimePicker AlignalTopBorderStylebsNone
DataSource
dsConsultaEditCalculated	Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style Options	dgEditingdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit
dgWordWrapdgProportionalColResize 
ParentFont	PopupMenuPMTabOrderTitleAlignmenttaCenterTitleFont.CharsetANSI_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameTahomaTitleFont.Style 
TitleLinesTitleButtons	
UseTFieldsOnTitleButtonClickGridAtosTitleButtonClickOnMouseMoveGridAtosMouseMove  TsPanelP1Left Top WidthHeight3AlignalTopFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderSkinData.SkinSectionPANEL 
TsComboBoxcbBuscarLeftTopWidth� Height	AlignmenttaLeftJustifyBoundLabel.ParentFontBoundLabel.Font.CharsetDEFAULT_CHARSETBoundLabel.Font.ColorclWindowTextBoundLabel.Font.Height�BoundLabel.Font.NameMS Sans SerifBoundLabel.Font.Style SkinData.SkinSectionCOMBOBOXVerticalAlignment
taAlignTopStylecsDropDownListColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 	ItemIndex�
ParentFontTabOrder OnChangecbBuscarChangeItems.StringsRECIBOPORTADORDEVEDOR	DOCUMENTOSACADORCEDENTE   Nº TÍTULODATA DE ENTRADA   TsEditedPesquisarLeft� TopWidth�HeightAutoSizeCharCaseecUpperCaseColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrder
OnKeyPressedPesquisarKeyPressSkinData.SkinSectionEDITBoundLabel.ParentFontBoundLabel.Font.CharsetDEFAULT_CHARSETBoundLabel.Font.ColorclWindowTextBoundLabel.Font.Height�BoundLabel.Font.NameMS Sans SerifBoundLabel.Font.Style   
TsDateEditedDataLeft�TopWidthiHeightHint8DATA DE ENTRADA (PRESSIONE [ENTER] PARA EFETUAR A BUSCA)AutoSizeColor��� EditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.StylefsBold 	MaxLength

ParentFontParentShowHintShowHint	TabOrder
OnKeyPressedDataKeyPressBoundLabel.ParentFontBoundLabel.Font.CharsetDEFAULT_CHARSETBoundLabel.Font.ColorclWindowTextBoundLabel.Font.Height�BoundLabel.Font.NameMS Sans SerifBoundLabel.Font.Style SkinData.SkinSectionMAINMENUGlyphMode.Blend GlyphMode.GrayedGlyphMode.Hint8DATA DE ENTRADA (PRESSIONE [ENTER] PARA EFETUAR A BUSCA)DefaultToday	WeekendsdowSaturday	dowSunday    TDataSource	dsTitulosDataSet
dm.TitulosLeft0Top�   TTimerTimerIntervalOnTimer
TimerTimerLeftpTop�   TDataSetProviderdspConsultaDataSetqryConsultaOptionspoAllowCommandText LeftpTopy  TClientDataSetConsulta
Aggregates Params ProviderNamedspConsulta	AfterOpenConsultaAfterOpenLeft� Topy TIntegerFieldConsultaID_ATO	FieldNameID_ATORequired	  
TDateFieldConsultaDT_ENTRADA	FieldName
DT_ENTRADA  TIntegerFieldConsultaRECIBO	FieldNameRECIBO  TStringFieldConsultaAPRESENTANTE	FieldNameAPRESENTANTESized  TStringFieldConsultaDEVEDOR	FieldNameDEVEDORSized  TStringFieldConsultaSTATUS	FieldNameSTATUS  TStringFieldConsultaProtocolar	FieldKindfkInternalCalc	FieldName
ProtocolarSize   TDataSource
dsConsultaDataSetConsultaOnDataChangedsConsultaDataChangeLeft� Topy  
TPopupMenuPMLeft� Top�  	TMenuItemMarcarTodos1CaptionMarcar TodosOnClickMarcarTodos1Click  	TMenuItemDesmarcarTodos1CaptionDesmarcar TodosOnClickDesmarcarTodos1Click  	TMenuItemN1Caption-  	TMenuItemCustas1CaptionCustasOnClickCustas1Click   TFDQueryqryConsulta
Connectiondm.conSISTEMASQL.Stringsselect     ID_ATO,DT_ENTRADA,RECIBO,APRESENTANTE,DEVEDOR,STATUS    from TITULOS    order by PROTOCOLO Left0Topx   