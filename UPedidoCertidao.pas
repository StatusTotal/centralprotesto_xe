unit UPedidoCertidao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, sCurrEdit, sDBCalcEdit, Mask,
  sMaskEdit, sCustomComboEdit, sTooledit, sDBDateEdit, DBCtrls, sDBEdit,
  RxDBComb, acPNG, ExtCtrls, sLabel, sPanel, DB, sCheckBox, sDBCheckBox,
  wwdbedit, Wwdbspin, Grids, DBGrids, sRadioButton, acImage, SQLExpr;

type
  TFPedidoCertidao = class(TForm)
    dsCertidao: TDataSource;
    dsEscreventes: TDataSource;
    P1: TsPanel;
    sLabel1: TsLabel;
    sLabel3: TsLabel;
    sLabel2: TsLabel;
    ImAviso1: TImage;
    ImOk1: TImage;
    ImAviso2: TImage;
    ImOk2: TImage;
    CbTipoCertidao: TRxDBComboBox;
    edDocumento1: TsDBEdit;
    edRequerido: TsDBEdit;
    edRecibo: TsDBEdit;
    cbCobranca: TRxDBComboBox;
    edDataPedido: TsDBDateEdit;
    edDocumento2: TsDBEdit;
    edDataPrazo: TsDBDateEdit;
    edDevedor: TsDBEdit;
    cbTipoRequerido: TRxDBComboBox;
    edTotal: TsDBCalcEdit;
    btCustas: TsBitBtn;
    edTelefone: TsDBEdit;
    btSalvar: TsBitBtn;
    btCancelar: TsBitBtn;
    spAnos: TwwDBSpinEdit;
    lbAnos: TsLabel;
    DataSource1: TDataSource;
    ckImprimir: TsCheckBox;
    ImIgual: TsImage;
    sDBCheckBox1: TsDBCheckBox;
    procedure btSalvarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure edDataPedidoExit(Sender: TObject);
    procedure Custas(Codigo: Integer);
    procedure btCustasClick(Sender: TObject);
    procedure edDocumento2Change(Sender: TObject);
    procedure edDocumento2Enter(Sender: TObject);
    procedure edDocumento2Exit(Sender: TObject);
    procedure cbTipoRequeridoChange(Sender: TObject);
    procedure edDocumento1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure spAnosKeyPress(Sender: TObject; var Key: Char);
    procedure spAnosChange(Sender: TObject);
    procedure cbCobrancaChange(Sender: TObject);
    procedure edDocumento1Enter(Sender: TObject);
    procedure edDocumento1Exit(Sender: TObject);
    procedure ImIgualClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPedidoCertidao: TFPedidoCertidao;

implementation

uses UDM, UPF, UQuickPedido1, UCustas, DateUtils, UGeral, UGDM;

{$R *.dfm}

procedure TFPedidoCertidao.btSalvarClick(Sender: TObject);
var
  TD: TTransactionDesc;
  Imprime: Boolean;
begin
  if edDevedor.Text='' then
  begin
      GR.Aviso('INFORME O NOME DO REQUERENTE!');
      edDevedor.SetFocus;
      Exit;
  end;

  if Trim(edDocumento1.Text)='' then
  begin
      GR.Aviso('INFORME O DOCUMENTO DO REQUERIDO!');
      edDocumento1.SetFocus;
      Exit;
  end;

  if edRequerido.Text='' then
  begin
      GR.Aviso('INFORME O NOME DO REQUERIDO!');
      edRequerido.SetFocus;
      Exit;
  end;

  try
    if dm.conSISTEMA.Connected then
      dm.conSISTEMA.StartTransaction;

    Imprime:=False;

    if dm.Certidoes.State=dsInsert then
    begin
        dm.CertidoesID_CERTIDAO.AsInteger:=dm.IdAtual('ID_ATO','S');

        if StrToInt(dm.ValorParametro(14))<>0 then
        begin
            dm.CertidoesRECIBO.AsInteger:=StrToInt(dm.ValorParametro(14));
            dm.AtualizarParametro(14,IntToStr(dm.CertidoesRECIBO.AsInteger+1));
        end
        else
        begin
            dm.CertidoesRECIBO.AsInteger:=StrToInt(dm.ValorParametro(11));
            dm.AtualizarParametro(11,IntToStr(dm.CertidoesRECIBO.AsInteger+1));
        end;

        Imprime:=True;
    end;

    dm.CertidoesEMOLUMENTOS.AsFloat :=dm.vEmolumentos;
    dm.CertidoesFETJ.AsFloat        :=dm.vFetj;
    dm.CertidoesFUNDPERJ.AsFloat    :=dm.vFundperj;
    dm.CertidoesFUNPERJ.AsFloat     :=dm.vFunperj;
    dm.CertidoesFUNARPEN.AsFloat    :=dm.vFunarpen;
    dm.CertidoesPMCMV.AsFloat       :=dm.vPmcmv;
    dm.CertidoesISS.AsFloat         :=dm.vIss;
    dm.CertidoesAPONTAMENTO.AsFloat :=dm.vApontamento;
    dm.CertidoesTOTAL.AsFloat       :=dm.vTotal;

    if dm.CertidoesCONVENIO.AsString='S' then
    dm.CertidoesCODIGO.AsInteger:=4038;

    dm.Certidoes.Post;
    if dm.Certidoes.ApplyUpdates(0)>0 then Raise Exception.Create('Erro: ApplyUpdates dm.Certidoes');

   {GRAVANDO AS CUSTAS}
    if not dm.RxCustas.IsEmpty then
    begin
        dm.RxCustas.First;
        while not dm.RxCustas.Eof do
        begin
            dm.Custas.Close;
            dm.Custas.Params[0].AsInteger:=dm.RxCustasID_CUSTA.AsInteger;
            dm.Custas.Open;
            if dm.Custas.IsEmpty then
            begin
                dm.Custas.Append;
                dm.CustasID_CUSTA.AsInteger:=dm.IdAtual('ID_CUSTA','S');
            end
            else
              dm.Custas.Edit;

            dm.CustasID_ATO.AsInteger     :=dm.CertidoesID_CERTIDAO.AsInteger;
            dm.CustasTABELA.AsString      :=dm.RxCustasTABELA.AsString;
            dm.CustasITEM.AsString        :=dm.RxCustasITEM.AsString;
            dm.CustasSUBITEM.AsString     :=dm.RxCustasSUBITEM.AsString;
            dm.CustasVALOR.AsFloat        :=dm.RxCustasVALOR.AsFloat;
            dm.CustasQTD.AsInteger        :=dm.RxCustasQTD.AsInteger;
            dm.CustasTOTAL.AsFloat        :=dm.RxCustasTOTAL.AsFloat;
            dm.Custas.Post;
            if dm.Custas.ApplyUpdates(0)>0 then Raise Exception.Create('Erro: ApplyUpdates dm.Custas');

            dm.RxCustas.Next;
        end;
    end;

    if dm.conSISTEMA.InTransaction then
      dm.conSISTEMA.Commit;

    if not ckImprimir.Checked then
      if Imprime then
        if dm.ValorParametro(42)='S' then
          dm.ReciboMatricial(dm.CertidoesID_CERTIDAO.AsInteger,dm.CertidoesTIPO_CERTIDAO.AsString)
            else Application.CreateForm(TFQuickPedido1,FQuickPedido1);

    dm.vOkGeral:=True;
  except
    on E: Exception do
    begin
      if dm.conSISTEMA.InTransaction then
        dm.conSISTEMA.Rollback;

      GR.Aviso('N�o foi poss�vel gerar o pedido.'+#13#13+E.Message);
    end;
  end;

  Close;
end;

procedure TFPedidoCertidao.btCancelarClick(Sender: TObject);
begin
  dm.vOkGeral:=False;
  dm.Certidoes.Cancel;
  Close;
end;

procedure TFPedidoCertidao.edDataPedidoExit(Sender: TObject);
begin
  dm.CertidoesDT_ENTREGA.AsDateTime:=PF.DataFinalFeriados(edDataPedido.Date,StrToInt(dm.ValorParametro(12)));
end;

procedure TFPedidoCertidao.btCustasClick(Sender: TObject);
begin
  dm.Tabela.Close;
  dm.Tabela.Open;
  GR.CriarForm(TFCustas,FCustas);
  dm.Tabela.Close;
  edTotal.Value:=dm.vTotal;
end;

procedure TFPedidoCertidao.edDocumento2Change(Sender: TObject);
begin
  if GR.PegarNumeroTexto(edDocumento2.Text)='' then
  begin
      ImAviso2.Visible:=True;
      ImOk2.Visible   :=False;
      Exit;
  end;

  ImAviso2.Visible:=not PF.VerificarCPF(GR.PegarNumeroTexto(edDocumento2.Text));

  ImOk2.Visible:=not ImAviso2.Visible;
end;

procedure TFPedidoCertidao.edDocumento2Enter(Sender: TObject);
begin
  dm.CertidoesCPF_CNPJ_REQUERENTE.EditMask:='';
  dm.CertidoesCPF_CNPJ_REQUERENTE.AsString:=GR.PegarNumeroTexto(dm.CertidoesCPF_CNPJ_REQUERENTE.AsString);
end;

procedure TFPedidoCertidao.edDocumento2Exit(Sender: TObject);
begin
  if dm.CertidoesCPF_CNPJ_REQUERENTE.AsString='' then
  begin
      edDocumento2.BoundLabel.Caption           :='Documento';
      dm.CertidoesCPF_CNPJ_REQUERENTE.EditMask  :='';
      ImAviso2.Visible:=True;
      ImOk2.Visible   :=False;
      Exit;
  end;

  if Length(dm.CertidoesCPF_CNPJ_REQUERENTE.AsString)<=11 then
  begin
      edDocumento2.BoundLabel.Caption           :='CPF';
      dm.CertidoesCPF_CNPJ_REQUERENTE.EditMask  :='999.999.999-99;0;_';
  end
  else
  begin
      edDocumento2.BoundLabel.Caption           :='CNPJ';
      dm.CertidoesCPF_CNPJ_REQUERENTE.EditMask  :='99.999.999/9999-99;0;_';
  end;
end;

procedure TFPedidoCertidao.cbTipoRequeridoChange(Sender: TObject);
begin
  if CbTipoRequerido.ItemIndex=0 then
  begin
      edDocumento1.BoundLabel.Caption         :='CPF';
      dm.CertidoesCPF_CNPJ_REQUERIDO.EditMask :='999.999.999-99;0;_';
      dm.CertidoesTIPO_REQUERIDO.AsString     :='F';
  end
  else
  begin
      edDocumento1.BoundLabel.Caption         :='CNPJ';
      dm.CertidoesCPF_CNPJ_REQUERIDO.EditMask :='99.999.999/9999-99;0;_';
      dm.CertidoesTIPO_REQUERIDO.AsString     :='J';
  end;
end;

procedure TFPedidoCertidao.edDocumento1Change(Sender: TObject);
begin
  if GR.PegarNumeroTexto(edDocumento1.Text)='' then
  begin
      ImAviso1.Visible:=True;
      ImOk1.Visible   :=False;
      Exit;
  end;

  if dm.CertidoesTIPO_REQUERIDO.AsString='F' then
    ImAviso1.Visible:= not PF.VerificarCPF(GR.PegarNumeroTexto(edDocumento1.Text))
      else ImAviso1.Visible:= not PF.VerificarCNPJ(GR.PegarNumeroTexto(edDocumento1.Text));

  ImOk1.Visible:=not ImAviso1.Visible;
end;

procedure TFPedidoCertidao.FormCreate(Sender: TObject);
begin
  dm.Escreventes.Close;
  dm.Escreventes.Open;

  CbTipoRequeridoChange(Sender);
  
  edDocumento1Exit(Sender);
  edDocumento2Exit(Sender);

  if dm.CertidoesCODIGO.AsInteger=4022 then
  begin
      spAnosChange(Sender);
      if dm.CertidoesTIPO_CERTIDAO.AsString='F' then
      begin
          lbAnos.Visible:=False;
          spAnos.Visible:=False;
      end;
  end
  else
  begin
      lbAnos.Visible:=False;
      spAnos.Visible:=False;
      if dm.Certidoes.State=dsInsert then
      Custas(4010);
  end;

  if dm.Certidoes.State=dsEdit then
  begin
      PF.CarregarCustas(dm.CertidoesID_CERTIDAO.AsInteger);
      dm.vEmolumentos :=dm.CertidoesEMOLUMENTOS.AsFloat;
      dm.vFetj        :=dm.CertidoesFETJ.AsFloat;
      dm.vFundperj    :=dm.CertidoesFUNDPERJ.AsFloat;
      dm.vFunperj     :=dm.CertidoesFUNPERJ.AsFloat;
      dm.vFunarpen    :=dm.CertidoesFUNARPEN.AsFloat;
      dm.vPmcmv       :=dm.CertidoesPMCMV.AsFloat;
      dm.vIss         :=dm.CertidoesISS.AsFloat;
      dm.vApontamento :=dm.CertidoesAPONTAMENTO.AsFloat;
      dm.vMutua       :=dm.CertidoesMUTUA.AsFloat;
      dm.vAcoterj     :=dm.CertidoesACOTERJ.AsFloat;
      dm.vTotal       :=dm.CertidoesTOTAL.AsFloat;
  end;
end;

procedure TFPedidoCertidao.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then btCancelarClick(Sender);
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFPedidoCertidao.spAnosKeyPress(Sender: TObject; var Key: Char);
begin
  key:=#0;
end;

procedure TFPedidoCertidao.spAnosChange(Sender: TObject);
begin
  if dm.Certidoes.State=dsInsert then
    if dm.CertidoesTIPO_CERTIDAO.AsString='X' then
      Custas(4010)
        else Custas(4022);
end;

procedure TFPedidoCertidao.cbCobrancaChange(Sender: TObject);
begin
  spAnosChange(Sender);
end;

procedure TFPedidoCertidao.edDocumento1Enter(Sender: TObject);
begin
  dm.CertidoesCPF_CNPJ_REQUERIDO.EditMask:='';
  dm.CertidoesCPF_CNPJ_REQUERIDO.AsString:=GR.PegarNumeroTexto(dm.CertidoesCPF_CNPJ_REQUERIDO.AsString);
end;

procedure TFPedidoCertidao.edDocumento1Exit(Sender: TObject);
begin
  if dm.CertidoesCPF_CNPJ_REQUERIDO.AsString='' then
  begin
      edDocumento1.BoundLabel.Caption           :='Documento';
      dm.CertidoesCPF_CNPJ_REQUERENTE.EditMask  :='';
      ImAviso1.Visible:=True;
      ImOk1.Visible   :=False;
      Exit;
  end;

  if Length(dm.CertidoesCPF_CNPJ_REQUERIDO.AsString)<=11 then
  begin
      edDocumento1.BoundLabel.Caption           :='CPF';
      dm.CertidoesCPF_CNPJ_REQUERIDO.EditMask   :='999.999.999-99;0;_';
  end
  else
  begin
      edDocumento1.BoundLabel.Caption           :='CNPJ';
      dm.CertidoesCPF_CNPJ_REQUERIDO.EditMask   :='99.999.999/9999-99;0;_';
  end;
end;

procedure TFPedidoCertidao.Custas(Codigo: Integer);
var
  Soma,Qtd,Comuns: Double;
begin
  if not (dm.Certidoes.State in [dsEdit,dsInsert]) then Exit;

  dm.RxCustas.Close;
  dm.RxCustas.Open;

  if (cbCobranca.ItemIndex=0) or (cbCobranca.ItemIndex=2) or (cbCobranca.ItemIndex=4) then
  begin
      dm.CertidoesEMOLUMENTOS.AsFloat   :=0;
      dm.CertidoesFETJ.AsFloat          :=0;
      dm.CertidoesFUNDPERJ.AsFloat      :=0;
      dm.CertidoesFUNPERJ.AsFloat       :=0;
      dm.CertidoesFUNARPEN.AsFloat      :=0;
      dm.CertidoesPMCMV.AsFloat         :=0;
      dm.CertidoesISS.AsFloat           :=0;
      dm.CertidoesMUTUA.AsFloat         :=0;
      dm.CertidoesACOTERJ.AsFloat       :=0;
      dm.CertidoesDISTRIBUICAO.AsFloat  :=0;
      dm.CertidoesTOTAL.AsFloat         :=0;
      dm.vEmolumentos                   :=0;
      dm.vFetj                          :=0;
      dm.vFundperj                      :=0;
      dm.vFunperj                       :=0;
      dm.vFunarpen                      :=0;
      dm.vPmcmv                         :=0;
      dm.vIss                           :=0;
      dm.vMutua                         :=0;
      dm.vAcoterj                       :=0;
      dm.vDistribuicao                  :=0;
      dm.vAAR                           :=0;
      dm.vApontamento                   :=0;
      dm.vTotal                         :=0;
      edTotal.Value                     :=0;
      Exit;
  end;

  {SE FOR CERTID�O ESPEC�FICA (2�VIA) EU ADICIONO SEMPRE APENAS O ITEM 16.2.*}
  {SE A PESSOA QUISER OUTRO ELA DEVE ADICIONAR NA TELA DE CUSTAS}
  if dm.CertidoesTIPO_CERTIDAO.AsString='F' then
  begin
      dm.Subitem.Close;
      dm.Subitem.Params[0].AsString :='16';
      dm.Subitem.Params[1].AsString :='2';
      dm.Subitem.Params[2].AsString :='*';
      dm.Subitem.Params[3].AsInteger:=StrToInt(dm.ValorParametro(77));
      dm.Subitem.Open;

      dm.RxCustas.Append;
      dm.RxCustasTABELA.AsString    :=dm.SubitemTAB.AsString;
      dm.RxCustasITEM.AsString      :=dm.SubitemITEM.AsString;
      dm.RxCustasSUBITEM.AsString   :=dm.SubitemSUB.AsString;
      dm.RxCustasVALOR.AsFloat      :=dm.SubitemVALOR.AsFloat;
      dm.RxCustasQTD.AsInteger      :=1;
      dm.RxCustasTOTAL.AsFloat      :=dm.SubitemVALOR.AsFloat;
      dm.RxCustasDESCRICAO.AsString :=dm.SubitemDESCR.AsString;
      dm.RxCustas.Post;
  end
  else
  begin
      dm.Codigos.Close;
      dm.Codigos.Params.ParamByName('OCULTO1').AsString:='N';
      dm.Codigos.Params.ParamByName('OCULTO2').AsString:='N';
      dm.Codigos.Open;
      dm.Codigos.Locate('COD',Codigo,[]);

      dm.Itens.Close;
      dm.Itens.Params[0].AsInteger:=Codigo;
      dm.Itens.Open;
      dm.Itens.First;

      Soma:=0;
      while not dm.Itens.Eof do
      begin
          dm.RxCustas.Append;
          dm.RxCustasTABELA.AsString    :=dm.ItensTAB.AsString;
          dm.RxCustasITEM.AsString      :=dm.ItensITEM.AsString;
          dm.RxCustasSUBITEM.AsString   :=dm.ItensSUB.AsString;
          dm.RxCustasVALOR.AsFloat      :=dm.ItensVALOR.AsFloat;
          dm.RxCustasQTD.AsString       :=dm.ItensQTD.AsString;
          dm.RxCustasTOTAL.AsFloat      :=dm.ItensTOTAL.AsFloat;
          dm.RxCustasDESCRICAO.AsString :=dm.ItensDESCR.AsString;
          dm.RxCustas.Post;
          dm.Itens.Next;
      end;

      {TABELA DE CUSTAS FIXA PARA CERTID�O S/N}
      {SE A TABELA N�O FOR FIXA EU MEXO NAS QUANTIDADES DE BUSCAS DE ACORDO COM OS ANOS}
      if dm.ValorParametro(29)='N' then
      begin
          if (dm.CertidoesTIPO_CERTIDAO.AsString='N') or (dm.CertidoesTIPO_CERTIDAO.AsString='I') then
          begin
              dm.Tabela.Close;
              dm.Tabela.Params[0].AsInteger:=StrToInt(dm.ValorParametro(77));
              dm.Tabela.Open;
              Qtd:=spAnos.Value/5;
              if dm.RxCustas.Locate('TABELA;ITEM;SUBITEM',VarArrayOf(['16','1','*']),[]) then
              begin
                  dm.RxCustas.Edit;
                  dm.RxCustasQTD.AsFloat:=Qtd;
                  dm.RxCustas.Post;
              end;
          end;
      end;

      {ADICIONANDO ARQUIVAMENTO NA CERTID�O DE INTEIRO TEOR SE O PAR�METRO=S}
      if dm.ValorParametro(90)='S' then
      begin
          if (dm.CertidoesTIPO_CERTIDAO.AsString='I') then
          begin
              dm.Subitem.Close;
              dm.Subitem.Params[0].AsString:='16';
              dm.Subitem.Params[1].AsString:='4';
              dm.Subitem.Params[2].AsString:='*';
              dm.Subitem.Params[3].AsInteger:=2016;
              dm.Subitem.Open;
              if not dm.RxCustas.Locate('TABELA;ITEM;SUBITEM',VarArrayOf(['16','4','*']),[]) then
              begin
                  dm.RxCustas.Append;
                  dm.RxCustasTABELA.AsString    :='16';
                  dm.RxCustasITEM.AsString      :='4';
                  dm.RxCustasSUBITEM.AsString   :='*';
                  dm.RxCustasVALOR.AsFloat      :=dm.SubitemVALOR.AsFloat;
                  dm.RxCustasQTD.AsString       :='1';
                  dm.RxCustasTOTAL.AsFloat      :=dm.SubitemVALOR.AsFloat;
                  dm.RxCustasDESCRICAO.AsString :=dm.SubitemDESCR.AsString;
                  dm.RxCustas.Post;
                  dm.Subitem.Close;
              end;
          end;
      end;
  end;

  Comuns:=0;
  dm.RxCustas.First;
  while not dm.RxCustas.Eof do
  begin
      Soma:=Soma+dm.RxCustasTOTAL.AsFloat;

      if (dm.RxCustasTABELA.AsString='1') or (dm.RxCustasTABELA.AsString='16') then
      Comuns:=Comuns+dm.RxCustasTOTAL.AsFloat;

      dm.RxCustas.Next;
  end;

  if cbCobranca.ItemIndex=3 then
    dm.CertidoesEMOLUMENTOS.AsFloat:=0
      else dm.CertidoesEMOLUMENTOS.AsFloat:=Soma;

  dm.CertidoesFETJ.AsFloat        :=GR.NoRound(Soma*0.2,2);
  dm.CertidoesFUNDPERJ.AsFloat    :=GR.NoRound(Soma*0.05,2);
  dm.CertidoesFUNPERJ.AsFloat     :=GR.NoRound(Soma*0.05,2);
  dm.CertidoesFUNARPEN.AsFloat    :=GR.NoRound(Soma*0.04,2);
  dm.CertidoesPMCMV.AsFloat       :=GR.NoRound((Soma-Comuns)*0.02,2);
  dm.CertidoesISS.AsFloat         :=GR.NoRound(Soma*Gdm.vAliquotaISS,2);

  if Codigo=4010 then
  begin
      dm.CertidoesMUTUA.AsFloat         :=dm.CodigosMUTUA.AsFloat;
      dm.CertidoesACOTERJ.AsFloat       :=dm.CodigosACOTERJ.AsFloat;
      dm.CertidoesDISTRIBUICAO.AsFloat  :=dm.CodigosDISTRIB.AsFloat;
      dm.CertidoesAPONTAMENTO.AsFloat   :=dm.vApontamento;
      dm.CertidoesPMCMV.AsFloat         :=GR.NoRound((Soma-Comuns)*0.02,2);
  end
  else
  begin
      dm.CertidoesMUTUA.AsFloat         :=0;
      dm.CertidoesACOTERJ.AsFloat       :=0;
      dm.CertidoesDISTRIBUICAO.AsFloat  :=0;
      dm.CertidoesAPONTAMENTO.AsFloat   :=0;
  end;

  dm.CertidoesTOTAL.AsFloat :=dm.CertidoesEMOLUMENTOS.AsFloat+dm.CertidoesFETJ.AsFloat+
                              dm.CertidoesFUNDPERJ.AsFloat+dm.CertidoesFUNPERJ.AsFloat+
                              dm.CertidoesFUNARPEN.AsFloat+dm.CertidoesPMCMV.AsFloat+
                              dm.CertidoesISS.AsFloat+dm.CertidoesMUTUA.AsFloat+
                              dm.CertidoesACOTERJ.AsFloat+dm.CertidoesDISTRIBUICAO.AsFloat+
                              dm.CertidoesAPONTAMENTO.AsFloat;

  dm.vEmolumentos   :=dm.CertidoesEMOLUMENTOS.AsFloat;
  dm.vFetj          :=dm.CertidoesFETJ.AsFloat;
  dm.vFundperj      :=dm.CertidoesFUNDPERJ.AsFloat;
  dm.vFunperj       :=dm.CertidoesFUNPERJ.AsFloat;
  dm.vFunarpen      :=dm.CertidoesFUNARPEN.AsFloat;
  dm.vPmcmv         :=dm.CertidoesPMCMV.AsFloat;
  dm.vIss           :=dm.CertidoesISS.AsFloat;
  dm.vMutua         :=dm.CertidoesMUTUA.AsFloat;
  dm.vAcoterj       :=dm.CertidoesACOTERJ.AsFloat;
  dm.vDistribuicao  :=dm.CertidoesDISTRIBUICAO.AsFloat;
  dm.vAAR           :=0;
  dm.vTotal         :=dm.CertidoesTOTAL.AsFloat;
  edTotal.Value     :=dm.vTotal;

  dm.RxCustas.First;
end;

procedure TFPedidoCertidao.ImIgualClick(Sender: TObject);
begin
  dm.CertidoesREQUERENTE.AsString:=dm.CertidoesREQUERIDO.AsString;
  dm.CertidoesCPF_CNPJ_REQUERENTE.AsString:=dm.CertidoesCPF_CNPJ_REQUERIDO.AsString;
  edDocumento2Exit(Sender);
end;

end.
