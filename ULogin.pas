unit ULogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, FMTBcd, DB, SqlExpr, jpeg,
  sBitBtn, sEdit, sComboBox, sPanel, acPNG, acImage, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFLogin = class(TForm)
    P1: TsPanel;
    edSenha: TsEdit;
    cbNome: TsComboBox;
    btOk: TsBitBtn;
    btCancelar: TsBitBtn;
    Image2: TImage;
    Label1: TLabel;
    lbVersao: TLabel;
    lbAtualizacao: TLabel;
    Timer: TTimer;
    ImEletronico: TsImage;
    procedure btOkClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbNomeChange(Sender: TObject);
    procedure edSenhaKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure TimerTimer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FLogin: TFLogin;
  Fechar: Boolean;

implementation

uses UDM, UPF, UPrincipal, USplash, UGeral;

{$R *.dfm}

procedure TFLogin.btOkClick(Sender: TObject);
var
  Q: TFDQuery;
  Senha: String;
begin
  if cbNome.ItemIndex = -1 then
    Exit;

  if edSenha.Text = '' then
  begin
    edSenha.SetFocus;
    Exit;
  end;

  PF.Aguarde(True,'EFETUANDO LOGIN...');

  if cbNome.Text = 'TOTAL' then
  begin
    Senha := FormatDateTime('HHMM', Now);

    if Senha <> edSenha.Text then
    begin
      PF.Aguarde(False);
      GR.Aviso('Senha inv�lida.');
      edSenha.Clear;
      edSenha.SetFocus;
    end
    else
    begin
      Fechar:=True;
      Close;
    end;
  end
  else
  begin
    Q := TFDQuery.Create(Nil);
    Q.Connection := dm.conSISTEMA;
    Q.Close;
    Q.SQL.Clear;
    Q.SQL.Add('SELECT * FROM ESCREVENTES WHERE LOGIN = :LOGIN');
    Q.ParamByName('LOGIN').AsString := cbNome.Text;
    Q.Open;

    Senha := PF.Cript(Q.FieldByName('SENHA').AsString,False);

    if Senha <> edSenha.Text then
    begin
      PF.Aguarde(False);
      GR.Aviso('Senha inv�lida.');
      edSenha.Clear;
      edSenha.SetFocus;
    end
    else
    begin
      Fechar := True;
      dm.vCPF := Q.FieldByName('CPF').AsString;
      dm.vNomeCompleto := Q.FieldByName('NOME').AsString;
      Close;
    end;

    Q.Close;
    Q.Free;
  end;
end;

procedure TFLogin.btCancelarClick(Sender: TObject);
begin
  Fechar:=True;
  FPrincipal.Free;
  Application.Terminate;
end;

procedure TFLogin.FormShow(Sender: TObject);
begin
  lbVersao.Caption      := dm.vVersao;
  lbAtualizacao.Caption := dm.vAtualizacao;

  Fechar := False;

  dm.Escreventes.Close;
  dm.Escreventes.Open;
  dm.Escreventes.First;

  cbNome.Items.Clear;

  while not dm.Escreventes.Eof do
  begin
    cbNome.Items.Add(dm.EscreventesLOGIN.Value);
    dm.Escreventes.Next;
  end;

  if cbNome.Items.IndexOf('TOTAL') = -1 then
    cbNome.Items.Add('TOTAL');

  Timer.Enabled := True;

  lbAtualizacao.Caption := FSplash.lbAtualizacao.Caption;
  lbVersao.Caption      := FSplash.lbVersao.Caption;

  FSplash.Close;
  FSplash.Free;
end;

procedure TFLogin.cbNomeChange(Sender: TObject);
begin
  if cbNome.ItemIndex=-1 then btOk.Enabled:=False else btOk.Enabled:=True;
end;

procedure TFLogin.edSenhaKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then btOkClick(Sender);
end;

procedure TFLogin.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then btCancelarClick(Sender);
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.vNome        :=cbNome.Text;
  dm.vVersao      :=lbVersao.Caption;
  dm.vAtualizacao :=lbAtualizacao.Caption;
end;

procedure TFLogin.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose:=Fechar;
end;

procedure TFLogin.TimerTimer(Sender: TObject);
begin
  if FileExists('C:\FNMASTOTAL') then
  begin
      cbNome.ItemIndex  :=cbNome.Items.IndexOf('TOTAL');
      edSenha.Text      :=FormatDateTime('HHMM',Now);
      dm.vNomeCompleto  :='TOTAL';
      dm.vCPF           :='12345678909';
      cbNomeChange(Sender);
      Fechar:=True;
      Close;
  end
  else
    Timer.Enabled:=False;
end;

end.
