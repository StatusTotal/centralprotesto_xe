unit UObservacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, sMemo, sLabel;

type
  TFObservacao = class(TForm)
    lbCaracteres: TsLabel;
    MObservacao: TsMemo;
    btOk: TsBitBtn;
    btCancelar: TsBitBtn;
    function Caracteres: Integer;
    procedure FormCreate(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure MObservacaoChange(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FObservacao: TFObservacao;

implementation

uses UDM;

{$R *.dfm}

function TFObservacao.Caracteres: Integer;
begin
    Result:=250-Length(MObservacao.Text);
end;

procedure TFObservacao.FormCreate(Sender: TObject);
begin
  MObservacao.Text:=dm.vObservacao;
  MObservacaoChange(Sender);
end;

procedure TFObservacao.btOkClick(Sender: TObject);
begin
  dm.vObservacao:=MObservacao.Text;
  Close;
end;

procedure TFObservacao.MObservacaoChange(Sender: TObject);
begin
  lbCaracteres.Caption:=IntToStr(Caracteres)+' restantes';
end;

procedure TFObservacao.btCancelarClick(Sender: TObject);
begin
  Close;
end;

end.
