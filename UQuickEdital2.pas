unit UQuickEdital2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, DB, RxMemDS, StdCtrls, ComCtrls;

type
  TFQuickEdital2 = class(TForm)
    Edital: TQuickRep;
    Cabecalho: TQRBand;
    QRLabel1: TQRLabel;
    Detalhe: TQRBand;
    RX: TRxMemoryData;
    RXTitulo: TStringField;
    RXApresentante: TStringField;
    RXDevedor: TStringField;
    RXValor: TFloatField;
    RXVencimento: TDateField;
    RXProtocolo: TStringField;
    RXSaldo: TFloatField;
    RXCustas: TFloatField;
    RXEndereco: TStringField;
    RXDt_Titulo: TDateField;
    RXDt_Prazo: TDateField;
    RXDt_Protocolo: TDateField;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRLabel7: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    RXEspecie: TStringField;
    QRLabel8: TQRLabel;
    QRDBText6: TQRDBText;
    qrDocumento: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText7: TQRDBText;
    QRLabel15: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRBand1: TQRBand;
    QRShape4: TQRShape;
    lbCidade: TQRLabel;
    lbAssinatura: TQRLabel;
    lbTabeliao: TQRLabel;
    lbCartorio: TQRLabel;
    QRLabel16: TQRLabel;
    lbEndereco: TQRLabel;
    lbTelefone: TQRLabel;
    lbSite: TQRLabel;
    RXMotivo: TStringField;
    lbHorario: TQRLabel;
    ChildBand1: TQRChildBand;
    RE: TRichEdit;
    RXDt_Edital: TDateField;
    QRRichText1: TQRRichText;
    RXSacador: TStringField;
    RXTarifa: TFloatField;
    RXDt_Intimacao: TDateField;
    RXCPF_CNPJ_Devedor: TStringField;
    QRDBText10: TQRDBText;
    QRDBText14: TQRDBText;
    QRShape5: TQRShape;
    RXTipo: TStringField;
    ChildBand2: TQRChildBand;
    M: TQRMemo;
    RXID_ATO: TIntegerField;
    RXEmol: TFloatField;
    RXFund: TFloatField;
    RXFunp: TFloatField;
    RXMutua: TFloatField;
    RXAcoterj: TFloatField;
    RXDist: TFloatField;
    RXTotal: TFloatField;
    RXFetj: TFloatField;
    ChildBand3: TQRChildBand;
    QRLabel3: TQRLabel;
    lbMatricula: TQRLabel;
    lbHora: TQRLabel;
    procedure CabecalhoBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure EditalEndPage(Sender: TCustomQuickRep);
    procedure FormCreate(Sender: TObject);
    procedure DetalheBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickEdital2: TFQuickEdital2;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFQuickEdital2.CabecalhoBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbTabeliao.Caption:=dm.ServentiaTABELIAO.AsString+' - '+dm.ServentiaTITULO.AsString;
  lbEndereco.Caption:=dm.ServentiaENDERECO.AsString+' - '+dm.ServentiaBAIRRO.AsString+' - Cep: '+dm.ServentiaCEP.AsString;
  lbTelefone.Caption:='E-mail: '+dm.ServentiaEMAIL.AsString+' - '+'('+Copy(dm.ServentiaTELEFONE.AsString,1,2)+')'+Copy(dm.ServentiaTELEFONE.AsString,3,4)+'-'+Copy(dm.ServentiaTELEFONE.AsString,7,4);
  lbSite.Caption    :=dm.ServentiaSITE.AsString;
end;

procedure TFQuickEdital2.EditalEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickEdital2.FormCreate(Sender: TObject);
begin
  lbCartorio.Caption  :=dm.ServentiaDESCRICAO.AsString;
  lbHorario.Caption   :='Hor�rio de Funcionamento: '+dm.ValorParametro(25);

  {if dm.ServentiaCODIGO.AsInteger=1430 then
    if GR.Pergunta('Utilizar informa��es do 4� OFicio') then
      lbCartorio.Caption:='VASSOURAS - 4� OF�CIO';}
end;

procedure TFQuickEdital2.DetalheBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  RE.Clear;
  RE.Text:='           Nos termos do art. 15, da Lei n� 9.492, de 10 de setembro de 1997, intimo atrav�s deste '+
           'edital, o sacado abaixo-descrito a vir pagar o t�tulo correspondente ou dar as raz�es pelo qual n�o o '+
           'faz, nesta Serventia, em prazo de tr�s dias �teis contados da protocoliza��o dos mesmos, de acordo com o '+
           'art. 125 da Lei supra, em '+FormatDateTime('dd/mm/yyyy',RXDt_Edital.AsDateTime)+', pelo seguinte motivo:';
  PF.JustificarRichEdit(RE,True);
  if RXTipo.AsString='J' then
    qrDocumento.Caption:='CNPJ:'
      else
        if RXTipo.AsString='J' then
          qrDocumento.Caption:='CPF:'
            else qrDocumento.Caption:='Documento:';

  lbCidade.Caption:=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',RXDt_Edital.AsDateTime);

  M.Lines.Clear;
  PF.CarregarCustas(RXID_ATO.AsInteger);
  dm.RxCustas.First;
  while not dm.RxCustas.Eof do
  begin
      M.Lines.Add(dm.RxCustasTABELA.AsString+'.'+dm.RxCustasITEM.AsString+'.'+dm.RxCustasSUBITEM.AsString+'   '+dm.RxCustasDESCRICAO.AsString+' x '+dm.RxCustasQTD.AsString+Format('%-1s %7.2f',[' =',dm.RxCustasTOTAL.AsFloat]));
      dm.RxCustas.Next;
  end;
  M.Lines.Add(Format('%-1s %7.2f',['Emolumentos =',RXEmol.AsFloat]));
  M.Lines.Add(Format('%-1s %7.2f',['FETJ =',RXFETJ.AsFloat]));
  M.Lines.Add(Format('%-1s %7.2f',['Fundperj =',RXFund.AsFloat]));
  M.Lines.Add(Format('%-1s %7.2f',['Funperj =',RXFunp.AsFloat]));
  M.Lines.Add(Format('%-1s %7.2f',['M�tua =',RXMutua.AsFloat]));
  M.Lines.Add(Format('%-1s %7.2f',['Acoterj =',RXAcoterj.AsFloat]));
  M.Lines.Add(Format('%-1s %7.2f',['Distribui��o =',RXDist.AsFloat]));
  M.Lines.Add(Format('%-1s %7.2f',['Total =',RXTotal.AsFloat]));
end;

procedure TFQuickEdital2.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbAssinatura.Caption :=dm.vAssinatura;
  lbMatricula.Caption  :=dm.vMatricula;
end;

end.



