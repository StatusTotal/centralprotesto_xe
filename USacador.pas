unit USacador;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBCtrls, sDBLookupComboBox, StdCtrls, Buttons, sBitBtn,
  ExtCtrls, sPanel, sGroupBox, sDBRadioGroup, Mask, sDBEdit, sMaskEdit,
  sCustomComboEdit, sTooledit, sDBDateEdit, acPNG, sDBMemo, sDBComboBox,
  RxDBComb, sCheckBox, sDBCheckBox, wwclearbuttongroup, wwradiogroup,
  sButton;

type
  TFSacador = class(TForm)
    P1: TsPanel;
    dsRXSacador: TDataSource;
    edNome: TsDBEdit;
    dsMunicipios: TDataSource;
    P3: TsPanel;
    btOk: TsBitBtn;
    edEndereco: TsDBEdit;
    cbUF: TsDBComboBox;
    lkCidade: TsDBLookupComboBox;
    ImAviso1: TImage;
    ImOk1: TImage;
    edDocumento: TsDBEdit;
    cbTipo: TsDBComboBox;
    edBairro: TsDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure cbUFChange(Sender: TObject);
    procedure cbTipoChange(Sender: TObject);
    procedure edDocumentoChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSacador: TFSacador;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFSacador.FormCreate(Sender: TObject);
begin
  cbUFChange(Sender);
  cbTipoChange(Sender);
  edDocumentoChange(Sender);
end;

procedure TFSacador.btOkClick(Sender: TObject);
begin
  Close;
end;

procedure TFSacador.cbUFChange(Sender: TObject);
begin
  dm.Municipios.Close;
  dm.Municipios.Params[0].AsString:=cbUF.Text;
  dm.Municipios.Open;
end;

procedure TFSacador.cbTipoChange(Sender: TObject);
begin
  if cbTipo.ItemIndex=0 then
  begin
      edDocumento.BoundLabel.Caption :='CPF';
      dm.RxSacadorDOCUMENTO.EditMask :='999.999.999-99;0;_';
      dm.RxSacadorTIPO.AsString      :='F';
      edDocumento.Enabled            :=True;
  end
  else
    if cbTipo.ItemIndex=1 then
    begin
        edDocumento.BoundLabel.Caption :='CNPJ';
        dm.RxSacadorDOCUMENTO.EditMask :='99.999.999/9999-99;0;_';
        dm.RxSacadorTIPO.AsString      :='J';
        edDocumento.Enabled            :=True;
    end
    else
    begin
        dm.RxSacadorDOCUMENTO.EditMask :='';
        dm.RxSacadorTIPO.AsString      :='N';
        edDocumento.Enabled            :=False;
        dm.RxSacadorDOCUMENTO.Clear;
    end;
end;

procedure TFSacador.edDocumentoChange(Sender: TObject);
begin
  if GR.PegarNumeroTexto(edDocumento.Text)='' then
  begin
      ImAviso1.Visible:=True;
      ImOk1.Visible   :=False;
      Exit;
  end;

  if cbTipo.ItemIndex=0 then ImAviso1.Visible:=not PF.VerificarCPF(GR.PegarNumeroTexto(edDocumento.Text));
  if cbTipo.ItemIndex=1 then ImAviso1.Visible:=not PF.VerificarCNPJ(GR.PegarNumeroTexto(edDocumento.Text));

  ImOk1.Visible:=not ImAviso1.Visible;
end;

end.
