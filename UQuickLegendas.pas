unit UQuickLegendas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls;

type
  TFQuickLegendas = class(TForm)
    Legendas: TQuickRep;
    QRBand1: TQRBand;
    M: TQRMemo;
    procedure FormCreate(Sender: TObject);
    procedure LegendasEndPage(Sender: TCustomQuickRep);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickLegendas: TFQuickLegendas;

implementation

uses UDM, UPF, DB;

{$R *.dfm}

procedure TFQuickLegendas.FormCreate(Sender: TObject);
var
  Sigla: String;
begin
  dm.Tipos.Close;
  dm.Tipos.Open;
  dm.Tipos.First;
  while not dm.Tipos.Eof do
  begin
      if dm.TiposCODIGO.AsInteger<>0 then
      begin
          Sigla:=dm.TiposSIGLA.AsString;
          Sigla:=PF.Espaco(Sigla,'E',5);
          M.Lines.Add(Sigla+' = '+dm.TiposDESCRICAO.AsString);
      end;
      dm.Tipos.Next;
  end;
  dm.Tipos.Close;
  Legendas.Preview;
end;

procedure TFQuickLegendas.LegendasEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

end.
