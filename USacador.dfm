object FSacador: TFSacador
  Left = 393
  Top = 232
  BorderStyle = bsDialog
  Caption = 'Dados do Sacador'
  ClientHeight = 181
  ClientWidth = 492
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 14
  object P1: TsPanel
    Left = 3
    Top = 3
    Width = 486
    Height = 148
    TabOrder = 0
    SkinData.SkinSection = 'BARTITLE'
    object ImAviso1: TImage
      Left = 273
      Top = 39
      Width = 16
      Height = 16
      Hint = 'DOCUMENTO INV'#193'LIDO!'
      AutoSize = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        000000000000FFFFFF0000000000000000000000000000000000000000000000
        0000AA640070AA64007000000000000000000000000000000000000000000000
        0000FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
        0000AA6400CFAA6400CF00000000000000000000000000000000000000000000
        0000FFFFFF00FFFFFF000000000000000000000000000000000000000000AA64
        0020BF8B3DFFBF8B3DFFAA640020000000000000000000000000000000000000
        0000FFFFFF00FFFFFF000000000000000000000000000000000000000000AA64
        0060DABB8DFFDABB8DFFAA640060000000000000000000000000000000000000
        0000FFFFFF00FFFFFF00000000000000000000000000AA640060AA6400AFB577
        1FFFF4ECDDFFF4ECDDFFB5771FFFAA6400AFAA64006000000000000000000000
        0000FFFFFF00FFFFFF0000000000AA640010AA6400BFCA9E5DFFF4ECD8FFFFFF
        F8FFFFFFFFFFFFFFFEFFFFFFF7FFF4ECD8FFCA9E5DFFAA6400BFAA6400100000
        0000FFFFFF00FFFFFF00AA640010AA6400CFDFC59BFFFFFEF7FFFFFCF5FFFFFB
        F6FF010BBFFF010BBFFFFFFBF6FFFFFCF5FFFFFEF7FFDFC59BFFAA6400CFAA64
        0010FFFFFF00FFFFFF00AA640080DABB8CFFFFFAF0FFFFF6EAFFFFF6EAFFFFF6
        EAFF0021C2FF0021C2FFFFF6EAFFFFF6EAFFFFF6EAFFFFFBF2FFDABB8CFFAA64
        0080FFFFFF00FFFFFF00B57921EFFFFBF3FFFFF1DEFFFFF1DEFFFFF1DEFFFFF1
        DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFFDF7FFB579
        21EFFFFFFF00FFFFFF00C5944FFFFFF3E3FFFFEDD5FFFFEDD5FFFFEDD5FFFFED
        D5FF0007DAFF0007DAFFFFEDD5FFFFEDD5FFFFEDD5FFFFEDD5FFFFF6E8FFC594
        4FFFFFFFFF00FFFFFF00C5944FFFFFF0DCFFFFEACDFFFFEACDFFFFEACDFFFFEA
        CDFF0012E5FF0012E5FFFFEACDFFFFEACDFFFFEACDFFFFEACDFFFFF4E5FFC594
        4FFFFFFFFF00FFFFFF00B57922EFFFF5E6FFFFE7C8FFFFE7C8FFFFE7C8FFFFE7
        C8FF001DD5FF001DD5FFFFE7C8FFFFE7C8FFFFE7C8FFFFE7C8FFFFF6E9FFB579
        22EFFFFFFF00FFFFFF00AA640080DABA8BFFFFEBD0FFFFE6C5FFFFE6C5FFFFE6
        C5FF0029C2FF0029C2FFFFE6C5FFFFE6C5FFFFE6C5FFFFEBD0FFDABB8FFFAA64
        0080FFFFFF00FFFFFF00AA640010B17014CFEAD7BBFFFFEED7FFFFE6C5FFFFE6
        C5FF0024AFFF0024AFFFFFE6C5FFFFE6C5FFFFEFDAFFEAD7BBFFB17014CFAA64
        0010FFFFFF00FFFFFF0000000000AA640010AA6400CFCFA870FFF4E9D8FFFFF4
        E6FFFFF3E2FFFFF3E2FFFFF4E6FFF4E9D8FFCFA870FFAA6400CFAA6400100000
        0000FFFFFF00FFFFFF00000000000000000000000000AA640060AA6400CFBA81
        30FFBF8B40FFBF8B40FFBA8130FFAA6400CFAA64006000000000000000000000
        0000FFFFFF00}
      ShowHint = True
      Transparent = True
      Visible = False
    end
    object ImOk1: TImage
      Left = 273
      Top = 39
      Width = 16
      Height = 16
      Hint = 'DOCUMENTO OK!'
      AutoSize = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000939393F9939393F9939393F9939393F99393
        93F9939393F9939393F9939393F9939393F9939393F9939393F9939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF939393F90000
        0000000000000000000000000000939393F900750CF500750CF5EEEEEEFFEDED
        EDFFECECECFFEBEBEBFFEAEAEAFFE9E9E9FFE8E8E8FFFFFFFFFF939393F90000
        000000000000000000000000000000750CF5D3FFE9FF8EFDB4FF00750CF5EFEF
        EFFFEEEEEEFFEDEDEDFFECECECFFEBEBEBFFEAEAEAFFFFFFFFFF939393F90000
        0000000000000000000000750CF5D3FFE9FF41EF7DFF58EF8BFF8EF9B2FF0075
        0CF5F0F0F0FFEFEFEFFFEEEEEEFFEDEDEDFFECECECFFFFFFFFFF939393F90000
        00000000000000750CF5D3FFE9FF7FFFAEFF76BD8CFF00750CF56FF79EFF84F2
        A9FF00750CF5F1F1F1FFF0F0F0FFEFEFEFFFEEEEEEFFFFFFFFFF939393F90000
        00000000000050795D3500750CF5A7DBB7FF00750CF5F6F6F6FF00750CF588FD
        B0FF8BF4AFFF00750CF5F1F1F1FFF1F1F1FFF0F0F0FFFFFFFFFF939393F90000
        000000000000000000000014003800750CF5FFFFFFFFF8F8F8FFF2F2F2FF0075
        0CF5D3FFE9FF00750CF5E7ECE8FFF2F2F2FFF1F1F1FFFFFFFFFF939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFAFAFAFFF9F9F9FFF2F2
        F2FF00750CF5C5C5C5FFF5F5F5FFF4F4F4FFF3F3F3FFF5F5F5FF939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFBFBFBFFFAFAFAFFFAFA
        FAFFF9F9F9FFF8F8F8FF939393F9939393F9939393F9939393F9939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFDFDFDFFFCFCFCFFFBFB
        FBFFFAFAFAFFFAFAFAFF939393F9E1E1E1FFE1E1E1FFB5B5B5F9939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFFFFFFFFFEFEFEFFFDFD
        FDFFFCFCFCFFFBFBFBFF939393F9E1E1E1FFB5B5B5F9939393F9000000000000
        0000000000000000000000000000939393F9FFFFFFFFFFFFFFFFFFFFFFFFFEFE
        FEFFFEFEFEFFFDFDFDFF939393F9B5B5B5F9939393F900000000000000000000
        0000000000000000000000000000939393F9939393F9939393F9939393F99393
        93F9939393F9939393F9939393F9939393F90000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000}
      ShowHint = True
      Transparent = True
      Visible = False
    end
    object edNome: TsDBEdit
      Left = 73
      Top = 8
      Width = 406
      Height = 22
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'NOME'
      DataSource = dsRXSacador
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Nome'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edEndereco: TsDBEdit
      Left = 73
      Top = 64
      Width = 406
      Height = 22
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'ENDERECO'
      DataSource = dsRXSacador
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Endere'#231'o'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object cbUF: TsDBComboBox
      Left = 73
      Top = 118
      Width = 50
      Height = 22
      Style = csDropDownList
      Color = clWhite
      DataField = 'UF'
      DataSource = dsRXSacador
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemHeight = 14
      Items.Strings = (
        'AC'
        'AL'
        'AP'
        'AM'
        'BA'
        'CE'
        'DF'
        'ES'
        'GO'
        'MA'
        'MT'
        'MS'
        'MG'
        'PA'
        'PB'
        'PR'
        'PE'
        'PI'
        'RJ'
        'RN'
        'RS'
        'RO'
        'RR'
        'SC'
        'SP'
        'SE'
        'TO')
      ParentFont = False
      TabOrder = 5
      OnChange = cbUFChange
      BoundLabel.Active = True
      BoundLabel.Caption = 'UF'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
    end
    object lkCidade: TsDBLookupComboBox
      Left = 169
      Top = 118
      Width = 310
      Height = 22
      Color = clWhite
      DataField = 'MUNICIPIO'
      DataSource = dsRXSacador
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyField = 'CIDADE'
      ListField = 'CIDADE'
      ListSource = dsMunicipios
      ParentFont = False
      TabOrder = 6
      BoundLabel.Active = True
      BoundLabel.Caption = 'Cidade'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
    end
    object edDocumento: TsDBEdit
      Left = 128
      Top = 36
      Width = 141
      Height = 22
      Hint = 'DOCUMENTO DO PORTADOR'
      Color = clWhite
      DataField = 'DOCUMENTO'
      DataSource = dsRXSacador
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnChange = edDocumentoChange
      SkinData.SkinSection = 'COMBOBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object cbTipo: TsDBComboBox
      Left = 73
      Top = 36
      Width = 50
      Height = 22
      Style = csDropDownList
      Color = clWhite
      DataField = 'TIPO'
      DataSource = dsRXSacador
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemHeight = 14
      Items.Strings = (
        'F'
        'J')
      ParentFont = False
      TabOrder = 1
      OnChange = cbTipoChange
      BoundLabel.Active = True
      BoundLabel.Caption = 'Tipo'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
    end
    object edBairro: TsDBEdit
      Left = 73
      Top = 91
      Width = 406
      Height = 22
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'BAIRRO'
      DataSource = dsRXSacador
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Bairro'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
  end
  object P3: TsPanel
    Left = 0
    Top = 152
    Width = 492
    Height = 29
    Align = alBottom
    TabOrder = 1
    SkinData.SkinSection = 'RADIOBUTTON'
    object btOk: TsBitBtn
      Left = 411
      Top = 1
      Width = 78
      Height = 25
      Cancel = True
      Caption = 'Ok'
      TabOrder = 0
      OnClick = btOkClick
      SkinData.SkinSection = 'COLHEADER'
      ImageIndex = 3
      Images = dm.Imagens
    end
  end
  object dsRXSacador: TDataSource
    DataSet = dm.RxSacador
    Left = 141
    Top = 216
  end
  object dsMunicipios: TDataSource
    DataSet = dm.Municipios
    Left = 64
    Top = 216
  end
end
