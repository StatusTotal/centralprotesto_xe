unit UQuickTitulos1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QuickRpt, QRCtrls, ExtCtrls, DB, DBClient, SimpleDS,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFQuickTitulos1 = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    lbCartorio: TQRLabel;
    lbEndereco: TQRLabel;
    lbTitulo: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    lbPortador: TQRLabel;
    QRBand2: TQRBand;
    QRBand3: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRSysData1: TQRSysData;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRSysData2: TQRSysData;
    lbSituacao: TQRLabel;
    qrSituacao: TQRDBText;
    Todos: TQuickRep;
    QRBand4: TQRBand;
    lbCartorio2: TQRLabel;
    lbEndereco2: TQRLabel;
    lbTitulo2: TQRLabel;
    QRSysData3: TQRSysData;
    Detalhe: TQRBand;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    Footer: TQRBand;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    Grupo: TQRGroup;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRDBText16: TQRDBText;
    QRLabel11: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel22: TQRLabel;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    QRExpr7: TQRExpr;
    QRExpr8: TQRExpr;
    QRExpr9: TQRExpr;
    QRExpr10: TQRExpr;
    lbQtd: TQRLabel;
    QRLabel4: TQRLabel;
    lbTabeliao: TQRLabel;
    QRBand5: TQRBand;
    QRExpr11: TQRExpr;
    QRExpr12: TQRExpr;
    QRExpr13: TQRExpr;
    QRExpr14: TQRExpr;
    QRExpr15: TQRExpr;
    QRExpr16: TQRExpr;
    QRExpr17: TQRExpr;
    QRExpr18: TQRExpr;
    lbQgr: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText21: TQRDBText;
    QRExpr19: TQRExpr;
    QRExpr20: TQRExpr;
    QRLabel10: TQRLabel;
    QRDBText22: TQRDBText;
    QRExpr21: TQRExpr;
    QRExpr22: TQRExpr;
    QRLabel23: TQRLabel;
    QRDBText23: TQRDBText;
    qryTitulos: TFDQuery;
    qryTodosPortadores: TFDQuery;
    qryTitulosPROTOCOLO: TIntegerField;
    qryTitulosDT_PROTOCOLO: TDateField;
    qryTitulosTOTAL: TFloatField;
    qryTitulosNUMERO_TITULO: TStringField;
    qryTitulosVALOR_TITULO: TFloatField;
    qryTitulosNOSSO_NUMERO: TStringField;
    qryTitulosSACADOR: TStringField;
    qryTitulosDEVEDOR: TStringField;
    qryTitulosSTATUS: TStringField;
    qryTitulosCODIGO: TIntegerField;
    qryTitulosDataProtocolo: TStringField;
    qryTodosPortadoresAPRESENTANTE: TStringField;
    qryTodosPortadoresPROTOCOLO: TIntegerField;
    qryTodosPortadoresDEVEDOR: TStringField;
    qryTodosPortadoresEMOLUMENTOS: TFloatField;
    qryTodosPortadoresFETJ: TFloatField;
    qryTodosPortadoresFUNDPERJ: TFloatField;
    qryTodosPortadoresFUNPERJ: TFloatField;
    qryTodosPortadoresFUNARPEN: TFloatField;
    qryTodosPortadoresPMCMV: TFloatField;
    qryTodosPortadoresMUTUA: TFloatField;
    qryTodosPortadoresACOTERJ: TFloatField;
    qryTodosPortadoresDISTRIBUICAO: TFloatField;
    qryTodosPortadoresTOTAL: TFloatField;
    qryTodosPortadoresTIPO_TITULO: TIntegerField;
    qryTodosPortadoresVALOR_TITULO: TFloatField;
    qryTodosPortadoresSTATUS: TStringField;
    qryTodosPortadoresCONVENIO: TStringField;
    qryTodosPortadoresCODIGO: TIntegerField;
    qryTodosPortadoresTipoDocumento: TStringField;
    qryTodosPortadorescEmolumentos: TFloatField;
    qryTodosPortadorescFetj: TFloatField;
    qryTodosPortadorescFundperj: TFloatField;
    qryTodosPortadorescFunperj: TFloatField;
    qryTodosPortadorescFunarpen: TFloatField;
    qryTodosPortadorescPmcmv: TFloatField;
    qryTodosPortadorescMutua: TFloatField;
    qryTodosPortadorescAcoterj: TFloatField;
    qryTodosPortadorescDistribuicao: TFloatField;
    qryTodosPortadorescTotal: TFloatField;
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure QuickRep1EndPage(Sender: TCustomQuickRep);
    procedure TodosEndPage(Sender: TCustomQuickRep);
    procedure FooterBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure qryTodosPortadoresCalcFields(DataSet: TDataSet);
    procedure qryTitulosCalcFields(DataSet: TDataSet);
    procedure qryTitulosTOTALGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryTitulosVALOR_TITULOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryTodosPortadoresEMOLUMENTOSGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryTodosPortadoresFETJGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryTodosPortadoresFUNDPERJGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryTodosPortadoresFUNPERJGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryTodosPortadoresFUNARPENGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryTodosPortadoresPMCMVGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryTodosPortadoresMUTUAGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryTodosPortadoresDISTRIBUICAOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryTodosPortadoresTOTALGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryTodosPortadorescEmolumentosGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryTodosPortadorescFetjGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryTodosPortadorescFundperjGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryTodosPortadorescFunperjGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryTodosPortadorescFunarpenGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryTodosPortadorescPmcmvGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryTodosPortadorescMutuaGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryTodosPortadorescAcoterjGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryTodosPortadorescDistribuicaoGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure qryTodosPortadorescTotalGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    { Private declarations }
    Qtd,Qgr: Integer;
  public
    { Public declarations }
  end;

var
  FQuickTitulos1: TFQuickTitulos1;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFQuickTitulos1.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if dm.ServentiaCODIGO.AsInteger=1208 then
    if qryTitulosSTATUS.AsString='APONTADO' then
    begin
        lbSituacao.Enabled:=False;
        qrSituacao.Enabled:=False;
    end
    else
    begin
        lbSituacao.Enabled:=True;
        qrSituacao.Enabled:=True;
    end;

  Inc(Qtd);
  Inc(Qgr);
end;

procedure TFQuickTitulos1.FormCreate(Sender: TObject);
begin
  Qtd:=0;
  Qgr:=0;
  qryTitulos.Connection:=dm.conSISTEMA;
end;

procedure TFQuickTitulos1.QuickRep1EndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickTitulos1.TodosEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickTitulos1.FooterBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbQtd.Caption:='Quantidade: '+IntToStr(Qtd);
  Qtd:=0;
end;

procedure TFQuickTitulos1.QRBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbQgr.Caption:='Quantidade: '+IntToStr(Qgr);
  Qgr:=0;
end;

procedure TFQuickTitulos1.qryTitulosCalcFields(DataSet: TDataSet);
begin
  qryTitulosDataProtocolo.AsString:=PF.FormatarData(qryTitulosDT_PROTOCOLO.AsDateTime,'N')+' - '+qryTitulosPROTOCOLO.AsString;
end;

procedure TFQuickTitulos1.qryTitulosTOTALGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTitulosVALOR_TITULOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadorescAcoterjGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadoresCalcFields(DataSet: TDataSet);
begin
  qryTodosPortadoresTipoDocumento.AsString:=PF.RetornarTitulo(qryTodosPortadoresTIPO_TITULO.AsInteger);

  if qryTodosPortadoresCONVENIO.AsString='S' then
  begin
      qryTodosPortadorescEmolumentos.AsFloat :=0;
      qryTodosPortadorescFetj.AsFloat        :=0;
      qryTodosPortadorescFundperj.AsFloat    :=0;
      qryTodosPortadorescFunperj.AsFloat     :=0;
      qryTodosPortadorescFunarpen.AsFloat    :=0;
      qryTodosPortadorescPmcmv.AsFloat       :=0;
      qryTodosPortadorescMutua.AsFloat       :=0;
      qryTodosPortadorescAcoterj.AsFloat     :=0;
      qryTodosPortadorescDistribuicao.AsFloat:=0;
      qryTodosPortadorescTotal.AsFloat       :=0;
  end
  else
  begin
      qryTodosPortadorescEmolumentos.AsFloat :=qryTodosPortadoresEMOLUMENTOS.AsFloat;
      qryTodosPortadorescFetj.AsFloat        :=qryTodosPortadoresFETJ.AsFloat;
      qryTodosPortadorescFundperj.AsFloat    :=qryTodosPortadoresFUNDPERJ.AsFloat;
      qryTodosPortadorescFunperj.AsFloat     :=qryTodosPortadoresFUNPERJ.AsFloat;
      qryTodosPortadorescFunarpen.AsFloat    :=qryTodosPortadoresFUNARPEN.AsFloat;
      qryTodosPortadorescPmcmv.AsFloat       :=qryTodosPortadoresPMCMV.AsFloat;
      qryTodosPortadorescMutua.AsFloat       :=qryTodosPortadoresMUTUA.AsFloat;
      qryTodosPortadorescAcoterj.AsFloat     :=qryTodosPortadoresACOTERJ.AsFloat;
      qryTodosPortadorescDistribuicao.AsFloat:=qryTodosPortadoresDISTRIBUICAO.AsFloat;
      qryTodosPortadorescTotal.AsFloat       :=qryTodosPortadoresTOTAL.AsFloat;
  end;
end;

procedure TFQuickTitulos1.qryTodosPortadorescDistribuicaoGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadorescEmolumentosGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadorescFetjGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadorescFunarpenGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadorescFundperjGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadorescFunperjGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadorescMutuaGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadorescPmcmvGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadorescTotalGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadoresDISTRIBUICAOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadoresEMOLUMENTOSGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadoresFETJGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadoresFUNARPENGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadoresFUNDPERJGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadoresFUNPERJGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadoresMUTUAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadoresPMCMVGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickTitulos1.qryTodosPortadoresTOTALGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

end.
