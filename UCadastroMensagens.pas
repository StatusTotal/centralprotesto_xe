unit UCadastroMensagens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, sPanel, Grids, Wwdbigrd, Wwdbgrid, StdCtrls,
  Buttons, sBitBtn;

type
  TFCadastroMensagens = class(TForm)
    Grid: TwwDBGrid;
    dsMensagens: TDataSource;
    sPanel1: TsPanel;
    btIncluir: TsBitBtn;
    btAlterar: TsBitBtn;
    btExcluir: TsBitBtn;
    sBitBtn1: TsBitBtn;
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btExcluirClick(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroMensagens: TFCadastroMensagens;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFCadastroMensagens.sBitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TFCadastroMensagens.FormCreate(Sender: TObject);
begin
  dm.Mensagens.Close;
  dm.Mensagens.Open;
  dm.Mensagens.Filtered:=False;
  dm.Mensagens.Filter  :='ID_MSG<>'+QuotedStr('0');
  dm.Mensagens.Filtered:=True;
end;

procedure TFCadastroMensagens.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm.Mensagens.Filtered:=False;
end;

procedure TFCadastroMensagens.btExcluirClick(Sender: TObject);
begin
  if dm.Mensagens.IsEmpty then Exit;

  dm.Mensagens.Delete;
  dm.Mensagens.ApplyUpdates(0);
end;

procedure TFCadastroMensagens.btIncluirClick(Sender: TObject);
var
  Descricao: String;
begin
  dm.Mensagens.Append;

  if InputQuery('Inclus�o','Descri��o',Descricao) then
  begin
      dm.MensagensID_MSG.AsInteger :=dm.IdAtual('ID_MSG','S');
      dm.MensagensMENSAGEM.AsString:=Descricao;
      dm.Mensagens.Post;
      dm.Mensagens.ApplyUpdates(0);
  end
  else
    dm.Mensagens.Cancel;
end;

procedure TFCadastroMensagens.btAlterarClick(Sender: TObject);
var
  Descricao: String;
begin
  if dm.Mensagens.IsEmpty then Exit;
  
  dm.Mensagens.Edit;

  Descricao:=dm.MensagensMENSAGEM.AsString;
  if InputQuery('Altera��o','Descri��o',Descricao) then
  begin
      dm.MensagensMENSAGEM.AsString:=Descricao;
      dm.Mensagens.Post;
      dm.Mensagens.ApplyUpdates(0);
  end
  else
    dm.Mensagens.Cancel;
end;

procedure TFCadastroMensagens.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFCadastroMensagens.GridDblClick(Sender: TObject);
begin
  btAlterarClick(Sender);
end;

end.
