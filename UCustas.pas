unit UCustas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Mask, sMaskEdit, sCustomComboEdit, sCurrEdit, sDBCalcEdit,
  StdCtrls, Buttons, sBitBtn, sSpinEdit, DBCtrls, sDBEdit, sEdit, Grids,
  Wwdbigrd, Wwdbgrid, ExtCtrls, sLabel, sPanel, sCurrencyEdit, sSpeedButton;

type
  TFCustas = class(TForm)
    P1: TsPanel;
    sLabel1: TsLabel;
    ImAviso: TImage;
    Grid1: TwwDBGrid;
    edTabela: TsEdit;
    edItem: TsEdit;
    edVlr: TsDBEdit;
    spQtd: TsSpinEdit;
    btAdicionar: TsBitBtn;
    btRemover: TsBitBtn;
    btConcluir: TsBitBtn;
    P2: TsPanel;
    Grid2: TwwDBGrid;
    dsTabela: TDataSource;
    dsRxCustas: TDataSource;
    P3: TsPanel;
    sbOk: TsSpeedButton;
    sbCancelar: TsSpeedButton;
    edQuantidade: TsDBEdit;
    edValor: TsDBEdit;
    edEmolumentos: TsCurrencyEdit;
    edFetj: TsCurrencyEdit;
    edFundperj: TsCurrencyEdit;
    edFunperj: TsCurrencyEdit;
    edMutua: TsCurrencyEdit;
    edAcoterj: TsCurrencyEdit;
    edDistribuicao: TsCurrencyEdit;
    edTotal: TsCalcEdit;
    edAR: TsCurrencyEdit;
    btCalcular: TsBitBtn;
    edTarifa: TsCurrencyEdit;
    edApontamento: TsCalcEdit;
    edFunarpen: TsCurrencyEdit;
    edPmcmv: TsCurrencyEdit;
    edIss: TsCurrencyEdit;
    procedure Calcular;
    procedure edTabelaChange(Sender: TObject);
    procedure edItemChange(Sender: TObject);
    procedure btAdicionarClick(Sender: TObject);
    procedure btRemoverClick(Sender: TObject);
    procedure btConcluirClick(Sender: TObject);
    procedure Grid2DblClick(Sender: TObject);
    procedure sbOkClick(Sender: TObject);
    procedure sbCancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Grid1DblClick(Sender: TObject);
    procedure btCalcularClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCustas: TFCustas;

implementation

uses UDM, UPF, UGeral, UGDM;

{$R *.dfm}

procedure TFCustas.Calcular;
var
  Posicao: Integer;
  Soma,Comuns: Double;
begin
  Posicao:=dm.RxCustas.RecNo;
  dm.RxCustas.DisableControls;
  dm.RxCustas.First;
  Soma:=0;
  Comuns:=0;
  while not dm.RxCustas.Eof do
  begin
      if dm.RxCustasTABELA.AsString='16' then
      Comuns:=Comuns+dm.RxCustasTOTAL.AsFloat;

      Soma:=Soma+dm.RxCustasTOTAL.AsFloat;
      dm.RxCustas.Next;
  end;

  edEmolumentos.Value :=Soma;
  edFetj.Value        :=GR.NoRound(Soma*0.2,2);
  edFundperj.Value    :=GR.NoRound(Soma*0.05,2);
  edFunperj.Value     :=GR.NoRound(Soma*0.05,2);
  edFunarpen.Value    :=GR.NoRound(Soma*0.04,2);
  edPmcmv.Value       :=GR.NoRound((Soma-Comuns)*0.02,2);
  edIss.Value         :=GR.NoRound(Soma*Gdm.vAliquotaISS,2);
  edTotal.Value       :=edEmolumentos.Value+
                        edFetj.Value+
                        edFundperj.Value+
                        edFunperj.Value+
                        edFunarpen.Value+
                        edPmcmv.Value+
                        edIss.Value+
                        edMutua.Value+
                        edAcoterj.Value+
                        edDistribuicao.Value+
                        edAR.Value+
                        edTarifa.Value+
                        edApontamento.Value;
                        
  dm.RxCustas.RecNo:=Posicao;
  dm.RxCustas.EnableControls;
end;

procedure TFCustas.edTabelaChange(Sender: TObject);
begin
   if edTabela.Text='' then Exit;
   dm.Tabela.Locate('TAB',edTabela.Text,[loCaseInsensitive, loPartialKey]);
end;

procedure TFCustas.edItemChange(Sender: TObject);
begin
  if (edTabela.Text='') and (edItem.Text='')  then Exit;
  dm.Tabela.Locate('TAB;ITEM',VarArrayOf([edTabela.Text,edItem.Text]),[loCaseInsensitive, loPartialKey]);
end;

procedure TFCustas.btAdicionarClick(Sender: TObject);
begin
  dm.RxCustas.Append;
  dm.RxCustasID_ATO.AsInteger   :=dm.vIdAto; {S� VAI TER SENTIDO NA SE ESTA TELA ESTIVER VINDO DA TELA DE BAIXA - CANCELAMENTO}
  dm.RxCustasTABELA.AsString    :=dm.TabelaTAB.AsString;
  dm.RxCustasITEM.AsString      :=dm.TabelaITEM.AsString;
  dm.RxCustasSUBITEM.AsString   :=dm.TabelaSUB.AsString;
  dm.RxCustasDESCRICAO.AsString :=dm.TabelaDESCR.AsString;
  dm.RxCustasVALOR.AsFloat      :=dm.TabelaVALOR.AsFloat;
  dm.RxCustasQTD.AsString       :=spQtd.Text;
  dm.RxCustasTOTAL.AsFloat      :=dm.TabelaVALOR.AsFloat*spQtd.Value;
  dm.RxCustas.Post;
  Calcular;
end;

procedure TFCustas.btRemoverClick(Sender: TObject);
begin
  if dm.RxCustas.IsEmpty then Exit;
  if dm.RxCustasID_CUSTA.AsString<>'' then
  PF.ExcluirFD('CUSTAS WHERE ID_CUSTA='+QuotedStr(dm.RxCustasID_CUSTA.AsString),dm.conSISTEMA);
  dm.RxCustas.Delete;
  Calcular;
end;

procedure TFCustas.btConcluirClick(Sender: TObject);
begin
  dm.vEmolumentos   :=edEmolumentos.Value;
  dm.vFetj          :=edFetj.Value;
  dm.vFundperj      :=edFundperj.Value;
  dm.vFunperj       :=edFunperj.Value;
  dm.vFunarpen      :=edFunarpen.Value;
  dm.vPmcmv         :=edPmcmv.Value;
  dm.vIss           :=edIss.Value;
  dm.vMutua         :=edMutua.Value;
  dm.vAcoterj       :=edAcoterj.Value;
  dm.vDistribuicao  :=edDistribuicao.Value;
  dm.vAAR           :=edAR.Value;
  dm.vTarifa        :=edTarifa.Value;
  dm.vApontamento   :=edApontamento.Value;
  dm.vTotal         :=edTotal.Value;
  dm.vOkGeral       :=True;
  Close;
end;

procedure TFCustas.Grid2DblClick(Sender: TObject);
begin
  if dm.RxCustas.IsEmpty then Exit;

  P1.Enabled:=False;
  P2.Enabled:=False;
  P3.Visible:=True;
  edQuantidade.SetFocus;
  dm.RxCustas.Edit;
end;

procedure TFCustas.sbOkClick(Sender: TObject);
begin
  dm.RxCustas.Post;

  P1.Enabled:=True;
  P2.Enabled:=True;
  P3.Visible:=False;
end;

procedure TFCustas.sbCancelarClick(Sender: TObject);
begin
  dm.RxCustas.Cancel;

  P1.Enabled:=True;
  P2.Enabled:=True;
  P3.Visible:=False;
end;

procedure TFCustas.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_F5 then Calcular;
end;

procedure TFCustas.FormCreate(Sender: TObject);
begin
  dm.Tabela.Close;
  dm.Tabela.Open;
  edEmolumentos.Value   :=dm.vEmolumentos;
  edFetj.Value          :=dm.vFetj;
  edFundperj.Value      :=dm.vFundperj;
  edFunperj.Value       :=dm.vFunperj;
  edFunarpen.Value      :=dm.vFunarpen;
  edPmcmv.Value         :=dm.vPmcmv;
  edIss.Value           :=dm.vIss;
  edMutua.Value         :=dm.vMutua;
  edAcoterj.Value       :=dm.vAcoterj;
  edDistribuicao.Value  :=dm.vDistribuicao;
  edAR.Value            :=dm.vAAR;
  edTarifa.Value        :=dm.vTarifa;
  edApontamento.Value   :=dm.vApontamento;
  edTotal.Value         :=dm.vTotal;
  edAR.Visible          :=dm.ValorParametro(35)<>'0,00';
  dm.vOkGeral           :=False;
end;

procedure TFCustas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.Tabela.ApplyUpdates(0);
  dm.Tabela.Close;
end;

procedure TFCustas.Grid1DblClick(Sender: TObject);
begin
  btAdicionarClick(Sender);
end;

procedure TFCustas.btCalcularClick(Sender: TObject);
begin
  Calcular;
end;

end.
