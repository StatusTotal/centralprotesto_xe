object FCadastro: TFCadastro
  Left = 326
  Top = 202
  ActiveControl = edEmissao
  BorderStyle = bsDialog
  Caption = 'DEVEDOR'
  ClientHeight = 275
  ClientWidth = 501
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object P2: TsPanel
    Left = 7
    Top = 139
    Width = 486
    Height = 96
    TabOrder = 1
    SkinData.SkinSection = 'BARTITLE'
    object edEndereco: TsDBEdit
      Left = 73
      Top = 8
      Width = 406
      Height = 22
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'ENDERECO'
      DataSource = dsRXDevedor
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Endere'#231'o'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edBairro: TsDBEdit
      Left = 73
      Top = 37
      Width = 281
      Height = 22
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'BAIRRO'
      DataSource = dsRXDevedor
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Bairro'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object cbUF: TsDBComboBox
      Left = 73
      Top = 66
      Width = 45
      Height = 22
      Style = csDropDownList
      Color = clWhite
      DataField = 'UF'
      DataSource = dsRXDevedor
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemHeight = 16
      Items.Strings = (
        'AC'
        'AL'
        'AP'
        'AM'
        'BA'
        'CE'
        'DF'
        'ES'
        'GO'
        'MA'
        'MT'
        'MS'
        'MG'
        'PA'
        'PB'
        'PR'
        'PE'
        'PI'
        'RJ'
        'RN'
        'RS'
        'RO'
        'RR'
        'SC'
        'SP'
        'SE'
        'TO')
      ParentFont = False
      TabOrder = 3
      OnChange = cbUFChange
      BoundLabel.Active = True
      BoundLabel.Caption = 'UF'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
    end
    object lkCidade: TsDBLookupComboBox
      Left = 169
      Top = 66
      Width = 310
      Height = 22
      Color = clWhite
      DataField = 'MUNICIPIO'
      DataSource = dsRXDevedor
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyField = 'CIDADE'
      ListField = 'CIDADE'
      ListSource = dsMunicipios
      ParentFont = False
      TabOrder = 4
      BoundLabel.Active = True
      BoundLabel.Caption = 'Cidade'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
    end
    object edCep: TsDBEdit
      Left = 386
      Top = 37
      Width = 93
      Height = 22
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'CEP'
      DataSource = dsRXDevedor
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'CEP'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
  end
  object P1: TsPanel
    Left = 7
    Top = 7
    Width = 486
    Height = 125
    TabOrder = 0
    SkinData.SkinSection = 'BARTITLE'
    object edNome: TsDBEdit
      Left = 73
      Top = 37
      Width = 406
      Height = 22
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'NOME'
      DataSource = dsRXDevedor
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Nome'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edIdentidade: TsDBEdit
      Left = 73
      Top = 66
      Width = 132
      Height = 22
      Color = clWhite
      DataField = 'IDENTIDADE'
      DataSource = dsRXDevedor
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Identidade'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object lkOrgao: TsDBLookupComboBox
      Left = 73
      Top = 95
      Width = 406
      Height = 22
      Color = clWhite
      DataField = 'ORGAO'
      DataSource = dsRXDevedor
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyField = 'ID_ORGAO'
      ListField = 'DESCRICAO'
      ListSource = dsOrgaos
      ParentFont = False
      TabOrder = 8
      BoundLabel.Active = True
      BoundLabel.Caption = 'Descri'#231#227'o'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
    end
    object edEmissao: TsDBDateEdit
      Left = 260
      Top = 66
      Width = 93
      Height = 22
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 6
      Text = '  /  /    '
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.Caption = 'Emiss'#227'o'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      DataField = 'DT_EMISSAO'
      DataSource = dsRXDevedor
    end
    object edDocumento: TsDBEdit
      Left = 197
      Top = 8
      Width = 128
      Height = 22
      Color = clWhite
      DataField = 'DOCUMENTO'
      DataSource = dsRXDevedor
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Documento'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object cbTipoDev: TsDBComboBox
      Left = 73
      Top = 8
      Width = 47
      Height = 22
      Style = csDropDownList
      Color = clWhite
      DataField = 'TIPO'
      DataSource = dsRXDevedor
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemHeight = 16
      Items.Strings = (
        'F'
        'J')
      ParentFont = False
      TabOrder = 1
      OnChange = cbTipoDevChange
      BoundLabel.Active = True
      BoundLabel.Caption = 'Tipo'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
    end
    object cbOrgao: TsComboBox
      Left = 400
      Top = 66
      Width = 79
      Height = 22
      Alignment = taLeftJustify
      BoundLabel.Active = True
      BoundLabel.Caption = 'Org'#227'o'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemHeight = 16
      ItemIndex = 2
      ParentFont = False
      TabOrder = 7
      Text = 'OUTROS'
      OnClick = cbOrgaoClick
      Items.Strings = (
        'IFP'
        'DETRAN'
        'OUTROS')
    end
    object edTelefone: TsDBEdit
      Left = 386
      Top = 8
      Width = 93
      Height = 22
      Color = clWhite
      DataField = 'TELEFONE'
      DataSource = dsRXDevedor
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Telefone'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edOrdem: TsDBEdit
      Left = 8
      Top = 8
      Width = 24
      Height = 22
      Hint = 'ORDEM DO CADASTRO'
      TabStop = False
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'ORDEM'
      DataSource = dsRXDevedor
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
  end
  object ckIgnorado: TsDBCheckBox
    Left = 5
    Top = 246
    Width = 130
    Height = 20
    Hint = 'INDICA ENDERE'#199'O IGNORADO/INCERTO E N'#195'O SABIDO'
    TabStop = False
    Caption = 'Endere'#231'o Ignorado'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnClick = ckIgnoradoClick
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
    DataField = 'IGNORADO'
    DataSource = dsRXDevedor
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object btSalvar: TsBitBtn
    Left = 337
    Top = 243
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Caption = 'Salvar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = btSalvarClick
    SkinData.SkinSection = 'PAGECONTROL'
  end
  object btCancelar: TsBitBtn
    Left = 418
    Top = 243
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Caption = 'Cancelar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnClick = btCancelarClick
    SkinData.SkinSection = 'PAGECONTROL'
  end
  object dsRXDevedor: TDataSource
    DataSet = dm.RxDevedor
    Left = 285
    Top = 360
  end
  object dsMunicipios: TDataSource
    DataSet = dm.Municipios
    Left = 216
    Top = 360
  end
  object dsOrgaos: TDataSource
    DataSet = dm.Orgaos
    Left = 349
    Top = 360
  end
end
