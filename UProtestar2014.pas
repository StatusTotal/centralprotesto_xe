unit UProtestar2014;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, sDBLookupComboBox, StdCtrls, Buttons, sBitBtn, sEdit,
  Mask, sMaskEdit, sCustomComboEdit, sTooledit, ExtCtrls, sPanel, DB,
  sCheckBox, sGroupBox, acPNG, FMTBcd, SqlExpr, Grids, DBGrids,
  acDBGrid, Menus;

type
  TFProtestar2014 = class(TForm)
    P1: TsPanel;
    dsEscreventes: TDataSource;
    dsMensagens: TDataSource;
    Grid: TsDBGrid;
    dsBaixas: TDataSource;
    ckImprime: TsCheckBox;
    btConfirmar: TsBitBtn;
    btCancelar: TsBitBtn;
    PM: TPopupMenu;
    A1: TMenuItem;
    A2: TMenuItem;
    C1: TMenuItem;
    A3: TMenuItem;
    C2: TMenuItem;
    procedure btConfirmarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dsBaixasStateChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GridExit(Sender: TObject);
    procedure GridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure A1Click(Sender: TObject);
    procedure A2Click(Sender: TObject);
    procedure C1Click(Sender: TObject);
    procedure A3Click(Sender: TObject);
    procedure C2Click(Sender: TObject);
  private
    { Private declarations }
    vLivroInicial,vFolhaInicial: String;
  public
    { Public declarations }
  end;

var
  FProtestar2014: TFProtestar2014;

implementation

uses UDM, UPF, UGeral, UGdm;

{$R *.dfm}

procedure TFProtestar2014.btConfirmarClick(Sender: TObject);
begin
  dm.vOkGeral:=True;
  Close;
end;

procedure TFProtestar2014.btCancelarClick(Sender: TObject);
begin
  dm.vOkGeral:=False;
  Gdm.LiberarSelo('PROTESTO','',False,-1,dm.cBaixasID_RESERVADO.AsInteger);
  GR.ExecutarSQLDAC('UPDATE CONTROLE SET VALOR='''+vLivroInicial+'''WHERE SIGLA='''+'LRG'+'''',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE CONTROLE SET VALOR='''+vFolhaInicial+'''WHERE SIGLA='''+'FRG'+'''',dm.conSISTEMA);
  Close;
end;

procedure TFProtestar2014.FormCreate(Sender: TObject);
begin
  dm.Escreventes.Close;
  dm.Escreventes.Open;
  dm.Mensagens.Close;
  dm.Mensagens.Open;
end;

procedure TFProtestar2014.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if not dm.vOkGeral then
  btCancelar.Click;
end;

procedure TFProtestar2014.dsBaixasStateChange(Sender: TObject);
begin
  if dsBaixas.DataSet.State=dsInsert then dsBaixas.DataSet.Cancel;
end;

procedure TFProtestar2014.FormShow(Sender: TObject);
begin
  vLivroInicial:=dm.cBaixasLIVRO.AsString;
  vFolhaInicial:=dm.cBaixasFOLHA.AsString;
end;

procedure TFProtestar2014.GridExit(Sender: TObject);
begin
  if dm.cBaixas.State=dsEdit then dm.cBaixas.Post;
end;

procedure TFProtestar2014.GridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Shift=[ssCtrl]) and (Key=46) then key:=0;
end;

procedure TFProtestar2014.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFProtestar2014.A1Click(Sender: TObject);
var
  P: Pointer;
  vData: String;
begin
  PF.Aguarde(True);
  vData:=FormatDateTime('dd/mm/yyyy',dm.cBaixasDATA.AsDateTime);
  P:=dm.cBaixas.GetBookmark;
  dm.cBaixas.DisableControls;
  while not dm.cBaixas.Eof do
  begin
      dm.cBaixas.Edit;
      dm.cBaixasDATA.AsDateTime:=StrToDate(vData);
      dm.cBaixas.Post;
      dm.cBaixas.Next;
  end;
  if dm.cBaixas.BookmarkValid(P) then
  dm.cBaixas.GotoBookmark(P);
  dm.cBaixas.EnableControls;
  PF.Aguarde(False);
end;

procedure TFProtestar2014.A2Click(Sender: TObject);
var
  P: Pointer;
  vIdMsg: Integer;
begin
  PF.Aguarde(True);
  vIdMsg:=dm.cBaixasID_MSG.AsInteger;
  P:=dm.cBaixas.GetBookmark;
  dm.cBaixas.DisableControls;
  while not dm.cBaixas.Eof do
  begin
      dm.cBaixas.Edit;
      dm.cBaixasID_MSG.AsInteger:=vIdMsg;
      dm.cBaixas.Post;
      dm.cBaixas.Next;
  end;
  if dm.cBaixas.BookmarkValid(P) then
  dm.cBaixas.GotoBookmark(P);
  dm.cBaixas.EnableControls;
  PF.Aguarde(False);
end;

procedure TFProtestar2014.C1Click(Sender: TObject);
var
  P: Pointer;
  vLivro: Integer;
begin
  PF.Aguarde(True);
  vLivro:=dm.cBaixasLIVRO.AsInteger;
  P:=dm.cBaixas.GetBookmark;
  dm.cBaixas.DisableControls;
  while not dm.cBaixas.Eof do
  begin
      dm.cBaixas.Edit;
      dm.cBaixasLIVRO.AsInteger:=vLivro;
      dm.cBaixas.Post;
      dm.cBaixas.Next;
  end;
  if dm.cBaixas.BookmarkValid(P) then
  dm.cBaixas.GotoBookmark(P);
  dm.cBaixas.EnableControls;
  PF.Aguarde(False);
end;

procedure TFProtestar2014.A3Click(Sender: TObject);
var
  P: Pointer;
  vFolha: Integer;
  vInicial: String;
begin
  PF.Aguarde(True);
  if InputQuery('A partir de','Folha Inicial',vInicial) then
  begin
      vFolha:=StrToInt(vInicial);
      P:=dm.cBaixas.GetBookmark;
      dm.cBaixas.DisableControls;
      while not dm.cBaixas.Eof do
      begin
          dm.cBaixas.Edit;
          dm.cBaixasFOLHA.AsInteger:=vFolha;
          Inc(vFolha);
          dm.cBaixas.Post;
          dm.cBaixas.Next;
      end;
      if dm.cBaixas.BookmarkValid(P) then
      dm.cBaixas.GotoBookmark(P);
      dm.cBaixas.EnableControls;
  end;
  PF.Aguarde(False);
end;

procedure TFProtestar2014.C2Click(Sender: TObject);
var
  P: Pointer;
  vCPFEscrevente: String;
begin
  PF.Aguarde(True);
  vCPFEscrevente:=dm.cBaixasCPF_ESCREVENTE.AsString;
  P:=dm.cBaixas.GetBookmark;
  dm.cBaixas.DisableControls;
  while not dm.cBaixas.Eof do
  begin
      dm.cBaixas.Edit;
      dm.cBaixasCPF_ESCREVENTE.AsString:=vCPFEscrevente;
      dm.cBaixas.Post;
      dm.cBaixas.Next;
  end;
  if dm.cBaixas.BookmarkValid(P) then
  dm.cBaixas.GotoBookmark(P);
  dm.cBaixas.EnableControls;
  PF.Aguarde(False);
end;

end.
