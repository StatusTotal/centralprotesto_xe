�
 TFQUICKCANCELADO2 0"4  TPF0TFQuickCancelado2FQuickCancelado2LeftTop� Caption
Cancelado2ClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderScaledOnCreate
FormCreatePixelsPerInch`
TextHeight 	TQuickRep	CanceladoLeft(Top� WidthHeightcShowingPreviewFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.ContinuousPage.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPagePrinterSettings.LastPage"PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex"PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode	PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOptionPrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPreviewWidth�PreviewHeight�PrevInitialZoomqrZoomToWidthPreviewDefaultSaveTypestQRPPreviewLeft 
PreviewTop  TQRBandQRBand1Left9Top9Width�Height� Frame.DrawTop	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUU��@      :�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbTitle TQRLabellbCidadeLeft�TopEWidthiHeightSize.Values��������@      ��@      ��@      �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionCidade - EstadoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	lbTitularLeftTop.WidthkHeightSize.Values��������@������ֽ@������j�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionNome do TitularColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbCartorioLeft� TopWidth� HeightSize.Values������j�@UUUUUUm�@      ��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   NOME DO CARTÓRIOColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrTipoLeft� Top� WidthBHeightSize.Values������J�@UUUUUU��@UUUUUUU�@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   AVERBAÇÃO DE CANCELAMENTOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbReciboLeft?Top� Width+HeightSize.Values�������@VUUUUU�@VUUUUU�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionRecibo:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	   TQRChildBand
ChildBand1Left9Top� Width�HeightOFrame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUU�@      :�	@ PreCaluculateBandHeightKeepOnOnePage
ParentBandQRBand1
PrintOrdercboAfterParent TQRRichTextRTLeftTopWidth�HeightHSize.Values      ��@UUUUUUU�@UUUUUUU�@UUUUUU��	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAutoStretchColorclWindowFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style HiresExportTransparent
YIncrement2ParentRichEditRE   TQRChildBand
ChildBand2Left9Top;Width�HeightiFrame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUU��@      :�	@ PreCaluculateBandHeightKeepOnOnePage
ParentBand
ChildBand1
PrintOrdercboAfterParent TQRShapeShapeLeftTopWidth�HeightGSize.Values������ڻ@       �@      ��@UUUUUU��	@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRLabellbDataLeft� Top� Width�HeightSize.Values�������@UUUUUU��@��������@�������	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaptionCidade, DataColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRShapeQRShape2Left� Top� WidthRHeightSize.ValuesUUUUUUU� @������
�@UUUUUU��@��������@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabel
lbTabeliaoLeft�Top� Width!HeightSize.Values�������@������J�	@������B�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption	lbTitularColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbMatriculaLeft�Top� Width&HeightSize.Values�������@UUUUUU��	@UUUUUU��@UUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption	MatriculaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel3LeftTopWidth�HeightSize.Values������
�@UUUUUUU�@UUUUUU-�@��������	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	AutoSizeAutoStretch	Caption�   Esta Averbação é gerada de forma eletrônica, qualquer forma de rasura ou complementação a tornará como inválida, ou como princípio de fraude. (Art. 559 - Resolução 01/2000- CGJERJ) - Válida somente se aposta de selo de fiscalização.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRMemoMLeftTopTWidth�HeightSize.Values������*�@UUUUUUU�@UUUUUU��@��������	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretch	ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentFullJustifyMaxBreakChars FontSize  TQRLabelQRLabel1Left8TopFWidthhHeightSize.ValuesUUUUUU��@������ڻ	@��������@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionTaxas e Emolumentos:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	lbProcoloLeftTopWidth;HeightSize.Values�������@UUUUUUU�@UUUUUU�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption
Protocolo:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabellbTituloLeftfTopWidth2HeightSize.Values�������@UUUUUU�	@UUUUUU�@������J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption   Nº Título:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabellbValorLeftdTop@Width4HeightSize.Values�������@������g�	@UUUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption	Valor R$:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabellbVencimentoLeftPTop*WidthHHeightSize.Values�������@��������	@     @�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionVencimento:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabel	lbCedenteLeftTop*Width�HeightSize.Values�������@UUUUUUU�@     @�@UUUUUU_�	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionCedente:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabel
lbPortadorLeftTop@Width�HeightSize.Values�������@UUUUUUU�@UUUUUUU�@UUUUUU_�	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	Portador:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabellbCGJ1Left0Top� WidthhHeightSize.Values�������@       �@UUUUUU-�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption   Poder Judiciário - TJERJColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ2Left%Top� Width}HeightSize.Values�������@��������@��������@UUUUUU]�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption   Corregedoria Geral da JustiçaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ3Left#Top� Width� HeightSize.Values�������@UUUUUU5�@      H�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption"   Selo de Fiscalização EletrônicoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbSeloEletronicoLeft9Top� WidthVHeightSize.Values�������@      Ж@������z�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaptionLLLL 00000 XXXColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ4Left Top� Width� HeightSize.Values�������@UUUUUUU�@UUUUUU��@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaptionConsulte a validade do selo em:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ5LeftTop� Width� HeightSize.Values�������@      ��@UUUUUU��@UUUUUU5�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption$https://www3.tjrj.jus.br/sitepublicoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize    	TRichEditRELeftTopWidth�HeightFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTabOrderZoomd   