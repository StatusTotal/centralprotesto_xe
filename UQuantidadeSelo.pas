unit UQuantidadeSelo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TFQuantidadeSelo = class(TForm)
    Timer: TTimer;
    Panel1: TPanel;
    lbSerie: TLabel;
    lbCCT: TLabel;
    Bevel1: TBevel;
    procedure TimerTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lbSerieMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    vQ1,vQ2: Integer;
  public
    { Public declarations }
  end;

var
  FQuantidadeSelo: TFQuantidadeSelo;

implementation

uses UGDM, UDM, UGeral;

{$R *.dfm}

procedure TFQuantidadeSelo.TimerTimer(Sender: TObject);
begin
  vQ1:=Gdm.QtdSeloAtual(dm.SerieAtual);
  vQ2:=Gdm.QtdSeloAtual(-1);

  lbSerie.Caption :='SELO: '+Gdm.LetraAtual(dm.SerieAtual)+' = '+GR.Zeros(IntToStr(vQ1),'D',6)+' ';
  lbCCT.Caption   :='CCT: '+Gdm.LetraAtual(-1)+' = '+GR.Zeros(IntToStr(vQ2),'D',6)+' ';

  if (vQ1>=10) and (vQ1<=15) then
    lbSerie.Color:=clYellow
      else
        if vQ1<10 then
          lbSerie.Color:=$008080FF
            else lbSerie.Color:=$0080FF80;

  if (vQ2>=10) and (vQ2<=15) then
    lbCCT.Color:=clYellow
      else
        if vQ2<10 then
          lbCCT.Color:=$008080FF
            else lbCCT.Color:=$0080FF80;
end;

procedure TFQuantidadeSelo.FormCreate(Sender: TObject);
begin
  vQ1:=Gdm.QtdSeloAtual(dm.SerieAtual);
  vQ2:=Gdm.QtdSeloAtual(-1);

  lbSerie.Caption :='SELO: '+Gdm.LetraAtual(dm.SerieAtual)+' = '+GR.Zeros(IntToStr(vQ1),'D',6)+' ';
  lbCCT.Caption   :='CCT: '+Gdm.LetraAtual(-1)+' = '+GR.Zeros(IntToStr(vQ2),'D',6)+' ';

  if (vQ1>=10) and (vQ1<=15) then
    lbSerie.Color:=clYellow
      else
        if vQ1<10 then
          lbSerie.Color:=$008080FF
            else lbSerie.Color:=$0080FF80;

  if (vQ2>=10) and (vQ2<=15) then
    lbCCT.Color:=clYellow
      else
        if vQ2<10 then
          lbCCT.Color:=$008080FF
            else lbCCT.Color:=$0080FF80;
end;

procedure TFQuantidadeSelo.lbSerieMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  ReleaseCapture;
  PostMessage(Self.Handle,WM_SYSCOMMAND,$F012,0);
end;

procedure TFQuantidadeSelo.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Application.Terminate;
end;

end.
