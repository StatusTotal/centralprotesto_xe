�
 TFQUICKPAGAMENTO1 0��  TPF0TFQuickPagamento1FQuickPagamento1Left� ToptCaption
Pagamento1ClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderScaledOnCreate
FormCreatePixelsPerInch`
TextHeight 	TQuickRep	Certidao2Left[TopfWidthHeightkShowingPreviewFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeCustomPage.ContinuousPage.Values       �@      �
@       �@������L�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCode	PrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPreviewWidth�PreviewHeight�PrevInitialZoomqrZoomToFitPreviewDefaultSaveTypestQRPPreviewLeft 
PreviewTop  TQRBandDetail2LeftLTop9Width�HeightFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values      ��@VUUUUU��	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail TQRLabelqrPagamento2LeftTop
Width� HeightEnabledSize.Values       �@VUUUUU��@��������@      Ț@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption   PAGAMENTO DE TÍTULOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbPortador3LeftTop"Width2HeightEnabledSize.Values�������@VUUUUU��@�������@������J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionPortadorColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel
qrDeclaro2LeftTop2Width~Height'EnabledSize.Values      `�@VUUUUU��@������J�@VUUUUU�	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretch	Captionk   Declaro que me foi entregue o pagamento do título abaixo discriminado, face o PAGAMENTO INTEGRAL do mesmo.ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsBoldfsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbDevedor3LeftTopZWidth/HeightEnabledSize.Values�������@VUUUUU��@�������@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionDevedorColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbDocumento3LeftTopjWidthCHeightEnabledSize.Values�������@VUUUUU��@������:�@UUUUUUE�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption	DocumentoColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel
lbEspecie3LeftTopzWidth0HeightEnabledSize.Values�������@VUUUUU��@UUUUUUe�@       �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   EspécieColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel	lbNumero3LeftTop� WidthDHeightEnabledSize.Values�������@VUUUUU��@      ��@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   Nº do TítuloColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbValor2LeftTop� Width-HeightEnabledSize.Values�������@VUUUUU��@UUUUUU��@       �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionValoresColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel
lbSacador3LeftTop� Width1HeightEnabledSize.Values�������@VUUUUU��@      �@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionSacadorColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel
lbCedente3LeftTop� Width1HeightEnabledSize.Values�������@VUUUUU��@UUUUUU��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionCedenteColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbForma3LeftTop� Width,HeightEnabledSize.Values�������@VUUUUU��@������2�@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionAtravesColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbProtocolo3LeftTop
Width~HeightEnabledSize.Values       �@��������	@��������@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption	PROTOCOLOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalicfsUnderline 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbSelo3Left
Top� WidthHeightEnabledSize.Values�������@��������@      Ț@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionSeloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbData3LeftNTop� Width;HeightEnabledSize.Values�������@UUUUUU!�	@      �@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionCidade, DataColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShapeQRShape1Left�Top� Width� HeightEnabledSize.ValuesUUUUUUU� @      ��	@UUUUUU]�@UUUUUU��@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabelqrValortitulo2LeftTop� WidthTHeightEnabledSize.Values�������@VUUUUU��@��������@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   Valor do TítuloColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
    	TQuickRepCertidaoTagLeft TopWidthHeightcShowingPreviewFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeCustomPage.ContinuousPage.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCode	PrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPreviewWidth�PreviewHeight�PrevInitialZoomqrZoomToWidthPreviewDefaultSaveTypestQRPPreviewLeft 
PreviewTop  TQRBandDetailLeftLTop9Width�HeightAlignToBottomBeforePrintDetailBeforePrintTransparentBandForceNewColumnForceNewPage	Size.Values��������
@VUUUUU��	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail TQRLabellbLinhaLeftTop�Width HeightSize.Values     @�@UUUUUUU�@TUUUUU��	@       �	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption� - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial Narrow
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShapeQRShape2Left Top� Width�HeightSize.Values      V�	@          UUUUUUU�@��������	@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRLabellbCidadeLeft$TopWidthNHeightSize.Values�������@UUUUUU%�@       �@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionCidade - EstadoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabel	lbTitularLeft#Top'WidthPHeightSize.Values�������@      |�@      `�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionNome do TitularColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabel
lbCartorioLeft� TopWidth� HeightSize.Values��������@      ��@��������@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   NOME DO CARTÓRIOColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel2Left� TopfWidthsHeightSize.Values       �@������ҿ@      ��@������f�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption!   CERTIDÃO DE PAGAMENTO DE TÍTULOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel4LeftTop� WidthoHeightSize.Values��������@UUUUUUU�@      ��@      ؒ@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionRecebemos de:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	lbDevedorLeftTop� WidthgHeightSize.Values�������@UUUUUUU�@      H�@������B�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionNome do DevedorColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbDocumentoLeftTop� WidthCHeightSize.Values�������@UUUUUUU�@������r�@UUUUUUE�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption	DocumentoColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbValorPagoLeftTop� WidthAHeightSize.Values�������@UUUUUUU�@������N�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption
Valor PagoColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbFormaLeftTop� Width,HeightSize.Values�������@UUUUUUU�@      �@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   AtravésColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabelQRLabel5LeftTop� Width�HeightSize.Values��������@UUUUUUU�@      ̘@�������	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionE   Referentes ao pagamento do título com as seguintes discriminações:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	lbEspecieLeftTop� WidthiHeightSize.Values�������@UUUUUUU�@UUUUUU]�@      �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   Espécie do Título:ColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbNumeroLeftTopWidthHHeightSize.Values�������@UUUUUUU�@UUUUUUE�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   Nº do Título:ColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel
lbPortadorLeftTop\Width6HeightSize.Values�������@UUUUUUU�@     0�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption	Portador:ColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel	lbSacadorLeftToplWidth5HeightSize.Values�������@UUUUUUU�@TUUUUU��@������:�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionSacador:ColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel	lbCedenteLeftTop|Width5HeightSize.Values�������@UUUUUUU�@������Z�@������:�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionCedente:ColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbAvisoLeftTop�Width�Height)Size.ValuesUUUUUU��@UUUUUUU�@UUUUUU��	@��������	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption�   Ficando assim o devedor quites com este Recibo, APÓS A COMPENSAÇÃO do boleto objeto do pagamento, feito através da câmara de compensação. (Lei 9492/97, Artigo 19 §3º).ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbDataLeftgTop�WidthDHeightSize.Values�������@������v�@UUUUUU}�	@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaptionCidade, DataColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShapesh1Left� Top�WidthRHeightSize.ValuesUUUUUUU� @������*�@�������	@��������@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabellbNome1LeftxTop Width!HeightSize.Values�������@TUUUUU��@UUUUUUU�	@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption	lbTitularColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbInfo1LeftvTopWidth&HeightSize.Values�������@������b�@��������	@UUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption	MatriculaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel3LeftTop0WidthqHeightSize.Values������
�@      ��@UUUUUU5�	@��������	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeAutoStretch	Caption�   Esta Certidão é gerada de forma eletrônica, qualquer forma de rasura ou complementação a tornará como inválida, ou como princípio de fraude. (Art. 559 - Resolução 01/2000- CGJERJ) - Válida somente se aposta de selo de fiscalização.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRMemoMLeftTopnWidthqHeight� Size.Values�������@      ��@��������	@��������	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentFullJustifyMaxBreakChars FontSize  TQRLabelQRLabel1Left�Top`Width� HeightSize.ValuesUUUUUU��@UUUUUU_�	@UUUUUU�	@UUUUUU%�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionTaxas e Emolumentos:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbProtocoloLeftTop�WidthBHeightSize.Values��������@UUUUUUU�@������.�	@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption	ProtocoloColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsBoldfsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbEnderecoLeft4Top6Width.HeightSize.Values�������@��������@      ��@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption	   EndereçoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabellbDadosLeft$TopEWidthMHeightSize.Values�������@UUUUUU%�@      ��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionTelefone / eMailColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabellbCNPJLeft<TopTWidthHeightSize.Values�������@UUUUUU�@     @�@UUUUUUu�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionCNPJColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabelqrPagamentoLeftTopWidth� HeightSize.Values       �@VUUUUU��@UUUUUU&�
@      Ț@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption   PAGAMENTO DE TÍTULOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbPortador2LeftTop%Width2HeightSize.Values�������@VUUUUU��@UUUUUU�
@������J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionPortadorColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel	qrDeclaroLeftTop5Width~Height'Size.Values      `�@VUUUUU��@������Ç
@VUUUUU�	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretch	Captionk   Declaro que me foi entregue o pagamento do título abaixo discriminado, face o PAGAMENTO INTEGRAL do mesmo.ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsBoldfsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbDevedor2LeftTop]Width/HeightSize.Values�������@VUUUUU��@      a�
@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionDevedorColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbDocumento2LeftTopmWidthCHeightSize.Values�������@VUUUUU��@UUUUUU�
@UUUUUUE�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption	DocumentoColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel
lbEspecie2LeftTop}Width0HeightSize.Values�������@VUUUUU��@��������
@       �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   EspécieColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel	lbNumero2LeftTop�WidthDHeightSize.Values�������@VUUUUU��@      Q�
@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   Nº do TítuloColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbValorLeftTop�Width-HeightSize.Values�������@VUUUUU��@��������
@       �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionValoresColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel
lbSacador2LeftTop�Width1HeightSize.Values�������@VUUUUU��@      A�
@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionSacadorColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel
lbCedente2LeftTop�Width1HeightSize.Values�������@VUUUUU��@UUUUUU�
@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionCedenteColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbForma2LeftTop�Width,HeightSize.Values�������@VUUUUU��@��������
@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionAtravesColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbProtocolo2LeftTopWidth~HeightSize.Values       �@��������	@UUUUUU&�
@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption	PROTOCOLOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalicfsUnderline 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbSelo2LeftTop�WidthHeightSize.Values�������@VUUUUU��@      1�
@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionSeloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbData2LeftNTop�Width;HeightSize.Values�������@UUUUUU!�	@      9�
@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionCidade, DataColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShapeQRShape3Left�Top�Width� HeightSize.ValuesUUUUUUU� @      ��	@UUUUUU֨
@UUUUUU��@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabellbValorTituloLeftTop<WidthXHeightSize.Values�������@UUUUUUU�@UUUUUU�@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   Valor do Título:ColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbCGJ1Left Top�WidthhHeightSize.Values�������@UUUUUUU�@UUUUUUu�	@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption   Poder Judiciário - TJERJColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ2LeftTop�Width}HeightSize.Values�������@     @�@�������	@UUUUUU]�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption   Corregedoria Geral da JustiçaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ3LeftTop�Width� HeightSize.Values�������@UUUUUU�@      ��	@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption"   Selo de Fiscalização EletrônicoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbSeloEletronicoLeft)Top�WidthVHeightSize.Values�������@UUUUUU��@�������	@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaptionLLLL 00000 XXXColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ4LeftTop Width� HeightSize.Values�������@UUUUUUU�@UUUUUUU�	@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaptionConsulte a validade do selo em:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ5LeftTopWidth� HeightSize.Values�������@������*�@��������	@UUUUUU5�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption$https://www3.tjrj.jus.br/sitepublicoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellb2ViaLeft8Top� Width&HeightSize.Values�������@      `�@UUUUUUM�@UUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption	   (2ª Via)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabellbSaldoTituloLeftTopLWidth[HeightSize.Values�������@UUUUUUU�@��������@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   Saldo do Título:ColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabelqrValorTituloLeftTop�WidthTHeightSize.Values�������@VUUUUU��@UUUUUU��
@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   Valor do TítuloColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbDataTituloLeftTop,WidthUHeightSize.Values�������@UUUUUUU�@      p�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   Data do Título:ColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbSeloLeftTopWidthHeightSize.Values�������@UUUUUUU�@������ڻ@������
�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionSelo:ColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
    TFDQuery
qryImprime
Connectiondm.conSISTEMASQL.Stringsselect * from TITULOS    where ID_ATO in (:Lista)    order by Protocolo Left Toph	ParamDataNameLISTADataTypeftString	ParamTypeptInput   TIntegerFieldqryImprimeID_ATO	FieldNameID_ATOOriginID_ATOProviderFlags
pfInUpdate	pfInWherepfInKey Required	  TIntegerFieldqryImprimeCODIGO	FieldNameCODIGOOriginCODIGO  TIntegerFieldqryImprimeRECIBO	FieldNameRECIBOOriginRECIBO  
TDateFieldqryImprimeDT_ENTRADA	FieldName
DT_ENTRADAOrigin
DT_ENTRADA  
TDateFieldqryImprimeDT_PROTOCOLO	FieldNameDT_PROTOCOLOOriginDT_PROTOCOLO  TIntegerFieldqryImprimePROTOCOLO	FieldName	PROTOCOLOOrigin	PROTOCOLO  TIntegerFieldqryImprimeLIVRO_PROTOCOLO	FieldNameLIVRO_PROTOCOLOOriginLIVRO_PROTOCOLO  TStringFieldqryImprimeFOLHA_PROTOCOLO	FieldNameFOLHA_PROTOCOLOOriginFOLHA_PROTOCOLOSize
  
TDateFieldqryImprimeDT_PRAZO	FieldNameDT_PRAZOOriginDT_PRAZO  
TDateFieldqryImprimeDT_REGISTRO	FieldNameDT_REGISTROOriginDT_REGISTRO  TIntegerFieldqryImprimeREGISTRO	FieldNameREGISTROOriginREGISTRO  TIntegerFieldqryImprimeLIVRO_REGISTRO	FieldNameLIVRO_REGISTROOriginLIVRO_REGISTRO  TStringFieldqryImprimeFOLHA_REGISTRO	FieldNameFOLHA_REGISTROOriginFOLHA_REGISTROSize
  TStringFieldqryImprimeSELO_REGISTRO	FieldNameSELO_REGISTROOriginSELO_REGISTROSize	  
TDateFieldqryImprimeDT_PAGAMENTO	FieldNameDT_PAGAMENTOOriginDT_PAGAMENTO  TStringFieldqryImprimeSELO_PAGAMENTO	FieldNameSELO_PAGAMENTOOriginSELO_PAGAMENTOSize	  TIntegerFieldqryImprimeRECIBO_PAGAMENTO	FieldNameRECIBO_PAGAMENTOOriginRECIBO_PAGAMENTO  TStringFieldqryImprimeCOBRANCA	FieldNameCOBRANCAOriginCOBRANCA	FixedChar	Size  TFloatFieldqryImprimeEMOLUMENTOS	FieldNameEMOLUMENTOSOriginEMOLUMENTOS  TFloatFieldqryImprimeFETJ	FieldNameFETJOriginFETJ  TFloatFieldqryImprimeFUNDPERJ	FieldNameFUNDPERJOriginFUNDPERJ  TFloatFieldqryImprimeFUNPERJ	FieldNameFUNPERJOriginFUNPERJ  TFloatFieldqryImprimeFUNARPEN	FieldNameFUNARPENOriginFUNARPEN  TFloatFieldqryImprimePMCMV	FieldNamePMCMVOriginPMCMV  TFloatFieldqryImprimeISS	FieldNameISSOriginISS  TFloatFieldqryImprimeMUTUA	FieldNameMUTUAOriginMUTUA  TFloatFieldqryImprimeDISTRIBUICAO	FieldNameDISTRIBUICAOOriginDISTRIBUICAO  TFloatFieldqryImprimeACOTERJ	FieldNameACOTERJOriginACOTERJ  TFloatFieldqryImprimeTOTAL	FieldNameTOTALOriginTOTAL  TIntegerFieldqryImprimeTIPO_PROTESTO	FieldNameTIPO_PROTESTOOriginTIPO_PROTESTO  TIntegerFieldqryImprimeTIPO_TITULO	FieldNameTIPO_TITULOOriginTIPO_TITULO  TStringFieldqryImprimeNUMERO_TITULO	FieldNameNUMERO_TITULOOriginNUMERO_TITULO  
TDateFieldqryImprimeDT_TITULO	FieldName	DT_TITULOOrigin	DT_TITULO  TStringFieldqryImprimeBANCO	FieldNameBANCOOriginBANCO  TStringFieldqryImprimeAGENCIA	FieldNameAGENCIAOriginAGENCIASize
  TStringFieldqryImprimeCONTA	FieldNameCONTAOriginCONTASize
  TFloatFieldqryImprimeVALOR_TITULO	FieldNameVALOR_TITULOOriginVALOR_TITULO  TFloatFieldqryImprimeSALDO_PROTESTO	FieldNameSALDO_PROTESTOOriginSALDO_PROTESTO  
TDateFieldqryImprimeDT_VENCIMENTO	FieldNameDT_VENCIMENTOOriginDT_VENCIMENTO  TStringFieldqryImprimeCONVENIO	FieldNameCONVENIOOriginCONVENIO	FixedChar	Size  TStringFieldqryImprimeTIPO_APRESENTACAO	FieldNameTIPO_APRESENTACAOOriginTIPO_APRESENTACAO	FixedChar	Size  
TDateFieldqryImprimeDT_ENVIO	FieldNameDT_ENVIOOriginDT_ENVIO  TStringFieldqryImprimeCODIGO_APRESENTANTE	FieldNameCODIGO_APRESENTANTEOriginCODIGO_APRESENTANTESize
  TStringFieldqryImprimeTIPO_INTIMACAO	FieldNameTIPO_INTIMACAOOriginTIPO_INTIMACAO	FixedChar	Size  
TMemoFieldqryImprimeMOTIVO_INTIMACAO	FieldNameMOTIVO_INTIMACAOOriginMOTIVO_INTIMACAOBlobTypeftMemo  
TDateFieldqryImprimeDT_INTIMACAO	FieldNameDT_INTIMACAOOriginDT_INTIMACAO  
TDateFieldqryImprimeDT_PUBLICACAO	FieldNameDT_PUBLICACAOOriginDT_PUBLICACAO  TFloatFieldqryImprimeVALOR_PAGAMENTO	FieldNameVALOR_PAGAMENTOOriginVALOR_PAGAMENTO  TStringFieldqryImprimeAPRESENTANTE	FieldNameAPRESENTANTEOriginAPRESENTANTESized  TStringFieldqryImprimeCPF_CNPJ_APRESENTANTE	FieldNameCPF_CNPJ_APRESENTANTEOriginCPF_CNPJ_APRESENTANTESize  TStringFieldqryImprimeTIPO_APRESENTANTE	FieldNameTIPO_APRESENTANTEOriginTIPO_APRESENTANTE	FixedChar	Size  TStringFieldqryImprimeCEDENTE	FieldNameCEDENTEOriginCEDENTESized  TStringFieldqryImprimeNOSSO_NUMERO	FieldNameNOSSO_NUMEROOriginNOSSO_NUMEROSize  TStringFieldqryImprimeSACADOR	FieldNameSACADOROriginSACADORSized  TStringFieldqryImprimeDEVEDOR	FieldNameDEVEDOROriginDEVEDORSized  TStringFieldqryImprimeCPF_CNPJ_DEVEDOR	FieldNameCPF_CNPJ_DEVEDOROriginCPF_CNPJ_DEVEDORSize  TStringFieldqryImprimeTIPO_DEVEDOR	FieldNameTIPO_DEVEDOROriginTIPO_DEVEDOR	FixedChar	Size  TStringFieldqryImprimeAGENCIA_CEDENTE	FieldNameAGENCIA_CEDENTEOriginAGENCIA_CEDENTESize  TStringFieldqryImprimePRACA_PROTESTO	FieldNamePRACA_PROTESTOOriginPRACA_PROTESTO  TStringFieldqryImprimeTIPO_ENDOSSO	FieldNameTIPO_ENDOSSOOriginTIPO_ENDOSSO	FixedChar	Size  TStringFieldqryImprimeACEITE	FieldNameACEITEOriginACEITE	FixedChar	Size  TStringFieldqryImprimeCPF_ESCREVENTE	FieldNameCPF_ESCREVENTEOriginCPF_ESCREVENTESize  TStringFieldqryImprimeCPF_ESCREVENTE_PG	FieldNameCPF_ESCREVENTE_PGOriginCPF_ESCREVENTE_PGSize  
TMemoFieldqryImprimeOBSERVACAO	FieldName
OBSERVACAOOrigin
OBSERVACAOBlobTypeftMemo  
TDateFieldqryImprimeDT_SUSTADO	FieldName
DT_SUSTADOOrigin
DT_SUSTADO  
TDateFieldqryImprimeDT_RETIRADO	FieldNameDT_RETIRADOOriginDT_RETIRADO  TStringFieldqryImprimeSTATUS	FieldNameSTATUSOriginSTATUS  TStringFieldqryImprimePROTESTADO	FieldName
PROTESTADOOrigin
PROTESTADO	FixedChar	Size  TStringFieldqryImprimeENVIADO_APONTAMENTO	FieldNameENVIADO_APONTAMENTOOriginENVIADO_APONTAMENTO	FixedChar	Size  TStringFieldqryImprimeENVIADO_PROTESTO	FieldNameENVIADO_PROTESTOOriginENVIADO_PROTESTO	FixedChar	Size  TStringFieldqryImprimeENVIADO_PAGAMENTO	FieldNameENVIADO_PAGAMENTOOriginENVIADO_PAGAMENTO	FixedChar	Size  TStringFieldqryImprimeENVIADO_RETIRADO	FieldNameENVIADO_RETIRADOOriginENVIADO_RETIRADO	FixedChar	Size  TStringFieldqryImprimeENVIADO_SUSTADO	FieldNameENVIADO_SUSTADOOriginENVIADO_SUSTADO	FixedChar	Size  TStringFieldqryImprimeENVIADO_DEVOLVIDO	FieldNameENVIADO_DEVOLVIDOOriginENVIADO_DEVOLVIDO	FixedChar	Size  TStringFieldqryImprimeEXPORTADO	FieldName	EXPORTADOOrigin	EXPORTADO	FixedChar	Size  TStringFieldqryImprimeAVALISTA_DEVEDOR	FieldNameAVALISTA_DEVEDOROriginAVALISTA_DEVEDOR	FixedChar	Size  TStringFieldqryImprimeFINS_FALIMENTARES	FieldNameFINS_FALIMENTARESOriginFINS_FALIMENTARES	FixedChar	Size  TFloatFieldqryImprimeTARIFA_BANCARIA	FieldNameTARIFA_BANCARIAOriginTARIFA_BANCARIA  TStringFieldqryImprimeFORMA_PAGAMENTO	FieldNameFORMA_PAGAMENTOOriginFORMA_PAGAMENTO	FixedChar	Size  TStringFieldqryImprimeNUMERO_PAGAMENTO	FieldNameNUMERO_PAGAMENTOOriginNUMERO_PAGAMENTOSize(  TStringFieldqryImprimeARQUIVO	FieldNameARQUIVOOriginARQUIVO  TStringFieldqryImprimeRETORNO	FieldNameRETORNOOriginRETORNO  
TDateFieldqryImprimeDT_DEVOLVIDO	FieldNameDT_DEVOLVIDOOriginDT_DEVOLVIDO  TStringFieldqryImprimeCPF_CNPJ_SACADOR	FieldNameCPF_CNPJ_SACADOROriginCPF_CNPJ_SACADORSize  TIntegerFieldqryImprimeID_MSG	FieldNameID_MSGOriginID_MSG  TFloatFieldqryImprimeVALOR_AR	FieldNameVALOR_AROriginVALOR_AR  TStringFieldqryImprimeELETRONICO	FieldName
ELETRONICOOrigin
ELETRONICO	FixedChar	Size  TIntegerFieldqryImprimeIRREGULARIDADE	FieldNameIRREGULARIDADEOriginIRREGULARIDADE  TStringFieldqryImprimeAVISTA	FieldNameAVISTAOriginAVISTA	FixedChar	Size  TFloatFieldqryImprimeSALDO_TITULO	FieldNameSALDO_TITULOOriginSALDO_TITULO  TStringFieldqryImprimeTIPO_SUSTACAO	FieldNameTIPO_SUSTACAOOriginTIPO_SUSTACAO	FixedChar	Size  TStringFieldqryImprimeRETORNO_PROTESTO	FieldNameRETORNO_PROTESTOOriginRETORNO_PROTESTO	FixedChar	Size  
TDateFieldqryImprimeDT_RETORNO_PROTESTO	FieldNameDT_RETORNO_PROTESTOOriginDT_RETORNO_PROTESTO  
TDateFieldqryImprimeDT_DEFINITIVA	FieldNameDT_DEFINITIVAOriginDT_DEFINITIVA  TStringFieldqryImprimeJUDICIAL	FieldNameJUDICIALOriginJUDICIAL	FixedChar	Size  TStringFieldqryImprimeALEATORIO_PROTESTO	FieldNameALEATORIO_PROTESTOOriginALEATORIO_PROTESTOSize  TStringFieldqryImprimeALEATORIO_SOLUCAO	FieldNameALEATORIO_SOLUCAOOriginALEATORIO_SOLUCAOSize  TStringFieldqryImprimeCCT	FieldNameCCTOriginCCTSize	  TStringFieldqryImprimeDETERMINACAO	FieldNameDETERMINACAOOriginDETERMINACAOSized  TStringFieldqryImprimeANTIGO	FieldNameANTIGOOriginANTIGO	FixedChar	Size  TStringFieldqryImprimeLETRA	FieldNameLETRAOriginLETRASize  TIntegerFieldqryImprimePROTOCOLO_CARTORIO	FieldNamePROTOCOLO_CARTORIOOriginPROTOCOLO_CARTORIO  TStringFieldqryImprimeARQUIVO_CARTORIO	FieldNameARQUIVO_CARTORIOOriginARQUIVO_CARTORIO  TStringFieldqryImprimeRETORNO_CARTORIO	FieldNameRETORNO_CARTORIOOriginRETORNO_CARTORIO  
TDateFieldqryImprimeDT_DEVOLVIDO_CARTORIO	FieldNameDT_DEVOLVIDO_CARTORIOOriginDT_DEVOLVIDO_CARTORIO  TIntegerFieldqryImprimeCARTORIO	FieldNameCARTORIOOriginCARTORIO  
TDateFieldqryImprimeDT_PROTOCOLO_CARTORIO	FieldNameDT_PROTOCOLO_CARTORIOOriginDT_PROTOCOLO_CARTORIO    