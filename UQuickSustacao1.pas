unit UQuickSustacao1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QuickRpt, QRCtrls, ExtCtrls, DB, RxMemDS;

type
  TFQuickSustacao1 = class(TForm)
    Relatorio: TQuickRep;
    Liminar: TQRBand;
    lbAverbacao1: TQRLabel;
    lbLinha1: TQRLabel;
    lbLinha2: TQRLabel;
    lbLinha3: TQRLabel;
    lbLinha4: TQRLabel;
    lblinha5: TQRLabel;
    lbNomeJuiz1: TQRLabel;
    lbCidade1: TQRLabel;
    Definitiva: TQRChildBand;
    lbAverbacao2: TQRLabel;
    lbLinha6: TQRLabel;
    lbLinha7: TQRLabel;
    lbLinha8: TQRLabel;
    lbLinha9: TQRLabel;
    lbLinha10: TQRLabel;
    lbNomeJuiz2: TQRLabel;
    lbCidade2: TQRLabel;
    Cancelamento: TQRChildBand;
    lbAverbacao3: TQRLabel;
    lbLinha11: TQRLabel;
    lbLinha12: TQRLabel;
    lbLinha13: TQRLabel;
    lbLinha14: TQRLabel;
    lbLinha15: TQRLabel;
    lbNomeJuiz3: TQRLabel;
    lbCidade3: TQRLabel;
    lbCGJ1: TQRLabel;
    lbCGJ2: TQRLabel;
    lbCGJ3: TQRLabel;
    lbSeloEletronico: TQRLabel;
    lbCGJ4: TQRLabel;
    lbCGJ5: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure RelatorioEndPage(Sender: TCustomQuickRep);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickSustacao1: TFQuickSustacao1;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFQuickSustacao1.FormCreate(Sender: TObject);
begin
  lbNomeJuiz1.Caption:=dm.ValorParametro(89)+'.';
  lbNomeJuiz2.Caption:=lbNomeJuiz1.Caption;
  lbNomeJuiz3.Caption:=lbNomeJuiz1.Caption;
end;

procedure TFQuickSustacao1.RelatorioEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

end.
