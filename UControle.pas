unit UControle;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBClient, SimpleDS, Grids, Wwdbigrd, Wwdbgrid, StdCtrls,
  Buttons, sBitBtn, ExtCtrls, sPanel, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFControle = class(TForm)
    dsControle: TDataSource;
    btOk: TsBitBtn;
    btCancelar: TsBitBtn;
    sPanel1: TsPanel;
    Grid: TwwDBGrid;
    qryControle: TFDQuery;
    qryControleID_CONTROLE: TIntegerField;
    qryControleSIGLA: TStringField;
    qryControleDESCRICAO: TStringField;
    qryControleVALOR: TIntegerField;
    qryControleLETRA: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FControle: TFControle;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFControle.FormCreate(Sender: TObject);
begin
  qryControle.Connection:=dm.conSISTEMA;
  qryControle.Close;
  qryControle.Open;
  qryControle.Edit;
end;

procedure TFControle.btOkClick(Sender: TObject);
begin
  qryControle.Post;
  Close;
end;

procedure TFControle.btCancelarClick(Sender: TObject);
begin
  qryControle.Cancel;
  Close;
end;

procedure TFControle.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_ESCAPE then
    btCancelarClick(Sender);
end;

procedure TFControle.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryControle.Close;
end;

end.
