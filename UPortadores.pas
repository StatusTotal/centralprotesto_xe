unit UPortadores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, DB, Grids, Wwdbigrd, Wwdbgrid, StdCtrls,
  DBCtrls, sDBMemo, sCheckBox, sDBCheckBox, sDBComboBox, Mask, sDBEdit,
  Buttons, sBitBtn, acPNG, sEdit, sBevel;

type
  TFPortadores = class(TForm)
    P2: TsPanel;
    GridPorta: TwwDBGrid;
    dsPortadores: TDataSource;
    edCodigo: TsDBEdit;
    edNome: TsDBEdit;
    cbTipo: TsDBComboBox;
    edDocumento: TsDBEdit;
    edEndereco: TsDBEdit;
    ckBanco: TsDBCheckBox;
    edConta: TsDBEdit;
    ckConvenio: TsDBCheckBox;
    MObs: TsDBMemo;
    P1: TsPanel;
    btIncluir: TsBitBtn;
    btAlterar: TsBitBtn;
    btExcluir: TsBitBtn;
    btSalvar: TsBitBtn;
    btCancelar: TsBitBtn;
    ImAviso1: TImage;
    ImOk1: TImage;
    edAgencia: TsDBEdit;
    ckCRA: TsDBCheckBox;
    edSequencia: TsDBEdit;
    sBevel1: TsBevel;
    edDOC: TsDBEdit;
    edTED: TsDBEdit;
    ckNominal: TsDBCheckBox;
    sPanel1: TsPanel;
    edCodigoPortador: TsEdit;
    edNomePortador: TsEdit;
    btDuplicados: TsBitBtn;
    ckForca: TsDBCheckBox;
    ckEspecie: TsDBCheckBox;
    chbPagaAntecipado: TsDBCheckBox;
    procedure cbTipoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dsPortadoresStateChange(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dsPortadoresDataChange(Sender: TObject; Field: TField);
    procedure GridPortaTitleButtonClick(Sender: TObject;
      AFieldName: String);
    procedure edDocumentoChange(Sender: TObject);
    procedure MObsExit(Sender: TObject);
    procedure edCodigoPortadorChange(Sender: TObject);
    procedure edNomePortadorChange(Sender: TObject);
    procedure edNomePortadorEnter(Sender: TObject);
    procedure edCodigoPortadorEnter(Sender: TObject);
    procedure btDuplicadosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ckConvenioClick(Sender: TObject);
    procedure ckConvenioExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPortadores: TFPortadores;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFPortadores.cbTipoChange(Sender: TObject);
begin
  if cbTipo.ItemIndex=0 then
  begin
      edDocumento.BoundLabel.Caption   :='CPF';
      dm.PortadoresDOCUMENTO.EditMask  :='999.999.999-99;0;_';
      dm.PortadoresTIPO.AsString       :='F';
  end
  else
  begin
      edDocumento.BoundLabel.Caption   :='CNPJ';
      dm.PortadoresDOCUMENTO.EditMask  :='99.999.999/9999-99;0;_';
      dm.PortadoresTIPO.AsString       :='J';
  end;
end;

procedure TFPortadores.ckConvenioClick(Sender: TObject);
begin
  chbPagaAntecipado.Enabled := ckConvenio.Checked;

  if not ckConvenio.Checked then
    chbPagaAntecipado.Checked := False;
end;

procedure TFPortadores.ckConvenioExit(Sender: TObject);
begin
  chbPagaAntecipado.Enabled := ckConvenio.Checked;

  if not ckConvenio.Checked then
    chbPagaAntecipado.Checked := False;
end;

procedure TFPortadores.FormCreate(Sender: TObject);
begin
  dm.Portadores.Close;
  dm.Portadores.CommandText:='SELECT * FROM PORTADORES ORDER BY NOME';
  dm.Portadores.Open;
end;

procedure TFPortadores.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.Portadores.Close;
end;

procedure TFPortadores.dsPortadoresStateChange(Sender: TObject);
begin
  if dm.Portadores.State=dsInsert then
  begin
      btIncluir.Enabled :=False;
      btAlterar.Enabled :=False;
      btExcluir.Enabled :=False;
      btSalvar.Enabled  :=True;
      btCancelar.Enabled:=True;
      GridPorta.Enabled :=False;
      P2.Enabled        :=True;
      edCodigo.SetFocus;
  end;

  if dm.Portadores.State=dsEdit then
  begin
      btIncluir.Enabled :=False;
      btAlterar.Enabled :=False;
      btExcluir.Enabled :=False;
      GridPorta.Enabled :=False;
      btSalvar.Enabled  :=True;
      btCancelar.Enabled:=True;
      P2.Enabled        :=True;
      edCodigo.SetFocus;
  end;

  if dm.Portadores.State=dsBrowse then
  begin
      btIncluir.Enabled :=True;
      btAlterar.Enabled :=True;
      btExcluir.Enabled :=True;
      GridPorta.Enabled :=True;
      btSalvar.Enabled  :=False;
      btCancelar.Enabled:=False;
      P2.Enabled:=False;
  end;
end;

procedure TFPortadores.btIncluirClick(Sender: TObject);
begin
  dm.Portadores.Append;
  dm.PortadoresBANCO.AsString              := 'N';
  dm.PortadoresCONVENIO.AsString           := 'N';
  dm.PortadoresCRA.AsString                := 'N';
  dm.PortadoresNOMINAL.AsString            := 'N';
  dm.PortadoresFLG_PAGAANTECIPADO.AsString := 'N';
end;

procedure TFPortadores.btCancelarClick(Sender: TObject);
begin
  dm.Portadores.Cancel;
end;

procedure TFPortadores.btAlterarClick(Sender: TObject);
begin
  if dm.Portadores.IsEmpty then Exit;

  dm.Portadores.Edit;
end;

procedure TFPortadores.btExcluirClick(Sender: TObject);
begin
  if dm.Portadores.IsEmpty then Exit;

  if GR.Pergunta('Confirma a exclus�o') then
  begin
      dm.Portadores.Delete;
      dm.Portadores.ApplyUpdates(0);
  end;
end;

procedure TFPortadores.btSalvarClick(Sender: TObject);
begin
  if cbTipo.Text='' then
  begin
      GR.Aviso('INFORME O TIPO DO PORTADOR!');
      cbTipo.SetFocus;
      Exit;
  end;

  if edNome.Text='' then
  begin
      GR.Aviso('INFORME O NOME DO PORTADOR!');
      edNome.SetFocus;
      Exit;
  end;

  {if GR.PegarNumeroTexto(edDocumento.Text)='' then
  begin
      GR.Aviso('INFORME O CPF/CNPJ DO PORTADOR!');
      edDocumento.SetFocus;
      Exit;
  end;}

  dm.Portadores.Post;
  dm.Portadores.ApplyUpdates(0);
end;

procedure TFPortadores.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFPortadores.FormShow(Sender: TObject);
begin
  chbPagaAntecipado.Enabled := ckConvenio.Checked;
end;

procedure TFPortadores.dsPortadoresDataChange(Sender: TObject;
  Field: TField);
begin
  btAlterar.Enabled:=not dm.Portadores.IsEmpty;
  btExcluir.Enabled:=not dm.Portadores.IsEmpty;
end;

procedure TFPortadores.GridPortaTitleButtonClick(Sender: TObject;
  AFieldName: String);
begin
  dm.Portadores.IndexFieldNames:=AFieldName;
end;

procedure TFPortadores.edDocumentoChange(Sender: TObject);
begin
  if GR.PegarNumeroTexto(edDocumento.Text)='' then
  begin
      ImAviso1.Visible:=True;
      ImOk1.Visible   :=False;
      Exit;
  end;

  if cbTipo.Text='F' then
    ImAviso1.Visible:=not PF.VerificarCPF(GR.PegarNumeroTexto(edDocumento.Text))
      else ImAviso1.Visible:=not PF.VerificarCNPJ(GR.PegarNumeroTexto(edDocumento.Text));

  ImOk1.Visible:=not ImAviso1.Visible;

  if ActiveControl<>edDocumento then
    if dm.PortadoresTIPO.AsString='F' then
    begin
        edDocumento.BoundLabel.Caption   :='CPF';
        dm.PortadoresDOCUMENTO.EditMask  :='999.999.999-99;0;_';
    end
    else
    begin
        edDocumento.BoundLabel.Caption   :='CNPJ';
        dm.PortadoresDOCUMENTO.EditMask  :='99.999.999/9999-99;0;_';
    end;
end;

procedure TFPortadores.MObsExit(Sender: TObject);
begin
  btSalvar.SetFocus;
end;

procedure TFPortadores.edCodigoPortadorChange(Sender: TObject);
begin
  if edCodigoPortador.Text<>'' then
  dm.Portadores.Locate('CODIGO',edCodigoPortador.Text,[]);
end;

procedure TFPortadores.edNomePortadorChange(Sender: TObject);
begin
  if edNomePortador.Text<>'' then
  dm.Portadores.Locate('NOME',edNomePortador.Text,[loPartialKey,loCaseInsensitive]);
end;

procedure TFPortadores.edNomePortadorEnter(Sender: TObject);
begin
  edCodigoPortador.Clear;
end;

procedure TFPortadores.edCodigoPortadorEnter(Sender: TObject);
begin
  edNomePortador.Clear;
end;

procedure TFPortadores.btDuplicadosClick(Sender: TObject);
begin
  dm.PortadoresDuplicados(True);
end;

end.
