object FValores: TFValores
  Left = 409
  Top = 263
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Valores'
  ClientHeight = 281
  ClientWidth = 216
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 15
  object sLabel11: TsLabel
    Left = 0
    Top = 0
    Width = 216
    Height = 20
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    SkinSection = 'PANEL'
    Caption = 'Arquivo processado com sucesso!'
    ParentFont = False
    Layout = tlCenter
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
  end
  object P1: TsPanel
    Left = 0
    Top = 20
    Width = 216
    Height = 261
    Align = alClient
    TabOrder = 0
    SkinData.SkinSection = 'HINT'
    object sLabel1: TsLabel
      Left = 7
      Top = 11
      Width = 114
      Height = 15
      Alignment = taRightJustify
      Caption = 'Emolumentos: R$.....'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object sLabel2: TsLabel
      Left = 31
      Top = 28
      Width = 90
      Height = 15
      Alignment = taRightJustify
      Caption = 'Fetj (20%): R$.....'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object sLabel3: TsLabel
      Left = 9
      Top = 45
      Width = 112
      Height = 15
      Alignment = taRightJustify
      Caption = 'Fundperj (5%): R$.....'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object sLabel4: TsLabel
      Left = 16
      Top = 62
      Width = 105
      Height = 15
      Alignment = taRightJustify
      Caption = 'Funperj (5%): R$.....'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object sLabel5: TsLabel
      Left = 5
      Top = 79
      Width = 116
      Height = 15
      Alignment = taRightJustify
      Caption = 'Funarpen (4%): R$.....'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 15
      Top = 96
      Width = 106
      Height = 15
      Alignment = taRightJustify
      Caption = 'Pmcmv (2%): R$.....'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object sLabel7: TsLabel
      Left = 50
      Top = 130
      Width = 71
      Height = 15
      Alignment = taRightJustify
      Caption = 'M'#250'tua: R$.....'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object sLabel8: TsLabel
      Left = 44
      Top = 147
      Width = 77
      Height = 15
      Alignment = taRightJustify
      Caption = 'Acoterj: R$.....'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object sLabel9: TsLabel
      Left = 17
      Top = 164
      Width = 104
      Height = 15
      Alignment = taRightJustify
      Caption = 'Distribui'#231#227'o: R$.....'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object sLabel10: TsLabel
      Left = 58
      Top = 181
      Width = 63
      Height = 15
      Alignment = taRightJustify
      Caption = 'Total: R$.....'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object lbEM: TsLabel
      Left = 185
      Top = 11
      Width = 24
      Height = 15
      Alignment = taRightJustify
      Caption = '0,00'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object lbFE: TsLabel
      Left = 185
      Top = 28
      Width = 24
      Height = 15
      Alignment = taRightJustify
      Caption = '0,00'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object lbFD: TsLabel
      Left = 185
      Top = 45
      Width = 24
      Height = 15
      Alignment = taRightJustify
      Caption = '0,00'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object lbFP: TsLabel
      Left = 185
      Top = 62
      Width = 24
      Height = 15
      Alignment = taRightJustify
      Caption = '0,00'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object lbPM: TsLabel
      Left = 185
      Top = 96
      Width = 24
      Height = 15
      Alignment = taRightJustify
      Caption = '0,00'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object lbFN: TsLabel
      Left = 185
      Top = 79
      Width = 24
      Height = 15
      Alignment = taRightJustify
      Caption = '0,00'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object lbMU: TsLabel
      Left = 185
      Top = 130
      Width = 24
      Height = 15
      Alignment = taRightJustify
      Caption = '0,00'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object lbAC: TsLabel
      Left = 185
      Top = 147
      Width = 24
      Height = 15
      Alignment = taRightJustify
      Caption = '0,00'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object lbDB: TsLabel
      Left = 185
      Top = 164
      Width = 24
      Height = 15
      Alignment = taRightJustify
      Caption = '0,00'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object lbTT: TsLabel
      Left = 185
      Top = 181
      Width = 24
      Height = 15
      Alignment = taRightJustify
      Caption = '0,00'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object lbIss: TsLabel
      Left = 41
      Top = 113
      Width = 80
      Height = 15
      Alignment = taRightJustify
      Caption = 'Iss (2%): R$.....'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object lbSS: TsLabel
      Left = 185
      Top = 113
      Width = 24
      Height = 15
      Alignment = taRightJustify
      Caption = '0,00'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
    end
    object btOk: TsBitBtn
      Left = 76
      Top = 220
      Width = 64
      Height = 27
      Cursor = crHandPoint
      Caption = 'Ok'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = btOkClick
      ImageIndex = 19
      Images = dm.Im24
      SkinData.SkinSection = 'BUTTON'
    end
  end
end
