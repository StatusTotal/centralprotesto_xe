unit USelosUtilizados;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, sMaskEdit, sCustomComboEdit, sTooledit,
  ExtCtrls, sPanel, Buttons, sBitBtn, sRadioButton, QuickRpt, QRCtrls, DB,
  DBClient, SimpleDS, RxMemDS, FMTBcd, SqlExpr, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFSelosUtilizados = class(TForm)
    P1: TsPanel;
    edData1: TsDateEdit;
    edData2: TsDateEdit;
    btVisualizar: TsBitBtn;
    btFechar: TsBitBtn;
    qkRelatorio: TQuickRep;
    QRBand6: TQRBand;
    lbServentia: TQRLabel;
    lbTitulo: TQRLabel;
    QRSysData6: TQRSysData;
    QRSysData7: TQRSysData;
    lbMovimento: TQRLabel;
    QRChildBand2: TQRChildBand;
    QRLabel19: TQRLabel;
    QRBand7: TQRBand;
    QRBand8: TQRBand;
    lbCidade: TQRLabel;
    QRSysData8: TQRSysData;
    qrSelo: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel23: TQRLabel;
    QRDBText6: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    RX: TRxMemoryData;
    RXPROTOCOLO: TIntegerField;
    RXRECIBO: TIntegerField;
    RXSELO: TStringField;
    RXDATA: TDateField;
    RXCCT: TStringField;
    RXSTATUS: TStringField;
    QRLabel1: TQRLabel;
    QRDBText1: TQRDBText;
    CertidaoCanceladosFolhas: TFDQuery;
    Protestados: TFDQuery;
    Pagos: TFDQuery;
    Retirados: TFDQuery;
    Apontamentos: TFDQuery;
    CertidaoCanceladosFolhasID_CERTIDAO: TIntegerField;
    CertidaoCanceladosFolhasID_ATO: TIntegerField;
    CertidaoCanceladosFolhasDT_ENTREGA: TDateField;
    CertidaoCanceladosFolhasDT_CERTIDAO: TDateField;
    CertidaoCanceladosFolhasTIPO_CERTIDAO: TStringField;
    CertidaoCanceladosFolhasCODIGO: TIntegerField;
    CertidaoCanceladosFolhasRECIBO: TIntegerField;
    CertidaoCanceladosFolhasSELO: TStringField;
    CertidaoCanceladosFolhasALEATORIO: TStringField;
    CertidaoCanceladosFolhasCCT: TStringField;
    CertidaoCanceladosFolhasEXRECIBO: TIntegerField;
    ProtestadosPROTOCOLO: TIntegerField;
    ProtestadosRECIBO: TIntegerField;
    ProtestadosSELO_REGISTRO: TStringField;
    ProtestadosALEATORIO_PROTESTO: TStringField;
    ProtestadosDT_REGISTRO: TDateField;
    PagosPROTOCOLO: TIntegerField;
    PagosRECIBO: TIntegerField;
    PagosSELO_PAGAMENTO: TStringField;
    PagosALEATORIO_SOLUCAO: TStringField;
    PagosDT_PAGAMENTO: TDateField;
    RetiradosPROTOCOLO: TIntegerField;
    RetiradosRECIBO: TIntegerField;
    RetiradosSELO_PAGAMENTO: TStringField;
    RetiradosALEATORIO_SOLUCAO: TStringField;
    RetiradosDT_RETIRADO: TDateField;
    ApontamentosPROTOCOLO: TIntegerField;
    ApontamentosCCT: TStringField;
    ApontamentosDT_PROTOCOLO: TDateField;
    procedure FormCreate(Sender: TObject);
    procedure btVisualizarClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qkRelatorioEndPage(Sender: TCustomQuickRep);
    procedure RXBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSelosUtilizados: TFSelosUtilizados;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFSelosUtilizados.FormCreate(Sender: TObject);
begin
  edData1.Date:=Now;
  edData2.Date:=Now;
end;

procedure TFSelosUtilizados.btVisualizarClick(Sender: TObject);

  function Protocolo(IdAto: String): Integer;
  var
    Q: TFDQuery;
  begin
    Q:=TFDQuery.Create(Nil);
    Q.Connection:=dm.conSISTEMA;
    Q.Close;
    Q.SQL.Add('SELECT PROTOCOLO FROM TITULOS WHERE ID_ATO='+IdAto);
    Q.Open;
    Result:=Q.FieldByName('PROTOCOLO').AsInteger;
    Q.Close;
    Q.Free;
  end;

begin
  lbServentia.Caption :=dm.ServentiaDESCRICAO.AsString;
  lbMovimento.Caption :=edData1.Text+' - '+edData2.Text;
  lbCidade.Caption    :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd/mm/yyyy',Now);
  RX.Close;
  RX.Open;

  {APONTAMENTOS}
  Apontamentos.Close;
  Apontamentos.Params[0].AsDate:=edData1.Date;
  Apontamentos.Params[1].AsDate:=edData2.Date;
  Apontamentos.Open;
  while not Apontamentos.Eof do
  begin
      RX.Append;
      RXPROTOCOLO.AsInteger :=ApontamentosPROTOCOLO.AsInteger;
      RXSELO.AsString       :=ApontamentosCCT.AsString;
      RXDATA.AsDateTime     :=ApontamentosDT_PROTOCOLO.AsDateTime;
      RXSTATUS.AsString     :='APONTAMENTO';
      RX.Post;
      Apontamentos.Next;
  end;
  Apontamentos.Close;

  if not RX.IsEmpty then
  begin
      RX.SortOnFields('SELO');
      lbTitulo.Caption:='RELA��O DE CCT�s UTILIZADOS';
      qrSelo.Caption:='CCT';
      qkRelatorio.Preview;
  end;

  RX.Close;
  RX.Open;

  {CERTID�ES/CANCELADOS/FOLHAS EXCEDENTES}
  CertidaoCanceladosFolhas.Close;
  CertidaoCanceladosFolhas.Params[0].AsDate:=edData1.Date;
  CertidaoCanceladosFolhas.Params[1].AsDate:=edData2.Date;
  CertidaoCanceladosFolhas.Open;
  while not CertidaoCanceladosFolhas.Eof do
  begin
      RX.Append;
      if CertidaoCanceladosFolhasTIPO_CERTIDAO.AsString='X' then
      RXPROTOCOLO.AsInteger :=Protocolo(CertidaoCanceladosFolhasID_ATO.AsString);
      RXRECIBO.AsInteger    :=CertidaoCanceladosFolhasRECIBO.AsInteger;
      RXSELO.AsString       :=GR.SeloFormatado(CertidaoCanceladosFolhasSELO.AsString,CertidaoCanceladosFolhasALEATORIO.AsString);
      RXDATA.AsDateTime     :=CertidaoCanceladosFolhasDT_CERTIDAO.AsDateTime;
      RXSTATUS.AsString     :=GR.iif(CertidaoCanceladosFolhasTIPO_CERTIDAO.AsString='X','CANCELAMENTO','CERTID�O NORMAL');
      RX.Post;

      if CertidaoCanceladosFolhasCCT.AsString<>'' then
      begin
          RX.Append;
          RXRECIBO.AsInteger    :=CertidaoCanceladosFolhasEXRECIBO.AsInteger;
          RXSELO.AsString       :=GR.SeloFormatado(CertidaoCanceladosFolhasSELO.AsString,CertidaoCanceladosFolhasALEATORIO.AsString);
          RXCCT.AsString        :=CertidaoCanceladosFolhasCCT.AsString;
          RXDATA.AsDateTime     :=CertidaoCanceladosFolhasDT_ENTREGA.AsDateTime;
          RXSTATUS.AsString     :='FOLHA EXCEDENTE';
          RX.Post;
      end;

      CertidaoCanceladosFolhas.Next;
  end;
  CertidaoCanceladosFolhas.Close;

  {PROTESTADOS}
  Protestados.Close;
  Protestados.Params[0].AsDate:=edData1.Date;
  Protestados.Params[1].AsDate:=edData2.Date;
  Protestados.Open;
  while not Protestados.Eof do
  begin
      RX.Append;
      RXPROTOCOLO.AsInteger :=ProtestadosPROTOCOLO.AsInteger;
      RXRECIBO.AsInteger    :=ProtestadosRECIBO.AsInteger;
      RXSELO.AsString       :=GR.SeloFormatado(ProtestadosSELO_REGISTRO.AsString,ProtestadosALEATORIO_PROTESTO.AsString);
      RXDATA.AsDateTime     :=ProtestadosDT_REGISTRO.AsDateTime;
      RXSTATUS.AsString     :='PROTESTO';
      RX.Post;
      Protestados.Next;
  end;
  Protestados.Close;

  {PAGOS}
  Pagos.Close;
  Pagos.Params[0].AsDate:=edData1.Date;
  Pagos.Params[1].AsDate:=edData2.Date;
  Pagos.Open;
  while not Pagos.Eof do
  begin
      RX.Append;
      RXPROTOCOLO.AsInteger :=PagosPROTOCOLO.AsInteger;
      RXRECIBO.AsInteger    :=PagosRECIBO.AsInteger;
      RXSELO.AsString       :=GR.SeloFormatado(PagosSELO_PAGAMENTO.AsString,PagosALEATORIO_SOLUCAO.AsString);
      RXDATA.AsDateTime     :=PagosDT_PAGAMENTO.AsDateTime;
      RXSTATUS.AsString     :='PAGAMENTO';
      RX.Post;
      Pagos.Next;
  end;
  Pagos.Close;

  {RETIRADOS}
  Retirados.Close;
  Retirados.Params[0].AsDate:=edData1.Date;
  Retirados.Params[1].AsDate:=edData2.Date;
  Retirados.Open;
  while not Retirados.Eof do
  begin
      RX.Append;
      RXPROTOCOLO.AsInteger :=RetiradosPROTOCOLO.AsInteger;
      RXRECIBO.AsInteger    :=RetiradosRECIBO.AsInteger;
      RXSELO.AsString       :=GR.SeloFormatado(RetiradosSELO_PAGAMENTO.AsString,RetiradosALEATORIO_SOLUCAO.AsString);
      RXDATA.AsDateTime     :=RetiradosDT_RETIRADO.AsDateTime;
      RXSTATUS.AsString     :='RETIRADO';
      RX.Post;
      Retirados.Next;
  end;
  Retirados.Close;

  if not RX.IsEmpty then
  begin
      RX.SortOnFields('SELO');
      lbTitulo.Caption:='RELA��O DE SELOS UTILIZADOS';
      qrSelo.Caption:='Selo';
      qkRelatorio.Preview;
  end;
end;

procedure TFSelosUtilizados.btFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TFSelosUtilizados.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFSelosUtilizados.qkRelatorioEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFSelosUtilizados.RXBeforePost(DataSet: TDataSet);
begin
  if RXPROTOCOLO.AsInteger=0 then RXPROTOCOLO.Clear;
  if RXRECIBO.AsInteger=0    then RXRECIBO.Clear;
end;

procedure TFSelosUtilizados.FormShow(Sender: TObject);
begin
  edData1.SetFocus;
end;

end.
