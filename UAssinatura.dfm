object FAssinatura: TFAssinatura
  Left = 460
  Top = 295
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Assinatura'
  ClientHeight = 247
  ClientWidth = 429
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 18
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 429
    Height = 247
    Align = alClient
    TabOrder = 0
    SkinData.SkinSection = 'SELECTION'
    object sBevel1: TsBevel
      Left = 16
      Top = 24
      Width = 399
      Height = 50
      Shape = bsFrame
    end
    object btContinuar: TsBitBtn
      Left = 304
      Top = 200
      Width = 111
      Height = 34
      Cursor = crHandPoint
      Caption = 'Continuar...'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      Layout = blGlyphRight
      ParentFont = False
      TabOrder = 4
      OnClick = btContinuarClick
      ImageIndex = 24
      Images = dm.Im24
      SkinData.SkinSection = 'BUTTON'
    end
    object lkEscreventes: TsDBLookupComboBox
      Left = 16
      Top = 160
      Width = 399
      Height = 24
      Color = clWhite
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      KeyField = 'ID_ESCREVENTE'
      ListField = 'NOME'
      ListSource = dsEscreventes
      ParentFont = False
      TabOrder = 3
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Escreventes'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -16
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
    end
    object ckTabeliao: TsCheckBox
      Left = 30
      Top = 39
      Width = 146
      Height = 23
      Cursor = crHandPoint
      Caption = 'Tabeli'#227'o Assina'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      OnClick = ckTabeliaoClick
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object ckMatricula: TsCheckBox
      Left = 235
      Top = 39
      Width = 160
      Height = 23
      Cursor = crHandPoint
      Caption = 'Imprimir Matr'#237'cula'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = ckTabeliaoClick
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object edTitulo: TsEdit
      Left = 16
      Top = 107
      Width = 399
      Height = 26
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'T'#237'tulo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -16
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
  end
  object dsEscreventes: TDataSource
    DataSet = qryEscreventes
    Left = 120
    Top = 200
  end
  object qryEscreventes: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM ESCREVENTES')
    Left = 40
    Top = 200
    object qryEscreventesID_ESCREVENTE: TIntegerField
      FieldName = 'ID_ESCREVENTE'
      Origin = 'ID_ESCREVENTE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryEscreventesNOME: TStringField
      FieldName = 'NOME'
      Origin = 'NOME'
      Size = 60
    end
    object qryEscreventesCPF: TStringField
      FieldName = 'CPF'
      Origin = 'CPF'
      Size = 11
    end
    object qryEscreventesQUALIFICACAO: TStringField
      FieldName = 'QUALIFICACAO'
      Origin = 'QUALIFICACAO'
      Size = 60
    end
    object qryEscreventesLOGIN: TStringField
      FieldName = 'LOGIN'
      Origin = 'LOGIN'
      Size = 10
    end
    object qryEscreventesSENHA: TStringField
      FieldName = 'SENHA'
      Origin = 'SENHA'
      Size = 6
    end
    object qryEscreventesAUTORIZADO: TStringField
      FieldName = 'AUTORIZADO'
      Origin = 'AUTORIZADO'
      FixedChar = True
      Size = 1
    end
    object qryEscreventesMATRICULA: TStringField
      FieldName = 'MATRICULA'
      Origin = 'MATRICULA'
      Size = 15
    end
  end
end
