unit UCadastro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBCtrls, sDBLookupComboBox, StdCtrls, Buttons, sBitBtn,
  ExtCtrls, sPanel, sGroupBox, sDBRadioGroup, Mask, sDBEdit, sMaskEdit,
  sCustomComboEdit, sTooledit, sDBDateEdit, acPNG, sDBMemo, sDBComboBox,
  RxDBComb, sCheckBox, sDBCheckBox, wwclearbuttongroup, wwradiogroup,
  sButton, sComboBox;

type
  TFCadastro = class(TForm)
    P1: TsPanel;
    dsRXDevedor: TDataSource;
    edNome: TsDBEdit;
    P2: TsPanel;
    edEndereco: TsDBEdit;
    edBairro: TsDBEdit;
    cbUF: TsDBComboBox;
    lkCidade: TsDBLookupComboBox;
    edIdentidade: TsDBEdit;
    lkOrgao: TsDBLookupComboBox;
    edEmissao: TsDBDateEdit;
    dsMunicipios: TDataSource;
    edCep: TsDBEdit;
    edDocumento: TsDBEdit;
    cbTipoDev: TsDBComboBox;
    cbOrgao: TsComboBox;
    ckIgnorado: TsDBCheckBox;
    btSalvar: TsBitBtn;
    btCancelar: TsBitBtn;
    dsOrgaos: TDataSource;
    edTelefone: TsDBEdit;
    edOrdem: TsDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure ckIgnoradoClick(Sender: TObject);
    procedure cbUFChange(Sender: TObject);
    procedure cbTipoDevChange(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    function CamposEmBranco: Boolean;
    procedure cbOrgaoClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastro: TFCadastro;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFCadastro.FormCreate(Sender: TObject);
begin
  dm.Orgaos.Close;
  dm.Orgaos.Open;

  if dm.RxDevedorIFP_DETRAN.AsString='I' then cbOrgao.ItemIndex:=0;
  if dm.RxDevedorIFP_DETRAN.AsString='D' then cbOrgao.ItemIndex:=1;
  if dm.RxDevedorIFP_DETRAN.AsString='O' then cbOrgao.ItemIndex:=2;

  cbTipoDev.ItemIndex:=cbTipoDev.Items.IndexOf(dm.RxDevedorTIPO.AsString);

  cbTipoDevChange(Sender);
  cbOrgaoClick(Sender);
  cbUFChange(Sender);
end;

procedure TFCadastro.ckIgnoradoClick(Sender: TObject);
begin
  if ckIgnorado.Checked then
  begin
      P2.Enabled        :=False;
      edEndereco.Enabled:=False;
      edBairro.Enabled  :=False;
      edCep.Enabled     :=False;
      cbUF.Enabled      :=False;
      lkCidade.Enabled  :=False;

      dm.RxDevedorUF.Clear;
      dm.RxDevedorBAIRRO.Clear;
      dm.RxDevedorMUNICIPIO.Clear;
      dm.RxDevedorENDERECO.Clear;
      dm.RxDevedorCEP.Clear;
  end
  else
  begin
      P2.Enabled        :=True;
      edEndereco.Enabled:=True;
      edBairro.Enabled  :=True;
      edCep.Enabled     :=True;
      cbUF.Enabled      :=True;
      lkCidade.Enabled  :=True;
  end;
end;

procedure TFCadastro.cbUFChange(Sender: TObject);
begin
  dm.Municipios.Close;
  dm.Municipios.Params[0].AsString:=cbUF.Text;
  dm.Municipios.Open;
end;

procedure TFCadastro.cbTipoDevChange(Sender: TObject);
begin
  if cbTipoDev.ItemIndex=0 then
  begin
      edDocumento.BoundLabel.Caption  :='CPF';
      dm.RxDevedorDOCUMENTO.EditMask  :='999.999.999-99;0;_';
      dm.RxDevedorTIPO.AsString       :='F';
  end
  else
  begin
      edDocumento.BoundLabel.Caption  :='CNPJ';
      dm.RxDevedorDOCUMENTO.EditMask  :='99.999.999/9999-99;0;_';
      dm.RxDevedorTIPO.AsString       :='J';
  end;
end;

procedure TFCadastro.btSalvarClick(Sender: TObject);
begin
  if CamposEmBranco then Exit;

  if cbOrgao.ItemIndex=0 then dm.RxDevedorIFP_DETRAN.AsString:='I';
  if cbOrgao.ItemIndex=1 then dm.RxDevedorIFP_DETRAN.AsString:='D';
  if cbOrgao.ItemIndex=2 then dm.RxDevedorIFP_DETRAN.AsString:='O';

  dm.RxDevedor.Post;
  Close;
end;

procedure TFCadastro.btCancelarClick(Sender: TObject);
begin
  dm.RxDevedor.Cancel;
  Close;
end;

function TFCadastro.CamposEmBranco: Boolean;
begin
  Result:=False;

  if GR.PegarNumeroTexto(edDocumento.Text)='' then
  begin
      GR.Aviso('INFORME O DOCUMENTO DO DEVEDOR!');
      edDocumento.SetFocus;
      Result:=True;
      Exit;
  end;

  if edNome.Text='' then
  begin
      GR.Aviso('INFORME O NOME DO DEVEDOR!');
      edNome.SetFocus;
      Result:=True;
      Exit;
  end;
end;

procedure TFCadastro.cbOrgaoClick(Sender: TObject);
begin
  if not (dm.RxDevedor.State in [dsEdit,dsInsert]) then Exit;

  {IFP}
  if cbOrgao.ItemIndex=0 then
  begin
      dm.RxDevedorORGAO.Clear;
      edEmissao.Enabled               :=True;
      lkOrgao.Enabled                 :=False;
      dm.RxDevedorIFP_DETRAN.AsString :='I';
  end;

  {DETRAN}
  if cbOrgao.ItemIndex=1 then
  begin
      dm.RxDevedorORGAO.Clear;
      edEmissao.Enabled               :=True;
      lkOrgao.Enabled                 :=False;
      dm.RxDevedorIFP_DETRAN.AsString :='D';
  end;

  {OUTROS}
  if cbOrgao.ItemIndex=2 then
  begin
      dm.RxDevedorDT_EMISSAO.Clear;
      edEmissao.Enabled               :=False;
      lkOrgao.Enabled                 :=True;
      dm.RxDevedorIFP_DETRAN.AsString :='O';
  end;
end;

procedure TFCadastro.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then
    if btCancelar.Visible=True then
      btCancelar.Click
        else Close;
end;

procedure TFCadastro.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if dm.RxDevedor.State in [dsInsert,dsEdit] then dm.RxDevedor.Cancel;
end;

procedure TFCadastro.FormShow(Sender: TObject);
begin
  cbTipoDev.SetFocus;
end;

end.
