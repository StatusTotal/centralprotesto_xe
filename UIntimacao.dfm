�
 TFINTIMACAO 0'  TPF0TFIntimacao
FIntimacaoLeftuTop� BorderIconsbiSystemMenu BorderStylebsDialogCaption   INTIMAÇÃOClientHeight� ClientWidthColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
KeyPreview	OldCreateOrderPositionpoMainFormCenterOnCreate
FormCreatePixelsPerInch`
TextHeight TsPanelP1Left Top WidthHeight� AlignalTopTabOrder SkinData.SkinSectionPANEL 
TsDateEditedDataLeftTop WidtheHeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontParentShowHintShowHint	TabOrder OnExit
edDataExitBoundLabel.Active	BoundLabel.Caption   Data da IntimaçãoBoundLabel.Indent BoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style BoundLabel.Layout
sclTopLeftBoundLabel.MaxWidth BoundLabel.UseSkinColor	SkinData.SkinSectionEDITGlyphMode.Blend GlyphMode.GrayedDefaultToday	  
TsComboBoxcbTipoIntimacaoLeftTop WidtheHeightHint   TIPO DE INTIMAÇÃO	AlignmenttaLeftJustifyBoundLabel.Active	BoundLabel.Caption   Tipo da IntimaçãoBoundLabel.Indent BoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style BoundLabel.Layout
sclTopLeftBoundLabel.MaxWidth BoundLabel.UseSkinColor	SkinData.SkinSectionCOMBOBOXVerticalAlignment
taAlignTopStylecsDropDownListColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 
ItemHeight	ItemIndex 
ParentFontParentShowHintShowHint	TabOrderTextPessoalOnChangecbTipoIntimacaoChangeItems.StringsPessoalEditalCarta   
TsDateEditedDataPublicacaoLeft�Top WidtheHeightHint   DATA DA PUBLICAÇÃO DO EDITALAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontParentShowHintShowHint	TabOrderText
  /  /    BoundLabel.Active	BoundLabel.Caption   Data da PublicaçãoBoundLabel.Indent BoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style BoundLabel.Layout
sclTopLeftBoundLabel.MaxWidth BoundLabel.UseSkinColor	SkinData.SkinSectionEDITGlyphMode.Blend GlyphMode.Grayed  TsDBLookupComboBoxlkMotivoLeftTopXWidth�HeightHint    MOTIVO DA INTIMAÇÃO POR EDITALColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style KeyField	ID_MOTIVO	ListField	DESCRICAO
ListSource	dsMotivos
ParentFontParentShowHintShowHint	TabOrderBoundLabel.Active	BoundLabel.CaptionMotivo do EditalBoundLabel.Indent BoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style BoundLabel.Layout
sclTopLeftBoundLabel.MaxWidth BoundLabel.UseSkinColor	SkinData.SkinSectionCOMBOBOX  
TsDateEditedPrazoLeft� Top WidtheHeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontParentShowHintShowHint	TabOrderOnExit
edDataExitBoundLabel.Active	BoundLabel.CaptionData do PrazoBoundLabel.Indent BoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style BoundLabel.Layout
sclTopLeftBoundLabel.MaxWidth BoundLabel.UseSkinColor	SkinData.SkinSectionEDITGlyphMode.Blend GlyphMode.GrayedDefaultToday	   
TsCheckBox	ckImprimeLeftTop� Width� HeightCursorcrHandPointCaption   Imprimir IntimaçãoChecked	Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontState	cbCheckedTabOrderSkinData.SkinSectionCHECKBOX
ImgChecked ImgUnchecked   TsBitBtnbtOkLeft^Top� WidthKHeightCursorcrHandPointCaptionOkTabOrderOnClick	btOkClickSkinData.SkinSectionBUTTON
ImageIndexImages
dm.Imagens  TsBitBtn
btCancelarLeft�Top� WidthYHeightCursorcrHandPointCaptionCancelarTabOrderOnClickbtCancelarClickSkinData.SkinSectionBUTTON
ImageIndexImages
dm.Imagens  
TsCheckBoxckBaixarLeft� Top� WidthQHeightCursorcrHandPointCaptionIntimadoChecked	Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontState	cbCheckedTabOrderSkinData.SkinSectionCHECKBOX
ImgChecked ImgUnchecked   TDataSource	dsMotivosDataSet
dm.MotivosLeft(Top:   