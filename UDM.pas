unit UDM;

interface

uses
  System.SysUtils, System.Classes, Data.DBXFirebird, Data.FMTBcd, sSkinManager,
  Data.DB, Vcl.AppEvnts, SimpleDS, RxMemDS, Data.SqlExpr, System.ImageList,
  Vcl.ImgList, Vcl.Controls, acAlphaImageList, Datasnap.DBClient,
  Datasnap.Provider, Vcl.Forms, MidasLib, IniFiles, StdCtrls,
  Dialogs, Winapi.Windows, Messages, sComboBox, ShellAPI, IBServices,
  Data.DBXInterBase, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.Comp.UI,
  FireDAC.Phys.IBBase, frxClass, frxDBSet, frxDesgn, frxDesgnCtrls,
  System.DateUtils, System.Variants, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase, IdFTP;

type
  Tdm = class(TDataModule)
    dspTitulos: TDataSetProvider;
    Titulos: TClientDataSet;
    Imagens: TsAlphaImageList;
    dspTipos: TDataSetProvider;
    Tipos: TClientDataSet;
    dspQualificacoes: TDataSetProvider;
    Qualificacoes: TClientDataSet;
    QualificacoesCODIGO: TIntegerField;
    QualificacoesDESCRICAO: TStringField;
    QualificacoesMOSTRAR: TStringField;
    dspOrgaos: TDataSetProvider;
    Orgaos: TClientDataSet;
    OrgaosID_ORGAO: TIntegerField;
    OrgaosDESCRICAO: TStringField;
    dspMunicipios: TDataSetProvider;
    Municipios: TClientDataSet;
    MunicipiosUF: TStringField;
    MunicipiosCIDADE: TStringField;
    MunicipiosCODIGO: TIntegerField;
    dspIds: TDataSetProvider;
    Ids: TClientDataSet;
    IdsCAMPO: TStringField;
    IdsVALOR: TIntegerField;
    dspCodigos: TDataSetProvider;
    Codigos: TClientDataSet;
    dspTabela: TDataSetProvider;
    Tabela: TClientDataSet;
    TabelaORDEM: TIntegerField;
    TabelaANO: TIntegerField;
    TabelaVAI: TStringField;
    TabelaTAB: TStringField;
    TabelaITEM: TStringField;
    TabelaSUB: TStringField;
    TabelaDESCR: TStringField;
    TabelaVALOR: TFloatField;
    TabelaTEXTO: TStringField;
    dspItens: TDataSetProvider;
    Itens: TClientDataSet;
    dspCustas: TDataSetProvider;
    Custas: TClientDataSet;
    CustasID_CUSTA: TIntegerField;
    CustasID_ATO: TIntegerField;
    CustasTABELA: TStringField;
    CustasITEM: TStringField;
    CustasSUBITEM: TStringField;
    CustasVALOR: TFloatField;
    CustasQTD: TIntegerField;
    CustasTOTAL: TFloatField;
    RxCustas: TRxMemoryData;
    RxCustasID_CUSTA: TIntegerField;
    RxCustasID_ATO: TIntegerField;
    RxCustasTABELA: TStringField;
    RxCustasITEM: TStringField;
    RxCustasSUBITEM: TStringField;
    RxCustasVALOR: TFloatField;
    RxCustasQTD: TIntegerField;
    RxCustasTOTAL: TFloatField;
    RxCustasDESCRICAO: TStringField;
    dspDevedores: TDataSetProvider;
    Devedores: TClientDataSet;
    dspServentia: TDataSetProvider;
    Serventia: TClientDataSet;
    ServentiaCODIGO: TIntegerField;
    ServentiaDESCRICAO: TStringField;
    ServentiaENDERECO: TStringField;
    ServentiaBAIRRO: TStringField;
    ServentiaCIDADE: TStringField;
    ServentiaUF: TStringField;
    ServentiaCEP: TStringField;
    ServentiaTELEFONE: TStringField;
    ServentiaEMAIL: TStringField;
    ServentiaTABELIAO: TStringField;
    ServentiaMATRICULA: TStringField;
    ServentiaSUBSTITUTO: TStringField;
    ServentiaCNPJ: TStringField;
    RxDevedor: TRxMemoryData;
    dspPortadores: TDataSetProvider;
    Portadores: TClientDataSet;
    dspControle: TDataSetProvider;
    Controle: TClientDataSet;
    ControleID_CONTROLE: TIntegerField;
    ControleSIGLA: TStringField;
    ControleDESCRICAO: TStringField;
    ControleVALOR: TIntegerField;
    dspImportados: TDataSetProvider;
    Importados: TClientDataSet;
    ImportadosID_IMPORTADO: TIntegerField;
    ImportadosDATA: TDateField;
    ImportadosTITULOS: TIntegerField;
    ImportadosACEITOS: TIntegerField;
    ImportadosREJEITADOS: TIntegerField;
    dspEscreventes: TDataSetProvider;
    Escreventes: TClientDataSet;
    EscreventesID_ESCREVENTE: TIntegerField;
    EscreventesNOME: TStringField;
    EscreventesCPF: TStringField;
    EscreventesQUALIFICACAO: TStringField;
    EscreventesLOGIN: TStringField;
    EscreventesSENHA: TStringField;
    EscreventesAUTORIZADO: TStringField;
    dspMovimento: TDataSetProvider;
    Movimento: TClientDataSet;
    MovimentoID_MOVIMENTO: TIntegerField;
    MovimentoID_ATO: TIntegerField;
    MovimentoDATA: TDateField;
    MovimentoHORA: TTimeField;
    MovimentoBAIXA: TDateField;
    MovimentoESCREVENTE: TStringField;
    MovimentoSTATUS: TStringField;
    ControleLETRA: TStringField;
    dspMotivos: TDataSetProvider;
    Motivos: TClientDataSet;
    MotivosID_MOTIVO: TIntegerField;
    TiposCODIGO: TIntegerField;
    TiposDESCRICAO: TStringField;
    TiposSIGLA: TStringField;
    ServentiaSEXO: TStringField;
    ServentiaTITULO: TStringField;
    dspParametros: TDataSetProvider;
    Parametros: TClientDataSet;
    ParametrosID_PARAMETRO: TIntegerField;
    ParametrosDESCRICAO: TStringField;
    ParametrosVALOR: TStringField;
    dspFeriados: TDataSetProvider;
    Feriados: TClientDataSet;
    FeriadosID_FERIADO: TIntegerField;
    FeriadosDIA: TIntegerField;
    FeriadosMES: TIntegerField;
    FeriadosDESCRICAO: TStringField;
    FeriadosATIVO: TStringField;
    dspCertidoes: TDataSetProvider;
    Certidoes: TClientDataSet;
    RxPortador: TRxMemoryData;
    dspIrregularidades: TDataSetProvider;
    Irregularidades: TClientDataSet;
    IrregularidadesCODIGO: TIntegerField;
    IrregularidadesMOTIVO: TStringField;
    dspOcorrencias: TDataSetProvider;
    Ocorrencias: TClientDataSet;
    OcorrenciasCODIGO: TStringField;
    OcorrenciasDESCRICAO: TStringField;
    ServentiaPRACA: TStringField;
    ServentiaSITE: TStringField;
    dspSacadores: TDataSetProvider;
    Sacadores: TClientDataSet;
    RxSacador: TRxMemoryData;
    SacadoresID_ATO: TIntegerField;
    SacadoresID_SACADOR: TIntegerField;
    SacadoresNOME: TStringField;
    SacadoresDOCUMENTO: TStringField;
    SacadoresENDERECO: TStringField;
    SacadoresCEP: TStringField;
    SacadoresMUNICIPIO: TStringField;
    SacadoresUF: TStringField;
    RxSacadorID_ATO: TIntegerField;
    RxSacadorID_SACADOR: TIntegerField;
    RxSacadorNOME: TStringField;
    RxSacadorDOCUMENTO: TStringField;
    RxSacadorENDERECO: TStringField;
    RxSacadorCEP: TStringField;
    RxSacadorMUNICIPIO: TStringField;
    RxSacadorUF: TStringField;
    SacadoresTIPO: TStringField;
    RxSacadorTIPO: TStringField;
    SacadoresBAIRRO: TStringField;
    RxSacadorBAIRRO: TStringField;
    dspMensagens: TDataSetProvider;
    Mensagens: TClientDataSet;
    MensagensID_MSG: TIntegerField;
    MensagensMENSAGEM: TStringField;
    ServentiaCODIGO_PRACA: TIntegerField;
    dspResponsavel: TDataSetProvider;
    Responsavel: TClientDataSet;
    ResponsavelID_ESCREVENTE: TIntegerField;
    ResponsavelNOME: TStringField;
    ResponsavelCPF: TStringField;
    ResponsavelQUALIFICACAO: TStringField;
    ResponsavelLOGIN: TStringField;
    ResponsavelSENHA: TStringField;
    ResponsavelAUTORIZADO: TStringField;
    dspRemessas: TDataSetProvider;
    Remessas: TClientDataSet;
    RemessasID_REMESSA: TIntegerField;
    RemessasDATA: TDateField;
    RemessasNUMERO: TIntegerField;
    ServentiaCODIGO_MUNICIPIO: TStringField;
    Criticas: TRxMemoryData;
    CriticasID: TIntegerField;
    CriticasNumero_Titulo: TStringField;
    CriticasCodigo_Critica: TIntegerField;
    CriticasDescricao: TStringField;
    dspOnline: TDataSetProvider;
    Online: TClientDataSet;
    dspDiaria: TDataSetProvider;
    Diaria: TClientDataSet;
    DiariaID_ONLINE: TIntegerField;
    DiariaDATA: TDateField;
    DiariaPROTESTOS: TIntegerField;
    DiariaSUSTADOS: TIntegerField;
    DiariaENVIADO: TStringField;
    OnlineID_ONLINE: TIntegerField;
    OnlineDATA: TDateField;
    OnlinePROTESTOS: TIntegerField;
    OnlineSUSTADOS: TIntegerField;
    OnlineENVIADO: TStringField;
    OnlineCANCELADOS: TIntegerField;
    DiariaCANCELADOS: TIntegerField;
    IrregularidadesCheck: TBooleanField;
    Im24: TsAlphaImageList;
    dspSubitem: TDataSetProvider;
    Subitem: TClientDataSet;
    SubitemTAB: TStringField;
    SubitemITEM: TStringField;
    SubitemSUB: TStringField;
    SubitemDESCR: TStringField;
    SubitemVALOR: TFloatField;
    SubitemVAI: TStringField;
    SubitemORDEM: TIntegerField;
    dspPracas: TDataSetProvider;
    Pracas: TClientDataSet;
    PracasID_PRACA: TIntegerField;
    PracasNOME: TStringField;
    dspBoletos: TDataSetProvider;
    Boletos: TClientDataSet;
    dspAdicional: TDataSetProvider;
    Adicional: TClientDataSet;
    AdicionalATRIB: TIntegerField;
    AdicionalCOD: TIntegerField;
    AdicionalATOS: TStringField;
    AdicionalEMOL: TFloatField;
    AdicionalFETJ: TFloatField;
    AdicionalFUND: TFloatField;
    AdicionalFUNP: TFloatField;
    AdicionalTOT: TFloatField;
    AdicionalDIA: TIntegerField;
    AdicionalMINIMO: TFloatField;
    AdicionalMAXIMO: TFloatField;
    AdicionalMUTUA: TFloatField;
    AdicionalACOTERJ: TFloatField;
    AdicionalDISTRIB: TFloatField;
    AdicionalTITULO: TStringField;
    AdicionalCONVENIO: TStringField;
    AE: TApplicationEvents;
    EscreventesMATRICULA: TStringField;
    dsServentia: TDataSource;
    ResponsavelMATRICULA: TStringField;
    ItensID_CUS: TIntegerField;
    ItensCOD: TIntegerField;
    ItensTAB: TStringField;
    ItensITEM: TStringField;
    ItensSUB: TStringField;
    ItensDESCR: TStringField;
    ItensVALOR: TFloatField;
    ItensQTD: TStringField;
    ItensTOTAL: TFloatField;
    ItensCOMP: TStringField;
    ItensEXC: TStringField;
    AdicionalFUNA: TFloatField;
    AdicionalPMCMV: TFloatField;
    AdicionalOCULTO: TStringField;
    SM: TsSkinManager;
    cBaixas: TClientDataSet;
    PortadoresCODIGO: TStringField;
    PortadoresNOME: TStringField;
    PortadoresID_PORTADOR: TIntegerField;
    PortadoresTIPO: TStringField;
    PortadoresDOCUMENTO: TStringField;
    PortadoresENDERECO: TStringField;
    PortadoresBANCO: TStringField;
    PortadoresCONVENIO: TStringField;
    PortadoresCONTA: TStringField;
    PortadoresOBSERVACAO: TMemoField;
    PortadoresAGENCIA: TStringField;
    PortadoresPRACA: TStringField;
    PortadoresCRA: TStringField;
    PortadoresSEQUENCIA: TIntegerField;
    PortadoresVALOR_DOC: TFloatField;
    PortadoresVALOR_TED: TFloatField;
    RxPortadorAGENCIA: TStringField;
    RxPortadorPRACA: TStringField;
    RxPortadorCRA: TStringField;
    RxPortadorSEQUENCIA: TIntegerField;
    RxPortadorNOME: TStringField;
    RxPortadorID_PORTADOR: TIntegerField;
    RxPortadorCODIGO: TStringField;
    RxPortadorTIPO: TStringField;
    RxPortadorDOCUMENTO: TStringField;
    RxPortadorENDERECO: TStringField;
    RxPortadorBANCO: TStringField;
    RxPortadorCONVENIO: TStringField;
    RxPortadorCONTA: TStringField;
    RxPortadorOBSERVACAO: TMemoField;
    BDOnline: TSQLConnection;
    sEletronico: TSimpleDataSet;
    EletronicoCODIGO: TIntegerField;
    EletronicoPATH_GERENCIAL: TStringField;
    ImLogo: TsAlphaImageList;
    BoletosDATA_PROTOCOLO: TDateField;
    BoletosPROTOCOLO: TIntegerField;
    BoletosVALOR: TFloatField;
    BoletosNOSSO_NUMERO: TStringField;
    BoletosDATA_EMISSAO: TDateField;
    BoletosHORA: TTimeField;
    BoletosEMISSOR: TStringField;
    BoletosID_BOLETO: TIntegerField;
    BoletosID_ATO: TIntegerField;
    BoletosID_DEVEDOR: TIntegerField;
    BoletosESPECIE_DOCUMENTO: TStringField;
    BoletosMOEDA: TStringField;
    BoletosACEITE: TStringField;
    BoletosCARTEIRA: TStringField;
    BoletosVALOR_MORA_JUROS: TFloatField;
    BoletosVALOR_DESCONTO: TFloatField;
    BoletosVALOR_ABATIMENTO: TFloatField;
    BoletosPORCENTO_MULTA: TFloatField;
    BoletosDATA_MULTA_JUROS: TDateField;
    BoletosDATA_DESCONTO: TDateField;
    BoletosDATA_ABATIMENTO: TDateField;
    BoletosDATA_PROTESTO: TDateField;
    BoletosNUMERO_DOCUMENTO: TStringField;
    BoletosDATA_VENCIMENTO: TDateField;
    BoletosINSTRUCAO1: TStringField;
    BoletosINSTRUCAO2: TStringField;
    BoletosINSTRUCAO3: TStringField;
    BoletosINSTRUCAO4: TStringField;
    BoletosINSTRUCAO5: TStringField;
    BoletosPAGO: TStringField;
    BoletosEMOLUMENTOS: TFloatField;
    BoletosFETJ: TFloatField;
    BoletosFUNDPERJ: TFloatField;
    BoletosFUNPERJ: TFloatField;
    BoletosFUNARPEN: TFloatField;
    BoletosPMCMV: TFloatField;
    BoletosTOTAL: TFloatField;
    BoletosCCT: TStringField;
    cBaixasID_ATO: TIntegerField;
    cBaixasPROTOCOLO: TIntegerField;
    cBaixasDATA: TDateField;
    cBaixasHORA: TTimeField;
    cBaixasREGISTRO: TIntegerField;
    cBaixasLIVRO: TIntegerField;
    cBaixasFOLHA: TStringField;
    cBaixasRECIBO: TIntegerField;
    cBaixasID_RESERVADO: TIntegerField;
    cBaixasSELO: TStringField;
    cBaixasALEATORIO: TStringField;
    cBaixasESCREVENTE: TStringField;
    cBaixasCPF_ESCREVENTE: TStringField;
    cBaixasFORMA_PAGAMENTO: TStringField;
    cBaixasNUMERO_PAGAMENTO: TStringField;
    cBaixasVALOR_PAGAMENTO: TFloatField;
    cBaixasCPF_ESCREVENTE_PG: TStringField;
    cBaixasID_MSG: TIntegerField;
    cBaixasMENSAGEM: TStringField;
    cBaixasDETERMINACAO: TStringField;
    cBaixasCONVENIO: TStringField;
    cBaixasTIPO_COBRANCA: TStringField;
    cBaixasEMOLUMENTOS: TFloatField;
    cBaixasFETJ: TFloatField;
    cBaixasFUNDPERJ: TFloatField;
    cBaixasFUNPERJ: TFloatField;
    cBaixasFUNARPEN: TFloatField;
    cBaixasPMCMV: TFloatField;
    cBaixasMUTUA: TFloatField;
    cBaixasACOTERJ: TFloatField;
    cBaixasDISTRIBUICAO: TFloatField;
    cBaixasAPONTAMENTO: TFloatField;
    cBaixasTOTAL: TFloatField;
    cBaixasTARIFA: TStringField;
    RxDevedorID_ATO: TIntegerField;
    RxDevedorID_DEVEDOR: TIntegerField;
    RxDevedorORDEM: TIntegerField;
    RxDevedorNOME: TStringField;
    RxDevedorTIPO: TStringField;
    RxDevedorDOCUMENTO: TStringField;
    RxDevedorIFP_DETRAN: TStringField;
    RxDevedorIDENTIDADE: TStringField;
    RxDevedorDT_EMISSAO: TDateField;
    RxDevedorORGAO: TStringField;
    RxDevedorCEP: TStringField;
    RxDevedorENDERECO: TStringField;
    RxDevedorBAIRRO: TStringField;
    RxDevedorMUNICIPIO: TStringField;
    RxDevedorUF: TStringField;
    RxDevedorIGNORADO: TStringField;
    RxDevedorJUSTIFICATIVA: TMemoField;
    RxDevedorTELEFONE: TStringField;
    DevedoresNOME: TStringField;
    DevedoresDOCUMENTO: TStringField;
    DevedoresID_ATO: TIntegerField;
    DevedoresID_DEVEDOR: TIntegerField;
    DevedoresORDEM: TIntegerField;
    DevedoresTIPO: TStringField;
    DevedoresIFP_DETRAN: TStringField;
    DevedoresIDENTIDADE: TStringField;
    DevedoresDT_EMISSAO: TDateField;
    DevedoresORGAO: TStringField;
    DevedoresCEP: TStringField;
    DevedoresENDERECO: TStringField;
    DevedoresBAIRRO: TStringField;
    DevedoresMUNICIPIO: TStringField;
    DevedoresUF: TStringField;
    DevedoresIGNORADO: TStringField;
    DevedoresJUSTIFICATIVA: TMemoField;
    DevedoresTELEFONE: TStringField;
    RemessasID_PORTADOR: TIntegerField;
    cBaixasCODIGO: TIntegerField;
    cBaixasSALDO_TITULO: TFloatField;
    cBaixasVALOR_TITULO: TFloatField;
    PortadoresNOMINAL: TStringField;
    RXDuplicados: TRxMemoryData;
    RXDuplicadosID: TIntegerField;
    RXDuplicadosNOME: TStringField;
    RXDuplicadosCODIGO: TStringField;
    RXDuplicadosDOCUMENTO: TStringField;
    RXDuplicadosCRA: TStringField;
    RXDuplicadosBANCO: TStringField;
    RXDuplicadosCONVENIO: TStringField;
    MotivosDESCRICAO: TStringField;
    PortadoresFORCA_LEI: TStringField;
    CodigosATRIB: TIntegerField;
    CodigosCOD: TIntegerField;
    CodigosATOS: TStringField;
    CodigosTITULO: TStringField;
    CodigosEMOL: TFloatField;
    CodigosFETJ: TFloatField;
    CodigosFUND: TFloatField;
    CodigosFUNP: TFloatField;
    CodigosFUNA: TFloatField;
    CodigosPMCMV: TFloatField;
    CodigosISS: TFloatField;
    CodigosMUTUA: TFloatField;
    CodigosACOTERJ: TFloatField;
    CodigosDISTRIB: TFloatField;
    CodigosTOT: TFloatField;
    CodigosMINIMO: TFloatField;
    CodigosMAXIMO: TFloatField;
    CodigosDIA: TIntegerField;
    CodigosCONVENIO: TStringField;
    CodigosOCULTO: TStringField;
    TitulosEspecie: TStringField;
    TitulosDocDevedor: TStringField;
    TitulosDocApresentante: TStringField;
    cBaixasISS: TFloatField;
    CertidoesID_CERTIDAO: TIntegerField;
    CertidoesID_ATO: TIntegerField;
    CertidoesORDEM: TIntegerField;
    CertidoesDT_PEDIDO: TDateField;
    CertidoesDT_ENTREGA: TDateField;
    CertidoesDT_CERTIDAO: TDateField;
    CertidoesTIPO_CERTIDAO: TStringField;
    CertidoesNUMERO_TITULO: TStringField;
    CertidoesANOS: TIntegerField;
    CertidoesCONVENIO: TStringField;
    CertidoesCODIGO: TIntegerField;
    CertidoesSELO: TStringField;
    CertidoesALEATORIO: TStringField;
    CertidoesRECIBO: TIntegerField;
    CertidoesREQUERIDO: TStringField;
    CertidoesTIPO_REQUERIDO: TStringField;
    CertidoesCPF_CNPJ_REQUERIDO: TStringField;
    CertidoesREQUERENTE: TStringField;
    CertidoesCPF_CNPJ_REQUERENTE: TStringField;
    CertidoesTELEFONE_REQUERENTE: TStringField;
    CertidoesRESULTADO: TStringField;
    CertidoesFOLHAS: TIntegerField;
    CertidoesREGISTROS: TIntegerField;
    CertidoesCOBRANCA: TStringField;
    CertidoesEMOLUMENTOS: TFloatField;
    CertidoesFETJ: TFloatField;
    CertidoesFUNDPERJ: TFloatField;
    CertidoesFUNPERJ: TFloatField;
    CertidoesFUNARPEN: TFloatField;
    CertidoesPMCMV: TFloatField;
    CertidoesISS: TFloatField;
    CertidoesMUTUA: TFloatField;
    CertidoesACOTERJ: TFloatField;
    CertidoesDISTRIBUICAO: TFloatField;
    CertidoesAPONTAMENTO: TFloatField;
    CertidoesCAPA: TFloatField;
    CertidoesTOTAL: TFloatField;
    CertidoesEXCOBRANCA: TStringField;
    CertidoesEXRECIBO: TIntegerField;
    CertidoesEXEMOLUMENTOS: TFloatField;
    CertidoesEXFETJ: TFloatField;
    CertidoesEXFUNDPERJ: TFloatField;
    CertidoesEXFUNPERJ: TFloatField;
    CertidoesEXFUNARPEN: TFloatField;
    CertidoesEXISS: TFloatField;
    CertidoesEXTOTAL: TFloatField;
    CertidoesCCT: TStringField;
    CertidoesESCREVENTE: TStringField;
    CertidoesRESPONSAVEL: TStringField;
    CertidoesOBSERVACAO: TMemoField;
    CertidoesENVIADO: TStringField;
    CertidoesENVIADO_FLS: TStringField;
    CertidoesDocumento: TStringField;
    CertidoesPeriodo: TStringField;
    CertidoesNomeCertidao: TStringField;
    PortadoresESPECIE: TStringField;
    dspServentiaAgregada: TDataSetProvider;
    ServentiaAgregada: TClientDataSet;
    ServentiaAgregadaCODIGO: TIntegerField;
    ServentiaAgregadaDESCRICAO: TStringField;
    ServentiaAgregadaCODIGO_DISTRIBUICAO: TIntegerField;
    ServentiaAgregadaDATA_ULTIMO_SALDO: TDateField;
    ServentiaAgregadaFLG_ATIVA: TStringField;
    ServentiaAgregadaDATA_INATIVACAO: TDateField;
    ServentiaAgregadaVALOR_ULTIMO_SALDO: TFloatField;
    ServentiaAgregadaID_SERVENTIA_AGREGADA: TIntegerField;
    dspExportado: TDataSetProvider;
    Exportado: TClientDataSet;
    dsExportado: TDataSource;
    ExportadoID_EXPORTADO: TIntegerField;
    ExportadoNOME_ARQUIVO_IMP: TStringField;
    ExportadoCOD_SERV_AGREGADA: TIntegerField;
    ExportadoNOME_ARQUIVO_EXP: TStringField;
    ExportadoDATA_EXPORTACAO: TDateField;
    ExportadoQTD_TITULOS: TIntegerField;
    ServentiaAgregadaPRIORIDADE_VALOR: TIntegerField;
    ServentiaAgregadaPRIORIDADE_QTD: TIntegerField;
    ServentiaAgregadaQTD_MAX_PREV: TIntegerField;
    ServentiaAgregadaVALOR_DIF: TFloatField;
    ServentiaAgregadaVALOR_MAX_PREV: TFloatField;
    ServentiaAgregadaQTD_ULTIMO_SALDO: TIntegerField;
    ServentiaAgregadaQTD_DIF: TIntegerField;
    ServentiaAgregadaVALOR_REMANESCENTE: TFloatField;
    ServentiaAgregadaVALOR_ALOCADO: TFloatField;
    ServentiaAgregadaQTD_ALOCADA: TIntegerField;
    ImportadosARQUIVO: TStringField;
    conSISTEMA: TFDConnection;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    conCENTRAL: TFDConnection;
    FDPhysFBDriverLink2: TFDPhysFBDriverLink;
    FDGUIxWaitCursor2: TFDGUIxWaitCursor;
    ServentiaAgregadaFLG_USAPROTESTO: TStringField;
    frptRel: TfrxReport;
    frxDsgn: TfrxDesigner;
    qryRel01_ArqIE: TFDQuery;
    dsRel01_ArqIE: TDataSource;
    fdsRel01_ArqIE: TfrxDBDataset;
    TitulosSERVENTIA_AGREGADA: TStringField;
    qryRel01_Tit: TFDQuery;
    dsRel01_Tit: TDataSource;
    fdsRel01_Tit: TfrxDBDataset;
    qryRel01_TitDT_ENTRADA: TDateField;
    qryRel01_TitDT_PROTOCOLO: TDateField;
    qryRel01_TitPROTOCOLO: TIntegerField;
    qryRel01_TitPROTOCOLO_CARTORIO: TIntegerField;
    qryRel01_TitAPRESENTANTE: TStringField;
    qryRel01_TitDEVEDOR: TStringField;
    qryRel01_TitVALOR_TITULO: TFloatField;
    qryRel01_TitSALDO_TITULO: TFloatField;
    qryRel01_TitARQUIVO_IMPORTADO_CRA: TStringField;
    qryRel01_TitARQUIVO_EXPORTADO_SA: TStringField;
    qryRel01_TitARQUIVO_RET_SA: TStringField;
    qryRel01_TitARQUIVO_RET_CRA: TStringField;
    qryRel01_TitSTATUS_TITULO: TStringField;
    qryRel01_TitID_SA: TIntegerField;
    qryRel01_TitCODIGO_SA: TIntegerField;
    qryRel01_TitDESCRICAO: TStringField;
    qryRel01_TitVALOR_ULTIMO_SALDO: TFloatField;
    qryRel01_TitQTD_ULTIMO_SALDO: TIntegerField;
    qryRel01_TitDATA_ULTIMO_SALDO: TDateField;
    qryRel01_TitFLG_USAPROTESTO: TStringField;
    qryRel01_TitTOTAL_CUSTAS: TFloatField;
    FeriadosANO: TIntegerField;
    qryFeriados: TFDQuery;
    FeriadosFLG_FIXO: TStringField;
    qryDevedores: TFDQuery;
    qryImportados: TFDQuery;
    qryPortadores: TFDQuery;
    qryServentiaAgregada: TFDQuery;
    qryServentia: TFDQuery;
    qryIds: TFDQuery;
    qrySacadores: TFDQuery;
    ServentiaAgregadaLOGRADOURO: TStringField;
    ServentiaAgregadaCIDADE_LOGRADOURO: TStringField;
    ServentiaAgregadaUF_LOGRADOURO: TStringField;
    ServentiaAgregadaCEP: TStringField;
    ServentiaAgregadaNOME_TITULAR: TStringField;
    ServentiaAgregadaTELEFONE1: TStringField;
    ServentiaAgregadaTELEFONE2: TStringField;
    ServentiaAgregadaEMAIL1: TStringField;
    ServentiaAgregadaEMAIL2: TStringField;
    qryTitulos: TFDQuery;
    qryExportado: TFDQuery;
    qryPracas: TFDQuery;
    qryIrregularidades: TFDQuery;
    qryMovimento: TFDQuery;
    qryCustas: TFDQuery;
    qryRemessas: TFDQuery;
    qryControle: TFDQuery;
    qryTipos: TFDQuery;
    qryQualificacoes: TFDQuery;
    qryOrgaos: TFDQuery;
    qryMunicipios: TFDQuery;
    qryResponsavel: TFDQuery;
    qryDiaria: TFDQuery;
    qryVerbal: TFDQuery;
    qryVerbalID_VERBAL: TIntegerField;
    qryVerbalDATA: TDateField;
    qryVerbalCCT: TStringField;
    qryVerbalNUMERO: TStringField;
    qryVerbalESCREVENTE: TStringField;
    qryVerbalNOME: TStringField;
    qryVerbalTIPO_COBRANCA: TStringField;
    qryVerbalEMOLUMENTOS: TFloatField;
    qryVerbalFETJ: TFloatField;
    qryVerbalFUNDPERJ: TFloatField;
    qryVerbalFUNPERJ: TFloatField;
    qryVerbalFUNARPEN: TFloatField;
    qryVerbalTOTAL: TFloatField;
    qryVerbalENVIADO: TStringField;
    qryOnline: TFDQuery;
    qryBoletos: TFDQuery;
    qryMensagens: TFDQuery;
    qryOcorrencias: TFDQuery;
    qryCertidoes: TFDQuery;
    qryMotivos: TFDQuery;
    qryParametros: TFDQuery;
    qryEscreventes: TFDQuery;
    CertidoesQTD: TIntegerField;
    BoletosENVIADO: TStringField;
    BoletosDATA_BAIXA: TDateField;
    qryVersao: TFDQuery;
    qrySuporte: TFDQuery;
    qryVersaoDATA: TStringField;
    qryCodigos: TFDQuery;
    qryTabela: TFDQuery;
    qryItens: TFDQuery;
    qrySubItem: TFDQuery;
    qryAdicional: TFDQuery;
    qryFaixa: TFDQuery;
    qryFaixaCOD: TIntegerField;
    qryFaixaEMOL: TFloatField;
    qryFaixaFETJ: TFloatField;
    qryFaixaFUND: TFloatField;
    qryFaixaFUNP: TFloatField;
    qryFaixaFUNA: TFloatField;
    qryFaixaPMCMV: TFloatField;
    qryFaixaISS: TFloatField;
    qryFaixaMUTUA: TFloatField;
    qryFaixaACOTERJ: TFloatField;
    qryFaixaDISTRIB: TFloatField;
    qryFaixaTOT: TFloatField;
    qryFaixaTITULO: TStringField;
    AdicionalISS: TFloatField;
    SubitemANO: TIntegerField;
    SubitemTEXTO: TStringField;
    FeriadosDESCR_MES: TStringField;
    qryRel01_ArqIEARQUIVO_IMPORTADO_CRA: TStringField;
    qryRel01_ArqIEID_IMPORTADO: TIntegerField;
    qryRel01_ArqIEDATA_IMPORT_ARQ_CRA: TDateField;
    qryRel01_ArqIEQTD_TIT_ARQUIVO_CRA: TIntegerField;
    qryRel01_ArqIEQTD_TIT_ACEITOS_CRA: TIntegerField;
    qryRel01_ArqIEQTD_TIT_REJ_CRA: TIntegerField;
    qryRel01_ArqIEARQUIVO_EXPORTADO_SA: TStringField;
    qryRel01_ArqIEDATA_EXPORT_ARQ_SA: TDateField;
    qryRel01_ArqIEQTD_TIT_ARQUIVO_SA: TIntegerField;
    qryRel01_ArqIEARQUIVO_RET_SA: TStringField;
    qryRel01_ArqIEDATA_RETORNO_ARQ_SA: TDateField;
    qryRel01_ArqIEARQUIVO_RET_CRA: TStringField;
    qryRel01_ArqIEDATA_RETORNO_ARQ_CRA: TDateField;
    qryRel01_ArqIEDT_ENTRADA: TDateField;
    qryRel01_ArqIEQTD_ACEITO_CRA: TIntegerField;
    qryRel01_ArqIEVR_ACEITO_CRA: TFloatField;
    qryRel01_ArqIEQTD_EXPORTADO_SA: TIntegerField;
    qryRel01_ArqIEVR_EXPORTADO_SA: TFloatField;
    qryRel01_ArqIEVR_CUSTAS_EXP_SA: TFloatField;
    qryRel01_ArqIEQTD_RETORNO_SA: TIntegerField;
    qryRel01_ArqIEVR_RETORNO_SA: TFloatField;
    qryRel01_ArqIEVR_CUSTAS_RET_SA: TFloatField;
    qryRel01_ArqIEQTD_RETORNO_CRA: TIntegerField;
    qryRel01_ArqIEVR_RETORNO_CRA: TFloatField;
    qryRel01_ArqIEVR_CUSTAS_RET_CRA: TFloatField;
    mdLinhaArqD: TRxMemoryData;
    mdLinhaArqDID_REGISTRO: TIntegerField;
    mdLinhaArqDNUM_COD_PORTADOR: TStringField;
    mdLinhaArqDNOME_PORTADOR: TStringField;
    mdLinhaArqDDATA_MOVIMENTO: TStringField;
    mdLinhaArqDID_TRANS_REMET: TStringField;
    mdLinhaArqDID_TRANS_DESTIN: TStringField;
    mdLinhaArqDID_TRANS_TIPO: TStringField;
    mdLinhaArqDNUM_SEQ_REMESSA: TStringField;
    mdLinhaArqDQTD_REG_REMESSA: TStringField;
    mdLinhaArqDQTD_TIT_REMESSA: TStringField;
    mdLinhaArqDQTD_IND_REMESSA: TStringField;
    mdLinhaArqDQTD_ORIG_REMESSA: TStringField;
    mdLinhaArqDID_AGENCIA: TStringField;
    mdLinhaArqDVERSAO_LAYOUT: TStringField;
    mdLinhaArqDCOD_MUNIC_PRACA: TStringField;
    mdLinhaArqDCOMPL_REGISTRO: TStringField;
    mdLinhaArqDNUM_SEQ_REGISTRO: TStringField;
    dsLinhaArqD: TDataSource;
    dsArquivoD: TDataSource;
    mdArquivoD: TRxMemoryData;
    mdArquivoDID_REGISTRO: TStringField;
    mdArquivoDNUM_COD_PORTADOR: TStringField;
    mdArquivoDCOD_CEDENTE: TStringField;
    mdArquivoDNOME_CEDENTE: TStringField;
    mdArquivoDNOME_SACADOR: TStringField;
    mdArquivoDNUM_DOC_SACADOR: TStringField;
    mdArquivoDEND_SACADOR: TStringField;
    mdArquivoDCEP_SACADOR: TStringField;
    mdArquivoDCID_SACADOR: TStringField;
    mdArquivoDUF_SACADOR: TStringField;
    mdArquivoDNOSSO_NUMERO: TStringField;
    mdArquivoDESPECIE: TStringField;
    mdArquivoDDATA_EMISSAO: TStringField;
    mdArquivoDDATA_VENCIMENTO: TStringField;
    mdArquivoDTIPO_MOEDA: TStringField;
    mdArquivoDVALOR_TITULO: TStringField;
    mdArquivoDSALDO_TITULO: TStringField;
    mdArquivoDPRACA_PROTESTO: TStringField;
    mdArquivoDTIPO_ENDOSSO: TStringField;
    mdArquivoDINFO_ACEITE: TStringField;
    mdArquivoDNUM_CONTROLE_DEV: TStringField;
    mdArquivoDNOME_DEVEDOR: TStringField;
    mdArquivoDTIPO_ID_DEVEDOR: TStringField;
    mdArquivoDNUM_ID_DEVEDOR: TStringField;
    mdArquivoDNUM_DOC_DEVEDOR: TStringField;
    mdArquivoDEND_DEVEDOR: TStringField;
    mdArquivoDCEP_DEVEDOR: TStringField;
    mdArquivoDCID_DEVEDOR: TStringField;
    mdArquivoDUF_DEVEDOR: TStringField;
    mdArquivoDCOD_CARTORIO: TStringField;
    mdArquivoDNUM_PROTOCOLO_DIST: TStringField;
    mdArquivoDTIPO_OCORRENCIA: TStringField;
    mdArquivoDDATA_PROTOCOLO: TStringField;
    mdArquivoDVALOR_CUSTAS_CART: TStringField;
    mdArquivoDDECLARACAO_PORT: TStringField;
    mdArquivoDDATA_OCORRENCIA: TStringField;
    mdArquivoDCOD_IRREGULARIDADE: TStringField;
    mdArquivoDBAIRRO_DEVEDOR: TStringField;
    mdArquivoDVALOR_CUSTAS_DISTRIB: TStringField;
    mdArquivoDREG_DISTRIBUICAO: TStringField;
    mdArquivoDVALOR_GRAVACAO: TStringField;
    mdArquivoDNUM_OPERACAO: TStringField;
    mdArquivoDNUM_CONTRATO: TStringField;
    mdArquivoDNUM_PARCELA: TStringField;
    mdArquivoDTIPO_LETRA_CAMBIO: TStringField;
    mdArquivoDCOMPL_COD_IRREG: TStringField;
    mdArquivoDPROTESTO_FALENCIA: TStringField;
    mdArquivoDINSTRUMENTO_PROT: TStringField;
    mdArquivoDVALOR_DDESPESAS: TStringField;
    mdArquivoDCOMPL_REGISTRO: TStringField;
    mdArquivoDSEQUENCIAL_REG: TStringField;
    mdArquivoDEMOLUMENTOS: TStringField;
    mdArquivoDFETJ: TStringField;
    mdArquivoDFUNDPERJ: TStringField;
    mdArquivoDFUNPERJ: TStringField;
    mdArquivoDFUNARPEN: TStringField;
    mdArquivoDPMCMV: TStringField;
    mdArquivoDISS: TStringField;
    mdArquivoDMUTUA: TStringField;
    mdArquivoDACOTERJ: TStringField;
    mdArquivoDDISTRIBUICAO: TStringField;
    mdArquivoDPRIORIDADE_VALOR: TIntegerField;
    mdArquivoDFLG_ALOCADO: TBooleanField;
    mdArquivoDSEQ_BANCO: TIntegerField;
    mdArquivoDID_ATO: TIntegerField;
    mdArquivoDNOME_PORTADOR: TStringField;
    mdArqDAux: TRxMemoryData;
    mdArqDAuxID_REGISTRO: TStringField;
    mdArqDAuxNUM_COD_PORTADOR: TStringField;
    mdArqDAuxCOD_CEDENTE: TStringField;
    mdArqDAuxNOME_CEDENTE: TStringField;
    mdArqDAuxNOME_SACADOR: TStringField;
    mdArqDAuxNUM_DOC_SACADOR: TStringField;
    mdArqDAuxEND_SACADOR: TStringField;
    mdArqDAuxCEP_SACADOR: TStringField;
    mdArqDAuxCID_SACADOR: TStringField;
    mdArqDAuxUF_SACADOR: TStringField;
    mdArqDAuxNOSSO_NUMERO: TStringField;
    mdArqDAuxESPECIE: TStringField;
    mdArqDAuxNUM_TITULO: TStringField;
    mdArqDAuxDATA_EMISSAO: TStringField;
    mdArqDAuxDATA_VENCIMENTO: TStringField;
    mdArqDAuxTIPO_MOEDA: TStringField;
    mdArqDAuxVALOR_TITULO: TStringField;
    mdArqDAuxSALDO_TITULO: TStringField;
    mdArqDAuxPRACA_PROTESTO: TStringField;
    mdArqDAuxTIPO_ENDOSSO: TStringField;
    mdArqDAuxINFO_ACEITE: TStringField;
    mdArqDAuxNUM_CONTROLE_DEV: TStringField;
    mdArqDAuxNOME_DEVEDOR: TStringField;
    mdArqDAuxTIPO_ID_DEVEDOR: TStringField;
    mdArqDAuxNUM_ID_DEVEDOR: TStringField;
    mdArqDAuxNUM_DOC_DEVEDOR: TStringField;
    mdArqDAuxEND_DEVEDOR: TStringField;
    mdArqDAuxCEP_DEVEDOR: TStringField;
    mdArqDAuxCID_DEVEDOR: TStringField;
    mdArqDAuxUF_DEVEDOR: TStringField;
    mdArqDAuxCOD_CARTORIO: TStringField;
    mdArqDAuxNUM_PROTOCOLO_DIST: TStringField;
    mdArqDAuxTIPO_OCORRENCIA: TStringField;
    mdArqDAuxDATA_PROTOCOLO: TStringField;
    mdArqDAuxVALOR_CUSTAS_CART: TStringField;
    mdArqDAuxDECLARACAO_PORT: TStringField;
    mdArqDAuxDATA_OCORRENCIA: TStringField;
    mdArqDAuxCOD_IRREGULARIDADE: TStringField;
    mdArqDAuxBAIRRO_DEVEDOR: TStringField;
    mdArqDAuxVALOR_CUSTAS_DISTRIB: TStringField;
    mdArqDAuxREG_DISTRIBUICAO: TStringField;
    mdArqDAuxVALOR_GRAVACAO: TStringField;
    mdArqDAuxNUM_OPERACAO: TStringField;
    mdArqDAuxNUM_CONTRATO: TStringField;
    mdArqDAuxNUM_PARCELA: TStringField;
    mdArqDAuxTIPO_LETRA_CAMBIO: TStringField;
    mdArqDAuxCOMPL_COD_IRREG: TStringField;
    mdArqDAuxPROTESTO_FALENCIA: TStringField;
    mdArqDAuxINSTRUMENTO_PROT: TStringField;
    mdArqDAuxVALOR_DDESPESAS: TStringField;
    mdArqDAuxCOMPL_REGISTRO: TStringField;
    mdArqDAuxSEQUENCIAL_REG: TStringField;
    mdArqDAuxEMOLUMENTOS: TStringField;
    mdArqDAuxFETJ: TStringField;
    mdArqDAuxFUNDPERJ: TStringField;
    mdArqDAuxFUNPERJ: TStringField;
    mdArqDAuxFUNARPEN: TStringField;
    mdArqDAuxPMCMV: TStringField;
    mdArqDAuxISS: TStringField;
    mdArqDAuxMUTUA: TStringField;
    mdArqDAuxACOTERJ: TStringField;
    mdArqDAuxDISTRIBUICAO: TStringField;
    mdArqDAuxTOTAL_CUSTAS: TFloatField;
    mdArqDAuxPRIORIDADE_VALOR: TIntegerField;
    mdArqDAuxFLG_ALOCADO: TBooleanField;
    mdArqDAuxSEQ_BANCO: TIntegerField;
    mdArqDAuxID_ATO: TIntegerField;
    mdArqDAuxNOME_PORTADOR: TStringField;
    dsArqDAux: TDataSource;
    TitulosID_ATO: TIntegerField;
    TitulosCODIGO: TIntegerField;
    TitulosRECIBO: TIntegerField;
    TitulosDT_ENTRADA: TDateField;
    TitulosDT_PROTOCOLO: TDateField;
    TitulosPROTOCOLO: TIntegerField;
    TitulosLIVRO_PROTOCOLO: TIntegerField;
    TitulosFOLHA_PROTOCOLO: TStringField;
    TitulosDT_PRAZO: TDateField;
    TitulosDT_REGISTRO: TDateField;
    TitulosREGISTRO: TIntegerField;
    TitulosLIVRO_REGISTRO: TIntegerField;
    TitulosFOLHA_REGISTRO: TStringField;
    TitulosSELO_REGISTRO: TStringField;
    TitulosDT_PAGAMENTO: TDateField;
    TitulosSELO_PAGAMENTO: TStringField;
    TitulosRECIBO_PAGAMENTO: TIntegerField;
    TitulosCOBRANCA: TStringField;
    TitulosEMOLUMENTOS: TFloatField;
    TitulosFETJ: TFloatField;
    TitulosFUNDPERJ: TFloatField;
    TitulosFUNPERJ: TFloatField;
    TitulosFUNARPEN: TFloatField;
    TitulosPMCMV: TFloatField;
    TitulosISS: TFloatField;
    TitulosMUTUA: TFloatField;
    TitulosDISTRIBUICAO: TFloatField;
    TitulosACOTERJ: TFloatField;
    TitulosTOTAL: TFloatField;
    TitulosTIPO_PROTESTO: TIntegerField;
    TitulosTIPO_TITULO: TIntegerField;
    TitulosNUMERO_TITULO: TStringField;
    TitulosDT_TITULO: TDateField;
    TitulosBANCO: TStringField;
    TitulosAGENCIA: TStringField;
    TitulosCONTA: TStringField;
    TitulosVALOR_TITULO: TFloatField;
    TitulosSALDO_PROTESTO: TFloatField;
    TitulosDT_VENCIMENTO: TDateField;
    TitulosCONVENIO: TStringField;
    TitulosTIPO_APRESENTACAO: TStringField;
    TitulosDT_ENVIO: TDateField;
    TitulosCODIGO_APRESENTANTE: TStringField;
    TitulosTIPO_INTIMACAO: TStringField;
    TitulosMOTIVO_INTIMACAO: TMemoField;
    TitulosDT_INTIMACAO: TDateField;
    TitulosDT_PUBLICACAO: TDateField;
    TitulosVALOR_PAGAMENTO: TFloatField;
    TitulosAPRESENTANTE: TStringField;
    TitulosCPF_CNPJ_APRESENTANTE: TStringField;
    TitulosTIPO_APRESENTANTE: TStringField;
    TitulosCEDENTE: TStringField;
    TitulosNOSSO_NUMERO: TStringField;
    TitulosSACADOR: TStringField;
    TitulosDEVEDOR: TStringField;
    TitulosCPF_CNPJ_DEVEDOR: TStringField;
    TitulosTIPO_DEVEDOR: TStringField;
    TitulosAGENCIA_CEDENTE: TStringField;
    TitulosPRACA_PROTESTO: TStringField;
    TitulosTIPO_ENDOSSO: TStringField;
    TitulosACEITE: TStringField;
    TitulosCPF_ESCREVENTE: TStringField;
    TitulosCPF_ESCREVENTE_PG: TStringField;
    TitulosOBSERVACAO: TMemoField;
    TitulosDT_SUSTADO: TDateField;
    TitulosDT_RETIRADO: TDateField;
    TitulosSTATUS: TStringField;
    TitulosPROTESTADO: TStringField;
    TitulosENVIADO_APONTAMENTO: TStringField;
    TitulosENVIADO_PROTESTO: TStringField;
    TitulosENVIADO_PAGAMENTO: TStringField;
    TitulosENVIADO_RETIRADO: TStringField;
    TitulosENVIADO_SUSTADO: TStringField;
    TitulosENVIADO_DEVOLVIDO: TStringField;
    TitulosEXPORTADO: TStringField;
    TitulosAVALISTA_DEVEDOR: TStringField;
    TitulosFINS_FALIMENTARES: TStringField;
    TitulosTARIFA_BANCARIA: TFloatField;
    TitulosFORMA_PAGAMENTO: TStringField;
    TitulosNUMERO_PAGAMENTO: TStringField;
    TitulosARQUIVO: TStringField;
    TitulosRETORNO: TStringField;
    TitulosDT_DEVOLVIDO: TDateField;
    TitulosCPF_CNPJ_SACADOR: TStringField;
    TitulosID_MSG: TIntegerField;
    TitulosVALOR_AR: TFloatField;
    TitulosELETRONICO: TStringField;
    TitulosIRREGULARIDADE: TIntegerField;
    TitulosAVISTA: TStringField;
    TitulosSALDO_TITULO: TFloatField;
    TitulosTIPO_SUSTACAO: TStringField;
    TitulosRETORNO_PROTESTO: TStringField;
    TitulosDT_RETORNO_PROTESTO: TDateField;
    TitulosDT_DEFINITIVA: TDateField;
    TitulosJUDICIAL: TStringField;
    TitulosALEATORIO_PROTESTO: TStringField;
    TitulosALEATORIO_SOLUCAO: TStringField;
    TitulosCCT: TStringField;
    TitulosDETERMINACAO: TStringField;
    TitulosANTIGO: TStringField;
    TitulosLETRA: TStringField;
    TitulosPROTOCOLO_CARTORIO: TIntegerField;
    TitulosARQUIVO_CARTORIO: TStringField;
    TitulosRETORNO_CARTORIO: TStringField;
    TitulosDT_DEVOLVIDO_CARTORIO: TDateField;
    TitulosCARTORIO: TIntegerField;
    TitulosDT_PROTOCOLO_CARTORIO: TDateField;
    dsTitulos: TDataSource;
    TitulosDIAS_AVISTA: TIntegerField;
    mdLinhaArqDSOMAT_QTD_REMESSA: TStringField;
    mdLinhaArqDSOMAT_VAL_REMESSA: TStringField;
    mdLinhaArqDSERVENTIA_AGREGADA: TIntegerField;
    mdArquivoDARQUIVO_CRA: TStringField;
    PortadorAux: TRxMemoryData;
    PortadorAuxNOME: TStringField;
    PortadorAuxCODIGO: TStringField;
    dsPortadorAux: TDataSource;
    PortadorAuxAGENCIA: TStringField;
    PortadorAuxPRACA: TStringField;
    PortadorAuxSERVENTIA_AGREGADA: TIntegerField;
    TitulosFLG_SALDO: TStringField;
    PortadoresFLG_PAGAANTECIPADO: TStringField;
    RxPortadorFLG_PAGAANTECIPADO: TStringField;
    PortadorAuxFLG_PAGAANTECIPADO: TStringField;
    RxDevedorDOCUMENTO_FORMATADO: TStringField;
    mdArquivoDTOTAL_CUSTAS: TStringField;
    mdArquivoDNUM_TITULO: TStringField;
    TitulosPOSTECIPADO: TStringField;
    TitulosxDIA28: TBooleanField;
    mdArquivoDPOSTECIPADO: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure GerarID(Campo: String);
    procedure GerarValor(Sigla: String);
    procedure MudarValor(Sigla: String; Valor: Integer);
    procedure PortadoresBeforePost(DataSet: TDataSet);
    procedure EscreventesBeforePost(DataSet: TDataSet);
    procedure AtualizarParametro(ID_PARAMETRO: Integer; Valor: String);
    procedure MudarDescricaoParamentro(ID_PARAMETRO: Integer; Descricao: String);
    procedure TitulosCalcFields(DataSet: TDataSet);
    procedure CertidoesCalcFields(DataSet: TDataSet);
    procedure RxCustasBeforePost(DataSet: TDataSet);
    procedure ItensBeforePost(DataSet: TDataSet);
    procedure AtualizarRemessa(Data: TDate; Enviado: String; P,S,C: Integer);
    procedure ReciboMatricial(Id: Integer; Tipo: String);
    procedure Wrtln(T: String);
    procedure WriteLnEx(Const AMessage: String; AWidth: Integer);
    procedure CriarCampoDBX(Campo, Tabela, Tipo : String; Conexao : TSQLConnection);
    procedure AEMessage(var Msg: tagMSG; var Handled: Boolean);
    procedure VerificarConexoes;
    procedure CarregarObjetosAlpha(ComboBox: TsComboBox; Simple: TSimpleDataSet; FieldDescricao,FieldObjeto: String);
    procedure ConfigurarArquivoIni;
    procedure RxCustasFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure ApagarRXCustas(IdAto: Integer);
    procedure InserirTipoTitulo(Codigo,Descricao: String);
    procedure OrdenarRXDevedores;
    procedure RxDevedorBeforePost(DataSet: TDataSet);

    function PortadoresDuplicados(Imprimir: Boolean): Boolean;
    function IdAtual(Campo,Gerar: String): Integer;
    function IdAtualBancao(Campo: String): Integer;
    function ValorAtual(Sigla,Gerar: String): Integer;
    function ValorParametro(ID_PARAMETRO: Integer): String;
    function ControlarSelo: Boolean;
    function MontarTabelaConvenio(Codigo,IdAto: Integer; Valor: Double): Double;
    function DescricaoIrregularidade(Codigo: Integer): String;
    function MatriculaEscrevente(Nome: String): String;
    function CodigoAdicional(Valor: Double; Convenio: String): Integer;
    function SerieAtual: Integer;
    function CampoServentia(Campo: String): String;
    function CampoTitulos(Campo: String; IdAto: Integer): Variant;
    function ProximaOrdem(IdAto: Integer): Integer;
    function fIdAto(vProtocolo: String): Integer;
    function fProtocolo(vIdAto: String): String;
    function ForcaDaLei(Codigo: String): Boolean;
    function Especie(Codigo: String): Boolean;
    procedure ServentiaAgregadaVALOR_ULTIMO_SALDOGetText(Sender: TField;
      var Text: String; DisplayText: Boolean);
    procedure frptRelAfterPrint(Sender: TfrxReportComponent);
    procedure frptRelPreview(Sender: TObject);
    procedure qryVerbalEMOLUMENTOSGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryVerbalFETJGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryVerbalFUNDPERJGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryVerbalFUNPERJGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryVerbalFUNARPENGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qryVerbalTOTALGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure FeriadosCalcFields(DataSet: TDataSet);
    procedure qryRel01_ArqIECalcFields(DataSet: TDataSet);
    procedure TitulosEMOLUMENTOSGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure TitulosFETJGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure TitulosFUNDPERJGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure TitulosFUNPERJGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure TitulosFUNARPENGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure TitulosPMCMVGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure TitulosISSGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure TitulosMUTUAGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure TitulosDISTRIBUICAOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure TitulosACOTERJGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure TitulosTOTALGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure TitulosVALOR_TITULOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure TitulosSALDO_PROTESTOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure RxDevedorCalcFields(DataSet: TDataSet); // PEDRO - 20/04/2016
    function AtualizaCustasTitulo(pID_ATO:Integer):Double;
  private
    { Private declarations }

    F: TextFile;
  public
    { Public declarations }
    conSISTEMAAux,
    conCENTRALAux,
    conGERENCIALAux: TFDConnection;

    Q1, Q2: TFDQuery;
    vHora: TTime;

    vTipo                 :String; {SE A TELA DE REGISTRO EST� EM MODO DE INCLUS�O=I OU ALTERA��O=A / SE AS CUSTAS � DE CANCELAMENTO=C OU DESIST�NCIA=D}
    vMotivo               :String; {GUARDA O CAMPO MOTIVO DA INTIMA��O PARA CARREGAR NO FORM A PARTE}
    vObservacao           :String; {GUARDA O CAMPO OBSERVA��O PARA CARREGAR NO FORM A PARTE}
    vCPF                  :String;
    vNome                 :String;
    vNomeCompleto         :String;
    vAtualizacao          :String; {DATA DA ULTIMA ATUALIZACAO DO SISTEMA}
    vVersao               :String; {VERSAO DO SISTEMA}
    vVersaoTabela         :String; {VERSAO DA TABELA DA CORREGEDORIA}
    vStatus               :String; {QUAL STATUS FOI SELECIONADO}
    vConvenio             :String;
    vAssinatura           :String; {NOME DE QUEM VAI ASSINAR}
    vConferente           :String; {NOME DO CONFERENTE NA CERTID�O DE INTEIRO TEOR ADAPTADA PARA 1� RESENDE}
    vTitulo               :String; {T�TULO DE QUEM EST� ASSINANDO}
    vMatricula            :String; {MATR�CULA EMBAIXO DA ASSINATURA}
    vTipoEletronico       :String; {SE � PARA CONFIGURAR AS CUSTAS DO CANCELAMENTO/DESIST�NCIA ELETR�NICA OU CUSTAS DO BALC�O}
    vIrregularidade       :String;
    vAviso                :String;

    {USADO NA ETIQUETA DE RETIFICA��O}
    vDataProtocolo        :String;
    vProtocolo            :String;

    {USADO NA CERTID�O DO SERASA}
    vDataInicial          :String;
    vDataFinal            :String;
    vRecibo               :String;
    vDataCertidao         :String;
    vDataPedido           :String;
    vCobranca             :String;
    vFolhas               :String;
    vMarcados             :String;
    vCancelados           :String;
    vProtestados          :String;
    vDestino              :String;

    {PRIMEIRO DEVEDOR}
    vTipoDevedor          :String;
    vDoctDevedor          :String;
    vTeleDevedor          :String;
    vNomeDevedor          :String;

    {GUARDA AS INFORMA��ES DAS CUSTAS PARA CARREGAR EM FORM A PARTE}
    vEmolumentos          :Double;
    vFetj                 :Double;
    vFundperj             :Double;
    vFunperj              :Double;
    vFunarpen             :Double;
    vMutua                :Double;
    vAcoterj              :Double;
    vPmcmv                :Double;
    vIss                  :Double;
    vDistribuicao         :Double;
    vAAR                  :Double;
    vTarifa               :Double;
    vApontamento          :Double;
    vTotal                :Double;

    vIdReservado          :Integer;
    vIdAto                :Integer; {USADO NO FILTRO DO RXCUSTAS}

    {USADO PARA N�O CONTAR COMO REGISTRO QUANDO TIVER MAIS DE UM NOME NA EXPORTA��O DO SERASA/BOAVISTA}
    vDescontarMarcados    :Integer;
    vDescontarProtestados :Integer;
    vDescontarCancelados  :Integer;

    vCalc                 :Boolean;
    vOkGeral              :Boolean; {USO EM DIVERSAS TELAS - TOMAR CUIDADO PARA UMA TELA N�O ANULAR A OUTRA}
    vOkMotivo             :Boolean; {USADO NA TELA DE MOTIVOS E CADASTRO DE MOTIVO}
    vMudou                :Boolean; {SE O STATUS FOI ALTERADO OU N�O}
    vBuscar               :Boolean; {USADO NA TELA DE CERTID�O PARA SABER SE FA�O A BUSCA E PEGO A FOLHA AUTOM�TICA OU N�O}
    v2Via                 :Boolean; {USO PARA SABER SE EST� IMPRIMINDO A CERTID�O DE PAGAMENTO POR SEGUNDA VIA}
    vCancelamento         :Boolean;
    vReciboExcedente      :Boolean; {USADO PARA SABER SE ESTOU IMPRIMINDO RECIBO DE FOLHA EXCEDENTE OU RECIBO DO PEDIDO DA CERTID�O}
    vLivroConvenio        :Boolean; {USADO PARA SABER SE ESTOU IMPRIMINDO LIVRO DE CONV�NIO}
    vPergunta             :Boolean; {USADO PARA VERIRICAR PERGUNTA PEDRO - 04/04/2016}

    {CERTID�O - FOLHA DE SEGURAN�A - PEDRO - 28/03/2016}

    vFlTop                :String;
    vFlBottom             :String;
    vFlLeft               :String;
    vFlRight              :String;
    vFlFolha              :String;
    vFlTiracabecalho      :String;

    {FIM}

    { RELATORIOS FAST REPORT }
    dRelDataRelatorio: TDateTime;

    tRelHoraRelatorio: TTime;

    sRelNomeServentia,
    sRelTituloRelatorio,
    sRelSubtituloRelatorio,
    sRelNomeUsuario,
    sRelNomeArquivoRelatorio,
    sRelNomeArquivoPDF: String;

    lRelExibeDetalhe: Boolean;

    { DISTRIBUICAO E EXPORTACAO DE ARQUIVOS }
    iOrdem,
    iTotalTitulos,
    iTotalBancos: Integer;

    NomeArqCRA,
    NomeArqD,
    ExtArqCRA: String;

    cTotalEmolumentos: Double;

    procedure ImprimirRelatorio;
    procedure AtualizarFeriados;
    procedure CriarCampoFD(Campo, Tabela, Tipo: String; Conexao: TFDConnection);
    procedure DistribuirCartorios;
    procedure DMLInsertFD(Tabela, Campo, Valor: String; Conexao: TFDConnection);
    procedure DMLUpdateFD(Tabela, CampoValor: String; Conexao: TFDConnection; Condicao: String = '');
    procedure DMLDeleteFD(Tabela: String; Conexao: TFDConnection; Condicao: String = '');
    procedure DMLSelectFD(var QrySel: TFDQuery; Select, From: String;
                          Conexao: TFDConnection;
                          Where: String = ''; GroupBy: String = ''; OrderBy: String = '');
    procedure ExportarArquivoConfirmacao(NomeArqB: String; MemoData: TFDMemTable);
    procedure ExportarCartorios(var MsgRetorno: String; var Erro: Boolean; Reorganizar: Boolean; LstArqB: TListBox);
    procedure ReorganizarTitulosArquivo;
    procedure EnviarRemessasCartorios;

    procedure VerificarExistenciaPastasSistema;

    function PagamentoAntecipadoConvenio(CodPortador: String): Boolean;
    function ProximoDiaUtil(DataReferencia: TDate): TDate;
    function ProximoId(Campo, Tabela: String; Conexao: Integer; Gerar: Boolean): Integer;
    function EnviarEmail(NomeRemetente, EmailRemetente, NomeDestinatario,
                         EmailDestinatario, TituloEmail, MensagemEmail: String;
                         Anexo: Boolean;
                         LstAnexos: TListBox;
                         var MsgRetorno: String): Boolean;
  end;

var
  dm: Tdm;

implementation

uses UPF, UPrincipal, ULogin, Math,  UParametros, USplash, UGDM,
  StrUtils, UGeral, UVersaoSistema;

{$R *.dfm}

function Tdm.fIdAto(vProtocolo: String): Integer;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=conSISTEMA;
  Q.SQL.Add('SELECT ID_ATO FROM TITULOS WHERE PROTOCOLO='+vProtocolo);
  Q.Open;
  Result:=Q.FieldByName('ID_ATO').AsInteger;
  Q.Close;
  Q.Free;
end;

procedure Tdm.CriarCampoDBX(Campo,Tabela,Tipo: String; Conexao: TSQLConnection);
var
  SQL: String;
  Q: TSQLQuery;
begin
  try
    Q:=TSQLQuery.Create(Nil);
    Q.SQLConnection:=Conexao;
    Q.SQL.Clear;
    SQL:='select rdb$field_name from rdb$relation_fields where rdb$field_name= '''+Campo+
         ''' AND rdb$relation_name= '''+Tabela+'''';
    Q.SQL.Add(SQL);
    Q.Open;
    if Q.IsEmpty then
    begin
        Q.Close;
        Q.SQL.Clear;
        SQL:='ALTER TABLE '+Tabela+' ADD '+Campo+' '+Tipo;
        Q.SQL.Add(SQL);
        Q.ExecSQL(True);
    end;
    Q.Close;
    Q.Free;
  except
    Q.Close;
    Q.Free;
  end;
end;

procedure Tdm.CriarCampoFD(Campo, Tabela, Tipo: String; Conexao: TFDConnection);
var
  sSQL: String;
  QryC: TFDQuery;
  CmdC: TFDCommand;
  lVazio: Boolean;
begin
  try

    GR.CriarFDQuery(QryC,'',Conexao);

    QryC.Close;
    QryC.SQL.Clear;

    sSQL := 'SELECT RDB$FIELD_NAME ' +
            '  FROM RDB$RELATION_FIELDS ' +
            ' WHERE RDB$FIELD_NAME = ' + QuotedStr(Campo) +
            '   AND RDB$RELATION_NAME = ' + QuotedStr(Tabela);

    QryC.SQL.Text := sSQL;
    QryC.Open;

    lVazio := (QryC.RecordCount = 0);

    if lVazio then
    begin
      sSQL := '';

      sSQL := 'ALTER TABLE ' + Tabela + ' ADD ' + Campo + ' ' + Tipo;

      Conexao.ExecSQL(sSQL);
    end;

    FreeAndNil(QryC);
  except
    on E: Exception do
    begin
      FreeAndNil(QryC);

      GR.Aviso('Erro na Cria��o do Campo: ' + Campo + #13#10 + 'Mensagem: ' + E.Message);
    end;
  end;
end;

procedure Tdm.WriteLnEx(Const AMessage: String; AWidth: Integer);
begin
  if Trim(AMessage)='' then Exit;
  WriteLn(F,Copy(AMessage, 1, AWidth));
  WriteLnEx(Copy(AMessage, AWidth + 1, Length(AMessage)), AWidth);
end;

procedure Tdm.Wrtln(T: String);
begin
  Writeln(F,Copy(PF.RemoverAcento(T),1,55));
end;

procedure Tdm.ReciboMatricial(Id: Integer; Tipo: String);
var
  V: String;
  P: String;
  E: String;
  X: Integer;
  C: Integer;
  I: TIniFile;
begin
  I:=TIniFile.Create(ExtractFilePath(Application.ExeName)+'Par.ini');
  P:=I.ReadString('IMPRESSORAS','RECIBO','');
  E:=I.ReadString('ESPACOFINAL','RECIBO','');
  V:=I.ReadString('VIAS','RECIBO','');

  if V='' then
  I.WriteString('VIAS','RECIBO','1');
  V:=I.ReadString('VIAS','RECIBO','');
  for C := 1 to StrToInt(V) do
  begin
      AssignFile(F,P);
      Rewrite(F);
      Wrtln(dm.ServentiaDESCRICAO.AsString);
      Wrtln(dm.ServentiaENDERECO.AsString+' - RJ');
      Wrtln('CEP: '+dm.ServentiaCEP.AsString+' - Tel.: '+PF.FormatarTelefone(dm.ServentiaTELEFONE.AsString));
      Wrtln('CNPJ: '+PF.FormatarCNPJ(dm.ServentiaCNPJ.AsString));

      if Tipo='P' then
      begin
          Titulos.Close;
          Titulos.Params[0].AsInteger:=Id;
          Titulos.Open;
          PF.CarregarDevedor(dm.TitulosID_ATO.AsInteger);

          Wrtln('RECIBO: '+dm.ValorParametro(11));
          Wrtln('DATA DE ENTRADA: '+FormatDateTime('dd/mm/yyyy',dm.TitulosDT_PROTOCOLO.AsDateTime));
          Wrtln('PROTOCOLO: '+dm.TitulosPROTOCOLO.AsString);
          Wrtln('REQUERENTE: '+dm.TitulosAPRESENTANTE.AsString);
          Wrtln('DEVEDOR: '+dm.TitulosDEVEDOR.AsString);
          if dm.RxDevedorTIPO.AsString='J' then
            Wrtln('CNPJ: '+PF.FormatarCNPJ(dm.RxDevedorDOCUMENTO.AsString))
              else Wrtln('CPF: '+PF.FormatarCPF(dm.RxDevedorDOCUMENTO.AsString));
      end
      else
      begin                                         
          Certidoes.Close;
          Certidoes.Params[0].AsInteger:=Id;
          Certidoes.Open;
          Wrtln('RECIBO: '+dm.CertidoesRECIBO.AsString);
          Wrtln('DATA DO PEDIDO: '+FormatDateTime('dd/mm/yyyy',dm.CertidoesDT_PEDIDO.AsDateTime));
          Wrtln('DATA DE ENTREGA: '+FormatDateTime('dd/mm/yyyy',dm.CertidoesDT_ENTREGA.AsDateTime));
          Wrtln('REQUERENTE: '+dm.CertidoesREQUERENTE.AsString);
          Wrtln('REQUERIDO: '+dm.CertidoesREQUERIDO.AsString);
          if dm.CertidoesTIPO_REQUERIDO.AsString='J' then
            Wrtln('CNPJ: '+PF.FormatarCNPJ(dm.CertidoesCPF_CNPJ_REQUERIDO.AsString))
              else Wrtln('CPF: '+PF.FormatarCPF(dm.CertidoesCPF_CNPJ_REQUERIDO.AsString));
      end;

      Wrtln('FUNCIONARIO: '+dm.vNomeCompleto);
      Writeln(F,'------------------------------------------');
      if Tipo='P' then
        WriteLnEx('# '+PF.RetornarAdicional(dm.TitulosCODIGO.AsInteger),55)
          else
            if Tipo='N' then
              Writeln(F,'# CERTIDAO DE PROTESTO')
                else
                  if Tipo='X' then
                    Writeln(F,'# CANCELAMENTO (AVISO No 82/04)')
                      else
                        if Tipo='I' then
                          Writeln(F,'# CERTIDAO DE INTEIRO TEOR');
      Writeln(F,'------------------------------------------');
      if dm.ValorParametro(52)='S' then
        Writeln(F,'TABELA    DESCRICAO       VALOR    QTD.    TOTAL')
          else Writeln(F,'TABELA                    VALOR    QTD.    TOTAL');
      Writeln(F,'------------------------------------------');
      PF.CarregarCustas(Id);
      dm.RxCustas.First;
      while not dm.RxCustas.Eof do
      begin
          if dm.ValorParametro(52)='S' then
            Writeln(F,PF.Espaco(dm.RxCustasTABELA.AsString+'.'+dm.RxCustasITEM.AsString+'.'+dm.RxCustasSUBITEM.AsString,'E',10)+
                      PF.Espaco(Copy(PF.RemoverAcento(dm.RxCustasDESCRICAO.AsString),1,16),'E',12)+
                      PF.Espaco(FloatToStrF(dm.RxCustasVALOR.AsFloat,ffNumber,7,2),'D',7)+
                      PF.Espaco(dm.RxCustasQTD.AsString,'D',4)+
                      PF.Espaco(FloatToStrF(dm.RxCustasTOTAL.AsFloat,ffNumber,7,2),'D',11))
            else
              Writeln(F,PF.Espaco(dm.RxCustasTABELA.AsString+'.'+dm.RxCustasITEM.AsString+'.'+dm.RxCustasSUBITEM.AsString,'E',10)+
                        PF.Espaco(' ','E',10)+
                        PF.Espaco(FloatToStrF(dm.RxCustasVALOR.AsFloat,ffNumber,7,2),'D',7)+
                        PF.Espaco(dm.RxCustasQTD.AsString,'D',4)+
                        PF.Espaco(FloatToStrF(dm.RxCustasTOTAL.AsFloat,ffNumber,7,2),'D',11));
          dm.RxCustas.Next;
      end;
      Writeln(F,'------------------------------------------');
      if Tipo='P' then
      begin
          Wrtln('EMOLUMENTOS  '+PF.Espaco(FloatToStrF(dm.TitulosEMOLUMENTOS.AsFloat,ffNumber,7,2),'D',29));
          Wrtln('FETJ         '+PF.Espaco(FloatToStrF(dm.TitulosFETJ.AsFloat,ffNumber,7,2),'D',29));
          Wrtln('FUNDPERJ     '+PF.Espaco(FloatToStrF(dm.TitulosFUNDPERJ.AsFloat,ffNumber,7,2),'D',29));
          Wrtln('FUNPERJ      '+PF.Espaco(FloatToStrF(dm.TitulosFUNPERJ.AsFloat,ffNumber,7,2),'D',29));
          Wrtln('FUNARPEN     '+PF.Espaco(FloatToStrF(dm.TitulosFUNARPEN.AsFloat,ffNumber,7,2),'D',29));
          Wrtln('PMCMV        '+PF.Espaco(FloatToStrF(dm.TitulosPMCMV.AsFloat,ffNumber,7,2),'D',29));
          Wrtln('ISS          '+PF.Espaco(FloatToStrF(dm.TitulosISS.AsFloat,ffNumber,7,2),'D',29));

          if dm.TitulosVALOR_AR.AsFloat<>0 then
          Wrtln('A.R.         '+PF.Espaco(FloatToStrF(dm.TitulosVALOR_AR.AsFloat,ffNumber,7,2),'D',29));

          if dm.ServentiaCODIGO.AsInteger<>1500 then
          Wrtln('DISTRIBUICAO '+PF.Espaco(FloatToStrF(dm.TitulosDISTRIBUICAO.AsFloat,ffNumber,7,2),'D',29));
          Wrtln('MUTUA        '+PF.Espaco(FloatToStrF(dm.TitulosMUTUA.AsFloat,ffNumber,7,2),'D',29));
          Wrtln('ACOTERJ      '+PF.Espaco(FloatToStrF(dm.TitulosACOTERJ.AsFloat,ffNumber,7,2),'D',29));
          Writeln(F,'------------------------------------------');
          if dm.ServentiaCODIGO.AsInteger=1500 then
            Wrtln('TOTAL        '+PF.Espaco(FloatToStrF(dm.TitulosTOTAL.AsFloat-dm.TitulosDISTRIBUICAO.AsFloat,ffNumber,7,2),'D',29))
              else Wrtln('TOTAL        '+PF.Espaco(FloatToStrF(dm.TitulosTOTAL.AsFloat,ffNumber,7,2),'D',29));
      end
      else
      begin
          Wrtln('EMOLUMENTOS  '+PF.Espaco(FloatToStrF(dm.CertidoesEMOLUMENTOS.AsFloat,ffNumber,7,2),'D',29));
          Wrtln('FETJ         '+PF.Espaco(FloatToStrF(dm.CertidoesFETJ.AsFloat,ffNumber,7,2),'D',29));
          Wrtln('FUNDPERJ     '+PF.Espaco(FloatToStrF(dm.CertidoesFUNDPERJ.AsFloat,ffNumber,7,2),'D',29));
          Wrtln('FUNPERJ      '+PF.Espaco(FloatToStrF(dm.CertidoesFUNPERJ.AsFloat,ffNumber,7,2),'D',29));
          Wrtln('FUNARPEN     '+PF.Espaco(FloatToStrF(dm.CertidoesFUNARPEN.AsFloat,ffNumber,7,2),'D',29));
          Wrtln('PMCMV        '+PF.Espaco(FloatToStrF(dm.CertidoesPMCMV.AsFloat,ffNumber,7,2),'D',29));
          Wrtln('ISS          '+PF.Espaco(FloatToStrF(dm.CertidoesISS.AsFloat,ffNumber,7,2),'D',29));

          if dm.ServentiaCODIGO.AsInteger=1500 then
            Wrtln('DISTRIBUICAO '+PF.Espaco(FloatToStrF(dm.CertidoesDISTRIBUICAO.AsFloat,ffNumber,7,2),'D',29))
              else Wrtln('DISTRIBUICAO '+PF.Espaco(FloatToStrF(0,ffNumber,7,2),'D',29));
          Wrtln('MUTUA        '+PF.Espaco(FloatToStrF(dm.CertidoesMUTUA.AsFloat,ffNumber,7,2),'D',29));
          Wrtln('ACOTERJ      '+PF.Espaco(FloatToStrF(dm.CertidoesACOTERJ.AsFloat,ffNumber,7,2),'D',29));
          if dm.CertidoesAPONTAMENTO.AsFloat<>0 then
          Wrtln('APONTAMENTO  '+PF.Espaco(FloatToStrF(dm.CertidoesAPONTAMENTO.AsFloat,ffNumber,7,2),'D',29));
          Writeln(F,'------------------------------------------');
          if dm.ServentiaCODIGO.AsInteger=1500 then
            Wrtln('TOTAL        '+PF.Espaco(FloatToStrF(dm.CertidoesTOTAL.AsFloat-dm.CertidoesDISTRIBUICAO.AsFloat,ffNumber,7,2),'D',20))
              else Wrtln('TOTAL        '+PF.Espaco(FloatToStrF(dm.CertidoesTOTAL.AsFloat,ffNumber,7,2),'D',20));
      end;
      if Tipo='C' then
      begin
          Writeln(F,'******************************************');
          Writeln(F,'');
          Writeln(F,'');
          Writeln(F,'RECEBIDO:____/____/_______');
          Writeln(F,'');
          Writeln(F,'');
          Writeln(F,'ASS.:_____________________________________');
      end;

      for X:= 1 to StrToInt(E) do
      Writeln(F,' ');
      CloseFile(F);
  end;
  I.Free;
end;

procedure Tdm.ReorganizarTitulosArquivo;
var
  sValor: String;
  cSomatVlrSeg: Double;
  iTotGeral, iTotDev, iTotCmp12, iTotCmp12O, iSeqReg: Integer;
begin
  { Essa rotina gera os arquivos D que serao enviados para
    as Serventias Agregadas }

  mdLinhaArqD.Close;
  mdLinhaArqD.Open;

  sValor  := '';

  PortadorAux.Close;
  PortadorAux.Open;

  mdArquivoD.First;

  while not mdArquivoD.Eof do
  begin
    if not PortadorAux.Locate('CODIGO;NOME;SERVENTIA_AGREGADA', VarArrayOf([mdArquivoDNUM_COD_PORTADOR.AsString,
                                                                            mdArquivoDNOME_PORTADOR.AsString,
                                                                            mdArquivoDCOD_CARTORIO.AsInteger]), [loCaseInsensitive]) then
    begin
      Portadores.Close;
      Portadores.Open;

      if dm.Portadores.Locate('NOME', mdArquivoDNOME_PORTADOR.AsString, []) then
      begin
        if dm.Portadores.Locate('CODIGO', mdArquivoDNUM_COD_PORTADOR.AsString, []) then
        begin
          PortadorAux.Append;
          PortadorAuxCODIGO.AsString              := PortadoresCODIGO.AsString;
          PortadorAuxNOME.AsString                := PortadoresNOME.AsString;
          PortadorAuxAGENCIA.AsString             := PortadoresAGENCIA.AsString;
          PortadorAuxPRACA.AsString               := PortadoresPRACA.AsString;
          PortadorAuxSERVENTIA_AGREGADA.AsInteger := mdArquivoDCOD_CARTORIO.AsInteger;
          PortadorAuxSERVENTIA_AGREGADA.AsInteger := mdArquivoDCOD_CARTORIO.AsInteger;
          PortadorAux.Post;
        end
        else
        begin
          PortadorAux.Append;
          PortadorAuxCODIGO.AsString              := mdArquivoDNUM_COD_PORTADOR.AsString;
          PortadorAuxNOME.AsString                := mdArquivoDNOME_PORTADOR.AsString;
          PortadorAuxSERVENTIA_AGREGADA.AsInteger := mdArquivoDCOD_CARTORIO.AsInteger;
          PortadorAux.Post;
        end;
      end
      else
      begin
        PortadorAux.Append;
        PortadorAuxCODIGO.AsString              := mdArquivoDNUM_COD_PORTADOR.AsString;
        PortadorAuxNOME.AsString                := mdArquivoDNOME_PORTADOR.AsString;
        PortadorAuxSERVENTIA_AGREGADA.AsInteger := mdArquivoDCOD_CARTORIO.AsInteger;
        PortadorAux.Post;
      end;
    end;

    mdArquivoD.Next;
  end;

  sValor  := '';

  ServentiaAgregada.IndexFieldNames := 'CODIGO_DISTRIBUICAO';

  ServentiaAgregada.First;

  while not ServentiaAgregada.Eof do
  begin
    iSeqReg := 1;

    PortadorAux.SortOnFields('SERVENTIA_AGREGADA; CODIGO; NOME', True, True);

    PortadorAux.First;

    while not PortadorAux.Eof do
    begin
      if PortadorAuxSERVENTIA_AGREGADA.AsInteger = ServentiaAgregadaCODIGO_DISTRIBUICAO.AsInteger then
      begin
        iTotGeral  := 0;
        iTotDev    := 0;
        iTotCmp12  := 0;
        iTotCmp12O := 0;

        cSomatVlrSeg := 0;

        mdArquivoD.SortOnFields('COD_CARTORIO; APRESENTANTES; CODIGO_APRESENTANTE; NUM_PROTOCOLO_DIST; NUM_CONTROLE_DEV', True, True);

        mdArquivoD.First;

        while not mdArquivoD.Eof do
        begin
          if (mdArquivoDCOD_CARTORIO.AsInteger = ServentiaAgregadaCODIGO_DISTRIBUICAO.AsInteger) and
            (mdArquivoDNUM_COD_PORTADOR.AsString = PortadorAuxCODIGO.AsString) and
            (mdArquivoDNOME_PORTADOR.AsString = PortadorAuxNOME.AsString) then
          begin
            { TRANSACAO }
            Inc(iSeqReg);
            Inc(iTotGeral);

            cSomatVlrSeg := (cSomatVlrSeg + mdArquivoDSALDO_TITULO.AsFloat);

            mdArquivoD.Append;
            mdArquivoDID_REGISTRO.AsString          := '1';

            //Especie - Inicio
            Tipos.Close;
            Tipos.Open;

            if (Trim(TiposSIGLA.AsString) = 'DMI') or
              (Trim(TiposSIGLA.AsString) = 'DRI') or
              (Trim(TiposSIGLA.AsString) = 'CBI') then
              Inc(iTotCmp12)
            else
              Inc(iTotCmp12O);
            //Especie - Fim

            //Devedor - Inicio
            Devedores.Close;
            Devedores.Params.ParamByName('ID_ATO').AsInteger := mdArquivoDID_ATO.AsInteger;
            Devedores.Open;

            if Trim(DevedoresORDEM.AsString) = '1' then
              Inc(iTotDev);
            //Devedor - Fim

            mdArquivoDSEQUENCIAL_REG.AsString := PF.Espaco(IntToStr(iSeqReg), 'E', 4);
            mdArquivoD.Post;
          end;

          mdArquivoD.Next;
        end;

        if iTotGeral > 0 then
        begin
          { TRAILLER }
          Inc(iSeqReg);

          mdLinhaArqD.Append;
          mdLinhaArqDID_REGISTRO.AsInteger         := 9;

          if Trim(PortadorAuxCODIGO.AsString) = '' then
            mdLinhaArqDNUM_COD_PORTADOR.AsString   := GR.Zeros(' ', 'D', 3)
          else
            mdLinhaArqDNUM_COD_PORTADOR.AsString   := GR.Zeros(Trim(PortadorAuxCODIGO.AsString), 'D', 3);

          mdLinhaArqDNOME_PORTADOR.AsString       := PF.Espaco(PortadorAuxNOME.AsStrinG, 'E', 40);
          mdLinhaArqDDATA_MOVIMENTO.AsString      := GR.Zeros(GR.PegarNumeroTexto(DateToStr(Date)), 'D', 8);
          mdLinhaArqDCOMPL_REGISTRO.AsString      := PF.Espaco(' ', 'E', 521);
          mdLinhaArqDSOMAT_QTD_REMESSA.AsString   := GR.Zeros(IntToStr(iTotGeral +
                                                                       iTotDev +
                                                                       iTotCmp12 +
                                                                       iTotCmp12O), 'D', 5);

          sValor := FloatToStrF(GR.NoRound(cSomatVlrSeg, 2), ffFixed, 16, 2);
          mdLinhaArqDSOMAT_VAL_REMESSA.AsString   := GR.Zeros(GR.PegarNumeroTexto(sValor), 'D', 18);

          mdLinhaArqDNUM_SEQ_REGISTRO.AsString    := PF.Espaco(IntToStr(iSeqReg), 'E', 4);
          mdLinhaArqDSERVENTIA_AGREGADA.AsInteger := dm.ServentiaAgregadaCODIGO_DISTRIBUICAO.AsInteger;
          mdLinhaArqD.Post;

          { HEADER }
          mdLinhaArqD.Append;
          mdLinhaArqDID_REGISTRO.AsInteger        := 0;

          if Trim(PortadorAuxCODIGO.AsString) = '' then
            mdLinhaArqDNUM_COD_PORTADOR.AsString  := GR.Zeros('', 'D', 3)
          else
            mdLinhaArqDNUM_COD_PORTADOR.AsString  := GR.Zeros(PortadorAuxCODIGO.AsString, 'D', 3);

          mdLinhaArqDNOME_PORTADOR.AsString       := PF.Espaco(PortadorAuxNOME.AsString, 'E', 40);

          if Trim(PortadorAuxAGENCIA.AsString) = '' then
            mdLinhaArqDID_AGENCIA.AsString        := GR.Zeros('0', 'D', 6)
          else
            mdLinhaArqDID_AGENCIA.AsString        := GR.Zeros(PortadorAuxAGENCIA.AsString, 'D', 6);

          if Trim(PortadorAuxPRACA.AsString) = '' then
            mdLinhaArqDCOD_MUNIC_PRACA.AsString   := GR.Zeros('0', 'D', 7)
          else
            mdLinhaArqDCOD_MUNIC_PRACA.AsString   := GR.Zeros(PortadorAuxPRACA.AsString, 'D', 7);

          dm.mdLinhaArqDDATA_MOVIMENTO.AsString      := GR.Zeros(GR.PegarNumeroTexto(DateToStr(Date)), 'D', 8);
          dm.mdLinhaArqDID_TRANS_REMET.AsString      := 'BFO';
          dm.mdLinhaArqDID_TRANS_DESTIN.AsString     := 'SDT';
          dm.mdLinhaArqDID_TRANS_TIPO.AsString       := 'TPR';
          dm.mdLinhaArqDNUM_SEQ_REMESSA.AsString     := '0';  //Esse valor sera atualizado na geracao do arquivo D
          dm.mdLinhaArqDQTD_REG_REMESSA.AsString     := IntToStr(iTotGeral);
          dm.mdLinhaArqDQTD_TIT_REMESSA.AsString     := IntToStr(iTotDev);
          dm.mdLinhaArqDQTD_IND_REMESSA.AsString     := IntToStr(iTotCmp12);
          dm.mdLinhaArqDQTD_ORIG_REMESSA.AsString    := IntToStr(iTotCmp12O);
          dm.mdLinhaArqDVERSAO_LAYOUT.AsString       := '043';
          dm.mdLinhaArqDCOMPL_REGISTRO.AsString      := PF.Espaco(' ', 'E', 497);
          dm.mdLinhaArqDNUM_SEQ_REGISTRO.AsString    := PF.Espaco(IntToStr(iSeqReg - iTotGeral - 1), 'E', 4);
          dm.mdLinhaArqDSERVENTIA_AGREGADA.AsInteger := ServentiaAgregadaCODIGO_DISTRIBUICAO.AsInteger;
          dm.mdLinhaArqD.Post;

          Inc(iSeqReg);
        end;
      end;

      PortadorAux.Next;
    end;

    //Atualiza Serventia
    ServentiaAgregada.Edit;

    if iSeqReg > 1 then
      ServentiaAgregadaQTD_MAX_PREV.AsInteger := (iSeqReg - 1)
    else
      ServentiaAgregadaQTD_MAX_PREV.AsInteger := 0;

    ServentiaAgregada.Post;

    ServentiaAgregada.Next;
  end;
end;

procedure Tdm.AtualizarRemessa(Data: TDate; Enviado: String; P,S,C: Integer);
begin
  dm.Diaria.Close;
  dm.Diaria.Params[0].AsDate:=Data;
  dm.Diaria.Open;
  if dm.Diaria.IsEmpty then
  begin
      dm.Diaria.Append;
      dm.DiariaID_ONLINE.AsInteger:=dm.IdAtual('ID_ONLINE','S');
  end
  else
    dm.Diaria.Edit;
  dm.DiariaDATA.AsDateTime      :=Data;
  dm.DiariaPROTESTOS.AsInteger  :=P;
  dm.DiariaSUSTADOS.AsInteger   :=S;
  dm.DiariaCANCELADOS.AsInteger :=C;
  dm.DiariaENVIADO.AsString     :=Enviado;
  dm.Diaria.Post;
  dm.Diaria.ApplyUpdates(0);
end;

procedure Tdm.DataModuleCreate(Sender: TObject);
var
  I: TIniFile;
  sBanco, sSrv, sDB, sMsg: String;
begin
  sSrv   := '';
  sDB    := '';
  sBanco := '';
  sMsg   := '';

  GR.ChecarDLLs(True,False,True,False,False,False,False,True,True,False,False,False,False,False,'');
  VerificarConexoes;

  if not FileExists(GR.DirExe + 'Config.ini') then
    ConfigurarArquivoIni;

  I := TIniFile.Create(GR.DirExe + 'Config.ini');

  {1} FSplash.Caption := 'Conectando Central Protesto...';
  try
    sBanco := I.ReadString('DATABASE', 'PROTESTO', '*');

    //FIREDAC - SISTEMA
    //Gdm.RetornarServidor(sBanco, sSrv, sDB);

    conSISTEMA.Close;

    conSISTEMA.Params.Values['Database']     := GR.PathBD(sBanco);
    conSISTEMA.Params.Values['Server']       := GR.ServerBD(sBanco);  //localhost OU o nome do servidor OU o IP do servidor
    conSISTEMA.Params.Values['Protocol']     := 'Remote';  //Local ou Remote
    conSISTEMA.Params.Values['SQLDialect']   := '3';
    conSISTEMA.Params.Values['CharacterSet'] := 'ISO8859_1';

    conSISTEMA.Connected := True;

    conSISTEMAAux := conSISTEMA;
  except
    on E: Exception do
    begin
      GR.Aviso('ERRO: N�O FOI POSS�VEL CONECTAR NO BANCO DE CENTRAL PROTESTO!' + #13 + 'MSG: ' + E.Message);
      Application.Terminate;
    end;
  end;

  {1} FSplash.G.AddProgress(1);
  {2} FSplash.Caption:='Conectando Central...';

  sSrv   := '';
  sDB    := '';

  try
    sBanco := I.ReadString('DATABASE','CENTRAL','*');

    //FIREDAC - CENTRAL
    //Gdm.RetornarServidor(sBanco, sSrv, sDB);

    conCENTRAL.Close;

    conCENTRAL.Params.Values['Database']     := GR.PathBD(sBanco);
    conCENTRAL.Params.Values['Server']       := GR.ServerBD(sBanco);    //localhost OU o nome do servidor OU o IP do servidor
    conCENTRAL.Params.Values['Protocol']     := 'Remote';  //Local ou Remote
    conCENTRAL.Params.Values['SQLDialect']   := '3';
    conCENTRAL.Params.Values['CharacterSet'] := 'ISO8859_1';

    conCENTRAL.Connected := True;

    conCENTRALAux := conCENTRAL;
  except
    on E: Exception do
    begin
      GR.Aviso('ERRO: N�O FOI POSS�VEL CONECTAR NO BANCO CENTRAL!'+#13+'MSG: '+E.Message);
      Application.Terminate;
    end;
  end;

  if I.ReadString('DATABASE','GERENCIAL','') = '' then
  begin
    I.Free;
    GR.Aviso('N�O FOI POSS�VEL CONECTAR COM O GERENCIAL!'+#13#13+'O ARQUIVO DE CONFIGURA��O SER� ABERTO!!');
    ShellExecute(Application.Handle,'Open',PChar(GR.DirExe+'Config.ini'),Nil,Nil,SW_SHOWMAXIMIZED);
    Application.Terminate;
  end
  else
  begin
    I.Free;
    Application.CreateForm(TGdm, Gdm);

    //FIREDAC - GERENCIAL
    conGERENCIALAux := Gdm.FDGERENCIAL;

    //Verifica se existe atualizacao disponivel
    {Gdm.AtualizaExecutavelSistema('www\cartorios\StatusTotal\CentralProtesto',
                                  'CentralProtesto.exe',
                                  'CentralProtesto-update.exe',
                                  4,
                                  dm.conSISTEMA);  }
  end;

  {2} FSplash.G.AddProgress(1);

  try
    Application.CreateForm(TFPrincipal,FPrincipal);
    {38} FSplash.Caption:='Iniciando Login...';
    {38} FSplash.G.AddProgress(1);
    GR.CriarForm(TFLogin,FLogin);
    PF.Aguarde(False);
  except
    on E:Exception do
    begin
      GR.Aviso(E.Message);
      Application.Terminate;
    end;
  end;

  VS.VerificarVersaoSistema(sMsg);

  if Trim(sMsg) <> '' then
    ShowMessage(PChar(sMsg));

  VerificarExistenciaPastasSistema;
end;

procedure Tdm.GerarID(Campo: String);
begin
  Randomize;

  if conSISTEMA.Connected then
    conSISTEMA.StartTransaction;

  try
    Ids.Close;
    Ids.Params[0].AsString := Campo;
    Ids.Open;

    Ids.Edit;

    IdsVALOR.AsInteger := IdsVALOR.AsInteger + 1;

    Ids.Post;
    Ids.ApplyUpdates(0);

    if conSISTEMA.InTransaction then
      conSISTEMA.Commit;
  except
    on E:Exception do
    begin
      if conSISTEMA.InTransaction then
        conSISTEMA.Rollback;
      
      GR.Aviso('Erro ao gerar id!' + #13#13 + 'Mensagem: ' + E.Message);
    end;
  end;
end;

function Tdm.IdAtual(Campo, Gerar: String): Integer;
var
  QryIds, QryIdsAux: TFDQuery;
  Id: Integer;
begin
  Id := 0;

  //QryIds    := Gdm.CriarFDQuery(nil, conSISTEMAAux);
  //QryIdsAux := Gdm.CriarFDQuery(nil, conSISTEMAAux);

  GR.CriarFDQuery(QryIds,'', conSISTEMAAux);
  GR.CriarFDQuery(QryIdsAux,'', conSISTEMAAux);



  QryIds.Close;
  QryIds.SQL.Clear;

  QryIds.SQL.Text := 'SELECT * ' +
                     '  FROM IDS ' +
                     ' WHERE CAMPO = :CAMPO';

  QryIds.Params.ParamByName('CAMPO').Value := Campo;
  QryIds.Open;

  if not QryIds.FieldByName('VALOR').IsNull then
    Id := QryIds.FieldByName('VALOR').AsInteger;

  if Gerar = 'S' then
  begin
    try
      if conSISTEMAAux.Connected then
        conSISTEMAAux.StartTransaction;

      Id := Id + 1;

      QryIdsAux.Close;
      QryIdsAux.SQL.Clear;
      QryIdsAux.SQL.Text := 'UPDATE IDS ' +
                            '   SET VALOR = :VALOR ' +
                            ' WHERE CAMPO = :CAMPO';
      QryIdsAux.Params.ParamByName('VALOR').Value := Id;
      QryIdsAux.Params.ParamByName('CAMPO').Value := Campo;
      QryIdsAux.ExecSQL;

      if conSISTEMAAux.InTransaction then
        conSISTEMAAux.Commit;
    except
      on E: Exception do
      begin
        if conSISTEMAAux.InTransaction then
          conSISTEMAAux.Rollback;

        Id := Id - 1;

        GR.Aviso('Erro ao gerar id!' + #13#13 + 'Mensagem: ' + E.Message);
      end;
    end;
  end;

  Result := Id;

  FreeAndNil(QryIds);
  FreeAndNil(QryIdsAux);
end;

function Tdm.IdAtualBancao(Campo: String): Integer;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=conCENTRAL;
  Q.SQL.Add('SELECT GEN_ID('+Campo+',1) AS ID_ATUAL FROM RDB$DATABASE');
  Q.Open;
  Result:=Q.FieldByName('ID_ATUAL').AsInteger;
  Q.Close;
  Q.Free;
end;

procedure Tdm.ImprimirRelatorio;
begin
  frxDsgn.TemplateDir := dm.ValorParametro(101) + 'Modelos';

  frptRel.Clear;
  frptRel.Variables.Clear;
  frptRel.ParentReport := dm.ValorParametro(101) + 'Modelos\frRelatorio_Base.fr3';
  frptRel.LoadFromFile(dm.ValorParametro(101) + 'Modelos\' + sRelNomeArquivoRelatorio);
  frptRel.PrepareReport(True);
  frptRel.ShowReport(True);
end;

function Tdm.PagamentoAntecipadoConvenio(CodPortador: String): Boolean;
var
  qryPort: TFDQuery;
begin
  GR.CriarFDQuery(qryPort,'',conSISTEMAAux);


  qryPort.Close;
  qryPort.SQL.Clear;
  qryPort.SQL.Text := 'SELECT FLG_PAGAANTECIPADO ' +
                      '  FROM PORTADORES ' +
                      ' WHERE CODIGO = :CODIGO ' +
                      '   AND CONVENIO = ' + QuotedStr('S');
  qryPort.Params.ParamByName('CODIGO').Value := Trim(CodPortador);
  qryPort.Open;

  Result := (Trim(qryPort.FieldByName('FLG_PAGAANTECIPADO').AsString) = 'S');

  FreeAndNil(qryPort);
end;

procedure Tdm.PortadoresBeforePost(DataSet: TDataSet);
begin
  if Portadores.State=dsInsert  then PortadoresID_PORTADOR.AsInteger:=dm.IdAtual('ID_PORTADOR','S');
  if PortadoresBANCO.IsNull     then PortadoresBANCO.AsString:='N';
  if PortadoresCRA.IsNull       then PortadoresCRA.AsString:='N';
  if PortadoresCONVENIO.IsNull  then PortadoresCONVENIO.AsString:='N';
  if PortadoresNOMINAL.IsNull   then PortadoresNOMINAL.AsString:='N';
end;

function Tdm.ValorAtual(Sigla,Gerar: String): Integer;
begin
  Controle.Close;
  Controle.Params[0].AsString:=Sigla;
  Controle.Open;
  Result:=ControleVALOR.AsInteger;
  Controle.Close;
  if Gerar='S' then GerarValor(Sigla);
end;

procedure Tdm.GerarValor(Sigla: String);
begin
  Controle.Close;
  Controle.Params[0].AsString:=Sigla;
  Controle.Open;
  Controle.Edit;
  ControleVALOR.AsInteger:=ControleVALOR.AsInteger+1;
  Controle.Post;
  Controle.ApplyUpdates(0);
  Controle.Close;
end;

function Tdm.EnviarEmail(NomeRemetente, EmailRemetente, NomeDestinatario,
  EmailDestinatario, TituloEmail, MensagemEmail: String; Anexo: Boolean;
  LstAnexos: TListBox; var MsgRetorno: String): Boolean;
begin
{  Result := True;

  try
    //
  except
    on E: exception do
    begin
      MsgRetorno := E.Message;
      Result := False;
    end;
  end;  }
end;

procedure Tdm.EnviarRemessasCartorios;
var
  LstAnx: TListBox;
  sMsg, sTitEmail, sMsgRet, sMsgRetorno: String;
begin
{  if Application.MessageBox('Deseja enviar agora os arquivos D para os respectivos e-mails cadastrados das Serventias Agregadas?',
                            'Confirma��o',
                            MB_YESNO + MB_ICONQUESTION) = ID_YES then
  begin
    sMsgRetorno := '';
    sMsgRet     := '';

    sTitEmail := 'Remessa de arquivo D - ' + DateToStr(Date);

    sMsg := 'Prezado,' + #13#10 + #13#10 +
            'Segue anexada ao e-mail a remessa de T�tulos referente ao dia ' + DateToStr(Date) + '.' +
            #13#10 + #13#10 +
            'Saldo de T�tulos at� a data presente: ' + #13#10 +
            'Quantidade: ' + IfThen(ServentiaAgregada.Fields.FieldByName('QTD_ULTIMO_SALDO').IsNull,
                             '0',
                             FloatToStr(ServentiaAgregada.Fields.FieldByName('QTD_ULTIMO_SALDO').AsInteger)) +
            #13#10 +
            'Valor: ' + IfThen(ServentiaAgregada.Fields.FieldByName('VALOR_ULTIMO_SALDO').IsNull,
                               '0',
                               FloatToStr(ServentiaAgregada.Fields.FieldByName('VALOR_ULTIMO_SALDO').AsFloat)) +
            #13#10 + #13#10 +
            'Caso tenha qualquer d�vida, por favor, entre em contato.';

    ServentiaAgregada.Close;
    ServentiaAgregada.Open;

    ServentiaAgregada.First;

    while not ServentiaAgregada.Eof do
    begin
      if (Trim(ServentiaAgregada.Fields.FieldByName('EMAIL1').AsString) <> '') or
        (Trim(ServentiaAgregada.Fields.FieldByName('EMAIL2').AsString) <> '') then
      begin
        if EnviarEmail(Gdm.cUsuariosNOME.AsString,
                       Trim(Serventia.Fields.FieldByName('EMAIL').AsString),
                       ServentiaAgregada.Fields.FieldByName('DESCRICAO').AsString,
                       IfThen((Trim(ServentiaAgregada.Fields.FieldByName('EMAIL2').AsString) <> ''),
                              Trim(ServentiaAgregada.Fields.FieldByName('EMAIL2').AsString),
                              Trim(ServentiaAgregada.Fields.FieldByName('EMAIL1').AsString)),
                       sTitEmail,
                       sMsg,
                       True,
                       LstAnx,
                       sMsgRet) then
        begin
          if Trim(sMsgRetorno) = '' then
            sMsgRetorno := '* E-mail para ' +
                           Trim(ServentiaAgregada.Fields.FieldByName('DESCRICAO').AsString) +
                           ' com sucesso!'
          else
            sMsgRetorno := sMsgRetorno + #13#10 +
                           '* E-mail para ' +
                           Trim(ServentiaAgregada.Fields.FieldByName('DESCRICAO').AsString) +
                           ' com sucesso!';
        end
        else
        begin
          if Trim(sMsgRetorno) = '' then
            sMsgRetorno := '* Erro ao enviar e-mail para ' +
                           Trim(ServentiaAgregada.Fields.FieldByName('DESCRICAO').AsString) + #13#10 +
                           '(' + sMsgRet + ')' + #13#10 +
                           'Por favor, envie esse e-mail manualmente.'
          else
            sMsgRetorno := sMsgRetorno + #13#10 +
                           '* Erro ao enviar e-mail para ' +
                           Trim(ServentiaAgregada.Fields.FieldByName('DESCRICAO').AsString) + #13#10 +
                           '(' + sMsgRet + ')' + #13#10 +
                           'Por favor, envie esse e-mail manualmente.';
        end;
      end
      else
      begin
        if Trim(sMsgRetorno) = '' then
          sMsgRetorno := '* A Serventia ' + ServentiaAgregada.Fields.FieldByName('DESCRICAO').AsString +
                         ' n�o possui e-mails cadastrados. ser� preciso enviar o arquivo manualmente.'
        else
          sMsgRetorno := sMsgRetorno + #13#10 +
                         '* A Serventia ' + ServentiaAgregada.Fields.FieldByName('DESCRICAO').AsString +
                         ' n�o possui e-mails cadastrados. ser� preciso enviar o arquivo manualmente.';
      end;

      ServentiaAgregada.Next;
    end;

    Application.MessageBox(PChar(sMsgRetorno), 'Envio de e-mails para Serventias Agregadas', MB_OK);
  end;  }
end;

procedure Tdm.EscreventesBeforePost(DataSet: TDataSet);
begin
  if Escreventes.State=dsInsert then EscreventesID_ESCREVENTE.AsInteger:=dm.IdAtual('ID_ESCREVENTE','S');
end;

function Tdm.ValorParametro(ID_PARAMETRO: Integer): String;
begin
  Parametros.Close;
  Parametros.Params[0].AsInteger:=ID_PARAMETRO;
  Parametros.Open;
  Result:=ParametrosVALOR.AsString;
  Parametros.Close;
end;

function Tdm.AtualizaCustasTitulo(pID_ATO: Integer): Double;
var
  Q,Q1:TFDQuery;
  vEmolumentos:Double;
begin

  GR.ExecutarSQLDAC('DELETE FROM CUSTAS WHERE ID_ATO='+pID_ATO.ToString,conSISTEMA);

  Q:=TFDQuery.Create(Nil);
  Q.Connection:=conSISTEMA;

  Q1:=TFDQuery.Create(Nil);
  Q1.Connection:=conSISTEMA;

  Q.SQL.ADD('SELECT ID_ATO,CONVENIO,SALDO_TITULO,SALDO_PROTESTO,'+
                         'EMOLUMENTOS,FETJ,FUNDPERJ,FUNPERJ,FUNARPEN,PMCMV,ISS,VALOR_AR,MUTUA,ACOTERJ,DISTRIBUICAO,TARIFA_BANCARIA,TOTAL '+
                         'FROM TITULOS WHERE ID_ATO='+pID_ATO.ToString);
  Q.Open;

  Q1.SQL.ADD('SELECT * FROM CUSTAS WHERE ID_CUSTA=-1');
  Q1.Open;

  qryFaixa.Close;
  qryFaixa.ParamByName('C').AsString       :=Q.FieldByName('CONVENIO').AsString;
  qryFaixa.ParamByName('OCULTO1').AsString  :='N';
  qryFaixa.ParamByName('VALOR1').AsFloat    :=Q.FieldByName('SALDO_TITULO').AsFloat;
  qryFaixa.ParamByName('VALOR2').AsFloat    :=Q.FieldByName('SALDO_TITULO').AsFloat;
  qryFaixa.Open;

  Itens.Close;
  Itens.Params[0].AsInteger:=qryFaixaCOD.AsInteger;
  Itens.Open;
  Itens.First;

  dm.vEmolumentos:=0;
  while not Itens.Eof do
  begin
      Q1.Append;
      Q1.FieldByName('ID_CUSTA').AsInteger :=dm.IdAtual('ID_CUSTA','S');
      Q1.FieldByName('ID_ATO').AsInteger   :=pID_ATO;
      Q1.FieldByName('TABELA').AsString    :=dm.ItensTAB.AsString;
      Q1.FieldByName('ITEM').AsString      :=dm.ItensITEM.AsString;
      Q1.FieldByName('SUBITEM').AsString   :=dm.ItensSUB.AsString;
      Q1.FieldByName('VALOR').AsFloat      :=dm.ItensVALOR.AsFloat;
      Q1.FieldByName('QTD').AsString       :=dm.ItensQTD.AsString;
      Q1.FieldByName('DESCRICAO').AsString :=dm.ItensDESCR.AsString;
      Q1.FieldByName('TOTAL').AsFloat      :=Q1.FieldByName('VALOR').AsFloat*Q1.FieldByName('QTD').AsFloat;
      Q1.Post;
      vEmolumentos:=dm.vEmolumentos+Q1.FieldByName('TOTAL').AsFloat;
      Itens.Next;
  end;

  Q.Edit;

  Q.FieldByName('EMOLUMENTOS').AsFloat :=vEmolumentos;
  Q.FieldByName('FETJ').AsFloat        :=GR.NoRound(vEmolumentos*0.2,2);
  Q.FieldByName('FUNDPERJ').AsFloat    :=GR.NoRound(vEmolumentos*0.05,2);
  Q.FieldByName('FUNPERJ').AsFloat     :=GR.NoRound(vEmolumentos*0.05,2);
  Q.FieldByName('FUNARPEN').AsFloat    :=GR.NoRound(vEmolumentos*0.04,2);
  Q.FieldByName('PMCMV').AsFloat       :=GR.NoRound(vEmolumentos*0.02,2);

  if not Gdm.vIssPmcmv then
    Q.FieldByName('ISS').AsFloat         :=GR.NoRound(vEmolumentos*Gdm.vAliquotaISS,2)
      else
        Q.FieldByName('ISS').AsFloat         :=GR.NoRound((vEmolumentos+Q.FieldByName('PMCMV').AsFloat)*Gdm.vAliquotaISS,2);

  Q.FieldByName('MUTUA').AsFloat       :=dm.QryFaixaMUTUA.AsFloat;//StrToFloat(dm.ValorParametro(26));
  Q.FieldByName('ACOTERJ').AsFloat     :=dm.QryFaixaACOTERJ.AsFloat;//StrToFloat(dm.ValorParametro(27));
  Q.FieldByName('DISTRIBUICAO').AsFloat:=dm.QryFaixaDISTRIB.AsFloat;//StrToFloat(dm.ValorParametro(28));
  Q.FieldByName('TOTAL').AsFloat       :=Q.FieldByName('EMOLUMENTOS').AsFloat+
                                         Q.FieldByName('FETJ').AsFloat+
                                         Q.FieldByName('FUNDPERJ').AsFloat+
                                         Q.FieldByName('FUNPERJ').AsFloat+
                                         Q.FieldByName('FUNARPEN').AsFloat+
                                         Q.FieldByName('PMCMV').AsFloat+
                                         Q.FieldByName('ISS').AsFloat+
                                         Q.FieldByName('MUTUA').AsFloat+
                                         Q.FieldByName('ACOTERJ').AsFloat+
                                         Q.FieldByName('DISTRIBUICAO').AsFloat+
                                         Q.FieldByName('VALOR_AR').AsFloat+
                                         Q.FieldByName('TARIFA_BANCARIA').AsFloat;

  Q.FieldByName('SALDO_PROTESTO').AsFloat:=Q.FieldByName('SALDO_TITULO').AsFloat+Q.FieldByName('TOTAL').AsFloat;
  Q.Post;

  FreeAndNil(Q);
  FreeAndNil(Q1);

end;

procedure Tdm.AtualizarFeriados;
var
  QryF, QryFAux: TFDQuery;
  iAno: Integer;
begin
  iAno := 0;

  GR.CriarFDQuery(QryF,'',conSISTEMA);
  GR.CriarFDQuery(QryFAux,'', conSISTEMAAux);

  QryFAux.Close;
  QryFAux.SQL.Clear;
  QryFAux.SQL.Text := 'SELECT MAX(ANO) AS MAX_ANO ' +
                      '  FROM FERIADOS';
  QryFAux.Open;

  if QryFAux.RecordCount = 0 then
    Exit;

  iAno := QryFAux.FieldByName('MAX_ANO').AsInteger;

  if iAno < YearOf(Date) then
  begin
    QryFAux.Close;
    QryFAux.SQL.Clear;
    QryFAux.SQL.Text := 'SELECT * ' +
                        '  FROM FERIADOS ' +
                        ' WHERE ANO = ' + IntToStr(YearOf(Date) - 1) +
                        '   AND FLG_FIXO = ' + QuotedStr('S') ;
    QryFAux.Open;

    QryFAux.First;

    while not QryFAux.Eof do
    begin
      QryF.Close;
      QryF.SQL.Clear;

      QryF.SQL.Text := 'INSERT INTO FERIADOS (ID_FERIADO, DIA, MES, ANO, ' +
                       '                      DESCRICAO, ATIVO, FLG_FIXO) ' +
                       '              VALUES (:ID_FERIADO, :DIA, :MES, :ANO, ' +
                       '                      :DESCRICAO, :ATIVO, :FLG_FIXO)' ;

      QryF.Params.ParamByName('ID_FERIADO').Value := dm.IdAtual('ID_FERIADO', 'S');
      QryF.Params.ParamByName('DIA').Value        := QryFAux.FieldByName('DIA').AsInteger;
      QryF.Params.ParamByName('MES').Value        := QryFAux.FieldByName('MES').AsInteger;
      QryF.Params.ParamByName('ANO').Value        := YearOf(Date);
      QryF.Params.ParamByName('DESCRICAO').Value  := QryFAux.FieldByName('DESCRICAO').AsString;
      QryF.Params.ParamByName('ATIVO').Value      := QryFAux.FieldByName('ATIVO').AsString;
      QryF.Params.ParamByName('FLG_FIXO').Value   := QryFAux.FieldByName('FLG_FIXO').AsString;
      QryF.ExecSQL;

      QryFAux.Next;
    end;
  end;

  FreeAndNil(QryF);
  FreeAndNil(QryFAux);
end;

procedure Tdm.AtualizarParametro(ID_PARAMETRO: Integer; Valor: String);
begin
  Parametros.Close;
  Parametros.Params[0].AsInteger:=ID_PARAMETRO;
  Parametros.Open;
  Parametros.Edit;
  ParametrosVALOR.AsString:=Valor;
  Parametros.Post;
  Parametros.ApplyUpdates(0);
  Parametros.Close;
end;

procedure Tdm.TitulosACOTERJGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.TitulosCalcFields(DataSet: TDataSet);
var
  qryAux: TFDQuery;
begin
  if dm.vCalc then
  begin
      TitulosEspecie.AsString:=PF.RetornarTitulo(TitulosTIPO_TITULO.AsInteger)+' / N�: '+TitulosNUMERO_TITULO.AsString;

      if TitulosCPF_CNPJ_APRESENTANTE.AsString<>'' then
        if TitulosTIPO_APRESENTANTE.AsString='J' then
          TitulosDocApresentante.AsString:=PF.FormatarCNPJ(TitulosCPF_CNPJ_APRESENTANTE.AsString)
            else TitulosDocApresentante.AsString:=PF.FormatarCPF(TitulosCPF_CNPJ_APRESENTANTE.AsString);

      if TitulosCPF_CNPJ_DEVEDOR.AsString<>'' then
        if TitulosTIPO_DEVEDOR.AsString='J' then
          TitulosDocDevedor.AsString:=PF.FormatarCNPJ(TitulosCPF_CNPJ_DEVEDOR.AsString)
            else TitulosDocDevedor.AsString:=PF.FormatarCPF(TitulosCPF_CNPJ_DEVEDOR.AsString);
  end;

  TitulosxDIA28.AsBoolean             :=GR.iif(TitulosDT_PROTOCOLO.AsDateTime>=StrToDate('28/11/2019'),True,False);

  GR.CriarFDQuery(qryAux,'', conSISTEMAAux);

  qryAux.Close;
  qryAux.SQL.Clear;

  qryAux.SQL.Text := 'SELECT SA.DESCRICAO ' +
                     '  FROM SERVENTIA_AGREGADA SA ' +
                     ' WHERE SA.CODIGO_DISTRIBUICAO = :CODIGO_DISTRIBUICAO';

  qryAux.Params.ParamByName('CODIGO_DISTRIBUICAO').Value := Titulos.FieldByName('CARTORIO').AsInteger;
  qryAux.Open;

  if qryAux.RecordCount > 0 then
    Titulos.FieldByName('SERVENTIA_AGREGADA').AsString := qryAux.FieldByName('DESCRICAO').AsString;

  FreeAndNil(qryAux);
end;

procedure Tdm.TitulosDISTRIBUICAOGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.TitulosEMOLUMENTOSGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.TitulosFETJGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.TitulosFUNARPENGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.TitulosFUNDPERJGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.TitulosFUNPERJGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.TitulosISSGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.TitulosMUTUAGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.TitulosPMCMVGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.TitulosSALDO_PROTESTOGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.TitulosTOTALGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.TitulosVALOR_TITULOGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.CertidoesCalcFields(DataSet: TDataSet);
begin
  if CertidoesTIPO_CERTIDAO.AsString='N' then CertidoesNomeCertidao.AsString:='NORMAL';
  if CertidoesTIPO_CERTIDAO.AsString='E' then CertidoesNomeCertidao.AsString:='ESPECIAL';
  if CertidoesTIPO_CERTIDAO.AsString='C' then CertidoesNomeCertidao.AsString:='CADASTRO';
  if CertidoesTIPO_CERTIDAO.AsString='I' then CertidoesNomeCertidao.AsString:='INTEIRO TEOR';
  if CertidoesTIPO_CERTIDAO.AsString='R' then CertidoesNomeCertidao.AsString:='REVALIDA��O (VISTO)';
  if CertidoesTIPO_CERTIDAO.AsString='X' then CertidoesNomeCertidao.AsString:='CANCELAMENTO';
  if CertidoesTIPO_CERTIDAO.AsString='F' then CertidoesNomeCertidao.AsString:='2� VIA';

  CertidoesPeriodo.AsString:=CertidoesANOS.AsString+' anos';

  if CertidoesCPF_CNPJ_REQUERENTE.AsString<>'' then
    if Length(CertidoesCPF_CNPJ_REQUERENTE.AsString)=14 then
      CertidoesDocumento.AsString:=GR.FormatarCNPJ(CertidoesCPF_CNPJ_REQUERENTE.AsString)
        else CertidoesDocumento.AsString:=GR.FormatarCPF(CertidoesCPF_CNPJ_REQUERENTE.AsString);
end;

procedure Tdm.RxCustasBeforePost(DataSet: TDataSet);
begin
  RxCustasTOTAL.AsFloat:=RxCustasQTD.AsInteger*RxCustasVALOR.AsFloat;
end;

procedure Tdm.ItensBeforePost(DataSet: TDataSet);
begin
  ItensTOTAL.AsFloat:=ItensVALOR.AsFloat*ItensQTD.AsInteger;
end;

function Tdm.ControlarSelo: Boolean;
begin
  Result:=dm.ValorParametro(48)='S';
end;

function Tdm.MontarTabelaConvenio(Codigo,IdAto: Integer; Valor: Double): Double;
var
  Q: TFDQuery;
  vEM,vFE,vFU,vFN,vPM,vIS,vMU,vAC,vDB,xAR,vTT,vCM: Double;
begin
  dm.Custas.Close;
  dm.Custas.Params[0].AsInteger:=-1;
  dm.Custas.Open;

  GR.ExecutarSQLDAC('DELETE FROM CUSTAS WHERE ID_ATO='+QuotedStr(IntToStr(IdAto)), conSISTEMA);

  vEM:=0;
  vCM:=0;

  qryFaixa.Close;
  qryFaixa.ParamByName('C').AsString:='S';
  qryFaixa.ParamByName('VALOR1').AsFloat:=Valor;
  qryFaixa.ParamByName('VALOR2').AsFloat:=Valor;
  qryFaixa.ParamByName('OCULTO1').AsString:='N';
  qryFaixa.ParamByName('OCULTO2').AsString:='N';
  qryFaixa.Open;

  dm.Itens.Close;
  dm.Itens.Params[0].AsInteger:=qryFaixaCOD.AsInteger;
  dm.Itens.Open;
  dm.Itens.First;

  while not dm.Itens.Eof do
  begin
      dm.Custas.Append;
      dm.CustasID_CUSTA.AsInteger :=dm.IdAtual('ID_CUSTA','S');
      dm.CustasID_ATO.AsInteger   :=IdAto;
      dm.CustasTABELA.AsString    :=dm.ItensTAB.AsString;
      dm.CustasITEM.AsString      :=dm.ItensITEM.AsString;
      dm.CustasSUBITEM.AsString   :=dm.ItensSUB.AsString;
      dm.CustasVALOR.AsFloat      :=dm.ItensVALOR.AsFloat;
      dm.CustasQTD.AsString       :=dm.ItensQTD.AsString;
      dm.CustasTOTAL.AsFloat      :=dm.CustasVALOR.AsFloat*dm.CustasQTD.AsFloat;
      dm.Custas.Post;

      if dm.Custas.ApplyUpdates(0)>0 then
      Raise Exception.Create('ApplyUpdates dm.Custas');
      vEM:=vEM+dm.CustasTOTAL.AsFloat;

      if (dm.ItensTAB.AsString='1') or (dm.ItensTAB.AsString='16') then
      vCM:=vCM+dm.CustasTOTAL.AsFloat;

      dm.Itens.Next;
  end;

  vFE := GR.NoRound(vEM*0.2,2);
  vFU := GR.NoRound(vEM*0.05,2);
  vFN := GR.NoRound(vEM*0.04,2);
  vPM := GR.NoRound((vEM-vCM)*0.02,2);
  vIS := GR.NoRound(vEM*Gdm.vAliquotaISS,2);
  vMU := StrToFloat(dm.ValorParametro(26)); //qryFaixaMUTUA.AsFloat;
  vAC := StrToFloat(dm.ValorParametro(27)); //qryFaixaACOTERJ.AsFloat;
  vDB := qryFaixaDISTRIB.AsFloat;
  xAR := 0;
  vTT := vEM+vFE+vFU+vFU+vFN+vPM+vIS+vMU+vAC+vDB+xAR;

  Q:=TFDQuery.Create(Nil);
  Q.Connection:=conSISTEMA;
  Q.SQL.Add('UPDATE TITULOS SET EMOLUMENTOS=:EM,FETJ=:FE,FUNDPERJ=:FU,FUNPERJ=:FU,FUNARPEN=:FN,PMCMV=:PM,ISS=:IS,MUTUA=:MU,ACOTERJ=:AC,'+
            'DISTRIBUICAO=:DB,VALOR_AR=:AR,TOTAL=:TT WHERE ID_ATO=:ID');
  Q.ParamByName('EM').Value:=vEM;
  Q.ParamByName('FE').Value:=vFE;
  Q.ParamByName('FU').Value:=vFU;
  Q.ParamByName('FN').Value:=vFN;
  Q.ParamByName('PM').Value:=vPM;
  Q.ParamByName('IS').Value:=vIS;
  Q.ParamByName('MU').Value:=vMU;
  Q.ParamByName('AC').Value:=vAC;
  Q.ParamByName('DB').Value:=vDB;
  Q.ParamByName('AR').Value:=xAR;
  Q.ParamByName('TT').Value:=vTT;
  Q.ParamByName('ID').Value:=IdAto;
  Q.ExecSQL();
  Q.Close;
  Q.Free;

  Result:=vTT;
end;

procedure Tdm.AEMessage(var Msg: tagMSG; var Handled: Boolean);
var
  Shift: TShiftState;
  Key: Word;
begin
  case Msg.Message of
    WM_KeyDown,WM_SysKeyDown : begin
                                   Key   :=Msg.wParam;
                                   Shift :=KeyDataToShiftState(Msg.lParam);

                                   if (Shift=[ssCtrl]) and (Key=80) then
                                   GR.CriarForm(TFParametros,FParametros);
                               end;
  end;
  case Msg.Message of
    WM_KeyDown,WM_SysKeyDown : begin
                                   Key   :=Msg.wParam;
                                   Shift :=KeyDataToShiftState(Msg.lParam);

                                   if (Shift=[ssCtrl]) and (Key=83) then
                                   GR.Aviso('QUANTIDADE DISPON�VEL S�RIE "'+Gdm.LetraAtual(dm.SerieAtual)+'" = '+IntToStr(Gdm.QtdSeloAtual(dm.SerieAtual)));
                               end;
  end;
end;

function Tdm.DescricaoIrregularidade(Codigo: Integer): String;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=conSISTEMA;
  Q.SQL.Add('SELECT * FROM IRREGULARIDADES WHERE CODIGO=:CODIGO');
  Q.ParamByName('CODIGO').AsInteger:=Codigo;
  Q.Open;
  Result:=Q.FieldByName('MOTIVO').AsString;
  Q.Close;
  Q.Free;
end;

procedure Tdm.DistribuirCartorios;
var
  iQtdServAgregadas, iMenorUltQtd, iTotalDifQtds,
  iQtdMedia, iRestoQtd, iPrioridade, iQtdAloc, iPosTit,
  iTotalTitulosAlocados: Integer;
  cMenorUltSaldo, cTotalDifSaldos, cValorMedioEmol,
  cVlrAloc, cVlrMax, cCustas: Double;
  sNumTit, sContrDev,  sSeqReg: String;
  lGrava: Boolean;
  QryAux: TFDQuery;
begin
  { Essa rotina gera os arquivos D para que serao enviados para
    as Serventias Agregadas }
  sNumTit   := '';
  sContrDev := '';
  sSeqReg   := '';

  cMenorUltSaldo  := 0;
  cTotalDifSaldos := 0;
  cValorMedioEmol := 0;

  iOrdem            := 0;
  iQtdServAgregadas := 0;
  iRestoQtd         := 0;
  iMenorUltQtd      := 0;
  iTotalDifQtds     := 0;
  iQtdMedia         := 0;
  iPrioridade       := 0;
  iPosTit           := 0;

  iTotalTitulosAlocados := 0;

  { SERVENTIAS AGREGADAS }
  //Definir quantidade total de Serventias Agregadas
  dm.ServentiaAgregada.First;

  while not dm.ServentiaAgregada.Eof do
  begin
    dm.ServentiaAgregada.Edit;
    dm.ServentiaAgregadaPRIORIDADE_VALOR.AsInteger := 0;
    dm.ServentiaAgregadaVALOR_MAX_PREV.AsFloat     := 0;
    dm.ServentiaAgregadaVALOR_DIF.AsFloat          := 0;
    dm.ServentiaAgregadaVALOR_ALOCADO.AsFloat      := 0;
    dm.ServentiaAgregadaVALOR_REMANESCENTE.AsFloat := 0;
    dm.ServentiaAgregadaPRIORIDADE_QTD.AsInteger   := 0;
    dm.ServentiaAgregadaQTD_MAX_PREV.AsInteger     := 0;
    dm.ServentiaAgregadaQTD_DIF.AsInteger          := 0;
    dm.ServentiaAgregadaQTD_ALOCADA.AsInteger      := 0;
    dm.ServentiaAgregada.Post;

    Inc(iQtdServAgregadas);

    dm.ServentiaAgregada.Next;
  end;

  iOrdem := 1;

  //Define o menor Ultimo Valor
  dm.ServentiaAgregada.IndexFieldNames := 'QTD_ULTIMO_SALDO';

  dm.ServentiaAgregada.First;

  iMenorUltQtd := dm.ServentiaAgregadaQTD_ULTIMO_SALDO.AsInteger;

  while not dm.ServentiaAgregada.Eof do
  begin
    //Definir Prioridade de Quantidade e Diferencas de Valores
    dm.ServentiaAgregada.Edit;
    dm.ServentiaAgregadaPRIORIDADE_QTD.AsInteger := iOrdem;
    dm.ServentiaAgregadaQTD_DIF.AsInteger        := (dm.ServentiaAgregadaQTD_ULTIMO_SALDO.AsInteger -
                                                     iMenorUltQtd);
    dm.ServentiaAgregada.Post;

    iTotalDifQtds := (iTotalDifQtds +
                      (dm.ServentiaAgregadaQTD_ULTIMO_SALDO.AsInteger -
                       iMenorUltQtd));

    Inc(iOrdem);

    dm.ServentiaAgregada.Next;
  end;

  iQtdMedia := ((iTotalTitulos + iTotalDifQtds) div iQtdServAgregadas);
  iRestoQtd := ((iTotalTitulos + iTotalDifQtds) mod iQtdServAgregadas);

  dm.ServentiaAgregada.First;

  while not dm.ServentiaAgregada.Eof do
  begin
    //Definir Quantidade Maxima Prevista
    dm.ServentiaAgregada.Edit;

    if iRestoQtd > 0 then
      dm.ServentiaAgregadaQTD_MAX_PREV.AsInteger := ((iQtdMedia -
                                                      dm.ServentiaAgregadaQTD_DIF.AsInteger) + 1)
    else
      dm.ServentiaAgregadaQTD_MAX_PREV.AsInteger := (iQtdMedia -
                                                     dm.ServentiaAgregadaQTD_DIF.AsInteger);

    if dm.ServentiaAgregadaQTD_MAX_PREV.AsInteger < 0 then
      dm.ServentiaAgregadaQTD_MAX_PREV.AsInteger := 0;

    dm.ServentiaAgregada.Post;

    if iRestoQtd > 0 then
      Inc(iRestoQtd, -1);

    dm.ServentiaAgregada.Next;
  end;

  //Define o menor Ultimo Valor
  dm.ServentiaAgregada.IndexFieldNames := '';
  dm.ServentiaAgregada.IndexFieldNames := 'VALOR_ULTIMO_SALDO';

  dm.ServentiaAgregada.First;
  iOrdem := 1;

  cMenorUltSaldo := dm.ServentiaAgregadaVALOR_ULTIMO_SALDO.AsFloat;

  while not dm.ServentiaAgregada.Eof do
  begin
    //Definir Prioridade de Valor e Diferencas de Valores
    dm.ServentiaAgregada.Edit;
    dm.ServentiaAgregadaPRIORIDADE_VALOR.AsInteger := iOrdem;
    dm.ServentiaAgregadaVALOR_DIF.AsFloat          := (dm.ServentiaAgregadaVALOR_ULTIMO_SALDO.AsFloat -
                                                       cMenorUltSaldo);
    dm.ServentiaAgregada.Post;

    cTotalDifSaldos := (cTotalDifSaldos +
                       (dm.ServentiaAgregadaVALOR_ULTIMO_SALDO.AsFloat -
                        cMenorUltSaldo));

    Inc(iOrdem);

    dm.ServentiaAgregada.Next;
  end;

  if cTotalDifSaldos >= cTotalEmolumentos then
    cValorMedioEmol := (cTotalEmolumentos / iQtdServAgregadas)
  else
    cValorMedioEmol := ((cTotalEmolumentos + cTotalDifSaldos) / iQtdServAgregadas);

  //Definir Valores Maximos Previstos e Quantidades Maximas Previstas
  dm.ServentiaAgregada.First;

  while not dm.ServentiaAgregada.Eof do
  begin
    dm.ServentiaAgregada.Edit;

    if dm.ServentiaAgregadaVALOR_DIF.AsFloat <= cValorMedioEmol then
      dm.ServentiaAgregadaVALOR_MAX_PREV.AsFloat := (cValorMedioEmol - dm.ServentiaAgregadaVALOR_DIF.AsFloat)
    else
      dm.ServentiaAgregadaVALOR_MAX_PREV.AsFloat := cValorMedioEmol;

    dm.ServentiaAgregada.Post;

    dm.ServentiaAgregada.Next;
  end;

  { TITULOS }
  mdArquivoD.SortOnFields('NUM_TITULO; NUM_CONTROLE_DEV; TOTAL_CUSTAS', True, True);

  iOrdem := 1;
  mdArquivoD.First;

  while not mdArquivoD.Eof do
  begin
    mdArquivoD.Edit;
    mdArquivoDPRIORIDADE_VALOR.AsInteger := iOrdem;
    mdArquivoD.Post;

    Inc(iOrdem);

    mdArquivoD.Next;
  end;

  { CALCULAR DISTRIBUICAO DE TITULOS }
  dm.ServentiaAgregada.IndexFieldNames := '';

  if dm.ValorParametro(98) = 'S' then
    dm.ServentiaAgregada.IndexFieldNames := 'PRIORIDADE_QTD; PRIORIDADE_VALOR'
  else
    dm.ServentiaAgregada.IndexFieldNames := 'PRIORIDADE_VALOR';

  //Dentro da quantidade e dos valores pre-calculados
  mdArquivoD.First;

  sNumTit   := mdArquivoDNUM_TITULO.AsString;
  sContrDev := mdArquivoDNUM_CONTROLE_DEV.AsString;
  sSeqReg   := mdArquivoDSEQUENCIAL_REG.AsString;

  while not mdArquivoD.Eof do
  begin
    //Definir Serventia
    dm.ServentiaAgregada.First;
    Inc(iPrioridade);

    while not dm.ServentiaAgregada.Eof do
    begin
      lGrava := False;

      iQtdAloc := 0;
      cVlrAloc := 0;
      cVlrMax  := 0;

      if not mdArquivoDFLG_ALOCADO.AsBoolean then
      begin
        if PF.RemoverEspeciais(Trim(mdArquivoDTOTAL_CUSTAS.AsString)) = '' then
          cCustas := 0
        else
          cCustas := PF.TextoParaFloat(mdArquivoDTOTAL_CUSTAS.AsString);

        if cCustas < (dm.ServentiaAgregadaVALOR_MAX_PREV.AsFloat -
                      dm.ServentiaAgregadaVALOR_ALOCADO.AsFloat) then
        begin
          if dm.ValorParametro(98) = 'S' then  //Prioridade Quantidade
          begin
            if dm.ServentiaAgregadaQTD_ALOCADA.AsInteger < dm.ServentiaAgregadaQTD_MAX_PREV.AsInteger then
              lGrava := True
            else
            begin
              dm.ServentiaAgregada.Next;

              if dm.ServentiaAgregada.Eof then
                iPrioridade := 0;
            end
          end
          else  //Prioridade Valor
            lGrava := True;
        end
        else
        begin
          dm.ServentiaAgregada.Next;

          if dm.ServentiaAgregada.Eof then
            iPrioridade := 0;
        end;

        if lGrava then
        begin
          if dm.ValorParametro(98) = 'S' then  //Prioridade Quantidade
          begin
            if iPrioridade <> dm.ServentiaAgregadaPRIORIDADE_QTD.AsInteger then
            begin
              lGrava := False;

              dm.ServentiaAgregada.Next;

              if dm.ServentiaAgregada.Eof then
                iPrioridade := 0;
            end
          end
          else  //Prioridade Valor
          begin
            if iPrioridade <> dm.ServentiaAgregadaPRIORIDADE_VALOR.AsInteger then
            begin
              lGrava := False;

              dm.ServentiaAgregada.Next;

              if dm.ServentiaAgregada.Eof then
                iPrioridade := 0;
            end
          end;
        end;

        if lGrava then
        begin
          iPosTit   := mdArquivoD.RecNo;
          sNumTit   := mdArquivoDNUM_TITULO.AsString;
          sContrDev := mdArquivoDNUM_CONTROLE_DEV.AsString;
          sSeqReg   := mdArquivoDSEQUENCIAL_REG.AsString;

          mdArquivoD.DisableControls;
          mdArquivoD.First;

          while not mdArquivoD.Eof do
          begin
            if (Trim(sNumTit) = Trim(mdArquivoDNUM_TITULO.AsString)) and
              (Trim(sContrDev) = Trim(mdArquivoDNUM_CONTROLE_DEV.AsString)) and
              (Trim(sSeqReg) = Trim(mdArquivoDSEQUENCIAL_REG.AsString)) then
            begin
              if PF.RemoverEspeciais(Trim(mdArquivoDTOTAL_CUSTAS.AsString)) = '' then
                cCustas := 0
              else
                cCustas := PF.TextoParaFloat(mdArquivoDTOTAL_CUSTAS.AsString);

              //Arquivo D
              mdArquivoD.Edit;
              mdArquivoDCOD_CARTORIO.AsString := dm.ServentiaAgregadaCODIGO_DISTRIBUICAO.AsString;
              mdArquivoDFLG_ALOCADO.AsBoolean := True;
              mdArquivoD.Post;

              //Serventia Agregada
              iQtdAloc := (dm.ServentiaAgregadaQTD_ALOCADA.AsInteger + 1);
              cVlrAloc := (dm.ServentiaAgregadaVALOR_ALOCADO.AsFloat + cCustas);
              cVlrMax  := (dm.ServentiaAgregadaVALOR_MAX_PREV.AsFloat - cVlrAloc);

              Inc(iTotalTitulosAlocados);

              dm.ServentiaAgregada.Edit;
              dm.ServentiaAgregadaQTD_ALOCADA.AsInteger      := iQtdAloc;
              dm.ServentiaAgregadaVALOR_ALOCADO.AsFloat      := cVlrAloc;
              dm.ServentiaAgregadaVALOR_REMANESCENTE.AsFloat := cVlrMax;
              dm.ServentiaAgregada.Post;
            end;

            mdArquivoD.Next;
          end;

          dm.ServentiaAgregada.Last;

          mdArquivoD.RecNo := iPosTit;
          mdArquivoD.EnableControls;

          if iPrioridade = dm.ServentiaAgregada.RecordCount then
            iPrioridade := 0;
        end;
      end;

      mdArquivoD.Next;
    end;
  end;

  //Fora da quantidade e dos valores pre-calculados
  dm.ServentiaAgregada.IndexFieldNames := '';
  dm.ServentiaAgregada.AddIndex('IDX_VLR_REMAN', 'VALOR_REMANESCENTE', [ixDescending]);
  dm.ServentiaAgregada.IndexName := 'IDX_VLR_REMAN';

  mdArquivoD.First;
  sNumTit   := mdArquivoDNUM_TITULO.AsString;
  sContrDev := mdArquivoDNUM_CONTROLE_DEV.AsString;
  sSeqReg   := mdArquivoDSEQUENCIAL_REG.AsString;

  while not mdArquivoD.Eof do
  begin
    //Definir Serventia
    if not mdArquivoDFLG_ALOCADO.AsBoolean then
    begin
      if PF.RemoverEspeciais(Trim(mdArquivoDTOTAL_CUSTAS.AsString)) = '' then
        cCustas := 0
      else
        cCustas := PF.TextoParaFloat(mdArquivoDTOTAL_CUSTAS.AsString);

      dm.ServentiaAgregada.First;

      while not dm.ServentiaAgregada.Eof do
      begin
        lGrava := False;

        iQtdAloc := 0;
        cVlrAloc := 0;
        cVlrMax  := 0;

        if dm.ValorParametro(98) = 'S' then  //Prioridade Quantidade
        begin
          if dm.ServentiaAgregadaQTD_ALOCADA.AsInteger < dm.ServentiaAgregadaQTD_MAX_PREV.AsInteger then
            lGrava := True
          else
          begin
            dm.ServentiaAgregada.Next;
          end
        end
        else  //Prioridade Valor
        begin
          if dm.ServentiaAgregadaVALOR_ALOCADO.AsFloat < dm.ServentiaAgregadaVALOR_MAX_PREV.AsFloat then
            lGrava := True
          else
          begin
            dm.ServentiaAgregada.Next;
          end;
        end;

        if lGrava then
        begin
          iPosTit   := mdArquivoD.RecNo;
          sNumTit   := mdArquivoDNUM_TITULO.AsString;
          sContrDev := mdArquivoDNUM_CONTROLE_DEV.AsString;
          sSeqReg   := mdArquivoDSEQUENCIAL_REG.AsString;

          mdArquivoD.DisableControls;
          mdArquivoD.First;

          while not mdArquivoD.Eof do
          begin
            if (Trim(sNumTit) = Trim(mdArquivoDNUM_TITULO.AsString)) and
              (Trim(sContrDev) = Trim(mdArquivoDNUM_CONTROLE_DEV.AsString)) and
              (Trim(sSeqReg) = Trim(mdArquivoDSEQUENCIAL_REG.AsString)) then
            begin
              if PF.RemoverEspeciais(Trim(mdArquivoDTOTAL_CUSTAS.AsString)) = '' then
                cCustas := 0
              else
                cCustas := PF.TextoParaFloat(mdArquivoDTOTAL_CUSTAS.AsString);

              //Arquivo D
              mdArquivoD.Edit;
              mdArquivoDCOD_CARTORIO.AsString := dm.ServentiaAgregadaCODIGO_DISTRIBUICAO.AsString;
              mdArquivoDFLG_ALOCADO.AsBoolean := True;
              mdArquivoD.Post;

              //Serventia Agregada
              iQtdAloc := (dm.ServentiaAgregadaQTD_ALOCADA.AsInteger + 1);
              cVlrAloc := (dm.ServentiaAgregadaVALOR_ALOCADO.AsFloat + cCustas);
              cVlrMax  := (dm.ServentiaAgregadaVALOR_MAX_PREV.AsFloat - cVlrAloc);

              Inc(iTotalTitulosAlocados);

              dm.ServentiaAgregada.Edit;
              dm.ServentiaAgregadaQTD_ALOCADA.AsInteger      := iQtdAloc;
              dm.ServentiaAgregadaVALOR_ALOCADO.AsFloat      := cVlrAloc;
              dm.ServentiaAgregadaVALOR_REMANESCENTE.AsFloat := cVlrMax;
              dm.ServentiaAgregada.Post;
            end;

            mdArquivoD.Next;
          end;

          dm.ServentiaAgregada.Last;

          mdArquivoD.RecNo := iPosTit;
          mdArquivoD.EnableControls;
        end;
      end;
    end;

    mdArquivoD.Next;
  end;

  dm.ServentiaAgregada.DeleteIndex('IDX_VLR_REMAN');

{  if iTotalTitulosAlocados < iTotalTitulos then
  begin
    dm.ServentiaAgregada.IndexFieldNames := '';
    dm.ServentiaAgregada.AddIndex('IDX_VLR_TOTAL', 'VALOR_ULTIMO_SALDO', []);
    dm.ServentiaAgregada.IndexName := 'IDX_VLR_TOTAL';

    mdArquivoD.SortOnFields('TOTAL_CUSTAS', True, True);

    repeat
      mdArquivoD.First;
      sNumTit := mdArquivoDNUM_TITULO.AsString;

      dm.ServentiaAgregada.First;

      while not mdArquivoD.Eof do
      begin
        if PF.RemoverEspeciais(Trim(mdArquivoDTOTAL_CUSTAS.AsString)) = '' then
          cCustas := 0
        else
          cCustas := PF.TextoParaFloat(mdArquivoDTOTAL_CUSTAS.AsString);

        //Definir Serventia
        if not mdArquivoDFLG_ALOCADO.AsBoolean then
        begin
          iQtdAloc := 0;
          cVlrAloc := 0;
          cVlrMax  := 0;

          //Arquivo D
          mdArquivoD.Edit;
          mdArquivoDCOD_CARTORIO.AsString := dm.ServentiaAgregadaCODIGO_DISTRIBUICAO.AsString;
          mdArquivoDFLG_ALOCADO.AsBoolean := True;
          mdArquivoD.Post;

          //Serventia Agregada
          iQtdAloc := (dm.ServentiaAgregadaQTD_ALOCADA.AsInteger + 1);
          cVlrAloc := (dm.ServentiaAgregadaVALOR_ALOCADO.AsFloat + cCustas);
          cVlrMax  := (dm.ServentiaAgregadaVALOR_MAX_PREV.AsFloat - cVlrAloc);

          Inc(iTotalTitulosAlocados);

          dm.ServentiaAgregada.Edit;
          dm.ServentiaAgregadaQTD_ALOCADA.AsInteger      := iQtdAloc;
          dm.ServentiaAgregadaVALOR_ALOCADO.AsFloat      := cVlrAloc;
          dm.ServentiaAgregadaVALOR_REMANESCENTE.AsFloat := cVlrMax;
          dm.ServentiaAgregada.Post;

          dm.ServentiaAgregada.Next;
        end;

        mdArquivoD.Next;
      end;
    until (iTotalTitulosAlocados = iTotalTitulos);

    dm.ServentiaAgregada.DeleteIndex('IDX_VLR_TOTAL');
  end;  }

  { Atualiza Totais para a Serventia Agregada }
  GR.CriarFDQuery(QryAux,'', conSISTEMA);


  dm.ServentiaAgregada.First;

  while not dm.ServentiaAgregada.Eof do
  begin
    QryAux.Close;
    QryAux.SQL.Clear;

    QryAux.SQL.Text := 'UPDATE SERVENTIA_AGREGADA ' +
                       '   SET QTD_ULTIMO_SALDO   = :QTD_ULTIMO_SALDO, ' +
                       '       VALOR_ULTIMO_SALDO = :VALOR_ULTIMO_SALDO, ' +
                       '       DATA_ULTIMO_SALDO  = :DATA_ULTIMO_SALDO ' +
                       ' WHERE ID_SERVENTIA_AGREGADA = :ID_SERVENTIA_AGREGADA';

    QryAux.Params.ParamByName('QTD_ULTIMO_SALDO').Value      := (dm.ServentiaAgregadaQTD_ULTIMO_SALDO.AsInteger +
                                                                 dm.ServentiaAgregadaQTD_ALOCADA.AsInteger);
    QryAux.Params.ParamByName('VALOR_ULTIMO_SALDO').Value    := (dm.ServentiaAgregadaVALOR_ULTIMO_SALDO.AsFloat +
                                                                 dm.ServentiaAgregadaVALOR_ALOCADO.AsFloat);
    QryAux.Params.ParamByName('DATA_ULTIMO_SALDO').Value     := Date;
    QryAux.Params.ParamByName('ID_SERVENTIA_AGREGADA').Value := ServentiaAgregadaID_SERVENTIA_AGREGADA.AsInteger;
    QryAux.ExecSQL;

    dm.ServentiaAgregada.Next;
  end;

  FreeAndNil(QryAux);

  //Acertar Codigo da Serventia Agregada no Titulo
  mdArquivoD.DisableControls;
  mdArquivoD.First;

  //Incluir identificacao da Serventia Agregada no Titulo
  while not mdArquivoD.Eof do
  begin
    dm.Titulos.Close;
    dm.Titulos.Params.ParamByName('ID').AsInteger := mdArquivoDID_ATO.AsInteger;
    dm.Titulos.Open;

    if dm.Titulos.RecordCount > 0 then
    begin
      dm.Titulos.Edit;
      dm.TitulosCARTORIO.AsInteger := mdArquivoDCOD_CARTORIO.AsInteger;
      dm.TitulosFLG_SALDO.AsString := 'S';
      dm.Titulos.Post;
      dm.Titulos.ApplyUpdates(0);
    end;

    mdArquivoD.Next;
  end;

  mdArquivoD.EnableControls;
end;

procedure Tdm.DMLDeleteFD(Tabela: String; Conexao: TFDConnection;
  Condicao: String);
var
  QryDel: TFDQuery;
  sTexto: String;
begin
  sTexto := '';

  try
    GR.CriarFDQuery(QryDel,'', Conexao);

    if Conexao.Connected then
      Conexao.StartTransaction;

    QryDel.Close;
    QryDel.SQL.Clear;

    sTexto := 'DELETE FROM ' + Tabela;

    if Trim(Condicao) <> '' then
      sTexto := sTexto + ' WHERE ' + Condicao;

    QryDel.SQL.Text := sTexto;
    QryDel.ExecSQL;

    if Conexao.InTransaction then
      Conexao.Commit;

    FreeAndNil(QryDel);
  except
    if Conexao.InTransaction then
      Conexao.Rollback;

    FreeAndNil(QryDel);
  end;
end;

procedure Tdm.DMLInsertFD(Tabela, Campo, Valor: String; Conexao: TFDConnection);
var
  QryIns: TFDQuery;
  sTexto: String;
begin
  sTexto := '';

  try
    GR.CriarFDQuery(QryIns,'', Conexao);

    if Conexao.Connected then
      Conexao.StartTransaction;

    QryIns.Close;
    QryIns.SQL.Clear;

    sTexto := 'INSERT INTO ' + Tabela + '(' + Campo + ')' +
              '     VALUES (' + Valor + ')';

    QryIns.SQL.Text := sTexto;
    QryIns.ExecSQL;

    if Conexao.InTransaction then
      Conexao.Commit;

    FreeAndNil(QryIns);
  except
    if Conexao.InTransaction then
      Conexao.Rollback;

    FreeAndNil(QryIns);
  end;
end;

procedure Tdm.DMLSelectFD(var QrySel: TFDQuery; Select, From: String;
  Conexao: TFDConnection; Where, GroupBy, OrderBy: String);
var
  sTexto: String;
begin
  sTexto := '';

  QrySel.Close;
  QrySel.SQL.Clear;

  sTexto := 'SELECT ' + Select +
            '  FROM ' + From;

  if Trim(Where) <> '' then
    sTexto := sTexto + ' WHERE ' + Where;

  if Trim(GroupBy) <> '' then
    sTexto := sTexto + ' GROUP BY ' + GroupBy;

  if Trim(OrderBy) <> '' then
    sTexto := sTexto + ' ORDER BY ' + OrderBy;

  QrySel.SQL.Text := sTexto;
  QrySel.Open;
end;

procedure Tdm.DMLUpdateFD(Tabela, CampoValor: String; Conexao: TFDConnection;
  Condicao: String);
var
  QryUp: TFDQuery;
  sTexto: String;
begin
  sTexto := '';

  try
    GR.CriarFDQuery(QryUp,'', Conexao);

    if Conexao.Connected then
      Conexao.StartTransaction;

    QryUp.Close;
    QryUp.SQL.Clear;

    sTexto := 'UPDATE ' + Tabela +
              '   SET ' + CampoValor;

    if Trim(Condicao) <> '' then
      sTexto := sTexto + ' WHERE ' + Condicao;

    QryUp.SQL.Text := sTexto;
    QryUp.ExecSQL;

    if Conexao.InTransaction then
      Conexao.Commit;

    FreeAndNil(QryUp);
  except
    if Conexao.InTransaction then
      Conexao.Rollback;

    FreeAndNil(QryUp);
  end;
end;

function Tdm.MatriculaEscrevente(Nome: String): String;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=conSISTEMA;
  Q.SQL.Add('SELECT MATRICULA FROM ESCREVENTES WHERE LOGIN=:LOGIN');
  Q.ParamByName('LOGIN').AsString:=Nome;
  Q.Open;
  Result:=Q.FieldByName('MATRICULA').AsString;
  Q.Close;
  Q.Free;
end;

function Tdm.CodigoAdicional(Valor: Double; Convenio: String): Integer;
begin
  if Convenio='S' then
  begin
      if (Valor<=50)                              then Result:=4001;
      if (Valor>=50.01)    and (Valor<=100.00)    then Result:=4002;
      if (Valor>=100.01)   and (Valor<=200.00)    then Result:=4003;
      if (Valor>=200.01)   and (Valor<=500.00)    then Result:=4004;
      if (Valor>=500.01)   and (Valor<=1000.00)   then Result:=4005;
      if (Valor>=1000.01)  and (Valor<=5000.00)   then Result:=4006;
      if (Valor>=5000.01)  and (Valor<=10000.00)  then Result:=4007;
      if (Valor>=10000.01)                        then Result:=4008;
  end
  else
  begin
      if (Valor<=50)                              then Result:=4030;
      if (Valor>=50.01)    and (Valor<=100.00)    then Result:=4031;
      if (Valor>=100.01)   and (Valor<=200.00)    then Result:=4032;
      if (Valor>=200.01)   and (Valor<=500.00)    then Result:=4033;
      if (Valor>=500.01)   and (Valor<=1000.00)   then Result:=4034;
      if (Valor>=1000.01)  and (Valor<=5000.00)   then Result:=4035;
      if (Valor>=5000.01)  and (Valor<=10000.00)  then Result:=4036;
      if (Valor>=10000.01)                        then Result:=4037;
  end;
end;

procedure Tdm.MudarDescricaoParamentro(ID_PARAMETRO: Integer;
  Descricao: String);
begin
  Parametros.Close;
  Parametros.Params[0].AsInteger:=ID_PARAMETRO;
  Parametros.Open;
  Parametros.Edit;
  ParametrosDESCRICAO.AsString:=Descricao;
  Parametros.Post;
  Parametros.ApplyUpdates(0);
  Parametros.Close;
end;

procedure Tdm.VerificarConexoes;
begin

// MARCELO - TIREI ESSA PORRA - TAVA TRAVANDO SEMPRE AI.
{  if Protesto.Connected then
  begin
      ShowMessage('PROTESTO OPEN!');
      Application.Terminate;
  end;

  if Central.Connected then
  begin
      ShowMessage('CENTRAL OPEN!');
      Application.Terminate;
  end;}
end;

procedure Tdm.VerificarExistenciaPastasSistema;
var
  Dia, Mes, Ano: Word;
begin
  Dia := 0;
  Mes := 0;
  Ano := 0;

  DecodeDate(Date, Ano, Mes, Dia);

  //Remessas do CRA
  if Trim(dm.ValorParametro(2)) <> '' then
  begin
    if not DirectoryExists(dm.ValorParametro(2) + IntToStr(Ano) + '\' + GR.Zeros(IntToStr(Mes), 'D', 2) + '\' + GR.Zeros(IntToStr(Dia), 'D', 2) + '\') then
      ForceDirectories(dm.ValorParametro(2) + IntToStr(Ano) + '\' + GR.Zeros(IntToStr(Mes), 'D', 2) + '\' + GR.Zeros(IntToStr(Dia), 'D', 2) + '\');
  end;

  //Remessas para as Serventias Agregadas
  if Trim(dm.ValorParametro(97)) <> '' then
  begin
    if not DirectoryExists(dm.ValorParametro(97) + IntToStr(Ano) + '\' + GR.Zeros(IntToStr(Mes), 'D', 2) + '\' + GR.Zeros(IntToStr(Dia), 'D', 2) + '\') then
      ForceDirectories(dm.ValorParametro(97) + IntToStr(Ano) + '\' + GR.Zeros(IntToStr(Mes), 'D', 2) + '\' + GR.Zeros(IntToStr(Dia), 'D', 2) + '\');
  end;

  //Confirmacao para o CRA
  if Trim(dm.ValorParametro(3)) <> '' then
  begin
    if not DirectoryExists(dm.ValorParametro(3) + IntToStr(Ano) + '\' + GR.Zeros(IntToStr(Mes), 'D', 2) + '\' + GR.Zeros(IntToStr(Dia), 'D', 2) + '\') then
      ForceDirectories(dm.ValorParametro(3) + IntToStr(Ano) + '\' + GR.Zeros(IntToStr(Mes), 'D', 2) + '\' + GR.Zeros(IntToStr(Dia), 'D', 2) + '\');
  end;

  //Retorno das Serventias Agregadas
  if Trim(dm.ValorParametro(99)) <> '' then
  begin
    if not DirectoryExists(dm.ValorParametro(99) + IntToStr(Ano) + '\' + GR.Zeros(IntToStr(Mes), 'D', 2) + '\' + GR.Zeros(IntToStr(Dia), 'D', 2) + '\') then
      ForceDirectories(dm.ValorParametro(99) + IntToStr(Ano) + '\' + GR.Zeros(IntToStr(Mes), 'D', 2) + '\' + GR.Zeros(IntToStr(Dia), 'D', 2) + '\');
  end;

  //Retorno para o CRA
  if Trim(dm.ValorParametro(4)) <> '' then
  begin
    if not DirectoryExists(dm.ValorParametro(4) + IntToStr(Ano) + '\' + GR.Zeros(IntToStr(Mes), 'D', 2) + '\' + GR.Zeros(IntToStr(Dia), 'D', 2) + '\') then
      ForceDirectories(dm.ValorParametro(4) + IntToStr(Ano) + '\' + GR.Zeros(IntToStr(Mes), 'D', 2) + '\' + GR.Zeros(IntToStr(Dia), 'D', 2) + '\');
  end;
end;

procedure Tdm.CarregarObjetosAlpha(ComboBox: TsComboBox;
  Simple: TSimpleDataSet; FieldDescricao, FieldObjeto: String);
begin
  ComboBox.Items.Clear;
  Simple.Close;
  Simple.Open;
  Simple.First;
  while not Simple.Eof do
  begin
      ComboBox.Items.AddObject(Simple.FieldByName(FieldDescricao).AsString,TObject(Simple.FieldByName(FieldObjeto).AsInteger));
      Simple.Next;
  end;
end;

function Tdm.SerieAtual: Integer;
begin
  Result:=StrToInt(dm.ValorParametro(82));
end;

function Tdm.CampoServentia(Campo: String): String;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=conSISTEMA;
  Q.SQL.Add('SELECT '+Campo+' FROM SERVENTIA');
  Q.Open;
  Result:=Q.FieldByName(Campo).AsString;
  Q.Close;
  Q.Free;
end;

procedure Tdm.ConfigurarArquivoIni;
var
  F: TextFile;
  vPath: String;
begin
  try
    dm.sEletronico.Close;
    dm.sEletronico.DataSet.Params[0].AsString:=CampoServentia('CODIGO');
    dm.sEletronico.Open;
    if dm.EletronicoPATH_GERENCIAL.AsString='' then
    GR.Aviso('TOTAL GERENCIAL N�O CONFIGURADO!');
  except
    on E: Exception do
    begin
        GR.Aviso('ERRO: N�O FOI POSS�VEL CONECTAR AO SERVIDOR WEB!'+#13#13+'MSG: '+E.Message);
    end;
  end;

  AssignFile(F,GR.DirExe+'Config.ini');
  Rewrite(F);
  Writeln(F,'[DATABASE]');
  Writeln(F,'');
  vPath:=GR.LerTagTxt(GR.DirExe+'Conf.txt','SERVER')+':'+GR.LerTagTxt(GR.DirExe+'Conf.txt','PROTESTO');
  Writeln(F,'PROTESTO='+GR.TextoMaiusculo(vPath));
  vPath:=GR.LerTagTxt(GR.DirExe+'Conf.txt','SERVER')+':'+GR.LerTagTxt(GR.DirExe+'Conf.txt','CENTRAL');
  Writeln(F,'CENTRAL='+GR.TextoMaiusculo(vPath));
  vPath:=GR.LerTagTxt(GR.DirExe+'Conf.txt','SERVER')+':'+GR.LerTagTxt(GR.DirExe+'Conf.txt','XML');
  Writeln(F,'XML='+GR.TextoMaiusculo(vPath));
  Writeln(F,'GERENCIAL='+dm.EletronicoPATH_GERENCIAL.AsString);
  CloseFile(F);
  RenameFile(GR.DirExe+'Conf.txt',GR.DirExe+'ConfOld.txt');

  dm.sEletronico.Close;
  dm.BDOnline.Close;
end;

procedure Tdm.RxCustasFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept:=RxCustasID_ATO.AsInteger=vIdAto;
end;

procedure Tdm.ApagarRXCustas(IdAto: Integer);
begin
  dm.RxCustas.Filtered:=False;
  dm.RxCustas.Filtered:=True;
  dm.vIdAto:=IdAto;
  dm.RxCustas.First;
  if not dm.RxCustas.IsEmpty then
    repeat
      dm.RxCustas.Delete;
    until (dm.RxCustas.Eof) or (dm.RxCustas.IsEmpty);
end;

function Tdm.CampoTitulos(Campo: String; IdAto: Integer): Variant;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=conSISTEMA;
  Q.SQL.Add('SELECT '+Campo+' FROM TITULOS WHERE ID_ATO='+IntToStr(IdAto));
  Q.Open;
  Result:=Q.FieldByName(Campo).Value;
  Q.Close;
  Q.Free;
end;

procedure Tdm.MudarValor(Sigla: String; Valor: Integer);
begin
  GR.ExecutarSQLDAC('UPDATE CONTROLE SET VALOR='+IntToStr(Valor)+' WHERE SIGLA='''+Sigla+'''',conSISTEMA);
end;

procedure Tdm.InserirTipoTitulo(Codigo,Descricao: String);
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=conSISTEMA;
  Q.SQL.Add('SELECT * FROM TIPOS WHERE CODIGO='+Codigo);
  Q.Open;
  if Q.IsEmpty then
  GR.ExecutarSQLDAC('INSERT INTO TIPOS (CODIGO,DESCRICAO) VALUES ('+Codigo+','''+Descricao+''')',dm.conSISTEMA);
  Q.Close;
  Q.Free;
end;

function Tdm.ProximaOrdem(IdAto: Integer): Integer;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=conSISTEMA;
  Q.SQL.Add('SELECT MAX(ORDEM)+1 AS PROXIMO FROM DEVEDORES WHERE ID_ATO='+IntToStr(IdAto));
  Q.Open;
  Result:=Q.FieldByName('PROXIMO').AsInteger;
  Q.Close;
  Q.Free;
end;

function Tdm.ProximoDiaUtil(DataReferencia: TDate): TDate;
var
  qryFer: TFDQuery;
  Data: TDate;
  Dia: Integer;
begin
  Data := DataReferencia;

  GR.CriarFDQuery(qryFer,'', conSISTEMAAux);

  repeat
    Data := IncDay(Data, 1);

    qryFer.Close;
    qryFer.SQL.Clear;

    qryFer.SQL.Text := 'SELECT ID_FERIADO ' +
                       '  FROM FERIADOS ' +
                       ' WHERE DIA = :DIA ' +
                       '   AND MES = :MES ' +
                       '   AND ANO = :ANO';

    qryFer.Params.ParamByName('DIA').Value := DayOf(Data);
    qryFer.Params.ParamByName('MES').Value := MonthOf(Data);
    qryFer.Params.ParamByName('ANO').Value := YearOf(Data);

    qryFer.Open;

    if qryFer.RecordCount > 0 then
      Data := IncDay(Data, 1);

    Dia := DayOfWeek(Data);

  until Dia in [2, 3, 4, 5, 6];

  FreeAndNil(qryFer);

  Result := Data;
end;

function Tdm.ProximoId(Campo, Tabela: String; Conexao: Integer;
  Gerar: Boolean): Integer;
var
  Qry, QryAux: TFDQuery;
  Id: Integer;
begin
  if Conexao = 1 then
  begin
    GR.CriarFDQuery(Qry,'', conSISTEMAAux);
    GR.CriarFDQuery(QryAux,'', conSISTEMAAux);
  end
  else if Conexao = 2 then
  begin
    GR.CriarFDQuery(Qry,'', conCENTRALAux);
    GR.CriarFDQuery(QryAux,'', conCENTRALAux);
  end;

  with Qry, SQL do
  begin
    Id := 0;

    Close;
    Clear;

    if Tabela = 'IDS' then
    begin
      Text := 'SELECT VALOR AS ID ' +
              '  FROM ' + Tabela +
              ' WHERE CAMPO = ' + QuotedStr(Campo);
    end
    else
    begin
      Text := 'SELECT MAX(' + Campo + ') AS ID ' +
              '  FROM ' + Tabela;
    end;

    Open;

    if Qry.FieldByName('ID').IsNull then
      Id := 0
    else
      Id := Qry.FieldByName('ID').AsInteger;
  end;

  if Gerar and (Tabela = 'IDS') then
  begin
    try
      if conSISTEMAAux.Connected then
        conSISTEMAAux.StartTransaction;

      with QryAux, SQL do
      begin
        Close;
        Clear;

        Id := (Id + 1);

        Text := 'UPDATE ' + Tabela +
                '   SET VALOR = ' + IntToStr(Id) +
                ' WHERE CAMPO = ' + QuotedStr(Campo);

        ExecSQL;
      end;

      if conSISTEMAAux.InTransaction then
        conSISTEMAAux.Commit;
    except
      if conSISTEMAAux.InTransaction then
        conSISTEMAAux.Rollback;
    end;
  end;

  FreeAndNil(Qry);
  FreeAndNil(QryAux);

  Result := Id;
end;

procedure Tdm.qryRel01_ArqIECalcFields(DataSet: TDataSet);
//var
  //qryAux: TFDQuery;
begin
  //qryAux := Gdm.CriarFDQuery(nil, conSISTEMAAux);
(*
  { Quantidade e Valor de Titulos aceitos na importacao do arquivo de
    Remessa B do CRA }
  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Text := 'SELECT COUNT(TT.ID_ATO) AS QTD_TITULO, ' +
                     '       SUM(TT.VALOR_TITULO) AS VR_TITULO ' +
                     '  FROM TITULOS TT ' +
                     ' WHERE TT.ARQUIVO = :ARQUIVO_IMPORTADO_CRA ' +
                     '   AND TT.DT_ENTRADA = :DT_ENTRADA';
  qryAux.Params.ParamByName('ARQUIVO_IMPORTADO_CRA').Value := QuotedStr(qryRel01_ArqIE.FieldByName('ARQUIVO_IMPORTADO_CRA').AsString);
  qryAux.Params.ParamByName('DT_ENTRADA').Value            := qryRel01_ArqIE.FieldByName('DT_ENTRADA').AsDateTime;
  qryAux.Open;

  if qryAux.RecordCount > 0 then
  begin
    qryRel01_ArqIE.FieldByName('QTD_ACEITO_CRA').AsInteger := qryAux.FieldByName('QTD_TITULO').AsInteger;
    qryRel01_ArqIE.FieldByName('VR_ACEITO_CRA').AsFloat    := qryAux.FieldByName('VR_TITULO').AsFloat;
  end
  else
  begin
    qryRel01_ArqIE.FieldByName('QTD_ACEITO_CRA').AsInteger := 0;
    qryRel01_ArqIE.FieldByName('VR_ACEITO_CRA').AsFloat    := 0;
  end;

  { Quantidade, Total de Titulos e Total de Custas na exportacao do arquivo de
    Remessa D para as Serventias Agregadas }
  if not qryRel01_ArqIE.FieldByName('ARQUIVO_EXPORTADO_SA').IsNull then
  begin
    qryAux.Close;
    qryAux.SQL.Clear;
    qryAux.SQL.Text := 'SELECT COUNT(TT.ID_ATO) AS QTD_TITULO, ' +
                       '       SUM(TT.VALOR_TITULO) AS VR_TITULO, ' +
                       '       SUM(TT.TOTAL) AS VR_CUSTAS_TITULO ' +
                       '  FROM TITULOS TT ' +
                       ' WHERE TT.ARQUIVO_CARTORIO = :ARQUIVO_EXPORTADO_SA ' +
                       '   AND TT.DT_ENTRADA = :DT_ENTRADA';
//    qryAux.Params.ParamByName('ARQUIVO_IMPORTADO_CRA').Value := QuotedStr(qryRel01_ArqIE.FieldByName('ARQUIVO_IMPORTADO_CRA').AsString);
    qryAux.Params.ParamByName('ARQUIVO_EXPORTADO_SA').Value  := QuotedStr(qryRel01_ArqIE.FieldByName('ARQUIVO_EXPORTADO_SA').AsString);
    qryAux.Params.ParamByName('DT_ENTRADA').Value            := qryRel01_ArqIE.FieldByName('DT_ENTRADA').AsDateTime;
    qryAux.Open;

    if qryAux.RecordCount > 0 then
    begin
      qryRel01_ArqIE.FieldByName('QTD_EXPORTADO_SA').AsInteger := qryAux.FieldByName('QTD_TITULO').AsInteger;
      qryRel01_ArqIE.FieldByName('VR_EXPORTADO_SA').AsFloat    := qryAux.FieldByName('VR_TITULO').AsFloat;
      qryRel01_ArqIE.FieldByName('VR_CUSTAS_EXP_SA').AsFloat   := qryAux.FieldByName('VR_CUSTAS_TITULO').AsFloat;
    end
    else
    begin
      qryRel01_ArqIE.FieldByName('QTD_EXPORTADO_SA').AsInteger := 0;
      qryRel01_ArqIE.FieldByName('VR_EXPORTADO_SA').AsFloat    := 0;
      qryRel01_ArqIE.FieldByName('VR_CUSTAS_EXP_SA').AsFloat   := 0;
    end;
  end;

  { Quantidade, Total de Titulos e Total de Custas no retorno do arquivo de
    Retorno das Serventias Agregadas }
  if not qryRel01_ArqIE.FieldByName('ARQUIVO_RET_SA').IsNull then
  begin
    qryAux.Close;
    qryAux.SQL.Clear;
    qryAux.SQL.Text := 'SELECT COUNT(TT.ID_ATO) AS QTD_TITULO, ' +
                       '       SUM(TT.VALOR_TITULO) AS VR_TITULO, ' +
                       '       SUM(TT.TOTAL) AS VR_CUSTAS_TITULO ' +
                       '  FROM TITULOS TT ' +
                       ' WHERE TT.RETORNO_CARTORIO = :ARQUIVO_RET_SA ' +
                       '   AND TT.DT_ENTRADA = :DT_ENTRADA';
//    qryAux.Params.ParamByName('ARQUIVO_IMPORTADO_CRA').Value := QuotedStr(qryRel01_ArqIE.FieldByName('ARQUIVO_IMPORTADO_CRA').AsString);
//    qryAux.Params.ParamByName('ARQUIVO_EXPORTADO_SA').Value  := QuotedStr(qryRel01_ArqIE.FieldByName('ARQUIVO_EXPORTADO_SA').AsString);
    qryAux.Params.ParamByName('ARQUIVO_RET_SA').Value        := QuotedStr(qryRel01_ArqIE.FieldByName('ARQUIVO_RET_SA').AsString);
    qryAux.Params.ParamByName('DT_ENTRADA').Value            := qryRel01_ArqIE.FieldByName('DT_ENTRADA').AsDateTime;
    qryAux.Open;

    if qryAux.RecordCount > 0 then
    begin
      qryRel01_ArqIE.FieldByName('QTD_RETORNO_SA').AsInteger := qryAux.FieldByName('QTD_TITULO').AsInteger;
      qryRel01_ArqIE.FieldByName('VR_RETORNO_SA').AsFloat    := qryAux.FieldByName('VR_TITULO').AsFloat;
      qryRel01_ArqIE.FieldByName('VR_CUSTAS_RET_SA').AsFloat := qryAux.FieldByName('VR_CUSTAS_TITULO').AsFloat;
    end
    else
    begin
      qryRel01_ArqIE.FieldByName('QTD_RETORNO_SA').AsInteger := 0;
      qryRel01_ArqIE.FieldByName('VR_RETORNO_SA').AsFloat    := 0;
      qryRel01_ArqIE.FieldByName('VR_CUSTAS_RET_SA').AsFloat := 0;
    end;
  end;

  { Quantidade, Total de Titulos e Total de Custas no retorno do arquivo de
    Retorno para o CRA }
  if not qryRel01_ArqIE.FieldByName('ARQUIVO_RET_CRA').IsNull then
  begin
    qryAux.Close;
    qryAux.SQL.Clear;
    qryAux.SQL.Text := 'SELECT COUNT(TT.ID_ATO) AS QTD_TITULO, ' +
                       '       SUM(TT.VALOR_TITULO) AS VR_TITULO, ' +
                       '       SUM(TT.TOTAL) AS VR_CUSTAS_TITULO ' +
                       '  FROM TITULOS TT ' +
                       ' WHERE TT.RETORNO = :ARQUIVO_RET_CRA ' +
                       '   AND TT.DT_ENTRADA = :DT_ENTRADA';
//    qryAux.Params.ParamByName('ARQUIVO_IMPORTADO_CRA').Value := QuotedStr(qryRel01_ArqIE.FieldByName('ARQUIVO_IMPORTADO_CRA').AsString);
//    qryAux.Params.ParamByName('ARQUIVO_EXPORTADO_SA').Value  := QuotedStr(qryRel01_ArqIE.FieldByName('ARQUIVO_EXPORTADO_SA').AsString);
//    qryAux.Params.ParamByName('ARQUIVO_RET_SA').Value        := QuotedStr(qryRel01_ArqIE.FieldByName('ARQUIVO_RET_SA').AsString);
    qryAux.Params.ParamByName('ARQUIVO_RET_CRA').Value       := QuotedStr(qryRel01_ArqIE.FieldByName('ARQUIVO_RET_CRA').AsString);
    qryAux.Params.ParamByName('DT_ENTRADA').Value            := qryRel01_ArqIE.FieldByName('DT_ENTRADA').AsDateTime;
    qryAux.Open;

    if qryAux.RecordCount > 0 then
    begin
      qryRel01_ArqIE.FieldByName('QTD_RETORNO_CRA').AsInteger := qryAux.FieldByName('QTD_TITULO').AsInteger;
      qryRel01_ArqIE.FieldByName('VR_RETORNO_CRA').AsFloat    := qryAux.FieldByName('VR_TITULO').AsFloat;
      qryRel01_ArqIE.FieldByName('VR_CUSTAS_RET_CRA').AsFloat := qryAux.FieldByName('VR_CUSTAS_TITULO').AsFloat;
    end
    else
    begin
      qryRel01_ArqIE.FieldByName('QTD_RETORNO_CRA').AsInteger := 0;
      qryRel01_ArqIE.FieldByName('VR_RETORNO_CRA').AsFloat    := 0;
      qryRel01_ArqIE.FieldByName('VR_CUSTAS_RET_CRA').AsFloat := 0;
    end;
  end;
         *)
  //FreeAndNil(qryAux);
end;

procedure Tdm.qryVerbalEMOLUMENTOSGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.qryVerbalFETJGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.qryVerbalFUNARPENGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.qryVerbalFUNDPERJGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.qryVerbalFUNPERJGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.qryVerbalTOTALGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure Tdm.OrdenarRXDevedores;
var
  P: Pointer;
begin
  P:=RxDevedor.GetBookmark;
  RxDevedor.DisableControls;
  RxDevedor.First;
  while not RxDevedor.Eof do
  begin
      RxDevedor.Edit;
      RxDevedorORDEM.AsInteger:=RxDevedor.RecNo;
      RxDevedor.Post;
      RxDevedor.Next;
  end;
  if P<>Nil then
    if RxDevedor.BookmarkValid(P) then
      RxDevedor.GotoBookmark(P);
  RxDevedor.EnableControls;
end;

procedure Tdm.RxDevedorBeforePost(DataSet: TDataSet);
begin
  if (RxDevedorDT_EMISSAO.AsDateTime = 0) and
      RxDevedorDT_EMISSAO.IsNull then
    RxDevedorDT_EMISSAO.Clear;

  if Trim(RxDevedorDOCUMENTO.AsString) <> '' then
    RxDevedorDOCUMENTO.AsString := PF.RemoverMascara(RxDevedorDOCUMENTO.AsString);

  if RxDevedorORDEM.IsNull then
    RxDevedorORDEM.AsInteger := (RxDevedor.RecordCount + 1);
end;

procedure Tdm.RxDevedorCalcFields(DataSet: TDataSet);
begin
  if Trim(RxDevedorTIPO.AsString) = 'F' then
    RxDevedorDOCUMENTO_FORMATADO.AsString := PF.FormatarCPF(RxDevedorDOCUMENTO.AsString)
  else if Trim(RxDevedorTIPO.AsString) = 'J' then
    RxDevedorDOCUMENTO_FORMATADO.AsString := PF.FormatarCNPJ(RxDevedorDOCUMENTO.AsString)
  else
    RxDevedorDOCUMENTO_FORMATADO.AsString := RxDevedorDOCUMENTO.AsString;
end;

function Tdm.PortadoresDuplicados(Imprimir: Boolean): Boolean;
var
  Q1,Q2: TFDQuery;
begin
  RXDuplicados.Close;
  RXDuplicados.Open;

  Q1:=TFDQuery.Create(Nil);
  Q1.Connection:=conSISTEMA;
  Q1.SQL.Add('SELECT ID_PORTADOR,CODIGO,NOME,DOCUMENTO,CRA,BANCO,CONVENIO FROM PORTADORES WHERE (CRA='''+'S'+''') or (BANCO='''+'S'+''' or CONVENIO='''+'S'+''')');

  Q2:=TFDQuery.Create(Nil);
  Q2.Connection:=conSISTEMA;
  //Q2.SQL.Add('SELECT ID_PORTADOR FROM PORTADORES WHERE (CODIGO=:CODIGO OR NOME=:NOME OR DOCUMENTO=:DOCUMENTO) AND (ID_PORTADOR<>:ID_PORTADOR)');
  Q2.SQL.Add('SELECT ID_PORTADOR FROM PORTADORES WHERE (CODIGO=:CODIGO) AND (ID_PORTADOR<>:ID_PORTADOR) AND '+
             '(CODIGO<>''0'') AND (CODIGO<>'''') ORDER BY CODIGO,NOME');

  Q1.Open;
  while not Q1.Eof do
  begin
      Q2.Close;
      Q2.ParamByName('CODIGO').AsString       :=Q1.FieldByName('CODIGO').AsString;
      //Q2.ParamByName('NOME').AsString         :=Q1.FieldByName('NOME').AsString;
      //Q2.ParamByName('DOCUMENTO').AsString    :=IfThen(Trim(Q1.FieldByName('DOCUMENTO').AsString)<>'',Q1.FieldByName('DOCUMENTO').AsString,'#');
      Q2.ParamByName('ID_PORTADOR').AsInteger :=Q1.FieldByName('ID_PORTADOR').AsInteger;
      Q2.Open;
      if not Q2.IsEmpty then
      begin
          RXDuplicados.Append;
          RXDuplicadosID.AsInteger        :=Q1.FieldByName('ID_PORTADOR').AsInteger;
          RXDuplicadosNOME.AsString       :=Q1.FieldByName('NOME').AsString;
          RXDuplicadosCODIGO.AsString     :=Q1.FieldByName('CODIGO').AsString;
          RXDuplicadosDOCUMENTO.AsString  :=Q1.FieldByName('DOCUMENTO').AsString;
          RXDuplicadosCRA.AsString        :=Q1.FieldByName('CRA').AsString;
          RXDuplicadosBANCO.AsString      :=Q1.FieldByName('BANCO').AsString;
          RXDuplicadosCONVENIO.AsString   :=Q1.FieldByName('CONVENIO').AsString;
          RXDuplicados.Post;
      end;
      Q1.Next;
  end;

  Result:=RXDuplicados.RecordCount>1;

  RXDuplicados.SortOnFields('CODIGO;NOME');
  {if Imprimir then
  GR.Imprimir(RXDuplicados,'Rela��o de Portadores em Duplicidade',8,True);  }
  Q1.Free;
  Q2.Free;
end;

function Tdm.fProtocolo(vIdAto: String): String;
var
  Q: TFDQuery;
begin
    Q:=TFDQuery.Create(Nil);
    Q.Connection:=conSISTEMA;
    Q.SQL.Add('SELECT PROTOCOLO FROM TITULOS WHERE ID_ATO='+vIdAto);
    Q.Open;
    Result:=Q.FieldByName('PROTOCOLO').AsString;
    Q.Close;
    Q.Free;
end;

procedure Tdm.frptRelAfterPrint(Sender: TfrxReportComponent);
var
  vVariavel: String;
begin
  if Sender.Name = 'rptRelatorio' then
  begin
    //Data
    vVariavel := frptRel.Variables.Variables['Data'];
    dRelDataRelatorio := StrToDate(vVariavel);

    //Hora
    vVariavel := frptRel.Variables.Variables['Hora'];
    tRelHoraRelatorio := StrToTime(vVariavel);
  end;
end;

procedure Tdm.frptRelPreview(Sender: TObject);
begin
  //Nome da Serventia
  frptRel.Variables.Variables['NomeServentia'] := QuotedStr(UpperCase(ServentiaDESCRICAO.AsString));

  //Titulo
  frptRel.Variables.Variables['TituloRelatorio'] := QuotedStr(sRelTituloRelatorio);

  //Subtitulo (Periodo)
  frptRel.Variables.Variables['SubtituloRelatorio'] := QuotedStr(sRelSubtituloRelatorio);

  //Usuario
  frptRel.Variables.Variables['NomeUsuario'] := QuotedStr(UpperCase(Gdm.cUsuariosNOME.AsString));

  //Site Total Sistemas
  frptRel.Variables.Variables['SiteTotalSistemas'] := QuotedStr('www.totalsistemas.info');

  //Detalhe
  frptRel.Variables.Variables['TemDetalhe'] := lRelExibeDetalhe;
end;

{initialization
  rlconsts.setversion(3,72,'B');}

function Tdm.ForcaDaLei(Codigo: String): Boolean;
var
  Q: TFDQuery;
begin
    Q:=TFDQuery.Create(Nil);
    Q.Connection:=conSISTEMA;
    Q.SQL.Add('SELECT FORCA_LEI FROM PORTADORES WHERE CODIGO='+QuotedStr(Codigo));
    Q.Open;
    Result:=Q.FieldByName('FORCA_LEI').AsString='S';
    Q.Close;
    Q.Free;
end;

function Tdm.Especie(Codigo: String): Boolean;
var
  Q: TFDQuery;
begin
    Q:=TFDQuery.Create(Nil);
    Q.Connection:=conSISTEMA;
    Q.SQL.Add('SELECT ESPECIE FROM PORTADORES WHERE CODIGO='+QuotedStr(Codigo));
    Q.Open;
    Result:=Q.FieldByName('ESPECIE').AsString='S';
    Q.Close;
    Q.Free;
end;

procedure Tdm.ExportarArquivoConfirmacao(NomeArqB: String; MemoData: TFDMemTable);
var
  tfArquivoB, tfArquivoC: TextFile;
  sNomeArqC, sLinhaB, sLinhaBAnt, sLinhaC: String;
  qryTit: TFDQuery;
  cCustasC, cCustasD: Real;
  Dia, Mes, Ano: Word;
  v1,v2,v3:String;
begin
  sNomeArqC  := '';
  sLinhaB    := '';
  sLinhaBAnt := '';
  sLinhaC    := '';

  cCustasC := 0;
  cCustasD := 0;

  VerificarExistenciaPastasSistema;

  DecodeDate(Date, Ano, Mes, Dia);

  GR.CriarFDQuery(qryTit,'', conSISTEMAAux);

  //Criacao do nome do arquivo C
  sNomeArqC := 'C' + Copy(NomeArqB, 2, (Length(NomeArqB) - 1));

  //Leitura do arquivo B
  AssignFile(tfArquivoB, dm.ValorParametro(2) + NomeArqB);

  v1:=dm.ValorParametro(2) + NomeArqB;

  AssignFile(tfArquivoC, dm.ValorParametro(3) +
                         IntToStr(Ano) + '\' +
                         GR.Zeros(IntToStr(Mes), 'D', 2) + '\' +
                         GR.Zeros(IntToStr(Dia), 'D', 2) + '\' +
                         sNomeArqC);

  v2:=dm.ValorParametro(3) +
      IntToStr(Ano) + '\' +
      GR.Zeros(IntToStr(Mes), 'D', 2) + '\' +
      GR.Zeros(IntToStr(Dia), 'D', 2) + '\' +
      sNomeArqC;

  Reset(tfArquivoB);

  if not(FileExists(dm.ValorParametro(3) +
                    IntToStr(Ano) + '\' +
                    GR.Zeros(IntToStr(Mes), 'D', 2) + '\' +
                    GR.Zeros(IntToStr(Dia), 'D', 2) + '\' +
                    sNomeArqC)) then
    ReWrite(tfArquivoC);

  Append(tfArquivoC);

  while not Eof(tfArquivoB) do
  begin
    Readln(tfArquivoB, sLinhaB);

    if Trim(sLinhaB) <> '' then
    begin
      case StrToInt(Copy(sLinhaB, 1, 1)) of
        0: { HEADER }
        begin
          sLinhaC := '';
          sLinhaC := Copy(sLinhaB, 1, 52) + 'SDTBFOCRT' + Copy(sLinhaB, 62, 539);
        end;
        1: { REGISTER }
        begin
          sLinhaC := '';

          //Recupera o Titulo relativo ao arquivo B em questao
          qryTit.Close;
          qryTit.SQL.Clear;
          qryTit.SQL.Text := 'SELECT T.*, ' +
                             '       (CASE  ' +
                             '         WHEN DT_PROTOCOLO>=''28.11.2019'' THEN ''S'' ' +
                             '         ELSE ''N'' ' +
                             '       END) as XDIA28 , ' +
                             '       E.COD_SERV_AGREGADA, ' +
                             '       I.DATA AS DATA_IMPORTACAO ' +
                             '  FROM TITULOS T ' +
                             '  LEFT JOIN EXPORTADO E ' +
                             '    ON T.ARQUIVO = E.NOME_ARQUIVO_IMP ' +
                             '   AND T.ARQUIVO_CARTORIO = E.NOME_ARQUIVO_EXP ' +
                             '  LEFT JOIN DEVEDORES D ' +
                             '    ON T.ID_ATO = D.ID_ATO ' +
                             '  LEFT JOIN IMPORTADOS I ' +
                             '    ON T.ARQUIVO = I.ARQUIVO ' +
                             ' WHERE T.ARQUIVO = :ARQUIVO ' +
                             '   AND TRIM(T.AGENCIA_CEDENTE) = :AGENCIA_CEDENTE ' +
                             '   AND TRIM(T.NOSSO_NUMERO) = :NOSSO_NUMERO ' +  //'   AND LPAD(T.NOSSO_NUMERO, 15, ' + QuotedStr('0') + ') = :NOSSO_NUMERO ' +
                             '   AND TRIM(T.NUMERO_TITULO) = :NUMERO_TITULO ' +
                             '   AND D.ORDEM = :ORDEM';
          qryTit.Params.ParamByName('ARQUIVO').Value         := NomeArqB;
          qryTit.Params.ParamByName('AGENCIA_CEDENTE').Value := Trim(Copy(sLinhaB, 5, 15));  //PF.Espaco(Copy(sLinhaB, 5, 15), 'E', 15);
          qryTit.Params.ParamByName('NOSSO_NUMERO').Value    := Trim(Copy(sLinhaB, 199, 15));  //GR.Zeros(Copy(sLinhaB, 199, 15), 'D', 15);
          qryTit.Params.ParamByName('NUMERO_TITULO').Value   := Trim(Copy(sLinhaB, 217, 11));
{          qryTit.Params.ParamByName('NUMERO_TITULO').Value   := IfThen(Trim(Copy(sLinhaB, 2, 3)) = '582',
                                                                       Trim(Copy(sLinhaB, 199, 15)),
                                                                       Trim(Copy(sLinhaB, 217, 11)));  }
          qryTit.Params.ParamByName('ORDEM').Value           := Trim(Copy(sLinhaB, 297, 1));

          qryTit.Open;

          if qryTit.RecordCount > 0 then
          begin
            { Caso tenha havido duplicidade perfeita, o Memorydata vai indicar
              qual arquivo foi recusado  }
            if (PF.Espaco(MemoData.FieldByName('IRREGULARIDADE').AsString, 'E', 2) = '52') and
              (Trim(MemoData.FieldByName('STATUS').AsString) = 'Rejeitado') and
              (Trim(MemoData.FieldByName('MotivoRejeicao').AsString) <> '') and
              (Copy(sLinhaB, 1, (Length(sLinhaB) - 4)) = Copy(sLinhaBAnt, 1, (Length(sLinhaBAnt) - 4))) then //Rejeitado
            begin
              MemoData.Locate('AGENCIA;NOSSO_NUMERO;NUMERO_TITULO;NUMDEVEDOR;SEQUENCIAL_ARQ',
                              VarArrayOf([(Copy(sLinhaB, 5, 15)),
                                          (Copy(sLinhaB, 199, 15)),
                                          (Trim(Copy(sLinhaB, 217, 11))),
{                                          (IfThen(Trim(Copy(sLinhaB, 2, 3)) = '582',
                                                  Trim(Copy(sLinhaB, 199, 15)),
                                                  Trim(Copy(sLinhaB, 217, 11)))),  }
                                          (StrToInt(Copy(sLinhaB, 297, 1))),
                                          Copy(sLinhaB, 597, 4)]),
                              []);

              sLinhaC := Copy(sLinhaB, 1, 445) +
                         '00' +  //Codigo da Serventia Agregada no Distribuidor (CODIGO_DISTRIBUIDOR)
                         '0000000000' +  //Numero do Protocolo do Distribuidor
                         '5' +  //Tipo de Ocorrencia (devolvido por Irregularidade Sem Custas)
                         '00000000' +  //Data do Protocolo do Distribuidor
                         '0000000000' +  //Valor das Custas do Cartorio (se cobradas antecipadamente)
                         Copy(sLinhaB, 477, 1) +
                         FormatDateTime('ddmmyyyy', Date) +  //Data da Ocorrencia
                         PF.Espaco(MemoData.FieldByName('IRREGULARIDADE').AsString, 'E', 2) +  //Codigo da Irregularidade
                         Copy(sLinhaB, 488, 20) +
                         '0000000000' +  //Valor das Custas do Distribuidor (se cobradas antecipadamente) +
                         Copy(sLinhaB, 518, 83);
            end
            else  //Aceito
            begin
              if qryTit.FieldByName('XDIA28').AsString='N' then
              begin
                  if Trim(qryTit.FieldByName('CONVENIO').AsString) = 'S' then
                  begin
                    if PagamentoAntecipadoConvenio(qryTit.FieldByName('CODIGO_APRESENTANTE').AsString) then
                    begin
                      if dm.ValorParametro(83) = 'S' then
                        cCustasC := qryTit.FieldByName('TOTAL').AsFloat
                      else
                        cCustasC := qryTit.FieldByName('TOTAL').AsFloat - StrToFloat(dm.ValorParametro(35));

                      cCustasD := qryTit.FieldByName('DISTRIBUICAO').AsFloat;
                    end
                    else
                      cCustasC := 0;
                  end
                  else
                  begin
                    if dm.ValorParametro(83) = 'S' then
                      cCustasC := qryTit.FieldByName('TOTAL').AsFloat
                    else
                      cCustasC := qryTit.FieldByName('TOTAL').AsFloat - StrToFloat(dm.ValorParametro(35));
                  end;
              end
              else
              begin
                  if Trim(qryTit.FieldByName('POSTECIPADO').AsString) = 'P' then
                  begin
                    cCustasC := 0;
                  end
                  else
                  begin
                    if dm.ValorParametro(83) = 'S' then
                      cCustasC := qryTit.FieldByName('TOTAL').AsFloat
                    else
                      cCustasC := qryTit.FieldByName('TOTAL').AsFloat - StrToFloat(dm.ValorParametro(35));

                    cCustasD := qryTit.FieldByName('DISTRIBUICAO').AsFloat;
                  end;
              end;

              sLinhaC := Copy(sLinhaB, 1, 445) +
                         GR.Zeros(Trim(qryTit.FieldByName('CARTORIO').AsString), 'D', 2) +  //Codigo da Serventia Agregada no Distribuidor (CODIGO_DISTRIBUIDOR)
                         GR.Zeros(Trim(qryTit.FieldByName('PROTOCOLO').AsString), 'D', 10) +  //Numero do Protocolo do Distribuidor
                         ' ' +  //Tipo de Ocorrencia
                         FormatDateTime('ddmmyyyy', dm.ProximoDiaUtil(qryTit.FieldByName('DT_PROTOCOLO').AsDateTime)) +  //Data do Protocolo do Distribuidor
                         PF.Zeros(GR.PegarNumeroTexto(FloatToStrF(cCustasC, ffNumber, 7, 2)), 'D', 10) +  //Valor das Custas do Cartorio (se cobradas antecipadamente)
                         Copy(sLinhaB, 477, 1) +
                         '00000000' +  //Data da Ocorrencia
                         PF.Espaco(qryTit.FieldByName('IRREGULARIDADE').AsString, 'E', 2) +  //Codigo da Irregularidade
                         Copy(sLinhaB, 488, 20) +
                         PF.Zeros(GR.PegarNumeroTexto(FloatToStrF(cCustasD, ffNumber, 7, 2)), 'D', 10) +  //Valor das Custas do Distribuidor (se cobradas antecipadamente)
                         Copy(sLinhaB, 518, 83);
            end;
          end
          else  //Rejeitado
          begin
            MemoData.Locate('AGENCIA;NOSSO_NUMERO;NUMERO_TITULO;NUMDEVEDOR;SEQUENCIAL_ARQ',
                            VarArrayOf([(Copy(sLinhaB, 5, 15)),
                                        (Copy(sLinhaB, 199, 15)),
                                        (Trim(Copy(sLinhaB, 217, 11))),
{                                          (IfThen(Trim(Copy(sLinhaB, 2, 3)) = '582',
                                                  Trim(Copy(sLinhaB, 199, 15)),
                                                  Trim(Copy(sLinhaB, 217, 11)))),  }
                                        (StrToInt(Copy(sLinhaB, 297, 1))),
                                        Copy(sLinhaB, 597, 4)]),
                            []);

            sLinhaC := Copy(sLinhaB, 1, 445) +
                       '00' +  //Codigo da Serventia Agregada no Distribuidor (CODIGO_DISTRIBUIDOR)
                       '0000000000' +  //Numero do Protocolo do Distribuidor
                       '5' +  //Tipo de Ocorrencia (devolvido por Irregularidade Sem Custas)
                       '00000000' +  //Data do Protocolo do Distribuidor
                       '0000000000' +  //Valor das Custas do Cartorio (se cobradas antecipadamente)
                       Copy(sLinhaB, 477, 1) +
                       FormatDateTime('ddmmyyyy', Date) +  //Data da Ocorrencia
                       PF.Espaco(MemoData.FieldByName('IRREGULARIDADE').AsString, 'E', 2) +  //Codigo da Irregularidade
                       Copy(sLinhaB, 488, 20) +
                       '0000000000' +  //Valor das Custas do Distribuidor (se cobradas antecipadamente) +
                       Copy(sLinhaB, 518, 83);
          end;

          sLinhaBAnt := sLinhaB;
        end;
        9: { TRAILLER }
        begin
          sLinhaC := '';
          sLinhaC := sLinhaB;
        end;
      end;

      Writeln(tfArquivoC, sLinhaC);
    end;
  end;

  CloseFile(tfArquivoB);
  CloseFile(tfArquivoC);

  FreeAndNil(qryTit);
end;

procedure Tdm.ExportarCartorios(var MsgRetorno: String; var Erro: Boolean; Reorganizar: Boolean; LstArqB: TListBox);
var
  tfArquivo: TextFile;
  LinhaArq, NomeArq, sCodPortAnt, sNomePortAnt: String;
  cTotalEmolAlocado: Double;
  j,k, m, iTotalAlocado, iIdRem, iIdExp, NumRem: Integer;
  QryAux: TFDQuery;
  lEncerrada: Boolean;
  Dia, Mes, Ano: Word;
begin
  { Essa rotina gera os arquivos D que serao enviados para
    as Serventias Agregadas }

  if Reorganizar then
    ReorganizarTitulosArquivo;

  VerificarExistenciaPastasSistema;

  DecodeDate(Date, Ano, Mes, Dia);

  iIdRem := 0;
  iIdExp := 0;

  LinhaArq  := '';
  NomeArq   := '';
  NomeArqD  := '';

  GR.CriarFDQuery(QryAux,'', conSISTEMA);

  iIdRem := ProximoId('ID_REMESSA',
                      'IDS',
                      1,
                      False);

  iIdExp := ProximoId('ID_EXPORTADO',
                      'IDS',
                      1,
                      False);

  ServentiaAgregada.IndexFieldNames := 'CODIGO_DISTRIBUICAO';

  ServentiaAgregada.First;

  while not ServentiaAgregada.Eof do
  begin
    lEncerrada := False;

    if ServentiaAgregadaQTD_MAX_PREV.AsInteger > 0 then
    begin
      try
        if conSISTEMA.Connected then
          conSISTEMA.StartTransaction;

        j := 0;
        k := 0;

        iTotalAlocado     := 0;
        cTotalEmolAlocado := 0;

        NumRem := 0;

        { Grava Remessa }
        Remessas.Close;
        Remessas.Params[0].AsDate    := Date;
        Remessas.Params[1].AsInteger := -1;
        Remessas.Open;

        if Remessas.IsEmpty then
        begin
          Inc(iIdRem);

          Remessas.Append;
          RemessasID_REMESSA.AsInteger  := iIdRem;
          RemessasID_PORTADOR.AsInteger := -1;
          RemessasDATA.AsDateTime       := Date;
          RemessasNUMERO.AsInteger      := 1;

          NumRem := 1;
        end
        else
        begin
          Remessas.Edit;
          RemessasNUMERO.AsInteger := (RemessasNUMERO.AsInteger + 1);

          NumRem := (RemessasNUMERO.AsInteger + 1);
        end;

        Remessas.Post;

        GR.ExecutarSQLDAC('UPDATE IDS ' +
                         '   SET VALOR = ' + IntToStr(iIdRem) +
                         ' WHERE CAMPO = ' + QuotedStr('ID_REMESSA'), conSISTEMA);

        Remessas.ApplyUpdates(0);

        { Cria e abre o Arquivo D }
        if Trim(ServentiaAgregadaFLG_USAPROTESTO.AsString) = 'S' then  //SA usa sistema Protesto
        begin
          NomeArqD := 'D' +
                      GR.Zeros(IntToStr(DayOf(Date)), 'D', 2) +
                      GR.Zeros(IntToStr(MonthOf(Date)), 'D', 2) +
                      GR.Zeros(ServentiaAgregadaCODIGO_DISTRIBUICAO.AsString, 'D', 2) + '.' +
                      Copy(IntToStr(YearOf(Date)), 3, 2) +
                      IntToStr(NumRem);  //Sequencial da Remessa
        end
        else
        begin
          NomeArqD := 'D' +
                      GR.Zeros(IntToStr(DayOf(Date)), 'D', 2) +
                      GR.Zeros(IntToStr(MonthOf(Date)), 'D', 2) + '.' +
                      Copy(IntToStr(YearOf(Date)), 3, 2) +
                      IntToStr(NumRem);
        end;

        NomeArq := dm.ValorParametro(97) +
                   IntToStr(Ano) + '\' +
                   GR.Zeros(IntToStr(Mes), 'D', 2) + '\' +
                   GR.Zeros(IntToStr(Dia), 'D', 2) + '\' +
                   NomeArqD;

        AssignFile(tfArquivo, NomeArq);

        if not(FileExists(NomeArq)) then
          ReWrite(tfArquivo);

        Append(tfArquivo);

        repeat
          { Grava Header }
          LinhaArq := '';

          if mdLinhaArqD.Locate('SERVENTIA_AGREGADA; ID_REGISTRO; NUM_SEQ_REGISTRO',
                                VarArrayOf([ServentiaAgregadaCODIGO_DISTRIBUICAO.AsInteger,
                                            0,
                                            PF.Espaco(IntToStr(j + 1), 'E', 4)]),
                                [loCaseInsensitive, loPartialKey]) then
          begin
            Inc(j);

            LinhaArq := (mdLinhaArqDID_REGISTRO.AsString +
                         mdLinhaArqDNUM_COD_PORTADOR.AsString +
                         mdLinhaArqDNOME_PORTADOR.AsString +
                         mdLinhaArqDDATA_MOVIMENTO.AsString +
                         mdLinhaArqDID_TRANS_REMET.AsString +
                         mdLinhaArqDID_TRANS_DESTIN.AsString +
                         mdLinhaArqDID_TRANS_TIPO.AsString +
                         GR.Zeros(dm.RemessasNUMERO.AsString, 'D', 6) +
                         GR.Zeros(mdLinhaArqDQTD_REG_REMESSA.AsString, 'D', 4) +
                         GR.Zeros(mdLinhaArqDQTD_TIT_REMESSA.AsString, 'D', 4) +
                         GR.Zeros(mdLinhaArqDQTD_IND_REMESSA.AsString, 'D', 4) +
                         GR.Zeros(mdLinhaArqDQTD_ORIG_REMESSA.AsString, 'D', 4) +
                         mdLinhaArqDID_AGENCIA.AsString +
                         mdLinhaArqDVERSAO_LAYOUT.AsString +
                         mdLinhaArqDCOD_MUNIC_PRACA.AsString +
                         mdLinhaArqDCOMPL_REGISTRO.AsString +
                         GR.Zeros(mdLinhaArqDNUM_SEQ_REGISTRO.AsString, 'D', 4));

            Writeln(tfArquivo, LinhaArq);
          end;

          { Grava Transacao }
          mdArquivoD.SortOnFields('COD_CARTORIO; SEQUENCIAL_REG', True, True);

          mdArquivoD.First;

          while not mdArquivoD.Eof do
          begin
            if (mdArquivoDCOD_CARTORIO.AsInteger = ServentiaAgregadaCODIGO_DISTRIBUICAO.AsInteger) and
              (mdArquivoDSEQUENCIAL_REG.AsInteger = (j + 1)) then
            begin
              LinhaArq := '';

              //Titulo
              for k := 0 to (mdArquivoD.Fields.Count - 1) do
              begin
                //GR.Aviso(mdArquivoD.Fields.Fields[k].FieldName);

                if k = 23 then
                begin
                  case AnsiIndexStr(UpperCase(mdArquivoD.Fields.Fields[k].AsString), ['001', '002', '003', 'J', 'F']) of
                    0, 1, 2: LinhaArq := (LinhaArq + mdArquivoD.Fields.Fields[k].AsString);
                    3: LinhaArq := (LinhaArq + '001');
                    4: LinhaArq := (LinhaArq + '002');
                    else LinhaArq := (LinhaArq + '000');
                  end;
                end
                else if k = 24 then
                  LinhaArq := (LinhaArq + GR.Zeros(mdArquivoD.Fields.Fields[k].AsString, 'D', 14))
                else if k = 30 then
                  LinhaArq := (LinhaArq + GR.Zeros(mdArquivoD.Fields.Fields[k].AsString, 'D', 2))
                else if k = 51 then
                  LinhaArq := (LinhaArq + GR.Zeros(Trim(mdArquivoD.Fields.Fields[k].AsString), 'D', 4))
                else if k = 62 then
                  LinhaArq := (LinhaArq + GR.Zeros(GR.PegarNumeroTexto(mdArquivoD.Fields.Fields[k].AsString), 'D', 14))
                else if not (k in [63, 64, 65, 66, 67, 68]) then
                  LinhaArq := (LinhaArq + mdArquivoD.Fields.Fields[k].AsString);

                if Trim(ServentiaAgregadaFLG_USAPROTESTO.AsString) = 'N' then  //SA nao usa sistema Protesto
                begin
                  if k = 51 then
                    Break;
                end;
              end;

              Writeln(tfArquivo, LinhaArq);

              //Incluir nome do Arquivo de Remessa enviado para Serventia Agregada
              QryAux.Close;
              QryAux.SQL.Clear;
              QryAux.SQL.Text := 'UPDATE TITULOS ' +
                                 '   SET ARQUIVO_CARTORIO = :ARQUIVO_CARTORIO ' +
                                 ' WHERE ID_ATO = :ID_ATO';
              QryAux.Params.ParamByName('ARQUIVO_CARTORIO').Value := NomeArqD;
              QryAux.Params.ParamByName('ID_ATO').Value           := mdArquivoDID_ATO.AsInteger;
              QryAux.ExecSQL;

              //Incrementar Dados na Serventia
              iTotalAlocado     := (iTotalAlocado + 1);

              if PF.RemoverEspeciais(Trim(mdArquivoDTOTAL_CUSTAS.AsString)) = '' then
                cTotalEmolAlocado := cTotalEmolAlocado
              else
                cTotalEmolAlocado := (cTotalEmolAlocado + PF.TextoParaFloat(mdArquivoDTOTAL_CUSTAS.AsString));

//              cTotalEmolAlocado := (cTotalEmolAlocado + PF.TextoParaFloat(mdArquivoDTOTAL_CUSTAS.AsString));

              //Exportado
              Exportado.Close;
              Exportado.Params.ParamByName('NOME_ARQUIVO_IMP').AsString   := mdArquivoDARQUIVO_CRA.AsString;
              Exportado.Params.ParamByName('COD_SERV_AGREGADA').AsInteger := ServentiaAgregadaCODIGO_DISTRIBUICAO.AsInteger;
              Exportado.Params.ParamByName('NOME_ARQUIVO_EXP').AsString   := NomeArqD;
              Exportado.Open;

              if Exportado.IsEmpty then
              begin
                Inc(iIdExp);

                GR.ExecutarSQLDAC('UPDATE IDS ' +
                                 '   SET VALOR = ' + IntToStr(iIdExp) +
                                 ' WHERE CAMPO = ' + QuotedStr('ID_EXPORTADO'), conSISTEMA);

                QryAux.Close;
                QryAux.SQL.Clear;

                QryAux.SQL.Text := 'INSERT INTO EXPORTADO (ID_EXPORTADO, NOME_ARQUIVO_IMP, ' +
                                   '                       COD_SERV_AGREGADA, NOME_ARQUIVO_EXP, ' +
                                   '                       DATA_EXPORTACAO, QTD_TITULOS) ' +
                                   '               VALUES (:ID_EXPORTADO, :NOME_ARQUIVO_IMP, ' +
                                   '                       :COD_SERV_AGREGADA, :NOME_ARQUIVO_EXP, ' +
                                   '                       :DATA_EXPORTACAO, :QTD_TITULOS)';

                QryAux.Params.ParamByName('ID_EXPORTADO').Value      := iIdExp;
                QryAux.Params.ParamByName('NOME_ARQUIVO_IMP').Value  := mdArquivoDARQUIVO_CRA.AsString;
                QryAux.Params.ParamByName('COD_SERV_AGREGADA').Value := ServentiaAgregadaCODIGO_DISTRIBUICAO.AsInteger;
                QryAux.Params.ParamByName('NOME_ARQUIVO_EXP').Value  := NomeArqD;
                QryAux.Params.ParamByName('DATA_EXPORTACAO').Value   := Date;
                QryAux.Params.ParamByName('QTD_TITULOS').Value       := 1;
                QryAux.ExecSQL;
              end
              else
              begin
                QryAux.Close;
                QryAux.SQL.Clear;

                QryAux.SQL.Text := 'UPDATE EXPORTADO ' +
                                   '   SET QTD_TITULOS = :QTD_TITULOS ' +
                                   ' WHERE ID_EXPORTADO = :ID_EXPORTADO';

                QryAux.Params.ParamByName('QTD_TITULOS').Value  := (ExportadoQTD_TITULOS.AsInteger + 1);
                QryAux.Params.ParamByName('ID_EXPORTADO').Value := ExportadoID_EXPORTADO.AsInteger;
                QryAux.ExecSQL;
              end;

              Inc(j);
            end;

            mdArquivoD.Next;
          end;

          { Grava Trailler }
          LinhaArq := '';

          if mdLinhaArqD.Locate('SERVENTIA_AGREGADA; ID_REGISTRO; NUM_SEQ_REGISTRO',
                                VarArrayOf([ServentiaAgregadaCODIGO_DISTRIBUICAO.AsInteger,
                                            9,
                                            PF.Espaco(IntToStr(j + 1), 'E', 4)]),
                                [loCaseInsensitive, loPartialKey]) then
          begin
            Inc(j);

            LinhaArq := (mdLinhaArqDID_REGISTRO.AsString +
                         mdLinhaArqDNUM_COD_PORTADOR.AsString +
                         mdLinhaArqDNOME_PORTADOR.AsString +
                         mdLinhaArqDDATA_MOVIMENTO.AsString +
                         mdLinhaArqDSOMAT_QTD_REMESSA.AsString +
                         mdLinhaArqDSOMAT_VAL_REMESSA.AsString +
                         mdLinhaArqDCOMPL_REGISTRO.AsString +
                         GR.Zeros(mdLinhaArqDNUM_SEQ_REGISTRO.AsString, 'D', 4));

            Writeln(tfArquivo, LinhaArq);
          end;

          lEncerrada := (ServentiaAgregadaQTD_MAX_PREV.AsInteger = j);
        until lEncerrada;

        { Fecha o Arquivo D }
        CloseFile(tfArquivo);

        { Atualiza Totais para a Serventia Agregada }
{        QryAux.Close;
        QryAux.SQL.Clear;

        QryAux.SQL.Text := 'UPDATE SERVENTIA_AGREGADA ' +
                           '   SET QTD_ULTIMO_SALDO   = :QTD_ULTIMO_SALDO, ' +
                           '       VALOR_ULTIMO_SALDO = :VALOR_ULTIMO_SALDO, ' +
                           '       DATA_ULTIMO_SALDO  = :DATA_ULTIMO_SALDO ' +
                           ' WHERE ID_SERVENTIA_AGREGADA = :ID_SERVENTIA_AGREGADA';

        QryAux.Params.ParamByName('QTD_ULTIMO_SALDO').Value      := iTotalAlocado;
        QryAux.Params.ParamByName('VALOR_ULTIMO_SALDO').Value    := cTotalEmolAlocado;
        QryAux.Params.ParamByName('DATA_ULTIMO_SALDO').Value     := Date;
        QryAux.Params.ParamByName('ID_SERVENTIA_AGREGADA').Value := ServentiaAgregadaID_SERVENTIA_AGREGADA.AsInteger;
        QryAux.ExecSQL;  }

        if Trim(MsgRetorno) = '' then
          MsgRetorno := 'T�tulos exportados e arquivo ' + NomeArqD + ' gerado com sucesso!'
        else
          MsgRetorno := MsgRetorno + #13#10 + 'T�tulos exportados e arquivo ' + NomeArqD + ' gerado com sucesso!';

        if conSISTEMA.InTransaction then
          conSISTEMA.Commit;
      except
        on E: Exception do
        begin
          if conSISTEMA.InTransaction then
            conSISTEMA.Rollback;

          if FileExists(NomeArq) then
            DeleteFile(PChar(NomeArq));

          Erro := True;
          MsgRetorno := 'Erro na gera��o do(s) arquivo(s) de Remessa D. (' + E.Message + ')';
        end;
      end;
    end;

    ServentiaAgregada.Next;
  end;

  { Move o arquivo de Remessa D da raiz da pasta Remessas para a pasta do dia }
  for m := 0 to LstArqB.Items.Count - 1 do
  begin
    if FileExists(PChar(dm.ValorParametro(2) +
                        IntToStr(Ano) + '\' +
                        GR.Zeros(IntToStr(Mes), 'D', 2) + '\' +
                        GR.Zeros(IntToStr(Dia), 'D', 2) + '\' +
                        LstArqB.Items.Strings[m])) then
      DeleteFile(PChar(dm.ValorParametro(2) +
                       IntToStr(Ano) + '\' +
                       GR.Zeros(IntToStr(Mes), 'D', 2) + '\' +
                       GR.Zeros(IntToStr(Dia), 'D', 2) + '\' +
                       LstArqB.Items.Strings[m]));

    if CopyFile(PChar(dm.ValorParametro(2) + LstArqB.Items.Strings[m]),
                PChar(dm.ValorParametro(2) +
                      IntToStr(Ano) + '\' +
                      GR.Zeros(IntToStr(Mes), 'D', 2) + '\' +
                      GR.Zeros(IntToStr(Dia), 'D', 2) + '\' +
                      LstArqB.Items.Strings[m]),
                True) then
      DeleteFile(PChar(dm.ValorParametro(2) + LstArqB.Items.Strings[m]));
  end;

  FreeAndNil(QryAux);
end;

procedure Tdm.FeriadosCalcFields(DataSet: TDataSet);
begin
  case Feriados.FieldByName('MES').AsInteger of
    1: Feriados.FieldByName('DESCR_MES').AsString := 'JANEIRO';
    2: Feriados.FieldByName('DESCR_MES').AsString := 'FEVEREIRO';
    3: Feriados.FieldByName('DESCR_MES').AsString := 'MAR�O';
    4: Feriados.FieldByName('DESCR_MES').AsString := 'ABRIL';
    5: Feriados.FieldByName('DESCR_MES').AsString := 'MAIO';
    6: Feriados.FieldByName('DESCR_MES').AsString := 'JUNHO';
    7: Feriados.FieldByName('DESCR_MES').AsString := 'JULHO';
    8: Feriados.FieldByName('DESCR_MES').AsString := 'AGOSTO';
    9: Feriados.FieldByName('DESCR_MES').AsString := 'SETEMBRO';
    10: Feriados.FieldByName('DESCR_MES').AsString := 'OUTUBRO';
    11: Feriados.FieldByName('DESCR_MES').AsString := 'NOVEMBRO';
    12: Feriados.FieldByName('DESCR_MES').AsString := 'DEZEMBRO';
    else Feriados.FieldByName('DESCR_MES').AsString := '';
  end;
end;

procedure Tdm.ServentiaAgregadaVALOR_ULTIMO_SALDOGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

end.
