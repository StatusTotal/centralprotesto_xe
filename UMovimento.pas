unit UMovimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Mask, sMaskEdit, sCustomComboEdit, sToolEdit,
  sDBDateEdit, Buttons, sBitBtn, ExtCtrls, sPanel, DBCtrls, sDBEdit;

type
  TFMovimento = class(TForm)
    sPanel1: TsPanel;
    btSalvar: TsBitBtn;
    btCancelar: TsBitBtn;
    edData: TsDBDateEdit;
    dsMovimento: TDataSource;
    edHora: TsDBEdit;
    sDBDateEdit1: TsDBDateEdit;
    procedure btSalvarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMovimento: TFMovimento;

implementation

uses UGDM, UDM;

{$R *.dfm}

procedure TFMovimento.btSalvarClick(Sender: TObject);
begin
  dm.Movimento.Post;
  dm.Movimento.ApplyUpdates(0);
  Close;
end;

procedure TFMovimento.btCancelarClick(Sender: TObject);
begin
  dm.Movimento.Cancel;
  Close;
end;

procedure TFMovimento.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if dm.Movimento.State=dsEdit then dm.Movimento.Cancel;
end;

end.
