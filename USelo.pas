unit USelo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, Buttons, sBitBtn, ExtCtrls, sPanel;

type
  TFSelo = class(TForm)
    sPanel1: TsPanel;
    edSelo: TsEdit;
    btSalvar: TsBitBtn;
    btCancelar: TsBitBtn;
    procedure btSalvarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSelo: TFSelo;

implementation

uses UDM;

{$R *.dfm}

procedure TFSelo.btSalvarClick(Sender: TObject);
begin
  Close;
end;

procedure TFSelo.btCancelarClick(Sender: TObject);
begin
  edSelo.Clear;
  Close;
end;

procedure TFSelo.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then btCancelarClick(Sender);
end;

end.
