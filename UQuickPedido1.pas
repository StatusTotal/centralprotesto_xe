unit UQuickPedido1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls;

type
  TFQuickPedido1 = class(TForm)
    Pedido1: TQuickRep;
    Cabecalho: TQRBand;
    txNomeServentia: TQRDBText;
    txEndereco: TQRDBText;
    lbTitular1: TQRLabel;
    Detalhe: TQRBand;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    qrDocumento: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    QRLabel7: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText6: TQRDBText;
    lbPedido1: TQRLabel;
    lbRecibo1: TQRLabel;
    Tabelas: TQRSubDetail;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    lbTabela: TQRLabel;
    QRBand1: TQRBand;
    QRLabel3: TQRLabel;
    QRLabel9: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    ChildBand1: TQRChildBand;
    qrAviso: TQRLabel;
    QRLabel16: TQRLabel;
    lbCidade1: TQRLabel;
    QRLabel32: TQRLabel;
    qrEmolumentos1: TQRLabel;
    qrFetj1: TQRLabel;
    qrFundperj1: TQRLabel;
    qrFunperj1: TQRLabel;
    qrApontamento1: TQRLabel;
    qrTotal1: TQRLabel;
    QRLabel1: TQRLabel;
    qrCobranca1: TQRLabel;
    QRLabel17: TQRLabel;
    qrFunarpen1: TQRLabel;
    QRLabel30: TQRLabel;
    qrPmcmv1: TQRLabel;
    lbPedido2: TQRLabel;
    lbRecibo2: TQRLabel;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    lbTitular2: TQRLabel;
    QRShape5: TQRShape;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRShape6: TQRShape;
    QRLabel22: TQRLabel;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRLabel23: TQRLabel;
    QRDBText22: TQRDBText;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    lbCidade2: TQRLabel;
    qrEmolumentos2: TQRLabel;
    qrFetj2: TQRLabel;
    qrFundperj2: TQRLabel;
    qrFunperj2: TQRLabel;
    qrApontamento2: TQRLabel;
    qrTotal2: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    lbApontamento: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel28: TQRLabel;
    qrCobranca2: TQRLabel;
    QRLabel31: TQRLabel;
    qrFunarpen2: TQRLabel;
    QRLabel33: TQRLabel;
    qrPmcmv2: TQRLabel;
    lbCustas: TQRLabel;
    M: TQRMemo;
    QRShape9: TQRShape;
    QRLabel34: TQRLabel;
    QRImage1: TQRImage;
    QRLabel35: TQRLabel;
    qrMutua1: TQRLabel;
    QRLabel37: TQRLabel;
    qrAcoterj1: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel38: TQRLabel;
    qrAcoterj2: TQRLabel;
    qrMutua2: TQRLabel;
    ChildBand2: TQRChildBand;
    Band3Via: TQRChildBand;
    QRDBText7: TQRDBText;
    QRShape10: TQRShape;
    QRLabel39: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRDBText14: TQRDBText;
    QRLabel42: TQRLabel;
    QRDBText15: TQRDBText;
    QRLabel43: TQRLabel;
    QRDBText16: TQRDBText;
    lbPedido3: TQRLabel;
    lbRecibo3: TQRLabel;
    QRLabel46: TQRLabel;
    qrTotal3: TQRLabel;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    lbTelefone1: TQRLabel;
    lbTelefone2: TQRLabel;
    QRLabel5: TQRLabel;
    qrIss1: TQRLabel;
    QRLabel44: TQRLabel;
    qrIss2: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure TabelasBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure DetalheBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickPedido1: TFQuickPedido1;

implementation

uses UDM, UPF, DB, UGeral;

{$R *.dfm}

procedure TFQuickPedido1.FormCreate(Sender: TObject);
var
  Cobranca: String;
begin
  if dm.CertidoesCOBRANCA.AsString='CC' then Cobranca:='Com Cobran�a';
  if dm.CertidoesCOBRANCA.AsString='JG' then Cobranca:='Justi�a Gratuita';
  if dm.CertidoesCOBRANCA.AsString='SC' then Cobranca:='Sem Cobran�a';
  if dm.CertidoesCOBRANCA.AsString='NH' then Cobranca:='NIHIL';
  if dm.CertidoesCOBRANCA.AsString='FL' then Cobranca:='For�a da Lei';

  qrCobranca1.Caption :=Cobranca;
  lbTitular1.Caption  :=dm.ValorParametro(7)+': '+dm.ServentiaTABELIAO.AsString;
  lbCidade1.Caption   :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',dm.CertidoesDT_PEDIDO.AsDateTime);
  lbTelefone1.Caption :=GR.FormatarTelefone(dm.ServentiaTELEFONE.AsString);
  lbTelefone2.Caption :=GR.FormatarTelefone(dm.ServentiaTELEFONE.AsString);

  if (dm.vReciboExcedente) and (dm.CertidoesEXRECIBO.AsInteger<>0) then
    lbRecibo1.Caption:='RECIBO N� '+dm.CertidoesEXRECIBO.AsString+' - '+Cobranca
      else lbRecibo1.Caption:='RECIBO N� '+dm.CertidoesRECIBO.AsString+' - '+Cobranca;

  if dm.CertidoesTIPO_CERTIDAO.AsString='F' then
    lbPedido1.Caption:='Pedido de Certid�o: '+dm.CertidoesNomeCertidao.AsString+' - Protocolo: '+IntToStr(dm.CampoTitulos('PROTOCOLO',dm.CertidoesID_ATO.AsInteger))+
                       ' ('+dm.CampoTitulos('STATUS',dm.CertidoesID_ATO.AsInteger)+')'
      else
        if dm.ServentiaCODIGO.AsInteger = 1554 then
           lbPedido1.Caption:='Pedido de Certid�o: '+dm.CertidoesNomeCertidao.AsString+' - Per�odo: '+dm.CertidoesANOS.AsString+' anos'+GR.iif(dm.CertidoesID_ATO.IsNull,'','- Protocolo: '+IntToStr(dm.CampoTitulos('PROTOCOLO',dm.CertidoesID_ATO.AsInteger)))
           else lbPedido1.Caption:='Pedido de Certid�o: '+dm.CertidoesNomeCertidao.AsString+' - Per�odo: '+dm.CertidoesANOS.AsString+' anos';

  lbCidade2.Caption       :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy',Now)+', __________________________ (Apresentante).';
  lbPedido2.Caption       :=lbPedido1.Caption;
  lbRecibo2.Caption       :=lbRecibo1.Caption;
  lbTitular2.Caption      :=lbTitular1.Caption;
  qrCobranca2.Caption     :=qrCobranca1.Caption;
  qrEmolumentos1.Caption  :=FloatToStrF(dm.vEmolumentos,ffNumber,7,2);
  qrFetj1.Caption         :=FloatToStrF(dm.vFetj,ffNumber,7,2);
  qrFundperj1.Caption     :=FloatToStrF(dm.vFundperj,ffNumber,7,2);
  qrFunperj1.Caption      :=FloatToStrF(dm.vFunperj,ffNumber,7,2);
  qrFunarpen1.Caption     :=FloatToStrF(dm.vFunarpen,ffNumber,7,2);
  qrPmcmv1.Caption        :=FloatToStrF(dm.vPmcmv,ffNumber,7,2);
  qrIss1.Caption          :=FloatToStrF(dm.vIss,ffNumber,7,2);
  qrApontamento1.Caption  :=FloatToStrF(dm.vApontamento,ffNumber,7,2);
  qrMutua1.Caption        :=FloatToStrF(dm.vMutua,ffNumber,7,2);
  qrAcoterj1.Caption      :=FloatToStrF(dm.vAcoterj,ffNumber,7,2);
  qrTotal1.Caption        :=FloatToStrF(dm.vTotal,ffNumber,7,2);
  qrEmolumentos2.Caption  :=qrEmolumentos1.Caption;
  qrFetj2.Caption         :=qrFetj1.Caption;
  qrFundperj2.Caption     :=qrFundperj1.Caption;
  qrFunperj2.Caption      :=qrFunperj1.Caption;
  qrFunarpen2.Caption     :=qrFunarpen1.Caption;
  qrPmcmv2.Caption        :=qrPmcmv1.Caption;
  qrIss2.Caption          :=qrIss1.Caption;
  qrApontamento2.Caption  :=qrApontamento1.Caption;
  qrMutua2.Caption        :=qrMutua1.Caption;
  qrAcoterj2.Caption      :=qrAcoterj1.Caption;
  qrTotal2.Caption        :=qrTotal1.Caption;
  qrTotal3.Caption        :=qrTotal1.Caption;
  lbPedido3.Caption       :=lbPedido1.Caption;
  lbRecibo3.Caption       :=lbRecibo1.Caption;

  if dm.ServentiaCODIGO.AsInteger=2358 then
  Band3Via.Enabled:=True;

  if dm.RxCustas.IsEmpty then
  begin
      Tabelas.Enabled:=False;
      lbCustas.Enabled:=False;
  end;

  if dm.vAviso<>'' then
  qrAviso.Caption:=dm.vAviso;

  Pedido1.Preview;
end;

procedure TFQuickPedido1.TabelasBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  lbTabela.Caption:=dm.RxCustasTABELA.AsString+' '+PF.Espaco(dm.RxCustasITEM.AsString,'D',2)+' '+dm.RxCustasSUBITEM.AsString;
end;

procedure TFQuickPedido1.DetalheBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if Length(dm.CertidoesCPF_CNPJ_REQUERIDO.AsString)=14 then
    qrDocumento.Caption:='CNPJ:'
      else qrDocumento.Caption:='CPF:';
end;

procedure TFQuickPedido1.QRBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  M.Lines.Clear;
  dm.RxCustas.First;
  while not dm.RxCustas.Eof do
  begin
      M.Lines.Add(dm.RxCustasTABELA.AsString+'.'+dm.RxCustasITEM.AsString+'.'+dm.RxCustasSUBITEM.AsString+'   '+dm.RxCustasDESCRICAO.AsString+' x '+dm.RxCustasQTD.AsString+Format('%-1s %7.2f',[' =',dm.RxCustasTOTAL.AsFloat]));
      dm.RxCustas.Next;
  end;
end;

end.
