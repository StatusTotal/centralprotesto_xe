unit UQuickReciboApontamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, StdCtrls, ComCtrls, QuickRpt, DB, DBClient, SimpleDS,
  ExtCtrls, FMTBcd, SqlExpr, RxMemDS, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFQuickReciboApontamento = class(TForm)
    Relatorio: TQuickRep;
    Header: TQRBand;
    txNomeServentia: TQRDBText;
    txEndereco: TQRDBText;
    txEmail: TQRDBText;
    txCNPJ: TQRDBText;
    txSite: TQRDBText;
    lbTelefone: TQRLabel;
    GrupoHeader: TQRGroup;
    Detail: TQRBand;
    RX: TRxMemoryData;
    RXDT_PROTOCOLO: TDateField;
    RXAPRESENTANTE: TStringField;
    RXCODIGO_APRESENTANTE: TStringField;
    RXFAIXA: TStringField;
    RXPROTOCOLOS: TMemoField;
    RXEMOLUMENTOS: TFloatField;
    RXFETJ: TFloatField;
    RXFUNDPERJ: TFloatField;
    RXFUNPERJ: TFloatField;
    RXFUNARPEN: TFloatField;
    RXPMCMV: TFloatField;
    RXMUTUA: TFloatField;
    RXACOTERJ: TFloatField;
    RXDISTRIBUICAO: TFloatField;
    RXTOTAL: TFloatField;
    RXQTD: TIntegerField;
    ChildBand1: TQRChildBand;
    qrFundos1: TQRLabel;
    qrFundos2: TQRLabel;
    lbPortador: TQRLabel;
    qrQtd: TQRLabel;
    GrupoFooter: TQRBand;
    QRBand1: TQRBand;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRShape1: TQRShape;
    qrCidade: TQRLabel;
    qrAssinatura: TQRLabel;
    ChildBand2: TQRChildBand;
    lbRecibo: TQRLabel;
    QRDBRichText1: TQRDBRichText;
    RXISS: TFloatField;
    qrySoma: TFDQuery;
    qryProtocolos: TFDQuery;
    qrySomaTOTAL: TFloatField;
    qryProtocolosID_ATO: TIntegerField;
    qryProtocolosCODIGO: TIntegerField;
    qryProtocolosRECIBO: TIntegerField;
    qryProtocolosDT_PROTOCOLO: TDateField;
    qryProtocolosPROTOCOLO: TIntegerField;
    qryProtocolosCOBRANCA: TStringField;
    qryProtocolosEMOLUMENTOS: TFloatField;
    qryProtocolosFETJ: TFloatField;
    qryProtocolosFUNDPERJ: TFloatField;
    qryProtocolosFUNPERJ: TFloatField;
    qryProtocolosFUNARPEN: TFloatField;
    qryProtocolosPMCMV: TFloatField;
    qryProtocolosISS: TFloatField;
    qryProtocolosMUTUA: TFloatField;
    qryProtocolosACOTERJ: TFloatField;
    qryProtocolosDISTRIBUICAO: TFloatField;
    qryProtocolosTOTAL: TFloatField;
    qryProtocolosCONVENIO: TStringField;
    qryProtocolosCODIGO_APRESENTANTE: TStringField;
    qryProtocolosAPRESENTANTE: TStringField;
    qryProtocolosFaixa: TStringField;
    procedure GrupoHeaderBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure RelatorioEndPage(Sender: TCustomQuickRep);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure qryProtocolosCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    vGerarRecibo: Boolean;
    vReciboInicial,vReciboFinal: Integer;
  end;

var
  FQuickReciboApontamento: TFQuickReciboApontamento;

implementation

uses UDM, UGeral, UAssinatura, UPF;

{$R *.dfm}

procedure TFQuickReciboApontamento.GrupoHeaderBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  qrySoma.Close;
  qrySoma.Params[0].AsDate:=RXDT_PROTOCOLO.AsDateTime;
  qrySoma.Params[1].AsString:=RXCODIGO_APRESENTANTE.AsString;
  qrySoma.Open;

  lbPortador.Caption:='Recebemos de '+RXAPRESENTANTE.AsString+', a import�ncia de: R$ '+FloatToStrF(qrySomaTOTAL.AsFloat,ffNumber,7,2)+'. '+
                      'Referente a entrada de:';
end;

procedure TFQuickReciboApontamento.ChildBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  qrFundos1.Caption:='Emolumentos: R$'+FloatToStrF(RXEMOLUMENTOS.AsFloat,ffNumber,7,2)+' / '+
                     'Fetj: R$'+FloatToStrF(RXFETJ.AsFloat,ffNumber,7,2)+' / '+
                     'Fundperj: R$'+FloatToStrF(RXFUNDPERJ.AsFloat,ffNumber,7,2)+' / '+
                     'Funperj: R$'+FloatToStrF(RXFUNPERJ.AsFloat,ffNumber,7,2)+' / '+
                     'Funarpen: R$'+FloatToStrF(RXFUNARPEN.AsFloat,ffNumber,7,2)+' / '+
                     'Iss: R$'+FloatToStrF(RXISS.AsFloat,ffNumber,7,2);

  qrFundos2.Caption:='Pmcmv: R$'+FloatToStrF(RXPMCMV.AsFloat,ffNumber,7,2)+' / '+
                     'Mutua: R$'+FloatToStrF(RXMUTUA.AsFloat,ffNumber,7,2)+' / '+
                     'Acoterj: R$'+FloatToStrF(RXACOTERJ.AsFloat,ffNumber,7,2)+' / '+
                     'Total: R$'+FloatToStrF(RXTOTAL.AsFloat,ffNumber,7,2);

  qrQtd.Caption    :='Quantidade: '+RXQTD.AsString;
end;

procedure TFQuickReciboApontamento.QRBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  qrCidade.Caption      :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',RXDT_PROTOCOLO.AsDateTime);
  qrAssinatura.Caption  :=dm.vAssinatura;
end;

procedure TFQuickReciboApontamento.qryProtocolosCalcFields(DataSet: TDataSet);
begin
  if qryProtocolosCODIGO.AsInteger=4049 then qryProtocolosFaixa.AsString:='A';
  if qryProtocolosCODIGO.AsInteger=4050 then qryProtocolosFaixa.AsString:='B';
  if qryProtocolosCODIGO.AsInteger=4051 then qryProtocolosFaixa.AsString:='C';
  if qryProtocolosCODIGO.AsInteger=4052 then qryProtocolosFaixa.AsString:='D';
  if qryProtocolosCODIGO.AsInteger=4053 then qryProtocolosFaixa.AsString:='E';
  if qryProtocolosCODIGO.AsInteger=4054 then qryProtocolosFaixa.AsString:='F';
  if qryProtocolosCODIGO.AsInteger=4055 then qryProtocolosFaixa.AsString:='G';
  if qryProtocolosCODIGO.AsInteger=4056 then qryProtocolosFaixa.AsString:='H';
  if qryProtocolosCODIGO.AsInteger=4057 then qryProtocolosFaixa.AsString:='I';
  if qryProtocolosCODIGO.AsInteger=4058 then qryProtocolosFaixa.AsString:='J';
  if qryProtocolosCODIGO.AsInteger=4059 then qryProtocolosFaixa.AsString:='K';
  if qryProtocolosCODIGO.AsInteger=4060 then qryProtocolosFaixa.AsString:='L';
  if qryProtocolosCODIGO.AsInteger=4061 then qryProtocolosFaixa.AsString:='M';
  if qryProtocolosCODIGO.AsInteger=4062 then qryProtocolosFaixa.AsString:='N';
  if qryProtocolosCODIGO.AsInteger=4063 then qryProtocolosFaixa.AsString:='O';
  if qryProtocolosCODIGO.AsInteger=4064 then qryProtocolosFaixa.AsString:='P';
  if qryProtocolosCODIGO.AsInteger=4065 then qryProtocolosFaixa.AsString:='Q';
  if qryProtocolosCODIGO.AsInteger=4066 then qryProtocolosFaixa.AsString:='R';
  if qryProtocolosCODIGO.AsInteger=4067 then qryProtocolosFaixa.AsString:='S';
  if qryProtocolosCODIGO.AsInteger=4068 then qryProtocolosFaixa.AsString:='T';
  if qryProtocolosCODIGO.AsInteger=4069 then qryProtocolosFaixa.AsString:='U';
  if qryProtocolosCODIGO.AsInteger=4070 then qryProtocolosFaixa.AsString:='V';
  if qryProtocolosCODIGO.AsInteger=4071 then qryProtocolosFaixa.AsString:='X';
  if qryProtocolosCODIGO.AsInteger=4072 then qryProtocolosFaixa.AsString:='Y';
  if qryProtocolosCODIGO.AsInteger=4073 then qryProtocolosFaixa.AsString:='W';
  if qryProtocolosCODIGO.AsInteger=4074 then qryProtocolosFaixa.AsString:='Z';
end;

procedure TFQuickReciboApontamento.FormCreate(Sender: TObject);
begin
  GR.CriarForm(TFAssinatura,FAssinatura);
  lbTelefone.Caption:=GR.FormatarTelefone(dm.ServentiaTELEFONE.AsString);
end;

procedure TFQuickReciboApontamento.RelatorioEndPage(
  Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickReciboApontamento.ChildBand2BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  lbRecibo.Caption:='Recibo(s) N�(s): '+IntToStr(vReciboInicial)+' a '+IntToStr(vReciboFinal)+'.';
end;

end.
