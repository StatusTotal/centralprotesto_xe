unit UReciboPagamentoConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FMTBcd, StdCtrls, Buttons, sBitBtn, sEdit, sGroupBox, ExtCtrls,
  sPanel, DB, SqlExpr, DBClient, SimpleDS;

type
  TFReciboPagamentoConvenio = class(TForm)
    P1: TsPanel;
    sGroupBox1: TsGroupBox;
    edProtocolo: TsEdit;
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    procedure btVisualizarClick(Sender: TObject);
    procedure edProtocoloKeyPress(Sender: TObject; var Key: Char);
    procedure btSairClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FReciboPagamentoConvenio: TFReciboPagamentoConvenio;

implementation

uses UQuickPagamento1, UDM, UPF, UGeral, UQuickReciboPagamento;

{$R *.dfm}

procedure TFReciboPagamentoConvenio.btVisualizarClick(Sender: TObject);
begin
  if edProtocolo.Text='' then
  begin
      edProtocolo.SetFocus;
      Exit;
  end;

  Application.CreateForm(TFQuickReciboPagamento,FQuickReciboPagamento);
  with FQuickReciboPagamento do
  begin
      Protocolo.Close;
      Protocolo.Params[0].AsInteger:=StrToInt(edProtocolo.Text);
      Protocolo.Open;

      if Protocolo.IsEmpty then
      begin
          GR.Aviso('PROTOCOLO N�O ENCONTRADO!');
          edProtocolo.SetFocus;
          Exit;
          FQuickReciboPagamento.Free;
      end;

      if ProtocoloRECIBO_PAGAMENTO.AsInteger=0 then
        if GR.Pergunta('GERAR N� DE RECIBO') then
        begin
            Protocolo.Edit;
            if StrToInt(dm.ValorParametro(14))<>0 then
            begin
                ProtocoloRECIBO_PAGAMENTO.AsInteger:=StrToInt(dm.ValorParametro(14));
                dm.AtualizarParametro(14,IntToStr(ProtocoloRECIBO_PAGAMENTO.AsInteger+1));
            end
            else
            begin
                ProtocoloRECIBO_PAGAMENTO.AsInteger:=StrToInt(dm.ValorParametro(11));
                dm.AtualizarParametro(11,IntToStr(ProtocoloRECIBO_PAGAMENTO.AsInteger+1));
            end;
            Protocolo.Post;
            Protocolo.ApplyUpdates(0);
        end;

      RE.Lines.Add('Declaramos, que o documento de d�vida, com as caracter�sticas abaixo discriminadas, foi QUITADO, de acordo com o que '+
                   'preceitua (Lei 9492/97, CAP. VII, Art. 16), conforme requerido.');

      if dm.ServentiaCODIGO.AsInteger=1430 then
        RE.Text:=PF.JstParagrafo(RE.Text,78)
          else PF.JustificarRichEdit(RE,True);

      PF.CarregarCustas(ProtocoloID_ATO.AsInteger);
      dm.RxCustas.First;
      while not dm.RxCustas.Eof do
      begin
          M.Lines.Add(dm.RxCustasTABELA.AsString+'.'+dm.RxCustasITEM.AsString+'.'+dm.RxCustasSUBITEM.AsString+'   '+dm.RxCustasDESCRICAO.AsString+' x '+dm.RxCustasQTD.AsString+Format('%-1s %7.2f',[' =',dm.RxCustasTOTAL.AsFloat]));
          dm.RxCustas.Next;
      end;

      M.Lines.Add(Format('%-1s %7.2f',['Emolumentos =',ProtocoloEMOLUMENTOS.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['FETJ =',ProtocoloFETJ.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Fundperj =',ProtocoloFUNDPERJ.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Funperj =',ProtocoloFUNPERJ.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Funarpen =',ProtocoloFUNARPEN.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Pmcmv =',ProtocoloPMCMV.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Iss =',ProtocoloISS.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['M�tua =',ProtocoloMUTUA.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Acoterj =',ProtocoloACOTERJ.AsFloat]));
      M.Lines.Add(Format('%-1s %7.2f',['Total =',ProtocoloTOTAL.AsFloat-ProtocoloTARIFA_BANCARIA.AsFloat]));

      if ProtocoloRECIBO_PAGAMENTO.AsInteger<>0 then
      begin
          qrRecibo.Enabled  :=True;
          lbRecibo.Enabled  :=True;
          lbRecibo.Caption  :=ProtocoloRECIBO_PAGAMENTO.AsString;
      end
      else
      begin
          qrRecibo.Enabled  :=False;
          lbRecibo.Enabled  :=False;
      end;

      lbProtocolo.Caption   :=ProtocoloPROTOCOLO.AsString;
      lbDevedor.Caption     :=ProtocoloDEVEDOR.AsString;
      lbDocumento.Caption   :=GR.FormatarCPFCNPJ(ProtocoloCPF_CNPJ_DEVEDOR.AsString,'S');
      lbTitulo.Caption      :=ProtocoloNUMERO_TITULO.AsString;
      lbSacador.Caption     :=ProtocoloSACADOR.AsString;
      lbCedente.Caption     :=ProtocoloCEDENTE.AsString;
      lbValor.Caption       :='R$ '+FloatToStrF(ProtocoloVALOR_TITULO.AsFloat,ffNumber,7,2);
      lbVencimento.Caption  :=PF.FormatarData(ProtocoloDT_VENCIMENTO.AsDateTime,'N');
      lbApresentante.Caption:=ProtocoloAPRESENTANTE.AsString;

      Desistencia.Preview;

      Protocolo.Close;
  end;
end;

procedure TFReciboPagamentoConvenio.edProtocoloKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in ['0'..'9']) then key:=#0;
end;

procedure TFReciboPagamentoConvenio.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFReciboPagamentoConvenio.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

end.
