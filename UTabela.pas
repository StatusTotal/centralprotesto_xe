unit UTabela;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, Grids, Wwdbigrd, Wwdbgrid, DB, Menus,
  StdCtrls, ComCtrls, sStatusBar, Buttons, sBitBtn;

type
  TFTabela = class(TForm)
    Grid: TwwDBGrid;
    P1: TsPanel;
    dsCodigos: TDataSource;
    PM: TPopupMenu;
    Ajustar1: TMenuItem;
    btItens: TsBitBtn;
    btDetalhes: TsBitBtn;
    P2: TsPanel;
    Shape2: TShape;
    Label1: TLabel;
    Opd: TOpenDialog;
    procedure Ajustar1Click(Sender: TObject);
    procedure GridCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure btItensClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btDetalhesClick(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
    procedure Calcular;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FTabela: TFTabela;

implementation

uses
  UDM, UPF, UItens, UDetalhes, SqlExpr, UGeral;

{$R *.dfm}

procedure TFTabela.Ajustar1Click(Sender: TObject);
var
  Code: Integer;
begin
  if PF.PasswordInputBox('Seguran�a','Senha')<>FormatDateTime('HHMM',Now) then Exit;

  {NESTA ROTINA CRIO O T�TULO MENOR DEFINO AS FAIXAS M�NIMO E M�XIMO E DEFINO OS CONV�NIOS}

  PF.Aguarde(True);
  dm.Codigos.DisableControls;
  dm.Codigos.First;
  while not dm.Codigos.Eof do
  begin
      Code:=StrToInt(Copy(dm.CodigosCOD.AsString,3,2));

      dm.Codigos.Edit;
      if (Code in [1..8]) or (Code in [30..37])  then
        dm.CodigosTITULO.AsString:=Trim(Copy(dm.CodigosATOS.AsString,Pos(')',dm.CodigosATOS.AsString)+1,Length(dm.CodigosATOS.AsString)))
          else dm.CodigosTITULO.AsString:=dm.CodigosATOS.AsString;

      if dm.CodigosCOD.AsInteger=4001 then      
      begin                                     
          dm.CodigosMINIMO.AsFloat:=0;          
          dm.CodigosMAXIMO.AsFloat:=50;         
      end;

      if dm.CodigosCOD.AsInteger=4002 then 
      begin
          dm.CodigosMINIMO.AsFloat:=50.01;
          dm.CodigosMAXIMO.AsFloat:=100;   
      end;                                 

      if dm.CodigosCOD.AsInteger=4003 then
      begin
           dm.CodigosMINIMO.AsFloat:=100.01;
           dm.CodigosMAXIMO.AsFloat:=200;
      end;

      if dm.CodigosCOD.AsInteger=4004 then      
      begin                                     
          dm.CodigosMINIMO.AsFloat:=200.01;     
          dm.CodigosMAXIMO.AsFloat:=500;        
      end;

      if dm.CodigosCOD.AsInteger=4005 then 
      begin                                
          dm.CodigosMINIMO.AsFloat:=500.01;
          dm.CodigosMAXIMO.AsFloat:=1000;  
      end;                                 

      if dm.CodigosCOD.AsInteger=4006 then
      begin
          dm.CodigosMINIMO.AsFloat:=1000.01;
          dm.CodigosMAXIMO.AsFloat:=5000;
      end;

      if dm.CodigosCOD.AsInteger=4007 then     
      begin                                    
          dm.CodigosMINIMO.AsFloat:=5000.01;   
          dm.CodigosMAXIMO.AsFloat:=10000;     
      end;

      if dm.CodigosCOD.AsInteger=4008 then  
      begin                                 
          dm.CodigosMINIMO.AsFloat:=1000.01;
          dm.CodigosMAXIMO.AsFloat:=1000000000.00;
      end;                                  

      if dm.CodigosCOD.AsInteger=4030 then
      begin
          dm.CodigosMINIMO.AsFloat:=0;
          dm.CodigosMAXIMO.AsFloat:=50;
      end;

      if dm.CodigosCOD.AsInteger=4031 then     
      begin                                    
          dm.CodigosMINIMO.AsFloat:=50.01;     
          dm.CodigosMAXIMO.AsFloat:=100;       
      end;

      if dm.CodigosCOD.AsInteger=4032 then 
      begin                                
          dm.CodigosMINIMO.AsFloat:=100.01;
          dm.CodigosMAXIMO.AsFloat:=200;   
      end;                                 

      if dm.CodigosCOD.AsInteger=4033 then
      begin
          dm.CodigosMINIMO.AsFloat:=200.01;
          dm.CodigosMAXIMO.AsFloat:=500;
      end;

      if dm.CodigosCOD.AsInteger=4034 then    
      begin                                   
          dm.CodigosMINIMO.AsFloat:=500.01;   
          dm.CodigosMAXIMO.AsFloat:=1000;     
      end;

      if dm.CodigosCOD.AsInteger=4035 then  
      begin                                 
          dm.CodigosMINIMO.AsFloat:=1000.01;
          dm.CodigosMAXIMO.AsFloat:=5000;   
      end;                                  

      if dm.CodigosCOD.AsInteger=4036 then
      begin
          dm.CodigosMINIMO.AsFloat:=5000.01;
          dm.CodigosMAXIMO.AsFloat:=10000;
      end;

      if dm.CodigosCOD.AsInteger=4037 then
      begin
          dm.CodigosMINIMO.AsFloat:=10000.01;
          dm.CodigosMAXIMO.AsFloat:=1000000000.00;
      end;

      dm.Codigos.Post;
      dm.Codigos.ApplyUpdates(0);

      dm.Codigos.Next;
  end;
  GR.ExecutarSQLDAC('UPDATE COD SET MUTUA  =0 WHERE MUTUA   IS NULL',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE COD SET ACOTERJ=0 WHERE ACOTERJ IS NULL',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE COD SET DISTRIB=0 WHERE DISTRIB IS NULL',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE COD SET CONVENIO='''+'N'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE COD SET CONVENIO='''+'S'+''' WHERE COD BETWEEN 4030 AND 4048',dm.conCENTRAL);

  dm.Codigos.Refresh;

  dm.Codigos.First;
  dm.Codigos.EnableControls;

  PF.Aguarde(False);
end;

procedure TFTabela.GridCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  if (gdSelected in State) or (gdFocused in State) then
    AFont.Color:=clWhite
  else
    if dm.CodigosCONVENIO.AsString='S' then
      AFont.Color:=clRed
        else AFont.Color:=clBlack;
end;

procedure TFTabela.btItensClick(Sender: TObject);
begin
  dm.Itens.Close;
  dm.Itens.Params[0].AsInteger:=dm.CodigosCOD.AsInteger;
  dm.Itens.Open;

  dm.Tabela.Close;
  dm.Tabela.Open;

  dm.Codigos.Edit;

  GR.CriarForm(TFItens,FItens);

  dm.Tabela.Close;
  dm.Itens.Close;
end;

procedure TFTabela.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFTabela.btDetalhesClick(Sender: TObject);
begin
  GR.CriarForm(TFDetalhes,FDetalhes);
end;

procedure TFTabela.GridDblClick(Sender: TObject);
begin
  btItensClick(Sender);
end;

procedure TFTabela.Calcular;
var
  Posicao: Integer;
  Soma: Double;
begin
  Posicao:=dm.Itens.RecNo;
  dm.Itens.DisableControls;
  dm.Itens.First;
  Soma:=0;
  while not dm.Itens.Eof do
  begin
      Soma:=Soma+dm.ItensTOTAL.AsFloat;
      dm.Itens.Next;
  end;

  dm.Codigos.Edit;
  dm.CodigosEMOL.AsFloat:=Soma;
  dm.CodigosFETJ.AsFloat:=GR.NoRound(Soma*0.2,2);
  dm.CodigosFUND.AsFloat:=GR.NoRound(Soma*0.05,2);
  dm.CodigosFUNP.AsFloat:=GR.NoRound(Soma*0.05,2);

  dm.CodigosTOT.AsFloat:=dm.CodigosEMOL.AsFloat+
                         dm.CodigosFETJ.AsFloat+
                         dm.CodigosFUND.AsFloat+
                         dm.CodigosFUNP.AsFloat+
                         dm.CodigosMUTUA.AsFloat+
                         dm.CodigosACOTERJ.AsFloat+
                         dm.CodigosDISTRIB.AsFloat;
  dm.Codigos.Post;
  dm.Codigos.ApplyUpdates(0);
  dm.Itens.RecNo:=Posicao;
  dm.Itens.EnableControls;
end;

procedure TFTabela.FormShow(Sender: TObject);
begin
  dm.Codigos.Close;
  dm.Codigos.Params.ParamByName('OCULTO1').AsString:='N';
  dm.Codigos.Params.ParamByName('OCULTO2').AsString:='N';
  dm.Codigos.Open;
end;

end.
