unit UDevolvido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, Mask, sMaskEdit, sCustomComboEdit,
  sTooledit, ExtCtrls, sPanel, Grids, Wwdbigrd, Wwdbgrid, DB, sBevel,
  sLabel, sCheckBox;

type
  TFDevolvido = class(TForm)
    P1: TsPanel;
    edData: TsDateEdit;
    btOk: TsBitBtn;
    btCancelar: TsBitBtn;
    Grid: TwwDBGrid;
    dsIrregularidades: TDataSource;
    lbMsg: TsLabel;
    sBevel1: TsBevel;
    ckRetirado: TsCheckBox;
    ckDevolvido: TsCheckBox;
    procedure btOkClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure GridMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure P1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure GridMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DesmarcarTodos;
    function NenhumMarcado: Boolean;
    procedure ckRetiradoClick(Sender: TObject);
    procedure ckDevolvidoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDevolvido: TFDevolvido;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFDevolvido.btOkClick(Sender: TObject);
begin
  if edData.Date=0 then
  begin
      GR.Aviso('Informe a Data da Devolu��o.');
      edData.SetFocus;
      Exit;
  end;

  if NenhumMarcado then
  begin
      GR.Aviso('Informe a Irregularidade.');
      Grid.SetFocus;
      Exit;
  end;

  dm.vOkGeral:=True;
  Close;
end;

procedure TFDevolvido.btCancelarClick(Sender: TObject);
begin
  dm.vOkGeral:=False;
  Close;
end;

procedure TFDevolvido.GridMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if X in [11..44] then
    Screen.Cursor:=crHandPoint
      else Screen.Cursor:=crDefault;
end;

procedure TFDevolvido.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  Screen.Cursor:=crDefault;
end;

procedure TFDevolvido.P1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  Screen.Cursor:=crDefault;
end;

procedure TFDevolvido.FormCreate(Sender: TObject);
begin
  dm.Irregularidades.Close;
  dm.Irregularidades.Open;
  dm.Irregularidades.Filtered:=False;
  dm.Irregularidades.Filter:='CODIGO>0';
  dm.Irregularidades.Filtered:=True;
end;

procedure TFDevolvido.GridMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Screen.Cursor=crHandPoint then
  begin
      DesmarcarTodos;
      dm.Irregularidades.Edit;
      dm.IrregularidadesCheck.AsBoolean:=not dm.IrregularidadesCheck.AsBoolean;
      dm.Irregularidades.Post;
  end;
end;

procedure TFDevolvido.DesmarcarTodos;
var
  Posicao: Integer;
begin
  Posicao:=dm.Irregularidades.RecNo;
  dm.Irregularidades.DisableControls;
  dm.Irregularidades.First;
  while not dm.Irregularidades.Eof do
  begin
      dm.Irregularidades.Edit;
      dm.IrregularidadesCheck.AsBoolean:=False;
      dm.Irregularidades.Post;
      dm.Irregularidades.Next;
  end;
  dm.Irregularidades.RecNo:=Posicao;
  dm.Irregularidades.EnableControls;
end;

function TFDevolvido.NenhumMarcado: Boolean;
var
  Posicao: Integer;
begin
  Result:=True;
  Posicao:=dm.Irregularidades.RecNo;
  dm.Irregularidades.DisableControls;
  dm.Irregularidades.First;
  while not dm.Irregularidades.Eof do
  begin
      if dm.IrregularidadesCheck.AsBoolean then
      begin
          Result:=False;
          break;
      end;
      dm.Irregularidades.Next;
  end;
  dm.Irregularidades.RecNo:=Posicao;
  dm.Irregularidades.EnableControls;
end;

procedure TFDevolvido.ckRetiradoClick(Sender: TObject);
begin
  ckDevolvido.Checked:=False;
end;

procedure TFDevolvido.ckDevolvidoClick(Sender: TObject);
begin
  ckRetirado.Checked:=False;
end;

procedure TFDevolvido.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.Irregularidades.Filtered:=False;
end;

end.
