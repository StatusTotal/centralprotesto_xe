unit UExportarTxtUnico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, sMemo, DBCtrls, sDBLookupComboBox,
  DB, Mask, sMaskEdit, sCustomComboEdit, sTooledit, Buttons, sBitBtn,
  sGroupBox, FMTBcd, Grids, Wwdbigrd, Wwdbgrid, DBClient, Provider, SqlExpr,
  sDBEdit, sEdit, DateUtils, Menus, ComCtrls, sStatusBar, RxMemDS, DBGrids,
  sBevel;

type
  TFExportarTxtUnico = class(TForm)
    P1: TsPanel;
    dsPortadores: TDataSource;
    RgTipo: TsRadioGroup;
    btVisualizar: TsBitBtn;
    btExportar2: TsBitBtn;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    dtsTitulos: TSQLDataSet;
    dspTitulos: TDataSetProvider;
    Titulos: TClientDataSet;
    TitulosID_ATO: TIntegerField;
    TitulosCODIGO: TIntegerField;
    TitulosRECIBO: TIntegerField;
    TitulosDT_PROTOCOLO: TDateField;
    TitulosPROTOCOLO: TIntegerField;
    TitulosLIVRO_PROTOCOLO: TIntegerField;
    TitulosFOLHA_PROTOCOLO: TStringField;
    TitulosDT_PRAZO: TDateField;
    TitulosDT_REGISTRO: TDateField;
    TitulosREGISTRO: TIntegerField;
    TitulosLIVRO_REGISTRO: TIntegerField;
    TitulosFOLHA_REGISTRO: TStringField;
    TitulosSELO_REGISTRO: TStringField;
    TitulosDT_PAGAMENTO: TDateField;
    TitulosSELO_PAGAMENTO: TStringField;
    TitulosRECIBO_PAGAMENTO: TIntegerField;
    TitulosCOBRANCA: TStringField;
    TitulosEMOLUMENTOS: TFloatField;
    TitulosFETJ: TFloatField;
    TitulosFUNDPERJ: TFloatField;
    TitulosFUNPERJ: TFloatField;
    TitulosMUTUA: TFloatField;
    TitulosDISTRIBUICAO: TFloatField;
    TitulosACOTERJ: TFloatField;
    TitulosTOTAL: TFloatField;
    TitulosTIPO_PROTESTO: TIntegerField;
    TitulosTIPO_TITULO: TIntegerField;
    TitulosNUMERO_TITULO: TStringField;
    TitulosDT_TITULO: TDateField;
    TitulosBANCO: TStringField;
    TitulosVALOR_TITULO: TFloatField;
    TitulosSALDO_PROTESTO: TFloatField;
    TitulosDT_VENCIMENTO: TDateField;
    TitulosCONVENIO: TStringField;
    TitulosTIPO_APRESENTACAO: TStringField;
    TitulosDT_ENVIO: TDateField;
    TitulosTIPO_INTIMACAO: TStringField;
    TitulosMOTIVO_INTIMACAO: TMemoField;
    TitulosDT_INTIMACAO: TDateField;
    TitulosDT_PUBLICACAO: TDateField;
    TitulosVALOR_PAGAMENTO: TFloatField;
    TitulosAPRESENTANTE: TStringField;
    TitulosCPF_CNPJ_APRESENTANTE: TStringField;
    TitulosTIPO_APRESENTANTE: TStringField;
    TitulosCEDENTE: TStringField;
    TitulosNOSSO_NUMERO: TStringField;
    TitulosSACADOR: TStringField;
    TitulosDEVEDOR: TStringField;
    TitulosCPF_CNPJ_DEVEDOR: TStringField;
    TitulosTIPO_DEVEDOR: TStringField;
    TitulosAGENCIA_CEDENTE: TStringField;
    TitulosPRACA_PROTESTO: TStringField;
    TitulosTIPO_ENDOSSO: TStringField;
    TitulosACEITE: TStringField;
    TitulosCPF_ESCREVENTE: TStringField;
    TitulosCPF_ESCREVENTE_PG: TStringField;
    TitulosOBSERVACAO: TMemoField;
    TitulosDT_SUSTADO: TDateField;
    TitulosDT_RETIRADO: TDateField;
    TitulosSTATUS: TStringField;
    TitulosPROTESTADO: TStringField;
    TitulosENVIADO_PROTESTO: TStringField;
    TitulosENVIADO_PAGAMENTO: TStringField;
    TitulosEXPORTADO: TStringField;
    dsTitulos: TDataSource;
    GridAtos: TwwDBGrid;
    TitulosCheck: TStringField;
    dsParametros: TDataSource;
    TitulosDT_ENTRADA: TDateField;
    TitulosAGENCIA: TStringField;
    TitulosCONTA: TStringField;
    TitulosAVALISTA_DEVEDOR: TStringField;
    TitulosFINS_FALIMENTARES: TStringField;
    TitulosTARIFA_BANCARIA: TFloatField;
    TitulosFORMA_PAGAMENTO: TStringField;
    TitulosNUMERO_PAGAMENTO: TStringField;
    TitulosARQUIVO: TStringField;
    TitulosRETORNO: TStringField;
    PM: TPopupMenu;
    MarcarTodos1: TMenuItem;
    DesmarcarTodos1: TMenuItem;
    Barra: TsStatusBar;
    dsRemessas: TDataSource;
    btRejeitar: TsBitBtn;
    TitulosDT_DEVOLVIDO: TDateField;
    TitulosCPF_CNPJ_SACADOR: TStringField;
    TitulosID_MSG: TIntegerField;
    TitulosVALOR_AR: TFloatField;
    TitulosELETRONICO: TStringField;
    TitulosIRREGULARIDADE: TIntegerField;
    RX: TRxMemoryData;
    RXPortador: TStringField;
    RXDocumento: TStringField;
    RXCodigo: TStringField;
    edData: TsDateEdit;
    sBevel1: TsBevel;
    btExportar: TsBitBtn;
    M: TsMemo;
    TitulosCODIGO_APRESENTANTE: TStringField;
    TitulosENVIADO_APONTAMENTO: TStringField;
    TitulosENVIADO_RETIRADO: TStringField;
    TitulosENVIADO_SUSTADO: TStringField;
    TitulosENVIADO_DEVOLVIDO: TStringField;
    TitulosAVISTA: TStringField;
    TitulosSALDO_TITULO: TFloatField;
    TitulosTIPO_SUSTACAO: TStringField;
    TitulosRETORNO_PROTESTO: TStringField;
    TitulosDT_RETORNO_PROTESTO: TDateField;
    TitulosDT_DEFINITIVA: TDateField;
    TitulosJUDICIAL: TStringField;
    TitulosFUNARPEN: TFloatField;
    TitulosPMCMV: TFloatField;
    TitulosALEATORIO_PROTESTO: TStringField;
    TitulosALEATORIO_SOLUCAO: TStringField;
    TitulosCCT: TStringField;
    TitulosDETERMINACAO: TStringField;
    TitulosANTIGO: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btVisualizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Visualizar2;
    procedure btExportar2Click(Sender: TObject);
    procedure edRemessaKeyPress(Sender: TObject; var Key: Char);
    procedure MarcarTodos1Click(Sender: TObject);
    procedure DesmarcarTodos1Click(Sender: TObject);
    function Marcados: Integer;
    function Vazio: Boolean;
    procedure btRejeitarClick(Sender: TObject);
    procedure btExportarClick(Sender: TObject);
    procedure GridAtosMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure GridAtosMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FExportarTxtUnico: TFExportarTxtUnico;
  Posicao: Integer;

implementation

uses UDM, UPF, UIrregularidades, UGeral;

{$R *.dfm}

function TFExportarTxtUnico.Vazio: Boolean;
var
  Posicao: Integer;
begin
  Result :=True;
  Posicao:=Titulos.RecNo;
  Titulos.DisableControls;
  Titulos.First;
  while not Titulos.Eof do
  begin
      if TitulosCheck.AsString='S' then
      begin
          Result:=False;
          Break;
      end;
      Titulos.Next;
  end;
  Titulos.RecNo:=Posicao;
  Titulos.EnableControls;
end;

procedure TFExportarTxtUnico.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm.Portadores.Close;
end;

procedure TFExportarTxtUnico.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFExportarTxtUnico.Visualizar2;
var
  I,Qtd,Count: Integer;
  Ocorrencia,Data,Custas: String;
  Soma,Saldo: Double;
begin
  M.Lines.Clear;

  RX.First;
  while not RX.Eof do
  begin
      Titulos.Filtered:=False;
      Titulos.Filter:='APRESENTANTE='+QuotedStr(RXPortador.AsString);
      Titulos.Filtered:=True;
      Titulos.First;
      if RXCodigo.AsString<>'' then
        PF.BuscarApresentante('C',RXCodigo.AsString)
          else PF.BuscarApresentante('N',RXPortador.AsString);
      M.Lines.Add('0'+PF.Zeros(Copy(dm.PortadoresCODIGO.AsString,1,3),'D',3)+
                      PF.Espaco(dm.PortadoresNOME.AsString,'E',40)+
                      FormatDateTime('ddmmyyyy',edData.Date)+'SDTBFORTP'+
                      PF.Zeros(dm.PortadoresSEQUENCIA.AsString,'D',6)+
                      PF.Zeros(IntToStr(Marcados),'D',4)+
                      '000000000000'+
                      PF.Zeros(dm.PortadoresAGENCIA.AsString,'E',6)+
                      '043'+
                      dm.ServentiaCODIGO_PRACA.AsString+
                      PF.Espaco(' ','E',497)+
                      '0001');
      Count :=2;
      Soma  :=0;
      Qtd   :=0;
      Saldo :=0;

      for I:=0 to Titulos.RecordCount-1 do
      begin
          if TitulosCheck.AsString='S' then
          begin
              if TitulosSTATUS.AsString='PAGO' then
              begin
                  Ocorrencia:='1';
                  Data      :=FormatDateTime('ddmmyyyy',TitulosDT_PAGAMENTO.AsDateTime);
                  Saldo     :=TitulosSALDO_PROTESTO.AsFloat;
              end;
          
              if TitulosSTATUS.AsString='PROTESTADO' then
              begin
                  Ocorrencia:='2';
                  Data      :=FormatDateTime('ddmmyyyy',TitulosDT_REGISTRO.AsDateTime);
                  Saldo     :=TitulosVALOR_TITULO.AsFloat;
              end;

              if TitulosSTATUS.AsString='RETIRADO' then
              begin
                  Ocorrencia:='3';
                  Data      :=FormatDateTime('ddmmyyyy',TitulosDT_RETIRADO.AsDateTime);
                  Saldo     :=TitulosVALOR_TITULO.AsFloat;

                  {if TitulosIRREGULARIDADE.AsString='' then
                  begin
                      PF.Mensagem('!','Informe o motivo da retirada do protocolo n� '+TitulosPROTOCOLO.AsString+'.');
                      Titulos.Locate('PROTOCOLO',TitulosPROTOCOLO.AsInteger,[]);
                      btRejeitarClick(Self);
                  end;}
              end;

              if TitulosSTATUS.AsString='SUSTADO' then
              begin
                  Ocorrencia:='4';
                  Data      :=FormatDateTime('ddmmyyyy',TitulosDT_SUSTADO.AsDateTime);
                  Saldo     :=TitulosVALOR_TITULO.AsFloat;
              end;

              if TitulosSTATUS.AsString='CANCELADO' then
              begin
                  Ocorrencia:='A';
                  Data      :=FormatDateTime('ddmmyyyy',TitulosDT_PAGAMENTO.AsDateTime);
                  Saldo     :=TitulosSALDO_PROTESTO.AsFloat;
              end;

              if (TitulosSTATUS.AsString='DEVOLVIDO') then
              begin
                  if TitulosIRREGULARIDADE.AsString='' then
                  begin
                      GR.Aviso('Informe o motivo da devolu��o do protocolo n� '+TitulosPROTOCOLO.AsString+'.');
                      Titulos.Locate('PROTOCOLO',TitulosPROTOCOLO.AsInteger,[]);
                      btRejeitarClick(Self);
                  end;

                  {SEM CUSTAS}
                  if TitulosCONVENIO.AsString='S' then
                  begin
                      Ocorrencia:='5';
                      Data      :=FormatDateTime('ddmmyyyy',TitulosDT_DEVOLVIDO.AsDateTime);
                      Saldo     :=TitulosVALOR_TITULO.AsFloat;
                  end
                  else
                  begin
                      Ocorrencia:='6';
                      Data      :=FormatDateTime('ddmmyyyy',TitulosDT_DEVOLVIDO.AsDateTime);
                      Saldo     :=TitulosSALDO_PROTESTO.AsFloat;
                  end;
              end;

              Soma:=Soma+Saldo;

              {SE FOR CRA N�O INFORMO AS CUSTAS}
              (*if dm.PortadoresCRA.AsString='S' then
                Custas:='0000000000'
                  else
                    if dm.ValorParametro(40)='S' then {SE O CART�RIO COBRAR AS CUSTAS NO FINAL DO EVENTO}
                      Custas:=PF.Zeros(PF.PegarNumeroTexto(FloatToStrF(PF.NoRound(TitulosTOTAL.AsFloat,2),ffNumber,7,2)),'D',10)
                        else Custas:='0000000000';*)

              if dm.PortadoresCONVENIO.AsString='S' then
                Custas:=PF.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(TitulosTOTAL.AsFloat,2),ffNumber,7,2)),'D',10)
                  else Custas:='0000000000';

              M.Lines.Add('1'+PF.Zeros(dm.PortadoresCODIGO.AsString,'D',3)+
                              PF.Espaco(TitulosAGENCIA_CEDENTE.AsString,'E',15)+
                              PF.Espaco(' ','E',45)+
                              PF.Espaco(' ','E',45)+
                              PF.Espaco(' ','E',14)+
                              PF.Espaco(' ','E',45)+
                              '00000000'+
                              PF.Espaco(' ','E',20)+
                              PF.Espaco(' ','E',2)+
                              PF.Espaco(TitulosNOSSO_NUMERO.AsString,'E',15)+
                              PF.Espaco(' ','E',3)+
                              PF.Espaco(' ','E',11)+
                              '00000000'+
                              '00000000'+
                              '001'+
                              PF.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(TitulosVALOR_TITULO.AsFloat,2),ffNumber,7,2)),'D',14,)+
                              PF.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(Saldo,2),ffNumber,7,2)),'D',14)+
                              PF.Espaco(' ','E',20)+
                              PF.Espaco(' ','E',1)+
                              PF.Espaco(' ','E',1)+
                              PF.Espaco(' ','E',1)+
                              PF.Espaco(' ','E',45)+
                              '000'+
                              '00000000000000'+
                              PF.Espaco(' ','E',11)+
                              PF.Espaco(' ','E',45)+
                              '00000000'+
                              PF.Espaco(' ','E',20)+
                              PF.Espaco(' ','E',2)+
                              dm.ValorParametro(23)+
                              PF.Zeros(TitulosPROTOCOLO.AsString,'D',10)+
                              Ocorrencia+
                              FormatDateTime('ddmmyyyy',TitulosDT_PROTOCOLO.AsDateTime)+
                              Custas+
                              PF.Espaco(' ','E',1)+
                              Data+
                              PF.Zeros(TitulosIRREGULARIDADE.AsString,'D',2)+
                              PF.Espaco(' ','E',20)+
                              PF.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(TitulosDISTRIBUICAO.AsFloat,2),ffNumber,7,2)),'D',10)+
                              '000000'+
                              '0000000000'+
                              '00000'+
                              '000000000000000'+
                              '000'+
                              '0'+
                              PF.Espaco(' ','E',8)+
                              PF.Espaco(' ','E',1)+
                              PF.Espaco(' ','E',1)+
                              '0000000000'+
                              PF.Espaco(' ','E',19)+
                              PF.Zeros(IntToStr(Count),'D',4));
              Inc(Count);
              Inc(Qtd);
          end;
          Titulos.Next;
      end;

      M.Lines.Add('9'+PF.Zeros(Copy(dm.PortadoresCODIGO.AsString,1,3),'D',3)+
                      PF.Espaco(dm.PortadoresNOME.AsString,'E',40)+
                      FormatDateTime('ddmmyyyy',edData.Date)+
                      PF.Zeros(IntToStr(Qtd),'D',5)+
                      PF.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(Soma,2),ffNumber,7,2)),'D',18)+
                      PF.Espaco(' ','E',521)+
                      PF.Zeros(IntToStr(Count),'D',4));

      dm.Remessas.Close;
      dm.Remessas.Params[0].AsInteger :=dm.PortadoresID_PORTADOR.AsInteger;
      dm.Remessas.Params[1].AsDate    :=edData.Date;
      dm.Remessas.Params[2].AsString  :='SE';
      dm.Remessas.Open;
      if dm.Remessas.IsEmpty then
      begin
          dm.Remessas.Append;
          dm.RemessasID_REMESSA.AsInteger  :=dm.IdAtual('ID_REMESSA','S');
          dm.RemessasID_PORTADOR.AsInteger :=dm.PortadoresID_PORTADOR.AsInteger;
          dm.RemessasDATA.AsDateTime       :=edData.Date;
          dm.RemessasNUMERO.AsInteger      :=PF.UltimaRemessa(edData.Date,dm.PortadoresID_PORTADOR.AsInteger,'SE')+1;
          dm.RemessasTIPO.AsString         :='SE';
          dm.Remessas.Post;
          dm.Remessas.ApplyUpdates(0);
      end;
      dm.Remessas.Edit;
      dm.RemessasNUMERO.AsInteger:=dm.RemessasNUMERO.AsInteger+1;
      dm.Remessas.Post;
      dm.Remessas.ApplyUpdates(0);
      dm.Remessas.Close;
      dm.Portadores.Edit;
      dm.PortadoresSEQUENCIA.AsInteger:=dm.PortadoresSEQUENCIA.AsInteger+1;
      dm.Portadores.Post;
      dm.Portadores.ApplyUpdates(0);
      RX.Next;
  end;
  Titulos.Close;
end;

procedure TFExportarTxtUnico.btVisualizarClick(Sender: TObject);
begin
  Titulos.Close;
  Titulos.Filtered:=False;
  Titulos.Params.ParamByName('D1').AsDate:=edInicio.Date;
  Titulos.Params.ParamByName('D2').AsDate:=edFim.Date;
  Titulos.Open;
  MarcarTodos1Click(Sender);
  Barra.Panels[1].Text:=IntToStr(Titulos.RecordCount);
end;

{ESSE EXPORTAR EST� OK, POR�M CRIA CABE�ALHO SEPARADO PARA CADA PORTADOR}
procedure TFExportarTxtUnico.btExportar2Click(Sender: TObject);
var
  Arquivo: String;
  Posicao: Integer;
begin
  try
    if Vazio                then Exit;
    if Titulos.IsEmpty      then Exit;

    if dm.RemessasNUMERO.AsInteger>9 then
    begin
        GR.Aviso('O n�mero de remessas n�o pode ser superior a 9!'+#13#13+'Arquivo n�o gerado!!');
        Exit;
    end;

    Arquivo:='R'+PF.Zeros(dm.PortadoresCODIGO.AsString,'D',3)+FormatDateTime('ddmm.yy',edData.Date)+'1';
    Posicao:=Titulos.RecNo;
    Titulos.DisableControls;
    Titulos.First;
    RX.Close;
    RX.Open;
    while not Titulos.Eof do
    begin
        if TitulosCheck.AsString='S' then
        begin
            if not RX.Locate('Portador',TitulosAPRESENTANTE.AsString,[loPartialKey,loCaseInsensitive]) then
            begin
                RX.Append;
                if TitulosCODIGO_APRESENTANTE.AsInteger<>0 then
                RXCodigo.AsString   :=TitulosCODIGO_APRESENTANTE.AsString;
                RXPortador.AsString :=TitulosAPRESENTANTE.AsString;
                RXDocumento.AsString:=TitulosCPF_CNPJ_APRESENTANTE.AsString;
            end;

            Titulos.Edit;
            TitulosEXPORTADO.AsString :='S';
            TitulosRETORNO.AsString   :=Arquivo;
            Titulos.Post;
            Titulos.ApplyUpdates(0);
        end;
        Titulos.Next;
    end;
    Titulos.RecNo:=Posicao;
    Titulos.EnableControls;
  Visualizar2;
    M.Lines.SaveToFile(dm.ValorParametro(4)+Arquivo);
    Titulos.Close;
    M.Lines.Clear;
    GR.Aviso('Remessa de retorno gerada com sucesso.');
  except
    on E: Exception do
    GR.Aviso('N�o foi poss�vel gerar a remessa de retorno.'+#13#13+'Erro: '+E.Message);
  end;
end;

procedure TFExportarTxtUnico.edRemessaKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFExportarTxtUnico.MarcarTodos1Click(Sender: TObject);
begin
  Posicao:=Titulos.RecNo;
  Titulos.DisableControls;
  Titulos.First;
  while not Titulos.Eof do
  begin
      Titulos.Edit;
      TitulosCheck.AsString:='S';
      Titulos.Next;
  end;
  Titulos.RecNo:=Posicao;
  Titulos.EnableControls;
end;

procedure TFExportarTxtUnico.DesmarcarTodos1Click(Sender: TObject);
begin
  Posicao:=Titulos.RecNo;
  Titulos.DisableControls;
  Titulos.First;
  while not Titulos.Eof do
  begin
      Titulos.Edit;
      TitulosCheck.AsString:='N';
      Titulos.Next;
  end;
  Titulos.RecNo:=Posicao;
  Titulos.EnableControls;
end;

function TFExportarTxtUnico.Marcados: Integer;
begin
  Posicao:=Titulos.RecNo;
  Titulos.DisableControls;
  Titulos.First;
  Result:=0;
  while not Titulos.Eof do
  begin
      if TitulosCheck.AsString='S' then
      Result:=Result+1;
      Titulos.Next;
  end;
  Titulos.RecNo:=Posicao;
  Titulos.EnableControls;
end;

procedure TFExportarTxtUnico.btRejeitarClick(Sender: TObject);
begin
  dm.vOk:=False;
  GR.CriarForm(TFIrregularidades,FIrregularidades);

  if dm.vOk then
  begin
      Titulos.Edit;
      TitulosIRREGULARIDADE.AsInteger:=dm.IrregularidadesCODIGO.AsInteger;
      Titulos.Post;
      Titulos.ApplyUpdates(0);
  end;
  dm.Irregularidades.Close;
end;

procedure TFExportarTxtUnico.btExportarClick(Sender: TObject);
var
  Numero: Integer;
  Qtd,Count: Integer;
  Soma,Saldo: Double;
  Arquivo,Serventia: String;
  Ocorrencia,Data,Custas: String;
begin
  try
    if Vazio           then Exit;
    if Titulos.IsEmpty then Exit;

    if dm.RemessasNUMERO.AsInteger>9 then
    begin
        GR.Aviso('O n�mero de remessas n�o pode ser superior a 9!'+#13#13+'Arquivo n�o gerado!!');
        Exit;
    end;

    if dm.ServentiaCODIGO.AsInteger=1500 then
      Serventia:='007'
        else
          if dm.ServentiaCODIGO.AsInteger=1513 then
            Serventia:='001'
              else
                if dm.ServentiaCODIGO.AsInteger=1515 then
                  Serventia:='005';

    Numero:=1;
    repeat
      Arquivo:='R'+Serventia+FormatDateTime('ddmm.yy',edData.Date)+IntToStr(Numero);
      Inc(Numero);
    until
      not FileExists(dm.ValorParametro(4)+Arquivo);

    PF.Aguarde(True);
    Titulos.DisableControls;
    Titulos.First;
    RX.Close;
    RX.Open;
    while not Titulos.Eof do
    begin
        if TitulosCheck.AsString='S' then
        begin
            if not RX.Locate('Portador',TitulosAPRESENTANTE.AsString,[loPartialKey,loCaseInsensitive]) then
            begin
                RX.Append;
                RXCodigo.AsString   :=TitulosCODIGO_APRESENTANTE.AsString;
                RXPortador.AsString :=TitulosAPRESENTANTE.AsString;
                RXDocumento.AsString:=TitulosCPF_CNPJ_APRESENTANTE.AsString;
            end;

            Titulos.Edit;
            TitulosEXPORTADO.AsString :='S';
            TitulosRETORNO.AsString   :=Arquivo;
            Titulos.Post;
            Titulos.ApplyUpdates(0);
        end;
        Titulos.Next;
    end;

{EXPORTA��O}
    M.Lines.Clear;

    if RXCodigo.AsString<>'' then
      PF.BuscarApresentante('C',RXCodigo.AsString)
        else PF.BuscarApresentante('N',RXPortador.AsString);
    M.Lines.Add('0'+PF.Zeros(Copy(dm.PortadoresCODIGO.AsString,1,3),'D',3)+
                    PF.Espaco(dm.PortadoresNOME.AsString,'E',40)+
                    FormatDateTime('ddmmyyyy',edData.Date)+'SDTBFORTP'+
                    PF.Zeros(dm.PortadoresSEQUENCIA.AsString,'D',6)+
                    PF.Zeros(IntToStr(Marcados),'D',4)+
                    '000000000000'+
                    PF.Zeros(dm.PortadoresAGENCIA.AsString,'E',6)+
                    '043'+
                    '       '+
                    PF.Espaco(' ','E',497)+
                    '0001');

    Count :=2;
    Soma  :=0;
    Qtd   :=0;
    Saldo :=0;

    Titulos.First;
    while not Titulos.Eof do
    begin
        if TitulosCheck.AsString='S' then
        begin
            if TitulosSTATUS.AsString='PAGO' then
            begin
                Ocorrencia:='1';
                Data      :=FormatDateTime('ddmmyyyy',TitulosDT_PAGAMENTO.AsDateTime);
                Saldo     :=TitulosSALDO_PROTESTO.AsFloat-TitulosTARIFA_BANCARIA.AsFloat;
            end;
          
            if TitulosSTATUS.AsString='PROTESTADO' then
            begin
                Ocorrencia:='2';
                Data      :=FormatDateTime('ddmmyyyy',TitulosDT_REGISTRO.AsDateTime);
                Saldo     :=TitulosVALOR_TITULO.AsFloat;
            end;

            if TitulosSTATUS.AsString='RETIRADO' then
            begin
                Ocorrencia:='3';
                Data      :=FormatDateTime('ddmmyyyy',TitulosDT_RETIRADO.AsDateTime);
                Saldo     :=TitulosVALOR_TITULO.AsFloat;
            end;

            if TitulosSTATUS.AsString='SUSTADO' then
            begin
                Ocorrencia:='4';
                Data      :=FormatDateTime('ddmmyyyy',TitulosDT_SUSTADO.AsDateTime);
                Saldo     :=TitulosVALOR_TITULO.AsFloat;
            end;

            if TitulosSTATUS.AsString='CANCELADO' then
            begin
                Ocorrencia:='A';
                Data      :=FormatDateTime('ddmmyyyy',TitulosDT_PAGAMENTO.AsDateTime);
                Saldo     :=TitulosSALDO_PROTESTO.AsFloat;
            end;

            if (TitulosSTATUS.AsString='DEVOLVIDO') then
            begin
                if TitulosIRREGULARIDADE.AsString='' then
                begin
                    GR.Aviso('Informe o motivo da devolu��o do protocolo n� '+TitulosPROTOCOLO.AsString+'.');
                    Titulos.Locate('PROTOCOLO',TitulosPROTOCOLO.AsInteger,[]);
                    btRejeitarClick(Self);
                end;

                {SEM CUSTAS}
                if TitulosCONVENIO.AsString='S' then
                begin
                    Ocorrencia:='5';
                    Data      :=FormatDateTime('ddmmyyyy',TitulosDT_DEVOLVIDO.AsDateTime);
                    Saldo     :=TitulosVALOR_TITULO.AsFloat;
                end
                else
                begin
                    Ocorrencia:='6';
                    Data      :=FormatDateTime('ddmmyyyy',TitulosDT_DEVOLVIDO.AsDateTime);
                    Saldo     :=TitulosSALDO_PROTESTO.AsFloat;
                end;
            end;

            Soma:=Soma+Saldo;

            if dm.PortadoresCONVENIO.AsString='S' then
              Custas:=PF.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(TitulosTOTAL.AsFloat,2),ffNumber,7,2)),'D',10)
                else Custas:='0000000000';

            M.Lines.Add('1'+PF.Zeros(TitulosCODIGO_APRESENTANTE.AsString,'D',3)+
                            PF.Espaco(TitulosAGENCIA_CEDENTE.AsString,'E',15)+
                            PF.Espaco(' ','E',45)+
                            PF.Espaco(' ','E',45)+
                            PF.Espaco(' ','E',14)+
                            PF.Espaco(' ','E',45)+
                            '00000000'+
                            PF.Espaco(' ','E',20)+
                            PF.Espaco(' ','E',2)+
                            PF.Espaco(TitulosNOSSO_NUMERO.AsString,'E',15)+
                            PF.Espaco(' ','E',3)+
                            PF.Espaco(' ','E',11)+
                            '00000000'+
                            '00000000'+
                            '001'+
                            PF.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(TitulosVALOR_TITULO.AsFloat,2),ffNumber,7,2)),'D',14,)+
                            PF.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(Saldo,2),ffNumber,7,2)),'D',14)+
                            PF.Espaco(' ','E',20)+
                            PF.Espaco(' ','E',1)+
                            PF.Espaco(' ','E',1)+
                            PF.Espaco(' ','E',1)+
                            PF.Espaco(' ','E',45)+
                            '000'+
                            '00000000000000'+
                            PF.Espaco(' ','E',11)+
                            PF.Espaco(' ','E',45)+
                            '00000000'+
                            PF.Espaco(' ','E',20)+
                            PF.Espaco(' ','E',2)+
                            dm.ValorParametro(23)+
                            PF.Zeros(TitulosPROTOCOLO.AsString,'D',10)+
                            Ocorrencia+
                            FormatDateTime('ddmmyyyy',TitulosDT_PROTOCOLO.AsDateTime)+
                            Custas+
                            PF.Espaco(' ','E',1)+
                            Data+
                            PF.Zeros(TitulosIRREGULARIDADE.AsString,'D',2)+
                            PF.Espaco(' ','E',20)+
                            '0000000000'+
                            '000000'+
                            '0000000000'+
                            '00000'+
                            '000000000000000'+
                            '000'+
                            '0'+
                            PF.Espaco(' ','E',8)+
                            PF.Espaco(' ','E',1)+
                            PF.Espaco(' ','E',1)+
                            '0000000000'+
                            PF.Espaco(' ','E',19)+
                            PF.Zeros(IntToStr(Count),'D',4));
            Inc(Count);
            Inc(Qtd);
        end;
        Titulos.Next;
    end;

    M.Lines.Add('9'+PF.Zeros(Copy(dm.PortadoresCODIGO.AsString,1,3),'D',3)+
                    PF.Espaco(dm.PortadoresNOME.AsString,'E',40)+
                    FormatDateTime('ddmmyyyy',edData.Date)+
                    PF.Zeros(IntToStr(Qtd),'D',5)+
                    PF.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(Soma,2),ffNumber,7,2)),'D',18)+
                    PF.Espaco(' ','E',521)+
                    PF.Zeros(IntToStr(Count),'D',4));

    dm.Remessas.Close;
    dm.Remessas.Params[0].AsInteger :=dm.PortadoresID_PORTADOR.AsInteger;
    dm.Remessas.Params[1].AsDate    :=edData.Date;
    dm.Remessas.Params[2].AsString  :='SE';
    dm.Remessas.Open;
    if dm.Remessas.IsEmpty then
    begin
        dm.Remessas.Append;
        dm.RemessasID_REMESSA.AsInteger  :=dm.IdAtual('ID_REMESSA','S');
        dm.RemessasID_PORTADOR.AsInteger :=dm.PortadoresID_PORTADOR.AsInteger;
        dm.RemessasDATA.AsDateTime       :=edData.Date;
        dm.RemessasNUMERO.AsInteger      :=PF.UltimaRemessa(edData.Date,dm.PortadoresID_PORTADOR.AsInteger,'SE')+1;
        dm.RemessasTIPO.AsString         :='SE';
        dm.Remessas.Post;
        dm.Remessas.ApplyUpdates(0);
    end;
    dm.Remessas.Edit;
    dm.RemessasNUMERO.AsInteger:=dm.RemessasNUMERO.AsInteger+1;
    dm.Remessas.Post;
    dm.Remessas.ApplyUpdates(0);
    dm.Remessas.Close;
    dm.Portadores.Edit;
    dm.PortadoresSEQUENCIA.AsInteger:=dm.PortadoresSEQUENCIA.AsInteger+1;
    dm.Portadores.Post;
    dm.Portadores.ApplyUpdates(0);

    Titulos.Close;
    Titulos.EnableControls;
    Barra.Panels[1].Text:='0';
{EXPORTA��O}

    M.Lines.SaveToFile(dm.ValorParametro(4)+Arquivo);
    Titulos.Close;
    M.Lines.Clear;
    PF.Aguarde(False);
    GR.Aviso('Remessa de retorno gerada com sucesso.');
  except
    on E: Exception do
    begin
        PF.Aguarde(False);
        GR.Aviso('N�o foi poss�vel gerar a remessa de retorno.'+#13#13+'Erro: '+E.Message);
    end;
  end;
end;

procedure TFExportarTxtUnico.GridAtosMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if X in [0..22] then
    Screen.Cursor:=crHandPoint
      else
        Screen.Cursor:=crDefault;
end;

procedure TFExportarTxtUnico.GridAtosMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Screen.Cursor=crHandPoint then
    if not Titulos.IsEmpty then
    begin
        Titulos.Edit;
        if TitulosCheck.AsString='S' then
          TitulosCheck.AsString:='N'
            else TitulosCheck.AsString:='S';
        Titulos.Post;
        Titulos.ApplyUpdates(0);
    end;
end;

end.
