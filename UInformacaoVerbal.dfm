object FInformacaoVerbal: TFInformacaoVerbal
  Left = 359
  Top = 303
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Informa'#231#227'o Verbal - Lan'#231'amento'
  ClientHeight = 206
  ClientWidth = 501
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object btSalvar: TsBitBtn
    Left = 310
    Top = 162
    Width = 83
    Height = 36
    Cursor = crHandPoint
    Caption = 'Salvar'
    TabOrder = 0
    OnClick = btSalvarClick
    ImageIndex = 2
    Images = Gdm.Im24
    SkinData.SkinSection = 'BUTTON'
  end
  object btCancelar: TsBitBtn
    Left = 399
    Top = 162
    Width = 95
    Height = 36
    Cursor = crHandPoint
    Caption = 'Cancelar'
    TabOrder = 1
    OnClick = btCancelarClick
    ImageIndex = 3
    Images = Gdm.Im24
    SkinData.SkinSection = 'BUTTON'
  end
  object P1: TsPanel
    Left = 8
    Top = 8
    Width = 486
    Height = 145
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object edData: TsDBDateEdit
      Left = 8
      Top = 24
      Width = 93
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 0
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Blend = 0
      DataField = 'DATA'
      DataSource = dsVerbal
    end
    object edCCT: TsDBEdit
      Left = 108
      Top = 24
      Width = 78
      Height = 23
      Color = 8454016
      DataField = 'CCT'
      DataSource = dsVerbal
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'CCT'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edRecibo: TsDBEdit
      Left = 193
      Top = 24
      Width = 104
      Height = 23
      Color = clWhite
      DataField = 'NUMERO'
      DataSource = dsVerbal
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'N'#186' Recibo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object cbEscrevente: TsDBComboBox
      Left = 304
      Top = 24
      Width = 174
      Height = 23
      Style = csDropDownList
      Color = clWhite
      DataField = 'ESCREVENTE'
      DataSource = dsVerbal
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Escrevente'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
    end
    object edNome: TsDBEdit
      Left = 8
      Top = 69
      Width = 470
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'NOME'
      DataSource = dsVerbal
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Nome no qual foi solicitada a informa'#231#227'o verbal'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object cbCobranca: TsComboBox
      Left = 8
      Top = 113
      Width = 111
      Height = 23
      Alignment = taLeftJustify
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Tipo de Cobran'#231'a'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ItemIndex = 1
      ParentFont = False
      TabOrder = 5
      Text = 'Com Cobran'#231'a'
      Items.Strings = (
        'Justi'#231'a Gratuita'
        'Com Cobran'#231'a'
        'Sem Cobran'#231'a'
        'Nihil')
    end
    object edEmolumentos: TsDBEdit
      Left = 126
      Top = 113
      Width = 60
      Height = 23
      Color = clWhite
      DataField = 'EMOLUMENTOS'
      DataSource = dsVerbal
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Emol.'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edFetj: TsDBEdit
      Left = 193
      Top = 113
      Width = 47
      Height = 23
      Color = clWhite
      DataField = 'FETJ'
      DataSource = dsVerbal
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Fetj'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edFundperj: TsDBEdit
      Left = 247
      Top = 113
      Width = 50
      Height = 23
      Color = clWhite
      DataField = 'FUNDPERJ'
      DataSource = dsVerbal
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Fundperj'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edFunperj: TsDBEdit
      Left = 304
      Top = 113
      Width = 50
      Height = 23
      Color = clWhite
      DataField = 'FUNPERJ'
      DataSource = dsVerbal
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Funperj'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edFunarpen: TsDBEdit
      Left = 361
      Top = 113
      Width = 55
      Height = 23
      Color = clWhite
      DataField = 'FUNARPEN'
      DataSource = dsVerbal
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Funarpen'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edTotal: TsDBEdit
      Left = 423
      Top = 113
      Width = 55
      Height = 23
      Color = clWhite
      DataField = 'TOTAL'
      DataSource = dsVerbal
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Total'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
  end
  object dsVerbal: TDataSource
    Left = 64
    Top = 160
  end
  object Custas: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select * from CUSTAS'
      ''
      'where ID_ATO=:ID_ATO')
    Left = 112
    Top = 160
    ParamData = <
      item
        Name = 'ID_ATO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object CustasID_CUSTA: TIntegerField
      FieldName = 'ID_CUSTA'
      Origin = 'ID_CUSTA'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object CustasID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
      Required = True
    end
    object CustasTABELA: TStringField
      FieldName = 'TABELA'
      Origin = 'TABELA'
    end
    object CustasITEM: TStringField
      FieldName = 'ITEM'
      Origin = 'ITEM'
    end
    object CustasSUBITEM: TStringField
      FieldName = 'SUBITEM'
      Origin = 'SUBITEM'
    end
    object CustasVALOR: TFloatField
      FieldName = 'VALOR'
      Origin = 'VALOR'
    end
    object CustasQTD: TIntegerField
      FieldName = 'QTD'
      Origin = 'QTD'
    end
    object CustasTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
    end
    object CustasDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 100
    end
  end
end
