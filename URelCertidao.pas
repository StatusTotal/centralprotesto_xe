unit URelCertidao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, Mask, sMaskEdit, sCustomComboEdit,
  sTooledit, Buttons, sBitBtn, DB, QRCtrls, QuickRpt, DBClient, SimpleDS,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFRelCertidao = class(TForm)
    P1: TsPanel;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    Relatorio: TQuickRep;
    Cabecalho: TQRBand;
    QRLabel1: TQRLabel;
    lbTabeliao: TQRLabel;
    lbCartorio: TQRLabel;
    QRLabel16: TQRLabel;
    lbEndereco: TQRLabel;
    lbTelefone: TQRLabel;
    QRBand1: TQRBand;
    ChildBand1: TQRChildBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    Certidoes: TFDQuery;
    CertidoesID_CERTIDAO: TIntegerField;
    CertidoesID_ATO: TIntegerField;
    CertidoesDT_CERTIDAO: TDateField;
    CertidoesTIPO_CERTIDAO: TStringField;
    CertidoesSELO: TStringField;
    CertidoesALEATORIO: TStringField;
    CertidoesREQUERENTE: TStringField;
    CertidoesSeloAleatorio: TStringField;
    CertidoesTipoCertidao: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btVisualizarClick(Sender: TObject);
    procedure CabecalhoBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure btSairClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FDQuery1CalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelCertidao: TFRelCertidao;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFRelCertidao.FDQuery1CalcFields(DataSet: TDataSet);
begin
  if CertidoesTIPO_CERTIDAO.AsString='N' then CertidoesTipoCertidao.AsString:='Normal';
  if CertidoesTIPO_CERTIDAO.AsString='X' then CertidoesTipoCertidao.AsString:='Cancelamento';
  if CertidoesTIPO_CERTIDAO.AsString='I' then CertidoesTipoCertidao.AsString:='Inteiro Teor';

  CertidoesSeloAleatorio.AsString:=GR.SeloFormatado(CertidoesSELO.AsString,CertidoesALEATORIO.AsString);
end;

procedure TFRelCertidao.FormCreate(Sender: TObject);
begin
  edInicio.Date :=Now;
  edFim.Date    :=Now;
end;

procedure TFRelCertidao.btVisualizarClick(Sender: TObject);
begin
  PF.Aguarde(True);
  Certidoes.Close;
  Certidoes.Params[0].AsDate:=edInicio.Date;
  Certidoes.Params[1].AsDate:=edFim.Date;
  Certidoes.Open;
  PF.Aguarde(False);
  if not Certidoes.IsEmpty then
    Relatorio.Preview
      else GR.Aviso('NENHUM MOVIMENTO ENCONTRADO!');
end;

procedure TFRelCertidao.CabecalhoBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbCartorio.Caption:=dm.ServentiaDESCRICAO.AsString;
  lbTabeliao.Caption:=dm.ServentiaTABELIAO.AsString;
  lbEndereco.Caption:=dm.ServentiaENDERECO.AsString+' - '+dm.ServentiaBAIRRO.AsString+' - RJ';
  lbTelefone.Caption:=PF.FormatarTelefone(dm.ServentiaTELEFONE.AsString);
end;

procedure TFRelCertidao.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFRelCertidao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

end.
