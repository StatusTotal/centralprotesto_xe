unit UDadosEdital;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Mask, sMaskEdit, sCustomComboEdit, sTooledit,
  sDBDateEdit, Buttons, sBitBtn, DBCtrls, sDBLookupComboBox, sLabel,
  acDBTextFX, ExtCtrls, sPanel;

type
  TFDadosEdital = class(TForm)
    PDados: TsPanel;
    lkMotivo: TsDBLookupComboBox;
    btContinuar: TsBitBtn;
    dsMotivos: TDataSource;
    dsProtocolo: TDataSource;
    lbProtocolo: TsLabel;
    edPublicacao: TsDateEdit;
    procedure btContinuarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDadosEdital: TFDadosEdital;

implementation

uses UDM, UPF, URelEdital, UGeral;

{$R *.dfm}

procedure TFDadosEdital.btContinuarClick(Sender: TObject);
begin
  if edPublicacao.Date=0 then
    GR.Aviso('Informe a data da publicação do edital.')
      else
        Close;
end;

procedure TFDadosEdital.FormCreate(Sender: TObject);
begin
  dm.Motivos.Close;
  dm.Motivos.Open;
end;

end.
