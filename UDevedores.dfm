object FDevedores: TFDevedores
  Left = 490
  Top = 382
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'DEVEDORES'
  ClientHeight = 219
  ClientWidth = 480
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 480
    Height = 219
    Align = alClient
    TabOrder = 0
    SkinData.SkinSection = 'TRANSPARENT'
    object P1: TsPanel
      Left = 1
      Top = 1
      Width = 478
      Height = 173
      Align = alTop
      TabOrder = 0
      SkinData.SkinSection = 'PANEL'
      object Grid: TwwDBGrid
        Left = 1
        Top = 1
        Width = 476
        Height = 171
        Cursor = crHandPoint
        Selected.Strings = (
          'ORDEM'#9'2'#9'#'#9'F'
          'NOME'#9'60'#9'Nome'#9'F')
        IniAttributes.Delimiter = ';;'
        IniAttributes.UnicodeIniFile = False
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        BorderStyle = bsNone
        DataSource = dsDevedores
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
        TabOrder = 0
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'Arial'
        TitleFont.Style = []
        TitleLines = 1
        TitleButtons = False
        UseTFields = False
        OnDblClick = GridDblClick
      end
    end
    object btIncluir: TsBitBtn
      Left = 10
      Top = 184
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Caption = 'Incluir'
      TabOrder = 1
      OnClick = btIncluirClick
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object btAlterar: TsBitBtn
      Left = 90
      Top = 184
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Caption = 'Alterar'
      TabOrder = 2
      OnClick = btAlterarClick
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object btExcluir: TsBitBtn
      Left = 170
      Top = 184
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Caption = 'Excluir'
      TabOrder = 3
      OnClick = btExcluirClick
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object btFechar: TsBitBtn
      Left = 250
      Top = 184
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Caption = 'Fechar'
      TabOrder = 4
      OnClick = btFecharClick
      SkinData.SkinSection = 'PAGECONTROL'
    end
  end
  object dsDevedores: TDataSource
    DataSet = dm.RxDevedor
    Left = 114
    Top = 90
  end
end
