unit UIntimacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, sCheckBox, DBCtrls,
  sDBLookupComboBox, sComboBox, Mask, sMaskEdit, sCustomComboEdit,
  sTooledit, ExtCtrls, sPanel, DB;

type
  TFIntimacao = class(TForm)
    P1: TsPanel;
    edData: TsDateEdit;
    cbTipoIntimacao: TsComboBox;
    edDataPublicacao: TsDateEdit;
    lkMotivo: TsDBLookupComboBox;
    ckImprime: TsCheckBox;
    btOk: TsBitBtn;
    btCancelar: TsBitBtn;
    dsMotivos: TDataSource;
    edPrazo: TsDateEdit;
    ckBaixar: TsCheckBox;
    procedure cbTipoIntimacaoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure edDataExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FIntimacao: TFIntimacao;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFIntimacao.cbTipoIntimacaoChange(Sender: TObject);
begin
  if cbTipoIntimacao.ItemIndex<>1 then
  begin
      edDataPublicacao.Clear;
      lkMotivo.KeyValue:=0;
      edDataPublicacao.Enabled:=False;
      lkMotivo.Enabled        :=False;
      ckImprime.Checked       :=True;
      ckImprime.Enabled       :=True;
  end
  else
  begin
      edDataPublicacao.Enabled:=True;
      lkMotivo.Enabled        :=True;
      edDataPublicacao.Date   :=PF.DataFinalFeriados(edData.Date,2);
      ckImprime.Enabled       :=False;
      ckImprime.Checked       :=False;
  end;
end;

procedure TFIntimacao.FormCreate(Sender: TObject);
begin
  dm.Motivos.Close;
  dm.Motivos.Open;

  cbTipoIntimacaoChange(Sender);
end;

procedure TFIntimacao.btOkClick(Sender: TObject);
begin
  if (edData.Date=0) then
  begin
      GR.Aviso('Informe a Data da Intima��o.');
      edData.SetFocus;
      Exit;
  end;

  if (cbTipoIntimacao.ItemIndex=-1) then
  begin
      GR.Aviso('Informe o Tipo da Intima��o.');
      cbTipoIntimacao.SetFocus;
      Exit;
  end;

  if (edDataPublicacao.Enabled) and (edDataPublicacao.Date=0) then
  begin
      GR.Aviso('Informe a Data de Publica��o.');
      edDataPublicacao.SetFocus;
      Exit;
  end;

  if (lkMotivo.Enabled) and (lkMotivo.KeyValue=0) then
  begin
      GR.Aviso('Informe o Motivo da Intima��o por Edital.');
      lkMotivo.SetFocus;
      Exit;
  end;

  dm.vOkGeral:=True;
  Close;
end;

procedure TFIntimacao.btCancelarClick(Sender: TObject);
begin
  dm.vOkGeral:=False;
  Close;
end;

procedure TFIntimacao.edDataExit(Sender: TObject);
begin
  if cbTipoIntimacao.ItemIndex=1 then
    edDataPublicacao.Date:=PF.DataFinalFeriados(edData.Date,2)
      else edDataPublicacao.Clear;
end;

end.
