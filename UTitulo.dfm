object FTitulo: TFTitulo
  Left = 247
  Top = 151
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Protesto'
  ClientHeight = 671
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  DesignSize = (
    784
    671)
  PixelsPerInch = 96
  TextHeight = 14
  object lbStatus: TsLabel
    Left = 6
    Top = 637
    Width = 115
    Height = 21
    Cursor = crHandPoint
    SkinSection = 'TRACKBAR'
    Caption = 'PROTESTADO'
    ParentFont = False
    Layout = tlCenter
    OnClick = lbStatusClick
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -17
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
  end
  object GbTitulo: TsGroupBox
    Left = 8
    Top = 113
    Width = 772
    Height = 201
    Anchors = [akLeft, akTop, akRight]
    Caption = 'T'#237'tulo'
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    SkinData.SkinSection = 'SCROLLSLIDERV'
    CaptionSkin = 'MAINMENU'
    DesignSize = (
      772
      201)
    object sSpeedButton1: TsSpeedButton
      Left = 522
      Top = 140
      Width = 22
      Height = 22
      Cursor = crHandPoint
      Hint = 'MOTIVO DA INTIMA'#199#195'O POR EDITAL'
      ParentShowHint = False
      ShowHint = True
      Visible = False
      OnClick = sSpeedButton1Click
      SkinData.SkinSection = 'GROUPBOX'
      Images = dm.Imagens
      ImageIndex = 1
    end
    object ImTabela: TImage
      Left = 531
      Top = 102
      Width = 16
      Height = 16
      Cursor = crHandPoint
      Hint = 'VISUALIZAR TABELA DE CUSTAS'
      AutoSize = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        000000000000BB8A5EFFBB8A5EFFBB8A5EFFBB8A5EFFBB8A5EFFBB8A5EFFBB8A
        5EFFBB8A5EFFC8A27EFFD6BCA2FFBD8E63FFBB8A5EFFBB8A5EFFBB8A5EFFFFFF
        FF00FFFFFF00BC8B5EFFFFFFFEFFFFFCF7FFFFFEF5FFFFFEF6FFFFFBF4FFFFF7
        EEFFFBF6EFFFF7F9F8FF54A15FFF278834FFEBE5D1FFFFF8ECFFBB8A5EFFFFFF
        FF00FFFFFF00BE8C5EFFFFFFFEFFFFFCF7FFFFFEF5FFFFFEF6FFFFFBF4FFFBF8
        F2FFF7FAF8FF60A86BFF3F9C4CFF5AB768FF228630FFECEEDCFFBB8A5EFFFFFF
        FF00FFFFFF00BF8E5FFFFFFFFEFF7B432BFFFFFFF5FF7B432BFFB49889FFF7FA
        F8FF62A96DFF3E9D4CFF5FC16FFF5FC16FFF59BC69FF258832FFAF8D5EFFFFFF
        FF00FFFFFF00C18F5FFFFFFDFAFFFFFFFCFFFFFFFCFFFEFEFBFFF7FAF8FF5FA8
        6AFF3EA04CFF5EC86FFF5EC86FFF52BB63FF5EC86FFF59C269FF268832FF4096
        4C1AFFFFFF00C39160FFFFFFFCFF7B432BFFFFFFF5FF956A56FFF7F9F8FF53A2
        60FF3EA44DFF5DCE70FF359643FF3D964AFF3EA54EFF5DCE70FF58C86AFF2689
        34FB1F842D16C49260FFFFFFFCFFFFFDF7FFFFFFF5FFFFFFFFFFFBFDFBFFF7F9
        F8FF5AA765FF2D8F3BFF74B47BFFF4F7F5FF479C53FF3EA84FFF5CD571FF56CE
        6BFF288B36FAC79361FFFFFEF6FF7B432BFFFFFFF5FF7B432BFF7B432BFF9D75
        62FFFAFCF8FF866B50FF7B432BFFBCA396FFF2F6F3FF459B52FF3EAB4FFF5BDA
        72FF3DAE4FFFC89561FFFFFFFBFFFFFFF5FFFFFFF5FFFFFFF5FFFFFFF5FFFFFF
        F5FFFFFFF5FFFFFFF5FFFFFFF5FFFFFFF5FFFBF8F1FFF0F5F2FF439A50FF3DAC
        4FFF5BDD72FFCA9662FFFFFFFCFF7B432BFFF7F2E8FF7B432BFF7B432BFF7B43
        2BFFFFFFFCFF7B432BFF7B432BFF7B432BFF7B432BFFFBF9F3FFEFF5F1FF3F99
        4DFF3DA74DFFCC9862FFFFFBF8FFFFFFFCFFFFFFFCFFFFFFFCFFFFFFFCFFFFFF
        FCFFFFFFFCFFFFFFFCFFFFFFFCFFFFFFFCFFFFF3EAFFFFF9F3FFD7BFA7FFF7F9
        F8FF469D52FFCD9963FFFFFFFCFFFFFFFCFFFFFFFCFFFFFFFCFFFFFFFCFFFFFF
        FCFFFFFFFCFFFFFFFCFFFFFFFCFFDDDBDAFFDDDBDAFFDDDBDAFFBB8A5EFFF7FA
        F746F7FAF704CF9A63FFFFFBF8FFFFF9F3FFFFF9F3FF0000A1FF0000A1FF0000
        A1FF0000A1FF00009EFFFFFFFFFFDDDBDAFFDDDBDAFFF5F7F8FFBB8A5EFFFFFF
        FF00FFFFFF00D09C64FFFFFBF8FFFEF5EFFFFFFFF5FFFFFFF5FFFFFFF5FFFFFF
        F5FFFFFFF5FFFFFFF5FFFFFFF5FFE9E5D5FFF9FCFDFFE7D7CAFFC39B7BB7FFFF
        FF00FFFFFF00D29D64FFFFFFFFFFFFFBF8FFFFFBF8FFFFFFFEFFFFFFFEFFFFFF
        FEFFFFFFFEFFFFFFFEFFFFFFFFFFEEEEEEFFEDE0D8FFC19979D7C39B7B1CFFFF
        FF00FFFFFF00D4A067FFD19C64FFCF9A63FFCC9862FFCA9662FFC89461FFC492
        60FFC29060FFBF8E5FFFBD8C5EFFC2976FFFC39B7CD2C39B7B1CFFFFFF00FFFF
        FF00FFFFFF00}
      ShowHint = True
      Transparent = True
      OnClick = ImTabelaClick
    end
    object ImGerar: TImage
      Left = 423
      Top = 102
      Width = 16
      Height = 16
      Cursor = crHandPoint
      Hint = 'GERAR TABELA DE CUSTAS'
      AutoSize = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        000000000000000000120000002C212109722A2A0B872A2A0B872A2A0B872A2A
        0B872A2A0B872A2A0B872A2A0B872A2A0B872A2A0B872A2A0B87212109720000
        002C00000012000000090000001638381782FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FEFFFEFEFDFFFEFEFCFFFDFDFBFFFDFDFAFFFCFCF8FFFEFEF9FF383817820000
        0016000000090000000038381D004949267BFFFFFFFFFFFFFFFFFEFEFEFFFDFD
        FCFFFDFDFBFFFCFCF9FFFBFBF8FFFAFAF6FFF8F8F4FFFBFBF6FF4949267B3838
        1D00000000003F3F240054542F0054542F77FFFFFFFFFF9966FFFDFDFCFFCCCC
        BBFFDDDDCCFFDDDDCCFFE2E2D7FFF8F8F4FFF7F7F2FFFBFBF5FF54542F775454
        2F003F3F2400585833005858330058583376FFFFFEFFFDFDFCFFFDFDFBFFFCFC
        F9FFFBFBF8FFFAFAF6FFF8F8F4FFF7F7F2FFF6F6F0FFFAFAF3FF585833765858
        3300585833005C5C37005C5C37005C5C3774FEFEFDFF777777FFFCFCF9FFCCCC
        BBFFDBDBCAFFD1D1C0FFD8D8C7FFD6D6C5FFF5F5EEFFFAFAF2FF5C5C37745C5C
        37005C5C370061613A0061613A0061613A72FEFEFCFFFCFCF9FFFBFBF8FFFAFA
        F6FFF8F8F4FFF7F7F2FFF6F6F0FFF5F5EEFFF4F4ECFFF9F9EFFF61613A726161
        3A0061613A0065653E0065653E0065653E71FDFDFBFFFF9966FFFAFAF6FFC9C9
        B8FFD8D8C7FFD6D6C5FFDCDCD0FFF4F4ECFFF1F1E7FFF7F7EAFF65653E716565
        3E0065653E0069694200696942006969426FFDFDFAFFFAFAF6FFF8F8F4FFF7F7
        F2FFF6F6F0FFF5F5EEFFF4F4ECFFF1F1E7FFECECDFFFF4F4E5FF6969426F6969
        4200696942006D6D45006D6D45006D6D456DFCFCF8FF6E71C2FFF7F7F2FFCDCD
        BCFFD4D4C3FFCACAB9FFD1D1C0FFCFCFBEFFD6D6C5FFF3F3E2FF6D6D456D6D6D
        45006D6D450071714800717148007171486CFCFCF7FFF7F7F2FFF6F6F0FFF5F5
        EEFFF4F4ECFFF1F1E7FFECECDFFFE8E8D9FFE6E6D5FFF2F2E1FF7171486C7171
        48007171480074744C0074744C0074744C6BFBFBF6FF22AA33FFF5F5EEFFC2C2
        B1FFD1D1C0FFECECDFFFE8E8D9FFE6E6D5FFE5E5D4FFF2F2E1FF74744C6B7474
        4C0074744C0077774E0077774E0077774E69FAFAF4FFF5F5EEFFF4F4ECFFF1F1
        E7FFECECDFFFE8E8D9FFE6E6D5FFA4A493FFA4A493FFA4A493FF4949257C4949
        2500494925007A7A51007A7A51007A7A5168FAFAF3FFF4F4ECFFF1F1E7FFECEC
        DFFFE8E8D9FFE6E6D5FFE5E5D4FFB6B6A5FFFFFFFFFF7A7A51687A7A51257979
        5000797950007D7D53007D7D53007D7D5367FCFCF5FFF9F9EFFFF6F6EAFFF4F4
        E5FFF3F3E2FFF2F2E1FFF2F2E1FFC2C2B1FF7D7D53677D7D53257A7A51007979
        5000797950007E7E54007E7E54007F7F554D7F7F55667F7F55667F7F55667F7F
        55667F7F55667F7F55667F7F55667F7F55667F7F55247D7D53007A7A51007979
        500079795000}
      ShowHint = True
      Transparent = True
      OnClick = ImGerarClick
    end
    object Image2: TImage
      Left = 543
      Top = 15
      Width = 16
      Height = 16
      Cursor = crHandPoint
      Hint = 'TABELA DE C'#211'DIGOS'
      AutoSize = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        000000000000000000120000002C7351309F986C40C4986C40C4986C40C4986C
        40C4986C40C4986C40C4986C40C4986C40C4986C40C4986C40C47351309F0000
        002C0000001200000009000000169E6F40B5FDF4E1FFFBF2DDFFFBF2DCFFFAF1
        DCFFFAF1DBFFFAF0DBFFF9F0DAFFF9EFDAFFF9EED9FFFCF2DFFF9E6F40B50000
        001600000009000000007D573000A67340A2FBF2DDFFF6EED4FFF5EDD3FFF5EC
        D2FFF4EBD1FFF4EAD0FFF3E8CEFFF2E7CDFFF2E5CBFFF8EDD8FFA67340A27D57
        30000000000080583000AB753F00AB753F97FBF2DCFFF5EDD3FFF5ECD2FFF4EB
        D1FFF4EAD0FFF3E8CEFFF2E7CDFFF2E5CBFFF1E4CAFFF8ECD7FFAB753F97AB75
        3F008058300056807000AD763F00AD763F92FAF1DCFFF5ECD2FFF4EBD1FFF4EA
        D0FFF3E8CEFFF2E7CDFFF2E5CBFFF1E4CAFFF0E2C8FFF7ECD6FFAD763F92AD76
        3F00AD763F000089A0002C828400AE773F8DFAF1DBFFF4EBD1FFF4EAD0FFF3E8
        CEFFF2E7CDFFF2E5CBFFF1E4CAFFF0E2C8FFEFE1C7FFF7EBD5FFAE773F8DAE77
        3F00AE773F00008BA2940089A0C6188793E539A1ADFF379FAAFF379EAAFF379E
        AAFF379EA9FF66B0B2FFF0E2C8FFEFE1C7FFEFDFC5FFF7EAD4FFB0783F88B078
        3F00B0783F000199B1BCAEF4FFFF9FF1FEFF9CF0FDFF99EEFCFF95EDFBFF92EB
        F9FF97EDFAFF40ACB7FFEFE1C7FFEFDFC5FFEDDBC1FFF5E7D1FFB2793F83B279
        3F00B2793F00019FB7B89FF1FEFF85ECFDFF83EAFBFF80E8F9FF7EE6F7FF7BE4
        F5FF88E8F7FF43B1BBFFEFDFC5FFEDDBC1FFEAD6BCFFF4E4CEFFB47A3F7FB47A
        3F00B47A3F0001A4BCB59CF0FDFF83EAFBFF80E8F9FF7EE6F7FF7BE4F5FF77E1
        F2FF7BE3F3FF47B5BFFFEDDBC1FFEAD6BCFFE8D0B6FFF3E2CCFFB67B3F7AB67B
        3F00B67B3F0001A8C1B299EEFCFF80E8F9FF7EE6F7FF7BE4F5FF37AEBFFF37AE
        BFFF37AEBFFF3DA6B1FFEAD6BCFFE8D0B6FFE6CDB3FFF2E1CCFFB77C3F76B77C
        3F00B77C3F0001ACC5B095EDFBFF7EE6F7FF7BE4F5FF77E1F2FF45BCCDFFBBF6
        FFFF4BBBC4FFB1CBBDFFE8D0B6FFE6CDB3FFE5CCB2FFF2E1CCFFB97D3F72B97D
        3F00B97D3F0001B0C9AE9CEEFBFF8DEAF8FF87E8F7FF7BE2F3FF4DC4D5FF4DBF
        C6FFB2CCBEFFE8D0B6FFE6CDB3FFC2A485FFC2A485FFC2A485FFA67340A3A673
        4000A673400001B3CC8101B3CCAC20AAB4CF52C5CFFF4FC1C9FF4FC0C8FFB3CC
        BFFFE8D0B6FFE6CDB3FFE5CCB2FFD2B69AFFFFF6E5FFBB7E3F6CBB7E3F26BB7E
        3F00BB7E3F0001B3CC0030A6A900BC7F3F69FBF0DDFFF7E9D4FFF5E7D1FFF4E4
        CEFFF3E2CCFFF2E1CCFFF2E1CCFFDCC2A7FFBC7F3F69BC7F3F25BB7E3F00BB7E
        3F00BB7E3F0001B3CC00BD7F3F00BD7F3F4DBD7F3F67BD7F3F67BD7F3F67BD7F
        3F67BD7F3F67BD7F3F67BD7F3F67BD7F3F67BD7F3F24BC7F3F00BB7E3F00BB7E
        3F008D5F3000}
      ShowHint = True
      Transparent = True
      OnClick = Image2Click
    end
    object lblServentiaAgregada: TLabel
      Left = 735
      Top = 148
      Width = 25
      Height = 48
      Alignment = taCenter
      Caption = '2'
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -40
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblVencimentoAVista: TLabel
      Left = 321
      Top = 93
      Width = 80
      Height = 14
      Caption = 'Venc. A Vista'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edValor: TsCurrencyEdit
      Left = 524
      Top = 75
      Width = 74
      Height = 22
      Hint = 'VALOR DO T'#205'TULO'
      AutoSize = False
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 10
      OnClick = edValorClick
      OnExit = edValorExit
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Valor'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'VALOR DO T'#205'TULO'
      GlyphMode.Blend = 0
      DisplayFormat = '###,##0.00'
    end
    object lkEspecie: TsDBLookupComboBox
      Left = 16
      Top = 34
      Width = 265
      Height = 22
      Hint = 'ESP'#201'CIE DO T'#205'TULO'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyField = 'CODIGO'
      ListField = 'DESCRICAO'
      ListSource = dsTipos
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = lkEspecieClick
      OnKeyPress = lkEspecieKeyPress
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Esp'#233'cie'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
    end
    object cbTipoProtesto: TsComboBox
      Left = 287
      Top = 34
      Width = 101
      Height = 22
      Hint = 'TIPO DE PROTESTO'
      Alignment = taLeftJustify
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Tipo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = -1
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Items.Strings = (
        'N'#227'o pagamento'
        'N'#227'o aceite'
        'N'#227'o devolu'#231#227'o')
    end
    object edNumero: TsEdit
      Left = 566
      Top = 34
      Width = 140
      Height = 22
      Hint = 'N'#218'MERO DE IDENTIFICA'#199#195'O DO T'#205'TULO (POR EX.: N'#186' DO CHEQUE)'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 20
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'N'#186' do T'#237'tulo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edBanco: TsEdit
      Left = 16
      Top = 121
      Width = 165
      Height = 22
      Hint = 'N'#218'MERO DO BANCO ASSOCIADO AO T'#205'TULO'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 20
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 12
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Banco'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edDataVencimento: TsDateEdit
      Left = 311
      Top = 74
      Width = 100
      Height = 22
      Hint = 'DATA DE VENCIMENTO DO T'#205'TULO'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data Vencimento'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DE VENCIMENTO DO T'#205'TULO'
      GlyphMode.Blend = 0
    end
    object cbTipoIntimacao: TsComboBox
      Left = 695
      Top = 74
      Width = 70
      Height = 22
      Hint = 'TIPO DA INTIMA'#199#195'O POR EDITAL'
      Alignment = taLeftJustify
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Tipo da Intima'#231#227'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = 0
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 15
      Text = 'Pessoal'
      Visible = False
      OnChange = cbTipoIntimacaoChange
      OnClick = cbTipoIntimacaoClick
      Items.Strings = (
        'Pessoal'
        'Edital'
        'Carta')
    end
    object edDataIntimacao: TsDateEdit
      Left = 550
      Top = 140
      Width = 65
      Height = 22
      Hint = 'DATA DA INTIMA'#199#195'O (RETORNO)'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 16
      Text = '  /  /    '
      Visible = False
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data da Intima'#231#227'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DA INTIMA'#199#195'O (RETORNO)'
      GlyphMode.Blend = 0
    end
    object edTotal: TsCurrencyEdit
      Left = 462
      Top = 121
      Width = 85
      Height = 22
      Hint = 'SALDO A PROTESTAR'
      AutoSize = False
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 19
      OnClick = edTotalClick
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Total'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'SALDO A PROTESTAR'
      GlyphMode.Blend = 0
      DisplayFormat = '###,##0.00'
    end
    object edDataPublicacao: TsDateEdit
      Left = 621
      Top = 140
      Width = 65
      Height = 22
      Hint = 'DATA DA PUBLICA'#199#195'O DO EDITAL'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 17
      Text = '  /  /    '
      Visible = False
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data da Publica'#231#227'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DA PUBLICA'#199#195'O DO EDITAL'
      GlyphMode.Blend = 0
    end
    object edCustas: TsCurrencyEdit
      Left = 382
      Top = 121
      Width = 74
      Height = 22
      Hint = 'VALOR TOTAL COBRADO PELA PR'#193'TICA DO ATO'
      AutoSize = False
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 18
      OnClick = edCustasClick
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Custas'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'VALOR TOTAL COBRADO PELA PR'#193'TICA DO ATO'
      GlyphMode.Blend = 0
      DisplayFormat = '#####0.00'
    end
    object edDataTitulo: TsDateEdit
      Left = 205
      Top = 74
      Width = 100
      Height = 22
      Hint = 'DATA DE VENCIMENTO DO T'#205'TULO'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data do T'#237'tulo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DE VENCIMENTO DO T'#205'TULO'
      GlyphMode.Blend = 0
    end
    object edDataEnvio: TsDateEdit
      Left = 417
      Top = 75
      Width = 100
      Height = 22
      Hint = 'DATA DO ENVIO DA INTIMA'#199#195'O'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 9
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data do Envio'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DO ENVIO DA INTIMA'#199#195'O'
      GlyphMode.Blend = 0
    end
    object btObs: TsBitBtn
      Left = 554
      Top = 111
      Width = 211
      Height = 32
      Cursor = crHandPoint
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Observa'#231#227'o'
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00EEEEEEEEEEEE
        EEEEEEEE0000EEEEEEEE00000000000E0000EEEEEEEE0FFFFFFFFF0E0000EE00
        00000FCCCCCCCF0E0000E07888880FFFFFFFFF0E0000E0F777770FCCCCCCCF0E
        0000E0F777770FFFFFFFFF0E0000E0F777770FCCCCFF770E0000E0F777770FFF
        FFF0000E0000E0F777770FCCCF80FF0E0000E0F777770FFFFF80F0EE0000E0F7
        7777000000000EEE0000E0F77777777777780EEE0000E0F70000000000780EEE
        0000E0F70BBBBBBBB0780EEE0000E0FF80BFFFFF08F70EEE0000EE00000B88F0
        0000EEEE0000EEEEEE0BFFF0EEEEEEEE0000EEEEEE000000EEEEEEEE0000EEEE
        EEEEEEEEEEEEEEEE0000}
      Layout = blGlyphRight
      TabOrder = 20
      TabStop = False
      OnClick = btObsClick
      ImageIndex = 0
      SkinData.SkinSection = 'SCROLLSLIDERH'
    end
    object cbConv: TsDBComboBox
      Left = 712
      Top = 34
      Width = 53
      Height = 22
      Hint = 
        'INDICA SE O ATO FOI CELEBRADO POR MEIO DO ATO NORMATIVO CONJUNTO' +
        ' 05/2005'
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight]
      Color = clWhite
      DataField = 'CONVENIO'
      DataSource = dsRXPortador
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'S'
        'N')
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnExit = cbConvExit
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Conv'#234'nio'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
    end
    object edAgencia: TsEdit
      Left = 187
      Top = 121
      Width = 45
      Height = 22
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 13
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Ag'#234'ncia'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edConta: TsEdit
      Left = 237
      Top = 121
      Width = 139
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 14
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Conta'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edSaldo: TsCurrencyEdit
      Left = 604
      Top = 75
      Width = 85
      Height = 22
      Hint = 'SALDO A PROTESTAR'
      AutoSize = False
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 11
      OnClick = edValorClick
      OnExit = edValorExit
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Saldo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'SALDO A PROTESTAR'
      GlyphMode.Blend = 0
      DisplayFormat = '###,##0.00'
    end
    object edLAE: TsEdit
      Left = 474
      Top = 34
      Width = 85
      Height = 22
      Hint = 'C'#211'DIGO DO LIVRO ADICIONAL'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnKeyPress = edLAEKeyPress
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'C'#243'digo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object cbAceite: TsComboBox
      Left = 394
      Top = 34
      Width = 74
      Height = 22
      Hint = 'INFORMA SE O T'#205'TULO FOI ACEITE PELO DEVEDOR'
      Alignment = taLeftJustify
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Aceite'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = -1
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Items.Strings = (
        ''
        'Aceito'
        #209' Aceito')
    end
    object edtArquivoD: TsEdit
      Left = 16
      Top = 168
      Width = 165
      Height = 22
      Hint = 'Arquivo de Importa'#231#227'o para Serventia Agredada (Arquivo D)'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 20
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 21
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Arquivo D (Exp. Serv. Ag.)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edtArquivoR: TsEdit
      Left = 187
      Top = 168
      Width = 165
      Height = 22
      Hint = 'Arquivo de Retorno da Serventia Agredada (Arquivo R)'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 20
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 22
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Arquivo R (Retorno Serv. Ag.)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edtServentiaAgregada: TsEdit
      Left = 358
      Top = 168
      Width = 371
      Height = 22
      Hint = 'Nome da Serventia Agregada'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 20
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 23
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Serventia Agregada'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edtNossoNumero: TsEdit
      Left = 16
      Top = 74
      Width = 183
      Height = 22
      Hint = 'N'#218'MERO DE CONTROLE DO T'#205'TULO NO BANCO'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 20
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Nosso N'#250'mero'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
  end
  object btSalvar: TsBitBtn
    Left = 607
    Top = 633
    Width = 80
    Height = 30
    Cursor = crHandPoint
    Anchors = [akTop, akRight]
    Caption = 'Salvar'
    Enabled = False
    TabOrder = 4
    OnClick = btSalvarClick
    ImageIndex = 3
    Images = dm.Imagens
    SkinData.SkinSection = 'BUTTON'
  end
  object btCancelar: TsBitBtn
    Left = 690
    Top = 633
    Width = 88
    Height = 30
    Cursor = crHandPoint
    Anchors = [akTop, akRight]
    Caption = 'Cancelar'
    Enabled = False
    TabOrder = 5
    OnClick = btCancelarClick
    ImageIndex = 5
    Images = dm.Imagens
    SkinData.SkinSection = 'BUTTON'
  end
  object GbProtocolo: TsGroupBox
    Left = 8
    Top = 47
    Width = 772
    Height = 62
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Protocolo'
    Enabled = False
    TabOrder = 0
    SkinData.SkinSection = 'SCROLLSLIDERV'
    CaptionSkin = 'MAINMENU'
    DesignSize = (
      772
      62)
    object ImNovoCCT: TsImage
      Left = 728
      Top = 13
      Width = 16
      Height = 16
      Cursor = crHandPoint
      Hint = 'CARREGAR NOVO CCT'
      AutoSize = True
      Center = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000049B6401C48B4439F48B344C748B3439B47B3423247B3
        449A48B344C749B3449E49B6422349B2438548B344C348B444C447B3456B55AA
        55030000000043AE431348B447E748B447FF48B447FF48B447FF48B447FF48B4
        47FF48B448FF48B448FF48B448FF48B448FF48B448FF48B448FF48B448FF48B4
        48A700FF000148B54A8648B54BFF48B54BFF48B54BFF48B54BFF48B54BFF77C8
        79FFC3E7C4FF4FB852FF48B54BFF48B54BFF48B54BFF48B54BFF48B54BFF48B5
        4BFF49B64D5047B64EB847B54EFF47B54FFF47B54FFF47B54FFF74C77AFFF6FB
        F6FFFFFFFFFFA3DBA7FF47B64FFF47B64FFF47B64FFF47B64FFF47B64FFF47B6
        4FFF48B64FA447B751A947B652FF47B652FF47B652FF79CA81FFFBFDFBFFC4E8
        C7FFABDEB0FFFBFDFBFF65C26EFF47B652FF47B652FF47B652FF47B652FF47B6
        53FF47B753BE47B8565646B756FF46B756FF46B756FF99D7A2FFB1E1B7FF49B8
        59FF4DBA5CFFE7F6E9FFD6EFD9FF48B858FF46B756FF46B756FF46B756FF46B7
        56FF46B756A08080800246B9599846B859FF46B85AFF46B85AFF46B85AFF46B8
        5AFF46B85AFF7ACC89FFFEFFFEFF99D8A4FF46B85AFF46B85AFF46B85AFF46B8
        5AFF47B85948000000000000000044BB5D4746B95DFF46B95DFF46B95DFF46B9
        5DFF46B95DFF46B95DFFB6E4BFFFF6FCF7FF5DC271FF46B95DFF46B95DFF46B9
        5DA100FF0001000000000000000044BB660F45BA61FB45BA61FF45BA61FF45BA
        61FF45BA61FF45BA61FF4CBD67FFE1F4E6FFAFE2BBFF45BA61FF46B9616E8080
        80020000000000000000000000000000000045BA637E45BB64FF45BB64FF45BB
        64FC45BB64FF45BB64FF45BB65FF4FBF6DFF4EBE6CFF45BB65F733CC66050000
        0000000000000000000000000000000000000000000044BB6A2946BE693345BD
        685D44BC68FF44BC68FF44BC68FF44BC68FF44BC68FF44BC6788000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000044BE6D5244BC6CB844BD6CCC45BD6CB644BD6C6155AA5503000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000}
      ShowHint = True
      Transparent = True
      Visible = False
      OnClick = ImNovoCCTClick
      SkinData.SkinSection = 'CHECKBOX'
    end
    object ImLimparCCT: TsImage
      Left = 749
      Top = 13
      Width = 16
      Height = 16
      Cursor = crHandPoint
      Hint = 'LIMPAR CCT'
      AutoSize = True
      Center = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF001D5FDE4E356BD9CA1F61
        E032FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001D5FDE4E5E81D2FDADA3B2FF4B73
        CFF61F61E032FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF001D5FDE4E688CDCFDCAC2C9FFBEB4BDFFB0A4
        B2FF577DD3F7ECF1FBCAF1F5FBCFFFFFFF24FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF001D5FDE4E7297E5FDE3DFE3FFD7D0D6FFCAC2C9FFC3B9
        C2FFEEEBF0FF7397DDFF517DD2FFF0F2F9E7FFFFFF24FFFFFF00FFFFFF00FFFF
        FF00FFFFFF001D5FDE4E779EEBFDFCFBFCFFF0EDEFFFE3DFE3FFDAD3D9FFF3F1
        F4FF789FE3FF2964CFFF255DC9FF517DD2FFF0F2F9E7FFFFFF24FFFFFF00FFFF
        FF00FFFFFF003F78E3CAF7FAFEFFFFFFFFFFFCFBFCFFF1EEF0FFF7F7F9FF7DA6
        E9FF316FD8FF265EC8FF265EC9FF255ECAFF517DD2FFF0F2F9E7FFFFFF24FFFF
        FF00FFFFFF002666E1346490E9F6FAFCFEFFFFFFFFFFFCFDFEFF83ADEFFF397B
        E1FF2963CCFF2962CCFF2962CCFF245CC7FF255ECAFF517DD2FFF0F2F9E7FFFF
        FF24FFFFFF00FFFFFF001F61E0326D97EAF7FCFDFFFF88B5F5FF4286EBFF2D67
        D0FF2C67D0FF2E6AD3FF265EC7FF2A65CFFF245CC6FF255ECAFF517DD2FFF0F2
        F9E7FFFFFF24FFFFFF00FFFFFF00EDF2FCCA8DBCFBFF4A92F4FF306CD3FF306C
        D4FF3371D8FF2861CAFF306ED6FF245CC6FF2A65CFFF245CC6FF255ECAFF537F
        D3FFFBFBFCAEFFFFFF00FFFFFF00F4F9FECF78B2FDFF4990F3FF3776DCFF3878
        DEFF2A63CCFF3776DDFF265EC7FF316ED7FF255DC6FF2962CCFF2B65CFFFB1C6
        ECFFFFFFFF7EFFFFFF00FFFFFF00FFFFFF24F3F6FEE778B2FDFF4D96F8FF306C
        D4FF3D7FE4FF2760C9FF3877DEFF275FC8FF2E6AD3FF3370D8FFB4C9EFFFD6E2
        F9AFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF24F3F6FEE778B2FDFF4E97
        F9FF2E69D1FF3E80E5FF2861CAFF3371D8FF3B7CE1FFB7CEF3FFD6E2F9AFFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF24F3F6FEE778B2
        FDFF4E97F9FF2E6AD1FF3878DEFF4487EBFFBAD2F6FFD6E2F9AFFFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF24F3F6
        FEE778B2FDFF4D96F8FF4C93F4FFBDD6F9FFD6E2F9AFFFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF24F3F6FEE77AB3FDFFC0DAFDFFD6E2F9AFFFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF24FBFCFEAEFFFFFF7EFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00}
      ShowHint = True
      Transparent = True
      Visible = False
      OnClick = ImLimparCCTClick
      SkinData.SkinSection = 'CHECKBOX'
    end
    object edDataProtocoloD: TsDateEdit
      Left = 16
      Top = 31
      Width = 101
      Height = 22
      Hint = 'DATA DO PROTOCOLO'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data (Distribuidor)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DO PROTOCOLO'
      GlyphMode.Blend = 0
    end
    object edProtocoloD: TsEdit
      Left = 123
      Top = 31
      Width = 101
      Height = 22
      Hint = 'N'#218'MERO DO PROTOCOLO'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 20
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnKeyPress = edProtocoloDKeyPress
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'N'#186' Protocolo (D.)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edRecibo: TsEdit
      Left = 444
      Top = 31
      Width = 74
      Height = 22
      Hint = 'N'#218'MERO DO RECIBO'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 20
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnKeyPress = edReciboKeyPress
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Recibo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object cbEndosso: TsComboBox
      Left = 524
      Top = 31
      Width = 141
      Height = 22
      Hint = 'TIPO DE ENDOSSO'
      Alignment = taLeftJustify
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Tipo de Endosso'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = -1
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      Items.Strings = (
        'Mandato'
        'Translativo'
        'Sem Endosso')
    end
    object cbTipoApresentacao: TsComboBox
      Left = 671
      Top = 31
      Width = 92
      Height = 22
      Hint = 'TIPO DE APRESENTA'#199#195'O'
      Anchors = [akLeft, akTop, akRight]
      Alignment = taLeftJustify
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Tipo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = -1
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      Items.Strings = (
        'B'
        'C'
        'I')
    end
    object edLivroProtocolo: TsEdit
      Left = 550
      Top = -6
      Width = 54
      Height = 22
      Hint = 'N'#218'MERO DO LIVRO DE PROTOCOLO'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
      Visible = False
      OnKeyPress = edLivroProtocoloKeyPress
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Livro'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edFolhaProtocolo: TsEdit
      Left = 610
      Top = -6
      Width = 42
      Height = 22
      Hint = 'FOLHA INICIAL DO LIVRO DE PROTOCOLO'
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      Visible = False
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Folha'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edCCT: TsEdit
      Left = 658
      Top = -6
      Width = 85
      Height = 22
      Hint = 'CCT UTILIZADO NO APONTAMENTO'
      CharCase = ecUpperCase
      Color = 11468718
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 9
      Visible = False
      OnExit = edSeloRegistroExit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'CCT'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edDataProtocoloSA: TsDateEdit
      Left = 230
      Top = 31
      Width = 101
      Height = 22
      Hint = 'DATA DO PROTOCOLO'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data (Serv. Ag.)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DO PROTOCOLO'
      GlyphMode.Blend = 0
    end
    object edProtocoloSA: TsEdit
      Left = 337
      Top = 31
      Width = 101
      Height = 22
      Hint = 'N'#218'MERO DO PROTOCOLO'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 20
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnKeyPress = edProtocoloSAKeyPress
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'N'#186' Protocolo (SA)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
  end
  object GbPartes: TsGroupBox
    Left = 6
    Top = 320
    Width = 772
    Height = 307
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Participantes     '
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    SkinData.SkinSection = 'SCROLLSLIDERV'
    CaptionSkin = 'MAINMENU'
    DesignSize = (
      772
      307)
    object ImAviso1: TImage
      Left = 243
      Top = 32
      Width = 16
      Height = 16
      Hint = 'DOCUMENTO INV'#193'LIDO!'
      AutoSize = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        000000000000FFFFFF0000000000000000000000000000000000000000000000
        0000AA640070AA64007000000000000000000000000000000000000000000000
        0000FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
        0000AA6400CFAA6400CF00000000000000000000000000000000000000000000
        0000FFFFFF00FFFFFF000000000000000000000000000000000000000000AA64
        0020BF8B3DFFBF8B3DFFAA640020000000000000000000000000000000000000
        0000FFFFFF00FFFFFF000000000000000000000000000000000000000000AA64
        0060DABB8DFFDABB8DFFAA640060000000000000000000000000000000000000
        0000FFFFFF00FFFFFF00000000000000000000000000AA640060AA6400AFB577
        1FFFF4ECDDFFF4ECDDFFB5771FFFAA6400AFAA64006000000000000000000000
        0000FFFFFF00FFFFFF0000000000AA640010AA6400BFCA9E5DFFF4ECD8FFFFFF
        F8FFFFFFFFFFFFFFFEFFFFFFF7FFF4ECD8FFCA9E5DFFAA6400BFAA6400100000
        0000FFFFFF00FFFFFF00AA640010AA6400CFDFC59BFFFFFEF7FFFFFCF5FFFFFB
        F6FF010BBFFF010BBFFFFFFBF6FFFFFCF5FFFFFEF7FFDFC59BFFAA6400CFAA64
        0010FFFFFF00FFFFFF00AA640080DABB8CFFFFFAF0FFFFF6EAFFFFF6EAFFFFF6
        EAFF0021C2FF0021C2FFFFF6EAFFFFF6EAFFFFF6EAFFFFFBF2FFDABB8CFFAA64
        0080FFFFFF00FFFFFF00B57921EFFFFBF3FFFFF1DEFFFFF1DEFFFFF1DEFFFFF1
        DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFFDF7FFB579
        21EFFFFFFF00FFFFFF00C5944FFFFFF3E3FFFFEDD5FFFFEDD5FFFFEDD5FFFFED
        D5FF0007DAFF0007DAFFFFEDD5FFFFEDD5FFFFEDD5FFFFEDD5FFFFF6E8FFC594
        4FFFFFFFFF00FFFFFF00C5944FFFFFF0DCFFFFEACDFFFFEACDFFFFEACDFFFFEA
        CDFF0012E5FF0012E5FFFFEACDFFFFEACDFFFFEACDFFFFEACDFFFFF4E5FFC594
        4FFFFFFFFF00FFFFFF00B57922EFFFF5E6FFFFE7C8FFFFE7C8FFFFE7C8FFFFE7
        C8FF001DD5FF001DD5FFFFE7C8FFFFE7C8FFFFE7C8FFFFE7C8FFFFF6E9FFB579
        22EFFFFFFF00FFFFFF00AA640080DABA8BFFFFEBD0FFFFE6C5FFFFE6C5FFFFE6
        C5FF0029C2FF0029C2FFFFE6C5FFFFE6C5FFFFE6C5FFFFEBD0FFDABB8FFFAA64
        0080FFFFFF00FFFFFF00AA640010B17014CFEAD7BBFFFFEED7FFFFE6C5FFFFE6
        C5FF0024AFFF0024AFFFFFE6C5FFFFE6C5FFFFEFDAFFEAD7BBFFB17014CFAA64
        0010FFFFFF00FFFFFF0000000000AA640010AA6400CFCFA870FFF4E9D8FFFFF4
        E6FFFFF3E2FFFFF3E2FFFFF4E6FFF4E9D8FFCFA870FFAA6400CFAA6400100000
        0000FFFFFF00FFFFFF00000000000000000000000000AA640060AA6400CFBA81
        30FFBF8B40FFBF8B40FFBA8130FFAA6400CFAA64006000000000000000000000
        0000FFFFFF00}
      ShowHint = True
      Transparent = True
      Visible = False
    end
    object ImOk1: TImage
      Left = 243
      Top = 32
      Width = 16
      Height = 16
      Hint = 'DOCUMENTO OK!'
      AutoSize = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000939393F9939393F9939393F9939393F99393
        93F9939393F9939393F9939393F9939393F9939393F9939393F9939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF939393F90000
        0000000000000000000000000000939393F900750CF500750CF5EEEEEEFFEDED
        EDFFECECECFFEBEBEBFFEAEAEAFFE9E9E9FFE8E8E8FFFFFFFFFF939393F90000
        000000000000000000000000000000750CF5D3FFE9FF8EFDB4FF00750CF5EFEF
        EFFFEEEEEEFFEDEDEDFFECECECFFEBEBEBFFEAEAEAFFFFFFFFFF939393F90000
        0000000000000000000000750CF5D3FFE9FF41EF7DFF58EF8BFF8EF9B2FF0075
        0CF5F0F0F0FFEFEFEFFFEEEEEEFFEDEDEDFFECECECFFFFFFFFFF939393F90000
        00000000000000750CF5D3FFE9FF7FFFAEFF76BD8CFF00750CF56FF79EFF84F2
        A9FF00750CF5F1F1F1FFF0F0F0FFEFEFEFFFEEEEEEFFFFFFFFFF939393F90000
        00000000000050795D3500750CF5A7DBB7FF00750CF5F6F6F6FF00750CF588FD
        B0FF8BF4AFFF00750CF5F1F1F1FFF1F1F1FFF0F0F0FFFFFFFFFF939393F90000
        000000000000000000000014003800750CF5FFFFFFFFF8F8F8FFF2F2F2FF0075
        0CF5D3FFE9FF00750CF5E7ECE8FFF2F2F2FFF1F1F1FFFFFFFFFF939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFAFAFAFFF9F9F9FFF2F2
        F2FF00750CF5C5C5C5FFF5F5F5FFF4F4F4FFF3F3F3FFF5F5F5FF939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFBFBFBFFFAFAFAFFFAFA
        FAFFF9F9F9FFF8F8F8FF939393F9939393F9939393F9939393F9939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFDFDFDFFFCFCFCFFFBFB
        FBFFFAFAFAFFFAFAFAFF939393F9E1E1E1FFE1E1E1FFB5B5B5F9939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFFFFFFFFFEFEFEFFFDFD
        FDFFFCFCFCFFFBFBFBFF939393F9E1E1E1FFB5B5B5F9939393F9000000000000
        0000000000000000000000000000939393F9FFFFFFFFFFFFFFFFFFFFFFFFFEFE
        FEFFFEFEFEFFFDFDFDFF939393F9B5B5B5F9939393F900000000000000000000
        0000000000000000000000000000939393F9939393F9939393F9939393F99393
        93F9939393F9939393F9939393F9939393F90000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000}
      ShowHint = True
      Transparent = True
      Visible = False
    end
    object Image1: TImage
      Left = 84
      Top = 2
      Width = 16
      Height = 16
      AutoSize = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        0000000000000000000F00000032000000330000003300000033000000330000
        003300000033000000320000000F000000000000000000000000000000000000
        000000000000753D0D6FA8540CFBA7530BFFA7520AFFA7520AFFA7520AFFA752
        0AFFA7530BFFAA540BFB7D3A076D298373000000000236826E0036826E003682
        6E0036836F00B25F18FFD8995DFFE3A971FFE3A870FFE3A770FFE3A770FFE3A8
        70FFE4A971FFDA995DFFBF5D11FF000000330A19153D00000033000000330000
        00320000000FC27937FFEAC097FFE1A261FFE19F5CFFE19D59FFE19D59FFE19F
        5DFFE1A261FFECC097FFC8681BFF268272FF34856FFF337E6CFF337E6CFF347F
        6CFB24584A6FCF8D4FFFF1D6BCFFF0B776FFF0BA7BFFF5FAFDFFEBEFF2FFF0B9
        79FFF1B877FFF5D7BDFFD27224FF45AE88FF4BA980FF55AE86FF56AF87FF50A5
        83FF338270FFD08843FFF2DBC5FFFAE6CEFFFFF8EAFFFFFEFEFFF6F5F3FFF0E4
        D3FFFFE7CEFFFAD9C1FFC58034FF45AC87FF4EAB83FF39A275FF3EA577FF75C4
        A2FF318472FFD58D460DD88C40D0EEBA86FFAFC7CEFF5D9FC3FF3C7CA0FF7684
        88FFF8B983FFB28F47FF44AD7AFFFDFAFCFFF6EFEFFF3BAF81FF35AF7CFF95DA
        BEFF308674FFDB904600E5934300E19040288DA2A3EA67A3C5FF5089A9FF5E73
        70FFA4C9A1FF87E4C5FFD5F4E7FFFFFFFDFFFFF5F2FFC6DFCFFF93E4C5FF9ADD
        C6FF308977FDF09842002F7BB1001127373F78BDE2FF7FC0E3FF71AFD2FF5A97
        BFFF2B847CE263B9A1FF99C8D7FF63A0C2FF437C9FFF5D8693FF68BAA4FF2D8B
        79D0338E7D0E0138730005376D00012C61EAA7EFFFFF8ED1F0FF7EBFDEFF76B3
        D5FF002058D82D9678125D9FAFE86CA3C3FF548AA9FF306D7DEA2C8B73283393
        7B0036927E0010447400104372000A3A6AFF69AACEFF79B3D6FF8CCCECFF80BF
        E0FF072356FF000000297FBFE1FF81C0E2FF72B0D1FF5F99BFFF1728373F487D
        AB00399A7F00154A7A0015497900114374FF2D77A9FF4E89B7FF517AA5FF334F
        7BFF0D2C5CFF042E5FD6A8EFFFFF8FD1F0FF7FBFDEFF76B4D5FF052A5DEA0C33
        67000E336A001343720013427100103E6EFC287BAEFF418ABAFF407CABFF254A
        77FF0F3263E30A3B6BFF69ABCEFF79B5D6FF8CCCECFF81C0E0FF0B2B5BFF1233
        620014346300124275001242750011407322134879FB1E6193FF1D5787FF1340
        70F710407408114474FF2D78A9FF4E89B7FF517AA5FF33507BFF103160FF1536
        650015376600124275001242750012417400154C7C00103F7200103F73101543
        720011417500113E6EF9287BAEFF4089BAFF407CABFF244A77FF103466FC1337
        680013376800124275001242750012417400164D7E0012417400124175001643
        730012417500113F7321124778FC1D5F91FF1C5786FF124070FC114275221243
        760012437600}
      ShowHint = False
      Transparent = True
    end
    object sbSacador: TsSpeedButton
      Left = 337
      Top = 57
      Width = 26
      Height = 22
      Cursor = crHandPoint
      Hint = 'MAIS INFORMA'#199#213'ES DO SACADOR'
      ParentShowHint = False
      ShowHint = True
      OnClick = sbSacadorClick
      SkinData.SkinSection = 'MAINMENU'
      Images = dm.Imagens
      ImageIndex = 8
    end
    object sbPortador: TsSpeedButton
      Left = 707
      Top = 29
      Width = 25
      Height = 22
      Cursor = crHandPoint
      Hint = 'MAIS INFORMA'#199#213'ES DO PORTADOR'
      ParentShowHint = False
      ShowHint = True
      OnClick = sbPortadorClick
      SkinData.SkinSection = 'FORMTITLE'
      Images = Gdm.Im16
      ImageIndex = 26
    end
    object sbLocalizar: TsSpeedButton
      Left = 737
      Top = 29
      Width = 25
      Height = 22
      Cursor = crHandPoint
      Hint = 'LOCALIZAR PORTADOR'
      ParentShowHint = False
      ShowHint = True
      OnClick = sbLocalizarClick
      SkinData.SkinSection = 'FORMTITLE'
      Images = Gdm.Im16
      ImageIndex = 21
    end
    object edCedente: TsEdit
      Left = 417
      Top = 57
      Width = 347
      Height = 22
      Hint = 'NOME DO CEDENTE'
      Anchors = [akLeft, akTop, akRight]
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 100
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnExit = edCedenteExit
      DisabledKind = []
      SkinData.SkinSection = 'COMBOBOX'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Cedente'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
    end
    object cbTipoApr: TsDBComboBox
      Left = 57
      Top = 29
      Width = 50
      Height = 22
      Style = csDropDownList
      Color = clWhite
      DataField = 'TIPO'
      DataSource = dsRXPortador
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'F'
        'J')
      ParentFont = False
      TabOrder = 0
      OnChange = cbTipoAprChange
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Portador'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      DisabledKind = []
      SkinData.SkinSection = 'COMBOBOX'
    end
    object edDocApr: TsDBEdit
      Left = 110
      Top = 29
      Width = 131
      Height = 22
      Hint = 'DOCUMENTO DO PORTADOR'
      Color = clWhite
      DataField = 'DOCUMENTO'
      DataSource = dsRXPortador
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnChange = edDocAprChange
      OnClick = edDocAprClick
      OnExit = edDocAprExit
      DisabledKind = []
      SkinData.SkinSection = 'COMBOBOX'
    end
    object edCodigo: TsDBEdit
      Left = 262
      Top = 29
      Width = 58
      Height = 22
      Hint = 'C'#211'DIGO DO PORTADOR'
      Color = clWhite
      DataField = 'CODIGO'
      DataSource = dsRXPortador
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnExit = edCodigoExit
      DisabledKind = []
      SkinData.SkinSection = 'EDIT'
    end
    object edApresentante: TsDBEdit
      Left = 324
      Top = 29
      Width = 378
      Height = 22
      Hint = 'NOME DO PORTADOR'
      Anchors = [akLeft, akTop, akRight]
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'NOME'
      DataSource = dsRXPortador
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnExit = edApresentanteExit
      DisabledKind = []
      SkinData.SkinSection = 'COMBOBOX'
    end
    object edSacador: TsDBEdit
      Left = 57
      Top = 57
      Width = 278
      Height = 22
      Hint = 'NOME DO SACADOR'
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'NOME'
      DataSource = dsRxSacador
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnExit = edSacadorExit
      DisabledKind = []
      SkinData.SkinSection = 'COMBOBOX'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Sacador'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
    end
    object gbDevedor: TsGroupBox
      Left = 2
      Top = 85
      Width = 768
      Height = 220
      Align = alBottom
      Caption = 'Devedor'
      TabOrder = 6
      SkinData.SkinSection = 'SCROLLSLIDERV'
      object ImAviso2: TImage
        Left = 223
        Top = 21
        Width = 16
        Height = 16
        Hint = 'DOCUMENTO INV'#193'LIDO!'
        AutoSize = True
        ParentShowHint = False
        Picture.Data = {
          0B54504E474772617068696336040000424D3604000000000000360000002800
          0000100000001000000001002000000000000004000000000000000000000000
          000000000000FFFFFF0000000000000000000000000000000000000000000000
          0000AA640070AA64007000000000000000000000000000000000000000000000
          0000FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
          0000AA6400CFAA6400CF00000000000000000000000000000000000000000000
          0000FFFFFF00FFFFFF000000000000000000000000000000000000000000AA64
          0020BF8B3DFFBF8B3DFFAA640020000000000000000000000000000000000000
          0000FFFFFF00FFFFFF000000000000000000000000000000000000000000AA64
          0060DABB8DFFDABB8DFFAA640060000000000000000000000000000000000000
          0000FFFFFF00FFFFFF00000000000000000000000000AA640060AA6400AFB577
          1FFFF4ECDDFFF4ECDDFFB5771FFFAA6400AFAA64006000000000000000000000
          0000FFFFFF00FFFFFF0000000000AA640010AA6400BFCA9E5DFFF4ECD8FFFFFF
          F8FFFFFFFFFFFFFFFEFFFFFFF7FFF4ECD8FFCA9E5DFFAA6400BFAA6400100000
          0000FFFFFF00FFFFFF00AA640010AA6400CFDFC59BFFFFFEF7FFFFFCF5FFFFFB
          F6FF010BBFFF010BBFFFFFFBF6FFFFFCF5FFFFFEF7FFDFC59BFFAA6400CFAA64
          0010FFFFFF00FFFFFF00AA640080DABB8CFFFFFAF0FFFFF6EAFFFFF6EAFFFFF6
          EAFF0021C2FF0021C2FFFFF6EAFFFFF6EAFFFFF6EAFFFFFBF2FFDABB8CFFAA64
          0080FFFFFF00FFFFFF00B57921EFFFFBF3FFFFF1DEFFFFF1DEFFFFF1DEFFFFF1
          DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFFDF7FFB579
          21EFFFFFFF00FFFFFF00C5944FFFFFF3E3FFFFEDD5FFFFEDD5FFFFEDD5FFFFED
          D5FF0007DAFF0007DAFFFFEDD5FFFFEDD5FFFFEDD5FFFFEDD5FFFFF6E8FFC594
          4FFFFFFFFF00FFFFFF00C5944FFFFFF0DCFFFFEACDFFFFEACDFFFFEACDFFFFEA
          CDFF0012E5FF0012E5FFFFEACDFFFFEACDFFFFEACDFFFFEACDFFFFF4E5FFC594
          4FFFFFFFFF00FFFFFF00B57922EFFFF5E6FFFFE7C8FFFFE7C8FFFFE7C8FFFFE7
          C8FF001DD5FF001DD5FFFFE7C8FFFFE7C8FFFFE7C8FFFFE7C8FFFFF6E9FFB579
          22EFFFFFFF00FFFFFF00AA640080DABA8BFFFFEBD0FFFFE6C5FFFFE6C5FFFFE6
          C5FF0029C2FF0029C2FFFFE6C5FFFFE6C5FFFFE6C5FFFFEBD0FFDABB8FFFAA64
          0080FFFFFF00FFFFFF00AA640010B17014CFEAD7BBFFFFEED7FFFFE6C5FFFFE6
          C5FF0024AFFF0024AFFFFFE6C5FFFFE6C5FFFFEFDAFFEAD7BBFFB17014CFAA64
          0010FFFFFF00FFFFFF0000000000AA640010AA6400CFCFA870FFF4E9D8FFFFF4
          E6FFFFF3E2FFFFF3E2FFFFF4E6FFF4E9D8FFCFA870FFAA6400CFAA6400100000
          0000FFFFFF00FFFFFF00000000000000000000000000AA640060AA6400CFBA81
          30FFBF8B40FFBF8B40FFBA8130FFAA6400CFAA64006000000000000000000000
          0000FFFFFF00}
        ShowHint = True
        Transparent = True
        Visible = False
      end
      object ImOk2: TImage
        Left = 223
        Top = 24
        Width = 16
        Height = 16
        Hint = 'DOCUMENTO OK!'
        AutoSize = True
        ParentShowHint = False
        Picture.Data = {
          0B54504E474772617068696336040000424D3604000000000000360000002800
          0000100000001000000001002000000000000004000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000939393F9939393F9939393F9939393F99393
          93F9939393F9939393F9939393F9939393F9939393F9939393F9939393F90000
          0000000000000000000000000000939393F9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF939393F90000
          0000000000000000000000000000939393F900750CF500750CF5EEEEEEFFEDED
          EDFFECECECFFEBEBEBFFEAEAEAFFE9E9E9FFE8E8E8FFFFFFFFFF939393F90000
          000000000000000000000000000000750CF5D3FFE9FF8EFDB4FF00750CF5EFEF
          EFFFEEEEEEFFEDEDEDFFECECECFFEBEBEBFFEAEAEAFFFFFFFFFF939393F90000
          0000000000000000000000750CF5D3FFE9FF41EF7DFF58EF8BFF8EF9B2FF0075
          0CF5F0F0F0FFEFEFEFFFEEEEEEFFEDEDEDFFECECECFFFFFFFFFF939393F90000
          00000000000000750CF5D3FFE9FF7FFFAEFF76BD8CFF00750CF56FF79EFF84F2
          A9FF00750CF5F1F1F1FFF0F0F0FFEFEFEFFFEEEEEEFFFFFFFFFF939393F90000
          00000000000050795D3500750CF5A7DBB7FF00750CF5F6F6F6FF00750CF588FD
          B0FF8BF4AFFF00750CF5F1F1F1FFF1F1F1FFF0F0F0FFFFFFFFFF939393F90000
          000000000000000000000014003800750CF5FFFFFFFFF8F8F8FFF2F2F2FF0075
          0CF5D3FFE9FF00750CF5E7ECE8FFF2F2F2FFF1F1F1FFFFFFFFFF939393F90000
          0000000000000000000000000000939393F9FFFFFFFFFAFAFAFFF9F9F9FFF2F2
          F2FF00750CF5C5C5C5FFF5F5F5FFF4F4F4FFF3F3F3FFF5F5F5FF939393F90000
          0000000000000000000000000000939393F9FFFFFFFFFBFBFBFFFAFAFAFFFAFA
          FAFFF9F9F9FFF8F8F8FF939393F9939393F9939393F9939393F9939393F90000
          0000000000000000000000000000939393F9FFFFFFFFFDFDFDFFFCFCFCFFFBFB
          FBFFFAFAFAFFFAFAFAFF939393F9E1E1E1FFE1E1E1FFB5B5B5F9939393F90000
          0000000000000000000000000000939393F9FFFFFFFFFFFFFFFFFEFEFEFFFDFD
          FDFFFCFCFCFFFBFBFBFF939393F9E1E1E1FFB5B5B5F9939393F9000000000000
          0000000000000000000000000000939393F9FFFFFFFFFFFFFFFFFFFFFFFFFEFE
          FEFFFEFEFEFFFDFDFDFF939393F9B5B5B5F9939393F900000000000000000000
          0000000000000000000000000000939393F9939393F9939393F9939393F99393
          93F9939393F9939393F9939393F9939393F90000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000}
        ShowHint = True
        Transparent = True
        Visible = False
      end
      object sbDevedores: TsSpeedButton
        Left = 740
        Top = 21
        Width = 23
        Height = 22
        Cursor = crHandPoint
        Hint = 'CADASTRO DE DEVEDORES'
        ParentShowHint = False
        ShowHint = True
        OnClick = sbDevedoresClick
        SkinData.SkinSection = 'FORMTITLE'
        Images = Gdm.Im20
        ImageIndex = 13
      end
      object sbDevedor: TsSpeedButton
        Left = 710
        Top = 21
        Width = 23
        Height = 22
        Cursor = crHandPoint
        Hint = 'MAIS INFORMA'#199#213'ES DO DEVEDOR'
        ParentShowHint = False
        ShowHint = True
        OnClick = sbDevedorClick
        SkinData.SkinSection = 'FORMTITLE'
        Images = Gdm.Im16
        ImageIndex = 25
      end
      object btnCancelarDev: TsSpeedButton
        Left = 738
        Top = 97
        Width = 25
        Height = 22
        Cursor = crHandPoint
        ParentShowHint = False
        ShowHint = False
        OnClick = btnCancelarDevClick
        SkinData.SkinSection = 'FORMTITLE'
        Images = Gdm.Im16
        ImageIndex = 14
      end
      object btnConfirmarDev: TsSpeedButton
        Left = 711
        Top = 97
        Width = 25
        Height = 22
        Cursor = crHandPoint
        ParentShowHint = False
        ShowHint = False
        OnClick = btnConfirmarDevClick
        SkinData.SkinSection = 'FORMTITLE'
        Images = Gdm.Im16
        ImageIndex = 38
      end
      object btnExcluirDev: TsSpeedButton
        Left = 684
        Top = 97
        Width = 25
        Height = 22
        Cursor = crHandPoint
        ParentShowHint = False
        ShowHint = False
        OnClick = btnExcluirDevClick
        SkinData.SkinSection = 'FORMTITLE'
        Images = Gdm.Im16
        ImageIndex = 18
      end
      object btnEditarDev: TsSpeedButton
        Left = 657
        Top = 97
        Width = 25
        Height = 22
        Cursor = crHandPoint
        ParentShowHint = False
        ShowHint = False
        OnClick = btnEditarDevClick
        SkinData.SkinSection = 'FORMTITLE'
        Images = Gdm.Im16
        ImageIndex = 36
      end
      object btnIncluirDev: TsSpeedButton
        Left = 630
        Top = 97
        Width = 25
        Height = 22
        Cursor = crHandPoint
        ParentShowHint = False
        ShowHint = False
        OnClick = btnIncluirDevClick
        SkinData.SkinSection = 'FORMTITLE'
        Images = Gdm.Im16
        ImageIndex = 34
      end
      object cbTipoDev: TsComboBox
        Left = 5
        Top = 21
        Width = 48
        Height = 22
        Alignment = taLeftJustify
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'cbTipoDev'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        DisabledKind = []
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Style = csDropDownList
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemIndex = 0
        ParentFont = False
        TabOrder = 0
        Text = 'F'
        OnChange = cbTipoDevChange
        Items.Strings = (
          'F'
          'J')
      end
      object edDocDev: TsMaskEdit
        Left = 61
        Top = 21
        Width = 156
        Height = 22
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 18
        ParentFont = False
        TabOrder = 1
        Text = ''
        OnChange = edDocDevChange
        OnClick = edDocDevClick
        OnExit = edDocDevExit
        CheckOnExit = True
        DisabledKind = []
        SkinData.SkinSection = 'EDIT'
      end
      object edDevedor: TsEdit
        Left = 245
        Top = 21
        Width = 459
        Height = 22
        CharCase = ecUpperCase
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 100
        ParentFont = False
        TabOrder = 2
        DisabledKind = []
        SkinData.SkinSection = 'EDIT'
      end
      object edtEndDevedor: TsEdit
        Left = 5
        Top = 46
        Width = 758
        Height = 22
        Hint = 'Endere'#231'o do Devedor'
        CharCase = ecUpperCase
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 100
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        DisabledKind = []
        SkinData.SkinSection = 'EDIT'
      end
      object edtBairroDevedor: TsEdit
        Left = 5
        Top = 71
        Width = 204
        Height = 22
        Hint = 'Bairro do Devedor'
        CharCase = ecUpperCase
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 100
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        DisabledKind = []
        SkinData.SkinSection = 'EDIT'
      end
      object cbUFDevedor: TsComboBox
        Left = 586
        Top = 71
        Width = 59
        Height = 22
        Hint = 'UF do Devedor'
        Alignment = taLeftJustify
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        DisabledKind = []
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Style = csDropDownList
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemIndex = -1
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        Items.Strings = (
          'AC'
          'AL'
          'AM'
          'AP'
          'BA'
          'CE'
          'DF'
          'ES'
          'GO'
          'MA'
          'MG'
          'MS'
          'MT'
          'PA'
          'PB'
          'PE'
          'PI'
          'PR'
          'RJ'
          'RN'
          'RO'
          'RR'
          'RS'
          'SC'
          'SE'
          'SP'
          'TO')
      end
      object medCEPDevedor: TsMaskEdit
        Left = 651
        Top = 71
        Width = 112
        Height = 22
        Hint = 'CEP do Devedor'
        Color = clWhite
        EditMask = '99999-999;0;_'
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 9
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        Text = ''
        CheckOnExit = True
        DisabledKind = []
        SkinData.SkinSection = 'EDIT'
      end
      object lcbMunDevedor: TsDBLookupComboBox
        Left = 215
        Top = 71
        Width = 365
        Height = 23
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        KeyField = 'CIDADE'
        ListField = 'CIDADE'
        ListSource = dsMunicipios
        ParentFont = False
        TabOrder = 7
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        DisabledKind = []
        SkinData.SkinSection = 'COMBOBOX'
      end
      object dbgDevedores: TwwDBGrid
        AlignWithMargins = True
        Left = 5
        Top = 122
        Width = 758
        Height = 93
        ControlType.Strings = (
          'Check;CheckBox;S;N'
          'SELECIONADO;CheckBox;True;False')
        Selected.Strings = (
          'TIPO'#9'8'#9'Tipo'#9'F'
          'DOCUMENTO_FORMATADO'#9'21'#9'Documento'#9'F'
          'NOME'#9'65'#9'Nome'#9'F'
          'ORDEM'#9'7'#9'Ordem'#9'F')
        IniAttributes.Delimiter = ';;'
        IniAttributes.UnicodeIniFile = False
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = False
        Align = alBottom
        BorderStyle = bsNone
        DataSource = dsRXDevedor
        KeyOptions = []
        Options = [dgEditing, dgTitles, dgIndicator, dgColLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgProportionalColResize, dgFixedProportionalResize]
        TabOrder = 8
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        TitleLines = 1
        TitleButtons = False
        UseTFields = False
      end
    end
  end
  object P1: TsPanel
    Left = 6
    Top = 7
    Width = 772
    Height = 38
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 6
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      772
      38)
    object btIncluir: TsBitBtn
      Left = 4
      Top = 4
      Width = 76
      Height = 30
      Cursor = crHandPoint
      Hint = 'INCLUIR NOVO T'#205'TULO'
      Caption = 'Incluir'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btIncluirClick
      Reflected = True
      ImageIndex = 6
      Images = dm.Imagens
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object btAlterar: TsBitBtn
      Left = 83
      Top = 4
      Width = 76
      Height = 30
      Cursor = crHandPoint
      Hint = 'ALTERAR INFORMA'#199#213'ES DO T'#205'TULO'
      Caption = 'Alterar'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btAlterarClick
      Reflected = True
      ImageIndex = 8
      Images = dm.Imagens
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object btExcluir: TsBitBtn
      Left = 162
      Top = 4
      Width = 76
      Height = 30
      Cursor = crHandPoint
      Hint = 'EXCLUIR T'#205'TULO'
      Caption = 'Excluir'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btExcluirClick
      Reflected = True
      ImageIndex = 9
      Images = dm.Imagens
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object btPesquisar: TsBitBtn
      Left = 241
      Top = 4
      Width = 82
      Height = 30
      Cursor = crHandPoint
      Hint = 'PESQUISAR PROTOCOLO'
      Caption = 'Pesquisar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btPesquisarClick
      Reflected = True
      ImageIndex = 0
      Images = dm.Imagens
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object btProximo: TsBitBtn
      Left = 689
      Top = 4
      Width = 79
      Height = 30
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = 'Pr'#243'ximo'
      Enabled = False
      Glyph.Data = {
        E6000000424DE60000000000000076000000280000000E0000000E0000000100
        04000000000070000000120B0000120B00001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7700777777777777770077777777777777007777777707777700777777770077
        7700770000000107770077091919191077007701919191910700770919191910
        7700770000000107770077777777007777007777777707777700777777777777
        77007777777777777700}
      Layout = blGlyphRight
      TabOrder = 4
      OnClick = btProximoClick
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object btAnterior: TsBitBtn
      Left = 606
      Top = 4
      Width = 80
      Height = 30
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = 'Anterior'
      Enabled = False
      Glyph.Data = {
        E6000000424DE60000000000000076000000280000000E0000000E0000000100
        04000000000070000000120B0000120B00001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7700777777777777770077777777777777007777707777777700777700777777
        7700777010000000770077019191919077007019191919107700770191919190
        7700777010000000770077770077777777007777707777777700777777777777
        77007777777777777700}
      TabOrder = 5
      OnClick = btAnteriorClick
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object btLimpar: TsBitBtn
      Left = 326
      Top = 4
      Width = 76
      Height = 30
      Cursor = crHandPoint
      Hint = 'LIMPAR TODOS OS CAMPOS'
      Caption = 'Limpar'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      OnClick = btLimparClick
      Reflected = True
      ImageIndex = 11
      Images = dm.Imagens
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object btSair: TsBitBtn
      Left = 405
      Top = 4
      Width = 82
      Height = 30
      Cursor = crHandPoint
      Hint = 'SAIR'
      Caption = 'Sair'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
      OnClick = btSairClick
      Reflected = True
      ImageIndex = 7
      Images = dm.Imagens
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object btnDesfazerExportacaoSA: TsBitBtn
      Left = 506
      Top = 4
      Width = 82
      Height = 30
      Cursor = crHandPoint
      Hint = 'DESFAZER EXPORTA'#199#195'O PARA SERVENTIA AGREGADA'
      Caption = 'Desfazer Exp. SA'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      OnClick = btnDesfazerExportacaoSAClick
      Reflected = True
      ImageIndex = 35
      Images = dm.Imagens
      SkinData.SkinSection = 'PAGECONTROL'
    end
  end
  object GbRegistro: TsGroupBox
    Left = 778
    Top = 459
    Width = 772
    Height = 105
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Registro'
    TabOrder = 2
    Visible = False
    SkinData.SkinSection = 'SCROLLSLIDERH'
    CaptionSkin = 'SCROLLSLIDERV'
    DesignSize = (
      772
      105)
    object ImNovoSelo1: TsImage
      Left = 684
      Top = 14
      Width = 16
      Height = 16
      Cursor = crHandPoint
      Hint = 'CARREGAR NOVO SELO PARA O PROTESTO'
      AutoSize = True
      Center = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000049B6401C48B4439F48B344C748B3439B47B3423247B3
        449A48B344C749B3449E49B6422349B2438548B344C348B444C447B3456B55AA
        55030000000043AE431348B447E748B447FF48B447FF48B447FF48B447FF48B4
        47FF48B448FF48B448FF48B448FF48B448FF48B448FF48B448FF48B448FF48B4
        48A700FF000148B54A8648B54BFF48B54BFF48B54BFF48B54BFF48B54BFF77C8
        79FFC3E7C4FF4FB852FF48B54BFF48B54BFF48B54BFF48B54BFF48B54BFF48B5
        4BFF49B64D5047B64EB847B54EFF47B54FFF47B54FFF47B54FFF74C77AFFF6FB
        F6FFFFFFFFFFA3DBA7FF47B64FFF47B64FFF47B64FFF47B64FFF47B64FFF47B6
        4FFF48B64FA447B751A947B652FF47B652FF47B652FF79CA81FFFBFDFBFFC4E8
        C7FFABDEB0FFFBFDFBFF65C26EFF47B652FF47B652FF47B652FF47B652FF47B6
        53FF47B753BE47B8565646B756FF46B756FF46B756FF99D7A2FFB1E1B7FF49B8
        59FF4DBA5CFFE7F6E9FFD6EFD9FF48B858FF46B756FF46B756FF46B756FF46B7
        56FF46B756A08080800246B9599846B859FF46B85AFF46B85AFF46B85AFF46B8
        5AFF46B85AFF7ACC89FFFEFFFEFF99D8A4FF46B85AFF46B85AFF46B85AFF46B8
        5AFF47B85948000000000000000044BB5D4746B95DFF46B95DFF46B95DFF46B9
        5DFF46B95DFF46B95DFFB6E4BFFFF6FCF7FF5DC271FF46B95DFF46B95DFF46B9
        5DA100FF0001000000000000000044BB660F45BA61FB45BA61FF45BA61FF45BA
        61FF45BA61FF45BA61FF4CBD67FFE1F4E6FFAFE2BBFF45BA61FF46B9616E8080
        80020000000000000000000000000000000045BA637E45BB64FF45BB64FF45BB
        64FC45BB64FF45BB64FF45BB65FF4FBF6DFF4EBE6CFF45BB65F733CC66050000
        0000000000000000000000000000000000000000000044BB6A2946BE693345BD
        685D44BC68FF44BC68FF44BC68FF44BC68FF44BC68FF44BC6788000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000044BE6D5244BC6CB844BD6CCC45BD6CB644BD6C6155AA5503000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000}
      ShowHint = True
      Transparent = True
      Visible = False
      OnClick = ImNovoSelo1Click
      ImageIndex = 13
      Images = Gdm.Im16
      SkinData.SkinSection = 'CHECKBOX'
    end
    object ImLimparSelo1: TsImage
      Left = 705
      Top = 14
      Width = 16
      Height = 16
      Cursor = crHandPoint
      Hint = 'LIMPAR SELO'
      AutoSize = True
      Center = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF001D5FDE4E356BD9CA1F61
        E032FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001D5FDE4E5E81D2FDADA3B2FF4B73
        CFF61F61E032FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF001D5FDE4E688CDCFDCAC2C9FFBEB4BDFFB0A4
        B2FF577DD3F7ECF1FBCAF1F5FBCFFFFFFF24FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF001D5FDE4E7297E5FDE3DFE3FFD7D0D6FFCAC2C9FFC3B9
        C2FFEEEBF0FF7397DDFF517DD2FFF0F2F9E7FFFFFF24FFFFFF00FFFFFF00FFFF
        FF00FFFFFF001D5FDE4E779EEBFDFCFBFCFFF0EDEFFFE3DFE3FFDAD3D9FFF3F1
        F4FF789FE3FF2964CFFF255DC9FF517DD2FFF0F2F9E7FFFFFF24FFFFFF00FFFF
        FF00FFFFFF003F78E3CAF7FAFEFFFFFFFFFFFCFBFCFFF1EEF0FFF7F7F9FF7DA6
        E9FF316FD8FF265EC8FF265EC9FF255ECAFF517DD2FFF0F2F9E7FFFFFF24FFFF
        FF00FFFFFF002666E1346490E9F6FAFCFEFFFFFFFFFFFCFDFEFF83ADEFFF397B
        E1FF2963CCFF2962CCFF2962CCFF245CC7FF255ECAFF517DD2FFF0F2F9E7FFFF
        FF24FFFFFF00FFFFFF001F61E0326D97EAF7FCFDFFFF88B5F5FF4286EBFF2D67
        D0FF2C67D0FF2E6AD3FF265EC7FF2A65CFFF245CC6FF255ECAFF517DD2FFF0F2
        F9E7FFFFFF24FFFFFF00FFFFFF00EDF2FCCA8DBCFBFF4A92F4FF306CD3FF306C
        D4FF3371D8FF2861CAFF306ED6FF245CC6FF2A65CFFF245CC6FF255ECAFF537F
        D3FFFBFBFCAEFFFFFF00FFFFFF00F4F9FECF78B2FDFF4990F3FF3776DCFF3878
        DEFF2A63CCFF3776DDFF265EC7FF316ED7FF255DC6FF2962CCFF2B65CFFFB1C6
        ECFFFFFFFF7EFFFFFF00FFFFFF00FFFFFF24F3F6FEE778B2FDFF4D96F8FF306C
        D4FF3D7FE4FF2760C9FF3877DEFF275FC8FF2E6AD3FF3370D8FFB4C9EFFFD6E2
        F9AFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF24F3F6FEE778B2FDFF4E97
        F9FF2E69D1FF3E80E5FF2861CAFF3371D8FF3B7CE1FFB7CEF3FFD6E2F9AFFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF24F3F6FEE778B2
        FDFF4E97F9FF2E6AD1FF3878DEFF4487EBFFBAD2F6FFD6E2F9AFFFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF24F3F6
        FEE778B2FDFF4D96F8FF4C93F4FFBDD6F9FFD6E2F9AFFFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF24F3F6FEE77AB3FDFFC0DAFDFFD6E2F9AFFFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF24FBFCFEAEFFFFFF7EFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00}
      ShowHint = True
      Transparent = True
      Visible = False
      OnClick = ImLimparSelo1Click
      SkinData.SkinSection = 'CHECKBOX'
    end
    object ImNovoSelo2: TsImage
      Left = 684
      Top = 57
      Width = 16
      Height = 16
      Cursor = crHandPoint
      Hint = 'CARREGAR NOVO SELO PARA A SOLU'#199#195'O'
      AutoSize = True
      Center = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000049B6401C48B4439F48B344C748B3439B47B3423247B3
        449A48B344C749B3449E49B6422349B2438548B344C348B444C447B3456B55AA
        55030000000043AE431348B447E748B447FF48B447FF48B447FF48B447FF48B4
        47FF48B448FF48B448FF48B448FF48B448FF48B448FF48B448FF48B448FF48B4
        48A700FF000148B54A8648B54BFF48B54BFF48B54BFF48B54BFF48B54BFF77C8
        79FFC3E7C4FF4FB852FF48B54BFF48B54BFF48B54BFF48B54BFF48B54BFF48B5
        4BFF49B64D5047B64EB847B54EFF47B54FFF47B54FFF47B54FFF74C77AFFF6FB
        F6FFFFFFFFFFA3DBA7FF47B64FFF47B64FFF47B64FFF47B64FFF47B64FFF47B6
        4FFF48B64FA447B751A947B652FF47B652FF47B652FF79CA81FFFBFDFBFFC4E8
        C7FFABDEB0FFFBFDFBFF65C26EFF47B652FF47B652FF47B652FF47B652FF47B6
        53FF47B753BE47B8565646B756FF46B756FF46B756FF99D7A2FFB1E1B7FF49B8
        59FF4DBA5CFFE7F6E9FFD6EFD9FF48B858FF46B756FF46B756FF46B756FF46B7
        56FF46B756A08080800246B9599846B859FF46B85AFF46B85AFF46B85AFF46B8
        5AFF46B85AFF7ACC89FFFEFFFEFF99D8A4FF46B85AFF46B85AFF46B85AFF46B8
        5AFF47B85948000000000000000044BB5D4746B95DFF46B95DFF46B95DFF46B9
        5DFF46B95DFF46B95DFFB6E4BFFFF6FCF7FF5DC271FF46B95DFF46B95DFF46B9
        5DA100FF0001000000000000000044BB660F45BA61FB45BA61FF45BA61FF45BA
        61FF45BA61FF45BA61FF4CBD67FFE1F4E6FFAFE2BBFF45BA61FF46B9616E8080
        80020000000000000000000000000000000045BA637E45BB64FF45BB64FF45BB
        64FC45BB64FF45BB64FF45BB65FF4FBF6DFF4EBE6CFF45BB65F733CC66050000
        0000000000000000000000000000000000000000000044BB6A2946BE693345BD
        685D44BC68FF44BC68FF44BC68FF44BC68FF44BC68FF44BC6788000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000044BE6D5244BC6CB844BD6CCC45BD6CB644BD6C6155AA5503000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000}
      ShowHint = True
      Transparent = True
      Visible = False
      OnClick = ImNovoSelo2Click
      SkinData.SkinSection = 'CHECKBOX'
    end
    object ImLimparSelo2: TsImage
      Left = 705
      Top = 57
      Width = 16
      Height = 16
      Cursor = crHandPoint
      Hint = 'LIMPAR SELO'
      AutoSize = True
      Center = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF001D5FDE4E356BD9CA1F61
        E032FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001D5FDE4E5E81D2FDADA3B2FF4B73
        CFF61F61E032FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF001D5FDE4E688CDCFDCAC2C9FFBEB4BDFFB0A4
        B2FF577DD3F7ECF1FBCAF1F5FBCFFFFFFF24FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF001D5FDE4E7297E5FDE3DFE3FFD7D0D6FFCAC2C9FFC3B9
        C2FFEEEBF0FF7397DDFF517DD2FFF0F2F9E7FFFFFF24FFFFFF00FFFFFF00FFFF
        FF00FFFFFF001D5FDE4E779EEBFDFCFBFCFFF0EDEFFFE3DFE3FFDAD3D9FFF3F1
        F4FF789FE3FF2964CFFF255DC9FF517DD2FFF0F2F9E7FFFFFF24FFFFFF00FFFF
        FF00FFFFFF003F78E3CAF7FAFEFFFFFFFFFFFCFBFCFFF1EEF0FFF7F7F9FF7DA6
        E9FF316FD8FF265EC8FF265EC9FF255ECAFF517DD2FFF0F2F9E7FFFFFF24FFFF
        FF00FFFFFF002666E1346490E9F6FAFCFEFFFFFFFFFFFCFDFEFF83ADEFFF397B
        E1FF2963CCFF2962CCFF2962CCFF245CC7FF255ECAFF517DD2FFF0F2F9E7FFFF
        FF24FFFFFF00FFFFFF001F61E0326D97EAF7FCFDFFFF88B5F5FF4286EBFF2D67
        D0FF2C67D0FF2E6AD3FF265EC7FF2A65CFFF245CC6FF255ECAFF517DD2FFF0F2
        F9E7FFFFFF24FFFFFF00FFFFFF00EDF2FCCA8DBCFBFF4A92F4FF306CD3FF306C
        D4FF3371D8FF2861CAFF306ED6FF245CC6FF2A65CFFF245CC6FF255ECAFF537F
        D3FFFBFBFCAEFFFFFF00FFFFFF00F4F9FECF78B2FDFF4990F3FF3776DCFF3878
        DEFF2A63CCFF3776DDFF265EC7FF316ED7FF255DC6FF2962CCFF2B65CFFFB1C6
        ECFFFFFFFF7EFFFFFF00FFFFFF00FFFFFF24F3F6FEE778B2FDFF4D96F8FF306C
        D4FF3D7FE4FF2760C9FF3877DEFF275FC8FF2E6AD3FF3370D8FFB4C9EFFFD6E2
        F9AFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF24F3F6FEE778B2FDFF4E97
        F9FF2E69D1FF3E80E5FF2861CAFF3371D8FF3B7CE1FFB7CEF3FFD6E2F9AFFFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF24F3F6FEE778B2
        FDFF4E97F9FF2E6AD1FF3878DEFF4487EBFFBAD2F6FFD6E2F9AFFFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF24F3F6
        FEE778B2FDFF4D96F8FF4C93F4FFBDD6F9FFD6E2F9AFFFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF24F3F6FEE77AB3FDFFC0DAFDFFD6E2F9AFFFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF24FBFCFEAEFFFFFF7EFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00}
      ShowHint = True
      Transparent = True
      Visible = False
      OnClick = ImLimparSelo2Click
      SkinData.SkinSection = 'CHECKBOX'
    end
    object ImAle2: TsImage
      Left = 750
      Top = 57
      Width = 16
      Height = 16
      Cursor = crHandPoint
      Hint = 'BUSCAR ALEAT'#211'RIO'
      AutoSize = True
      Center = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000B3810C82B2800C8100000000000000000000000000000000000000000000
        000000000000000000000000000000000000000000000000000000000000B483
        0C6DB5830CFFB5830CFFB6820C6C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000B7850D60B885
        0CFEB8850CFFB8850CFFB8850CFDB9860B5F0000000000000000000000000000
        00000000000000000000000000000000000000000000B9890C54BA880DFCBA88
        0DFFBA880DFFBA880DFFBA880DFFBA880DFCB9890C5400000000000000000000
        000000000000000000000000000000000000BE8B0D4FBD8A0DFBBD8A0DFFBD8A
        0DFFBD8A0DFFBD8A0DFFBD8A0DFFBD8A0DFFBD8A0DFABE8B0D4F000000000000
        0000000000000000000000000000C08C0F45C08D0DF7C08D0DFFC08D0DFFC08D
        0DFFC08D0DFFC08D0DFFC08D0DFFC08D0DFFC08D0DFFC08D0DF7BF8F0B440000
        00000000000000000000FFFF0001C4900D4EC3900F55C3900E6EC38F0EFFC38F
        0EFFC38F0EFFC38F0EFFC38F0EFFC38F0EFFC38F0D72C3900F55C38E0D4DFFFF
        00010000000000000000000000000000000000000000C8910E25C6920EFFC692
        0EFFC6920EFFC6920EFFC6920EFFC6920EFFC4940C2B00000000000000000000
        00000000000000000000000000000000000000000000C8910E25C9940EFFC994
        0EFFC9940EFFC9940EFFC9940EFFC9940EFFCA940C2B00000000000000000000
        00000000000000000000000000000000000000000000CF980E25CC970EFFCC97
        0EFFCC970EFFCC970EFFCC970EFFCC970EFFCA940C2B00000000000000000000
        00000000000000000000000000000000000000000000CF980E25CF990FFFCF99
        0FFFCF990FFFCF990FFFCF990FFFCF990FFFD09A122B00000000000000000000
        00000000000000000000000000000000000000000000CF980E25D19B0FFFD19B
        0FFFD19B0FFFD19B0FFFD19B0FFFD19B0FFFD09A122B00000000000000000000
        00000000000000000000000000000000000000000000D69F0E25D49E0FFFD49E
        0FFFD49E0FFFD49E0FFFD49E0FFFD49E0FFFD5A0122B00000000000000000000
        00000000000000000000000000000000000000000000D69F0E25D7A010FFD7A0
        10FFD7A010FFD7A010FFD7A010FFD7A010FFD5A0122B00000000000000000000
        00000000000000000000000000000000000000000000DDA50E25DAA310FFDAA3
        10FFDAA310FFDAA310FFDAA310FFDAA310FFDBA0122B00000000000000000000
        00000000000000000000000000000000000000000000DDA50E25DDA510FFDDA5
        10FFDDA510FFDDA510FFDDA510FFDDA510FFDBA6122B00000000000000000000
        000000000000}
      ShowHint = True
      Transparent = True
      Visible = False
      OnClick = ImAle2Click
      SkinData.SkinSection = 'CHECKBOX'
    end
    object ImAle1: TsImage
      Left = 750
      Top = 14
      Width = 16
      Height = 16
      Cursor = crHandPoint
      Hint = 'BUSCAR ALEAT'#211'RIO'
      AutoSize = True
      Center = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000B3810C82B2800C8100000000000000000000000000000000000000000000
        000000000000000000000000000000000000000000000000000000000000B483
        0C6DB5830CFFB5830CFFB6820C6C000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000B7850D60B885
        0CFEB8850CFFB8850CFFB8850CFDB9860B5F0000000000000000000000000000
        00000000000000000000000000000000000000000000B9890C54BA880DFCBA88
        0DFFBA880DFFBA880DFFBA880DFFBA880DFCB9890C5400000000000000000000
        000000000000000000000000000000000000BE8B0D4FBD8A0DFBBD8A0DFFBD8A
        0DFFBD8A0DFFBD8A0DFFBD8A0DFFBD8A0DFFBD8A0DFABE8B0D4F000000000000
        0000000000000000000000000000C08C0F45C08D0DF7C08D0DFFC08D0DFFC08D
        0DFFC08D0DFFC08D0DFFC08D0DFFC08D0DFFC08D0DFFC08D0DF7BF8F0B440000
        00000000000000000000FFFF0001C4900D4EC3900F55C3900E6EC38F0EFFC38F
        0EFFC38F0EFFC38F0EFFC38F0EFFC38F0EFFC38F0D72C3900F55C38E0D4DFFFF
        00010000000000000000000000000000000000000000C8910E25C6920EFFC692
        0EFFC6920EFFC6920EFFC6920EFFC6920EFFC4940C2B00000000000000000000
        00000000000000000000000000000000000000000000C8910E25C9940EFFC994
        0EFFC9940EFFC9940EFFC9940EFFC9940EFFCA940C2B00000000000000000000
        00000000000000000000000000000000000000000000CF980E25CC970EFFCC97
        0EFFCC970EFFCC970EFFCC970EFFCC970EFFCA940C2B00000000000000000000
        00000000000000000000000000000000000000000000CF980E25CF990FFFCF99
        0FFFCF990FFFCF990FFFCF990FFFCF990FFFD09A122B00000000000000000000
        00000000000000000000000000000000000000000000CF980E25D19B0FFFD19B
        0FFFD19B0FFFD19B0FFFD19B0FFFD19B0FFFD09A122B00000000000000000000
        00000000000000000000000000000000000000000000D69F0E25D49E0FFFD49E
        0FFFD49E0FFFD49E0FFFD49E0FFFD49E0FFFD5A0122B00000000000000000000
        00000000000000000000000000000000000000000000D69F0E25D7A010FFD7A0
        10FFD7A010FFD7A010FFD7A010FFD7A010FFD5A0122B00000000000000000000
        00000000000000000000000000000000000000000000DDA50E25DAA310FFDAA3
        10FFDAA310FFDAA310FFDAA310FFDAA310FFDBA0122B00000000000000000000
        00000000000000000000000000000000000000000000DDA50E25DDA510FFDDA5
        10FFDDA510FFDDA510FFDDA510FFDDA510FFDBA6122B00000000000000000000
        000000000000}
      ShowHint = True
      Transparent = True
      Visible = False
      OnClick = ImAle1Click
      SkinData.SkinSection = 'CHECKBOX'
    end
    object edDataRegistro: TsDateEdit
      Left = 16
      Top = 32
      Width = 101
      Height = 22
      Hint = 'DATA DO REGISTRO DO PROTESTO'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data do Protesto'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DO REGISTRO DO PROTESTO'
      GlyphMode.Blend = 0
    end
    object edRegistro: TsEdit
      Left = 264
      Top = 32
      Width = 54
      Height = 22
      Hint = 'N'#218'MERO DO TERMO'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnKeyPress = edRegistroKeyPress
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Termo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edLivroRegistro: TsEdit
      Left = 323
      Top = 32
      Width = 42
      Height = 22
      Hint = 'N'#218'MERO DO LIVRO DE REGISTRO'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnKeyPress = edLivroRegistroKeyPress
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Livro'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edFolhaRegistro: TsEdit
      Left = 382
      Top = 32
      Width = 74
      Height = 22
      Hint = 'FOLHA INICIAL DO LIVRO DE REGISTRO'
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Folha'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edSeloRegistro: TsEdit
      Left = 636
      Top = 32
      Width = 85
      Height = 22
      Hint = 'SELO UTILIZADO NO PROTESTO'
      CharCase = ecUpperCase
      Color = 8454143
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      MaxLength = 9
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 6
      Visible = False
      OnExit = edSeloRegistroExit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Selo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object lkEscrevente: TsDBLookupComboBox
      Left = 462
      Top = 32
      Width = 167
      Height = 22
      Hint = 'ESCREVENTE QUE REALIZOU O ATO DO PROTESTO'
      Anchors = [akLeft, akTop, akRight]
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyField = 'CPF'
      ListField = 'LOGIN'
      ListSource = dsEscreventes
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Escrevente (Protesto)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
    end
    object edDataPagamento: TsDateEdit
      Left = 264
      Top = 75
      Width = 101
      Height = 22
      Hint = 'DATA DO PAGAMENTO'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 10
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data Pagamento'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DO PAGAMENTO'
      GlyphMode.Blend = 0
    end
    object edPagamento: TsCurrencyEdit
      Left = 382
      Top = 75
      Width = 74
      Height = 22
      Hint = 'VALOR EFETIVAMENTE DESEMBOLSADO PARA LIQUIDAR O PROTESTO'
      AutoSize = False
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 11
      OnClick = edValorClick
      OnExit = edValorExit
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Pago'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'VALOR EFETIVAMENTE DESEMBOLSADO PARA LIQUIDAR O PROTESTO'
      GlyphMode.Blend = 0
      DisplayFormat = '#####0.00'
    end
    object edDataSustacao: TsDateEdit
      Left = 16
      Top = 75
      Width = 101
      Height = 22
      Hint = 'DATA DA SUSTA'#199#195'O'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data da Susta'#231#227'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DA SUSTA'#199#195'O'
      GlyphMode.Blend = 0
    end
    object lkEscreventeP: TsDBLookupComboBox
      Left = 462
      Top = 75
      Width = 167
      Height = 22
      Hint = 'ESCREVENTE QUE REALIZOU O ATO DO PAGAMENTO'
      Anchors = [akLeft, akTop, akRight]
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyField = 'CPF'
      ListField = 'LOGIN'
      ListSource = dsEscreventes
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 12
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Escrevente (Pagamento)'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
    end
    object edDataRetirada: TsDateEdit
      Left = 138
      Top = 75
      Width = 103
      Height = 22
      Hint = 'DATA DA RETIRADA'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 9
      Text = '  /  /    '
      Visible = False
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data da Retirada'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DA RETIRADA'
      GlyphMode.Blend = 0
    end
    object edSeloSolucao: TsEdit
      Left = 636
      Top = 75
      Width = 85
      Height = 22
      Hint = 'SELO UTILIZADO NA SOLU'#199#195'O'
      CharCase = ecUpperCase
      Color = 8454143
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      MaxLength = 9
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 13
      Visible = False
      OnExit = edSeloRegistroExit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Selo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edDataPrazo: TsDateEdit
      Left = 138
      Top = 32
      Width = 103
      Height = 22
      Hint = 'DATA DO PRAZO'
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = '  /  /    '
      Visible = False
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Data do Prazo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Grayed = False
      GlyphMode.Hint = 'DATA DO PRAZO'
      GlyphMode.Blend = 0
    end
    object edAleatorioProtesto: TsEdit
      Left = 728
      Top = 32
      Width = 38
      Height = 22
      Hint = 'ALEAT'#211'RIO UTILIZADO NO PROTESTO'
      CharCase = ecUpperCase
      Color = 16777088
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = False
      TabOrder = 7
      Visible = False
      OnExit = edSeloRegistroExit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Layout = sclTopLeft
    end
    object edAleatorioSolucao: TsEdit
      Left = 728
      Top = 75
      Width = 38
      Height = 22
      Hint = 'ALEAT'#211'RIO UTILIZADO NA SOLU'#199#195'O'
      CharCase = ecUpperCase
      Color = 16777088
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = False
      TabOrder = 14
      Visible = False
      OnExit = edSeloRegistroExit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Layout = sclTopLeft
    end
  end
  object ckAntigo: TsCheckBox
    Left = 533
    Top = 637
    Width = 68
    Height = 20
    Caption = 'Antigo'
    Anchors = [akTop, akRight]
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    ImgChecked = 0
    ImgUnchecked = 0
    SkinData.SkinSection = 'CHECKBOX'
    ReadOnly = True
  end
  object ckFins: TsCheckBox
    Left = 389
    Top = 637
    Width = 135
    Height = 20
    Cursor = crHandPoint
    Caption = 'Fins Falimentares'
    Anchors = [akTop, akRight]
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    ImgChecked = 0
    ImgUnchecked = 0
    SkinData.SkinSection = 'CHECKBOX'
  end
  object ckPostecipado: TsCheckBox
    Left = 248
    Top = 637
    Width = 104
    Height = 20
    Cursor = crHandPoint
    Caption = 'Postecipado'
    Anchors = [akTop, akRight]
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
    ImgChecked = 0
    ImgUnchecked = 0
    SkinData.SkinSection = 'CHECKBOX'
  end
  object dsTipos: TDataSource
    DataSet = dm.Tipos
    Left = 368
    Top = 623
  end
  object dsRXDevedor: TDataSource
    DataSet = dm.RxDevedor
    OnDataChange = dsRXDevedorDataChange
    Left = 302
    Top = 623
  end
  object dsEscreventes: TDataSource
    DataSet = dm.Escreventes
    Left = 147
    Top = 623
  end
  object dsRXPortador: TDataSource
    DataSet = dm.RxPortador
    Left = 230
    Top = 623
  end
  object dsRxSacador: TDataSource
    DataSet = dm.RxSacador
    Left = 62
    Top = 623
  end
  object sSkinProvider: TsSkinProvider
    AddedTitle.Font.Charset = DEFAULT_CHARSET
    AddedTitle.Font.Color = clNone
    AddedTitle.Font.Height = -11
    AddedTitle.Font.Name = 'MS Sans Serif'
    AddedTitle.Font.Style = []
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 582
    Top = 622
  end
  object qryTabela: TFDQuery
    Connection = dm.conCENTRAL
    SQL.Strings = (
      'select * from CONCUS'
      ''
      'where TAB='#39'9'#39' and ITEM='#39'1'#39' and SUB=:S')
    Left = 470
    Top = 625
    ParamData = <
      item
        Name = 'S'
        DataType = ftString
        ParamType = ptInput
      end>
    object qryTabelaORDEM: TIntegerField
      FieldName = 'ORDEM'
      Origin = 'ORDEM'
    end
    object qryTabelaANO: TIntegerField
      FieldName = 'ANO'
      Origin = 'ANO'
    end
    object qryTabelaVAI: TStringField
      FieldName = 'VAI'
      Origin = 'VAI'
      FixedChar = True
      Size = 1
    end
    object qryTabelaTAB: TStringField
      FieldName = 'TAB'
      Origin = 'TAB'
    end
    object qryTabelaITEM: TStringField
      FieldName = 'ITEM'
      Origin = 'ITEM'
    end
    object qryTabelaSUB: TStringField
      FieldName = 'SUB'
      Origin = 'SUB'
    end
    object qryTabelaDESCR: TStringField
      FieldName = 'DESCR'
      Origin = 'DESCR'
      Size = 100
    end
    object qryTabelaVALOR: TFloatField
      FieldName = 'VALOR'
      Origin = 'VALOR'
    end
    object qryTabelaTEXTO: TStringField
      FieldName = 'TEXTO'
      Origin = 'TEXTO'
      Size = 250
    end
  end
  object dsMunicipios: TDataSource
    DataSet = dm.Municipios
    Left = 420
    Top = 623
  end
end
