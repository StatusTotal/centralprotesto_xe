unit URelCancelamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FMTBcd, StdCtrls, Buttons, sBitBtn, sEdit, sGroupBox, ExtCtrls,
  sPanel, DB, SqlExpr, DBCtrls, sDBLookupComboBox, sCheckBox, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFRelCancelamento = class(TForm)
    P1: TsPanel;
    sGroupBox1: TsGroupBox;
    edProtocolo: TsEdit;
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    Protocolo: TFDQuery;
    ProtocoloID_ATO: TIntegerField;
    procedure btVisualizarClick(Sender: TObject);
    procedure edProtocoloKeyPress(Sender: TObject; var Key: Char);
    procedure btSairClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelCancelamento: TFRelCancelamento;

implementation

uses UQuickPagamento1, UDM, UPF, UQuickCancelado2, UQuickCancelado1,
  UAssinatura, UGeral;

{$R *.dfm}

procedure TFRelCancelamento.btVisualizarClick(Sender: TObject);
var
  Q: TFDQuery;
begin
  if edProtocolo.Text='' then
  begin
      edProtocolo.SetFocus;
      Exit;
  end;

  Protocolo.Close;
  Protocolo.Params[0].AsInteger:=StrToInt(edProtocolo.Text);
  Protocolo.Open;
  if (Protocolo.IsEmpty) or (ProtocoloID_ATO.IsNull) then
  begin
      GR.Aviso('Protocolo n�o encontrado.');
      Exit;
  end;

  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT ID_CERTIDAO FROM CERTIDOES WHERE ID_ATO='''+ProtocoloID_ATO.AsString+'''');
  Q.Open;

  dm.Certidoes.Close;
  dm.Certidoes.Params[0].AsInteger:=Q.FieldByName('ID_CERTIDAO').AsInteger;
  dm.Certidoes.Open;

  Q.Close;
  Q.Free;

  dm.Titulos.Close;
  dm.Titulos.Params[0].AsInteger:=ProtocoloID_ATO.AsInteger;
  dm.Titulos.Open;

  GR.CriarForm(TFAssinatura,FAssinatura);

  if dm.ValorParametro(51)='S' then
    Application.CreateForm(TFQuickCancelado1,FQuickCancelado1);
  Application.CreateForm(TFQuickCancelado2,FQuickCancelado2);

  dm.Certidoes.Close;
  dm.Titulos.Close;
end;

procedure TFRelCancelamento.edProtocoloKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in ['0'..'9']) then key:=#0;
end;

procedure TFRelCancelamento.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFRelCancelamento.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFRelCancelamento.FormCreate(Sender: TObject);
begin
  dm.Responsavel.Close;
  dm.Responsavel.Open;
end;

end.
