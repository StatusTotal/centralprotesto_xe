object FPortadores: TFPortadores
  Left = 476
  Top = 250
  BorderStyle = bsDialog
  Caption = 'CADASTRO DE PORTADORES'
  ClientHeight = 500
  ClientWidth = 701
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object P2: TsPanel
    Left = 0
    Top = 304
    Width = 701
    Height = 196
    Align = alBottom
    TabOrder = 0
    SkinData.SkinSection = 'SELECTION'
    ExplicitTop = 310
    object ImAviso1: TImage
      Left = 671
      Top = 7
      Width = 16
      Height = 16
      Hint = 'DOCUMENTO INV'#193'LIDO!'
      AutoSize = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        000000000000FFFFFF0000000000000000000000000000000000000000000000
        0000AA640070AA64007000000000000000000000000000000000000000000000
        0000FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
        0000AA6400CFAA6400CF00000000000000000000000000000000000000000000
        0000FFFFFF00FFFFFF000000000000000000000000000000000000000000AA64
        0020BF8B3DFFBF8B3DFFAA640020000000000000000000000000000000000000
        0000FFFFFF00FFFFFF000000000000000000000000000000000000000000AA64
        0060DABB8DFFDABB8DFFAA640060000000000000000000000000000000000000
        0000FFFFFF00FFFFFF00000000000000000000000000AA640060AA6400AFB577
        1FFFF4ECDDFFF4ECDDFFB5771FFFAA6400AFAA64006000000000000000000000
        0000FFFFFF00FFFFFF0000000000AA640010AA6400BFCA9E5DFFF4ECD8FFFFFF
        F8FFFFFFFFFFFFFFFEFFFFFFF7FFF4ECD8FFCA9E5DFFAA6400BFAA6400100000
        0000FFFFFF00FFFFFF00AA640010AA6400CFDFC59BFFFFFEF7FFFFFCF5FFFFFB
        F6FF010BBFFF010BBFFFFFFBF6FFFFFCF5FFFFFEF7FFDFC59BFFAA6400CFAA64
        0010FFFFFF00FFFFFF00AA640080DABB8CFFFFFAF0FFFFF6EAFFFFF6EAFFFFF6
        EAFF0021C2FF0021C2FFFFF6EAFFFFF6EAFFFFF6EAFFFFFBF2FFDABB8CFFAA64
        0080FFFFFF00FFFFFF00B57921EFFFFBF3FFFFF1DEFFFFF1DEFFFFF1DEFFFFF1
        DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFFDF7FFB579
        21EFFFFFFF00FFFFFF00C5944FFFFFF3E3FFFFEDD5FFFFEDD5FFFFEDD5FFFFED
        D5FF0007DAFF0007DAFFFFEDD5FFFFEDD5FFFFEDD5FFFFEDD5FFFFF6E8FFC594
        4FFFFFFFFF00FFFFFF00C5944FFFFFF0DCFFFFEACDFFFFEACDFFFFEACDFFFFEA
        CDFF0012E5FF0012E5FFFFEACDFFFFEACDFFFFEACDFFFFEACDFFFFF4E5FFC594
        4FFFFFFFFF00FFFFFF00B57922EFFFF5E6FFFFE7C8FFFFE7C8FFFFE7C8FFFFE7
        C8FF001DD5FF001DD5FFFFE7C8FFFFE7C8FFFFE7C8FFFFE7C8FFFFF6E9FFB579
        22EFFFFFFF00FFFFFF00AA640080DABA8BFFFFEBD0FFFFE6C5FFFFE6C5FFFFE6
        C5FF0029C2FF0029C2FFFFE6C5FFFFE6C5FFFFE6C5FFFFEBD0FFDABB8FFFAA64
        0080FFFFFF00FFFFFF00AA640010B17014CFEAD7BBFFFFEED7FFFFE6C5FFFFE6
        C5FF0024AFFF0024AFFFFFE6C5FFFFE6C5FFFFEFDAFFEAD7BBFFB17014CFAA64
        0010FFFFFF00FFFFFF0000000000AA640010AA6400CFCFA870FFF4E9D8FFFFF4
        E6FFFFF3E2FFFFF3E2FFFFF4E6FFF4E9D8FFCFA870FFAA6400CFAA6400100000
        0000FFFFFF00FFFFFF00000000000000000000000000AA640060AA6400CFBA81
        30FFBF8B40FFBF8B40FFBA8130FFAA6400CFAA64006000000000000000000000
        0000FFFFFF00}
      ShowHint = True
      Transparent = True
      Visible = False
    end
    object ImOk1: TImage
      Left = 671
      Top = 6
      Width = 16
      Height = 16
      Hint = 'DOCUMENTO OK!'
      AutoSize = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000939393F9939393F9939393F9939393F99393
        93F9939393F9939393F9939393F9939393F9939393F9939393F9939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF939393F90000
        0000000000000000000000000000939393F900750CF500750CF5EEEEEEFFEDED
        EDFFECECECFFEBEBEBFFEAEAEAFFE9E9E9FFE8E8E8FFFFFFFFFF939393F90000
        000000000000000000000000000000750CF5D3FFE9FF8EFDB4FF00750CF5EFEF
        EFFFEEEEEEFFEDEDEDFFECECECFFEBEBEBFFEAEAEAFFFFFFFFFF939393F90000
        0000000000000000000000750CF5D3FFE9FF41EF7DFF58EF8BFF8EF9B2FF0075
        0CF5F0F0F0FFEFEFEFFFEEEEEEFFEDEDEDFFECECECFFFFFFFFFF939393F90000
        00000000000000750CF5D3FFE9FF7FFFAEFF76BD8CFF00750CF56FF79EFF84F2
        A9FF00750CF5F1F1F1FFF0F0F0FFEFEFEFFFEEEEEEFFFFFFFFFF939393F90000
        00000000000050795D3500750CF5A7DBB7FF00750CF5F6F6F6FF00750CF588FD
        B0FF8BF4AFFF00750CF5F1F1F1FFF1F1F1FFF0F0F0FFFFFFFFFF939393F90000
        000000000000000000000014003800750CF5FFFFFFFFF8F8F8FFF2F2F2FF0075
        0CF5D3FFE9FF00750CF5E7ECE8FFF2F2F2FFF1F1F1FFFFFFFFFF939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFAFAFAFFF9F9F9FFF2F2
        F2FF00750CF5C5C5C5FFF5F5F5FFF4F4F4FFF3F3F3FFF5F5F5FF939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFBFBFBFFFAFAFAFFFAFA
        FAFFF9F9F9FFF8F8F8FF939393F9939393F9939393F9939393F9939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFDFDFDFFFCFCFCFFFBFB
        FBFFFAFAFAFFFAFAFAFF939393F9E1E1E1FFE1E1E1FFB5B5B5F9939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFFFFFFFFFEFEFEFFFDFD
        FDFFFCFCFCFFFBFBFBFF939393F9E1E1E1FFB5B5B5F9939393F9000000000000
        0000000000000000000000000000939393F9FFFFFFFFFFFFFFFFFFFFFFFFFEFE
        FEFFFEFEFEFFFDFDFDFF939393F9B5B5B5F9939393F900000000000000000000
        0000000000000000000000000000939393F9939393F9939393F9939393F99393
        93F9939393F9939393F9939393F9939393F90000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000}
      ShowHint = True
      Transparent = True
      Visible = False
    end
    object sBevel1: TsBevel
      Left = 7
      Top = 55
      Width = 122
      Height = 116
      Shape = bsFrame
    end
    object ckConvenio: TsDBCheckBox
      Left = 10
      Top = 102
      Width = 71
      Height = 22
      Cursor = crHandPoint
      Caption = 'Conv'#234'nio'
      AutoSize = False
      TabOrder = 13
      OnClick = ckConvenioClick
      OnExit = ckConvenioExit
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
      DataField = 'CONVENIO'
      DataSource = dsPortadores
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object edCodigo: TsDBEdit
      Left = 5
      Top = 26
      Width = 108
      Height = 23
      Color = clWhite
      DataField = 'CODIGO'
      DataSource = dsPortadores
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'C'#243'digo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edNome: TsDBEdit
      Left = 181
      Top = 26
      Width = 280
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'NOME'
      DataSource = dsPortadores
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Nome'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object cbTipo: TsDBComboBox
      Left = 119
      Top = 26
      Width = 56
      Height = 23
      Style = csDropDownList
      Color = clWhite
      DataField = 'TIPO'
      DataSource = dsPortadores
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Items.Strings = (
        'F'
        'J')
      ParentFont = False
      TabOrder = 1
      OnChange = cbTipoChange
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Tipo'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'COMBOBOX'
    end
    object edDocumento: TsDBEdit
      Left = 539
      Top = 26
      Width = 148
      Height = 23
      Color = clWhite
      DataField = 'DOCUMENTO'
      DataSource = dsPortadores
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnChange = edDocumentoChange
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Documento'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edEndereco: TsDBEdit
      Left = 135
      Top = 93
      Width = 326
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'ENDERECO'
      DataSource = dsPortadores
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Endere'#231'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object ckBanco: TsDBCheckBox
      Left = 10
      Top = 82
      Width = 60
      Height = 19
      Cursor = crHandPoint
      Caption = 'Banco'
      TabOrder = 12
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
      DataField = 'BANCO'
      DataSource = dsPortadores
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object edConta: TsDBEdit
      Left = 538
      Top = 93
      Width = 149
      Height = 23
      Color = clWhite
      DataField = 'CONTA'
      DataSource = dsPortadores
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Conta'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object MObs: TsDBMemo
      Left = 323
      Top = 141
      Width = 364
      Height = 23
      Color = clWhite
      DataField = 'OBSERVACAO'
      DataSource = dsPortadores
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 10
      OnExit = MObsExit
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Observa'#231#227'o'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
    end
    object edAgencia: TsDBEdit
      Left = 467
      Top = 93
      Width = 66
      Height = 23
      Color = clWhite
      DataField = 'AGENCIA'
      DataSource = dsPortadores
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Ag'#234'ncia'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object ckCRA: TsDBCheckBox
      Left = 10
      Top = 61
      Width = 50
      Height = 19
      Cursor = crHandPoint
      Caption = 'CRA'
      TabOrder = 11
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
      DataField = 'CRA'
      DataSource = dsPortadores
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object edSequencia: TsDBEdit
      Left = 467
      Top = 26
      Width = 66
      Height = 23
      Color = clWhite
      DataField = 'SEQUENCIA'
      DataSource = dsPortadores
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Sequ'#234'ncia'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edDOC: TsDBEdit
      Left = 135
      Top = 141
      Width = 88
      Height = 23
      Color = clWhite
      DataField = 'VALOR_DOC'
      DataSource = dsPortadores
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Valor do DOC'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object edTED: TsDBEdit
      Left = 229
      Top = 141
      Width = 88
      Height = 23
      Color = clWhite
      DataField = 'VALOR_TED'
      DataSource = dsPortadores
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Valor do TED'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
    end
    object ckNominal: TsDBCheckBox
      Left = 7
      Top = 173
      Width = 520
      Height = 22
      Cursor = crHandPoint
      Caption = 
        'Marque esta op'#231#227'o se o cheque administrativo deste Portador for ' +
        'NOMINAL ao Cart'#243'rio'
      AutoSize = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 14
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
      DataField = 'NOMINAL'
      DataSource = dsPortadores
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object ckForca: TsDBCheckBox
      Left = 10
      Top = 122
      Width = 91
      Height = 25
      Cursor = crHandPoint
      Caption = 'For'#231'a da Lei'
      AutoSize = False
      TabOrder = 15
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
      DataField = 'FORCA_LEI'
      DataSource = dsPortadores
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object ckEspecie: TsDBCheckBox
      Left = 596
      Top = 171
      Width = 91
      Height = 20
      Cursor = crHandPoint
      Caption = 'Esp'#233'cie'
      AutoSize = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 16
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
      DataField = 'ESPECIE'
      DataSource = dsPortadores
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object chbPagaAntecipado: TsDBCheckBox
      Left = 10
      Top = 148
      Width = 117
      Height = 19
      Cursor = crHandPoint
      Caption = 'Pgto. Antecipado'
      TabOrder = 17
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
      DataField = 'FLG_PAGAANTECIPADO'
      DataSource = dsPortadores
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
  end
  object GridPorta: TwwDBGrid
    Left = 0
    Top = 73
    Width = 701
    Height = 231
    Selected.Strings = (
      'CODIGO'#9'10'#9'C'#243'digo'#9'F'
      'NOME'#9'84'#9'Nome'#9'F')
    IniAttributes.Delimiter = ';;'
    IniAttributes.UnicodeIniFile = False
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    BorderStyle = bsNone
    DataSource = dsPortadores
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Arial'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    OnTitleButtonClick = GridPortaTitleButtonClick
    ExplicitWidth = 672
  end
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 701
    Height = 40
    Align = alTop
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    ExplicitWidth = 672
    object btIncluir: TsBitBtn
      Left = 12
      Top = 6
      Width = 75
      Height = 28
      Cursor = crHandPoint
      Caption = 'Incluir'
      TabOrder = 0
      OnClick = btIncluirClick
      ImageIndex = 6
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object btAlterar: TsBitBtn
      Left = 92
      Top = 6
      Width = 75
      Height = 28
      Cursor = crHandPoint
      Caption = 'Alterar'
      TabOrder = 1
      OnClick = btAlterarClick
      ImageIndex = 8
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object btExcluir: TsBitBtn
      Left = 172
      Top = 6
      Width = 75
      Height = 28
      Cursor = crHandPoint
      Caption = 'Excluir'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333333333333333333FFF33FF333FFF339993370733
        999333777FF37FF377733339993000399933333777F777F77733333399970799
        93333333777F7377733333333999399933333333377737773333333333990993
        3333333333737F73333333333331013333333333333777FF3333333333910193
        333333333337773FF3333333399000993333333337377737FF33333399900099
        93333333773777377FF333399930003999333337773777F777FF339993370733
        9993337773337333777333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      TabOrder = 2
      OnClick = btExcluirClick
      SkinData.SkinSection = 'BUTTON'
    end
    object btSalvar: TsBitBtn
      Left = 268
      Top = 6
      Width = 75
      Height = 28
      Cursor = crHandPoint
      Caption = 'Salvar'
      TabOrder = 3
      OnClick = btSalvarClick
      ImageIndex = 3
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object btCancelar: TsBitBtn
      Left = 348
      Top = 6
      Width = 87
      Height = 28
      Cursor = crHandPoint
      Caption = 'Cancelar'
      TabOrder = 4
      OnClick = btCancelarClick
      ImageIndex = 5
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object btDuplicados: TsBitBtn
      Left = 587
      Top = 6
      Width = 100
      Height = 28
      Cursor = crHandPoint
      Hint = 'VISUALIZAR RELA'#199#195'O DE PORTADORES DUPLICADOS'
      Caption = 'Duplicados'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = btDuplicadosClick
      ImageIndex = 4
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object sPanel1: TsPanel
    Left = 0
    Top = 40
    Width = 701
    Height = 33
    Align = alTop
    TabOrder = 3
    SkinData.SkinSection = 'SELECTION'
    ExplicitWidth = 672
    object edCodigoPortador: TsEdit
      Left = 15
      Top = 5
      Width = 68
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnChange = edCodigoPortadorChange
      OnEnter = edCodigoPortadorEnter
      SkinData.SkinSection = 'EDIT'
    end
    object edNomePortador: TsEdit
      Left = 89
      Top = 4
      Width = 598
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnChange = edNomePortadorChange
      OnEnter = edNomePortadorEnter
      SkinData.SkinSection = 'EDIT'
    end
  end
  object dsPortadores: TDataSource
    DataSet = dm.Portadores
    OnStateChange = dsPortadoresStateChange
    OnDataChange = dsPortadoresDataChange
    Left = 64
    Top = 176
  end
end
