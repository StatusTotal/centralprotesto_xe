�
 TFQUICKRECIBO1 0j�  TPF0TFQuickRecibo1FQuickRecibo1LeftTop� CaptionReciboClientHeightPClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderScaledOnCreate
FormCreatePixelsPerInch`
TextHeight 	TQuickRepRecibo1Left+Top{�WidthHeightcFrame.DrawTop	ShowingPreviewDataSet
dm.TitulosFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' 	OnEndPageRecibo1EndPageOptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.ContinuousPage.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPreviewWidth�PreviewHeight�PrevInitialZoomqrZoomToWidthPreviewDefaultSaveTypestQRPPreviewLeft 
PreviewTop  TQRBand	CabecalhoLeft9Top9Width�HeightXFrame.DrawTop	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.ValuesVUUUUU��@      :�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageHeader 	TQRDBTexttxNomeServentiaLeft�TopWidthNHeightSize.Values�������@UUUUUU�@       �@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataSetdm.Serventia	DataField	DESCRICAOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize
  	TQRDBText
txEnderecoLeft�Top$Width7HeightSize.Values�������@UUUUUU	�@      ��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataSetdm.Serventia	DataFieldENDERECOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabel
qrTitular1Left?TopWidth+HeightSize.Values�������@VUUUUU�@��������@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionTITULARColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel1Left�TopFWidth� HeightSize.Values�������@UUUUUUu�@UUUUUU5�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption$   P R O T E S T O  D E  T Í T U L O SColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel	lbRecibo1Left`TopFWidthDHeightSize.Values�������@UUUUUU�	@UUUUUU5�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption
   RECIBO NºColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbProtocolo1LeftTopFWidthfHeightFrame.StylepsInsideFrameSize.Values�������@UUUUUUU�@UUUUUU5�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   PROTOCOLO NºColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  	TQRDBText
QRDBText31Left:Top4Width4HeightSize.Values�������@��������@UUUUUU��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataSetdm.Serventia	DataFieldTELEFONEFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize   TQRBandDetalheLeft9Top� Width�Height� Frame.DrawBottom	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values��������@      :�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail TQRShapeQRShape1Left TopWidth� Height� Size.Values��������@                 �@UUUUUUM�@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRLabelQRLabel2Left3TopWidthMHeightSize.ValuesUUUUUUU�@      ��@VUUUUU��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionApresentanteColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrDoc1Left
Top&WidthvHeightSize.ValuesUUUUUUU�@��������@UUUUUU�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionCPF do ApresentanteColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel5LeftTop6WidthbHeightSize.Values      ��@      ��@      ��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption   Espécie / Nº TítuloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel6LeftQTopFWidth/HeightSize.ValuesUUUUUUU�@      P�@UUUUUU5�@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionDevedorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRShapeQRShape2Left� TopWidthkHeight� Size.Values��������@�������@       �@      �@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRLabelqrDoc2Left(TopVWidthXHeightSize.ValuesUUUUUUU�@��������@��������@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionCPF do DevedorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText1Left� TopWidthdHeightSize.ValuesUUUUUUU�@UUUUUU=�@VUUUUU��@������z�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldAPRESENTANTEFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText2Left� Top&WidthVHeightSize.ValuesUUUUUUU�@UUUUUU=�@UUUUUU�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataSet
dm.Titulos	DataFieldDocApresentanteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText3Left� Top6WidthdHeightSize.ValuesUUUUUUU�@UUUUUU=�@      ��@������z�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldEspecieFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText4Left� TopFWidthdHeightSize.ValuesUUUUUUU�@UUUUUU=�@UUUUUU5�@������z�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldDEVEDORFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText5Left� TopVWidth>HeightSize.ValuesUUUUUUU�@UUUUUU=�@��������@������
�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataSet
dm.Titulos	DataField
DocDevedorFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabelQRLabel8Left+TopWidthUHeightSize.ValuesUUUUUUU�@��������@       �@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionData de EntradaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText6Left� TopWidthEHeightSize.ValuesUUUUUUU�@UUUUUU=�@       �@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataSet
dm.Titulos	DataField
DT_ENTRADAFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRShapeQRShape3LeftOTopWidthZHeight� Size.Values��������@      v�	@       �@�������@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRShapeQRShape4Left�TopWidthYHeight� Size.Values��������@UUUUUU]�	@       �@������z�@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRLabel	QRLabel10Left�TopWidthQHeightSize.ValuesUUUUUUU�@      ��	@       �@      P�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionEmolumentos:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel11Left,TopWidthHeightSize.ValuesUUUUUUU�@�������	@VUUUUU��@UUUUUUu�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionFETJ:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel12LeftTop&Width5HeightSize.ValuesUUUUUUU�@�������	@UUUUUU�@������:�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption	Fundperj:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel13LeftTop6Width.HeightSize.ValuesUUUUUUU�@UUUUUUC�	@      ��@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionFunperj:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel14Left*Top� WidthHeightSize.ValuesUUUUUUU�@UUUUUU9�	@      p�@������
�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionTotal:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText7LeftRTopWidthTHeightSize.ValuesUUUUUUU�@      t�	@VUUUUU��@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldFETJFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText8LeftRTopWidthTHeightSize.ValuesUUUUUUU�@      t�	@       �@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldEMOLUMENTOSFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText	QRDBText9LeftRTop&WidthTHeightSize.ValuesUUUUUUU�@      t�	@UUUUUU�@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldFUNDPERJFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText14LeftRTop6WidthTHeightSize.ValuesUUUUUUU�@      t�	@      ��@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldFUNPERJFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText15LeftRTop� WidthTHeightSize.ValuesUUUUUUU�@      t�	@      p�@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldTOTALFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabelqrCobranca1Left� TopfWidth@HeightSize.ValuesUUUUUUU�@UUUUUU=�@      ��@UUUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionqrCobranca1ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel4LeftKTopfWidth5HeightSize.ValuesUUUUUUU�@      p�@      ��@������:�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption	   CobrançaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel7LeftTopFWidth8HeightSize.ValuesUUUUUUU�@��������	@UUUUUU5�@������*�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption	Funarpen:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText16LeftRTopFWidthTHeightSize.ValuesUUUUUUU�@      t�	@UUUUUU5�@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldFUNARPENFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabel	QRLabel20LeftTopVWidth-HeightSize.ValuesUUUUUUU�@      ��	@��������@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionPmcmv:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText33LeftRTopVWidthTHeightSize.ValuesUUUUUUU�@      t�	@��������@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldPMCMVFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabel	QRLabel29Left4TopfWidthHeightSize.ValuesUUUUUUU�@      ��	@      ��@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionIss:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel33Left#TopvWidth&HeightSize.ValuesUUUUUUU�@�������	@�������@UUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption   Mútua:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel34LeftTop� Width,HeightSize.ValuesUUUUUUU�@�������	@UUUUUUE�@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionAcoterj:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText35LeftRTopfWidthTHeightSize.ValuesUUUUUUU�@      t�	@      ��@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldISSFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText36LeftRTopvWidthTHeightSize.ValuesUUUUUUU�@      t�	@�������@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldMUTUAFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText37LeftRTop� WidthTHeightSize.ValuesUUUUUUU�@      t�	@UUUUUUE�@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldACOTERJFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize   TQRSubDetailSubDetailCustasLeft9TopNWidth�HeightFrame.DrawLeft	Frame.DrawRight	AlignToBottomBeforePrintSubDetailCustasBeforePrintTransparentBandForceNewColumnForceNewPageSize.Values       �@      :�	@ PreCaluculateBandHeightKeepOnOnePageMasterRecibo1DataSetdm.RxCustasPrintBeforePrintIfEmpty	 	TQRDBText
QRDBText10Left� TopWidthtHeightSize.Values XUUUU��@ ������@ XUUUUU� @ ������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetdm.RxCustas	DataField	DESCRICAOFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText11LeftTopWidthHeightSize.Values XUUUU��@ XUUUUS�	@ XUUUUU� @ XUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSetdm.RxCustas	DataFieldQTDFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText12Left0TopWidth,HeightSize.Values XUUUU��@ XUUUU5�	@ XUUUUU� @ `UUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetdm.RxCustas	DataFieldVALORFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText13LeftpTopWidth,HeightSize.Values XUUUU��@     `�	@ XUUUUU� @ `UUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSetdm.RxCustas	DataFieldTOTALFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabellbTabelaLeftTopWidthuHeightSize.Values XUUUU��@ XUUUUU�@ XUUUUU� @      Ț@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionlbTabelaColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel3LeftTop WidthHeightSize.Values      ��@�������	@          ������*�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionxColorclWhiteTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel9LeftgTop WidthHeightSize.Values      ��@      f�	@          ������*�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption=ColorclWhiteTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   TQRBandQRBand1Left9TopZWidth�Height�Frame.DrawTop	AlignToBottomBeforePrintQRBand1BeforePrintTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUU}�	@      :�	@ PreCaluculateBandHeightKeepOnOnePageBandType	rbSummary TQRShapeQRShape9Left ToppWidth�HeightWSize.Values     0�@          ������*�@      :�	@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRLabel	lbCidade1Left�Top$Width� HeightSize.Values      ��@UUUUUUI�@      ��@      p�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionCidade, 00 de XXXXX de 0000.ColorclWhiteTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel22LeftTopLWidth�HeightSize.Values�������@UUUUUUU�@UUUUUU�@      H�	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption�- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRImageQRImage1LeftTopLWidthHeightSize.Values��������@       �@UUUUUU�@��������@ XLColumn XLNumFormat	nfGeneralAutoSize	Picture.Data
r  TBitmapf  BMf      v   (               �                    �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ����������  �������������������������������� �������������������������������������������������������������������������������������������������������������������������������� ��������������������������Stretch	  TQRLabel	QRLabel30Left�Top� Width� HeightSize.Values�������@UUUUUUu�@������r�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption$   P R O T E S T O  D E  T Í T U L O SColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel	lbRecibo2Left`Top� WidthDHeightSize.Values�������@UUUUUU�	@������r�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption
   RECIBO NºColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  	TQRDBText
QRDBText29Left-TopsWidthNHeightSize.Values�������@UUUUUU�@������"�@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataSetdm.Serventia	DataField	DESCRICAOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize
  	TQRDBText
QRDBText30Left�Top� Width7HeightSize.Values�������@UUUUUU	�@��������@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataSetdm.Serventia	DataFieldENDERECOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabel
qrTitular2Left?Top� Width+HeightSize.Values�������@VUUUUU�@      ��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionTITULARColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbProtocolo2LeftTop� WidthfHeightFrame.StylepsInsideFrameSize.Values�������@       �@������r�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   PROTOCOLO NºColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  	TQRDBText
QRDBText32Left:Top� Width4HeightSize.Values�������@��������@UUUUUU��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataSetdm.Serventia	DataFieldTELEFONEFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRShapeQRShape5Left Top� Width� Height� Size.Values��������@          ������F�@UUUUUUM�@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRShapeQRShape6Left� Top� WidthkHeight� Size.Values��������@�������@������F�@      �@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRShapeQRShape7LeftOTop� WidthZHeight� Size.Values��������@      v�	@������F�@�������@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRShapeQRShape8Left�Top� WidthYHeight� Size.Values��������@UUUUUU]�	@������F�@������z�@ XLColumn XLNumFormat	nfGeneralShapeqrsRectangle
VertAdjust   TQRLabel	QRLabel24Left�Top� WidthQHeightSize.ValuesUUUUUUU�@      ��	@������B�@      P�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionEmolumentos:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel25Left,Top� WidthHeightSize.ValuesUUUUUUU�@�������	@      ؒ@UUUUUUu�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionFETJ:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel26LeftTop� Width5HeightSize.ValuesUUUUUUU�@�������	@UUUUUUm�@������:�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption	Fundperj:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel27LeftTop� Width.HeightSize.ValuesUUUUUUU�@UUUUUUC�	@�������@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionFunperj:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel28Left*Top^WidthHeightSize.ValuesUUUUUUU�@UUUUUU9�	@��������@������
�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionTotal:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText23LeftRTop� WidthTHeightSize.ValuesUUUUUUU�@      t�	@      ؒ@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldFETJFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText24LeftRTop� WidthTHeightSize.ValuesUUUUUUU�@      t�	@������B�@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldEMOLUMENTOSFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText25LeftRTop� WidthTHeightSize.ValuesUUUUUUU�@      t�	@UUUUUUm�@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldFUNDPERJFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText26LeftRTop� WidthTHeightSize.ValuesUUUUUUU�@      t�	@�������@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldFUNPERJFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText27LeftRTop^WidthTHeightSize.ValuesUUUUUUU�@      t�	@��������@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldTOTALFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText17Left� Top� WidthdHeightSize.ValuesUUUUUUU�@UUUUUU=�@      ؒ@������z�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldAPRESENTANTEFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText18Left� Top� WidthVHeightSize.ValuesUUUUUUU�@UUUUUU=�@UUUUUUm�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataSet
dm.Titulos	DataFieldDocApresentanteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText19Left� Top� WidthdHeightSize.ValuesUUUUUUU�@UUUUUU=�@�������@������z�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldEspecieFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText20Left� TopWidthdHeightSize.ValuesUUUUUUU�@UUUUUU=�@      ��@������z�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldDEVEDORFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText21Left� TopWidth>HeightSize.ValuesUUUUUUU�@UUUUUU=�@UUUUUU-�@������
�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataSet
dm.Titulos	DataField
DocDevedorFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText22Left� Top� WidthEHeightSize.ValuesUUUUUUU�@UUUUUU=�@������B�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataSet
dm.Titulos	DataField
DT_ENTRADAFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabel	QRLabel16Left3Top� WidthMHeightSize.ValuesUUUUUUU�@      ��@      ؒ@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionApresentanteColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrDoc3Left
Top� WidthvHeightSize.ValuesUUUUUUU�@��������@UUUUUUm�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionCPF do ApresentanteColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel18LeftTop� WidthbHeightSize.Values      ��@      ��@�������@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption   Espécie / Nº TítuloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel19LeftQTopWidth/HeightSize.ValuesUUUUUUU�@      P�@      ��@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionDevedorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrDoc4Left(TopWidthXHeightSize.ValuesUUUUUUU�@��������@UUUUUU-�@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionCPF do DevedorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel21Left+Top� WidthUHeightSize.ValuesUUUUUUU�@��������@������B�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionData de EntradaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrCobranca2Left� Top.Width@HeightSize.ValuesUUUUUUU�@UUUUUU=�@��������@UUUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionqrCobranca2ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel17LeftKTop.Width5HeightSize.ValuesUUUUUUU�@      p�@��������@������:�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption	   CobrançaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel15LeftTopWidth8HeightSize.ValuesUUUUUUU�@��������	@      ��@������*�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption	Funarpen:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText28LeftRTopWidthTHeightSize.ValuesUUUUUUU�@      t�	@      ��@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldFUNARPENFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText34LeftRTopWidthTHeightSize.ValuesUUUUUUU�@      t�	@UUUUUU-�@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldPMCMVFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabel	QRLabel23LeftTopWidth-HeightSize.ValuesUUUUUUU�@      ��	@UUUUUU-�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionPmcmv:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	lbCidade2LeftTop�Width�HeightSize.Values      ��@UUUUUUU�@������H�	@������R�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionFCidade, 00 de XXXXX de 0000, __________________________ (Apresentante)ColorclWhiteTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel31LeftTTop�WidthQHeightSize.ValuesUUUUUU��@UUUUUU�	@UUUUUU��	@      P�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionTabela de CustasColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRMemoMLeftTop�Width�HeightSize.ValuesUUUUUU��@UUUUUUU�@UUUUUU��	@��������	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretch	ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentFullJustifyMaxBreakChars FontSize  TQRLabel	QRLabel35Left4Top.WidthHeightSize.ValuesUUUUUUU�@      ��	@��������@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionIss:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel36Left#Top>Width&HeightSize.ValuesUUUUUUU�@�������	@      X�@UUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption   Mútua:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel37LeftTopNWidth,HeightSize.ValuesUUUUUUU�@�������	@UUUUUU��@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionAcoterj:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText38LeftRTop.WidthTHeightSize.ValuesUUUUUUU�@      t�	@��������@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldISSFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText39LeftRTop>WidthTHeightSize.ValuesUUUUUUU�@      t�	@      X�@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldMUTUAFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText40LeftRTopNWidthTHeightSize.ValuesUUUUUUU�@      t�	@UUUUUU��@     @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataSet
dm.Titulos	DataFieldACOTERJFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize   TQRChildBand
ChildBand1Left9Top� Width�HeightFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values�������@      :�	@ PreCaluculateBandHeightKeepOnOnePage
ParentBand	Cabecalho
PrintOrdercboAfterParent  TQRLabel	QRLabel32Left9Top*WidthHHeightSize.Values      ��@      Ж@     @�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption   1ª Via - ClienteColorclWhiteTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize    