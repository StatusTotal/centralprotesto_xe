object FSelosUtilizados: TFSelosUtilizados
  Left = 235
  Top = 211
  ActiveControl = edData2
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Relat'#243'rio de Selos'
  ClientHeight = 119
  ClientWidth = 250
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 250
    Height = 119
    Align = alClient
    TabOrder = 0
    SkinData.SkinSection = 'SELECTION'
    object edData1: TsDateEdit
      Left = 9
      Top = 21
      Width = 108
      Height = 24
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 0
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'In'#237'cio'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -13
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
    end
    object edData2: TsDateEdit
      Left = 132
      Top = 21
      Width = 108
      Height = 24
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Fim'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -13
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
    end
    object btVisualizar: TsBitBtn
      Left = 84
      Top = 69
      Width = 75
      Height = 31
      Caption = 'Visualizar'
      TabOrder = 2
      OnClick = btVisualizarClick
      SkinData.SkinSection = 'BUTTON'
    end
    object btFechar: TsBitBtn
      Left = 165
      Top = 69
      Width = 75
      Height = 31
      Caption = 'Fechar'
      TabOrder = 3
      OnClick = btFecharClick
      SkinData.SkinSection = 'BUTTON'
    end
    object qkRelatorio: TQuickRep
      Left = 248
      Top = 214
      Width = 794
      Height = 1123
      ShowingPreview = False
      DataSet = RX
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      Functions.Strings = (
        'PAGENUMBER'
        'COLUMNNUMBER'
        'REPORTTITLE')
      Functions.DATA = (
        '0'
        '0'
        #39#39)
      OnEndPage = qkRelatorioEndPage
      Options = [FirstPageHeader, LastPageFooter]
      Page.Columns = 1
      Page.Orientation = poPortrait
      Page.PaperSize = A4
      Page.Continuous = False
      Page.Values = (
        100.000000000000000000
        2970.000000000000000000
        100.000000000000000000
        2100.000000000000000000
        100.000000000000000000
        100.000000000000000000
        0.000000000000000000)
      PrinterSettings.Copies = 1
      PrinterSettings.OutputBin = Auto
      PrinterSettings.Duplex = False
      PrinterSettings.FirstPage = 0
      PrinterSettings.LastPage = 0
      PrinterSettings.UseStandardprinter = False
      PrinterSettings.UseCustomBinCode = False
      PrinterSettings.CustomBinCode = 0
      PrinterSettings.ExtendedDuplex = 0
      PrinterSettings.UseCustomPaperCode = False
      PrinterSettings.CustomPaperCode = 0
      PrinterSettings.PrintMetaFile = False
      PrinterSettings.MemoryLimit = 1000000
      PrinterSettings.PrintQuality = 0
      PrinterSettings.Collate = 0
      PrinterSettings.ColorOption = 0
      PrintIfEmpty = True
      SnapToGrid = True
      Units = MM
      Zoom = 100
      PrevFormStyle = fsNormal
      PreviewInitialState = wsMaximized
      PreviewWidth = 500
      PreviewHeight = 500
      PrevInitialZoom = qrZoomToWidth
      PreviewDefaultSaveType = stQRP
      PreviewLeft = 0
      PreviewTop = 0
      object QRBand6: TQRBand
        Left = 38
        Top = 38
        Width = 718
        Height = 84
        Frame.DrawTop = True
        Frame.DrawBottom = True
        Frame.DrawLeft = True
        Frame.DrawRight = True
        AlignToBottom = False
        TransparentBand = False
        ForceNewColumn = False
        ForceNewPage = False
        Size.Values = (
          222.250000000000000000
          1899.708333333333000000)
        PreCaluculateBandHeight = False
        KeepOnOnePage = False
        BandType = rbPageHeader
        object lbServentia: TQRLabel
          Left = 302
          Top = 8
          Width = 113
          Height = 17
          Size.Values = (
            44.979166666666670000
            799.041666666666700000
            21.166666666666670000
            298.979166666666700000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = True
          Caption = 'Nome do Cart'#243'rio'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = False
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 10
        end
        object lbTitulo: TQRLabel
          Left = 252
          Top = 36
          Width = 213
          Height = 17
          Size.Values = (
            44.979166666666670000
            666.750000000000000000
            95.250000000000000000
            563.562500000000000000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = True
          Caption = 'RELA'#199#195'O DE SELOS UTILIZADOS'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = False
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 10
        end
        object QRSysData6: TQRSysData
          Left = 637
          Top = 64
          Width = 77
          Height = 17
          Size.Values = (
            44.979166666666670000
            1685.395833333333000000
            169.333333333333300000
            203.729166666666700000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taRightJustify
          AlignToBand = False
          Color = clWhite
          Data = qrsPageNumber
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Text = 'P'#225'gina: '
          Transparent = False
          ExportAs = exptText
          FontSize = 8
        end
        object QRSysData7: TQRSysData
          Left = 4
          Top = 64
          Width = 56
          Height = 17
          Size.Values = (
            44.979166666666670000
            10.583333333333330000
            169.333333333333300000
            148.166666666666700000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taLeftJustify
          AlignToBand = False
          Color = clWhite
          Data = qrsDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Text = ''
          Transparent = False
          ExportAs = exptText
          FontSize = 8
        end
        object lbMovimento: TQRLabel
          Left = 327
          Top = 64
          Width = 63
          Height = 17
          Size.Values = (
            44.979166666666670000
            865.187500000000000000
            169.333333333333300000
            166.687500000000000000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = True
          Caption = 'Movimento'
          Color = clWhite
          Transparent = False
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 10
        end
      end
      object QRChildBand2: TQRChildBand
        Left = 38
        Top = 122
        Width = 718
        Height = 37
        Frame.DrawBottom = True
        Frame.DrawLeft = True
        Frame.DrawRight = True
        AlignToBottom = False
        TransparentBand = False
        ForceNewColumn = False
        ForceNewPage = False
        Size.Values = (
          97.895833333333330000
          1899.708333333333000000)
        PreCaluculateBandHeight = False
        KeepOnOnePage = False
        ParentBand = QRBand6
        PrintOrder = cboAfterParent
        object QRLabel19: TQRLabel
          Left = 29
          Top = 19
          Width = 64
          Height = 17
          Size.Values = (
            44.979166666666670000
            76.729166666666670000
            50.270833333333330000
            169.333333333333300000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = False
          Caption = 'Protocolo'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 9
        end
        object qrSelo: TQRLabel
          Left = 279
          Top = 19
          Width = 29
          Height = 17
          Size.Values = (
            44.979166666666670000
            738.187500000000000000
            50.270833333333330000
            76.729166666666670000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = False
          Caption = 'Selo'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 9
        end
        object QRLabel28: TQRLabel
          Left = 383
          Top = 19
          Width = 29
          Height = 17
          Size.Values = (
            44.979166666666670000
            1013.354166666667000000
            50.270833333333330000
            76.729166666666670000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = False
          Caption = 'Data'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 9
        end
        object QRLabel20: TQRLabel
          Left = 494
          Top = 19
          Width = 22
          Height = 17
          Size.Values = (
            44.979166666666670000
            1307.041666666667000000
            50.270833333333330000
            58.208333333333330000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = False
          Caption = 'CCT'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 9
        end
        object QRLabel23: TQRLabel
          Left = 615
          Top = 19
          Width = 43
          Height = 17
          Size.Values = (
            44.979166666666670000
            1627.187500000000000000
            50.270833333333330000
            113.770833333333300000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = False
          Caption = 'Status'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 9
        end
        object QRLabel1: TQRLabel
          Left = 152
          Top = 19
          Width = 43
          Height = 17
          Size.Values = (
            44.979166666666670000
            402.166666666666700000
            50.270833333333330000
            113.770833333333300000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = False
          Caption = 'Recibo'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 9
        end
      end
      object QRBand7: TQRBand
        Left = 38
        Top = 159
        Width = 718
        Height = 16
        Frame.DrawBottom = True
        Frame.DrawLeft = True
        Frame.DrawRight = True
        AlignToBottom = False
        TransparentBand = False
        ForceNewColumn = False
        ForceNewPage = False
        Size.Values = (
          42.333333333333330000
          1899.708333333333000000)
        PreCaluculateBandHeight = False
        KeepOnOnePage = False
        BandType = rbDetail
        object QRDBText6: TQRDBText
          Left = 18
          Top = 1
          Width = 87
          Height = 15
          Size.Values = (
            39.687500000000000000
            47.625000000000000000
            2.645833333333333000
            230.187500000000000000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = False
          AutoSize = False
          Color = clWhite
          DataSet = RX
          DataField = 'PROTOCOLO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = False
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          FullJustify = False
          MaxBreakChars = 0
          VerticalAlignment = tlTop
          FontSize = 8
        end
        object QRDBText9: TQRDBText
          Left = 242
          Top = 1
          Width = 103
          Height = 15
          Size.Values = (
            39.687500000000000000
            640.291666666666700000
            2.645833333333333000
            272.520833333333300000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = False
          AutoSize = False
          Color = clWhite
          DataSet = RX
          DataField = 'SELO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = False
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          FullJustify = False
          MaxBreakChars = 0
          VerticalAlignment = tlTop
          FontSize = 8
        end
        object QRDBText10: TQRDBText
          Left = 359
          Top = 1
          Width = 78
          Height = 15
          Size.Values = (
            39.687500000000000000
            949.854166666666700000
            2.645833333333333000
            206.375000000000000000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = False
          AutoSize = False
          Color = clWhite
          DataSet = RX
          DataField = 'DATA'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = False
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          FullJustify = False
          MaxBreakChars = 0
          VerticalAlignment = tlTop
          FontSize = 8
        end
        object QRDBText11: TQRDBText
          Left = 458
          Top = 1
          Width = 95
          Height = 15
          Size.Values = (
            39.687500000000000000
            1211.791666666667000000
            2.645833333333333000
            251.354166666666700000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taLeftJustify
          AlignToBand = False
          AutoSize = False
          Color = clWhite
          DataSet = RX
          DataField = 'CCT'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = False
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          FullJustify = False
          MaxBreakChars = 0
          VerticalAlignment = tlTop
          FontSize = 8
        end
        object QRDBText12: TQRDBText
          Left = 560
          Top = 1
          Width = 153
          Height = 15
          Size.Values = (
            39.687500000000000000
            1481.666666666667000000
            2.645833333333333000
            404.812500000000000000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = False
          AutoSize = False
          Color = clWhite
          DataSet = RX
          DataField = 'STATUS'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = False
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          FullJustify = False
          MaxBreakChars = 0
          VerticalAlignment = tlTop
          FontSize = 8
        end
        object QRDBText1: TQRDBText
          Left = 122
          Top = 1
          Width = 103
          Height = 15
          Size.Values = (
            39.687500000000000000
            322.791666666666700000
            2.645833333333333000
            272.520833333333300000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = False
          AutoSize = False
          Color = clWhite
          DataSet = RX
          DataField = 'RECIBO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = False
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          FullJustify = False
          MaxBreakChars = 0
          VerticalAlignment = tlTop
          FontSize = 8
        end
      end
      object QRBand8: TQRBand
        Left = 38
        Top = 175
        Width = 718
        Height = 87
        Frame.DrawBottom = True
        Frame.DrawLeft = True
        Frame.DrawRight = True
        AlignToBottom = False
        TransparentBand = False
        ForceNewColumn = False
        ForceNewPage = False
        Size.Values = (
          230.187500000000000000
          1899.708333333333000000)
        PreCaluculateBandHeight = False
        KeepOnOnePage = False
        BandType = rbSummary
        object lbCidade: TQRLabel
          Left = 321
          Top = 56
          Width = 76
          Height = 17
          Size.Values = (
            44.979166666666670000
            849.312500000000000000
            148.166666666666700000
            201.083333333333300000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = True
          Caption = 'Cidade, Data'
          Color = clWhite
          Transparent = False
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 10
        end
        object QRSysData8: TQRSysData
          Left = 179
          Top = 24
          Width = 359
          Height = 17
          Size.Values = (
            44.979166666666670000
            473.604166666666700000
            63.500000000000000000
            949.854166666666700000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = True
          Color = clWhite
          Data = qrsDetailCount
          Text = 'TOTAL DE SELOS UTILIZADOS NO PER'#205'ODO: '
          Transparent = False
          ExportAs = exptText
          FontSize = 10
        end
      end
    end
  end
  object RX: TRxMemoryData
    FieldDefs = <>
    BeforePost = RXBeforePost
    Left = 816
    Top = 120
    object RXPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
    end
    object RXRECIBO: TIntegerField
      FieldName = 'RECIBO'
    end
    object RXSELO: TStringField
      FieldName = 'SELO'
      Size = 14
    end
    object RXDATA: TDateField
      FieldName = 'DATA'
    end
    object RXCCT: TStringField
      FieldName = 'CCT'
      Size = 10
    end
    object RXSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 30
    end
  end
  object CertidaoCanceladosFolhas: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      ''
      'ID_CERTIDAO,'
      'ID_ATO,'
      'DT_ENTREGA,'
      'DT_CERTIDAO,'
      'TIPO_CERTIDAO,'
      'CODIGO,'
      'RECIBO,'
      'SELO,'
      'ALEATORIO,'
      'CCT,'
      'EXRECIBO'
      ''
      'from CERTIDOES'
      ''
      'where DT_CERTIDAO between :D1 and :D2'
      ''
      'order by RECIBO')
    Left = 464
    Top = 24
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end>
    object CertidaoCanceladosFolhasID_CERTIDAO: TIntegerField
      FieldName = 'ID_CERTIDAO'
      Origin = 'ID_CERTIDAO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object CertidaoCanceladosFolhasID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
    end
    object CertidaoCanceladosFolhasDT_ENTREGA: TDateField
      FieldName = 'DT_ENTREGA'
      Origin = 'DT_ENTREGA'
    end
    object CertidaoCanceladosFolhasDT_CERTIDAO: TDateField
      FieldName = 'DT_CERTIDAO'
      Origin = 'DT_CERTIDAO'
    end
    object CertidaoCanceladosFolhasTIPO_CERTIDAO: TStringField
      FieldName = 'TIPO_CERTIDAO'
      Origin = 'TIPO_CERTIDAO'
      FixedChar = True
      Size = 1
    end
    object CertidaoCanceladosFolhasCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
    end
    object CertidaoCanceladosFolhasRECIBO: TIntegerField
      FieldName = 'RECIBO'
      Origin = 'RECIBO'
    end
    object CertidaoCanceladosFolhasSELO: TStringField
      FieldName = 'SELO'
      Origin = 'SELO'
      Size = 9
    end
    object CertidaoCanceladosFolhasALEATORIO: TStringField
      FieldName = 'ALEATORIO'
      Origin = 'ALEATORIO'
      Size = 3
    end
    object CertidaoCanceladosFolhasCCT: TStringField
      FieldName = 'CCT'
      Origin = 'CCT'
      Size = 9
    end
    object CertidaoCanceladosFolhasEXRECIBO: TIntegerField
      FieldName = 'EXRECIBO'
      Origin = 'EXRECIBO'
    end
  end
  object Protestados: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      ''
      'PROTOCOLO,'
      'RECIBO,'
      'SELO_REGISTRO,'
      'ALEATORIO_PROTESTO,'
      'DT_REGISTRO'
      ''
      'FROM TITULOS'
      ''
      'where '
      ''
      'DT_REGISTRO between :D1 and :D2 and'
      ''
      'PROTESTADO='#39'S'#39)
    Left = 552
    Top = 48
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end>
    object ProtestadosPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object ProtestadosRECIBO: TIntegerField
      FieldName = 'RECIBO'
      Origin = 'RECIBO'
    end
    object ProtestadosSELO_REGISTRO: TStringField
      FieldName = 'SELO_REGISTRO'
      Origin = 'SELO_REGISTRO'
      Size = 9
    end
    object ProtestadosALEATORIO_PROTESTO: TStringField
      FieldName = 'ALEATORIO_PROTESTO'
      Origin = 'ALEATORIO_PROTESTO'
      Size = 3
    end
    object ProtestadosDT_REGISTRO: TDateField
      FieldName = 'DT_REGISTRO'
      Origin = 'DT_REGISTRO'
    end
  end
  object Pagos: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      ''
      'PROTOCOLO,'
      'RECIBO,'
      'SELO_PAGAMENTO,'
      'ALEATORIO_SOLUCAO,'
      'DT_PAGAMENTO'
      ''
      'FROM TITULOS'
      ''
      'where '
      ''
      'DT_PAGAMENTO between :D1 and :D2 and'
      ''
      'STATUS='#39'PAGO'#39)
    Left = 632
    Top = 24
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end>
    object PagosPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object PagosRECIBO: TIntegerField
      FieldName = 'RECIBO'
      Origin = 'RECIBO'
    end
    object PagosSELO_PAGAMENTO: TStringField
      FieldName = 'SELO_PAGAMENTO'
      Origin = 'SELO_PAGAMENTO'
      Size = 9
    end
    object PagosALEATORIO_SOLUCAO: TStringField
      FieldName = 'ALEATORIO_SOLUCAO'
      Origin = 'ALEATORIO_SOLUCAO'
      Size = 3
    end
    object PagosDT_PAGAMENTO: TDateField
      FieldName = 'DT_PAGAMENTO'
      Origin = 'DT_PAGAMENTO'
    end
  end
  object Retirados: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      ''
      'PROTOCOLO,'
      'RECIBO,'
      'SELO_PAGAMENTO,'
      'ALEATORIO_SOLUCAO,'
      'DT_RETIRADO'
      ''
      'FROM TITULOS'
      ''
      'where '
      ''
      'DT_RETIRADO between :D1 and :D2 and'
      ''
      'STATUS='#39'RETIRADO'#39)
    Left = 720
    Top = 48
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end>
    object RetiradosPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object RetiradosRECIBO: TIntegerField
      FieldName = 'RECIBO'
      Origin = 'RECIBO'
    end
    object RetiradosSELO_PAGAMENTO: TStringField
      FieldName = 'SELO_PAGAMENTO'
      Origin = 'SELO_PAGAMENTO'
      Size = 9
    end
    object RetiradosALEATORIO_SOLUCAO: TStringField
      FieldName = 'ALEATORIO_SOLUCAO'
      Origin = 'ALEATORIO_SOLUCAO'
      Size = 3
    end
    object RetiradosDT_RETIRADO: TDateField
      FieldName = 'DT_RETIRADO'
      Origin = 'DT_RETIRADO'
    end
  end
  object Apontamentos: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      ''
      'PROTOCOLO,'
      'CCT,'
      'DT_PROTOCOLO'
      ''
      'FROM TITULOS'
      ''
      'where '
      ''
      'DT_PROTOCOLO between :D1 and :D2')
    Left = 816
    Top = 24
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end>
    object ApontamentosPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object ApontamentosCCT: TStringField
      FieldName = 'CCT'
      Origin = 'CCT'
      Size = 9
    end
    object ApontamentosDT_PROTOCOLO: TDateField
      FieldName = 'DT_PROTOCOLO'
      Origin = 'DT_PROTOCOLO'
    end
  end
end
