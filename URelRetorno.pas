unit URelRetorno;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, sDBLookupComboBox, StdCtrls, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, sGroupBox, ExtCtrls, sPanel, Buttons,
  sBitBtn, DB, DBClient, SimpleDS, Grids, DBGrids, acDBGrid, Wwdbigrd,
  Wwdbgrid, RxMemDS, Menus, Data.DBXFirebird, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFRelRetorno = class(TForm)
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    P1: TsPanel;
    sGroupBox1: TsGroupBox;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    lkPortador: TsDBLookupComboBox;
    dsPortador: TDataSource;
    sBitBtn1: TsBitBtn;
    dsRX: TDataSource;
    RX: TRxMemoryData;
    RXCheck: TBooleanField;
    RXProtocolo: TIntegerField;
    RXNossoNumero: TStringField;
    RXCustas: TFloatField;
    RXSituacao: TStringField;
    RXData: TDateField;
    RXEdital: TFloatField;
    RXRetorno: TStringField;
    RXValorTitulo: TFloatField;
    Grid: TwwDBGrid;
    PM: TPopupMenu;
    Marcartodos1: TMenuItem;
    Desmarcartodos1: TMenuItem;
    Titulos: TFDQuery;
    TitulosID_ATO: TIntegerField;
    TitulosPROTOCOLO: TIntegerField;
    TitulosDT_REGISTRO: TDateField;
    TitulosDT_PAGAMENTO: TDateField;
    TitulosTOTAL: TFloatField;
    TitulosNOSSO_NUMERO: TStringField;
    TitulosSTATUS: TStringField;
    TitulosDT_RETIRADO: TDateField;
    TitulosDT_SUSTADO: TDateField;
    TitulosARQUIVO: TStringField;
    TitulosVALOR_TITULO: TFloatField;
    TitulosTARIFA_BANCARIA: TFloatField;
    TitulosCheck: TBooleanField;
    procedure btSairClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btVisualizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure sBitBtn1Click(Sender: TObject);
    procedure RXFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure GridMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Marcartodos1Click(Sender: TObject);
    procedure Desmarcartodos1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Total: Double;
  end;

var
  FRelRetorno: TFRelRetorno;

implementation

uses UDM, UPF, UQuickRetorno1, UGeral;

{$R *.dfm}

procedure TFRelRetorno.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFRelRetorno.FormCreate(Sender: TObject);
begin
  Titulos.Connection:=dm.conSISTEMA;
  dm.Portadores.Close;
  dm.Portadores.Open;
  RX.Close;
  RX.Open;
end;

procedure TFRelRetorno.btVisualizarClick(Sender: TObject);
var
  Query: String;
begin
  if lkPortador.KeyValue=Null then
  begin
      lkPortador.SetFocus;
      Exit;
  end;

  if edInicio.Date=0 then
  begin
      edInicio.SetFocus;
      Exit;
  end;

  if edFim.Date=0 then
  begin
      edFim.SetFocus;
      Exit;
  end;

  Query:='SELECT ID_ATO,PROTOCOLO,DT_REGISTRO,DT_PAGAMENTO,TOTAL,NOSSO_NUMERO,STATUS,DT_RETIRADO,DT_SUSTADO,'+
         'ARQUIVO,VALOR_TITULO,TARIFA_BANCARIA FROM TITULOS WHERE ';

  if dm.PortadoresCODIGO.AsString<>'' then
    Query:=Query+'CODIGO_APRESENTANTE='+QuotedStr(dm.PortadoresCODIGO.AsString)
      else
        if dm.PortadoresDOCUMENTO.AsString<>'' then
          Query:=Query+'CPF_CNPJ_APRESENTANTE='+QuotedStr(dm.PortadoresDOCUMENTO.AsString)
            else
              Query:=Query+'APRESENTANTE='+QuotedStr(dm.PortadoresNOME.AsString);

  PF.Aguarde(True);
  Total:=0;
  RX.Close;
  RX.Open;

  {PROTESTADOS}
  Titulos.Close;
  Titulos.SQL.Text:=Query+' AND STATUS='''+'PROTESTADO'+'''AND DT_REGISTRO BETWEEN :D1 AND :D2';
  Titulos.Params.ParamByName('D1').AsDate:=edInicio.Date;
  Titulos.Params.ParamByName('D2').AsDate:=edFim.Date;
  Titulos.Open;
  Titulos.First;
  while not Titulos.Eof do
  begin
      RX.Append;
      RXCheck.AsBoolean       :=True;
      RXProtocolo.AsInteger   :=TitulosPROTOCOLO.AsInteger;
      RXNossoNumero.AsString  :=TitulosNOSSO_NUMERO.AsString;
      RXData.AsDateTime       :=TitulosDT_REGISTRO.AsDateTime;
      RXSituacao.AsString     :=TitulosSTATUS.AsString;
      RXCustas.AsFloat        :=TitulosTOTAL.AsFloat-TitulosTARIFA_BANCARIA.AsFloat;
      RXValorTitulo.AsFloat   :=TitulosVALOR_TITULO.AsFloat;
      RXRetorno.AsString      :=TitulosARQUIVO.AsString;
      Total                   :=Total+TitulosVALOR_TITULO.AsFloat;
      RX.Post;
      Titulos.Next;
  end;

  {PAGOS}
  Titulos.Close;
  Titulos.SQL.Text:=Query+' AND STATUS='''+'PAGO'+'''AND DT_PAGAMENTO BETWEEN :D1 AND :D2';
  Titulos.Params.ParamByName('D1').AsDate:=edInicio.Date;
  Titulos.Params.ParamByName('D2').AsDate:=edFim.Date;
  Titulos.Open;
  Titulos.First;
  while not Titulos.Eof do
  begin
      RX.Append;
      RXCheck.AsBoolean       :=True;
      RXProtocolo.AsInteger   :=TitulosPROTOCOLO.AsInteger;
      RXNossoNumero.AsString  :=TitulosNOSSO_NUMERO.AsString;
      RXData.AsDateTime       :=TitulosDT_PAGAMENTO.AsDateTime;
      RXSituacao.AsString     :=TitulosSTATUS.AsString;
      RXCustas.AsFloat        :=TitulosTOTAL.AsFloat-TitulosTARIFA_BANCARIA.AsFloat;
      RXValorTitulo.AsFloat   :=TitulosVALOR_TITULO.AsFloat;
      RXRetorno.AsString      :=TitulosARQUIVO.AsString;
      Total                   :=Total+TitulosVALOR_TITULO.AsFloat;
      RX.Post;
      Titulos.Next;
  end;

  {RETIRADOS}
  Titulos.Close;
  Titulos.SQL.Text:=Query+' AND STATUS='''+'RETIRADO'+'''AND DT_RETIRADO BETWEEN :D1 AND :D2';
  Titulos.Params.ParamByName('D1').AsDate:=edInicio.Date;
  Titulos.Params.ParamByName('D2').AsDate:=edFim.Date;
  Titulos.Open;
  Titulos.First;
  while not Titulos.Eof do
  begin
      RX.Append;
      RXCheck.AsBoolean       :=True;
      RXProtocolo.AsInteger   :=TitulosPROTOCOLO.AsInteger;
      RXNossoNumero.AsString  :=TitulosNOSSO_NUMERO.AsString;
      RXData.AsDateTime       :=TitulosDT_RETIRADO.AsDateTime;
      RXSituacao.AsString     :=TitulosSTATUS.AsString;
      RXCustas.AsFloat        :=TitulosTOTAL.AsFloat-TitulosTARIFA_BANCARIA.AsFloat;
      RXValorTitulo.AsFloat   :=TitulosVALOR_TITULO.AsFloat;
      RXRetorno.AsString      :=TitulosARQUIVO.AsString;
      Total                   :=Total+TitulosVALOR_TITULO.AsFloat;
      RX.Post;
      Titulos.Next;
  end;

  {SUSTADOS}
  Titulos.Close;
  Titulos.SQL.Text:=Query+' AND STATUS='''+'SUSTADO'+'''AND DT_SUSTADO BETWEEN :D1 AND :D2';
  Titulos.Params.ParamByName('D1').AsDate:=edInicio.Date;
  Titulos.Params.ParamByName('D2').AsDate:=edFim.Date;
  Titulos.Open;
  Titulos.First;
  while not Titulos.Eof do
  begin
      RX.Append;
      RXCheck.AsBoolean       :=True;
      RXProtocolo.AsInteger   :=TitulosPROTOCOLO.AsInteger;
      RXNossoNumero.AsString  :=TitulosNOSSO_NUMERO.AsString;
      RXData.AsDateTime       :=TitulosDT_SUSTADO.AsDateTime;
      RXSituacao.AsString     :=TitulosSTATUS.AsString;
      RXCustas.AsFloat        :=TitulosTOTAL.AsFloat-TitulosTARIFA_BANCARIA.AsFloat;
      RXValorTitulo.AsFloat   :=TitulosVALOR_TITULO.AsFloat;
      RXRetorno.AsString      :=TitulosARQUIVO.AsString;
      Total                   :=Total+TitulosVALOR_TITULO.AsFloat;
      RX.Post;
      Titulos.Next;
  end;

  {CANCELADO}
  Titulos.Close;
  Titulos.SQL.Text:=Query+' AND STATUS='''+'CANCELADO'+'''AND DT_PAGAMENTO BETWEEN :D1 AND :D2';
  Titulos.Params.ParamByName('D1').AsDate:=edInicio.Date;
  Titulos.Params.ParamByName('D2').AsDate:=edFim.Date;
  Titulos.Open;
  Titulos.First;
  while not Titulos.Eof do
  begin
      RX.Append;
      RXCheck.AsBoolean       :=True;
      RXProtocolo.AsInteger   :=TitulosPROTOCOLO.AsInteger;
      RXNossoNumero.AsString  :=TitulosNOSSO_NUMERO.AsString;
      RXData.AsDateTime       :=TitulosDT_PAGAMENTO.AsDateTime;
      RXSituacao.AsString     :=TitulosSTATUS.AsString;
      RXCustas.AsFloat        :=TitulosTOTAL.AsFloat-TitulosTARIFA_BANCARIA.AsFloat;
      RXValorTitulo.AsFloat   :=TitulosVALOR_TITULO.AsFloat;
      RXRetorno.AsString      :=TitulosARQUIVO.AsString;
      Total                   :=Total+TitulosVALOR_TITULO.AsFloat;
      RX.Post;
      Titulos.Next;
  end;
  PF.Aguarde(False);
end;

procedure TFRelRetorno.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFRelRetorno.GridMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if X in [11..37] then
    Screen.Cursor:=crHandPoint
      else Screen.Cursor:=crDefault;
end;

procedure TFRelRetorno.sBitBtn1Click(Sender: TObject);
begin
  if RX.IsEmpty then Exit;

  Application.CreateForm(TFQuickRetorno1,FQuickRetorno1);
  with FQuickRetorno1 do
  begin
      RX.DisableControls;
      RX.Filtered:=False;
      RX.Filtered:=True;
      RX.SortOnFields('PROTOCOLO');
      lbData.Caption      :=dm.ServentiaCIDADE.AsString+FormatDateTime(', dd "de" mmmm "de" yyyy.',Now);
      lbCartorio.Caption  :='De: '+dm.ServentiaDESCRICAO.AsString;
      lbBanco.Caption     :='Ao: '+dm.PortadoresNOME.AsString;
      lbArquivo.Caption   :='Arquivo: '+RXRetorno.AsString;
      lbTotal.Caption     :='Total: R$ '+FloatToStrF(Total,ffNumber,7,2);

      PF.Aguarde(False);
      if RX.IsEmpty then
        GR.Aviso('Nenhum movimento encontrado.')
          else Relatorio.Preview;

      FQuickRetorno1.Release;
      FQuickRetorno1:=Nil;
      RX.Filtered:=False;
      RX.EnableControls;
  end;
end;

procedure TFRelRetorno.RXFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept:=RXCheck.AsBoolean;
end;

procedure TFRelRetorno.GridMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Screen.Cursor=crHandPoint then
  begin
      RX.Edit;
      RXCheck.AsBoolean:=not RXCheck.AsBoolean;
      RX.Post;
  end;
end;

procedure TFRelRetorno.Marcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  if RX.IsEmpty then Exit;

  Posicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  while not RX.Eof do
  begin
      RX.Edit;
      RXCheck.AsBoolean:=True;
      RX.Post;
      RX.Next;
  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;
end;

procedure TFRelRetorno.Desmarcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  if RX.IsEmpty then Exit;

  Posicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  while not RX.Eof do
  begin
      RX.Edit;
      RXCheck.AsBoolean:=False;
      RX.Post;
      RX.Next;
  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;
end;

end.
