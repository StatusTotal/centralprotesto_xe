unit UPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, Menus, ExtCtrls, sPanel, Buttons, sSpeedButton,
  StdCtrls, sBitBtn, acPNG, ImgList, acAlphaImageList, jpeg, sButton, DB, Grids, StrUtils, ComCtrls, DBCtrls, DBClient,
  SimpleDS, SqlExpr, sCheckBox, RXShell, RXCtrls, IniFiles, ShellApi, sDialogs, FMTBcd,
  sLabel, sDBText, sBevel, AppEvnts, IdFTP, RXSwitch, RxGIF, acImage,
  dxGDIPlusClasses, sSkinProvider, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFPrincipal = class(TForm)
    Menu: TMainMenu;
    Cadastr1: TMenuItem;
    Entrada: TMenuItem;
    sPanel1: TsPanel;
    sbBalcao: TsSpeedButton;
    sbPedido: TsSpeedButton;
    Relatrios1: TMenuItem;
    Configuraes1: TMenuItem;
    Sair1: TMenuItem;
    Consultas1: TMenuItem;
    Completa1: TMenuItem;
    sbCompleta: TsSpeedButton;
    Custas1: TMenuItem;
    Serventia1: TMenuItem;
    MenuGerencial: TMenuItem;
    LimparBancodedados1: TMenuItem;
    Intimao1: TMenuItem;                         
    Portadores1: TMenuItem;
    Importar1: TMenuItem;
    Protocolo1: TMenuItem;
    Importados1: TMenuItem;
    Escreventes1: TMenuItem;
    Baixas1: TMenuItem;
    sbBaixas: TsSpeedButton;
    Link1: TMenuItem;
    XML1: TMenuItem;
    Edital1: TMenuItem;
    Feri1: TMenuItem;
    Certides1: TMenuItem;
    Parametros: TMenuItem;
    Exportar: TMenuItem;
    LivrodeProtocolo1: TMenuItem;
    sbProtocolar: TsSpeedButton;
    InstrumentodeProtesto1: TMenuItem;
    sbCertidao: TsSpeedButton;
    OrdemdeProtesto1: TMenuItem;
    sbCancelamento: TsSpeedButton;
    CertidodePagamento1: TMenuItem;
    MotivosEdital1: TMenuItem;
    Distribuidor1: TMenuItem;
    SerasaEqiofax1: TMenuItem;
    AverbaodeCancelamento1: TMenuItem;
    Retorno1: TMenuItem;
    pnVersao: TPanel;
    ndicePessoal1: TMenuItem;
    Excel1: TMenuItem;
    Convnio1: TMenuItem;
    sbImportar: TsSpeedButton;
    RecibodeDesistncia1: TMenuItem;
    MensagensInstrumento1: TMenuItem;
    tulosdodia1: TMenuItem;
    CancelamentoEletrnico1: TMenuItem;
    CancelamentoEletrnico2: TMenuItem;
    Quantidades: TMenuItem;
    Confirmao1: TMenuItem;
    Online1: TMenuItem;
    tulosDistribudos1: TMenuItem;
    Notificao1: TMenuItem;
    AvisodeIrregularidade1: TMenuItem;
    MovimentoDirio1: TMenuItem;
    RelaodeTtulos1: TMenuItem;
    LivroAdicional1: TMenuItem;
    PraasdeProtesto1: TMenuItem;
    Opd: TsOpenDialog;
    Sustao1: TMenuItem;
    Boletos1: TMenuItem;
    SelosUtilizados1: TMenuItem;
    tulosnoenviadoaoCRA1: TMenuItem;
    RelaodeCertides1: TMenuItem;
    lb2: TLabel;
    lb3: TLabel;
    lb6: TLabel;
    lb1: TLabel;
    lb5: TLabel;
    lb7: TLabel;
    lb4: TLabel;
    lb0: TStaticText;
    ImHelp: TImage;
    SERVENTIA: TsDBText;
    TABELIAO: TsDBText;
    lbVersaoSistema: TsLabel;
    lbAtualizacao: TsLabel;
    lbTelefone: TsLabel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sbUpdate: TsSpeedButton;
    Update: TRxSwitch;
    Conferencia: TMenuItem;
    sbSerieAtual: TsSpeedButton;
    sbLink: TsSpeedButton;
    A2: TMenuItem;
    I1: TMenuItem;
    R1: TMenuItem;
    R2: TMenuItem;
    lbAtualizarTabela: TsLabel;
    MenuAtualizarEmolumentos: TMenuItem;
    N1: TMenuItem;
    MenuExportar: TMenuItem;
    ckPainel: TsCheckBox;
    Teste: TRxSwitch;
    Label1: TLabel;
    Label2: TLabel;
    sSkinProvider: TsSkinProvider;
    sbISS: TsSpeedButton;
    ckAssistente: TsCheckBox;
    ServentiasAgregadas1: TMenuItem;
    ImportarArquivoCRA1: TMenuItem;
    ImportarRetornoServentias1: TMenuItem;
    pmImportarArquivo: TPopupMenu;
    pmiImportarArquivoCRA: TMenuItem;
    pmiImportarRetornoServentias: TMenuItem;
    ExportarRemessaServentias1: TMenuItem;
    a1: TMenuItem;
    P1: TsPanel;
    ImLogotipo: TsImage;
    pmExportarArquivo: TPopupMenu;
    pmiExportarArquivoSA: TMenuItem;
    pmiExportarRetornoCRA: TMenuItem;
    sbExportar: TsSpeedButton;
    lbVersaoTabela: TsLabel;
    sbCopiar: TSpeedButton;
    procedure Sair1Click(Sender: TObject);
    procedure EntradaClick(Sender: TObject);
    procedure sbBalcaoClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Custas1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Completa1Click(Sender: TObject);
    procedure LimparBancodedados1Click(Sender: TObject);
    procedure Intimao1Click(Sender: TObject);
    procedure Serventia1Click(Sender: TObject);
    procedure Portadores1Click(Sender: TObject);
    procedure Protocolo1Click(Sender: TObject);
    procedure Importados1Click(Sender: TObject);
    procedure sbCompletaClick(Sender: TObject);
    procedure Escreventes1Click(Sender: TObject);
    procedure Baixas1Click(Sender: TObject);
    procedure sbBaixasClick(Sender: TObject);
    procedure XML1Click(Sender: TObject);
    procedure Edital1Click(Sender: TObject);
    procedure Feri1Click(Sender: TObject);
    procedure sbPedidoClick(Sender: TObject);
    procedure Certides1Click(Sender: TObject);
    procedure ParametrosClick(Sender: TObject);
    procedure LivrodeProtocolo1Click(Sender: TObject);
    procedure sbProtocolarClick(Sender: TObject);
    procedure InstrumentodeProtesto1Click(Sender: TObject);
    procedure sbCertidaoClick(Sender: TObject);
    procedure Serasa1Click(Sender: TObject);
    procedure OrdemdeProtesto1Click(Sender: TObject);
    procedure sbCancelamentoClick(Sender: TObject);
    procedure CertidodePagamento1Click(Sender: TObject);
    procedure InserirID(Id: String; Valor: Integer);
    procedure MotivosEdital1Click(Sender: TObject);
    procedure Distribuidor1Click(Sender: TObject);
    procedure SerasaEqiofax1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AverbaodeCancelamento1Click(Sender: TObject);
    procedure Retorno1Click(Sender: TObject);
    procedure CriarParametro(ID: Integer; Descricao,Valor: String);
    procedure Excel1Click(Sender: TObject);
    procedure ndicePessoal1Click(Sender: TObject);
    procedure Convnio1Click(Sender: TObject);
    procedure DefinirChavePrimaria(TabelaNova,Nome,Chave: String;Conexao:TSQLConnection);
    procedure DefinirChavePrimariaFD(TabelaNova, Nome, Chave: String; Conexao: TFDConnection);
    procedure RecibodeDesistncia1Click(Sender: TObject);
    procedure InicializarID_MSG;
    procedure ckAssistenteClick(Sender: TObject);
    procedure MensagensInstrumento1Click(Sender: TObject);
    procedure CriarMensagemZero;
    procedure AtualizarPracas;
    procedure tulosdodia1Click(Sender: TObject);
    procedure AtualizarCRA;
    procedure AtualizarCRANull;
    procedure CorrigirID_BALCAO;
    procedure CancelamentoEletrnico1Click(Sender: TObject);
    procedure AtualizarCodigoMunicipio;
    procedure IniciarSequenciaRemessas;
    procedure EletronicoNullNao;
    procedure CancelamentoEletrnico2Click(Sender: TObject);
    procedure QuantidadesClick(Sender: TObject);
    procedure CorrigirTipoProtesto;
    procedure Confirmao1Click(Sender: TObject);
    procedure CorrigirConvenioTangua;
    procedure CorrigirStatusDuasBarras;
    procedure CorrigirProtestadoCarmo;
    procedure Online1Click(Sender: TObject);
    procedure tulosDistribudos1Click(Sender: TObject);
    procedure AlterarARParaApontado;
    procedure Notificao1Click(Sender: TObject);
    procedure IniciarVersao;
    procedure AvisodeIrregularidade1Click(Sender: TObject);
    procedure AdicionarIrregularidade(Codigo: Integer; Descricao: String);
    procedure MovimentoDirio1Click(Sender: TObject);
    procedure CriarIndiceDescending(Indice,NomeTabela,Campos: String);
    procedure RelaodeTtulos1Click(Sender: TObject);
    procedure LivroAdicional1Click(Sender: TObject);
    procedure CorrigirConveniosCampos;
    procedure CorrigirID_Mensagem;
    procedure TornarCampoNaoNulo(Tabela,Campo: String; Banco: TFDConnection);
    procedure PraasdeProtesto1Click(Sender: TObject);
    procedure CriarChaveEstrangeira(Tabela,Chave,Campo,Referencia,Regra: String; Conexao: TSQLConnection);
    procedure CriarChaveEstrangeiraFD(Tabela, Chave, Campo, Referencia, Regra: String; Conexao: TFDConnection);
    procedure AtualizarSaldoTitulo;
    procedure Sustao1Click(Sender: TObject);
    procedure Relatrios1Click(Sender: TObject);
    procedure Boletos1Click(Sender: TObject);
    procedure SelosUtilizados1Click(Sender: TObject);
    procedure tulosnoenviadoaoCRA1Click(Sender: TObject);
    procedure RelaodeCertides1Click(Sender: TObject);
    procedure lb2Click(Sender: TObject);
    procedure lb3Click(Sender: TObject);
    procedure lb6Click(Sender: TObject);
    procedure lb5Click(Sender: TObject);
    procedure lb4Click(Sender: TObject);
    procedure HabilitarHelp;
    procedure DesabilitarHelp;
    procedure InserirTabelas;
    procedure AjustarPosicoesTabelaCOD;
    procedure AjustarPosicoesTabelaTABCUS;
    procedure PreencherDescricaoCustasEInserirNovasTabelas;
    procedure AjustarNovasTabelas;
    procedure CorrigirDescricaoDoisCodigosEUmItem;
    procedure sbImportarMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure sbImportarMouseLeave(Sender: TObject);
    procedure sbBalcaoMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure sbProtocolarMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure sbBaixasMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure sbCompletaMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure sbPedidoMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure sbCertidaoMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure sbCancelamentoMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure sbBalcaoMouseLeave(Sender: TObject);
    procedure sbProtocolarMouseLeave(Sender: TObject);
    procedure sbBaixasMouseLeave(Sender: TObject);
    procedure sbCompletaMouseLeave(Sender: TObject);
    procedure sbPedidoMouseLeave(Sender: TObject);
    procedure sbCertidaoMouseLeave(Sender: TObject);
    procedure sbCancelamentoMouseLeave(Sender: TObject);
    procedure sbUpdateClick(Sender: TObject);
    procedure CorrigirDocumentoSacador;
    procedure ConferenciaClick(Sender: TObject);
    procedure AjustarPosicoesCONCUS;
    procedure Atualizacoes2013;
    procedure Atualizacoes2014;
    procedure Atualizacoes2015;
    procedure CorrigirFaixaConvenio;
    procedure AumentarTamanhoSelos;
    procedure MudarTipoCodigoApresentante;
    procedure CorrigirDescricaoTabelasConcus;
    procedure sbSerieAtualClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure sbLinkMouseLeave(Sender: TObject);
    procedure sbLinkMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure sbLinkClick(Sender: TObject);
    procedure A2Click(Sender: TObject);
    procedure SincronizarConvenioComPortador;
    procedure AjustarPosicoesCamposCertidoes;
    procedure TrocarTipoCamposFolhasCertidao;
    procedure lb7Click(Sender: TObject);
    procedure BuscarAleatoriosCancelamentos;
    procedure I1Click(Sender: TObject);
    procedure R1Click(Sender: TObject);
    procedure AumentarDescricaoTipos;
    procedure AjustarTabeladeRemessas;
    procedure R2Click(Sender: TObject);
    procedure PuxarValoresApontamentosCanceladosConvenio;
    procedure lbAtualizarTabelaClick(Sender: TObject);
    procedure AtualizarEmolumentos;
    procedure MenuAtualizarEmolumentosClick(Sender: TObject);
    procedure Configuraes1Click(Sender: TObject);
    procedure MenuExportarClick(Sender: TObject);
    procedure ckPainelClick(Sender: TObject);
    procedure AtualizarSiglasTipos;
    procedure sbISSClick(Sender: TObject);
    procedure ServentiasAgregadas1Click(Sender: TObject);
    procedure ImportarArquivoCRA1Click(Sender: TObject);
    procedure ImportarRetornoServentias1Click(Sender: TObject);
    procedure lb1Click(Sender: TObject);
    procedure sbImportarClick(Sender: TObject);
    procedure pmiImportarArquivoCRAClick(Sender: TObject);
    procedure pmiImportarRetornoServentiasClick(Sender: TObject);
    procedure ExportarRemessaServentias1Click(Sender: TObject);
    procedure a1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure pmiExportarArquivoSAClick(Sender: TObject);
    procedure pmiExportarRetornoCRAClick(Sender: TObject);
    procedure sbExportarClick(Sender: TObject);
    procedure sbExportarMouseLeave(Sender: TObject);
    procedure sbExportarMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure sbCopiarClick(Sender: TObject);
  private
    { Private declarations }
    I: TIniFile;
    TD: TTransactionDesc;
  public
    { Public declarations }
    Versao: String;
    vDestino: String;
  end;

var
  FPrincipal: TFPrincipal;

implementation

uses
  UPF, UDM, UTabela, UConsultaCompleta, URelIntimacao1,
  UServentia, UPortadores, UImportarTxt, UTitulo, UControle, UImportados,
  UEscreventes, UEdital, UFeriados, UCertidao,
  UConsultaCertidao, UParametros, UExportarTxt, ULivroProtocolo,
  UBuscaCertidao, UPedidos, UEntrada, UProtocolar,
  URelInstrumento1, USerasa, URelOrderm, URelPagamento,
  UCadastroMotivos, URelCancelamento, URelRetorno, UIndicePessoal,
  UQuickRetirado1, URelDesistencia,
  UCadastroMensagens, URelTitulos, UEletronico, URelQtds,
  UConfirmacao, UOnline, URelDistribuidos, URelNotificacao,
  UImportarDistribuidor, UAguarde, URelEdital, URelIrregularidade,
  URelDiario, URelMovimento, ULivroAdicional,
  UCadastroPracas, UGeral, URelSustacao, URelBoletos, USelo,
  USelosUtilizados, URelPendentes, URelCertidao, USplash, URelConferencia,
  DateUtils, UGDM, UXML2014, UBaixas2014, USerieAtual,
  UAtualizarSistema, UConsVerbal, UReciboPagamentoConvenio,
  UQuickReciboApontamento, UReciboApontamento, USeguranca,
  UQuantidadeSelo, UAliquotaISS, UServentiaAgregada,
  UImportarTxtRetornoServentias, UExportarTxtRemBalcaoServ,
  URelImportadosExportados;

{$R *.dfm}

procedure TFPrincipal.CriarChaveEstrangeira(Tabela,Chave,Campo,Referencia,Regra: String; Conexao: TSQLConnection);
var
  SQL: String;
  Q: TSQLQuery;
begin
  Q:=TSQLQuery.Create(Nil);
  Q.SQLConnection:=Conexao;
  Q.SQL.Add('select rdb$index_name from rdb$indices where rdb$index_name='''+Chave+'''');
  Q.Open;
  if Q.IsEmpty then
  begin
      Q.Close;
      Q.SQL.Clear;
      SQL:='ALTER TABLE '+Tabela+' ADD CONSTRAINT '+Chave+' FOREIGN KEY ('+Campo+') REFERENCES '+Referencia+
                        ' ON DELETE '+Regra+' ON UPDATE '+Regra;
      Q.SQL.Add(SQL);
      Q.ExecSQL();
  end;
  Q.Close;
  Q.Free;
end;

procedure TFPrincipal.CriarChaveEstrangeiraFD(Tabela, Chave, Campo, Referencia,
  Regra: String; Conexao: TFDConnection);
var
  SQL: String;
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection := Conexao;

  Q.SQL.Add('select rdb$index_name from rdb$indices where rdb$index_name='''+Chave+'''');
  Q.Open;

  if Q.IsEmpty then
  begin
      Q.Close;
      Q.SQL.Clear;
      SQL:='ALTER TABLE '+Tabela+' ADD CONSTRAINT '+Chave+' FOREIGN KEY ('+Campo+') REFERENCES '+Referencia+
                        ' ON DELETE '+Regra+' ON UPDATE '+Regra;
      Q.SQL.Add(SQL);
      Q.ExecSQL();
  end;
  Q.Close;
  Q.Free;
end;

procedure TFPrincipal.CorrigirConveniosCampos;
var
  Q: TFDQuery;
  Serventia: Integer;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT CODIGO FROM SERVENTIA');
  Q.Open;
  Serventia:=Q.FieldByName('CODIGO').AsInteger;
  if Serventia=1500 then
  begin
      PF.Aguarde(True,'Aguarde: 1 de 32');
      Q.Close;
      Q.SQL.Clear;
      Q.SQL.Add('UPDATE PORTADORES SET CONVENIO='''+'S'+'''WHERE CODIGO=:CODIGO');
      Q.ParamByName('CODIGO').AsInteger:=71;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 2 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=85;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 3 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=91;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 4 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=94;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 5 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=138;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 6 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=701;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 7 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=738;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 8 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=863;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 9 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=895;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 10 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=901;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 11 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=936;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 12 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=962;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 13 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=976;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 14 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=980;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 15 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=982;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 16 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=990;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 17 de 32');
      Q.Close;
      Q.SQL.Clear;
      Q.SQL.Add('UPDATE TITULOS SET CONVENIO='''+'S'+'''WHERE CODIGO_APRESENTANTE=:CODIGO');
      Q.ParamByName('CODIGO').AsInteger:=71;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 18 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=85;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 19 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=91;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 20 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=94;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 21 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=138;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 22 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=701;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 23 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=738;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 24 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=863;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 25 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=895;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 26 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=901;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 27 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=936;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 28 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=962;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 29 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=976;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 30 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=980;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 31 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=982;
      Q.ExecSQL();

      PF.Aguarde(True,'Aguarde: 32 de 32');
      Q.Close;
      Q.ParamByName('CODIGO').AsInteger:=990;
      Q.ExecSQL();
  end;
  Q.Close;
  Q.Free;
end;

procedure TFPrincipal.CorrigirConvenioTangua;
var
  Q: TFDQuery;
  Serventia: Integer;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT CODIGO FROM SERVENTIA');
  Q.Open;
  Serventia:=Q.FieldByName('CODIGO').AsInteger;
  if Serventia=2652 then
  begin
      Q.Close;
      Q.SQL.Clear;
      Q.SQL.Add('UPDATE TITULOS SET CONVENIO='''+'S'+'''WHERE CODIGO_APRESENTANTE=901');
      Q.ExecSQL();
  end;
  Q.Close;
  Q.Free;
end;

procedure TFPrincipal.IniciarSequenciaRemessas;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('UPDATE PORTADORES SET SEQUENCIA='''+'100000'+'''WHERE SEQUENCIA IS NULL');
  Q.ExecSQL();
  Q.Close;
  Q.Free;
end;

procedure TFPrincipal.InserirID(Id: String; Valor: Integer);
begin
  dm.Ids.Close;
  dm.Ids.Params[0].Value:=Id;
  dm.Ids.Open;
  if dm.Ids.IsEmpty then
  begin
      dm.Ids.Append;
      dm.IdsCAMPO.Value:=Id;
      dm.IdsVALOR.Value:=Valor;
      dm.Ids.Post;
      dm.Ids.ApplyUpdates(0);
  end;
  dm.Ids.Close;
end;

procedure TFPrincipal.Sair1Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TFPrincipal.EntradaClick(Sender: TObject);
begin
  dm.Tipos.Close;
  dm.Tipos.Open;

  {QUALQUER ALTERA��O AQUI ATUALIZAR IGUAL NO BOT�O SALVAR DA TELA DE ENTRADAS}
  dm.Titulos.Close;
  dm.Titulos.Params[0].AsInteger      :=-1;
  dm.Titulos.Open;
  dm.Titulos.Append;
  dm.TitulosCOBRANCA.AsString         :='CC';
  dm.TitulosDT_ENTRADA.AsDateTime     :=Now;
  dm.TitulosCONVENIO.AsString         :='N';
  dm.TitulosTIPO_APRESENTACAO.AsString:='B';
  dm.TitulosCPF_ESCREVENTE.AsString   :=dm.vCPF;
  dm.TitulosRECIBO.AsInteger          :=StrToInt(dm.ValorParametro(11));
  dm.TitulosTIPO_PROTESTO.AsInteger   :=1;
  dm.TitulosACEITE.AsString           :='N';
  dm.TitulosFINS_FALIMENTARES.AsString:='N';
  dm.TitulosAVALISTA_DEVEDOR.AsString :='N';
  dm.TitulosVALOR_AR.AsFloat          :=0;
  dm.TitulosVALOR_TITULO.AsFloat      :=0;
  dm.TitulosTOTAL.AsFloat             :=0;
  dm.TitulosSALDO_PROTESTO.AsFloat    :=0;
  dm.TitulosPRACA_PROTESTO.AsString   :=dm.ValorParametro(6);
  if dm.ValorParametro(35)<>'' then
  dm.TitulosVALOR_AR.AsFloat          :=StrToFloat(dm.ValorParametro(35));

  dm.RxPortador.Close;
  dm.RxPortador.Open;
  dm.RxPortador.Append;
  dm.RxPortadorTIPO.AsString               :='F';
  dm.RxPortadorCONVENIO.AsString           :='N';
  dm.RxPortadorBANCO.AsString              :='N';
  dm.RxPortadorFLG_PAGAANTECIPADO.AsString :='N';

  dm.RxDevedor.Close;
  dm.RxDevedor.Open;

  dm.RxSacador.Close;
  dm.RxSacador.Open;
  dm.RxSacador.Append;
  dm.RxSacadorTIPO.AsString           :='F';

  dm.RxCustas.Close;
  dm.RxCustas.Open;

  GR.CriarForm(TFEntrada,FEntrada);
  dm.Tipos.Close;
end;

procedure TFPrincipal.sbBalcaoClick(Sender: TObject);
begin
  EntradaClick(Sender);
end;

procedure TFPrincipal.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Application.Terminate;
end;

procedure TFPrincipal.FormResize(Sender: TObject);
begin
  P1.Left := (ClientWidth - P1.Width) div 2;
  P1.Top  := (ClientHeight - (P1.Height + sPanel1.Height)) div 2;
end;

procedure TFPrincipal.Custas1Click(Sender: TObject);
begin
  GR.CriarForm(TFTabela,FTabela);
  dm.Codigos.Close;
end;

procedure TFPrincipal.DefinirChavePrimaria(TabelaNova,Nome,Chave: String;Conexao:TSQLConnection);
var
  SQL: String;
  Q: TSQLQuery;
begin
  try
    Q:=TSQLQuery.Create(Nil);
    Q.SQLConnection:=Conexao;

    SQL:='SELECT RC.RDB$CONSTRAINT_NAME AS NOME_PK FROM RDB$RELATION_CONSTRAINTS RC, '+
         'RDB$INDEX_SEGMENTS IDX_S, RDB$INDICES IDX WHERE (IDX_S.RDB$INDEX_NAME = RC.RDB$INDEX_NAME) '+
         'AND (IDX.RDB$INDEX_NAME='+QuotedStr(Nome)+') AND (RC.RDB$CONSTRAINT_TYPE='''+'PRIMARY KEY'+''') '+
         'AND (RC.RDB$RELATION_NAME='+QuotedStr(TabelaNova)+')';

    Q.SQL.Add(SQL);
    Q.Open;
    if Q.IsEmpty then
    begin
        Q.Close;
        Q.SQL.Clear;
        SQL:='ALTER TABLE '+TabelaNova+' ADD CONSTRAINT '+Nome+' PRIMARY KEY '+Chave;
        Q.SQL.Add(SQL);
        Q.ExecSQL();
    end;
  except
  end;
  Q.Close;
  Q.Free;
end;

procedure TFPrincipal.DefinirChavePrimariaFD(TabelaNova, Nome, Chave: String;
  Conexao: TFDConnection);
var
  SQL: String;
  Q: TFDQuery;
begin
  try
    Q:=TFDQuery.Create(Nil);
    Q.Connection:=Conexao;

    SQL:='SELECT RC.RDB$CONSTRAINT_NAME AS NOME_PK FROM RDB$RELATION_CONSTRAINTS RC, '+
         'RDB$INDEX_SEGMENTS IDX_S, RDB$INDICES IDX WHERE (IDX_S.RDB$INDEX_NAME = RC.RDB$INDEX_NAME) '+
         'AND (IDX.RDB$INDEX_NAME='+QuotedStr(Nome)+') AND (RC.RDB$CONSTRAINT_TYPE='''+'PRIMARY KEY'+''') '+
         'AND (RC.RDB$RELATION_NAME='+QuotedStr(TabelaNova)+')';

    Q.SQL.Add(SQL);
    Q.Open;
    if Q.IsEmpty then
    begin
        Q.Close;
        Q.SQL.Clear;
        SQL:='ALTER TABLE '+TabelaNova+' ADD CONSTRAINT '+Nome+' PRIMARY KEY '+Chave;
        Q.SQL.Add(SQL);
        Q.ExecSQL();
    end;
  except
  end;
  Q.Close;
  Q.Free;
end;

procedure TFPrincipal.Completa1Click(Sender: TObject);
begin
  PF.Aguarde(True);
  GR.CriarForm(TFConsultaCompleta,FConsultaCompleta);
end;

procedure TFPrincipal.LimparBancodedados1Click(Sender: TObject);
begin
  if PF.PasswordInputBox('Seguran�a','Senha')<>FormatDateTime('HHMM',Now) then
  begin
    GR.Aviso('SENHA INV�LIDA!');
    Exit;
  end;

  if dm.conSISTEMA.Connected then
    dm.conSISTEMA.StartTransaction;

  PF.Aguarde(True);
  PF.ExcluirFD('CUSTAS',dm.conSISTEMA);
  PF.ExcluirFD('TITULOS',dm.conSISTEMA);
  PF.ExcluirFD('CERTIDOES',dm.conSISTEMA);
  PF.ExcluirFD('IMPORTADOS',dm.conSISTEMA);
  PF.ExcluirFD('REMESSAS',dm.conSISTEMA);
  PF.ExcluirFD('ONLINE',dm.conSISTEMA);

  PF.Aguarde(False);
  if GR.Pergunta('Limpar Portadores') then
  begin
      PF.Aguarde(True);
      PF.ExcluirFD('PORTADORES',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE IDS SET VALOR=1 WHERE CAMPO='+QuotedStr('ID_PORTADOR'),dm.conSISTEMA);
  end;

  PF.Aguarde(False);
  if GR.Pergunta('Limpar Sacadores') then
  begin
      PF.Aguarde(True);
      PF.ExcluirFD('SACADORES',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE IDS SET VALOR=1 WHERE CAMPO='+QuotedStr('ID_SACADOR'),dm.conSISTEMA);
  end;

  if GR.Pergunta('Limpar Escreventes') then
  begin
      PF.Aguarde(True);
      PF.ExcluirFD('ESCREVENTES',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE IDS SET VALOR=1 WHERE CAMPO='+QuotedStr('ID_ESCREVENTE'),dm.conSISTEMA);
  end;

  GR.ExecutarSQLDAC('UPDATE IDS SET VALOR=1 WHERE CAMPO='+QuotedStr('ID_ATO')+
                   ' OR CAMPO='+QuotedStr('ID_BOLETO')+
                   ' OR CAMPO='+QuotedStr('ID_CUSTA')+
                   ' OR CAMPO='+QuotedStr('ID_DEVEDOR')+
                   ' OR CAMPO='+QuotedStr('ID_IMPORTADO')+
                   ' OR CAMPO='+QuotedStr('ID_MOVIMENTO')+
                   ' OR CAMPO='+QuotedStr('ID_ONLINE')+
                   ' OR CAMPO='+QuotedStr('ID_REMESSA')+
                   ' OR CAMPO='+QuotedStr('ID_TRANSACAO'),dm.conSISTEMA);

  if dm.conSISTEMA.InTransaction then
    dm.conSISTEMA.Commit;

  PF.Aguarde(False);
  GR.Aviso('Limpeza efetuada com sucesso.');
end;

procedure TFPrincipal.Intimao1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelIntimacao1,FRelIntimacao1);
end;

procedure TFPrincipal.Serventia1Click(Sender: TObject);
begin
  dm.Serventia.Edit;
  GR.CriarForm(TFServentia,FServentia);
end;

procedure TFPrincipal.Portadores1Click(Sender: TObject);
begin
  GR.CriarForm(TFPortadores,FPortadores);
end;

procedure TFPrincipal.Protocolo1Click(Sender: TObject);
begin
  GR.CriarForm(TFControle,FControle);
end;

procedure TFPrincipal.Importados1Click(Sender: TObject);
begin
  GR.CriarForm(TFImportados, FImportados);
end;

procedure TFPrincipal.sbCompletaClick(Sender: TObject);
begin
  Completa1Click(Sender);
end;

procedure TFPrincipal.Escreventes1Click(Sender: TObject);
begin
  GR.CriarForm(TFEscreventes,FEscreventes);
end;

procedure TFPrincipal.Baixas1Click(Sender: TObject);
begin
  GR.CriarForm(TFBaixas2014,FBaixas2014)
end;

procedure TFPrincipal.sbBaixasClick(Sender: TObject);
begin
  Baixas1Click(Sender);
end;

procedure TFPrincipal.XML1Click(Sender: TObject);
begin
  GR.CriarForm(TFXML2014,FXML2014);
end;

procedure TFPrincipal.Edital1Click(Sender: TObject);
begin
  if dm.ValorParametro(50)='Lista' then
    GR.CriarForm(TFEdital,FEdital)
      else GR.CriarForm(TFRelEdital,FRelEdital);
end;

procedure TFPrincipal.Feri1Click(Sender: TObject);
begin
  GR.CriarForm(TFFeriados,FFeriados);
end;

procedure TFPrincipal.sbPedidoClick(Sender: TObject);
begin
  GR.CriarForm(TFBuscaCertidao,FBuscaCertidao);
end;

procedure TFPrincipal.Certides1Click(Sender: TObject);
begin
  GR.CriarForm(TFConsultaCertidao,FConsultaCertidao);
end;

procedure TFPrincipal.ParametrosClick(Sender: TObject);
begin
  GR.CriarForm(TFParametros,FParametros);

  if dm.ValorParametro(100) = 'S' then  //Geracao de arquivo automatica
  begin
    ExportarRemessaServentias1.Caption := 'Remessa (Balc�o) para Serv. Agregadas';
    pmiExportarArquivoSA.Caption       := 'Remessa (Balc�o) para Serv. Agregadas';
  end
  else  //Geracao de arquivo manual
  begin
    ExportarRemessaServentias1.Caption := 'Remessa para Serventias Agregadas';
    pmiExportarArquivoSA.Caption       := 'Remessa para Serventias Agregadas';
  end;
end;

procedure TFPrincipal.LivrodeProtocolo1Click(Sender: TObject);
begin
  dm.vLivroConvenio:=False;
  GR.CriarForm(TFLivroProtocolo,FLivroProtocolo);
end;

procedure TFPrincipal.sbProtocolarClick(Sender: TObject);
begin
  GR.CriarForm(TFProtocolar,FProtocolar);
end;

procedure TFPrincipal.InstrumentodeProtesto1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelInstrumento,FRelInstrumento);
end;

procedure TFPrincipal.sbCertidaoClick(Sender: TObject);
begin
  dm.vCancelamento:=False;
  GR.CriarForm(TFPedidos,FPedidos);
end;

procedure TFPrincipal.Serasa1Click(Sender: TObject);
begin
  GR.CriarForm(TFSerasa,FSerasa);
end;

procedure TFPrincipal.OrdemdeProtesto1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelOrdem,FRelOrdem);
end;

procedure TFPrincipal.sbCancelamentoClick(Sender: TObject);
begin
  dm.vCancelamento:=True;
  GR.CriarForm(TFPedidos,FPedidos);
end;

procedure TFPrincipal.CertidodePagamento1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelPagamento,FRelPagamento);
end;

procedure TFPrincipal.MotivosEdital1Click(Sender: TObject);
begin
  GR.CriarForm(TFCadastroMotivos,FCadastroMotivos);
end;

procedure TFPrincipal.Distribuidor1Click(Sender: TObject);
begin
  GR.CriarForm(TFExportarTxt, FExportarTxt);
end;

procedure TFPrincipal.SerasaEqiofax1Click(Sender: TObject);
begin
  PF.Aguarde(True);
  GR.CriarForm(TFSerasa,FSerasa);
end;

procedure TFPrincipal.FormShow(Sender: TObject);
begin
  lbVersaoTabela.Caption  := 'Vers�o Tabela: ' + dm.vVersaoTabela;
  lbVersaoSistema.Caption := 'Vers�o Sistema: ' + ReplaceStr(dm.vVersao, 'Vers�o ', '');
  lbAtualizacao.Caption   := 'Atualiza��o Sistema: ' + dm.vAtualizacao + 'h';

  MenuGerencial.Visible   := dm.vNome = 'TOTAL';

  IniciarVersao;

 // GR.CopiarTeste(Teste.StateOn,'CentralProtesto.exe','Protesto',Copy(dm.vVersao,8,10),Copy(dm.vAtualizacao,14,20));
  //GR.CopiarUpdate(Update.StateOn,'CentralProtesto.exe','Protesto',Copy(dm.vVersao,8,10),Copy(dm.vAtualizacao,14,20));

  Self.WindowState:=wsMaximized;
  lbAtualizarTabela.Visible:=StrToInt(dm.ValorParametro(77))<2021;

  {FOLHA DE SEGURAN�A - Pedro - 28/03/2016}
  dm.vFlTop    := dm.ValorParametro(91);
  dm.vFlBottom := dm.ValorParametro(92);
  dm.vFlLeft   := dm.ValorParametro(93);
  dm.vFlRight  := dm.ValorParametro(94);

  if dm.ValorParametro(95) = '0' then
     dm.vFlFolha  := 'A4'
     else dm.vFlFolha  := 'Legal';

  if dm.ValorParametro(96) = '0' then
     dm.vFlTiracabecalho := 'S'
     else dm.vFlTiracabecalho := 'N';
  {FIM}

  if dm.ValorParametro(100) = 'S' then  //Geracao de arquivo automatica
  begin
    ExportarRemessaServentias1.Caption := 'Remessa (Balc�o) para Serv. Agregadas';
    pmiExportarArquivoSA.Caption       := 'Remessa (Balc�o) para Serv. Agregadas';
  end
  else  //Geracao de arquivo manual
  begin
    ExportarRemessaServentias1.Caption := 'Remessa para Serventias Agregadas';
    pmiExportarArquivoSA.Caption       := 'Remessa para Serventias Agregadas';
  end;

  //ckPainel.Checked:=dm.ValorParametro(87)='S';

  { Desabilitar Campos - 16-06-2017 (Cristina) }
  //lbAtualizarTabela.Visible := False;

  lb4.Visible := False;
  lb5.Visible := False;
  lb6.Visible := False;
  lb7.Visible := False;

  sbISS.Visible := False;

  //Atalho
  sbBaixas.Visible       := False;
  sbPedido.Visible       := False;
  sbCertidao.Visible     := False;
  sbCancelamento.Visible := False;
  sbLink.Visible         := False;
  sbSerieAtual.Visible   := False;
  sbUpdate.Visible       := False;

  //Protesto
  Baixas1.Visible                := False;
  SerasaEqiofax1.Visible         := False;
  CancelamentoEletrnico1.Visible := False;
  Confirmao1.Visible             := False;
  LivroAdicional1.Visible        := False;
  CancelamentoEletrnico2.Visible := False;
  I1.Visible                     := False;

  //Consultas
  Certides1.Visible := False;
  Boletos1.Visible  := False;

  //Relatorios
  Intimao1.Visible               := False;
  Edital1.Visible                := False;
  LivrodeProtocolo1.Visible      := False;
  Convnio1.Visible               := False;
  InstrumentodeProtesto1.Visible := False;
  CertidodePagamento1.Visible    := False;
  CertidodePagamento1.Visible    := False;
  AverbaodeCancelamento1.Visible := False;
  R2.Visible                     := False;
  R1.Visible                     := False;
  RecibodeDesistncia1.Visible    := False;
  tulosdodia1.Visible            := False;
  MovimentoDirio1.Visible        := False;
  RelaodeCertides1.Visible       := False;
  SelosUtilizados1.Visible       := False;

  //Links
  Link1.Visible := False;

  //Configuracoes
  //Custas1.Visible                  := False;
  MenuExportar.Visible             := False;
  MenuAtualizarEmolumentos.Visible := False;

  //Suporte
  MenuGerencial.Visible := False;
end;

procedure TFPrincipal.AverbaodeCancelamento1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelCancelamento,FRelCancelamento);
end;

procedure TFPrincipal.Retorno1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelRetorno,FRelRetorno);
end;

procedure TFPrincipal.CriarParametro(ID: Integer; Descricao, Valor: String);
begin
  dm.Parametros.Close;
  dm.Parametros.Params[0].AsInteger:=ID;
  dm.Parametros.Open;
  if dm.Parametros.IsEmpty then
  begin
      dm.Parametros.Append;
      dm.ParametrosID_PARAMETRO.AsInteger :=ID;
      dm.ParametrosDESCRICAO.AsString     :=Descricao;
      dm.ParametrosVALOR.AsString         :=Valor;
      dm.Parametros.Post;
      dm.Parametros.ApplyUpdates(0);
  end;
end;

procedure TFPrincipal.Excel1Click(Sender: TObject);
var
  Q: TFDQuery;
begin
  PF.Aguarde(True,'Exportando...');
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT * FROM TITULOS ORDER BY PROTOCOLO');
  Q.Open;
  PF.ExportarParaExcel(Q,ExtractFilePath(Application.ExeName),'Titulos.csv');
  PF.Aguarde(False);
  Q.Close;
  Q.Free;
end;

procedure TFPrincipal.ndicePessoal1Click(Sender: TObject);
begin
  GR.CriarForm(TFIndicePessoal,FIndicePessoal);
end;

procedure TFPrincipal.Convnio1Click(Sender: TObject);
begin
  dm.vLivroConvenio:=True;
  GR.CriarForm(TFLivroProtocolo,FLivroProtocolo);
end;

procedure TFPrincipal.RecibodeDesistncia1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelDesistencia,FRelDesistencia);
end;

procedure TFPrincipal.InicializarID_MSG;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('UPDATE TITULOS SET ID_MSG=:N WHERE ID_MSG IS NULL');
  Q.ParamByName('N').AsInteger:=0;
  Q.ExecSQL();
  Q.Close;
  Q.Free;
end;

procedure TFPrincipal.ckAssistenteClick(Sender: TObject);
begin
  if ckAssistente.Checked  then
  begin
      HabilitarHelp;
      dm.AtualizarParametro(49,'S');
  end
  else
  begin
      DesabilitarHelp;
      dm.AtualizarParametro(49,'N');
  end;
end;

procedure TFPrincipal.MensagensInstrumento1Click(Sender: TObject);
begin
  GR.CriarForm(TFCadastroMensagens,FCadastroMensagens);
end;

procedure TFPrincipal.CriarMensagemZero;
begin
  dm.Mensagens.Close;
  dm.Mensagens.Open;
  if dm.Mensagens.IsEmpty then
  begin
      dm.Mensagens.Append;
      dm.MensagensID_MSG.AsInteger:=0;
      dm.MensagensMENSAGEM.AsString:='';
      dm.Mensagens.Post;
      dm.Mensagens.ApplyUpdates(0);
  end;
end;

procedure TFPrincipal.AtualizarPracas;
begin
  GR.ExecutarSQLDAC('UPDATE SERVENTIA SET PRACA='''+'BJR'+''' WHERE CODIGO=1166',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE SERVENTIA SET PRACA='''+'VAS'+''' WHERE CODIGO=1430',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE SERVENTIA SET PRACA='''+'ACB'+''' WHERE CODIGO=2353',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE SERVENTIA SET PRACA='''+'SPA'+''' WHERE CODIGO=1866',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE SERVENTIA SET PRACA='''+'TANG'+'''WHERE CODIGO=2652',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE SERVENTIA SET PRACA='''+'SZJ'+''' WHERE CODIGO=2366',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE SERVENTIA SET PRACA='''+'PCA'+''' WHERE CODIGO=1329',dm.conSISTEMA);

  GR.ExecutarSQLDAC('UPDATE SERVENTIA SET CODIGO_PRACA='''+'3300506'+'''WHERE CODIGO=1166',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE SERVENTIA SET CODIGO_PRACA='''+'3306206'+'''WHERE CODIGO=1430',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE SERVENTIA SET CODIGO_PRACA='''+'3300258'+'''WHERE CODIGO=2353',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE SERVENTIA SET CODIGO_PRACA='''+'3305208'+'''WHERE CODIGO=1866',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE SERVENTIA SET CODIGO_PRACA='''+'3305752'+'''WHERE CODIGO=2652',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE SERVENTIA SET CODIGO_PRACA='''+'3305158'+'''WHERE CODIGO=2366',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE SERVENTIA SET CODIGO_PRACA='''+'0000000'+'''WHERE CODIGO=1329',dm.conSISTEMA);
end;

procedure TFPrincipal.tulosdodia1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelTitulos,FRelTitulos);
end;

procedure TFPrincipal.AtualizarCRA;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('UPDATE PORTADORES SET CRA='''+'S'+'''WHERE CODIGO='''+'237'+'''');
  Q.ExecSQL();

  Q.Close;
  Q.SQL.Clear;
  Q.SQL.Add('UPDATE PORTADORES SET CRA='''+'S'+'''WHERE CODIGO='''+'341'+'''');
  Q.ExecSQL();

  Q.Close;
  Q.SQL.Clear;
  Q.SQL.Add('UPDATE PORTADORES SET CRA='''+'N'+'''WHERE CRA IS NULL');
  Q.ExecSQL();

  Q.Close;
  Q.Free;
end;

procedure TFPrincipal.CorrigirID_BALCAO;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT MAX(ID_BALCAO) AS ID FROM BALCAO');
  Q.Open;
  dm.Ids.Close;
  dm.Ids.Params[0].AsString:='ID_BALCAO';
  dm.Ids.Open;
  dm.Ids.Edit;
  dm.IdsVALOR.AsInteger:=Q.FieldByName('ID').AsInteger+1;
  dm.Ids.Post;
  dm.Ids.ApplyUpdates(0);
  Q.Close;
  Q.Free;
  dm.Ids.Close;
end;

procedure TFPrincipal.AtualizarCRANull;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('UPDATE PORTADORES SET CRA='''+'N'+'''WHERE CRA IS NULL');
  Q.ExecSQL();
  Q.Close;
  Q.Free;
end;

procedure TFPrincipal.CancelamentoEletrnico1Click(Sender: TObject);
begin
  GR.CriarForm(TFEletronico,FEletronico);
end;

procedure TFPrincipal.AtualizarCodigoMunicipio;
var
  Cidade,Codigo: String;
  Q: TFDQuery;
begin
  Q := TFDQuery.Create(Nil);
  Q.Connection := dm.conSISTEMA;

  Q.Close;
  Q.SQL.Clear;
  Q.SQL.Add('SELECT CIDADE FROM SERVENTIA');
  Q.Open;

  Cidade := Q.FieldByName('CIDADE').AsString;

  Q.Close;
  Q.SQL.Clear;
  Q.SQL.Add('SELECT CODIGO ' +
            '  FROM MUNICIPIOS ' +
            ' WHERE UF = ' + QuotedStr('RJ') +
            '   AND CIDADE = :CIDADE');
  Q.ParamByName('CIDADE').AsString := Cidade;
  Q.Open;

  if Q.IsEmpty then
    Codigo := ''
  else
    Codigo := Q.FieldByName('CODIGO').AsString;

  Q.Close;
  Q.SQL.Clear;
  Q.SQL.Add('UPDATE SERVENTIA SET CODIGO_MUNICIPIO=:CODIGO');
  Q.ParamByName('CODIGO').AsString:=Codigo;
  Q.ExecSQL;
  Q.Close;
  Q.Free;
end;

procedure TFPrincipal.CorrigirTipoProtesto;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('UPDATE TITULOS SET TIPO_PROTESTO='''+'1'+'''WHERE TIPO_PROTESTO='''+'25'+'''');
  Q.ExecSQL();
  Q.Close;
  Q.Free;
end;

procedure TFPrincipal.CancelamentoEletrnico2Click(Sender: TObject);
begin
  GR.CriarForm(TFEletronico,FEletronico);
end;

procedure TFPrincipal.EletronicoNullNao;
begin
  dm.qrySuporte.Close;
  dm.qrySuporte.Connection := dm.conSISTEMA;
  dm.qrySuporte.SQL.Clear;
  dm.qrySuporte.SQL.Add('UPDATE TITULOS ' +
                     '   SET ELETRONICO = ' + QuotedStr('N') +
                     ' WHERE ELETRONICO IS NULL');
  dm.qrySuporte.ExecSQL;
  dm.qrySuporte.Close;
end;

procedure TFPrincipal.QuantidadesClick(Sender: TObject);
begin
  GR.CriarForm(TFRelQtds,FRelQtds);
end;

procedure TFPrincipal.Confirmao1Click(Sender: TObject);
begin
  GR.CriarForm(TFConfirmacao,FConfirmacao);
end;

procedure TFPrincipal.CorrigirStatusDuasBarras;
var
  Serventia: Integer;
begin
  dm.qrySuporte.Close;
  dm.qrySuporte.Connection:=dm.conSISTEMA;
  dm.qrySuporte.SQL.Clear;
  dm.qrySuporte.SQL.Add('SELECT CODIGO FROM SERVENTIA');
  dm.qrySuporte.Open;
  Serventia:=dm.qrySuporte.FieldByName('CODIGO').AsInteger;
  if Serventia=1235 then
  begin
      dm.qrySuporte.Close;
      dm.qrySuporte.Connection:=dm.conSISTEMA;
      dm.qrySuporte.SQL.Clear;
      dm.qrySuporte.SQL.Add('UPDATE TITULOS SET STATUS='''+'CANCELADO'+''' WHERE PROTESTADO='''+'S'+''' AND STATUS='''+'PAGO'+'''');
      dm.qrySuporte.ExecSQL();
      dm.qrySuporte.Close;
  end;
end;

procedure TFPrincipal.CorrigirProtestadoCarmo;
var
  Serventia: Integer;
begin
  dm.qrySuporte.Close;
  dm.qrySuporte.Connection:=dm.conSISTEMA;
  dm.qrySuporte.SQL.Clear;
  dm.qrySuporte.SQL.Add('SELECT CODIGO FROM SERVENTIA');
  dm.qrySuporte.Open;
  Serventia:=dm.qrySuporte.FieldByName('CODIGO').AsInteger;
  if Serventia=1208 then
  begin
      dm.qrySuporte.Close;
      dm.qrySuporte.Connection:=dm.conSISTEMA;
      dm.qrySuporte.SQL.Clear;
      dm.qrySuporte.SQL.Add('UPDATE TITULOS SET PROTESTADO='''+'S'+'''WHERE NOT DT_REGISTRO IS NULL');
      dm.qrySuporte.ExecSQL();
      dm.qrySuporte.Close;
      dm.qrySuporte.SQL.Clear;
      dm.qrySuporte.SQL.Add('UPDATE TITULOS SET PROTESTADO='''+'S'+'''WHERE STATUS='''+'PROTESTADO'+'''');
      dm.qrySuporte.ExecSQL();
      dm.qrySuporte.Close;
  end;
end;

procedure TFPrincipal.Online1Click(Sender: TObject);
begin
  GR.CriarForm(TFOnline,FOnline);
end;

procedure TFPrincipal.tulosDistribudos1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelDistribuidos,FRelDistribuidos);
end;

procedure TFPrincipal.AlterarARParaApontado;
begin
  dm.qrySuporte.Close;
  dm.qrySuporte.Connection:=dm.conSISTEMA;
  dm.qrySuporte.SQL.Clear;
  dm.qrySuporte.SQL.Add('UPDATE TITULOS SET STATUS='''+'APONTADO'+'''WHERE STATUS='''+'A.R.'+'''');
  dm.qrySuporte.ExecSQL();
  dm.qrySuporte.Close;
  dm.qrySuporte.SQL.Clear;
  dm.qrySuporte.SQL.Add('UPDATE TITULOS SET STATUS='''+'ENTRADA'+'''WHERE PROTOCOLO IS NULL');
  dm.qrySuporte.ExecSQL();
  dm.qrySuporte.Close;
  dm.qrySuporte.SQL.Clear;
  dm.qrySuporte.SQL.Add('UPDATE MOVIMENTO SET STATUS='''+'APONTADO'+'''WHERE STATUS='''+'A.R.'+'''');
  dm.qrySuporte.ExecSQL();
  dm.qrySuporte.Close;
end;

procedure TFPrincipal.Notificao1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelNotificacao,FRelNotificacao);
end;

procedure TFPrincipal.IniciarVersao;
begin
  dm.qryVersao.Close;
  dm.qryVersao.Open;
  if dm.qryVersao.IsEmpty then
  begin
      dm.qryVersao.Append;
      dm.qryVersaoDATA.AsString:=Copy(dm.vAtualizacao,14,18);
      dm.qryVersao.Post;
      dm.qryVersao.ApplyUpdates(0);
  end;
end;

procedure TFPrincipal.AvisodeIrregularidade1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelIrregularidade,FRelIrregularidade);
end;

procedure TFPrincipal.AdicionarIrregularidade(Codigo: Integer;
  Descricao: String);
var
  Q: TFDQuery;
  Query: String;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT * FROM IRREGULARIDADES WHERE CODIGO='+QuotedStr(IntToStr(Codigo)));
  Q.Open;
  if Q.IsEmpty then
  begin
      Q.SQL.Clear;
      Query:='INSERT INTO IRREGULARIDADES (CODIGO,MOTIVO) VALUES ('+QuotedStr(IntToStr(Codigo))+','+QuotedStr(Descricao)+')';
      Q.SQL.Add(Query);
      Q.ExecSQL();
  end;
  Q.Close;
  Q.Free;
end;

procedure TFPrincipal.MovimentoDirio1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelDiario,FRelDiario);
end;

procedure TFPrincipal.RelaodeTtulos1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelMovimento,FRelMovimento);
end;

procedure TFPrincipal.CriarIndiceDescending(Indice, NomeTabela, Campos: String);
var
  SQL: String;
begin
  with dm do begin
    try
      qrySuporte.Close;
      qrySuporte.SQL.Clear;
      SQL:='select rdb$index_name from rdb$indices where rdb$index_name='''+Indice+'''';
      qrySuporte.SQL.Add(SQL);
      qrySuporte.ExecSQL();
      qrySuporte.Open;
      if qrySuporte.IsEmpty then
      begin
          qrySuporte.Close;
          qrySuporte.SQL.Clear;
          SQL:='CREATE DESCENDING INDEX '+Indice+' ON '+NomeTabela+' '+Campos;
          qrySuporte.SQL.Add(SQL);
          qrySuporte.ExecSQL();
      end;
    except
    end;
  end;
end;

procedure TFPrincipal.LivroAdicional1Click(Sender: TObject);
begin
  GR.CriarForm(TFLivroAdicional,FLivroAdicional);
end;

procedure TFPrincipal.CorrigirID_Mensagem;
begin
  dm.Mensagens.Close;
  dm.Mensagens.Open;
  dm.Mensagens.Last;
  dm.Ids.Close;
  dm.Ids.Params[0].AsString:='ID_MSG';
  dm.Ids.Open;
  dm.Ids.Edit;
  dm.IdsVALOR.AsInteger:=dm.MensagensID_MSG.AsInteger+1;
  dm.Ids.Post;
  dm.Ids.ApplyUpdates(0);
  dm.Mensagens.Close;
  dm.Ids.Close;
end;

procedure TFPrincipal.TornarCampoNaoNulo(Tabela, Campo: String; Banco: TFDConnection);
begin
  dm.qrySuporte.Close;
  dm.qrySuporte.Connection:=Banco;
  dm.qrySuporte.SQL.Clear;
  dm.qrySuporte.SQL.Add('update RDB$RELATION_FIELDS set RDB$NULL_FLAG = 1 where (RDB$FIELD_NAME = '+
                     QuotedStr(Campo)+') and (RDB$RELATION_NAME = '+QuotedStr(Tabela)+')');
  dm.qrySuporte.ExecSQL();
  dm.qrySuporte.Close;
end;

procedure TFPrincipal.PraasdeProtesto1Click(Sender: TObject);
begin
  GR.CriarForm(TFCadastroPracas,FCadastroPracas);
end;

function GetFileSize(FileName: string): int64;
var
  Srec: TSearchRec;
begin
  if FindFirst(FileName, $01+$04+$20, Srec) = 0 then
    Result := Srec.FindData.nFileSizeHigh *MAXDWORD +
    Srec.FindData.nFileSizeLow
  else
    Result := 0;
  findclose(Srec);
end;

procedure TFPrincipal.AtualizarSaldoTitulo;
var
  Q: TFDQuery;
begin
  PF.Aguarde(True,'Atualizando...');
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('UPDATE TITULOS SET SALDO_TITULO=VALOR_TITULO');
  Q.ExecSQL();
  Q.Close;
  Q.Free;
  PF.Aguarde(False);
end;

procedure TFPrincipal.Sustao1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelSustacao,FRelSustacao);
end;

procedure TFPrincipal.Relatrios1Click(Sender: TObject);
begin
  Sustao1.Visible:=dm.ServentiaCODIGO.AsInteger=1208;
  Conferencia.Visible:=dm.ServentiaCODIGO.AsInteger=2358;
end;

procedure TFPrincipal.Boletos1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelBoletos,FRelBoletos);
end;

procedure TFPrincipal.SelosUtilizados1Click(Sender: TObject);
begin
  GR.CriarForm(TFSelosUtilizados,FSelosUtilizados);
end;

procedure TFPrincipal.tulosnoenviadoaoCRA1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelPendentes,FRelPendentes);
end;

procedure TFPrincipal.RelaodeCertides1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelCertidao,FRelCertidao); 
end;

procedure TFPrincipal.lb2Click(Sender: TObject);
begin
  EntradaClick(Sender);
end;

procedure TFPrincipal.lb3Click(Sender: TObject);
begin
  Completa1Click(Sender);
end;

procedure TFPrincipal.lb6Click(Sender: TObject);
begin
  GR.CriarForm(TFBuscaCertidao,FBuscaCertidao);
end;

procedure TFPrincipal.lb5Click(Sender: TObject);
begin
  sbCertidaoClick(Sender);
end;

procedure TFPrincipal.lb4Click(Sender: TObject);
begin
  Baixas1Click(Sender);
end;

procedure TFPrincipal.DesabilitarHelp;
begin
  ImHelp.Visible  :=False;
  lb0.Visible     :=False;
  lb1.Visible     :=False;
  lb2.Visible     :=False;
  lb3.Visible     :=False;
  lb4.Visible     :=False;
  lb5.Visible     :=False;
  lb6.Visible     :=False;
  lb7.Visible     :=False;
end;

procedure TFPrincipal.HabilitarHelp;
begin
  ImHelp.Visible  :=True;
  lb0.Visible     :=True;
  lb1.Visible     :=True;
  lb2.Visible     :=True;
  lb3.Visible     :=True;
  lb4.Visible     :=True;
  lb5.Visible     :=True;
  lb6.Visible     :=True;
  lb7.Visible     :=True;
end;

procedure TFPrincipal.InserirTabelas;
var
  T: String;
  M,A,D,E: Double;
begin
  GR.ExecutarSQLDAC('DELETE FROM COD WHERE COD>=4049 AND COD<=4100',dm.conCENTRAL);

  M:=StrToFloat(dm.ValorParametro(26));
  A:=StrToFloat(dm.ValorParametro(27));
  D:=StrToFloat(dm.ValorParametro(28));
  T:='Protocoliza��o com o subsequente recebimento de pagamento elisivo do protesto, lavratura de protesto de t�tulos '+
     'ou de qualquer outro documento de d�vida, sobre o valor declarado: ';

  dm.Codigos.Close;
  dm.Codigos.Params.ParamByName('OCULTO1').AsString:='T';
  dm.Codigos.Params.ParamByName('OCULTO2').AsString:='T';
  dm.Codigos.Open;
  dm.Codigos.AppendRecord([4, 4049, T+': A - R$ 0,01 - 50,00', 'A - R$ 0,01 - 50,00', 9.22, 0, 0, 0, 0, 0, M, A, D, 0, 0.01, 50.00, 0, 'N', 'N']);
  dm.Codigos.AppendRecord([4,4050,T+': B - R$ 50,01 - 100,00','B - R$ 50,01 - 100,00',18.53,0,0,0,0,0,M,A,D,0,50.01,100.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4051,T+': C - R$ 100,01 - 150,00','C - R$ 100,01 - 150,00',27.76,0,0,0,0,0,M,A,D,0,100.01,150.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4052,T+': D - R$ 150,01 - 200,00','D - R$ 150,01 - 200,00',37.07,0,0,0,0,0,M,A,D,0,150.01,200.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4053,T+': E - R$ 200,01 - 250,00','E - R$ 200,01 - 250,00',46.31,0,0,0,0,0,M,A,D,0,200.01,250.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4054,T+': F - R$ 250,01 - 300,00','F - R$ 250,01 - 300,00',55.55,0,0,0,0,0,M,A,D,0,250.01,300.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4055,T+': G - R$ 300,01 - 350,00','G - R$ 300,01 - 350,00',64.86,0,0,0,0,0,M,A,D,0,300.01,350.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4056,T+': H - R$ 350,01 - 400,00','H - R$ 350,01 - 400,00',74.09,0,0,0,0,0,M,A,D,0,350.01,400.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4057,T+': I - R$ 400,01 - 450,00','I - R$ 400,01 - 450,00',83.32,0,0,0,0,0,M,A,D,0,400.01,450.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4058,T+': J - R$ 450,01 - 500,00','J - R$ 450,01 - 500,00',92.63,0,0,0,0,0,M,A,D,0,450.01,500.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4059,T+': K - R$ 500,01 - 600,00','K - R$ 500,01 - 600,00',111.18,0,0,0,0,0,M,A,D,0,500.01,600.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4060,T+': L - R$ 600,01 - 700,00','L - R$ 600,01 - 700,00',129.72,0,0,0,0,0,M,A,D,0,600.01,700.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4061,T+': M - R$ 700,01 - 800,00','M - R$ 700,01 - 800,00',148.20,0,0,0,0,0,M,A,D,0,700.01,800.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4062,T+': N - R$ 800,01 - 900,00','N - R$ 800,01 - 900,00',166.75,0,0,0,0,0,M,A,D,0,800.01,900.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4063,T+': O - R$ 900,01 - 1.000,00','O - R$ 900,01 - 1.000,00',185.28,0,0,0,0,0,M,A,D,0,900.01,1000.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4064,T+': P - R$ 1.000,01 - 1.500,00','P - R$ 1.000,01 - 1.500,00',208.38,0,0,0,0,0,M,A,D,0,1000.01,1500.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4065,T+': Q - R$ 1.500,01 - 2.000,00','Q - R$ 1.500,01 - 2.000,00',231.47,0,0,0,0,0,M,A,D,0,1500.01,2000.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4066,T+': R - R$ 2.000,01 - 2.500,00','R - R$ 2.000,01 - 2.500,00',254.56,0,0,0,0,0,M,A,D,0,2000.01,2500.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4067,T+': S - R$ 2.500,01 - 3.000,00','S - R$ 2.500,01 - 3.000,00',277.67,0,0,0,0,0,M,A,D,0,2500.01,3000.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4068,T+': T - R$ 3.000,01 - 3.500,00','T - R$ 3.000,01 - 3.500,00',300.77,0,0,0,0,0,M,A,D,0,3000.01,3500.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4069,T+': U - R$ 3.500,01 - 4.000,00','U - R$ 3.500,01 - 4.000,00',323.87,0,0,0,0,0,M,A,D,0,3500.01,4000.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4070,T+': V - R$ 4.000,01 - 4.500,00','V - R$ 4.000,01 - 4.500,00',346.96,0,0,0,0,0,M,A,D,0,4000.01,4500.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4071,T+': W - R$ 4.500,01 - 5.000,00','W - R$ 4.500,01 - 5.000,00',370.05,0,0,0,0,0,M,A,D,0,4500.01,5000.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4072,T+': X - R$ 5.000,01 - 7.500,00','X - R$ 5.000,01 - 7.500,00',393.15,0,0,0,0,0,M,A,D,0,5000.01,7500.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4073,T+': Y - R$ 7.500,01 - 10.000,00','Y - R$ 7.500,01 - 10.000,00',416.24,0,0,0,0,0,M,A,D,0,7500.01,10000.00,0,'N','N']);
  dm.Codigos.AppendRecord([4,4074,T+': Z - Acima de R$ 10.000,01','Z - Acima de R$ 10.000,01',439.34,0,0,0,0,0,M,A,D,0,10000.01,100000000.00,0,'N','N']);
  {CONV�NIO}
  dm.Codigos.AppendRecord([4,4075,T+': A - R$ 0,01 - 50,00','A - R$ 0,01 - 50,00',9.22,0,0,0,0,0,M,A,D,0,0.01,50.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4076,T+': B - R$ 50,01 - 100,00','B - R$ 50,01 - 100,00',18.53,0,0,0,0,0,M,A,D,0,50.01,100.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4077,T+': C - R$ 100,01 - 150,00','C - R$ 100,01 - 150,00',27.76,0,0,0,0,0,M,A,D,0,100.01,150.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4078,T+': D - R$ 150,01 - 200,00','D - R$ 150,01 - 200,00',37.07,0,0,0,0,0,M,A,D,0,150.01,200.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4079,T+': E - R$ 200,01 - 250,00','E - R$ 200,01 - 250,00',46.31,0,0,0,0,0,M,A,D,0,200.01,250.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4080,T+': F - R$ 250,01 - 300,00','F - R$ 250,01 - 300,00',55.55,0,0,0,0,0,M,A,D,0,250.01,300.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4081,T+': G - R$ 300,01 - 350,00','G - R$ 300,01 - 350,00',64.86,0,0,0,0,0,M,A,D,0,300.01,350.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4082,T+': H - R$ 350,01 - 400,00','H - R$ 350,01 - 400,00',74.09,0,0,0,0,0,M,A,D,0,350.01,400.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4083,T+': I - R$ 400,01 - 450,00','I - R$ 400,01 - 450,00',83.32,0,0,0,0,0,M,A,D,0,400.01,450.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4084,T+': J - R$ 450,01 - 500,00','J - R$ 450,01 - 500,00',92.63,0,0,0,0,0,M,A,D,0,450.01,500.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4085,T+': K - R$ 500,01 - 600,00','K - R$ 500,01 - 600,00',111.18,0,0,0,0,0,M,A,D,0,500.01,600.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4086,T+': L - R$ 600,01 - 700,00','L - R$ 600,01 - 700,00',129.72,0,0,0,0,0,M,A,D,0,600.01,700.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4087,T+': M - R$ 700,01 - 800,00','M - R$ 700,01 - 800,00',148.20,0,0,0,0,0,M,A,D,0,700.01,800.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4088,T+': N - R$ 800,01 - 900,00','N - R$ 800,01 - 900,00',166.75,0,0,0,0,0,M,A,D,0,800.01,900.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4089,T+': O - R$ 900,01 - 1.000,00','O - R$ 900,01 - 1.000,00',185.28,0,0,0,0,0,M,A,D,0,900.01,1000.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4090,T+': P - R$ 1.000,01 - 1.500,00','P - R$ 1.000,01 - 1.500,00',208.38,0,0,0,0,0,M,A,D,0,1000.01,1500.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4091,T+': Q - R$ 1.500,01 - 2.000,00','Q - R$ 1.500,01 - 2.000,00',231.47,0,0,0,0,0,M,A,D,0,1500.01,2000.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4092,T+': R - R$ 2.000,01 - 2.500,00','R - R$ 2.000,01 - 2.500,00',254.56,0,0,0,0,0,M,A,D,0,2000.01,2500.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4093,T+': S - R$ 2.500,01 - 3.000,00','S - R$ 2.500,01 - 3.000,00',277.67,0,0,0,0,0,M,A,D,0,2500.01,3000.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4094,T+': T - R$ 3.000,01 - 3.500,00','T - R$ 3.000,01 - 3.500,00',300.77,0,0,0,0,0,M,A,D,0,3000.01,3500.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4095,T+': U - R$ 3.500,01 - 4.000,00','U - R$ 3.500,01 - 4.000,00',323.87,0,0,0,0,0,M,A,D,0,3500.01,4000.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4096,T+': V - R$ 4.000,01 - 4.500,00','V - R$ 4.000,01 - 4.500,00',346.96,0,0,0,0,0,M,A,D,0,4000.01,4500.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4097,T+': W - R$ 4.500,01 - 5.000,00','W - R$ 4.500,01 - 5.000,00',370.05,0,0,0,0,0,M,A,D,0,4500.01,5000.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4098,T+': X - R$ 5.000,01 - 7.500,00','X - R$ 5.000,01 - 7.500,00',393.15,0,0,0,0,0,M,A,D,0,5000.01,7500.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4099,T+': Y - R$ 7.500,01 - 10.000,00','Y - R$ 7.500,01 - 10.000,00',416.24,0,0,0,0,0,M,A,D,0,7500.01,10000.00,0,'S','N']);
  dm.Codigos.AppendRecord([4,4100,T+': Z - Acima de R$ 10.000,01','Z - Acima de R$ 10.000,01',439.34,0,0,0,0,0,M,A,D,0,10000.01,100000000.00,0,'S','N']);
  dm.Codigos.ApplyUpdates(0);

  GR.ExecutarSQLDAC('DELETE FROM TABCUS WHERE COD>=4049 AND COD<=4100',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM TABCUS WHERE COD='''+'4010'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM TABCUS WHERE COD='''+'4011'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM TABCUS WHERE COD='''+'4022'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM TABCUS WHERE COD='''+'4024'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM TABCUS WHERE COD='''+'4025'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM TABCUS WHERE COD='''+'4028'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM TABCUS WHERE COD='''+'4038'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM TABCUS WHERE COD='''+'4040'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM TABCUS WHERE COD='''+'4044'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM TABCUS WHERE COD='''+'4045'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM TABCUS WHERE COD='''+'4047'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM TABCUS WHERE COD='''+'4048'+'''',dm.conCENTRAL);

  GR.ExecutarSQLDAC('UPDATE CONCUS SET VALOR=34.56,VAI='''+'S'+'''WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'2'+'''',dm.conCENTRAL);

  dm.Itens.Close;
  dm.Itens.Open;
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4049,'9','1','A','PROTOCOLIZA��O DE R$ 0,01 AT� 50,00',9.22,1,9.22,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4050,'9','1','B','PROTOCOLIZA��O DE R$ 50,01 AT� 100,00',18.53,1,18.53,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4051,'9','1','C','PROTOCOLIZA��O DE R$ 100,01 AT� 150,00',27.76,1,27.76,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4052,'9','1','D','PROTOCOLIZA��O DE R$ 150,01 AT� 200,00',37.07,1,37.07,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4053,'9','1','E','PROTOCOLIZA��O DE R$ 200,01 AT� 250,00',46.31,1,46.31,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4054,'9','1','F','PROTOCOLIZA��O DE R$ 250,01 AT� 300,00',55.55,1,55.55,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4055,'9','1','G','PROTOCOLIZA��O DE R$ 300,01 AT� 350,00',64.86,1,64.86,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4056,'9','1','H','PROTOCOLIZA��O DE R$ 350,01 AT� 400,00',74.09,1,74.09,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4057,'9','1','I','PROTOCOLIZA��O DE R$ 400,01 AT� 450,00',83.32,1,83.32,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4058,'9','1','J','PROTOCOLIZA��O DE R$ 450,01 AT� 500,00',92.63,1,92.63,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4059,'9','1','K','PROTOCOLIZA��O DE R$ 500,01 AT� 600,00',111.18,1,111.18,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4060,'9','1','L','PROTOCOLIZA��O DE R$ 600,01 AT� 700,00',129.72,1,129.72,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4061,'9','1','M','PROTOCOLIZA��O DE R$ 700,01 AT� 800,00',148.20,1,148.20,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4062,'9','1','N','PROTOCOLIZA��O DE R$ 800,01 AT� 900,00',166.75,1,166.75,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4063,'9','1','O','PROTOCOLIZA��O DE R$ 900,01 AT� 1.000,00',185.28,1,185.28,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4064,'9','1','P','PROTOCOLIZA��O DE R$ 1.000,00 AT� 1.500,00',208.38,1,208.38,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4065,'9','1','Q','PROTOCOLIZA��O DE R$ 1.500,00 AT� 2.000,00',231.47,1,231.47,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4066,'9','1','R','PROTOCOLIZA��O DE R$ 2.000,01 AT� 2.500,00',254.56,1,254.56,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4067,'9','1','S','PROTOCOLIZA��O DE R$ 2.500,01 AT� 3.000,00',277.67,1,277.67,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4068,'9','1','T','PROTOCOLIZA��O DE R$ 3.000,01 AT� 3.500,00',300.77,1,300.77,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4069,'9','1','U','PROTOCOLIZA��O DE R$ 3.500,01 AT� 4.000,00',323.87,1,323.87,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4070,'9','1','V','PROTOCOLIZA��O DE R$ 4.500,01 AT� 4.500,00',346.96,1,346.96,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4071,'9','1','W','PROTOCOLIZA��O DE R$ 4.500,01 AT� 5.000,00',370.05,1,370.05,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4072,'9','1','X','PROTOCOLIZA��O DE R$ 5.000,01 AT� 7.500,00',393.15,1,393.15,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4073,'9','1','Y','PROTOCOLIZA��O DE R$ 7.500,01 AT� 10.000,00',416.24,1,416.24,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4074,'9','1','Z','PROTOCOLIZA��O ACIMA DE R$ 10.000,01',439.34,1,439.34,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4010,'9','1','2','CANCELAMENTO DO REGISTRO DO PROCESSO',34.56,1,34.56,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4011,'9','3','1','CERTID�O FORNECIDA A CADA ENTIDADE REQUERENTE',15.25,1,15.25,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4022,'1','1','*','BUSCAS EM LIVROS OU PAP�IS',0.65,1,0.65,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4022,'1','2','*','EXTRA��O DE CERTID�ES',14.51,1,14.51,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4028,'1','5','*','CONFER�NCIA DE C�PIA OU REPRODU��O, POR P�G.',8.78,1,8.78,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4024,'1','3','*','OPOSI��O DE VISTO',7.25,1,7.25,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4025,'1','3','*','OPOSI��O DE VISTO',7.25,1,7.25,'','']);
  {CONV�NIO}
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4075,'9','1','A','PROTOCOLIZA��O DE R$ 0,01 AT� 50,00',9.22,1,9.22,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4076,'9','1','B','PROTOCOLIZA��O DE R$ 50,01 AT� 100,00',18.53,1,18.53,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4077,'9','1','C','PROTOCOLIZA��O DE R$ 100,01 AT� 150,00',27.76,1,27.76,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4078,'9','1','D','PROTOCOLIZA��O DE R$ 150,01 AT� 200,00',37.07,1,37.07,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4079,'9','1','E','PROTOCOLIZA��O DE R$ 200,01 AT� 250,00',46.31,1,46.31,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4080,'9','1','F','PROTOCOLIZA��O DE R$ 250,01 AT� 300,00',55.55,1,55.55,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4081,'9','1','G','PROTOCOLIZA��O DE R$ 300,01 AT� 350,00',64.86,1,64.86,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4082,'9','1','H','PROTOCOLIZA��O DE R$ 350,01 AT� 400,00',74.09,1,74.09,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4083,'9','1','I','PROTOCOLIZA��O DE R$ 400,01 AT� 450,00',83.32,1,83.32,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4084,'9','1','J','PROTOCOLIZA��O DE R$ 450,01 AT� 500,00',92.63,1,92.63,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4085,'9','1','K','PROTOCOLIZA��O DE R$ 500,01 AT� 600,00',111.18,1,111.18,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4086,'9','1','L','PROTOCOLIZA��O DE R$ 600,01 AT� 700,00',129.72,1,129.72,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4087,'9','1','M','PROTOCOLIZA��O DE R$ 700,01 AT� 800,00',148.20,1,148.20,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4088,'9','1','N','PROTOCOLIZA��O DE R$ 800,01 AT� 900,00',166.75,1,166.75,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4089,'9','1','O','PROTOCOLIZA��O DE R$ 900,01 AT� 1.000,00',185.28,1,185.28,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4090,'9','1','P','PROTOCOLIZA��O DE R$ 1.000,00 AT� 1.500,00',208.38,1,208.38,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4091,'9','1','Q','PROTOCOLIZA��O DE R$ 1.500,00 AT� 2.000,00',231.47,1,231.47,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4092,'9','1','R','PROTOCOLIZA��O DE R$ 2.000,01 AT� 2.500,00',254.56,1,254.56,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4093,'9','1','S','PROTOCOLIZA��O DE R$ 2.500,01 AT� 3.000,00',277.67,1,277.67,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4094,'9','1','T','PROTOCOLIZA��O DE R$ 3.000,01 AT� 3.500,00',300.77,1,300.77,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4095,'9','1','U','PROTOCOLIZA��O DE R$ 3.500,01 AT� 4.000,00',323.87,1,323.87,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4096,'9','1','V','PROTOCOLIZA��O DE R$ 4.500,01 AT� 4.500,00',346.96,1,346.96,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4097,'9','1','W','PROTOCOLIZA��O DE R$ 4.500,01 AT� 5.000,00',370.05,1,370.05,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4098,'9','1','X','PROTOCOLIZA��O DE R$ 5.000,01 AT� 7.500,00',393.15,1,393.15,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4099,'9','1','Y','PROTOCOLIZA��O DE R$ 7.500,01 AT� 10.000,00',416.24,1,416.24,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4100,'9','1','Z','PROTOCOLIZA��O ACIMA DE R$ 10.000,01',439.34,1,439.34,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4038,'9','1','2','CANCELAMENTO DO REGISTRO DO PROCESSO',34.56,1,34.56,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4044,'1','1','*','BUSCAS EM LIVROS OU PAP�IS',0.65,1,0.65,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4044,'1','2','*','EXTRA��O DE CERTID�ES',14.51,1,14.51,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4045,'9','3','1','CERTID�O FORNECIDA A CADA ENTIDADE REQUERENTE',15.25,1,15.25,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4040,'1','5','*','CONFER�NCIA DE C�PIA OU REPRODU��O, POR P�G.',8.78,1,8.78,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4047,'1','3','*','OPOSI��O DE VISTO',7.25,1,7.25,'','']);
  dm.Itens.AppendRecord([GR.ValorGeneratorDAC('ID_CUS',dm.conCENTRAL),4048,'1','3','*','OPOSI��O DE VISTO',7.25,1,7.25,'','']);
  dm.Itens.ApplyUpdates(0);

  dm.Codigos.First;
  while not dm.Codigos.Eof do
  begin
      E:=0;
      dm.Itens.Close;
      dm.Itens.Params[0].AsInteger:=dm.CodigosCOD.AsInteger;
      dm.Itens.Open;
      while not dm.Itens.Eof do
      begin
          E:=E+dm.ItensTOTAL.AsFloat;
          dm.Itens.Next;
      end;

      dm.Codigos.Edit;
      dm.CodigosEMOL.AsFloat    :=E;
      dm.CodigosFETJ.AsFloat    :=GR.NoRound(E*0.2,2);
      dm.CodigosFUND.AsFloat    :=GR.NoRound(E*0.05,2);
      dm.CodigosFUNP.AsFloat    :=GR.NoRound(E*0.05,2);
      dm.CodigosFUNA.AsFloat    :=GR.NoRound(E*0.04,2);

      if (dm.CodigosCOD.AsInteger<>4028) and (dm.CodigosCOD.AsInteger<>4040) and
         (dm.CodigosCOD.AsInteger<>4024) and (dm.CodigosCOD.AsInteger<>4047) and
         (dm.CodigosCOD.AsInteger<>4022) and (dm.CodigosCOD.AsInteger<>4044) and
         (dm.CodigosCOD.AsInteger<>4025) and (dm.CodigosCOD.AsInteger<>4048) then
        dm.CodigosPMCMV.AsFloat:=GR.NoRound(E*0.02,2)
          else dm.CodigosPMCMV.AsFloat:=0;
          
      dm.CodigosTOT.AsFloat     :=dm.CodigosEMOL.AsFloat+
                                  dm.CodigosFETJ.AsFloat+
                                  dm.CodigosFUND.AsFloat+
                                  dm.CodigosFUNP.AsFloat+
                                  dm.CodigosFUNA.AsFloat+
                                  dm.CodigosPMCMV.AsFloat+
                                  dm.CodigosMUTUA.AsFloat+
                                  dm.CodigosACOTERJ.AsFloat+
                                  dm.CodigosDISTRIB.AsFloat;

      dm.Codigos.Post;
      dm.Codigos.ApplyUpdates(0);
      dm.Codigos.Next;
  end;
end;

procedure TFPrincipal.AjustarPosicoesTabelaCOD;
begin
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER ATRIB POSITION 1',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER COD POSITION 2',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER ATOS POSITION 3',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER TITULO POSITION 4',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER EMOL POSITION 5',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER FETJ POSITION 6',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER FUND POSITION 7',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER FUNP POSITION 8',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER FUNA POSITION 9',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER PMCMV POSITION 10',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER MUTUA POSITION 11',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER ACOTERJ POSITION 12',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER DISTRIB POSITION 13',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER TOT POSITION 14',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER MINIMO POSITION 15',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER MAXIMO POSITION 16',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER DIA POSITION 17',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER CONVENIO POSITION 18',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE COD ALTER OCULTO POSITION 19',dm.conCENTRAL);
end;

procedure TFPrincipal.AjustarPosicoesTabelaTABCUS;
begin
  GR.ExecutarSQLDAC('ALTER TABLE TABCUS ALTER ID_CUS POSITION 1', dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE TABCUS ALTER COD POSITION 2', dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE TABCUS ALTER TAB POSITION 3', dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE TABCUS ALTER ITEM POSITION 4', dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE TABCUS ALTER SUB POSITION 5', dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE TABCUS ALTER DESCR POSITION 6', dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE TABCUS ALTER VALOR POSITION 7', dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE TABCUS ALTER QTD POSITION 8', dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE TABCUS ALTER TOTAL POSITION 9', dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE TABCUS ALTER COMP POSITION 10', dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE TABCUS ALTER EXC POSITION 11', dm.conCENTRAL);
end;

procedure TFPrincipal.PreencherDescricaoCustasEInserirNovasTabelas;
var
  vOrdem: Integer;
begin
  dm.Tabela.Close;
  dm.Tabela.Open;
  while not dm.Tabela.Eof do
  begin
      GR.ExecutarSQLDAC('UPDATE CUSTAS SET DESCRICAO='''+dm.TabelaDESCR.AsString+
                        '''WHERE TABELA='''+dm.TabelaTAB.AsString+'''AND ITEM='''+dm.TabelaITEM.AsString+
                        '''AND SUBITEM='''+dm.TabelaSUB.AsString+'''',dm.conSISTEMA);
      dm.Tabela.Next;
  end;

  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'A'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'B'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'C'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'D'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'E'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'F'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'G'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'H'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'I'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'J'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'K'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'L'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'M'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'N'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'O'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'P'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'Q'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'R'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'S'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'T'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'U'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'V'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'W'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'X'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'Y'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'1'+'''AND SUB='''+'Z'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'3'+'''AND SUB='''+'1'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'9'+'''AND ITEM='''+'3'+'''AND SUB='''+'2'+'''',dm.conCENTRAL);

  vOrdem:=1;

  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','A','PROTOCOLIZA��O DE R$ 0,01 AT� 50,00',9.22,'PROTOCOLIZA��O DE R$ 0,01 AT� 50,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','B','PROTOCOLIZA��O DE R$ 50,01 AT� 100,00',18.53,'PROTOCOLIZA��O DE R$ 50,01 AT� 100,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','C','PROTOCOLIZA��O DE R$ 100,01 AT� 150,00',27.76,'PROTOCOLIZA��O DE R$ 100,01 AT� 150,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','D','PROTOCOLIZA��O DE R$ 150,01 AT� 200,00',37.07,'PROTOCOLIZA��O DE R$ 150,01 AT� 200,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','E','PROTOCOLIZA��O DE R$ 200,01 AT� 250,00',46.31,'PROTOCOLIZA��O DE R$ 200,01 AT� 250,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','F','PROTOCOLIZA��O DE R$ 250,01 AT� 300,00',55.55,'PROTOCOLIZA��O DE R$ 250,01 AT� 300,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','G','PROTOCOLIZA��O DE R$ 300,01 AT� 350,00',64.86,'PROTOCOLIZA��O DE R$ 300,01 AT� 350,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','H','PROTOCOLIZA��O DE R$ 350,01 AT� 400,00',74.09,'PROTOCOLIZA��O DE R$ 350,01 AT� 400,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','I','PROTOCOLIZA��O DE R$ 400,01 AT� 450,00',83.32,'PROTOCOLIZA��O DE R$ 400,01 AT� 450,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','J','PROTOCOLIZA��O DE R$ 450,01 AT� 500,00',92.63,'PROTOCOLIZA��O DE R$ 450,01 AT� 500,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','K','PROTOCOLIZA��O DE R$ 500,01 AT� 600,00',111.18,'PROTOCOLIZA��O DE R$ 500,01 AT� 600,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','L','PROTOCOLIZA��O DE R$ 600,01 AT� 700,00',129.72,'PROTOCOLIZA��O DE R$ 600,01 AT� 700,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','M','PROTOCOLIZA��O DE R$ 700,01 AT� 800,00',148.20,'PROTOCOLIZA��O DE R$ 700,01 AT� 800,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','N','PROTOCOLIZA��O DE R$ 800,01 AT� 900,00',166.75,'PROTOCOLIZA��O DE R$ 800,01 AT� 900,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','O','PROTOCOLIZA��O DE R$ 900,01 AT� 1.000,00',185.28,'PROTOCOLIZA��O DE R$ 900,01 AT� 1.000,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','P','PROTOCOLIZA��O DE R$ 1.000,00 AT� 1.500,00',208.38,'PROTOCOLIZA��O DE R$ 1.000,00 AT� 1.500,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','Q','PROTOCOLIZA��O DE R$ 1.500,00 AT� 2.000,00',231.47,'PROTOCOLIZA��O DE R$ 1.500,00 AT� 2.000,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','R','PROTOCOLIZA��O DE R$ 2.000,01 AT� 2.500,00',254.56,'PROTOCOLIZA��O DE R$ 2.000,01 AT� 2.500,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','S','PROTOCOLIZA��O DE R$ 2.500,01 AT� 3.000,00',277.67,'PROTOCOLIZA��O DE R$ 2.500,01 AT� 3.000,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','T','PROTOCOLIZA��O DE R$ 3.000,01 AT� 3.500,00',300.77,'PROTOCOLIZA��O DE R$ 3.000,01 AT� 3.500,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','U','PROTOCOLIZA��O DE R$ 3.500,01 AT� 4.000,00',323.87,'PROTOCOLIZA��O DE R$ 3.500,01 AT� 4.000,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','V','PROTOCOLIZA��O DE R$ 4.500,01 AT� 4.500,00',346.96,'PROTOCOLIZA��O DE R$ 4.500,01 AT� 4.500,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','W','PROTOCOLIZA��O DE R$ 4.500,01 AT� 5.000,00',370.05,'PROTOCOLIZA��O DE R$ 4.500,01 AT� 5.000,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','X','PROTOCOLIZA��O DE R$ 5.000,01 AT� 7.500,00',393.15,'PROTOCOLIZA��O DE R$ 5.000,01 AT� 7.500,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','Y','PROTOCOLIZA��O DE R$ 7.500,01 AT� 10.000,00',416.24,'PROTOCOLIZA��O DE R$ 7.500,01 AT� 10.000,00']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','1','Z','PROTOCOLIZA��O ACIMA DE R$ 10.000,01',439.34,'PROTOCOLIZA��O ACIMA DE R$ 10.000,01']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','3','1','CERTID�O FORNECIDA A CADA ENTIDADE REQUERENTE',15.25,'CERTID�O FORNECIDA A CADA ENTIDADE REQUERENTE']); Inc(vOrdem);
  dm.Tabela.AppendRecord([vOrdem,2013,'S','9','3','2','A CADA NOME E DOCUMENTO RELACIONADO NA CERTID�O',8.33,'A CADA NOME E DOCUMENTO RELACIONADO NA CERTID�O']);
  dm.Tabela.ApplyUpdates(0);
end;

procedure TFPrincipal.AjustarNovasTabelas;
var
  vOrdem: Integer;
begin
  GR.CriarCampoDAC('ANO','CONCUS','INTEGER',dm.conCENTRAL);

  dm.Tabela.Close;
  dm.Tabela.Open;

  GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'N'+'''WHERE TAB='''+'1'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'N'+'''WHERE TAB='''+'2'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'N'+'''WHERE TAB='''+'3'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'N'+'''WHERE TAB='''+'4'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'N'+'''WHERE TAB='''+'5'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'N'+'''WHERE TAB='''+'6'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'N'+'''WHERE TAB='''+'7'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'N'+'''WHERE TAB='''+'8'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'N'+'''WHERE TAB='''+'9'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'N'+'''WHERE TAB='''+'10'+'''',dm.conCENTRAL);

  GR.ExecutarSQLDAC('UPDATE CONCUS SET TAB='''+'24'+'''WHERE TAB='''+'9'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'S'+'''WHERE TAB='''+'24'+'''',dm.conCENTRAL);

  GR.ExecutarSQLDAC('UPDATE TABCUS SET TAB='''+'24'+'''WHERE TAB='''+'9'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE TABCUS SET TAB='''+'16'+'''WHERE TAB='''+'1'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('UPDATE TABCUS SET TAB='''+'19'+'''WHERE TAB='''+'4'+'''',dm.conCENTRAL);

  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'16'+'''AND ITEM='''+'1'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'16'+'''AND ITEM='''+'2'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'16'+'''AND ITEM='''+'3'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'16'+'''AND ITEM='''+'4'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'16'+'''AND ITEM='''+'5'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'16'+'''AND ITEM='''+'6'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);

  vOrdem:=1;

  dm.Tabela.AppendRecord([vOrdem,2013,'S','16','1','*','BUSCA EM LIVROS OU PAP�IS',0.65,'Buscas em livros ou pap�is, qualquer que seja o n�mero de '+
                                                                              'livros ou s�rie de livros nelas compreendidas, ou de pap�is '+
                                                                              'arquivados, relativas a nome ou im�vel, por assunto, cada '+
                                                                              'cinco anos ou fra��o.']); Inc(vOrdem);

  dm.Tabela.AppendRecord([vOrdem,2013,'S','16','2','*','CERTID�ES EXTRA�DAS DE LIVROS',14.51,'Certid�es extra�das de livros, assentamentos ou outros '+
                                                                                   'pap�is arquivados, de atos ou de fatos conhecidos em '+
                                                                                   'raz�o do of�cio, qualquer que seja, al�m da busca, '+
                                                                                   'devendo cada p�gina conter at� 30 (trinta) linhas: por '+
                                                                                   'folha.']); Inc(vOrdem);

  dm.Tabela.AppendRecord([vOrdem,2013,'S','16','3','*','APOSI��O DE VISTO',0,'Aposi��o de visto em certid�o, ou informa��o verbal, solicitada '+
                                                                   'pessoalmente, ou por qualquer outro meio, pelo interessado: valor '+
                                                                   'correspondente a 50% (cinquenta por cento) do valor e de uma certid�o.']); Inc(vOrdem);

  dm.Tabela.AppendRecord([vOrdem,2013,'S','16','4','*','ARQUIVAMENTO/DESARQUIVAMENTO',7.58,'Arquivamento/Desarquivamento de livros, processos ou pap�is.']); Inc(vOrdem);

  dm.Tabela.AppendRecord([vOrdem,2013,'S','16','5','*','EXPEDI��O E EMISS�O DE GUIAS',8.78,'Expedi��o e emiss�o de guias e comunica��es exigidas por '+
                                                                                 'Lei, Atos Normativos, Resolu��es, Portarias e Consolida��o '+
                                                                                 'Normativa.']); Inc(vOrdem);

  dm.Tabela.AppendRecord([vOrdem,2013,'S','16','6','*','NOTIFICA��O OU INTIMA��O, POR PESSOA',12.59,'Notifica��o ou intima��o, por pessoa.']); Inc(vOrdem);

  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'19'+'''AND ITEM='''+'1'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'19'+'''AND ITEM='''+'1'+'''AND SUB='''+'A'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'19'+'''AND ITEM='''+'2'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'19'+'''AND ITEM='''+'3'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'19'+'''AND ITEM='''+'4'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'19'+'''AND ITEM='''+'5'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'19'+'''AND ITEM='''+'6'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'19'+'''AND ITEM='''+'7'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'19'+'''AND ITEM='''+'8'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
  GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE TAB='''+'19'+'''AND ITEM='''+'9'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);


  dm.Tabela.AppendRecord([vOrdem,2013,'S','19','1','*','DISTRIBUI��O, REGISTRO, RETIFICA��O...',14.51,'Distribui��o, registro, retifica��o, averba��o, '+
                                                                                            'exclus�o, inclus�o, na distribui��o de ato notarial, '+
                                                                                            'habilita��o de casamento, t�tulo ou documento.']); Inc(vOrdem);

  dm.Tabela.AppendRecord([vOrdem,2013,'S','19','1','A','POR NOME EXCEDENTE (A PARTIR DO 3� NOME)',0.71,'Por nome excedente (a partir do 3� nome)']); Inc(vOrdem);

  dm.Tabela.AppendRecord([vOrdem,2013,'S','19','2','*','DISTRIBUI��O DE T�TULOS/DOCUMENTOS DE D�VIDA PARA PROTESTO',0,'Distribui��o de t�tulos e '+
                                                                                                            'outros documentos de d�vida '+
                                                                                                            'para protesto: um quinto dos '+
                                                                                                            'emolumentos previstos no item '+
                                                                                                            'n� 1 da tabela n� 24.']); Inc(vOrdem);

  dm.Tabela.AppendRecord([vOrdem,2013,'S','19','3','*','CANCELAMENTO/BAIXA NO REGISTRO DE A��O/FEITO AJUIZADO',14.51,'Cancelamento/baixa no registro de a��o ou feito ajuizado e da '+
                                                                                                           'distribui��o de ato notarial']); Inc(vOrdem);

  dm.Tabela.AppendRecord([vOrdem,2013,'S','19','4','*','CANCELAMENTO/BAIXA NO REGISTRO DE DISTRIBUI��O DE T�TULOS',35.65,'Cancelamento/baixa no registro de distribui��o de t�tulos '+
                                                                                                               'e outros documentos de d�vida para protesto.']); Inc(vOrdem);

  dm.Tabela.AppendRecord([vOrdem,2013,'S','19','5','*','REGISTRO DE DISTRIBUI��O DE NOTIFICA��O RTD',3.56,'Registro de distribui��o de Notifica��o no RTD, inclusive '+
                                                                                                'quando recepcionada por meio eletr�nico']); Inc(vOrdem);

  dm.Tabela.AppendRecord([vOrdem,2013,'S','19','6','*','NOTIFICA��O OU INTIMA��O, POR PESSOA',14.51,'Registro de a��o ou feito ajuizado, inclusive o '+
                                                                                          'do autor, incluindo posterior retifica��o, averba��o, '+
                                                                                          'redistribui��o, exclus�o e inclus�o.']); Inc(vOrdem);

  dm.Tabela.AppendRecord([vOrdem,2013,'S','19','7','*','POR NOME EXCEDENTE (A PARTIR DO 3� NOME)',0.71,'Por nome excedente (a partir do 3� nome)']); Inc(vOrdem);

  dm.Tabela.AppendRecord([vOrdem,2013,'S','19','8','*','CERTID�ES EXTRA�DAS DE LIVROS',29.02,'Certid�es extra�das de livros, assentamentos ou outros '+
                                                                                             'pap�is arquivados, de atos ou de fatos conhecidos em raz�o '+
                                                                                             'do of�cio, qualquer que seja, al�m da busca, devendo cada '+
                                                                                             'p�gina conter at� 30 (trinta) linhas.']); Inc(vOrdem);

  dm.Tabela.AppendRecord([vOrdem,2013,'S','19','9','*','POR FOLHA EXCEDENTE (A PARTIR DA 3� FOLHA)',3.32,'A partir da 3� folha, por folha excedente']);
  dm.Tabela.ApplyUpdates(0);
end;

procedure TFPrincipal.CorrigirDescricaoDoisCodigosEUmItem;
var
  T: String;
begin
  T:='Protocoliza��o com o subsequente recebimento de pagamento elisivo do protesto, lavratura de protesto de t�tulos '+
     'ou de qualquer outro documento de d�vida, sobre o valor declarado: ';

  dm.Codigos.Close;
  dm.Codigos.Params.ParamByName('OCULTO1').AsString:='N';
  dm.Codigos.Params.ParamByName('OCULTO2').AsString:='N';
  dm.Codigos.Open;

  if dm.Codigos.Locate('COD',4050,[]) then
  begin
      dm.Codigos.Edit;
      dm.CodigosATOS.AsString:=T+'B - R$ 50,01 - 100,00';
      dm.Codigos.Post;
      dm.Codigos.ApplyUpdates(0);
  end;

  if dm.Codigos.Locate('COD',4076,[]) then
  begin
      dm.Codigos.Edit;
      dm.CodigosATOS.AsString:=T+'B - R$ 50,01 - 100,00';
      dm.Codigos.Post;
      dm.Codigos.ApplyUpdates(0);
  end;

  dm.Tabela.Close;
  dm.Tabela.Open;
  if dm.Tabela.Locate('TAB;ITEM;SUB',VarArrayOf(['24','1','V']),[]) then
  begin
      dm.Tabela.Edit;
      dm.TabelaDESCR.AsString:='PROTOCOLIZA��O DE R$ 4.000,01 AT� 4.500,00';
      dm.Tabela.Post;
      dm.Tabela.ApplyUpdates(0);
  end;
end;

procedure TFPrincipal.sbImportarMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  sbImportar.Font.Color:=clWhite;
end;

procedure TFPrincipal.sbImportarMouseLeave(Sender: TObject);
begin
  sbImportar.Font.Color:=clBlack;
end;

procedure TFPrincipal.sbBalcaoMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  sbBalcao.Font.Color:=clWhite;
end;

procedure TFPrincipal.sbProtocolarMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  sbProtocolar.Font.Color:=clWhite;
end;

procedure TFPrincipal.sbBaixasMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  sbBaixas.Font.Color:=clWhite;
end;

procedure TFPrincipal.sbCompletaMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  sbCompleta.Font.Color:=clWhite;
end;

procedure TFPrincipal.sbCopiarClick(Sender: TObject);
begin
  GR.CopiarUPX(Application.ExeName,'CENTRAL_PROTESTO',lbVersaoSistema.Caption,lbAtualizacao.Caption);
end;

procedure TFPrincipal.sbExportarClick(Sender: TObject);
var
  pnt: TPoint;
begin
  if GetCursorPos(pnt) then
    pmExportarArquivo.Popup(pnt.X, pnt.Y);
end;

procedure TFPrincipal.sbExportarMouseLeave(Sender: TObject);
begin
  sbExportar.Font.Color:=clBlack;
end;

procedure TFPrincipal.sbExportarMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  sbExportar.Font.Color:=clWhite;
end;

procedure TFPrincipal.sbPedidoMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  sbPedido.Font.Color:=clWhite;
end;

procedure TFPrincipal.sbCertidaoMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  sbCertidao.Font.Color:=clWhite;
end;

procedure TFPrincipal.sbCancelamentoMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  sbCancelamento.Font.Color:=clWhite;
end;

procedure TFPrincipal.sbBalcaoMouseLeave(Sender: TObject);
begin
  sbBalcao.Font.Color:=clBlack;
end;

procedure TFPrincipal.sbProtocolarMouseLeave(Sender: TObject);
begin
  sbProtocolar.Font.Color:=clBlack;
end;

procedure TFPrincipal.sbBaixasMouseLeave(Sender: TObject);
begin
  sbBaixas.Font.Color:=clBlack;
end;

procedure TFPrincipal.sbCompletaMouseLeave(Sender: TObject);
begin
  sbCompleta.Font.Color:=clBlack;
end;

procedure TFPrincipal.sbPedidoMouseLeave(Sender: TObject);
begin
  sbPedido.Font.Color:=clBlack;
end;

procedure TFPrincipal.sbCertidaoMouseLeave(Sender: TObject);
begin
  sbCertidao.Font.Color:=clBlack;
end;

procedure TFPrincipal.sbCancelamentoMouseLeave(Sender: TObject);
begin
  sbCancelamento.Font.Color:=clBlack;
end;

procedure TFPrincipal.sbUpdateClick(Sender: TObject);
begin
  if not GR.MaquinaServidor then
  begin
    GR.Processando(False);
    GR.Aviso('ESTA OPERA��O DEVE SER EXECUTADA LOCALMENTE NO SERVIDOR!');
  end
  else
  begin
    Gdm.vDiretorio  :='www\cartorios\centralprotesto';
    Gdm.vExecutavel :='CentralProtesto.exe';
    Gdm.vTemp       :='CentralProtestoNewTemp.exe';

    GR.Processando(False);

    Application.CreateForm(TFAtualizarSistema,FAtualizarSistema);
    FAtualizarSistema.Free;
  end;
end;

procedure TFPrincipal.CorrigirDocumentoSacador;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT ID_ATO,DOCUMENTO FROM SACADORES WHERE DOCUMENTO<>'''+''+'''');
  Q.Open;
  while not Q.Eof do
  begin
      Application.ProcessMessages;
      GR.ExecutarSQLDAC('UPDATE TITULOS SET CPF_CNPJ_SACADOR='''+Q.FieldByName('DOCUMENTO').AsString+''' WHERE ID_ATO='+QuotedStr(Q.FieldByName('ID_ATO').AsString),dm.conSISTEMA);
      Q.Next;
  end;
  Q.Close;
  Q.Free;
end;

procedure TFPrincipal.ConferenciaClick(Sender: TObject);
begin
  GR.CriarForm(TFRelConferencia,FRelConferencia);
end;

procedure TFPrincipal.AjustarPosicoesConcus;
begin
  GR.ExecutarSQLDAC('ALTER TABLE CONCUS ALTER ORDEM POSITION 1',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE CONCUS ALTER ANO POSITION 2',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE CONCUS ALTER VAI POSITION 3',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE CONCUS ALTER TAB POSITION 4',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE CONCUS ALTER ITEM POSITION 5',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE CONCUS ALTER SUB POSITION 6',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE CONCUS ALTER DESCR POSITION 7',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE CONCUS ALTER VALOR POSITION 8',dm.conCENTRAL);
  GR.ExecutarSQLDAC('ALTER TABLE CONCUS ALTER TEXTO POSITION 9',dm.conCENTRAL);
end;

procedure TFPrincipal.Atualizacoes2013;
begin
  {3} FSplash.Caption:='Atualizando Vers�o 1...';

  if dm.IdAtual('ID_VERSAO','N')=1 then
  begin
      GR.CriarCampoDAC('TITULO','COD','VARCHAR(50)',dm.conCENTRAL);
      GR.CriarCampoDAC('CONVENIO','COD','CHAR(1)',dm.conCENTRAL);
      InserirID('ID_MOTIVO',8);
      GR.CriarCampoDAC('PRACA','SERVENTIA','VARCHAR(4)',dm.conSISTEMA);
      GR.CriarCampoDAC('ARQUIVO','TITULOS','VARCHAR(20)',dm.conSISTEMA);
      CriarParametro(15,'CAMINHO DO BANCO DE RECIBO','C:\TOTAL\RECIBO\RECIBO.GDB');
      CriarParametro(16,'PROTOCOLAR NA ENTRADA','N');
      CriarParametro(17,'BANCO DE DADOS SERASA','S');
      CriarParametro(18,'N� DE REGISTROS POR FOLHA NA CERTIDAO','5');
      CriarParametro(19,'TARIFA BANCARIA','9,95');
      CriarParametro(20,'CAMINHO PARA SALVAR O ARQUIVO DO SERASA/EQUIFAX','C:\TOTAL\CENTRAL PROTESTO\');
      CriarParametro(21,'CABE�ALHO LIVRO PROTOCOLO','S');
      CriarParametro(22,'CONTROLAR FOLHAS','S');
      CriarParametro(23,'C�DIGO DO CART�RIO','02');
      GR.CriarCampoDAC('RETORNO','TITULOS','VARCHAR(20)',dm.conSISTEMA);
      GR.CriarCampoDAC('DT_DEVOLVIDO','TITULOS','DATE',dm.conSISTEMA);
      GR.CriarCampoDAC('SITE','SERVENTIA','VARCHAR(60)',dm.conSISTEMA);
      CriarParametro(24,'MENSAGEM NO FINAL DA INTIMA��O','');
      CriarParametro(25,'HOR�RIO DE FUNCIONAMENTO','09:00H �s 17:00H');
      GR.CriarCampoDAC('CPF_CNPJ_SACADOR','TITULOS','VARCHAR(14)',dm.conSISTEMA);
      GR.CriarTabelaDAC('SACADORES','ID_ATO INTEGER NOT NULL,ID_SACADOR INTEGER NOT NULL,NOME VARCHAR(100),TIPO CHAR(1),'+
                        'DOCUMENTO VARCHAR(14),ENDERECO VARCHAR(100),BAIRRO VARCHAR(100),CEP VARCHAR(8),MUNICIPIO VARCHAR(100),UF CHAR(2)',dm.conSISTEMA);
      DefinirChavePrimariaFD('SACADORES','PK_SACADOR','(ID_SACADOR)',dm.conSISTEMA);
      InserirID('ID_SACADOR',1);
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {3} FSplash.G.AddProgress(1);
  {4} FSplash.Caption:='Atualizando Vers�o 2...';

  if dm.IdAtual('ID_VERSAO','N')=2 then
  begin
      GR.CriarTabelaDAC('BALCAO','ID_BALCAO INTEGER NOT NULL,TABELA VARCHAR(20),ITEM VARCHAR(20),SUBITEM VARCHAR(20),'+
                        'DESCRICAO VARCHAR(100),QTD INTEGER,VALOR DOUBLE PRECISION,TOTAL DOUBLE PRECISION',dm.conSISTEMA);
      DefinirChavePrimariaFD('BALCAO','PK_BALCAO','(ID_BALCAO)',dm.conSISTEMA);
      InserirID('ID_BALCAO',1);
      CriarParametro(26,'M�TUA','8,90');
      CriarParametro(27,'ACOTERJ','0,17');
      CriarParametro(28,'DISTRIBUI��O','0,00');
      GR.CriarCampoDAC('ID_MSG','TITULOS','INTEGER',dm.conSISTEMA);
      InicializarID_MSG;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {4} FSplash.G.AddProgress(1);
  {5} FSplash.Caption:='Atualizando Vers�o 3...';

  if dm.IdAtual('ID_VERSAO','N')=3 then
  begin
      CriarParametro(29,'TABELA DE CUSTAS FIXA PARA CERTID�O','N');
      CriarParametro(30,'QTD. DE ANOS PADR�O PARA BUSCA DA CERTID�O','5');
      CriarParametro(31,'MSG DO CONTRAPROTESTO','Pelo devedor foram apresentadas raz�es que seguem anexas ao presente instrumento.');
      GR.CriarTabelaDAC('MENSAGENS','ID_MSG INTEGER NOT NULL,MENSAGEM VARCHAR(200)',dm.conSISTEMA);
      DefinirChavePrimariaFD('MENSAGENS','PK_MENSAGEM','(ID_MSG)',dm.conSISTEMA);
      InserirID('ID_MSG',1);
      CriarMensagemZero;
      GR.CriarCampoDAC('CODIGO_PRACA','SERVENTIA','INTEGER',dm.conSISTEMA);
      AtualizarPracas;
      GR.CriarCampoDAC('CRA','PORTADORES','CHAR(1)',dm.conSISTEMA);
      AtualizarCRA;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {5} FSplash.G.AddProgress(1);
  {6} FSplash.Caption:='Atualizando Vers�o 4...';

  if dm.IdAtual('ID_VERSAO','N')=4 then
  begin
      GR.CriarCampoDAC('RESPONSAVEL','CERTIDOES','VARCHAR(11)',dm.conSISTEMA);
      CriarParametro(32,'ASSINATURA INTIMA��O','TABELI�O DE PROTESTO');
      CriarParametro(33,'UTILIZAR TERMO','S');
      CriarParametro(34,'2� TELEFONE','');
      CriarParametro(35,'VALOR A.R.','0,00');
      GR.CriarCampoDAC('VALOR_AR','TITULOS','DOUBLE PRECISION DEFAULT 0',dm.conSISTEMA);
      GR.CriarCampoDAC('CONVENIO','BALCAO','CHAR(1)',dm.conSISTEMA);
      CorrigirID_BALCAO;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {6} FSplash.G.AddProgress(1);
  {7} FSplash.Caption:='Atualizando Vers�o 5...';

  if dm.IdAtual('ID_VERSAO','N')=5 then
  begin
      CriarParametro(36,'N�MERO DO CART�RIO','XX');
      CriarParametro(37,'N� SEQUENCIAL DE REMESSAS DO SERASA','1');
      AtualizarCRANull;
      GR.CriarTabelaDAC('REMESSAS','ID_REMESSA INTEGER NOT NULL,ID_PORTADOR INTEGER NOT NULL,DATA DATE,NUMERO INTEGER',dm.conSISTEMA);
      DefinirChavePrimariaFD('REMESSAS','PK_REMESSAS','(ID_REMESSA)',dm.conSISTEMA);
      InserirID('ID_REMESSA',1);
      GR.CriarCampoDAC('CODIGO_MUNICIPIO','SERVENTIA','VARCHAR(7)',dm.conSISTEMA);
      AtualizarCodigoMunicipio;
      GR.CriarCampoDAC('TIPO','REMESSAS','CHAR(2)',dm.conSISTEMA);
      CriarParametro(38,'CAMINHO PARA EXPORTAR OS CANCELAMENTOS ELETR�NICOS (CP)','C:\TOTAL\CENTRAL PROTESTO\');
      CriarParametro(39,'CAMINHO PARA EXPORTAR AS DESIST�NCIAS ELETR�NICAS (DP)','C:\TOTAL\CENTRAL PROTESTO\');
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {7} FSplash.G.AddProgress(1);
  {8} FSplash.Caption:='Atualizando Vers�o 6...';

  if dm.IdAtual('ID_VERSAO','N')=6 then
  begin
      GR.CriarTabelaDAC('ELETRONICO','ID_ELETRONICO INTEGER NOT NULL,TIPO CHAR(1),TABELA VARCHAR(20),ITEM VARCHAR(20),SUBITEM VARCHAR(20),'+
                        'DESCRICAO VARCHAR(100),QTD INTEGER,VALOR DOUBLE PRECISION,TOTAL DOUBLE PRECISION',dm.conSISTEMA);
      DefinirChavePrimariaFD('ELETRONICO','PK_ELETRONICO','(ID_ELETRONICO)',dm.conSISTEMA);
      InserirID('ID_ELETRONICO',1);
      GR.CriarTabelaDAC('FUNDOS','ID_FUNDOS INTEGER NOT NULL,TIPO CHAR(1),EMOLUMENTOS DOUBLE PRECISION,FETJ DOUBLE PRECISION,FUNDPERJ DOUBLE PRECISION,'+
                  'FUNPERJ DOUBLE PRECISION,MUTUA DOUBLE PRECISION,ACOTERJ DOUBLE PRECISION,DISTRIBUICAO DOUBLE PRECISION,TOTAL DOUBLE PRECISION',dm.conSISTEMA);
      DefinirChavePrimariaFD('FUNDOS','PK_FUNDOS','(ID_FUNDOS)',dm.conSISTEMA);
      InserirID('ID_FUNDOS',1);
      GR.CriarCampoDAC('ELETRONICO','TITULOS','CHAR(1) DEFAULT '''+'N'+'''',dm.conSISTEMA);
      GR.CriarCampoDAC('SEQUENCIA','PORTADORES','INTEGER',dm.conSISTEMA);
      IniciarSequenciaRemessas;
      GR.CriarCampoDAC('ELETRONICO','TITULOS','CHAR(1) DEFAULT'''+'N'+'''',dm.conSISTEMA);
      EletronicoNullNao;
      CorrigirTipoProtesto;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {8} FSplash.G.AddProgress(1);
  {9} FSplash.Caption:='Atualizando Vers�o 7...';

  if dm.IdAtual('ID_VERSAO','N')=7 then
  begin
      GR.CriarCampoDAC('IRREGULARIDADE','TITULOS','INTEGER',dm.conSISTEMA);
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {9} FSplash.G.AddProgress(1);
  {10} FSplash.Caption:='Atualizando Vers�o 8...';

  if dm.IdAtual('ID_VERSAO','N')=8 then
  begin
      CorrigirConvenioTangua;
      CorrigirStatusDuasBarras;
      CorrigirProtestadoCarmo;
      CriarParametro(40,'CUSTAS NO FINAL DO EVENTO','N');
      GR.CriarTabelaDAC('ONLINE','ID_ONLINE INTEGER NOT NULL,DATA DATE,PROTESTOS INTEGER,SUSTADOS INTEGER,ENVIADO CHAR(1)',dm.conSISTEMA);
      DefinirChavePrimariaFD('ONLINE','PK_ONLINE','(ID_ONLINE)',dm.conSISTEMA);
      InserirID('ID_ONLINE',1);
      CriarParametro(41,'CAMINHO PARA O ARQUIVO DO BANCO UNIFICADO',ExtractFilePath(Application.ExeName));
      GR.CriarCampoDAC('CANCELADOS','ONLINE','INTEGER',dm.conSISTEMA);
      AlterarARParaApontado;
      AtualizarCRANull;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {10} FSplash.G.AddProgress(1);
  {11} FSplash.Caption:='Atualizando Vers�o 9...';

  if dm.IdAtual('ID_VERSAO','N')=9 then
  begin
      CriarParametro(42,'IMPRIMIR RECIBO NA MATRICIAL','N');
      CriarParametro(43,'IMPORTAR DO DISTRIBUIDOR','N');
      PF.ExcluirFD('ESCREVENTES WHERE NOME='''+'TOTAL'+''' OR LOGIN='''+'TOTAL'+'''',dm.conSISTEMA);
      GR.CriarTabelaDAC('VERSAO','DATA VARCHAR(18)',dm.conSISTEMA);
      CriarParametro(44,'NOME DO BANCO','');
      CriarParametro(45,'AG�NCIA','');
      CriarParametro(46,'CONTA CORRENTE','');
      CriarParametro(47,'IMPRIME GUIA INTIMA��O','N');
      CriarParametro(48,'ESTOQUE DE SELOS UNIFICADO','N');
      CriarParametro(49,'ASSISTENTE','N');
      CriarParametro(50,'TIPO DE EDITAL','Lista');
      AdicionarIrregularidade(-1,'Retirado pelo Cliente');
      GR.CriarCampoDAC('AVISTA','TITULOS','CHAR(1)',dm.conSISTEMA);
      CriarParametro(51,'IMPRIMIR CARIMBO DE CANCELAMENTO','N');
      CriarParametro(52,'IMPRIMIR TABELAS NO RECIBO MATRICIAL','N');
      CriarParametro(53,'IMPRIMIR HORA NO INSTRUMENTO','N');
      GR.CriarIndiceDAC('TITULOS_IDX9','TITULOS','(EMOLUMENTOS,FETJ,FUNDPERJ,FUNPERJ,MUTUA,DISTRIBUICAO,ACOTERJ,TOTAL)',dm.conSISTEMA);
      GR.CriarIndiceDAC('TITULOS_IDX10','TITULOS','(CODIGO,DT_PROTOCOLO,COBRANCA)',dm.conSISTEMA);
      GR.CriarIndiceDAC('TITULOS_IDX11','TITULOS','(DT_PAGAMENTO,COBRANCA,STATUS)',dm.conSISTEMA);
      GR.CriarIndiceDAC('TITULOS_IDX12','TITULOS','(DT_RETIRADO,COBRANCA,STATUS)',dm.conSISTEMA);
      CriarParametro(54,'EXPORTAR ARQUIVO DE RETORNO �NICO','N');
      CriarIndiceDescending('TITULOS_IDX13','TITULOS','(DT_PROTOCOLO,PROTOCOLO)');
      CriarIndiceDescending('TITULOS_IDX14','TITULOS','(DT_PROTOCOLO)');
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {11} FSplash.G.AddProgress(1);
  {12} FSplash.Caption:='Atualizando Vers�o 10...';

  if dm.IdAtual('ID_VERSAO','N')=10 then
  begin
      CorrigirConveniosCampos;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {12} FSplash.G.AddProgress(1);
  {13} FSplash.Caption:='Atualizando Vers�o 11...';

  if dm.IdAtual('ID_VERSAO','N')=11 then
  begin
      CorrigirID_Mensagem;
      GR.CriarIndiceDAC('TABCUS_IDX1','TABCUS','(COD)',dm.conCENTRAL);
      GR.CriarTabelaDAC('PRACAS','ID_PRACA INTEGER NOT NULL,NOME VARCHAR(20)',dm.conSISTEMA);
      DefinirChavePrimariaFD('PRACAS','PK_PRACA','(ID_PRACA)',dm.conSISTEMA);
      InserirID('ID_PRACA',0);
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {13} FSplash.G.AddProgress(1);
  {14} FSplash.Caption:='Atualizando Vers�o 12...';

  if dm.IdAtual('ID_VERSAO','N')=12 then
  begin
      CriarParametro(55,'MENSAGEM PARA LOCAL DE PAGAMENTO','');
      CriarParametro(56,'ESPECIE DA MOEDA','R$');
      CriarParametro(57,'CARTEIRA','');
      CriarParametro(58,'CEDENTE',dm.ServentiaDESCRICAO.AsString);
      CriarParametro(59,'INSTRU��O 1','PAGAMENTO SOMENTE EM DINHEIRO');
      CriarParametro(60,'INSTRU��O 2','N�O RECEBER AP�S O VENCIMENTO');
      CriarParametro(61,'INSTRU��O 3','RETORNAR AO CART�RIO AP�S EFETUAR O PAGAMENTO');
      CriarParametro(62,'INSTRU��O 4','');
      CriarParametro(63,'INSTRU��O 5','');
      CriarParametro(64,'D�GITO DA AG�NCIA','');
      CriarParametro(65,'D�GITO DA CONTA','');
      CriarParametro(66,'N�MERO DO BANCO','');
      CriarParametro(67,'D�GITO DO BANCO','');
      CriarParametro(68,'C�DIGO DO CEDENTE','');
      GR.CriarTabelaDAC('BOLETOS','ID_BOLETO INTEGER NOT NULL,'+
                        'ID_ATO INTEGER,'+
                        'ID_DEVEDOR INTEGER,'+
                        'MENSAGEM_LOCAL_PAGAMENTO VARCHAR(80),'+
                        'ESPECIE_DOCUMENTO VARCHAR(5),'+
                        'MOEDA VARCHAR(5),'+
                        'ACEITE CHAR(1),'+
                        'CARTEIRA VARCHAR(5),'+
                        'NOSSO_NUMERO VARCHAR(8),'+
                        'VALOR_MORA_JUROS DOUBLE PRECISION,'+
                        'VALOR_DESCONTO DOUBLE PRECISION,'+
                        'VALOR_ABATIMENTO DOUBLE PRECISION,'+
                        'PORCENTO_MULTA DOUBLE PRECISION,'+
                        'DATA_MULTA_JUROS DATE,'+
                        'DATA_DESCONTO DATE,'+
                        'DATA_ABATIMENTO DATE,'+
                        'DATA_PROTESTO DATE,'+
                        'NUMERO_DOCUMENTO VARCHAR(20),'+
                        'VALOR DOUBLE PRECISION,'+
                        'DATA_EMISSAO DATE,'+
                        'DATA_VENCIMENTO DATE,'+
                        'INSTRUCAO1 VARCHAR(100),'+
                        'INSTRUCAO2 VARCHAR(100),'+
                        'INSTRUCAO3 VARCHAR(100),'+
                        'INSTRUCAO4 VARCHAR(100),'+
                        'INSTRUCAO5 VARCHAR(100)'
                        ,dm.conSISTEMA);
      DefinirChavePrimariaFD('BOLETOS','PK_BOLETOS','ID_BOLETO',dm.conSISTEMA);
      InserirID('ID_BOLETO',1);
      CriarChaveEstrangeiraFD('BOLETOS','FK_BOLETOS_1','ID_ATO','TITULOS(ID_ATO)','CASCADE',dm.conSISTEMA);
      CriarParametro(69,'IMPRIMIR BOLETOS','N');
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {14} FSplash.G.AddProgress(1);
  {15} FSplash.Caption:='Atualizando Vers�o 13...';

  if dm.IdAtual('ID_VERSAO','N')=13 then
  begin
      {GR.InserirItemCustasFD('7','1','OBS12A','ATOS LAVRADOS FORA DO HOR�RIO NORMAL DO EXPEDIENTE/FORA DO CART�RIO',0.00,dm.conCENTRAL);
      GR.InserirItemCustasFD('7','2','C','EM CAUSA PR�PRIA NO LIVRO DE PROCURA��ES, OU NO LIVRO DE NOTAS',302.07,dm.conCENTRAL);
      GR.InserirItemCustasFD('7','7','AI','APROVA��O',60.37,dm.conCENTRAL);
      GR.InserirItemCustasFD('7','7','AII','SE ESCRITO PELO TABELI�O A ROGO DO TESTADOR',120.78,dm.conCENTRAL);  }
      GR.CriarCampoDAC('SALDO_TITULO','TITULOS','DOUBLE PRECISION',dm.conSISTEMA);
      AtualizarSaldoTitulo;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {15} FSplash.G.AddProgress(1);
  {16} FSplash.Caption:='Atualizando Vers�o 14...';

  if dm.IdAtual('ID_VERSAO','N')=14 then
  begin
      GR.CriarCampoDAC('TIPO_SUSTACAO','TITULOS','CHAR(1)',dm.conSISTEMA);
      GR.CriarCampoDAC('RETORNO_PROTESTO','TITULOS','CHAR(1)',dm.conSISTEMA);
      GR.CriarCampoDAC('DT_RETORNO_PROTESTO','TITULOS','DATE',dm.conSISTEMA);
      GR.CriarCampoDAC('DT_DEFINITIVA','TITULOS','DATE',dm.conSISTEMA);
      GR.CriarCampoDAC('VALOR_DOC','PORTADORES','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('VALOR_TED','PORTADORES','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('DATA_PROTOCOLO','BOLETOS','DATE',dm.conSISTEMA);
      GR.CriarCampoDAC('PROTOCOLO','BOLETOS','INTEGER',dm.conSISTEMA);
      GR.CriarCampoDAC('HORA','BOLETOS','TIME',dm.conSISTEMA);
      GR.CriarCampoDAC('EMISSOR','BOLETOS','VARCHAR(20)',dm.conSISTEMA);
      GR.CriarCampoDAC('PAGO','BOLETOS','CHAR(1)',dm.conSISTEMA);
      GR.RemoverCampoDAC('MENSAGEM_LOCAL_PAGAMENTO','BOLETOS',dm.conSISTEMA);
      GR.CriarCampoDAC('APONTAMENTO','CERTIDOES','DOUBLE PRECISION',dm.conSISTEMA);
      CriarParametro(70,'LOCAL DO PAGAMENTO','PAGAR PREFERENCIALMENTE NAS AG�NCIAS DO '+dm.ValorParametro(44));
      GR.CriarCampoDAC('EMOLUMENTOS','BOLETOS','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('FETJ','BOLETOS','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('FUNDPERJ','BOLETOS','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('FUNPERJ','BOLETOS','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('TOTAL','BOLETOS','DOUBLE PRECISION',dm.conSISTEMA);
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {16} FSplash.G.AddProgress(1);
  {17} FSplash.Caption:='Atualizando Vers�o 15...';

  if dm.IdAtual('ID_VERSAO','N')=15 then
  begin
      GR.CriarCampoDAC('JUDICIAL','TITULOS','CHAR(1)',dm.conSISTEMA);
      GR.CriarCampoDAC('CAPA','CERTIDOES','DOUBLE PRECISION',dm.conSISTEMA);
      CriarParametro(71,'VALOR DA CAPA','0,00');
      CriarParametro(72,'N�MERO DA CERTID�O','0');
      GR.CriarCampoDAC('ORDEM','CERTIDOES','INTEGER',dm.conSISTEMA);
      CriarParametro(73,'DATA DE REFER�NCIA DO PRAZO','INTIMA��O');
      GR.CriarCampoDAC('ID_ORIGEM','CERTIDOES','INTEGER',dm.conSISTEMA);
      I:=TIniFile.Create(ExtractFilePath(Application.ExeName)+'Par.ini');
      I.WriteString('VIAS','RECIBO','1');
      I.Free;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {17} FSplash.G.AddProgress(1);
  {18} FSplash.Caption:='Atualizando Vers�o 16...';

  if dm.IdAtual('ID_VERSAO','N')=16 then
  begin
      GR.CriarCampoDAC('MATRICULA','ESCREVENTES','VARCHAR(15)',dm.conSISTEMA);
      CriarParametro(74,'DISTRIBUI��O BALC�O','0,00');
      CriarParametro(75,'DISTRIBUI��O BALC�O (CONV�NIO)','0,00');
      CriarParametro(76,'EXPORTAR BOLETO PARA PDF','N');
      GR.CriarTabelaDAC('SELOS','ID_SELO INTEGER NOT NULL,'+
                                'LETRA CHAR(3),'+
                                'NUMERO INTEGER,'+
                                'TIPO CHAR(1),'+
                                'STATUS VARCHAR(10),'+
                                'DATA_ENTRADA DATE,'+
                                'DATA_SAIDA DATE',dm.conSISTEMA);
      InserirID('ID_SELO',0);
      GR.CriarChavePrimariaDAC('SELOS','PK_SELOS','ID_SELO',dm.conSISTEMA);
      GR.CriarCampoDAC('FUNARPEN','BOLETOS','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('FUNARPEN','CERTIDOES','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('FUNARPEN','FUNDOS','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('FUNARPEN','TITULOS','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('FUNA','COD','FLOAT',dm.conCENTRAL);
      CriarParametro(77,'TABELA','2012');
      CriarParametro(78,'N� SEQUENCIAL DE REMESSAS DO BOAVISTA S/A','1');
      CriarParametro(79,'CAMINHO PARA SALVAR O ARQUIVO DO BOAVISTA','');
      CriarParametro(80,'CAMINHO PARA SALVAR O ARQUIVO DO LAE',ExtractFilePath(Application.ExeName));
      dm.MudarDescricaoParamentro(20,'CAMINHO PARA SALVAR O ARQUIVO DO SERASA');
      dm.MudarDescricaoParamentro(41,'CAMINHO PARA O ARQUIVO DO BANCO ONLINE');
      CriarParametro(81,'TIPO COBRAN�A ACBrBoleto','cobNenhum');
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {18} FSplash.G.AddProgress(1);
  {19} FSplash.Caption:='Atualizando Vers�o 17...';

  if dm.IdAtual('ID_VERSAO','N')=17 then
  begin
      GR.CriarCampoDAC('TEXTO','CONCUS','VARCHAR(250)',dm.conCENTRAL);
      GR.CriarCampoDAC('PMCMV','TITULOS','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('PMCMV','CERTIDOES','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('DESCRICAO','CUSTAS','VARCHAR(100)',dm.conSISTEMA);
      GR.CriarCampoDAC('PMCMV','COD','FLOAT',dm.conCENTRAL);
      GR.CriarCampoDAC('OCULTO','COD','CHAR(1)',dm.conCENTRAL);
      GR.ExecutarSQLDAC('UPDATE COD SET OCULTO='''+'S'+'''WHERE COD>=4001 AND COD<=4008',dm.conCENTRAL);
      GR.ExecutarSQLDAC('UPDATE COD SET OCULTO='''+'S'+'''WHERE COD>=4030 AND COD<=4037',dm.conCENTRAL);
      GR.ExecutarSQLDAC('UPDATE COD SET OCULTO='''+'N'+'''WHERE ATRIB=4 AND OCULTO IS NULL',dm.conCENTRAL);
      GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'N'+'''',dm.conCENTRAL);
      GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'S'+'''WHERE TAB='''+'1'+'''AND ITEM='''+'1'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
      GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'S'+'''WHERE TAB='''+'1'+'''AND ITEM='''+'2'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
      GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'S'+'''WHERE TAB='''+'1'+'''AND ITEM='''+'3'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
      GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'S'+'''WHERE TAB='''+'1'+'''AND ITEM='''+'4'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
      GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'S'+'''WHERE TAB='''+'1'+'''AND ITEM='''+'5'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
      AjustarPosicoesTabelaCOD;
      AjustarPosicoesTabelaTABCUS;
      PreencherDescricaoCustasEInserirNovasTabelas;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {19} FSplash.G.AddProgress(1);
  {20} FSplash.Caption:='Atualizando Vers�o 18...';

  if dm.IdAtual('ID_VERSAO','N')=18 then
  begin
      InserirTabelas;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {20} FSplash.G.AddProgress(1);
  {21} FSplash.Caption:='Atualizando Vers�o 19...';

  if dm.IdAtual('ID_VERSAO','N')=19 then
  begin
      GR.CriarCampoDAC('TEXTO','CONCUS','VARCHAR(250)',dm.conCENTRAL);
      GR.ExecutarSQLDAC('UPDATE CONCUS SET VAI='''+'S'+'''',dm.conCENTRAL);
      AjustarNovasTabelas;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {21} FSplash.G.AddProgress(1);
  {22} FSplash.Caption:='Atualizando Vers�o 20...';

  if dm.IdAtual('ID_VERSAO','N')=20 then
  begin
      GR.CriarIndiceDAC('CUSTAS_IDX1','CUSTAS','ID_ATO',dm.conSISTEMA);
      GR.CriarIndiceDAC('TITULOS_IDX15','TITULOS','CPF_CNPJ_DEVEDOR',dm.conSISTEMA);
      GR.CriarCampoDAC('ANO','CONCUS','INTEGER',dm.conCENTRAL);
      CorrigirDescricaoDoisCodigosEUmItem;
      GR.CriarCampoDAC('PMCMV','BOLETOS','DOUBLE PRECISION',dm.conSISTEMA);
      CorrigirDocumentoSacador;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {22} FSplash.G.AddProgress(1);
  {23} FSplash.Caption:='Atualizando Vers�o 21...';

  if dm.IdAtual('ID_VERSAO','N')=21 then
  begin
      AdicionarIrregularidade(70,'Dados do Cedente em branco ou inv�lido');
      AdicionarIrregularidade(98,'Retirada por falta de tempo h�bil');
      GR.CriarCampoDAC('ANO','CONCUS','INTEGER',dm.conCENTRAL);
      GR.ExecutarSQLDAC('UPDATE CONCUS SET ANO=2012 WHERE VAI='''+'N'+'''',dm.conCENTRAL);
      GR.ExecutarSQLDAC('UPDATE CONCUS SET ANO=2013 WHERE VAI='''+'S'+'''',dm.conCENTRAL);
      AjustarPosicoesCONCUS;
      dm.IdAtual('ID_VERSAO', 'S');
  end;
end;

procedure TFPrincipal.CorrigirFaixaConvenio;
var
  E,C: Double;
  S: TFDQuery;
begin
  dm.Custas.Close;
  dm.Custas.Open;

  S:=TFDQuery.Create(Nil);
  S.Connection:=dm.conSISTEMA;
  S.Close;
  S.Sql.Text:='SELECT ID_ATO,EMOLUMENTOS,FETJ,FUNDPERJ,FUNPERJ,FUNARPEN,PMCMV,MUTUA,ACOTERJ,DISTRIBUICAO,VALOR_AR,'+
              'TARIFA_BANCARIA,TOTAL,CODIGO,'+
              'VALOR_TITULO,SALDO_TITULO,SALDO_PROTESTO FROM TITULOS WHERE DT_PROTOCOLO >=:DT AND CONVENIO='''+'S'+'''';
  S.ParamByName('DT').AsDate:=StrToDate('01/01/2014');
  S.Open;
  S.Last;
  S.First;
  while not S.Eof do
  begin
      Application.ProcessMessages;
      PF.Aguarde(True,'Aguarde: '+IntToStr(S.RecNo)+'/'+IntToStr(S.RecordCount));
      GR.ExecutarSQLDAC('DELETE FROM CUSTAS WHERE ID_ATO='+S.FieldByName('ID_ATO').AsString,dm.conSISTEMA);

      dm.qryFaixa.Close;
      dm.qryFaixa.ParamByName('C').AsString        :='S';
      dm.qryFaixa.ParamByName('OCULTO1').AsString  :='N';
      dm.qryFaixa.ParamByName('OCULTO2').AsString  :='N';
      dm.qryFaixa.ParamByName('VALOR1').AsFloat    :=S.FieldByName('SALDO_TITULO').AsFloat;
      dm.qryFaixa.ParamByName('VALOR2').AsFloat    :=S.FieldByName('SALDO_TITULO').AsFloat;
      dm.qryFaixa.Open;
      E:=0;
      C:=0;
      dm.Itens.Close;
      dm.Itens.Params[0].AsInteger:=dm.qryFaixaCOD.AsInteger;
      dm.Itens.Open;
      dm.Itens.First;
      while not dm.Itens.Eof do
      begin
          dm.Custas.Append;
          dm.CustasID_CUSTA.AsInteger :=dm.IdAtual('ID_CUSTA','S');
          dm.CustasID_ATO.AsInteger   :=S.FieldByName('ID_ATO').AsInteger;
          dm.CustasTABELA.AsString    :=dm.ItensTAB.AsString;
          dm.CustasITEM.AsString      :=dm.ItensITEM.AsString;
          dm.CustasSUBITEM.AsString   :=dm.ItensSUB.AsString;
          dm.CustasVALOR.AsFloat      :=dm.ItensVALOR.AsFloat;
          dm.CustasQTD.AsString       :=dm.ItensQTD.AsString;
          dm.CustasTOTAL.AsFloat      :=dm.CustasVALOR.AsFloat*dm.CustasQTD.AsFloat;
          dm.Custas.Post;
          dm.Custas.ApplyUpdates(0);

          E:=E+dm.CustasTOTAL.AsFloat;
          if dm.CustasTABELA.AsString='16' then
          C:=C+dm.CustasTOTAL.AsFloat;

          dm.Itens.Next;
      end;

      S.Edit;
      S.FieldByName('EMOLUMENTOS').AsFloat    :=E;
      S.FieldByName('FETJ').AsFloat           :=GR.FETJ(E);
      S.FieldByName('FUNDPERJ').AsFloat       :=GR.FUNDPERJ(E);
      S.FieldByName('FUNPERJ').AsFloat        :=GR.FUNPERJ(E);
      S.FieldByName('FUNARPEN').AsFloat       :=GR.FUNARPEN(E);
      S.FieldByName('PMCMV').AsFloat          :=GR.PMCMV(E-C);
      S.FieldByName('TOTAL').AsFloat          :=GR.SomarFundos(E,C)+S.FieldByName('MUTUA').AsFloat+S.FieldByName('ACOTERJ').AsFloat+
                                                S.FieldByName('DISTRIBUICAO').AsFloat+S.FieldByName('VALOR_AR').AsFloat+
                                                S.FieldByName('TARIFA_BANCARIA').AsFloat;

      S.FieldByName('SALDO_PROTESTO').AsFloat :=S.FieldByName('SALDO_TITULO').AsFloat+S.FieldByName('TOTAL').AsFloat;
      S.FieldByName('CODIGO').AsInteger       :=dm.qryFaixaCOD.AsInteger;
      S.Post;
      S.ApplyUpdates(0);
      S.Next;
  end;

  S.Close;
  S.Free;
  PF.Aguarde(False);
end;

procedure TFPrincipal.AumentarTamanhoSelos;
begin
  GR.ExecutarSQLDAC('UPDATE RDB$FIELDS SET RDB$FIELD_LENGTH=9,RDB$CHARACTER_LENGTH=9 WHERE RDB$FIELD_NAME='+
                  QuotedStr(GR.SourceCampoDAC('TITULOS','SELO_REGISTRO',dm.conSISTEMA)),dm.conSISTEMA);

  GR.ExecutarSQLDAC('UPDATE RDB$FIELDS SET RDB$FIELD_LENGTH=9,RDB$CHARACTER_LENGTH=9 WHERE RDB$FIELD_NAME='+
                  QuotedStr(GR.SourceCampoDAC('TITULOS','SELO_PAGAMENTO',dm.conSISTEMA)),dm.conSISTEMA);

  GR.ExecutarSQLDAC('UPDATE RDB$FIELDS SET RDB$FIELD_LENGTH=9,RDB$CHARACTER_LENGTH=9 WHERE RDB$FIELD_NAME='+
                    QuotedStr(GR.SourceCampoDAC('CERTIDOES','SELO',dm.conSISTEMA)),dm.conSISTEMA);
end;

procedure TFPrincipal.MudarTipoCodigoApresentante;
begin
  GR.CriarCampoDAC('TEMP','PORTADORES','VARCHAR(10)',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE PORTADORES SET TEMP=CODIGO',dm.conSISTEMA);
  GR.RemoverCampoDAC('CODIGO','PORTADORES',dm.conSISTEMA);
  GR.CriarCampoDAC('CODIGO','PORTADORES','VARCHAR(10)',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE PORTADORES SET CODIGO=TEMP',dm.conSISTEMA);
  GR.RemoverCampoDAC('TEMP','PORTADORES',dm.conSISTEMA);
  GR.AjustarPosicaoDAC('PORTADORES','CODIGO','2',dm.conSISTEMA);

  GR.CriarCampoDAC('TEMP','TITULOS','VARCHAR(10)',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TITULOS SET TEMP=CODIGO_APRESENTANTE',dm.conSISTEMA);
  GR.RemoverCampoDAC('CODIGO_APRESENTANTE','TITULOS',dm.conSISTEMA);
  GR.CriarCampoDAC('CODIGO_APRESENTANTE','TITULOS','VARCHAR(10)',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TITULOS SET CODIGO_APRESENTANTE=TEMP',dm.conSISTEMA);
  GR.RemoverCampoDAC('TEMP','TITULOS',dm.conSISTEMA);
  GR.AjustarPosicaoDAC('TITULOS','CODIGO_APRESENTANTE','40',dm.conSISTEMA);
end;

procedure TFPrincipal.CorrigirDescricaoTabelasConcus;
begin
  GR.ExecutarSQLDAC('UPDATE CONCUS SET DESCR='''+'PROTOCOLIZA��O DE R$ 800,01 AT� 900,00'+'''WHERE '+
                  'TAB='''+'24'+'''AND ITEM='''+'1'+'''AND SUB='''+'N'+'''AND ANO=2014',dm.conCENTRAL);

  GR.ExecutarSQLDAC('UPDATE CONCUS SET DESCR='''+'PROTOCOLIZA��O DE R$ 900,01 AT� 1.000,00'+'''WHERE '+
                  'TAB='''+'24'+'''AND ITEM='''+'1'+'''AND SUB='''+'O'+'''AND ANO=2014',dm.conCENTRAL);

  GR.ExecutarSQLDAC('UPDATE CONCUS SET DESCR='''+'PROTOCOLIZA��O DE R$ 1.000,01 AT� 1.500,00'+'''WHERE '+
                  'TAB='''+'24'+'''AND ITEM='''+'1'+'''AND SUB='''+'P'+'''AND ANO=2014',dm.conCENTRAL);

  GR.ExecutarSQLDAC('UPDATE CONCUS SET DESCR='''+'PROTOCOLIZA��O DE R$ 1.500,01 AT� 2.000,00'+'''WHERE '+
                    'TAB='''+'24'+'''AND ITEM='''+'1'+'''AND SUB='''+'Q'+'''AND ANO=2014',dm.conCENTRAL);
end;

procedure TFPrincipal.sbSerieAtualClick(Sender: TObject);
begin
  Gdm.vIdSerie:=StrToInt(dm.ValorParametro(82));
  GR.CriarForm(TFSerieAtual,FSerieAtual);
  if Gdm.vOkay then
  dm.AtualizarParametro(82,IntToStr(Gdm.vIdSerie));
end;

procedure TFPrincipal.FormDestroy(Sender: TObject);
var
  vOldExe: String;
begin
  if not Gdm.vAtualizar then Exit;

  try
    if Gdm.vDownload then
    begin
        vOldExe:=GR.DirExe+'CentralProtesto_'+FormatDateTime('ddmmyyyy_hhmm',Now)+'.exe';
        RenameFile(GR.DirExe+'CentralProtesto.exe',vOldExe);
        RenameFile(GR.DirExe+'CentralProtestoNewTemp.exe','CentralProtesto.exe');
        GR.Processando(False);
        ShellExecute(Handle,'Open',PChar(GR.DirExe+'CentralProtesto.exe'),Nil,Nil,SW_SHOWNORMAL);
    end;
  except
    RenameFile(vOldExe,GR.DirExe+'CentralProtesto.exe');
  end;
end;

procedure TFPrincipal.sbLinkMouseLeave(Sender: TObject);
begin
  sbLink.Font.Color:=clBlack;
end;

procedure TFPrincipal.sbLinkMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  sbLink.Font.Color:=clWindow;
end;

procedure TFPrincipal.sbLinkClick(Sender: TObject);
begin
  GR.CriarForm(TFXML2014,FXML2014);
end;

procedure TFPrincipal.a1Click(Sender: TObject);
begin
  GR.CriarForm(TFRelImportadosExportados, FRelImportadosExportados);
end;

procedure TFPrincipal.A2Click(Sender: TObject);
begin
  if PF.PasswordInputBox('Seguran�a','Senha')<>FormatDateTime('HHMM',Now) then
  begin
      GR.Aviso('SENHA INV�LIDA!');
      Exit;
  end;

 PF.Aguarde(True);
 GR.ExecutarSQLDAC('UPDATE TITULOS SET TOTAL=EMOLUMENTOS+FETJ+FUNDPERJ+FUNPERJ+FUNARPEN+PMCMV+MUTUA+ACOTERJ+DISTRIBUICAO+TARIFA_BANCARIA+VALOR_AR',dm.conSISTEMA);
 PF.Aguarde(False);
 GR.Aviso('O SISTEMA SER� FECHADO!');
 Application.Terminate;
end;

{FIZ ESSA ROTINA S� PARA ARRUMAR OS CONV�NIO DE ITATIAIA}
{PEGUEI O BANCO E RODEI NA MINHA M�QUINA S� PARA ELES}
procedure TFPrincipal.SincronizarConvenioComPortador;
begin
  GR.ExecutarSQLDAC('DELETE FROM PORTADORES WHERE NOME='''+''+'''',dm.conSISTEMA);

  dm.Portadores.Close;
  dm.Portadores.Open;
  dm.Portadores.Last;
  dm.Portadores.First;
  while not dm.Portadores.Eof do
  begin
      Application.ProcessMessages;
      PF.Aguarde(True,'Aguarde: '+IntToStr(dm.Portadores.RecNo)+'/'+IntToStr(dm.Portadores.RecordCount));
      GR.ExecutarSQLDAC('UPDATE TITULOS SET CONVENIO='+QuotedStr(dm.PortadoresCONVENIO.AsString)+
                        'WHERE CODIGO_APRESENTANTE='+QuotedStr(dm.PortadoresCODIGO.AsString)+
                        ' OR APRESENTANTE='+QuotedStr(dm.PortadoresNOME.AsString),dm.conSISTEMA);
      dm.Portadores.Next;
  end;
  PF.Aguarde(False);
  GR.Aviso('CONCLU�DO!');
end;

procedure TFPrincipal.AjustarPosicoesCamposCertidoes;
var
  vOrdem: Integer;
begin
  vOrdem:=1;
  GR.AjustarPosicaoDAC('CERTIDOES','ID_CERTIDAO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','ID_ATO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','ORDEM',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','DT_PEDIDO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','DT_ENTREGA',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','DT_CERTIDAO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','TIPO_CERTIDAO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','ANOS',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','CONVENIO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','CODIGO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','SELO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','ALEATORIO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','RECIBO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','REQUERIDO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','TIPO_REQUERIDO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','CPF_CNPJ_REQUERIDO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','REQUERENTE',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','CPF_CNPJ_REQUERENTE',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','TELEFONE_REQUERENTE',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','RESULTADO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','FOLHAS',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','REGISTROS',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','COBRANCA',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','EMOLUMENTOS',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','FETJ',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','FUNDPERJ',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','FUNPERJ',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','FUNARPEN',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','PMCMV',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','MUTUA',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','ACOTERJ',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','DISTRIBUICAO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','APONTAMENTO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','CAPA',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','TOTAL',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','EXRECIBO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','EXEMOLUMENTOS',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','EXFETJ',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','EXFUNDPERJ',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','EXFUNPERJ',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','EXFUNARPEN',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','EXTOTAL',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','ESCREVENTE',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','RESPONSAVEL',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','OBSERVACAO',IntToStr(vOrdem),dm.conSISTEMA); Inc(vOrdem);
  GR.AjustarPosicaoDAC('CERTIDOES','ENVIADO',IntToStr(vOrdem),dm.conSISTEMA);
end;

procedure TFPrincipal.Atualizacoes2014;
begin
  {23} FSplash.G.AddProgress(1);
  {24} FSplash.Caption:='Atualizando Vers�o 22...';
  if dm.IdAtual('ID_VERSAO','N')=22 then
  begin
      CorrigirFaixaConvenio;
      Application.Terminate;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {24} FSplash.G.AddProgress(1);
  {25} FSplash.Caption:='Atualizando Vers�o 23...';
  if dm.IdAtual('ID_VERSAO','N')=23 then
  begin
      CriarParametro(82,'S�RIE ATUAL','0');
      AumentarTamanhoSelos;
      GR.CriarCampoDAC('ENVIADO_APONTAMENTO','TITULOS','CHAR(1)',dm.conSISTEMA);
      GR.CriarCampoDAC('ENVIADO_RETIRADO','TITULOS','CHAR(1)',dm.conSISTEMA);
      GR.CriarCampoDAC('ENVIADO_SUSTADO','TITULOS','CHAR(1)',dm.conSISTEMA);
      GR.CriarCampoDAC('ENVIADO_DEVOLVIDO','TITULOS','CHAR(1)',dm.conSISTEMA);
      GR.AjustarPosicaoDAC('TITULOS','ENVIADO_APONTAMENTO','66',dm.conSISTEMA);
      GR.AjustarPosicaoDAC('TITULOS','ENVIADO_RETIRADO','69',dm.conSISTEMA);
      GR.AjustarPosicaoDAC('TITULOS','ENVIADO_SUSTADO','70',dm.conSISTEMA);
      GR.AjustarPosicaoDAC('TITULOS','ENVIADO_DEVOLVIDO','71',dm.conSISTEMA);
      GR.CriarCampoDAC('ALEATORIO_PROTESTO','TITULOS','VARCHAR(3)',dm.conSISTEMA);
      GR.CriarCampoDAC('ALEATORIO_SOLUCAO','TITULOS','VARCHAR(3)',dm.conSISTEMA);
      GR.CriarCampoDAC('CCT','TITULOS','VARCHAR(9)',dm.conSISTEMA);
      GR.CriarCampoDAC('ALEATORIO','CERTIDOES','VARCHAR(3)',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE TITULOS SET ENVIADO_APONTAMENTO='''+'N'+'''WHERE ENVIADO_APONTAMENTO IS NULL',dm.conSISTEMA);
      GR.CriarCampoDAC('DETERMINACAO','TITULOS','VARCHAR(100)',dm.conSISTEMA);
      GR.Backup(dm.conSISTEMA.Params.Values['Database'],GR.DirExe,'PROTESTO');
      MudarTipoCodigoApresentante;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {25} FSplash.G.AddProgress(1);
  {26} FSplash.Caption:='Atualizando Vers�o 24...';
  if dm.IdAtual('ID_VERSAO','N')=24 then
  begin
      CorrigirDescricaoTabelasConcus;
      CriarParametro(83,'INCLUIR A.R. NA CONFIRMA��O','N');

      if (dm.CampoServentia('CODIGO')='1866') or (dm.CampoServentia('CODIGO')='1208') or
         (dm.CampoServentia('CODIGO')='8247') or (dm.CampoServentia('CODIGO')='1237') then
      GR.ExecutarSQLDAC('UPDATE PARAMETROS SET VALOR='''+'S'+'''WHERE ID_PARAMETRO=83',dm.conSISTEMA);
      GR.CriarIndiceDAC('TITULOS_IDX16','TITULOS','DT_REGISTRO',dm.conSISTEMA);
      GR.CriarIndiceDAC('TITULOS_IDX17','TITULOS','DT_SUSTADO',dm.conSISTEMA);
      GR.CriarIndiceDAC('TITULOS_IDX18','TITULOS','DT_DEFINITIVA',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE PARAMETROS SET PATH_PROTESTO='''+dm.conSISTEMA.Params.Values['Database']+'''',Gdm.FDGERENCIAL);
      GR.ExecutarSQLDAC('UPDATE PARAMETROS SET VALOR='''+Gdm.PathMas+'''WHERE ID_PARAMETRO=1',dm.conSISTEMA);
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {26} FSplash.G.AddProgress(1);
  {27} FSplash.Caption:='Atualizando Vers�o 25...';
  if dm.IdAtual('ID_VERSAO','N')=25 then
  begin
      GR.AjustarPosicaoDAC('BOLETOS','FUNARPEN','35',dm.conSISTEMA);
      GR.AjustarPosicaoDAC('BOLETOS','PMCMV','36',dm.conSISTEMA);
      GR.CriarCampoDAC('CCT','BOLETOS','VARCHAR(9)',dm.conSISTEMA);
      GR.CriarCampoDAC('ENVIADO','BOLETOS','CHAR(1) DEFAULT '''+'N'+'''',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE BOLETOS SET ENVIADO='''+'N'+'''WHERE ENVIADO IS NULL',dm.conSISTEMA);
      GR.CriarCampoDAC('ANTIGO','TITULOS','CHAR(1) DEFAULT '''+'N'+'''',dm.conSISTEMA);
      PF.Aguarde(True);
      GR.ExecutarSQLDAC('UPDATE TITULOS SET ANTIGO='''+'N'+'''WHERE ANTIGO IS NULL',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE CERTIDOES SET TIPO_CERTIDAO='''+'E'+'''WHERE TIPO_CERTIDAO='''+'C'+'''',dm.conSISTEMA);
      PF.Aguarde(False);
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {27} FSplash.G.AddProgress(1);
  {28} FSplash.Caption:='Atualizando Vers�o 26...';
  if dm.IdAtual('ID_VERSAO','N')=26 then
  begin
      GR.CriarCampoDAC('EXRECIBO','CERTIDOES','INTEGER',dm.conSISTEMA);
      GR.CriarCampoDAC('EXEMOLUMENTOS','CERTIDOES','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('EXFETJ','CERTIDOES','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('EXFUNDPERJ','CERTIDOES','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('EXFUNPERJ','CERTIDOES','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('EXFUNARPEN','CERTIDOES','DOUBLE PRECISION',dm.conSISTEMA);
      GR.CriarCampoDAC('EXTOTAL','CERTIDOES','DOUBLE PRECISION',dm.conSISTEMA);
      GR.RemoverCampoDAC('ID_ORIGEM','CERTIDOES',dm.conSISTEMA);
      TrocarTipoCamposFolhasCertidao;
      AjustarPosicoesCamposCertidoes;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {28} FSplash.G.AddProgress(1);
  {29} FSplash.Caption:='Atualizando Vers�o 27...';
  if dm.IdAtual('ID_VERSAO','N')=27 then
  begin
      GR.CriarCampoDAC('EXRECIBO','CERTIDOES','INTEGER',dm.conSISTEMA);
      AjustarPosicoesCamposCertidoes;
      GR.CriarIndiceDAC('TITULOS_IDX19','TITULOS','NUMERO_TITULO,CODIGO_APRESENTANTE,VALOR_TITULO,DT_TITULO,DT_VENCIMENTO',dm.conSISTEMA);
      GR.CriarIndiceDAC('PORTADORES_IDX1','PORTADORES','CODIGO',dm.conSISTEMA);
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {29} FSplash.G.AddProgress(1);
  {30} FSplash.Caption:='Atualizando Vers�o 28...';
  if dm.IdAtual('ID_VERSAO','N')=28 then
  begin
      BuscarAleatoriosCancelamentos;
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {30} FSplash.G.AddProgress(1);
  {31} FSplash.Caption:='Atualizando Vers�o 29...';
  if dm.IdAtual('ID_VERSAO','N')=29 then
  begin
      GR.RemoverTabelaDAC('BALCAO',dm.conSISTEMA);
      GR.RemoverTabelaDAC('ELETRONICO',dm.conSISTEMA);
      GR.RemoverTabelaDAC('FUNDOS',dm.conSISTEMA);
      GR.RemoverTabelaDAC('REQUERENTES',dm.conSISTEMA);
      GR.RemoverTabelaDAC('SELOS',dm.conSISTEMA);
      GR.ExecutarSQLDAC('DELETE FROM IDS WHERE CAMPO='''+'ID_BALCAO'+'''',dm.conSISTEMA);
      GR.ExecutarSQLDAC('DELETE FROM IDS WHERE CAMPO='''+'ID_ELETRONICO'+'''',dm.conSISTEMA);
      GR.ExecutarSQLDAC('DELETE FROM IDS WHERE CAMPO='''+'ID_FUNDOS'+'''',dm.conSISTEMA);
      GR.ExecutarSQLDAC('DELETE FROM IDS WHERE CAMPO='''+'ID_PARAMETRO'+'''',dm.conSISTEMA);
      GR.ExecutarSQLDAC('DELETE FROM IDS WHERE CAMPO='''+'ID_SELO'+'''',dm.conSISTEMA);
      GR.CriarTabelaDAC('VERBAL','ID_VERBAL INTEGER NOT NULL,DATA DATE,CCT VARCHAR(9),NUMERO VARCHAR(25),ESCREVENTE VARCHAR(40),'+
                                 'NOME VARCHAR(200),TIPO_COBRANCA VARCHAR(2),EMOLUMENTOS DOUBLE PRECISION,FETJ DOUBLE PRECISION,'+
                                 'FUNDPERJ DOUBLE PRECISION,FUNPERJ DOUBLE PRECISION,FUNARPEN DOUBLE PRECISION,'+
                                 'TOTAL DOUBLE PRECISION,ENVIADO CHAR(1) DEFAULT'''+'N'+'''',
                                 dm.conSISTEMA);
      GR.CriarChavePrimariaDAC('VERBAL','PK_VERBAL','ID_VERBAL',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE CONCUS SET DESCR='''+'APOSI��O DE VISTO/INFORMA��O VERBAL'+
                        '''WHERE TAB='''+'16'+'''AND ITEM='''+'3'+'''AND SUB='''+'*'+'''',dm.conCENTRAL);
      CriarParametro(84,'CABE�ALHO PERSONALIZADO','N');
      GR.CriarCampoDAC('CCT','CERTIDOES','VARCHAR(9)',dm.conSISTEMA);
      GR.AjustarPosicaoDAC('CERTIDOES','CCT','43',dm.conSISTEMA);
      GR.CriarCampoDAC('ENVIADO_FLS','CERTIDOES','CHAR(1) DEFAULT '''+'N'+'''',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE CERTIDOES SET ENVIADO_FLS='''+'N'+'''WHERE ENVIADO_FLS IS NULL',dm.conSISTEMA);
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {31} FSplash.G.AddProgress(1);
  {32} FSplash.Caption:='Atualizando Vers�o 30...';
  if dm.IdAtual('ID_VERSAO','N')=30 then
  begin
      CriarParametro(85,'MOSTRAR TOTAL DOS TITULOS NA CERTID�O','N');
      CriarParametro(86,'N� DE FOLHAS DO LIVRO DE PROTOCOLO','200');
      AumentarDescricaoTipos;
      dm.InserirTipoTitulo('41','Contrato de Ades�o ao Cr�dito Direto');
      dm.InserirTipoTitulo('42','Contrato de Ades�o ao Cr�dito Direto Caixa');
      dm.InserirTipoTitulo('43','Contrato de Arrendamento Mercantil');
      dm.InserirTipoTitulo('44','Contrato de Leasing');
      dm.InserirTipoTitulo('45','Contrato de Loca��o');
      dm.InserirTipoTitulo('46','Contrato de Cr�dito Rotativo de Cheque');
      dm.InserirTipoTitulo('47','Fatura de Presta��o de Servi�os');
      dm.InserirTipoTitulo('48','Fatura de Venda Mercantil');
      dm.InserirTipoTitulo('49','Nota de D�bito');
      dm.InserirTipoTitulo('50','Ordem de Servi�o');
      dm.InserirTipoTitulo('51','Recibo de Presta��o de Servi�os');
      dm.InserirTipoTitulo('52','Recibo de Presta��o de Servi�os por Indica��o');
      dm.InserirTipoTitulo('53','Termo de Acordo Judicial');
      dm.InserirTipoTitulo('54','Contrato de Servi�os Educacionais');
      dm.InserirTipoTitulo('55','Certid�o de Cr�dito Judicial');
      dm.InserirTipoTitulo('56','Contrato de Abertura de Cr�dito por Indica��o');
      dm.InserirTipoTitulo('57','Contrato de Financiamento');
      dm.InserirTipoTitulo('58','Carta de Cr�dito Judicial');
      dm.InserirTipoTitulo('59','T�tulo Executivo Judicial Definitivo(Protocoliza��o com base no Ato Executivo Conjunto 07/2014)');
      dm.InserirTipoTitulo('60','Certid�o de D�bito do Tribunal(Protocoliza��o com base no Ato Executivo Conjunto 07/2014)');
      GR.ExecutarSQLDAC('UPDATE CONTROLE SET DESCRICAO='''+'N�MERO DO TERMO'+'''WHERE SIGLA='''+'NRG'+'''',dm.conSISTEMA);
      GR.CriarCampoDAC('NUMERO_TITULO','CERTIDOES','VARCHAR(20)',dm.conSISTEMA);
      GR.AjustarPosicaoDAC('CERTIDOES','NUMERO_TITULO','8',dm.conSISTEMA);
      GR.CriarCampoDAC('QTD','CERTIDOES','INTEGER',dm.conSISTEMA);
      //CriarParametro(87,'CAMINHO DA ASSINATURA DIGITALIZADA','');
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {32} FSplash.G.AddProgress(1);
  {33} FSplash.Caption:='Atualizando Vers�o 31...';
  if dm.IdAtual('ID_VERSAO','N')=31 then
  begin
      GR.CriarCampoDAC('ORDEM','DEVEDORES','INTEGER',dm.conSISTEMA);
      GR.AjustarPosicaoDAC('DEVEDORES','ORDEM','3',dm.conSISTEMA);
      GR.CriarIndiceDAC('PK_DEVEDORES_3','DEVEDORES','ORDEM',dm.conSISTEMA);
      GR.Backup(dm.conSISTEMA.Params.Values['Database'],GR.DirExe,'PROTESTO');
      PF.Aguarde(True,'Aguarde...');
      GR.ExecutarSQLDAC('UPDATE DEVEDORES SET ORDEM=1 WHERE ORDEM IS NULL',dm.conSISTEMA);
      PF.Aguarde(False);
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {33} FSplash.G.AddProgress(1);
  {34} FSplash.Caption:='Atualizando Vers�o 32...';
  if dm.IdAtual('ID_VERSAO','N')=32 then
  begin
      AjustarTabeladeRemessas;
      dm.AtualizarParametro(10,'PAR�METRO DESABILITADO');
      dm.AtualizarParametro(54,'PAR�METRO DESABILITADO');
      PF.Aguarde(True);
      GR.ExecutarSQLDAC('UPDATE SELOS_CCT SET EMOLUMENTOS=0,FETJ=0,FUNDPERJ=0,FUNPERJ=0,FUNARPEN=0,PMCMV=0,MUTUA=0,ACOTERJ=0,'+
                        'DISTRIBUICAO=0,INDISPONIBILIDADE=0,PRENOTACAO=0,AR=0,BANCARIA=0,TOTAL=0 WHERE NATUREZA='''+
                        'REGISTRO DE PROTESTO'+'''OR NATUREZA='''+'DESIST�NCIA'+'''',Gdm.FDGERENCIAL);
      GR.ExecutarSQLDAC('UPDATE SELOS_CCT SET TIPOCOBRANCA='''+'CV'+'''WHERE (NATUREZA='''+
                        'REGISTRO DE PROTESTO'+'''OR NATUREZA='''+'DESIST�NCIA'+''') AND CONVENIO='''+'S'+'''',Gdm.FDGERENCIAL);
      GR.ExecutarSQLDAC('UPDATE SELOS_CCT SET TIPOCOBRANCA='''+'PZ'+'''WHERE (NATUREZA='''+
                        'REGISTRO DE PROTESTO'+'''OR NATUREZA='''+'DESIST�NCIA'+''') AND CONVENIO='''+'N'+'''',Gdm.FDGERENCIAL);
      PF.Aguarde(False);
      GR.CriarCampoDAC('ID_PORTADOR','REMESSAS','INTEGER',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE REMESSAS SET ID_PORTADOR=-1 WHERE ID_PORTADOR IS NULL',dm.conSISTEMA);
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {34} FSplash.G.AddProgress(1);
  {35} FSplash.Caption:='Atualizando Vers�o 33...';
  if dm.IdAtual('ID_VERSAO','N')=33 then
  begin
      PuxarValoresApontamentosCanceladosConvenio;
      GR.ExecutarSQLDAC('UPDATE SELOS_CCT SET EMOLUMENTOS=0,FETJ=0,FUNDPERJ=0,FUNPERJ=0,FUNARPEN=0,PMCMV=0,MUTUA=0,ACOTERJ=0,'+
                        'DISTRIBUICAO=0,INDISPONIBILIDADE=0,PRENOTACAO=0,AR=0,BANCARIA=0,TOTAL=0 WHERE NATUREZA='''+
                        'PAGAMENTO'+'''',Gdm.FDGERENCIAL);
      GR.ExecutarSQLDAC('UPDATE SELOS_CCT SET TIPOCOBRANCA='''+'PC'+'''WHERE (NATUREZA='''+
                        'PAGAMENTO'+''') AND CONVENIO='''+'S'+'''',Gdm.FDGERENCIAL);
      GR.ExecutarSQLDAC('UPDATE SELOS_CCT SET TIPOCOBRANCA='''+'PZ'+'''WHERE (NATUREZA='''+
                        'PAGAMENTO'+''') AND CONVENIO='''+'N'+'''',Gdm.FDGERENCIAL);
      dm.IdAtual('ID_VERSAO', 'S');
  end;

  {35} FSplash.G.AddProgress(1);
  {36} FSplash.Caption:='Atualizando Vers�o 34...';
  if dm.IdAtual('ID_VERSAO','N')=34 then
  begin
      if not GR.CampoExisteDAC('DATA_BAIXA','BOLETOS',dm.conSISTEMA) then
      begin
          PF.Aguarde(True);
          GR.CriarCampoDAC('DATA_BAIXA','BOLETOS','DATE',dm.conSISTEMA);
          GR.ExecutarSQLDAC('UPDATE BOLETOS SET DATA_BAIXA=DATA_EMISSAO WHERE DATA_BAIXA IS NULL',dm.conSISTEMA);
          PF.Aguarde(False);
      end;
      GR.CriarCampoDAC('NOMINAL','PORTADORES','CHAR(1)',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE PORTADORES SET NOMINAL='''+'N'+'''WHERE NOMINAL IS NULL OR NOMINAL='''+''+'''',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE PORTADORES SET CRA='''+'N'+'''WHERE CRA IS NULL OR CRA='''+''+'''',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE PORTADORES SET BANCO='''+'N'+'''WHERE BANCO IS NULL OR BANCO='''+''+'''',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE PORTADORES SET CONVENIO='''+'N'+'''WHERE CONVENIO IS NULL OR CONVENIO='''+''+'''',dm.conSISTEMA);

      GR.ExecutarSQLDAC('UPDATE SELOS_CCT SET CODIGO=4026 WHERE (NATUREZA='''+'PAGAMENTO'+''') AND CONVENIO='''+'N'+'''',Gdm.FDGERENCIAL);
      GR.ExecutarSQLDAC('UPDATE SELOS_CCT SET CODIGO=4041 WHERE (NATUREZA='''+'PAGAMENTO'+''') AND CONVENIO='''+'S'+'''',Gdm.FDGERENCIAL);
      dm.IdAtual('ID_VERSAO', 'S');
  end;
end;

procedure TFPrincipal.TrocarTipoCamposFolhasCertidao;
var
  S: TFDQuery;
begin
  dm.Subitem.Close;
  dm.Subitem.Params[0].AsString :='16';
  dm.Subitem.Params[1].AsString :='2';
  dm.Subitem.Params[2].AsString :='*';
  dm.Subitem.Params[3].AsInteger:=StrToInt(dm.ValorParametro(77));
  dm.Subitem.Open;

  GR.CriarCampoDAC('TEMP','CERTIDOES','INTEGER',dm.conSISTEMA);
  S:=TFDQuery.Create(Nil);
  S.Connection := dm.conSISTEMA;
  S.SQL.Text:='SELECT ID_CERTIDAO,DT_PEDIDO,FOLHAS,TEMP,EXEMOLUMENTOS,EXFETJ,EXFUNDPERJ,EXFUNPERJ,EXFUNARPEN,EXTOTAL FROM CERTIDOES';
  S.Open;
  PF.Aguarde(True);
  S.Last;
  S.First;
  while not S.Eof do
  begin
      Application.ProcessMessages;
      PF.Aguarde(True,'Aguarde: '+IntToStr(S.RecNo)+'/'+IntToStr(S.RecordCount));
      S.Edit;
      if Trim(S.FieldByName('FOLHAS').AsString)<>'' then
        S.FieldByName('TEMP').AsInteger:=GR.PegarNumero(S.FieldByName('FOLHAS').AsString)
          else S.FieldByName('TEMP').AsInteger:=1;

      if S.FieldByName('TEMP').AsInteger>1 then
        if S.FieldByName('DT_PEDIDO').AsDateTime>=StrToDate('01/01/2014') then
        begin
            S.FieldByName('EXEMOLUMENTOS').AsFloat  :=dm.SubitemVALOR.AsFloat*(S.FieldByName('TEMP').AsInteger-1);
            S.FieldByName('EXFETJ').AsFloat         :=GR.NoRound(GR.NoRound(dm.SubitemVALOR.AsFloat*0.2,2)*(S.FieldByName('TEMP').AsInteger-1),2);
            S.FieldByName('EXFUNDPERJ').AsFloat     :=GR.NoRound(GR.NoRound(dm.SubitemVALOR.AsFloat*0.05,2)*(S.FieldByName('TEMP').AsInteger-1),2);
            S.FieldByName('EXFUNPERJ').AsFloat      :=GR.NoRound(GR.NoRound(dm.SubitemVALOR.AsFloat*0.05,2)*(S.FieldByName('TEMP').AsInteger-1),2);
            S.FieldByName('EXFUNARPEN').AsFloat     :=GR.NoRound(GR.NoRound(dm.SubitemVALOR.AsFloat*0.04,2)*(S.FieldByName('TEMP').AsInteger-1),2);
            S.FieldByName('EXTOTAL').AsFloat        :=S.FieldByName('EXEMOLUMENTOS').AsFloat+
                                                      S.FieldByName('EXFETJ').AsFloat+
                                                      S.FieldByName('EXFUNDPERJ').AsFloat+
                                                      S.FieldByName('EXFUNPERJ').AsFloat+
                                                      S.FieldByName('EXFUNARPEN').AsFloat;
        end;

      S.Post;
      S.ApplyUpdates(0);

      S.Next;
  end;
  GR.RemoverCampoDAC('FOLHAS','CERTIDOES',dm.conSISTEMA);
  GR.CriarCampoDAC('FOLHAS','CERTIDOES','INTEGER',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE CERTIDOES SET FOLHAS=TEMP',dm.conSISTEMA);
  GR.RemoverCampoDAC('TEMP','CERTIDOES',dm.conSISTEMA);
  S.Close;
  S.Free;
  PF.Aguarde(False);
end;

procedure TFPrincipal.FormCreate(Sender: TObject);
var
  F: TextFile;
begin
(*  Atualizacoes2013;
  Atualizacoes2014;
  Atualizacoes2015;

  {38} FSplash.G.AddProgress(1);
  {39} FSplash.Caption:='Atualizando Vers�o 35...';
  if dm.IdAtual('ID_VERSAO','N')=36 then
  begin
      GR.CriarCampoDBX('ISS','COD','FLOAT',dm.Central,TD);
      GR.AjustarPosicaoDBX('COD','ISS','11',dm.Central);
      GR.CriarCampoDBX('ISS','TITULOS','DOUBLE PRECISION',dm.Protesto,TD);
      GR.AjustarPosicaoDBX('TITULOS','FUNARPEN','23',dm.Protesto);
      GR.AjustarPosicaoDBX('TITULOS','PMCMV','24',dm.Protesto);
      GR.AjustarPosicaoDBX('TITULOS','ISS','25',dm.Protesto);
      GR.CriarCampoDBX('ISS','CERTIDOES','DOUBLE PRECISION',dm.Protesto,TD);
      GR.AjustarPosicaoDBX('CERTIDOES','ISS','31',dm.Protesto);
      GR.CriarCampoDBX('EXISS','CERTIDOES','DOUBLE PRECISION',dm.Protesto,TD);
      GR.AjustarPosicaoDBX('CERTIDOES','EXISS','44',dm.Protesto);
      GR.CriarCampoDBX('EXCOBRANCA','CERTIDOES','CHAR(2)',dm.Protesto,TD);
      GR.AjustarPosicaoDBX('CERTIDOES','EXCOBRANCA','39',dm.Protesto);
      GR.ExecutarSQLDBX('UPDATE CERTIDOES SET EXCOBRANCA=''CC'' WHERE EXCOBRANCA IS NULL AND EXEMOLUMENTOS>0',dm.Protesto);
      
      {PEDRO - 28/03/2016}
      CriarParametro(91,'FOLHA DE SEGURAN�A - CABE�ALHO','15,00');
      CriarParametro(92,'FOLHA DE SEGURAN�A - RODAP�','10,00');
      CriarParametro(93,'FOLHA DE SEGURAN�A - ESQUERDA','20,00');
      CriarParametro(94,'FOLHA DE SEGURAN�A - DIREITA','10,00');
      CriarParametro(95,'FOLHA DE SEGURAN�A - FOLHA','0');
      CriarParametro(96,'FOLHA DE SEGURAN�A - TIRA CABE�ALHO','0');
      {FIM}

      {PEDRO - 20/04/2016}
      GR.CriarCampoDBX('ESPECIE','PORTADORES','CHAR(1)',dm.Protesto,TD);
      GR.ExecutarSQLDBX('UPDATE PORTADORES SET ESPECIE=''N'' WHERE ESPECIE IS NULL',dm.Protesto);

      GR.FloatParaDoublePrecision('COD','EMOL',dm.Central);
      GR.FloatParaDoublePrecision('COD','FETJ',dm.Central);
      GR.FloatParaDoublePrecision('COD','FUND',dm.Central);
      GR.FloatParaDoublePrecision('COD','FUNP',dm.Central);
      GR.FloatParaDoublePrecision('COD','FUNA',dm.Central);
      GR.FloatParaDoublePrecision('COD','PMCMV',dm.Central);
      GR.FloatParaDoublePrecision('COD','ISS',dm.Central);
      GR.FloatParaDoublePrecision('COD','MUTUA',dm.Central);
      GR.FloatParaDoublePrecision('COD','ACOTERJ',dm.Central);
      GR.FloatParaDoublePrecision('COD','DISTRIB',dm.Central);
      GR.FloatParaDoublePrecision('COD','TOT',dm.Central);
      GR.FloatParaDoublePrecision('COD','MINIMO',dm.Central);
      GR.FloatParaDoublePrecision('COD','MAXIMO',dm.Central);
      GR.FloatParaDoublePrecision('CONCUS','VALOR',dm.Central);
      GR.FloatParaDoublePrecision('TABCUS','VALOR',dm.Central);
      GR.FloatParaDoublePrecision('TABCUS','TOTAL',dm.Central);

  end;
*)

  {SEMPRE VAI FAZER}
  if not FileExists(ExtractFilePath(Application.ExeName)+'Par.ini') then
  begin
    AssignFile(F,ExtractFilePath(Application.ExeName)+'Par.ini');
    Rewrite(F);
    Writeln(F,'[IMPRESSORAS]');
    Writeln(F,'ETIQUETA=C:\TesteEtiqueta.txt');
    Writeln(F,'RECIBO=C:\TesteRecibo.txt');
    Writeln(F,'');
    Writeln(F,'[ESPACOFINAL]');
    Writeln(F,'ETIQUETA=1');
    Writeln(F,'RECIBO=1');
    CloseFile(F);
  end;

  sbCopiar.Visible        :=FileExists('C:\PROGRAMM');
  
  dm.Serventia.Open;

  dm.vCalc:=True;

  dm.Tabela.Close;
  dm.Tabela.Params[0].AsInteger:=StrToInt(dm.ValorParametro(77));
  dm.Tabela.Open;

  ckAssistente.Checked:=dm.ValorParametro(49)='S';
  ckAssistenteClick(Sender);

  P1.BevelInner:=bvNone;
  P1.BevelOuter:=bvNone;

  {37} FSplash.G.AddProgress(1);

  dm.AtualizarFeriados;
end;

procedure TFPrincipal.lb7Click(Sender: TObject);
begin
  GR.CriarForm(TFXML2014,FXML2014);
end;

procedure TFPrincipal.BuscarAleatoriosCancelamentos;
var
  S: TFDQuery;
  vAleatorio: String;
begin
  S:=TFDQuery.Create(Nil);
  S.Connection := dm.conSISTEMA;
  S.SQL.Text:='SELECT ID_CERTIDAO,SELO,ALEATORIO FROM CERTIDOES WHERE SELO<>'''+''+''' AND ALEATORIO IS NULL '+
                         'AND TIPO_CERTIDAO='''+'X'+'''AND SELO<>'''+''+''' AND DT_CERTIDAO>='+
                         QuotedStr(FormatDateTime('mm/dd/yyyy',10/03/2014));
  S.Open;
  while not S.Eof do
  begin
      if S.FieldByName('SELO').AsString<>'' then
      begin
          S.Edit;
          vAleatorio:=Gdm.BuscarAleatorio(S.FieldByName('SELO').AsString,False);
          if vAleatorio<>'' then
          S.FieldByName('ALEATORIO').AsString:=vAleatorio;
          S.Post;
          S.ApplyUpdates(0);
      end;
      S.Next;
  end;
  S.Close;
  S.SQL.Text:='SELECT ID_ATO,SELO_PAGAMENTO,ALEATORIO_SOLUCAO FROM TITULOS WHERE '+
              'STATUS='''+'CANCELADO'+''' AND ALEATORIO_SOLUCAO IS NULL AND '+
              'DT_PAGAMENTO>='+QuotedStr('03/10/2014');
  S.Open;
  while not S.Eof do
  begin
      if S.FieldByName('SELO_PAGAMENTO').AsString<>'' then
      begin
          S.Edit;
          vAleatorio:=Gdm.BuscarAleatorio(S.FieldByName('SELO_PAGAMENTO').AsString,False);
          if vAleatorio<>'' then
          S.FieldByName('ALEATORIO_SOLUCAO').AsString:=vAleatorio;
          S.Post;
          S.ApplyUpdates(0);
      end;
      S.Next;
  end;

  S.Close;
  S.Free;
end;

procedure TFPrincipal.I1Click(Sender: TObject);
begin
  GR.CriarForm(TFConsVerbal,FConsVerbal);
end;

procedure TFPrincipal.R1Click(Sender: TObject);
begin
  GR.CriarForm(TFReciboPagamentoConvenio,FReciboPagamentoConvenio);
end;

procedure TFPrincipal.AumentarDescricaoTipos;
begin
  GR.ExecutarSQLDAC('UPDATE RDB$FIELDS SET RDB$FIELD_LENGTH=100,RDB$CHARACTER_LENGTH=100 WHERE RDB$FIELD_NAME='+
                   QuotedStr(GR.SourceCampoDAC('TIPOS','DESCRICAO',dm.conSISTEMA)),dm.conSISTEMA);
end;

procedure TFPrincipal.R2Click(Sender: TObject);
begin
  GR.CriarForm(TFReciboApontamento,FReciboApontamento);
end;

procedure TFPrincipal.AjustarTabeladeRemessas;
var
  S1,S2: TFDQuery;
begin
  {SE ESSE CAMPO N�O EXISTE N�O TEM MAIS COMO AJUSTAR A TABELA}
  {O CART�RIO TER� QUE VER A PR�XIMA REMESSA DO DIA E COLOCAR NA M�O}
  if not GR.CampoExisteDAC('ID_PORTADOR','REMESSAS',dm.conSISTEMA) then Exit;

  S1:=TFDQuery.Create(Nil);
  S1.Connection:=dm.conSISTEMA;
  S1.SQL.Text:='SELECT * FROM REMESSAS WHERE ID_REMESSA=-1';
  S1.Open;

  S2:=TFDQuery.Create(Nil);
  S2.Connection:=dm.conSISTEMA;
  S2.SQL.Text:='SELECT DATA,MAX(NUMERO) AS NUMERO FROM REMESSAS GROUP BY DATA';
  S2.Open;
  S2.First;
  while not S2.Eof do
  begin
      S1.Append;
      S1.FieldByName('ID_REMESSA').AsInteger  :=dm.IdAtual('ID_REMESSA','S');
      S1.FieldByName('ID_PORTADOR').AsInteger :=-1;
      S1.FieldByName('DATA').AsDateTime       :=S2.FieldByName('DATA').AsDateTime;
      S1.FieldByName('NUMERO').AsInteger      :=S2.FieldByName('NUMERO').AsInteger;
      S1.Post;
      S1.ApplyUpdates(0);
      S2.Next;
  end;
  S1.Free;
  S2.Free;
  GR.ExecutarSQLDAC('DELETE FROM REMESSAS WHERE ID_PORTADOR<>-1',dm.conSISTEMA);
  GR.RemoverCampoDAC('ID_PORTADOR','REMESSAS',dm.conSISTEMA);
  GR.RemoverCampoDAC('TIPO','REMESSAS',dm.conSISTEMA);
end;

procedure TFPrincipal.PuxarValoresApontamentosCanceladosConvenio;

  function EmolumentosConvenio(Codigo: Integer; Valor: Double): Double;
  begin
    Result:=0;
    dm.qryFaixa.Close;
    dm.qryFaixa.ParamByName('C').AsString:='S';
    dm.qryFaixa.ParamByName('VALOR1').AsFloat:=Valor;
    dm.qryFaixa.ParamByName('VALOR2').AsFloat:=Valor;
    dm.qryFaixa.ParamByName('OCULTO1').AsString:='N';
    dm.qryFaixa.ParamByName('OCULTO2').AsString:='N';
    dm.qryFaixa.Open;

    dm.Itens.Close;
    dm.Itens.Params[0].AsInteger:=dm.qryFaixaCOD.AsInteger;
    dm.Itens.Open;
    dm.Itens.First;

    while not dm.Itens.Eof do
    begin
        Result:=Result+dm.ItensVALOR.AsFloat*dm.ItensQTD.AsInteger;
        dm.Itens.Next;
    end;
  end;

var
  S1,S2: TFDQuery;
begin
  S1:=TFDQuery.Create(Nil);
  S2:=TFDQuery.Create(Nil);
  S1.Connection:=Gdm.FDGERENCIAL;
  S2.Connection:=dm.conSISTEMA;
  S1.SQL.Text:='SELECT ID_ATO,EMOLUMENTOS,FETJ,FUNDPERJ,FUNPERJ,FUNARPEN,PMCMV,MUTUA,'+
                          'ACOTERJ,DISTRIBUICAO,PRENOTACAO,TOTAL,ID_REFERENCIA FROM SELOS_CCT WHERE '+
                          'CONVENIO='''+'S'+''' AND CODIGO=4038 AND NATUREZA='''+
                          'CANCELAMENTO'+'''';

  S1.Open;
  while not S1.Eof do
  begin
      Application.ProcessMessages;
      PF.Aguarde(True,'Aguarde: '+IntToStr(S1.RecNo)+'-'+IntToStr(S1.RecordCount));
      S2.Close;
      S2.SQL.Text:='SELECT ID_ATO,SALDO_TITULO,VALOR_TITULO,CODIGO FROM TITULOS WHERE ID_ATO='+S1.FieldByName('ID_REFERENCIA').AsString;
      S2.Open;
      S1.Edit;
      S1.FieldByName('PRENOTACAO').AsFloat:=EmolumentosConvenio(S2.FieldByName('CODIGO').AsInteger,
                                                                GR.iif(S2.FieldByName('SALDO_TITULO').AsFloat<>0,
                                                                       S2.FieldByName('SALDO_TITULO').AsFloat,
                                                                       S2.FieldByName('VALOR_TITULO').AsFloat));
      S1.FieldByName('TOTAL').AsFloat:=S1.FieldByName('EMOLUMENTOS').AsFloat+
                                       S1.FieldByName('FETJ').AsFloat+
                                       S1.FieldByName('FUNDPERJ').AsFloat+
                                       S1.FieldByName('FUNPERJ').AsFloat+
                                       S1.FieldByName('FUNARPEN').AsFloat+
                                       S1.FieldByName('PMCMV').AsFloat+
                                       S1.FieldByName('MUTUA').AsFloat+
                                       S1.FieldByName('ACOTERJ').AsFloat+
                                       S1.FieldByName('DISTRIBUICAO').AsFloat+
                                       S1.FieldByName('PRENOTACAO').AsFloat;
      S1.Post;
      S1.ApplyUpdates(0);
      S1.Next;
  end;
  S1.Free;
  S2.Free;
  PF.Aguarde(False);
end;

procedure TFPrincipal.lbAtualizarTabelaClick(Sender: TObject);
begin
  MenuAtualizarEmolumentosClick(Sender);
end;

procedure TFPrincipal.AtualizarEmolumentos;
var
  vAno: Integer;
  vEmolumentos,vComuns: Double;
begin
  try
    PF.Aguarde(True);

    vAno:=2021;
    GR.ExecutarSQLDAC('DELETE FROM CONCUS WHERE ANO='+IntToStr(vAno),dm.conCENTRAL);

    dm.qrySuporte.Close;
    dm.qrySuporte.Connection:=dm.conCENTRAL;
    dm.qrySuporte.SQL.Clear;
    dm.qrySuporte.SQL.Add('UPDATE TABCUS SET VALOR=:V,TOTAL=CAST(QTD AS INTEGER) *:V WHERE TAB=:T AND ITEM=:I AND SUB=:S');

    dm.Tabela.Close;
    dm.Tabela.Params[0].AsInteger:=-1;
    dm.Tabela.Open;

    Gdm.fParametros.Close;
    Gdm.fParametros.Open;

    dm.AtualizarParametro(26,FloatToStrF(Gdm.fParametrosMUTUA.AsFloat,ffNumber,7,2));
    dm.AtualizarParametro(27,FloatToStrF(Gdm.fParametrosACOTERJ.AsFloat,ffNumber,7,2));
    dm.AtualizarParametro(28,FloatToStrF(Gdm.fParametrosDISTRIBUICAO1.AsFloat,ffNumber,7,2));

    {ATUALIZANDO CONCUS - TABELAS 16 E 24}
    Gdm.fCGJ.Close;
    Gdm.fCGJ.ParamByName('ATRIBUICAO').AsInteger:=4;
    Gdm.fCGJ.Open;
    Gdm.fCGJ.Last;
    Gdm.fCGJ.First;
    while not Gdm.fCGJ.Eof do
    begin
        Application.ProcessMessages;

        PF.Aguarde(True,'Aguarde (1/3): '+IntToStr(Gdm.fCGJ.RecNo)+' de '+IntToStr(Gdm.fCGJ.RecordCount));

        dm.Tabela.Append;
        dm.TabelaORDEM.AsInteger  :=Gdm.fCGJORDEM.AsInteger;
        dm.TabelaANO.AsInteger    :=vAno;
        dm.TabelaVAI.AsString     :='S';
        dm.TabelaTAB.AsString     :=Gdm.fCGJTABELA.AsString;
        dm.TabelaITEM.AsString    :=Gdm.fCGJITEM.AsString;
        dm.TabelaSUB.AsString     :=Gdm.fCGJSUBITEM.AsString;
        dm.TabelaDESCR.AsString   :=Gdm.fCGJDESCRICAO.AsString;
        dm.TabelaVALOR.AsFloat    :=Gdm.fCGJVALOR.AsFloat;
        dm.TabelaTEXTO.AsString   :=Gdm.fCGJDESCRICAO.AsString;
        dm.Tabela.Post;
        dm.Tabela.ApplyUpdates(0);

        dm.qrySuporte.Close;
        dm.qrySuporte.ParamByName('V').Value:=Gdm.fCGJVALOR.Value;
        dm.qrySuporte.ParamByName('T').Value:=Gdm.fCGJTABELA.Value;
        dm.qrySuporte.ParamByName('I').Value:=Gdm.fCGJITEM.Value;
        dm.qrySuporte.ParamByName('S').Value:=Gdm.fCGJSUBITEM.Value;
        dm.qrySuporte.ExecSQL();

        Gdm.fCGJ.Next;
    end;

    {RECALCULANDO A TABELA COD}
    dm.Codigos.Close;
    dm.Codigos.Params.ParamByName('OCULTO1').AsString:='N';
    dm.Codigos.Params.ParamByName('OCULTO2').AsString:='N';
    dm.Codigos.Open;
    dm.Codigos.Last;
    dm.Codigos.First;
    while not dm.Codigos.Eof do
    begin
        Application.ProcessMessages;
        PF.Aguarde(True,'Aguarde (2/3): '+IntToStr(dm.Codigos.RecNo)+' de '+IntToStr(Gdm.fCGJ.RecordCount));

        dm.Itens.Close;
        dm.Itens.Params[0].Value:=dm.CodigosCOD.Value;
        dm.Itens.Open;
        dm.Itens.First;
        vEmolumentos:=0;
        vComuns:=0;
        while not dm.Itens.Eof do
        begin
            dm.Itens.Edit;
            if Trim(dm.ItensQTD.AsString)='' then
              dm.ItensQTD.AsString:='1'
                else dm.ItensQTD.AsString:=GR.PegarNumeroTexto(Trim(dm.ItensQTD.AsString));

            if dm.ItensTAB.AsString='16' then
            vComuns:=vComuns+(dm.ItensVALOR.AsFloat*dm.ItensQTD.AsInteger);
            vEmolumentos:=vEmolumentos+(dm.ItensVALOR.AsFloat*dm.ItensQTD.AsInteger);

            dm.Itens.Post;
            dm.Itens.ApplyUpdates(0);

            dm.Itens.Next;
        end;
        dm.Codigos.Edit;

        if dm.CodigosMUTUA.AsFloat>0   then dm.CodigosMUTUA.AsFloat  :=StrToFloat(dm.ValorParametro(26));
        if dm.CodigosACOTERJ.AsFloat>0 then dm.CodigosACOTERJ.AsFloat:=StrToFloat(dm.ValorParametro(27));
        if dm.CodigosDISTRIB.AsFloat>0 then dm.CodigosDISTRIB.AsFloat:=StrToFloat(dm.ValorParametro(28));

        dm.CodigosEMOL.Value:=vEmolumentos;
        dm.CodigosFETJ.Value:=GR.NoRound(vEmolumentos*0.2,2);
        dm.CodigosFUND.Value:=GR.NoRound(vEmolumentos*0.05,2);
        dm.CodigosFUNP.Value:=GR.NoRound(vEmolumentos*0.05,2);
        dm.CodigosFUNA.Value:=GR.NoRound(vEmolumentos*0.04,2);

        if dm.CodigosPMCMV.AsFloat<>0 then
        dm.CodigosPMCMV.AsFloat:=GR.NoRound((vEmolumentos-vComuns)*0.02,2);

        dm.CodigosTOT.Value :=dm.CodigosEMOL.AsFloat+
                              dm.CodigosFETJ.AsFloat+
                              dm.CodigosFUND.AsFloat+
                              dm.CodigosFUNP.AsFloat+
                              dm.CodigosFUNA.AsFloat+
                              dm.CodigosPMCMV.AsFloat+
                              dm.CodigosMUTUA.AsFloat+
                              dm.CodigosACOTERJ.AsFloat+
                              dm.CodigosDISTRIB.AsFloat;
        dm.Codigos.Post;
        dm.Codigos.ApplyUpdates(0);
        dm.Codigos.Next;
    end;

    {ATUALIZANDO AS FAIXAS DE M�NIMO E M�XIMO}
    Gdm.AbrirAdicional('SELECT * FROM ADICIONAL WHERE ATRIBUICAO = 4');
    Gdm.fAdicional.Last;
    Gdm.fAdicional.First;
    while not Gdm.fAdicional.Eof do
    begin
        Application.ProcessMessages;
        PF.Aguarde(True,'Aguarde (3/3): '+IntToStr(Gdm.fAdicional.RecNo)+' de '+IntToStr(Gdm.fAdicional.RecordCount));

        dm.Codigos.Locate('COD',Gdm.fAdicionalCODIGO.AsInteger,[]);
        if not (dm.Codigos.IsEmpty) then
        begin
            dm.Codigos.Edit;
            dm.CodigosATOS.Value  :=Gdm.fAdicionalDESCRICAO.Value;
            dm.CodigosMINIMO.Value:=Gdm.fAdicionalMINIMO.Value;
            dm.CodigosMAXIMO.Value:=Gdm.fAdicionalMAXIMO.Value;
            dm.Codigos.Post;
            dm.Codigos.ApplyUpdates(0);
        end;
        Gdm.fAdicional.Next;
    end;

    PF.Aguarde(True,'Finalizando...');

    try
      Gdm.SalvarServentiaOnline(dm.ServentiaCODIGO.AsInteger,dm.ServentiaDESCRICAO.AsString,Gdm.cUsuariosNOME.AsString,'PROTESTO');
    except
    end;

    dm.AtualizarParametro(77,IntToStr(vAno));
    Gdm.GerarLogGerencial(Self,'PROTESTO','PROTESTO: TABELA DE EMOLUMENTOS ATUALIZADA COM SUCESSO',Gdm.cUsuariosNOME.AsString);
    PF.Aguarde(False);
    GR.Aviso('TABELA ATUALIZADA COM SUCESSO!'#13#13+'O SISTEMA SER� FECHADO!!');
    Application.Terminate;
  except
    on E:Exception do
    begin
        PF.Aguarde(False);
        Gdm.GerarLogGerencial(Self,'PROTESTO','PROTESTO: N�O FOI POSS�VEL ATUALIZAR A TABELA DE EMOLUMENTOS 2021',Gdm.cUsuariosNOME.AsString);
        GR.Aviso('Erro: '+E.Message);
    end;
  end;
end;

procedure TFPrincipal.Configuraes1Click(Sender: TObject);
begin
  MenuExportar.Visible:=dm.ServentiaCODIGO.AsInteger=2354;
end;

procedure TFPrincipal.MenuExportarClick(Sender: TObject);
var
  F: TextFile;
  vArquivo: String;
  vData: TDate;
  S: TFDQuery;
begin
  vData:=IncYear(Now,-5);

  if GR.Pergunta('CONFIRMA A EXPORTA��O NO PER�ODO DE: '+GR.FormatarData(vData)+' AT� '+GR.FormatarData(Now)) then
  begin
      vArquivo:=GR.DirExe+'CPFCNPJ_SITE_'+FormatDateTime('dd-mm-yyyy',vData)+'_ATE_'+FormatDateTime('dd-mm-yyyy',Now)+'.txt';
      AssignFile(F,vArquivo);
      Rewrite(F);

      S:=TFDQuery.Create(Nil);
      S.Connection:=dm.conSISTEMA;
      S.SQL.Text:='SELECT CPF_CNPJ_DEVEDOR FROM TITULOS WHERE STATUS='''+'PROTESTADO'+'''AND '+
                             'DT_REGISTRO>=:DATA AND CPF_CNPJ_DEVEDOR<>'''+''+''' ORDER BY CPF_CNPJ_DEVEDOR';
      S.ParamByName('DATA').AsDate:=vData;
      PF.Aguarde(True);
      S.Open;
      while not S.Eof do
      begin
          Application.ProcessMessages;
          PF.Aguarde(True,'Aguarde: '+IntToStr(S.RecNo)+'/'+IntToStr(S.RecordCount));
          Writeln(F,S.FieldByName('CPF_CNPJ_DEVEDOR').AsString);
          S.Next;
      end;
      CloseFile(F);
      S.Close;
      S.Free;
      PF.Aguarde(False);
      GR.Aviso('ARQUIVO GERADO COM SUCESSO!');
  end;
end;

procedure TFPrincipal.ckPainelClick(Sender: TObject);
begin
  if not ckPainel.Checked then
  begin
      dm.AtualizarParametro(87,'N');
      FQuantidadeSelo.Free;
  end
  else
  begin
      dm.AtualizarParametro(87,'S');
      Application.CreateForm(TFQuantidadeSelo,FQuantidadeSelo);
      GR.FormPos(FQuantidadeSelo,3,2);
      //FQuantidadeSelo.Left:=ckPainel.Left; //Mouse.CursorPos.X-100;
      //FQuantidadeSelo.Top :=ckPainel.Top; //Mouse.CursorPos.Y+20;
      FQuantidadeSelo.Show;
  end;
end;

procedure TFPrincipal.AtualizarSiglasTipos;
begin
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''CBI'' WHERE CODIGO=1',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''CT''  WHERE CODIGO=18',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''DV''  WHERE CODIGO=25',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''EC''  WHERE CODIGO=26',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''DD''  WHERE CODIGO=40',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''CAD'' WHERE CODIGO=41',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''CAC'' WHERE CODIGO=42',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''CAM'' WHERE CODIGO=43',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''CL''  WHERE CODIGO=44',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''CLO'' WHERE CODIGO=45',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''CCR'' WHERE CODIGO=46',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''FPS'' WHERE CODIGO=47',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''FVM'' WHERE CODIGO=48',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''ND''  WHERE CODIGO=49',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''OS''  WHERE CODIGO=50',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''RPS'' WHERE CODIGO=51',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''PSI'' WHERE CODIGO=52',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''TAJ'' WHERE CODIGO=53',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''CSE'' WHERE CODIGO=54',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''CCJ'' WHERE CODIGO=55',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''ACI'' WHERE CODIGO=56',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''CF''  WHERE CODIGO=57',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''CJU'' WHERE CODIGO=58',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''TJD'' WHERE CODIGO=59',dm.conSISTEMA);
  GR.ExecutarSQLDAC('UPDATE TIPOS SET SIGLA=''CDT'' WHERE CODIGO=60',dm.conSISTEMA);
end;

procedure TFPrincipal.MenuAtualizarEmolumentosClick(Sender: TObject);
begin
  if GR.ValorCampoDAC('PARAMETROS','ANO_TABELA','I',Gdm.FDGerencial)<2021 then
  begin
      GR.Aviso('ATUALIZE ANTES O "TOTAL GERENCIAL"!');
      Exit;
  end;

  if not GR.Pergunta('ATUALIZAR TABELA DE EMOLUMENTOS 2021') then Exit;
  if not GR.Pergunta('CERTIFIQUE-SE DE QUE OS ATOS DE 2020 J� FORAM CONCLU�DOS!'+#13#13+'CONTINUAR') then Exit;

  {GR.Aviso('CONFIRME SEU LOGIN E SENHA');
  if not Gdm.ValidarUsuario(dm.conSISTEMA,'SELECT NOME,SENHA FROM ESCREVENTES',dm.vNomeCompleto,True) then
  Exit;  }

  AtualizarEmolumentos;
end;

procedure TFPrincipal.sbISSClick(Sender: TObject);
begin
  Gdm.vOkay4:=False;
  GR.Aviso('CONFIRME SEU LOGIN E SENHA');
  if not Gdm.ValidarUsuario(dm.conSISTEMA,'SELECT NOME,SENHA FROM ESCREVENTES',dm.vNomeCompleto,True) then
  Exit;

  GR.CriarForm(TFAliquotaISS,FAliquotaISS);
  if Gdm.vOkay4 then
  begin
      if GR.Pergunta('AS TABELAS DE EMOLUMENTOS SER�O ATUALIZADAS!'+#13#13+'CONFIRMA') then
      begin
          GR.Processando(True);
          dm.Codigos.Close;
          dm.Codigos.Params.ParamByName('OCULTO1').AsString:='N';
          dm.Codigos.Params.ParamByName('OCULTO2').AsString:='N';
          dm.Codigos.Open;
          while not dm.Codigos.Eof do
          begin
              Application.ProcessMessages;
              dm.Codigos.Edit;
              dm.CodigosISS.AsFloat:=GR.NoRound(dm.CodigosEMOL.AsFloat*Gdm.vAliquotaISS,2);
              dm.CodigosTOT.AsFloat:=dm.CodigosEMOL.AsFloat+
                                     dm.CodigosFETJ.AsFloat+
                                     dm.CodigosFUND.AsFloat+
                                     dm.CodigosFUNP.AsFloat+
                                     dm.CodigosFUNA.AsFloat+
                                     dm.CodigosPMCMV.AsFloat+
                                     dm.CodigosISS.AsFloat+
                                     dm.CodigosMUTUA.AsFloat+
                                     dm.CodigosACOTERJ.AsFloat+
                                     dm.CodigosDISTRIB.AsFloat;
              dm.Codigos.Post;
              dm.Codigos.ApplyUpdates(0);
              dm.Codigos.Next;
          end;

          GR.Processando(False);
          Gdm.GerarLogGerencial(Self,'PROTESTO','CONFIRMOU AL�QUOTA PROTESTO - ISS: '+FloatToStrF(Gdm.vAliquotaISS,ffNumber,7,2),Gdm.vUsuario);
          GR.Aviso('TABELAS ATUALIZADAS COM SUCESSO!'+#13#13+'POR FAVOR, REINICIE O SISTEMA.');
          Application.Terminate;
      end;
  end
  else
  begin
      GR.Processando(False);
      GR.Aviso('AS TABELAS N�O FORAM ATUALIZADAS!');
  end;
end;

procedure TFPrincipal.Atualizacoes2015;
begin
  {36} FSplash.G.AddProgress(1);
  {37} FSplash.Caption:='Atualizando Vers�o 34...';
  if dm.IdAtual('ID_VERSAO','N')=35 then
  begin
      GR.CriarCampoDAC('LETRA','TITULOS','VARCHAR(1)',dm.conSISTEMA);
      GR.AumentarTamanhoCampoDAC('MOTIVOS','DESCRICAO','400',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE IDS SET VALOR='+
                        IntToStr(GR.MaxCampoDAC('MOTIVOS','ID_MOTIVO',dm.conSISTEMA)+1)+
                        ' WHERE CAMPO='+QuotedStr('ID_MOTIVO'),
                        dm.conSISTEMA);
      CriarParametro(87,'PAINEL SELO','N');
      CriarParametro(88,'NUMERAR FOLHAS AO GERAR O LIVRO','N');
      CriarParametro(89,'NOME DO JUIZ (SUSTA��O)','');
      dm.AtualizarParametro(87,'N');
      AtualizarSiglasTipos;
      GR.CriarCampoDAC('FORCA_LEI','PORTADORES','CHAR(1)',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE PORTADORES SET FORCA_LEI=''S'' WHERE FORCA_LEI IS NULL AND CODIGO=''582''',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE PORTADORES SET FORCA_LEI=''S'' WHERE FORCA_LEI IS NULL AND CODIGO=''701''',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE PORTADORES SET FORCA_LEI=''S'' WHERE FORCA_LEI IS NULL AND CODIGO=''911''',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE PORTADORES SET FORCA_LEI=''S'' WHERE FORCA_LEI IS NULL AND CODIGO=''774''',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE PORTADORES SET FORCA_LEI=''S'' WHERE FORCA_LEI IS NULL AND CODIGO=''A64''',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE PORTADORES SET FORCA_LEI=''S'' WHERE FORCA_LEI IS NULL AND CODIGO=''A40''',dm.conSISTEMA);
      GR.ExecutarSQLDAC('UPDATE PORTADORES SET FORCA_LEI=''N'' WHERE FORCA_LEI IS NULL',dm.conSISTEMA);
      GR.CriarArquivoTXTINI(GR.DirExe+'Retificacao.txt','Procedo na data de hoje, a presente AVERBACAO DE RETIFICACAO'+#13+
                                                        'em razao de erro material, conforme art. 1003 da CNCGJ - RJ.'+#13+
                                                        'Na data de [DATAPROTOCOLO] foi apontado um titulo para protesto'+#13+
                                                        'que recebeu o numero de protocolo [PROTOCOLO] entretanto, h� incorrecao'+#13+
                                                        'quanto'+#13+
                                                        '[CIDADE], [DATAATUAL]. [NOMETABELIAO], [TITULOTABELIAO], Matricula [MATRICULATABELIAO].');
      CriarParametro(90,'ADICIONAR ARQUIVAMENTO NA CERTID�O DE INTEIRO TEOR','N');
      dm.IdAtual('ID_VERSAO', 'S');
  end;
end;

procedure TFPrincipal.ServentiasAgregadas1Click(Sender: TObject);
begin
  GR.CriarForm(TFServentiaAgregada,FServentiaAgregada);
end;

procedure TFPrincipal.ImportarArquivoCRA1Click(Sender: TObject);
begin
  dm.ServentiaAgregada.Close;
  dm.ServentiaAgregada.Open;
  dm.ServentiaAgregada.Filtered := False;
  dm.ServentiaAgregada.Filter   := 'FLG_ATIVA = ' + QuotedStr('S');
  dm.ServentiaAgregada.Filtered := True;

  if dm.ServentiaAgregada.IsEmpty then
  begin
    GR.Aviso('FALTA CADASTRAR AS SERVENTIAS AGREGADAS!');
    Exit;
  end
  else
    GR.CriarForm(TFImportarTxt, FImportarTxt);
end;

procedure TFPrincipal.ImportarRetornoServentias1Click(Sender: TObject);
begin
  dm.ServentiaAgregada.Close;
  dm.ServentiaAgregada.Open; 
  dm.ServentiaAgregada.Filtered := False;
  dm.ServentiaAgregada.Filter   := 'FLG_ATIVA = ' + QuotedStr('S');
  dm.ServentiaAgregada.Filtered := True;

  if dm.ServentiaAgregada.IsEmpty then
  begin
    GR.Aviso('FALTA CADASTRAR AS SERVENTIAS AGREGADAS!');
    Exit;
  end
  else
    GR.CriarForm(TFImportarTxtRetornoServentias, FImportarTxtRetornoServentias);
end;

procedure TFPrincipal.lb1Click(Sender: TObject);
var
  pnt: TPoint;
begin
  if GetCursorPos(pnt) then
    pmImportarArquivo.Popup(pnt.X, pnt.Y);
end;

procedure TFPrincipal.sbImportarClick(Sender: TObject);
var
  pnt: TPoint;
begin
  if GetCursorPos(pnt) then
    pmImportarArquivo.Popup(pnt.X, pnt.Y);
end;

procedure TFPrincipal.pmiExportarArquivoSAClick(Sender: TObject);
begin
  ExportarRemessaServentias1.Click;
end;

procedure TFPrincipal.pmiExportarRetornoCRAClick(Sender: TObject);
begin
  Distribuidor1.Click;
end;

procedure TFPrincipal.pmiImportarArquivoCRAClick(Sender: TObject);
begin
  ImportarArquivoCRA1.Click;
end;

procedure TFPrincipal.pmiImportarRetornoServentiasClick(Sender: TObject);
begin
  ImportarRetornoServentias1.Click;
end;

procedure TFPrincipal.ExportarRemessaServentias1Click(Sender: TObject);
begin
  GR.CriarForm(TFExportarTxtRemBalcaoServ, FExportarTxtRemBalcaoServ);
end;

end.
