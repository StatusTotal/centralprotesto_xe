object FEscreventes: TFEscreventes
  Left = 305
  Top = 209
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Escreventes'
  ClientHeight = 343
  ClientWidth = 672
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 14
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 672
    Height = 40
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object btIncluir: TsBitBtn
      Left = 12
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Incluir'
      TabOrder = 0
      OnClick = btIncluirClick
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 6
      Images = dm.Imagens
    end
    object btAlterar: TsBitBtn
      Left = 92
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Alterar'
      TabOrder = 1
      OnClick = btAlterarClick
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 8
      Images = dm.Imagens
    end
    object btExcluir: TsBitBtn
      Left = 172
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Excluir'
      TabOrder = 2
      OnClick = btExcluirClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333333333333333333FFF33FF333FFF339993370733
        999333777FF37FF377733339993000399933333777F777F77733333399970799
        93333333777F7377733333333999399933333333377737773333333333990993
        3333333333737F73333333333331013333333333333777FF3333333333910193
        333333333337773FF3333333399000993333333337377737FF33333399900099
        93333333773777377FF333399930003999333337773777F777FF339993370733
        9993337773337333777333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      SkinData.SkinSection = 'BUTTON'
    end
    object btSalvar: TsBitBtn
      Left = 268
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Salvar'
      TabOrder = 3
      OnClick = btSalvarClick
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 3
      Images = dm.Imagens
    end
    object btCancelar: TsBitBtn
      Left = 348
      Top = 8
      Width = 87
      Height = 25
      Caption = 'Cancelar'
      TabOrder = 4
      OnClick = btCancelarClick
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 5
      Images = dm.Imagens
    end
  end
  object GridPorta: TwwDBGrid
    Left = 0
    Top = 40
    Width = 672
    Height = 195
    Selected.Strings = (
      'NOME'#9'47'#9'Nome'#9'F'
      'QUALIFICACAO'#9'43'#9'Qualifica'#231#227'o'#9'F')
    IniAttributes.Delimiter = ';;'
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    BorderStyle = bsNone
    DataSource = dsEscreventes
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Arial'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
  end
  object P2: TsPanel
    Left = 0
    Top = 235
    Width = 672
    Height = 108
    Align = alBottom
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object ImAviso1: TImage
      Left = 647
      Top = 27
      Width = 16
      Height = 16
      Hint = 'DOCUMENTO INV'#193'LIDO!'
      AutoSize = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        000000000000FFFFFF0000000000000000000000000000000000000000000000
        0000AA640070AA64007000000000000000000000000000000000000000000000
        0000FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
        0000AA6400CFAA6400CF00000000000000000000000000000000000000000000
        0000FFFFFF00FFFFFF000000000000000000000000000000000000000000AA64
        0020BF8B3DFFBF8B3DFFAA640020000000000000000000000000000000000000
        0000FFFFFF00FFFFFF000000000000000000000000000000000000000000AA64
        0060DABB8DFFDABB8DFFAA640060000000000000000000000000000000000000
        0000FFFFFF00FFFFFF00000000000000000000000000AA640060AA6400AFB577
        1FFFF4ECDDFFF4ECDDFFB5771FFFAA6400AFAA64006000000000000000000000
        0000FFFFFF00FFFFFF0000000000AA640010AA6400BFCA9E5DFFF4ECD8FFFFFF
        F8FFFFFFFFFFFFFFFEFFFFFFF7FFF4ECD8FFCA9E5DFFAA6400BFAA6400100000
        0000FFFFFF00FFFFFF00AA640010AA6400CFDFC59BFFFFFEF7FFFFFCF5FFFFFB
        F6FF010BBFFF010BBFFFFFFBF6FFFFFCF5FFFFFEF7FFDFC59BFFAA6400CFAA64
        0010FFFFFF00FFFFFF00AA640080DABB8CFFFFFAF0FFFFF6EAFFFFF6EAFFFFF6
        EAFF0021C2FF0021C2FFFFF6EAFFFFF6EAFFFFF6EAFFFFFBF2FFDABB8CFFAA64
        0080FFFFFF00FFFFFF00B57921EFFFFBF3FFFFF1DEFFFFF1DEFFFFF1DEFFFFF1
        DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFF1DEFFFFFDF7FFB579
        21EFFFFFFF00FFFFFF00C5944FFFFFF3E3FFFFEDD5FFFFEDD5FFFFEDD5FFFFED
        D5FF0007DAFF0007DAFFFFEDD5FFFFEDD5FFFFEDD5FFFFEDD5FFFFF6E8FFC594
        4FFFFFFFFF00FFFFFF00C5944FFFFFF0DCFFFFEACDFFFFEACDFFFFEACDFFFFEA
        CDFF0012E5FF0012E5FFFFEACDFFFFEACDFFFFEACDFFFFEACDFFFFF4E5FFC594
        4FFFFFFFFF00FFFFFF00B57922EFFFF5E6FFFFE7C8FFFFE7C8FFFFE7C8FFFFE7
        C8FF001DD5FF001DD5FFFFE7C8FFFFE7C8FFFFE7C8FFFFE7C8FFFFF6E9FFB579
        22EFFFFFFF00FFFFFF00AA640080DABA8BFFFFEBD0FFFFE6C5FFFFE6C5FFFFE6
        C5FF0029C2FF0029C2FFFFE6C5FFFFE6C5FFFFE6C5FFFFEBD0FFDABB8FFFAA64
        0080FFFFFF00FFFFFF00AA640010B17014CFEAD7BBFFFFEED7FFFFE6C5FFFFE6
        C5FF0024AFFF0024AFFFFFE6C5FFFFE6C5FFFFEFDAFFEAD7BBFFB17014CFAA64
        0010FFFFFF00FFFFFF0000000000AA640010AA6400CFCFA870FFF4E9D8FFFFF4
        E6FFFFF3E2FFFFF3E2FFFFF4E6FFF4E9D8FFCFA870FFAA6400CFAA6400100000
        0000FFFFFF00FFFFFF00000000000000000000000000AA640060AA6400CFBA81
        30FFBF8B40FFBF8B40FFBA8130FFAA6400CFAA64006000000000000000000000
        0000FFFFFF00}
      ShowHint = True
      Transparent = True
      Visible = False
    end
    object ImOk1: TImage
      Left = 647
      Top = 27
      Width = 16
      Height = 16
      Hint = 'DOCUMENTO OK!'
      AutoSize = True
      ParentShowHint = False
      Picture.Data = {
        0B54504E474772617068696336040000424D3604000000000000360000002800
        0000100000001000000001002000000000000004000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000939393F9939393F9939393F9939393F99393
        93F9939393F9939393F9939393F9939393F9939393F9939393F9939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF939393F90000
        0000000000000000000000000000939393F900750CF500750CF5EEEEEEFFEDED
        EDFFECECECFFEBEBEBFFEAEAEAFFE9E9E9FFE8E8E8FFFFFFFFFF939393F90000
        000000000000000000000000000000750CF5D3FFE9FF8EFDB4FF00750CF5EFEF
        EFFFEEEEEEFFEDEDEDFFECECECFFEBEBEBFFEAEAEAFFFFFFFFFF939393F90000
        0000000000000000000000750CF5D3FFE9FF41EF7DFF58EF8BFF8EF9B2FF0075
        0CF5F0F0F0FFEFEFEFFFEEEEEEFFEDEDEDFFECECECFFFFFFFFFF939393F90000
        00000000000000750CF5D3FFE9FF7FFFAEFF76BD8CFF00750CF56FF79EFF84F2
        A9FF00750CF5F1F1F1FFF0F0F0FFEFEFEFFFEEEEEEFFFFFFFFFF939393F90000
        00000000000050795D3500750CF5A7DBB7FF00750CF5F6F6F6FF00750CF588FD
        B0FF8BF4AFFF00750CF5F1F1F1FFF1F1F1FFF0F0F0FFFFFFFFFF939393F90000
        000000000000000000000014003800750CF5FFFFFFFFF8F8F8FFF2F2F2FF0075
        0CF5D3FFE9FF00750CF5E7ECE8FFF2F2F2FFF1F1F1FFFFFFFFFF939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFAFAFAFFF9F9F9FFF2F2
        F2FF00750CF5C5C5C5FFF5F5F5FFF4F4F4FFF3F3F3FFF5F5F5FF939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFBFBFBFFFAFAFAFFFAFA
        FAFFF9F9F9FFF8F8F8FF939393F9939393F9939393F9939393F9939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFDFDFDFFFCFCFCFFFBFB
        FBFFFAFAFAFFFAFAFAFF939393F9E1E1E1FFE1E1E1FFB5B5B5F9939393F90000
        0000000000000000000000000000939393F9FFFFFFFFFFFFFFFFFEFEFEFFFDFD
        FDFFFCFCFCFFFBFBFBFF939393F9E1E1E1FFB5B5B5F9939393F9000000000000
        0000000000000000000000000000939393F9FFFFFFFFFFFFFFFFFFFFFFFFFEFE
        FEFFFEFEFEFFFDFDFDFF939393F9B5B5B5F9939393F900000000000000000000
        0000000000000000000000000000939393F9939393F9939393F9939393F99393
        93F9939393F9939393F9939393F9939393F90000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000}
      ShowHint = True
      Transparent = True
      Visible = False
    end
    object edLogin: TsDBEdit
      Left = 359
      Top = 72
      Width = 148
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'LOGIN'
      DataSource = dsEscreventes
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Login'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edNome: TsDBEdit
      Left = 151
      Top = 24
      Width = 356
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'NOME'
      DataSource = dsEscreventes
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Nome'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edDocumento: TsDBEdit
      Left = 516
      Top = 24
      Width = 128
      Height = 23
      Color = clWhite
      DataField = 'CPF'
      DataSource = dsEscreventes
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnChange = edDocumentoChange
      OnClick = edDocumentoClick
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Documento'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edQualificacao: TsDBEdit
      Left = 10
      Top = 72
      Width = 239
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'QUALIFICACAO'
      DataSource = dsEscreventes
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Qualifica'#231#227'o'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edSenha: TsDBEdit
      Left = 515
      Top = 72
      Width = 148
      Height = 23
      Color = clWhite
      DataField = 'SENHA'
      DataSource = dsEscreventes
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      PasswordChar = '*'
      TabOrder = 6
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Senha'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object RgTransmite: TsDBRadioGroup
      Left = 10
      Top = 8
      Width = 131
      Height = 39
      Caption = 'Autorizado'
      ParentBackground = False
      TabOrder = 0
      SkinData.SkinSection = 'GROUPBOX'
      Columns = 2
      Items.Strings = (
        'Sim'
        'N'#227'o')
      DataField = 'AUTORIZADO'
      DataSource = dsEscreventes
      Values.Strings = (
        'S'
        'N')
    end
    object edMatricula: TsDBEdit
      Left = 257
      Top = 72
      Width = 95
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      DataField = 'MATRICULA'
      DataSource = dsEscreventes
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Matr'#237'cula'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclTopLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
  end
  object dsEscreventes: TDataSource
    DataSet = dm.Escreventes
    OnStateChange = dsEscreventesStateChange
    OnDataChange = dsEscreventesDataChange
    Left = 48
    Top = 104
  end
end
