unit ULivroAdicional;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, Buttons, sBitBtn, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, DB, Grids, Wwdbigrd, Wwdbgrid, DBClient,
  SimpleDS, sRadioButton, RxMemDS, sLabel, ComCtrls, acProgressBar, Menus,
  FMTBcd, SqlExpr, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFLivroAdicional = class(TForm)
    sPanel1: TsPanel;
    sPanel2: TsPanel;
    Grid: TwwDBGrid;
    dsXml: TDataSource;
    edData: TsDateEdit;
    btVisualizar: TsBitBtn;
    btExportar: TsBitBtn;
    sPanel3: TsPanel;
    RX: TRxMemoryData;
    RXID_ATO: TIntegerField;
    RXDESCRICAO: TStringField;
    RXDATA: TDateField;
    RXCODIGO: TIntegerField;
    RXGRATUITO: TStringField;
    RXCONVENIO: TStringField;
    RXLIVRO: TStringField;
    RXFOLHA: TStringField;
    RXRECIBO: TStringField;
    RXEMOL: TFloatField;
    RXFETJ: TFloatField;
    RXFUND: TFloatField;
    RXFUNP: TFloatField;
    RXMUTUA: TFloatField;
    RXACOTERJ: TFloatField;
    RXDIST: TFloatField;
    RXTOTAL: TFloatField;
    RXSELO: TStringField;
    dsRX: TDataSource;
    RXCHECK: TBooleanField;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    Shape1: TShape;
    Shape2: TShape;
    sLabel4: TsLabel;
    Shape3: TShape;
    Shape4: TShape;
    P: TsProgressBar;
    PM: TPopupMenu;
    Desmarcartodos1: TMenuItem;
    Marcartodos1: TMenuItem;
    RXPROTOCOLO: TIntegerField;
    lbEtapa: TsLabel;
    Shape5: TShape;
    sLabel5: TsLabel;
    RXSTATUS: TStringField;
    Shape6: TShape;
    sLabel6: TsLabel;
    RXFUNA: TFloatField;
    qryXml: TFDQuery;
    qryCertidao: TFDQuery;
    qryBoletos: TFDQuery;
    qryCancelado: TFDQuery;
    qryXmlID_ATO: TIntegerField;
    qryXmlCODIGO: TIntegerField;
    qryXmlRECIBO: TIntegerField;
    qryXmlDT_ENTRADA: TDateField;
    qryXmlDT_PROTOCOLO: TDateField;
    qryXmlPROTOCOLO: TIntegerField;
    qryXmlLIVRO_PROTOCOLO: TIntegerField;
    qryXmlFOLHA_PROTOCOLO: TStringField;
    qryXmlDT_PRAZO: TDateField;
    qryXmlDT_REGISTRO: TDateField;
    qryXmlREGISTRO: TIntegerField;
    qryXmlLIVRO_REGISTRO: TIntegerField;
    qryXmlFOLHA_REGISTRO: TStringField;
    qryXmlSELO_REGISTRO: TStringField;
    qryXmlDT_PAGAMENTO: TDateField;
    qryXmlSELO_PAGAMENTO: TStringField;
    qryXmlRECIBO_PAGAMENTO: TIntegerField;
    qryXmlCOBRANCA: TStringField;
    qryXmlEMOLUMENTOS: TFloatField;
    qryXmlFETJ: TFloatField;
    qryXmlFUNDPERJ: TFloatField;
    qryXmlFUNPERJ: TFloatField;
    qryXmlFUNARPEN: TFloatField;
    qryXmlPMCMV: TFloatField;
    qryXmlISS: TFloatField;
    qryXmlMUTUA: TFloatField;
    qryXmlDISTRIBUICAO: TFloatField;
    qryXmlACOTERJ: TFloatField;
    qryXmlTOTAL: TFloatField;
    qryXmlTIPO_PROTESTO: TIntegerField;
    qryXmlTIPO_TITULO: TIntegerField;
    qryXmlNUMERO_TITULO: TStringField;
    qryXmlDT_TITULO: TDateField;
    qryXmlBANCO: TStringField;
    qryXmlAGENCIA: TStringField;
    qryXmlCONTA: TStringField;
    qryXmlVALOR_TITULO: TFloatField;
    qryXmlSALDO_PROTESTO: TFloatField;
    qryXmlDT_VENCIMENTO: TDateField;
    qryXmlCONVENIO: TStringField;
    qryXmlTIPO_APRESENTACAO: TStringField;
    qryXmlDT_ENVIO: TDateField;
    qryXmlCODIGO_APRESENTANTE: TStringField;
    qryXmlTIPO_INTIMACAO: TStringField;
    qryXmlMOTIVO_INTIMACAO: TMemoField;
    qryXmlDT_INTIMACAO: TDateField;
    qryXmlDT_PUBLICACAO: TDateField;
    qryXmlVALOR_PAGAMENTO: TFloatField;
    qryXmlAPRESENTANTE: TStringField;
    qryXmlCPF_CNPJ_APRESENTANTE: TStringField;
    qryXmlTIPO_APRESENTANTE: TStringField;
    qryXmlCEDENTE: TStringField;
    qryXmlNOSSO_NUMERO: TStringField;
    qryXmlSACADOR: TStringField;
    qryXmlDEVEDOR: TStringField;
    qryXmlCPF_CNPJ_DEVEDOR: TStringField;
    qryXmlTIPO_DEVEDOR: TStringField;
    qryXmlAGENCIA_CEDENTE: TStringField;
    qryXmlPRACA_PROTESTO: TStringField;
    qryXmlTIPO_ENDOSSO: TStringField;
    qryXmlACEITE: TStringField;
    qryXmlCPF_ESCREVENTE: TStringField;
    qryXmlCPF_ESCREVENTE_PG: TStringField;
    qryXmlOBSERVACAO: TMemoField;
    qryXmlDT_SUSTADO: TDateField;
    qryXmlDT_RETIRADO: TDateField;
    qryXmlSTATUS: TStringField;
    qryXmlPROTESTADO: TStringField;
    qryXmlENVIADO_APONTAMENTO: TStringField;
    qryXmlENVIADO_PROTESTO: TStringField;
    qryXmlENVIADO_PAGAMENTO: TStringField;
    qryXmlENVIADO_RETIRADO: TStringField;
    qryXmlENVIADO_SUSTADO: TStringField;
    qryXmlENVIADO_DEVOLVIDO: TStringField;
    qryXmlEXPORTADO: TStringField;
    qryXmlAVALISTA_DEVEDOR: TStringField;
    qryXmlFINS_FALIMENTARES: TStringField;
    qryXmlTARIFA_BANCARIA: TFloatField;
    qryXmlFORMA_PAGAMENTO: TStringField;
    qryXmlNUMERO_PAGAMENTO: TStringField;
    qryXmlARQUIVO: TStringField;
    qryXmlRETORNO: TStringField;
    qryXmlDT_DEVOLVIDO: TDateField;
    qryXmlCPF_CNPJ_SACADOR: TStringField;
    qryXmlID_MSG: TIntegerField;
    qryXmlVALOR_AR: TFloatField;
    qryXmlELETRONICO: TStringField;
    qryXmlIRREGULARIDADE: TIntegerField;
    qryXmlAVISTA: TStringField;
    qryXmlSALDO_TITULO: TFloatField;
    qryXmlTIPO_SUSTACAO: TStringField;
    qryXmlRETORNO_PROTESTO: TStringField;
    qryXmlDT_RETORNO_PROTESTO: TDateField;
    qryXmlDT_DEFINITIVA: TDateField;
    qryXmlJUDICIAL: TStringField;
    qryXmlALEATORIO_PROTESTO: TStringField;
    qryXmlALEATORIO_SOLUCAO: TStringField;
    qryXmlCCT: TStringField;
    qryXmlDETERMINACAO: TStringField;
    qryXmlANTIGO: TStringField;
    qryXmlLETRA: TStringField;
    qryXmlPROTOCOLO_CARTORIO: TIntegerField;
    qryXmlARQUIVO_CARTORIO: TStringField;
    qryXmlRETORNO_CARTORIO: TStringField;
    qryXmlDT_DEVOLVIDO_CARTORIO: TDateField;
    qryXmlCARTORIO: TIntegerField;
    qryXmlDT_PROTOCOLO_CARTORIO: TDateField;
    qryCertidaoID_CERTIDAO: TIntegerField;
    qryCertidaoID_ATO: TIntegerField;
    qryCertidaoORDEM: TIntegerField;
    qryCertidaoDT_PEDIDO: TDateField;
    qryCertidaoDT_ENTREGA: TDateField;
    qryCertidaoDT_CERTIDAO: TDateField;
    qryCertidaoTIPO_CERTIDAO: TStringField;
    qryCertidaoNUMERO_TITULO: TStringField;
    qryCertidaoANOS: TIntegerField;
    qryCertidaoCONVENIO: TStringField;
    qryCertidaoCODIGO: TIntegerField;
    qryCertidaoSELO: TStringField;
    qryCertidaoALEATORIO: TStringField;
    qryCertidaoRECIBO: TIntegerField;
    qryCertidaoREQUERIDO: TStringField;
    qryCertidaoTIPO_REQUERIDO: TStringField;
    qryCertidaoCPF_CNPJ_REQUERIDO: TStringField;
    qryCertidaoREQUERENTE: TStringField;
    qryCertidaoCPF_CNPJ_REQUERENTE: TStringField;
    qryCertidaoTELEFONE_REQUERENTE: TStringField;
    qryCertidaoRESULTADO: TStringField;
    qryCertidaoFOLHAS: TIntegerField;
    qryCertidaoREGISTROS: TIntegerField;
    qryCertidaoCOBRANCA: TStringField;
    qryCertidaoEMOLUMENTOS: TFloatField;
    qryCertidaoFETJ: TFloatField;
    qryCertidaoFUNDPERJ: TFloatField;
    qryCertidaoFUNPERJ: TFloatField;
    qryCertidaoFUNARPEN: TFloatField;
    qryCertidaoPMCMV: TFloatField;
    qryCertidaoISS: TFloatField;
    qryCertidaoMUTUA: TFloatField;
    qryCertidaoACOTERJ: TFloatField;
    qryCertidaoDISTRIBUICAO: TFloatField;
    qryCertidaoAPONTAMENTO: TFloatField;
    qryCertidaoCAPA: TFloatField;
    qryCertidaoTOTAL: TFloatField;
    qryCertidaoEXRECIBO: TIntegerField;
    qryCertidaoEXCOBRANCA: TStringField;
    qryCertidaoEXEMOLUMENTOS: TFloatField;
    qryCertidaoEXFETJ: TFloatField;
    qryCertidaoEXFUNDPERJ: TFloatField;
    qryCertidaoEXFUNPERJ: TFloatField;
    qryCertidaoEXISS: TFloatField;
    qryCertidaoEXFUNARPEN: TFloatField;
    qryCertidaoEXTOTAL: TFloatField;
    qryCertidaoCCT: TStringField;
    qryCertidaoESCREVENTE: TStringField;
    qryCertidaoRESPONSAVEL: TStringField;
    qryCertidaoOBSERVACAO: TMemoField;
    qryCertidaoENVIADO: TStringField;
    qryCertidaoENVIADO_FLS: TStringField;
    qryCertidaoQTD: TIntegerField;
    qryBoletosID_BOLETO: TIntegerField;
    qryBoletosID_ATO: TIntegerField;
    qryBoletosID_DEVEDOR: TIntegerField;
    qryBoletosESPECIE_DOCUMENTO: TStringField;
    qryBoletosMOEDA: TStringField;
    qryBoletosACEITE: TStringField;
    qryBoletosCARTEIRA: TStringField;
    qryBoletosNOSSO_NUMERO: TStringField;
    qryBoletosVALOR_MORA_JUROS: TFloatField;
    qryBoletosVALOR_DESCONTO: TFloatField;
    qryBoletosVALOR_ABATIMENTO: TFloatField;
    qryBoletosPORCENTO_MULTA: TFloatField;
    qryBoletosDATA_MULTA_JUROS: TDateField;
    qryBoletosDATA_DESCONTO: TDateField;
    qryBoletosDATA_ABATIMENTO: TDateField;
    qryBoletosDATA_PROTESTO: TDateField;
    qryBoletosNUMERO_DOCUMENTO: TStringField;
    qryBoletosVALOR: TFloatField;
    qryBoletosDATA_EMISSAO: TDateField;
    qryBoletosDATA_VENCIMENTO: TDateField;
    qryBoletosINSTRUCAO1: TStringField;
    qryBoletosINSTRUCAO2: TStringField;
    qryBoletosINSTRUCAO3: TStringField;
    qryBoletosINSTRUCAO4: TStringField;
    qryBoletosINSTRUCAO5: TStringField;
    qryBoletosDATA_PROTOCOLO: TDateField;
    qryBoletosPROTOCOLO: TIntegerField;
    qryBoletosHORA: TTimeField;
    qryBoletosEMISSOR: TStringField;
    qryBoletosPAGO: TStringField;
    qryBoletosEMOLUMENTOS: TFloatField;
    qryBoletosFETJ: TFloatField;
    qryBoletosFUNDPERJ: TFloatField;
    qryBoletosFUNPERJ: TFloatField;
    qryBoletosFUNARPEN: TFloatField;
    qryBoletosPMCMV: TFloatField;
    qryBoletosTOTAL: TFloatField;
    qryBoletosCCT: TStringField;
    qryBoletosENVIADO: TStringField;
    qryBoletosDATA_BAIXA: TDateField;
    qryCanceladoID_CERTIDAO: TIntegerField;
    qryCanceladoID_ATO: TIntegerField;
    qryCanceladoORDEM: TIntegerField;
    qryCanceladoDT_PEDIDO: TDateField;
    qryCanceladoDT_ENTREGA: TDateField;
    qryCanceladoDT_CERTIDAO: TDateField;
    qryCanceladoTIPO_CERTIDAO: TStringField;
    qryCanceladoNUMERO_TITULO: TStringField;
    qryCanceladoANOS: TIntegerField;
    qryCanceladoCONVENIO: TStringField;
    qryCanceladoCODIGO: TIntegerField;
    qryCanceladoSELO: TStringField;
    qryCanceladoALEATORIO: TStringField;
    qryCanceladoRECIBO: TIntegerField;
    qryCanceladoREQUERIDO: TStringField;
    qryCanceladoTIPO_REQUERIDO: TStringField;
    qryCanceladoCPF_CNPJ_REQUERIDO: TStringField;
    qryCanceladoREQUERENTE: TStringField;
    qryCanceladoCPF_CNPJ_REQUERENTE: TStringField;
    qryCanceladoTELEFONE_REQUERENTE: TStringField;
    qryCanceladoRESULTADO: TStringField;
    qryCanceladoFOLHAS: TIntegerField;
    qryCanceladoREGISTROS: TIntegerField;
    qryCanceladoCOBRANCA: TStringField;
    qryCanceladoEMOLUMENTOS: TFloatField;
    qryCanceladoFETJ: TFloatField;
    qryCanceladoFUNDPERJ: TFloatField;
    qryCanceladoFUNPERJ: TFloatField;
    qryCanceladoFUNARPEN: TFloatField;
    qryCanceladoPMCMV: TFloatField;
    qryCanceladoISS: TFloatField;
    qryCanceladoMUTUA: TFloatField;
    qryCanceladoACOTERJ: TFloatField;
    qryCanceladoDISTRIBUICAO: TFloatField;
    qryCanceladoAPONTAMENTO: TFloatField;
    qryCanceladoCAPA: TFloatField;
    qryCanceladoTOTAL: TFloatField;
    qryCanceladoEXRECIBO: TIntegerField;
    qryCanceladoEXCOBRANCA: TStringField;
    qryCanceladoEXEMOLUMENTOS: TFloatField;
    qryCanceladoEXFETJ: TFloatField;
    qryCanceladoEXFUNDPERJ: TFloatField;
    qryCanceladoEXFUNPERJ: TFloatField;
    qryCanceladoEXISS: TFloatField;
    qryCanceladoEXFUNARPEN: TFloatField;
    qryCanceladoEXTOTAL: TFloatField;
    qryCanceladoCCT: TStringField;
    qryCanceladoESCREVENTE: TStringField;
    qryCanceladoRESPONSAVEL: TStringField;
    qryCanceladoOBSERVACAO: TMemoField;
    qryCanceladoENVIADO: TStringField;
    qryCanceladoENVIADO_FLS: TStringField;
    qryCanceladoQTD: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btExportarClick(Sender: TObject);
    procedure btVisualizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GridCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure Marcartodos1Click(Sender: TObject);
    procedure GridMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure sPanel1MouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure GridMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Desmarcartodos1Click(Sender: TObject);
    procedure GravarRX(Selo,Status: String);
    procedure GravarCertidao;
    procedure GravarBoleto;
    function NenhumMarcado: Boolean; 
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FLivroAdicional: TFLivroAdicional;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFLivroAdicional.FormCreate(Sender: TObject);
begin
  qryXml.Connection:=dm.conSISTEMA;
  edData.Date:=Now;
end;

procedure TFLivroAdicional.btExportarClick(Sender: TObject);
var
  XML: TextFile;
  Posicao: Integer;
  Codigo,Gratuito,Registro,Livro,Folhas,Arquivo,Tipo: String;
  Recibo,Emolumentos,Fetj,Fundperj,Funperj,Funarpen,Mutua,Total,Selo: String;
begin
  if not RX.Active then Exit;

  if NenhumMarcado then
  begin
      GR.Aviso('NENHUM ATO SELECIONADO!');
      Exit;
  end;

  Arquivo:=dm.ValorParametro(80)+'LAE-PROTESTO-'+FormatDateTime('dd-mm-yyyy',Now)+'.xml';
  AssignFile(XML,Arquivo);
  Rewrite(XML);

  Writeln(XML,'<?xml version="1.0" encoding="ISO-8859-1"?>');
  Writeln(XML,'<LIVRO_ADICIONAL>');
  Writeln(XML,'<ATRIBUICAO>');
  Writeln(XML,'<COD_ATRIBUICAO>4</COD_ATRIBUICAO>');
  Writeln(XML,'<DESCRICAO>PROTESTO</DESCRICAO>');
  Writeln(XML,'<DATA_PRATICA>',FormatDateTime('dd/mm/yyyy',edData.Date),'</DATA_PRATICA>');
  Writeln(XML,'<QUANT_ATOS>',IntToStr(RX.RecordCount),'</QUANT_ATOS>');
  Posicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  while not RX.Eof do
  begin
      if RXCHECK.AsBoolean then
      begin
          Gratuito  :=RXGRATUITO.AsString;
          Codigo    :=RXCODIGO.AsString;
          Livro     :=RXLIVRO.AsString;
          Folhas    :=RXFOLHA.AsString;
          Recibo    :=RXRECIBO.AsString;
          Selo      :=RXSELO.AsString;

          if (RXSTATUS.AsString='PROTESTADO') or (RXSTATUS.AsString='CANCELADO') then
            Tipo:='R'
              else Tipo:='C';

          if RXSTATUS.AsString='APONTADO' then
          begin
              Tipo:='';
              Selo:='';
          end;

          Emolumentos :=GR.ValorComPontoXML(RXEMOL.AsFloat);
          Fetj        :=GR.ValorComPontoXML(RXFETJ.AsFloat);
          Fundperj    :=GR.ValorComPontoXML(RXFUND.AsFloat);
          Funperj     :=GR.ValorComPontoXML(RXFUNP.AsFloat);
          Funarpen    :=GR.ValorComPontoXML(RXFUNA.AsFloat);
          Mutua       :=GR.ValorComPontoXML(RXMUTUA.AsFloat);
          Total       :=GR.ValorComPontoXML(RXTOTAL.AsFloat);

          Writeln(XML,'<ATO>');
          Writeln(XML,'<COD_ATO>',Codigo,'</COD_ATO>');
          Writeln(XML,'<IND_GRATUIDADE>',Gratuito,'</IND_GRATUIDADE>');
          Writeln(XML,'<CONVENIO>',RXConvenio.AsString,'</CONVENIO>');
          Writeln(XML,'<NUM_PROTOCOLO>',RXProtocolo.AsString,'</NUM_PROTOCOLO>');
          Writeln(XML,'<NUM_MATRICULA></NUM_MATRICULA>');
          Writeln(XML,'<NUM_REGISTRO>',Registro,'</NUM_REGISTRO>');
          Writeln(XML,'<LIVRO>',Livro,'</LIVRO>');
          Writeln(XML,'<FOLHAS>',Folhas,'</FOLHAS>');
          Writeln(XML,'<NUM_RECIBO>',Recibo,'</NUM_RECIBO>');
          Writeln(XML,'<VALOR_EMOLUMENTO>',Emolumentos,'</VALOR_EMOLUMENTO>');
          Writeln(XML,'<VALOR_LEI3217>',Fetj,'</VALOR_LEI3217>');
          Writeln(XML,'<VALOR_LEI4664>',Fundperj,'</VALOR_LEI4664>');
          Writeln(XML,'<VALOR_LEI111>',Funperj,'</VALOR_LEI111>');
          Writeln(XML,'<VALOR_LEI6281>',Funarpen,'</VALOR_LEI6281>');
          Writeln(XML,'<VALOR_LEI3761>',Mutua,'</VALOR_LEI3761>');
          Writeln(XML,'<SELO>');
          Writeln(XML,'<TIPO_SELO>',Tipo,'</TIPO_SELO>');
          Writeln(XML,'<NUMERO_SELO>',Selo,'</NUMERO_SELO>');
          Writeln(XML,'</SELO>');
          Writeln(XML,'</ATO>');
      end;
      RX.Next;
  end;
  Writeln(XML,'</ATRIBUICAO>');
  Writeln(XML,'</LIVRO_ADICIONAL>');
  RX.RecNo:=Posicao;
  RX.EnableControls;
  CloseFile(XML);
  GR.Aviso('ARQUIVO GERADO COM SUCESSO!'+#13#13+Arquivo);
end;

procedure TFLivroAdicional.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFLivroAdicional.GridCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  if RXSTATUS.AsString='APONTADO'    then AFont.Color:=clBlue;
  if RXSTATUS.AsString='PAGO'        then AFont.Color:=clGreen;
  if RXSTATUS.AsString='PROTESTADO'  then AFont.Color:=clPurple;
  if RXSTATUS.AsString='CANCELADO'   then AFont.Color:=clRed;
  if RXSTATUS.AsString='CERTIDÃO'    then AFont.Color:=$004080FF;
  if RXSTATUS.AsString='BOLETO'      then AFont.Color:=clGreen;
  if RXSTATUS.AsString='DESISTÊNCIA' then AFont.Color:=clMaroon;
end;

procedure TFLivroAdicional.Marcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  while not RX.Eof do
  begin
      RX.Edit;
      RXCHECK.AsBoolean:=True;
      RX.Post;
      RX.Next;
  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;
end;

procedure TFLivroAdicional.GridMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if X in [11..34] then
    Screen.Cursor:=crHandPoint
      else Screen.Cursor:=crDefault;
end;

procedure TFLivroAdicional.sPanel1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  Screen.Cursor:=crDefault;
end;

procedure TFLivroAdicional.GridMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Screen.Cursor=crHandPoint then
  begin
      if RX.IsEmpty then Exit;
      RX.Edit;
      if RXCHECK.AsBoolean then
        RXCHECK.AsBoolean:=False
          else RXCHECK.AsBoolean:=True;
      RX.Post;
  end;
end;

procedure TFLivroAdicional.Desmarcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  while not RX.Eof do
  begin
      RX.Edit;
      RXCHECK.AsBoolean:=False;
      RX.Post;
      RX.Next;
  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;
end;

procedure TFLivroAdicional.GravarRX(Selo,Status: String);
begin
  qryXml.Last;
  P.Max:=qryXml.RecordCount;
  P.Position:=0;
  qryXml.First;
  while not qryXml.Eof do
  begin
      Application.ProcessMessages;
      P.StepBy(1);
      RX.Append;
      RXID_ATO.AsInteger    :=qryXmlID_ATO.AsInteger;
      RXDESCRICAO.AsString  :=PF.RetornarAdicional(qryXmlCODIGO.AsInteger);
      RXDATA.AsDateTime     :=qryXmlDT_PROTOCOLO.AsDateTime;
      if (qryXmlCOBRANCA.AsString='CC') or (qryXmlCOBRANCA.AsString='NH') then
        RXGRATUITO.AsString:='N'
          else RXGRATUITO.AsString:='S';
      RXCONVENIO.AsString   :=qryXmlCONVENIO.AsString;

      if Selo='R' then
      begin
          RXLIVRO.AsString  :=qryXmlLIVRO_REGISTRO.AsString;
          RXFOLHA.AsString  :=qryXmlFOLHA_REGISTRO.AsString;
          RXSELO.AsString   :=qryXmlSELO_REGISTRO.AsString;
      end
      else
      begin
          RXLIVRO.AsString  :=qryXmlLIVRO_PROTOCOLO.AsString;
          RXFOLHA.AsString  :=qryXmlFOLHA_PROTOCOLO.AsString;
          RXSELO.AsString   :=qryXmlSELO_PAGAMENTO.AsString;
      end;

      RXRECIBO.AsString     :=qryXmlRECIBO.AsString;
      RXPROTOCOLO.AsInteger :=qryXmlPROTOCOLO.AsInteger;

      if Status='APONTADO' then
      begin
          RXCODIGO.AsInteger:=qryXmlCODIGO.AsInteger;
          RXSELO.Clear;

          if qryXmlCONVENIO.AsString='N' then
          begin
              RXEMOL.AsFloat    :=qryXmlEMOLUMENTOS.AsFloat;
              RXFETJ.AsFloat    :=qryXmlFETJ.AsFloat;
              RXFUND.AsFloat    :=qryXmlFUNDPERJ.AsFloat;
              RXFUNP.AsFloat    :=qryXmlFUNPERJ.AsFloat;
              RXFUNA.AsFloat    :=qryXmlFUNARPEN.AsFloat;
              RXMUTUA.AsFloat   :=qryXmlMUTUA.AsFloat;
              RXACOTERJ.AsFloat :=qryXmlACOTERJ.AsFloat;
              RXDIST.AsFloat    :=qryXmlDISTRIBUICAO.AsFloat;
              RXTOTAL.AsFloat   :=qryXmlTOTAL.AsFloat-qryXmlPMCMV.AsFloat;
          end
          else
          begin
              RXEMOL.AsFloat    :=0;
              RXFETJ.AsFloat    :=0;
              RXFUND.AsFloat    :=0;
              RXFUNP.AsFloat    :=0;
              RXFUNA.AsFloat    :=0;
              RXMUTUA.AsFloat   :=0;
              RXACOTERJ.AsFloat :=0;
              RXDIST.AsFloat    :=0;
              RXTOTAL.AsFloat   :=0;
          end;
      end;

      if Status='PAGO' then
      begin
          RXCODIGO.AsInteger:=4026;

          if qryXmlCONVENIO.AsString='S' then
          begin
              RXEMOL.AsFloat    :=qryXmlEMOLUMENTOS.AsFloat;
              RXFETJ.AsFloat    :=qryXmlFETJ.AsFloat;
              RXFUND.AsFloat    :=qryXmlFUNDPERJ.AsFloat;
              RXFUNP.AsFloat    :=qryXmlFUNPERJ.AsFloat;
              RXFUNA.AsFloat    :=qryXmlFUNARPEN.AsFloat;
              RXMUTUA.AsFloat   :=qryXmlMUTUA.AsFloat;
              RXACOTERJ.AsFloat :=qryXmlACOTERJ.AsFloat;
              RXDIST.AsFloat    :=qryXmlDISTRIBUICAO.AsFloat;
              RXTOTAL.AsFloat   :=qryXmlTOTAL.AsFloat-qryXmlPMCMV.AsFloat;
          end
          else
          begin
              RXEMOL.AsFloat    :=0;
              RXFETJ.AsFloat    :=0;
              RXFUND.AsFloat    :=0;
              RXFUNP.AsFloat    :=0;
              RXFUNA.AsFloat    :=0;
              RXMUTUA.AsFloat   :=0;
              RXACOTERJ.AsFloat :=0;
              RXDIST.AsFloat    :=0;
              RXTOTAL.AsFloat   :=0;
          end;
      end;

      if Status='PROTESTADO' then
      begin
          if qryXmlCONVENIO.AsString='S' then
          begin
              RXCODIGO.AsInteger:=4043;
              RXEMOL.AsFloat    :=qryXmlEMOLUMENTOS.AsFloat;
              RXFETJ.AsFloat    :=qryXmlFETJ.AsFloat;
              RXFUND.AsFloat    :=qryXmlFUNDPERJ.AsFloat;
              RXFUNP.AsFloat    :=qryXmlFUNPERJ.AsFloat;
              RXFUNA.AsFloat    :=qryXmlFUNARPEN.AsFloat;
              RXMUTUA.AsFloat   :=qryXmlMUTUA.AsFloat;
              RXACOTERJ.AsFloat :=qryXmlACOTERJ.AsFloat;
              RXDIST.AsFloat    :=qryXmlDISTRIBUICAO.AsFloat;
              RXTOTAL.AsFloat   :=qryXmlTOTAL.AsFloat-qryXmlPMCMV.AsFloat;
          end
          else
          begin
              RXCODIGO.AsInteger:=4029;
              RXEMOL.AsFloat    :=0;
              RXFETJ.AsFloat    :=0;
              RXFUND.AsFloat    :=0;
              RXFUNP.AsFloat    :=0;
              RXFUNA.AsFloat    :=0;
              RXMUTUA.AsFloat   :=0;
              RXACOTERJ.AsFloat :=0;
              RXDIST.AsFloat    :=0;
              RXTOTAL.AsFloat   :=0;
          end;
      end;

      if Status='CANCELADO' then
      begin
          RXSELO.AsString:=qryXmlSELO_PAGAMENTO.AsString;
          if qryXmlCONVENIO.AsString='S' then
          begin
              RXCODIGO.AsInteger:=4038;
              RXEMOL.AsFloat    :=qryXmlEMOLUMENTOS.AsFloat;
              RXFETJ.AsFloat    :=qryXmlFETJ.AsFloat;
              RXFUND.AsFloat    :=qryXmlFUNDPERJ.AsFloat;
              RXFUNP.AsFloat    :=qryXmlFUNPERJ.AsFloat;
              RXFUNA.AsFloat    :=qryXmlFUNARPEN.AsFloat;
              RXMUTUA.AsFloat   :=qryXmlMUTUA.AsFloat;
              RXACOTERJ.AsFloat :=qryXmlACOTERJ.AsFloat;
              RXDIST.AsFloat    :=qryXmlDISTRIBUICAO.AsFloat;
              RXTOTAL.AsFloat   :=qryXmlTOTAL.AsFloat-qryXmlPMCMV.AsFloat;
          end
          else
          begin
              qryCancelado.Close;
              qryCancelado.Params[0].AsInteger:=qryXmlID_ATO.AsInteger;
              qryCancelado.Open;

              RXCODIGO.AsInteger:=4010;
              RXEMOL.AsFloat    :=qryCanceladoEMOLUMENTOS.AsFloat;
              RXFETJ.AsFloat    :=qryCanceladoFETJ.AsFloat;
              RXFUND.AsFloat    :=qryCanceladoFUNDPERJ.AsFloat;
              RXFUNP.AsFloat    :=qryCanceladoFUNPERJ.AsFloat;
              RXFUNA.AsFloat    :=qryCanceladoFUNARPEN.AsFloat;
              RXMUTUA.AsFloat   :=qryCanceladoMUTUA.AsFloat;
              RXACOTERJ.AsFloat :=qryCanceladoACOTERJ.AsFloat;
              RXDIST.AsFloat    :=qryCanceladoDISTRIBUICAO.AsFloat;
              RXTOTAL.AsFloat   :=qryCanceladoTOTAL.AsFloat-qryCanceladoPMCMV.AsFloat;
          end;
      end;

      if Status='DESISTÊNCIA' then
      begin
          RXCODIGO.AsInteger:=4021;
          RXEMOL.AsFloat    :=0;
          RXFETJ.AsFloat    :=0;
          RXFUND.AsFloat    :=0;
          RXFUNP.AsFloat    :=0;
          RXFUNA.AsFloat    :=0;
          RXMUTUA.AsFloat   :=0;
          RXACOTERJ.AsFloat :=0;
          RXDIST.AsFloat    :=0;
          RXTOTAL.AsFloat   :=0;
      end;

      RXSTATUS.AsString:=Status;
      RX.Post;
      qryXml.Next;
  end;
end;

function TFLivroAdicional.NenhumMarcado: Boolean;
var
  Posicao: Integer;
begin
  Result:=True;
  Posicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  while not RX.Eof do
  begin
      if RXCHECK.AsBoolean then
      begin
          Result:=False;
          Break;
      end;
      RX.Next;
  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;
end;

procedure TFLivroAdicional.GravarCertidao;
begin
  qryCertidao.Last;
  P.Max:=qryCertidao.RecordCount;
  P.Position:=0;
  qryCertidao.First;
  while not qryCertidao.Eof do
  begin
      Application.ProcessMessages;
      P.StepBy(1);
      RX.Append;
      RXID_ATO.AsInteger    :=qryCertidaoID_CERTIDAO.AsInteger;
      RXDESCRICAO.AsString  :=PF.RetornarAdicional(qryCertidaoCODIGO.AsInteger);
      RXDATA.AsDateTime     :=qryCertidaoDT_CERTIDAO.AsDateTime;
      RXCODIGO.AsInteger    :=qryCertidaoCODIGO.AsInteger;
      if (qryCertidaoCOBRANCA.AsString='CC') or (qryCertidaoCOBRANCA.AsString='NH') then
        RXGRATUITO.AsString:='N'
          else RXGRATUITO.AsString:='S';
      RXCONVENIO.AsString   :=qryCertidaoCONVENIO.AsString;
      RXFOLHA.AsString      :=qryCertidaoFOLHAS.AsString;
      RXRECIBO.AsString     :=qryCertidaoRECIBO.AsString;
      RXEMOL.AsFloat        :=qryCertidaoEMOLUMENTOS.AsFloat;
      RXFETJ.AsFloat        :=qryCertidaoFETJ.AsFloat;
      RXFUND.AsFloat        :=qryCertidaoFUNDPERJ.AsFloat;
      RXFUNP.AsFloat        :=qryCertidaoFUNPERJ.AsFloat;
      RXFUNA.AsFloat        :=qryCertidaoFUNARPEN.AsFloat;
      RXMUTUA.AsFloat       :=qryCertidaoMUTUA.AsFloat;
      RXACOTERJ.AsFloat     :=qryCertidaoACOTERJ.AsFloat;
      RXDIST.AsFloat        :=qryCertidaoDISTRIBUICAO.AsFloat;
      RXTOTAL.AsFloat       :=qryCertidaoTOTAL.AsFloat-qryCertidaoPMCMV.AsFloat;
      RXSELO.AsString       :=qryCertidaoSELO.AsString;
      RXSTATUS.AsString     :='CERTIDÃO';

      if qryCertidaoTIPO_CERTIDAO.AsString='X' then
      begin
          dm.Titulos.Close;
          dm.Titulos.Params[0].AsInteger:=qryCertidaoID_ATO.AsInteger;
          dm.Titulos.Open;

          RXEMOL.AsFloat        :=qryCertidaoEMOLUMENTOS.AsFloat+dm.TitulosEMOLUMENTOS.AsFloat;
          RXFETJ.AsFloat        :=qryCertidaoFETJ.AsFloat+dm.TitulosFETJ.AsFloat;
          RXFUND.AsFloat        :=qryCertidaoFUNDPERJ.AsFloat+dm.TitulosFUNDPERJ.AsFloat;
          RXFUNP.AsFloat        :=qryCertidaoFUNPERJ.AsFloat+dm.TitulosFUNPERJ.AsFloat;
          RXFUNA.AsFloat        :=qryCertidaoFUNARPEN.AsFloat+dm.TitulosFUNARPEN.AsFloat;
          RXMUTUA.AsFloat       :=qryCertidaoMUTUA.AsFloat+dm.TitulosMUTUA.AsFloat;
          RXACOTERJ.AsFloat     :=qryCertidaoACOTERJ.AsFloat+dm.TitulosACOTERJ.AsFloat;
          RXDIST.AsFloat        :=qryCertidaoDISTRIBUICAO.AsFloat+dm.TitulosDISTRIBUICAO.AsFloat;
          RXTOTAL.AsFloat       :=RXEMOL.AsFloat+RXFETJ.AsFloat+RXFUND.AsFloat+RXFUNP.AsFloat+RXFUNA.AsFloat+
                                  RXMUTUA.AsFloat+RXACOTERJ.AsFloat+RXDIST.AsFloat;

          RXPROTOCOLO.AsInteger :=dm.TitulosPROTOCOLO.AsInteger;
          RXLIVRO.AsString      :=dm.TitulosLIVRO_REGISTRO.AsString;
          RXFOLHA.AsString      :=dm.TitulosFOLHA_REGISTRO.AsString;
          RXSTATUS.AsString     :='CANCELADO';
          RXSELO.AsString       :=dm.TitulosSELO_PAGAMENTO.AsString;
      end;

      RX.Post;
      qryCertidao.Next;
  end;
end;

procedure TFLivroAdicional.GravarBoleto;
begin
  qryBoletos.Last;
  P.Max:=qryBoletos.RecordCount;
  P.Position:=0;
  qryBoletos.First;
  while not qryBoletos.Eof do
  begin
      Application.ProcessMessages;
      P.StepBy(1);
      RX.Append;
      RXID_ATO.AsInteger    :=qryBoletosID_BOLETO.AsInteger;
      RXDESCRICAO.AsString  :=PF.RetornarAdicional(4028);
      RXDATA.AsDateTime     :=edData.Date;
      RXGRATUITO.AsString   :='N';
      RXCONVENIO.AsString   :='N';
      RXCODIGO.AsInteger    :=4028;
      RXEMOL.AsFloat        :=qryBoletosEMOLUMENTOS.AsFloat;
      RXFETJ.AsFloat        :=qryBoletosFETJ.AsFloat;
      RXFUND.AsFloat        :=qryBoletosFUNDPERJ.AsFloat;
      RXFUNP.AsFloat        :=qryBoletosFUNPERJ.AsFloat;
      RXFUNA.AsFloat        :=qryBoletosFUNARPEN.AsFloat;
      RXMUTUA.AsFloat       :=0;
      RXACOTERJ.AsFloat     :=0;
      RXDIST.AsFloat        :=0;
      RXTOTAL.AsFloat       :=qryBoletosTOTAL.AsFloat-qryBoletosPMCMV.AsFloat;
      RXSTATUS.AsString     :='BOLETO';
      RXPROTOCOLO.AsInteger :=qryBoletosPROTOCOLO.AsInteger;
      RX.Post;
      qryBoletos.Next;
  end;
end;

procedure TFLivroAdicional.btVisualizarClick(Sender: TObject);
begin
  PF.Aguarde(True);
  RX.Close;
  RX.Open;
  RX.DisableControls;
  P.Visible:=True;
  lbEtapa.Visible:=True;
  lbEtapa.Caption:='Etapa: 1/7';
  qryXml.Close;
  qryXml.SQL.Text:='SELECT * FROM TITULOS WHERE DT_PROTOCOLO=:D ORDER BY PROTOCOLO';
  qryXml.ParamByName('D').AsDate:=edData.Date;
  qryXml.Open;
  GravarRX('*','APONTADO');

  lbEtapa.Caption:='Etapa: 2/7';
  Application.ProcessMessages;
  qryXml.Close;
  qryXml.SQL.Text:='SELECT * FROM TITULOS WHERE DT_PAGAMENTO=:D AND STATUS='''+'PAGO'+''' ORDER BY PROTOCOLO';
  qryXml.ParamByName('D').AsDate:=edData.Date;
  qryXml.Open;
  GravarRX('P','PAGO');

  lbEtapa.Caption:='Etapa: 3/7';
  Application.ProcessMessages;
  qryXml.Close;
  qryXml.SQL.Text:='SELECT * FROM TITULOS WHERE DT_REGISTRO=:D AND PROTESTADO='''+'S'+''' ORDER BY PROTOCOLO';
  qryXml.ParamByName('D').AsDate:=edData.Date;
  qryXml.Open;
  GravarRX('R','PROTESTADO');

  lbEtapa.Caption:='Etapa: 4/7';
  Application.ProcessMessages;
  qryXml.Close;
  qryXml.SQL.Text:='SELECT * FROM TITULOS WHERE DT_PAGAMENTO=:D AND STATUS='''+'CANCELADO'+''' ORDER BY PROTOCOLO';
  qryXml.ParamByName('D').AsDate:=edData.Date;
  qryXml.Open;
  GravarRX('P','CANCELADO');

  lbEtapa.Caption:='Etapa: 5/7';
  Application.ProcessMessages;
  qryXml.Close;
  qryXml.SQL.Text:='SELECT * FROM TITULOS WHERE DT_RETIRADO=:D AND STATUS='''+'RETIRADO'+''' ORDER BY PROTOCOLO';
  qryXml.ParamByName('D').AsDate:=edData.Date;
  qryXml.Open;
  GravarRX('*','DESISTÊNCIA');

  lbEtapa.Caption:='Etapa: 6/7';
  Application.ProcessMessages;
  qryBoletos.Close;
  qryBoletos.ParamByName('D1').AsDate:=edData.Date;
  qryBoletos.Open;
  GravarBoleto;

  lbEtapa.Caption:='Etapa: 7/7';
  Application.ProcessMessages;
  qryCertidao.Close;
  qryCertidao.ParamByName('D1').AsDate:=edData.Date;
  qryCertidao.Open;
  GravarCertidao;

  RX.First;
  RX.EnableControls;
  P.Visible:=False;
  lbEtapa.Visible:=False;
  PF.Aguarde(False);
end;

end.
