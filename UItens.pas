unit UItens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DBGrids, Mask, DBCtrls, sDBEdit,
  sSpeedButton, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, sPanel, sLabel, sEdit,
  DB, sSpinEdit, sBitBtn, sMaskEdit, sCustomComboEdit, sCurrEdit,
  sDBCalcEdit, FMTBcd, SqlExpr, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFItens = class(TForm)
    P2: TsPanel;
    Grid2: TwwDBGrid;
    edEmolumentos: TsDBEdit;
    edFetj: TsDBEdit;
    edFundperj: TsDBEdit;
    edFunperj: TsDBEdit;
    edMutua: TsDBEdit;
    edAcoterj: TsDBEdit;
    edDistribuicao: TsDBEdit;
    P1: TsPanel;
    Grid1: TwwDBGrid;
    dsTabela: TDataSource;
    edTabela: TsEdit;
    edItem: TsEdit;
    sLabel1: TsLabel;
    ImAviso: TImage;
    edVlr: TsDBEdit;
    spQtd: TsSpinEdit;
    btAdicionar: TsBitBtn;
    btRemover: TsBitBtn;
    dsItens: TDataSource;
    dsCodigos: TDataSource;
    btConcluir: TsBitBtn;
    P3: TsPanel;
    sbOk: TsSpeedButton;
    sbCancelar: TsSpeedButton;
    edQuantidade: TsDBEdit;
    edValor: TsDBEdit;
    edCodigo: TsEdit;
    btOk: TsBitBtn;
    btCalcular: TsBitBtn;
    edFunarpen: TsDBEdit;
    edPMCMV: TsDBEdit;
    edTotal: TsDBEdit;
    edIss: TsDBEdit;
    qryCopiar: TFDQuery;
    qryCopiarID_CUS: TIntegerField;
    qryCopiarCOD: TIntegerField;
    qryCopiarTAB: TStringField;
    qryCopiarITEM: TStringField;
    qryCopiarSUB: TStringField;
    qryCopiarDESCR: TStringField;
    qryCopiarVALOR: TFloatField;
    qryCopiarQTD: TStringField;
    qryCopiarTOTAL: TFloatField;
    qryCopiarCOMP: TStringField;
    qryCopiarEXC: TStringField;
    procedure sbOkClick(Sender: TObject);
    procedure Grid2DblClick(Sender: TObject);
    procedure sbCancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btConcluirClick(Sender: TObject);
    procedure btAdicionarClick(Sender: TObject);
    procedure btRemoverClick(Sender: TObject);
    procedure Calcular;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edTabelaChange(Sender: TObject);
    procedure edItemChange(Sender: TObject);
    procedure Grid1DblClick(Sender: TObject);
    procedure edCodigoKeyPress(Sender: TObject; var Key: Char);
    procedure btOkClick(Sender: TObject);
    procedure btCalcularClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FItens: TFItens;

implementation

uses UDM, UPF, UGeral, UGDM;

{$R *.dfm}

procedure TFItens.sbOkClick(Sender: TObject);
begin
  dm.Itens.Post;
  dm.Itens.ApplyUpdates(0);

  P1.Enabled:=True;
  P2.Enabled:=True;
  P3.Visible:=False;
end;

procedure TFItens.Grid2DblClick(Sender: TObject);
begin
  if dm.Itens.IsEmpty then Exit;

  P1.Enabled:=False;
  P2.Enabled:=False;
  P3.Visible:=True;
  edQuantidade.SetFocus;
  dm.Itens.Edit;
end;

procedure TFItens.sbCancelarClick(Sender: TObject);
begin
  dm.Itens.Cancel;

  P1.Enabled:=True;
  P2.Enabled:=True;
  P3.Visible:=False;
end;

procedure TFItens.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_F5 then Calcular;
end;

procedure TFItens.btConcluirClick(Sender: TObject);
begin
  Close;
end;

procedure TFItens.btAdicionarClick(Sender: TObject);
begin
  dm.Itens.Append;
  dm.ItensID_CUS.AsInteger  :=dm.IdAtualBancao('ID_CUS');
  dm.ItensCOD.AsInteger     :=dm.CodigosCOD.AsInteger;
  dm.ItensTAB.AsString      :=dm.TabelaTAB.AsString;
  dm.ItensITEM.AsString     :=dm.TabelaITEM.AsString;
  dm.ItensSUB.AsString      :=dm.TabelaSUB.AsString;
  dm.ItensDESCR.AsString    :=dm.TabelaDESCR.AsString;
  dm.ItensVALOR.AsFloat     :=dm.TabelaVALOR.AsFloat;
  dm.ItensQTD.AsString      :=spQtd.Text;
  dm.ItensTOTAL.AsFloat     :=dm.TabelaVALOR.AsFloat*spQtd.Value;
  dm.Itens.Post;
  dm.Itens.ApplyUpdates(0);

  Calcular;
end;

procedure TFItens.btRemoverClick(Sender: TObject);
begin
  if dm.Itens.IsEmpty then Exit;

  dm.Itens.Delete;
  dm.Itens.ApplyUpdates(0);

  Calcular;
end;

procedure TFItens.Calcular;
var
  Posicao: Integer;
  Soma,Comuns: Double;
begin
  Posicao:=dm.Itens.RecNo;
  dm.Itens.DisableControls;
  dm.Itens.First;
  Soma:=0;
  Comuns:=0;
  while not dm.Itens.Eof do
  begin
      if dm.ItensTAB.AsString='16' then
      Comuns:=Comuns+dm.ItensTOTAL.AsFloat;
      Soma:=Soma+dm.ItensTOTAL.AsFloat;
      dm.Itens.Next;
  end;

  dm.CodigosEMOL.AsFloat  :=Soma;
  dm.CodigosFETJ.AsFloat  :=GR.NoRound(Soma*0.2,2);
  dm.CodigosFUND.AsFloat  :=GR.NoRound(Soma*0.05,2);
  dm.CodigosFUNP.AsFloat  :=GR.NoRound(Soma*0.05,2);
  dm.CodigosFUNA.AsFloat  :=GR.NoRound(Soma*0.04,2);
  dm.CodigosISS.AsFloat   :=GR.NoRound(Soma*Gdm.vAliquotaISS,2);
  dm.CodigosPMCMV.AsFloat :=GR.NoRound((Soma-Comuns)*0.02,2);

  dm.CodigosTOT.AsFloat   :=dm.CodigosEMOL.AsFloat+
                            dm.CodigosFETJ.AsFloat+
                            dm.CodigosFUND.AsFloat+
                            dm.CodigosFUNP.AsFloat+
                            dm.CodigosFUNA.AsFloat+
                            dm.CodigosPMCMV.AsFloat+
                            dm.CodigosISS.AsFloat+
                            dm.CodigosMUTUA.AsFloat+
                            dm.CodigosACOTERJ.AsFloat+
                            dm.CodigosDISTRIB.AsFloat;
  dm.Itens.RecNo:=Posicao;
  dm.Itens.EnableControls;
end;

procedure TFItens.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.Tabela.ApplyUpdates(0);
  dm.Codigos.ApplyUpdates(0);
end;

procedure TFItens.edTabelaChange(Sender: TObject);
begin
   if edTabela.Text='' then Exit;
   dm.Tabela.Locate('TAB',edTabela.Text,[loCaseInsensitive, loPartialKey]);
end;

procedure TFItens.edItemChange(Sender: TObject);
begin
  if (edTabela.Text='') and (edItem.Text='')  then Exit;
  dm.Tabela.Locate('TAB;ITEM',VarArrayOf([edTabela.Text,edItem.Text]),[loCaseInsensitive, loPartialKey]);
end;

procedure TFItens.Grid1DblClick(Sender: TObject);
begin
  btAdicionarClick(Sender);
end;

procedure TFItens.edCodigoKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFItens.btOkClick(Sender: TObject);
begin
  if edCodigo.Text='' then Exit;

  qryCopiar.DisableControls;
  qryCopiar.Close;
  qryCopiar.Params[0].AsInteger:=StrToInt(edCodigo.Text);
  qryCopiar.Open;
  if not qryCopiar.IsEmpty then
  begin
      dm.Itens.DisableControls;
      dm.Itens.First;
      while not dm.Itens.Eof do
      begin
          dm.Itens.Delete;
          dm.Itens.ApplyUpdates(0);
      end;
      qryCopiar.First;
      while not qryCopiar.Eof do
      begin
          dm.Itens.Append;
          dm.ItensID_CUS.AsInteger  :=dm.IdAtualBancao('ID_CUS');
          dm.ItensCOD.AsInteger     :=dm.CodigosCOD.AsInteger;
          dm.ItensTAB.AsString      :=qryCopiarTAB.AsString;
          dm.ItensITEM.AsString     :=qryCopiarITEM.AsString;
          dm.ItensSUB.AsString      :=qryCopiarSUB.AsString;
          dm.ItensDESCR.AsString    :=qryCopiarDESCR.AsString;
          dm.ItensVALOR.AsFloat     :=qryCopiarVALOR.AsFloat;
          dm.ItensQTD.AsString      :=qryCopiarQTD.AsString;
          dm.ItensTOTAL.AsFloat     :=qryCopiarVALOR.AsFloat*qryCopiarQtd.AsInteger;
          dm.Itens.Post;
          dm.Itens.ApplyUpdates(0);
          qryCopiar.Next;
      end;
      qryCopiar.EnableControls;
      dm.Itens.EnableControls;
      dm.Itens.First;
      if GR.Pergunta('Adicionar M�tua e Acoterj') then
      begin
          dm.CodigosMUTUA.AsFloat:=StrToFloat(dm.ValorParametro(26));
          dm.CodigosACOTERJ.AsFloat:=StrToFloat(dm.ValorParametro(27));
      end;
      Calcular;
  end;
end;

procedure TFItens.btCalcularClick(Sender: TObject);
begin
  Calcular;
end;

end.
