unit UConsultaPortador;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, sEdit, DB, Grids, Wwdbigrd,
  Wwdbgrid, ExtCtrls, sPanel;

type
  TFConsultaPortador = class(TForm)
    P1: TsPanel;
    GridPortadores: TwwDBGrid;
    dsPortadores: TDataSource;
    edLocalizar: TsEdit;
    btSelecionar: TsBitBtn;
    btVoltar: TsBitBtn;
    procedure edLocalizarKeyPress(Sender: TObject; var Key: Char);
    procedure btVoltarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure btSelecionarClick(Sender: TObject);
    procedure GridPortadoresDblClick(Sender: TObject);
    procedure GridPortadoresKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FConsultaPortador: TFConsultaPortador;

implementation

uses UPF, UDM;

{$R *.dfm}

procedure TFConsultaPortador.edLocalizarKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key=#13 then
  begin
      if edLocalizar.Text='' then
      begin
          dm.Portadores.Close;
          Exit;
      end;

      PF.Aguarde(True);
      dm.Portadores.Close;
      dm.Portadores.CommandText:='select * from PORTADORES where NOME like:N order by NOME';
      dm.Portadores.Params.ParamByName('N').AsString:=edLocalizar.Text+'%';
      dm.Portadores.Open;
      PF.Aguarde(False);
  end;
end;

procedure TFConsultaPortador.btVoltarClick(Sender: TObject);
begin
  Close;
end;

procedure TFConsultaPortador.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;

  if (key=VK_UP) or (key=VK_DOWN) then GridPortadores.SetFocus; 
end;

procedure TFConsultaPortador.FormCreate(Sender: TObject);
begin
  dm.vOkGeral:=False;
end;

procedure TFConsultaPortador.btSelecionarClick(Sender: TObject);
begin
  if not dm.Portadores.IsEmpty then dm.vOkGeral:=True;
  Close;
end;

procedure TFConsultaPortador.GridPortadoresDblClick(Sender: TObject);
begin
  btSelecionarClick(Sender);
end;

procedure TFConsultaPortador.GridPortadoresKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key=#13 then btSelecionarClick(Sender);
end;

end.
