object FControle: TFControle
  Left = 339
  Top = 214
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Configura'#231#245'es'
  ClientHeight = 182
  ClientWidth = 386
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 15
  object btOk: TsBitBtn
    Left = 225
    Top = 148
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Caption = 'Ok'
    TabOrder = 0
    OnClick = btOkClick
    SkinData.SkinSection = 'BUTTON'
  end
  object btCancelar: TsBitBtn
    Left = 304
    Top = 148
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Caption = 'Cancelar'
    TabOrder = 1
    OnClick = btCancelarClick
    SkinData.SkinSection = 'BUTTON'
  end
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 386
    Height = 137
    Align = alTop
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object Grid: TwwDBGrid
      Left = 1
      Top = 1
      Width = 384
      Height = 135
      Selected.Strings = (
        'DESCRICAO'#9'34'#9'Descri'#231#227'o'#9'F'
        'VALOR'#9'16'#9'Valor'#9'F')
      IniAttributes.Delimiter = ';;'
      IniAttributes.UnicodeIniFile = False
      TitleColor = clBtnFace
      FixedCols = 1
      ShowHorzScrollBar = True
      Align = alClient
      BorderStyle = bsNone
      DataSource = dsControle
      KeyOptions = []
      Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'Arial'
      TitleFont.Style = []
      TitleLines = 1
      TitleButtons = False
      UseTFields = False
    end
  end
  object dsControle: TDataSource
    DataSet = qryControle
    Left = 88
    Top = 64
  end
  object qryControle: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM CONTROLE')
    Left = 24
    Top = 64
    object qryControleID_CONTROLE: TIntegerField
      FieldName = 'ID_CONTROLE'
      Origin = 'ID_CONTROLE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryControleSIGLA: TStringField
      FieldName = 'SIGLA'
      Origin = 'SIGLA'
      Required = True
      Size = 5
    end
    object qryControleDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Required = True
      Size = 30
    end
    object qryControleVALOR: TIntegerField
      FieldName = 'VALOR'
      Origin = 'VALOR'
      Required = True
    end
    object qryControleLETRA: TStringField
      FieldName = 'LETRA'
      Origin = 'LETRA'
      Size = 5
    end
  end
end
