object FRelOrdem: TFRelOrdem
  Left = 375
  Top = 286
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Ordem de Protesto'
  ClientHeight = 314
  ClientWidth = 480
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesigned
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 15
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 480
    Height = 277
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object gbPeriodoProtocolo: TsGroupBox
      Left = 10
      Top = 10
      Width = 459
      Height = 58
      Caption = 'Per'#237'odo do Protocolo'
      TabOrder = 0
      SkinData.SkinSection = 'GROUPBOX'
      object dteDataInicial: TsDateEdit
        Left = 46
        Top = 24
        Width = 100
        Height = 21
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
        Text = '  /  /    '
        OnExit = dteDataInicialExit
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Inicial'
        GlyphMode.Grayed = False
        GlyphMode.Blend = 0
      end
      object dteDataFinal: TsDateEdit
        Left = 184
        Top = 24
        Width = 100
        Height = 21
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '  /  /    '
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Final'
        GlyphMode.Grayed = False
        GlyphMode.Blend = 0
      end
    end
    object gbFaixaProtocolo: TsGroupBox
      Left = 10
      Top = 74
      Width = 459
      Height = 58
      Caption = 'Faixa de Protocolo'
      TabOrder = 1
      SkinData.SkinSection = 'GROUPBOX'
      object edtNumProtocoloInicial: TsEdit
        Left = 46
        Top = 24
        Width = 100
        Height = 23
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnExit = edtNumProtocoloInicialExit
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Inicial'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
      end
      object edtNumProtocoloFinal: TsEdit
        Left = 184
        Top = 24
        Width = 100
        Height = 23
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Final'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
      end
    end
    object gbPortador: TsGroupBox
      Left = 10
      Top = 138
      Width = 459
      Height = 58
      Caption = 'Portador'
      TabOrder = 2
      SkinData.SkinSection = 'GROUPBOX'
      object edtCodigoPortador: TsEdit
        Left = 46
        Top = 24
        Width = 60
        Height = 23
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'C'#243'd.'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
      end
      object edtNomePortador: TsEdit
        Left = 151
        Top = 24
        Width = 298
        Height = 23
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Nome'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
      end
      object chbTrocarPortadorCedente: TsCheckBox
        Left = 15
        Top = 46
        Width = 306
        Height = 18
        Caption = 'Trocar o nome do Portador pelo nome do Cedente'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        Visible = False
        ImgChecked = 0
        ImgUnchecked = 0
        SkinData.SkinSection = 'CHECKBOX'
      end
    end
    object gbDevedor: TsGroupBox
      Left = 10
      Top = 203
      Width = 459
      Height = 58
      Caption = 'Devedor'
      TabOrder = 3
      SkinData.SkinSection = 'GROUPBOX'
      object edtNomeDevedor: TsEdit
        Left = 46
        Top = 24
        Width = 403
        Height = 23
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 4473924
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Nome'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
      end
    end
  end
  object btVisualizar: TsBitBtn
    Left = 298
    Top = 284
    Width = 91
    Height = 25
    Cursor = crHandPoint
    Caption = 'Visualizar'
    TabOrder = 1
    OnClick = btVisualizarClick
    ImageIndex = 4
    Images = dm.Imagens
    SkinData.SkinSection = 'BUTTON'
  end
  object btSair: TsBitBtn
    Left = 394
    Top = 284
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Caption = 'Sair'
    TabOrder = 2
    OnClick = btSairClick
    ImageIndex = 7
    Images = dm.Imagens
    SkinData.SkinSection = 'BUTTON'
  end
  object Protocolos: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT T.*,'
      '       (CASE WHEN (T.CODIGO_APRESENTANTE = '#39'E19'#39')'
      '             THEN T.CEDENTE'
      '             ELSE T.APRESENTANTE'
      '        END) AS NOME_APRESENTANTE'
      '  FROM TITULOS T')
    Left = 216
    Top = 262
    object ProtocolosID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ProtocolosCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
    end
    object ProtocolosRECIBO: TIntegerField
      FieldName = 'RECIBO'
      Origin = 'RECIBO'
    end
    object ProtocolosDT_ENTRADA: TDateField
      FieldName = 'DT_ENTRADA'
      Origin = 'DT_ENTRADA'
    end
    object ProtocolosDT_PROTOCOLO: TDateField
      FieldName = 'DT_PROTOCOLO'
      Origin = 'DT_PROTOCOLO'
    end
    object ProtocolosPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object ProtocolosLIVRO_PROTOCOLO: TIntegerField
      FieldName = 'LIVRO_PROTOCOLO'
      Origin = 'LIVRO_PROTOCOLO'
    end
    object ProtocolosFOLHA_PROTOCOLO: TStringField
      FieldName = 'FOLHA_PROTOCOLO'
      Origin = 'FOLHA_PROTOCOLO'
      Size = 10
    end
    object ProtocolosDT_PRAZO: TDateField
      FieldName = 'DT_PRAZO'
      Origin = 'DT_PRAZO'
    end
    object ProtocolosDT_REGISTRO: TDateField
      FieldName = 'DT_REGISTRO'
      Origin = 'DT_REGISTRO'
    end
    object ProtocolosREGISTRO: TIntegerField
      FieldName = 'REGISTRO'
      Origin = 'REGISTRO'
    end
    object ProtocolosLIVRO_REGISTRO: TIntegerField
      FieldName = 'LIVRO_REGISTRO'
      Origin = 'LIVRO_REGISTRO'
    end
    object ProtocolosFOLHA_REGISTRO: TStringField
      FieldName = 'FOLHA_REGISTRO'
      Origin = 'FOLHA_REGISTRO'
      Size = 10
    end
    object ProtocolosSELO_REGISTRO: TStringField
      FieldName = 'SELO_REGISTRO'
      Origin = 'SELO_REGISTRO'
      Size = 9
    end
    object ProtocolosDT_PAGAMENTO: TDateField
      FieldName = 'DT_PAGAMENTO'
      Origin = 'DT_PAGAMENTO'
    end
    object ProtocolosSELO_PAGAMENTO: TStringField
      FieldName = 'SELO_PAGAMENTO'
      Origin = 'SELO_PAGAMENTO'
      Size = 9
    end
    object ProtocolosRECIBO_PAGAMENTO: TIntegerField
      FieldName = 'RECIBO_PAGAMENTO'
      Origin = 'RECIBO_PAGAMENTO'
    end
    object ProtocolosCOBRANCA: TStringField
      FieldName = 'COBRANCA'
      Origin = 'COBRANCA'
      FixedChar = True
      Size = 2
    end
    object ProtocolosEMOLUMENTOS: TFloatField
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
    end
    object ProtocolosFETJ: TFloatField
      FieldName = 'FETJ'
      Origin = 'FETJ'
    end
    object ProtocolosFUNDPERJ: TFloatField
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
    end
    object ProtocolosFUNPERJ: TFloatField
      FieldName = 'FUNPERJ'
      Origin = 'FUNPERJ'
    end
    object ProtocolosFUNARPEN: TFloatField
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
    end
    object ProtocolosPMCMV: TFloatField
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
    end
    object ProtocolosISS: TFloatField
      FieldName = 'ISS'
      Origin = 'ISS'
    end
    object ProtocolosMUTUA: TFloatField
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
    end
    object ProtocolosDISTRIBUICAO: TFloatField
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
    end
    object ProtocolosACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
    end
    object ProtocolosTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
    end
    object ProtocolosTIPO_PROTESTO: TIntegerField
      FieldName = 'TIPO_PROTESTO'
      Origin = 'TIPO_PROTESTO'
    end
    object ProtocolosTIPO_TITULO: TIntegerField
      FieldName = 'TIPO_TITULO'
      Origin = 'TIPO_TITULO'
    end
    object ProtocolosNUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
      Origin = 'NUMERO_TITULO'
    end
    object ProtocolosDT_TITULO: TDateField
      FieldName = 'DT_TITULO'
      Origin = 'DT_TITULO'
    end
    object ProtocolosBANCO: TStringField
      FieldName = 'BANCO'
      Origin = 'BANCO'
    end
    object ProtocolosAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Origin = 'AGENCIA'
      Size = 10
    end
    object ProtocolosCONTA: TStringField
      FieldName = 'CONTA'
      Origin = 'CONTA'
      Size = 10
    end
    object ProtocolosVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
      Origin = 'VALOR_TITULO'
    end
    object ProtocolosSALDO_PROTESTO: TFloatField
      FieldName = 'SALDO_PROTESTO'
      Origin = 'SALDO_PROTESTO'
    end
    object ProtocolosDT_VENCIMENTO: TDateField
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
    end
    object ProtocolosCONVENIO: TStringField
      FieldName = 'CONVENIO'
      Origin = 'CONVENIO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosTIPO_APRESENTACAO: TStringField
      FieldName = 'TIPO_APRESENTACAO'
      Origin = 'TIPO_APRESENTACAO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosDT_ENVIO: TDateField
      FieldName = 'DT_ENVIO'
      Origin = 'DT_ENVIO'
    end
    object ProtocolosCODIGO_APRESENTANTE: TStringField
      FieldName = 'CODIGO_APRESENTANTE'
      Origin = 'CODIGO_APRESENTANTE'
      Size = 10
    end
    object ProtocolosTIPO_INTIMACAO: TStringField
      FieldName = 'TIPO_INTIMACAO'
      Origin = 'TIPO_INTIMACAO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosMOTIVO_INTIMACAO: TMemoField
      FieldName = 'MOTIVO_INTIMACAO'
      Origin = 'MOTIVO_INTIMACAO'
      BlobType = ftMemo
    end
    object ProtocolosDT_INTIMACAO: TDateField
      FieldName = 'DT_INTIMACAO'
      Origin = 'DT_INTIMACAO'
    end
    object ProtocolosDT_PUBLICACAO: TDateField
      FieldName = 'DT_PUBLICACAO'
      Origin = 'DT_PUBLICACAO'
    end
    object ProtocolosVALOR_PAGAMENTO: TFloatField
      FieldName = 'VALOR_PAGAMENTO'
      Origin = 'VALOR_PAGAMENTO'
    end
    object ProtocolosAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Origin = 'APRESENTANTE'
      Size = 100
    end
    object ProtocolosCPF_CNPJ_APRESENTANTE: TStringField
      FieldName = 'CPF_CNPJ_APRESENTANTE'
      Origin = 'CPF_CNPJ_APRESENTANTE'
      Size = 15
    end
    object ProtocolosTIPO_APRESENTANTE: TStringField
      FieldName = 'TIPO_APRESENTANTE'
      Origin = 'TIPO_APRESENTANTE'
      FixedChar = True
      Size = 1
    end
    object ProtocolosCEDENTE: TStringField
      FieldName = 'CEDENTE'
      Origin = 'CEDENTE'
      Size = 100
    end
    object ProtocolosNOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Origin = 'NOSSO_NUMERO'
      Size = 15
    end
    object ProtocolosSACADOR: TStringField
      FieldName = 'SACADOR'
      Origin = 'SACADOR'
      Size = 100
    end
    object ProtocolosDEVEDOR: TStringField
      FieldName = 'DEVEDOR'
      Origin = 'DEVEDOR'
      Size = 100
    end
    object ProtocolosCPF_CNPJ_DEVEDOR: TStringField
      FieldName = 'CPF_CNPJ_DEVEDOR'
      Origin = 'CPF_CNPJ_DEVEDOR'
      Size = 15
    end
    object ProtocolosTIPO_DEVEDOR: TStringField
      FieldName = 'TIPO_DEVEDOR'
      Origin = 'TIPO_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object ProtocolosAGENCIA_CEDENTE: TStringField
      FieldName = 'AGENCIA_CEDENTE'
      Origin = 'AGENCIA_CEDENTE'
      Size = 15
    end
    object ProtocolosPRACA_PROTESTO: TStringField
      FieldName = 'PRACA_PROTESTO'
      Origin = 'PRACA_PROTESTO'
    end
    object ProtocolosTIPO_ENDOSSO: TStringField
      FieldName = 'TIPO_ENDOSSO'
      Origin = 'TIPO_ENDOSSO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosACEITE: TStringField
      FieldName = 'ACEITE'
      Origin = 'ACEITE'
      FixedChar = True
      Size = 1
    end
    object ProtocolosCPF_ESCREVENTE: TStringField
      FieldName = 'CPF_ESCREVENTE'
      Origin = 'CPF_ESCREVENTE'
      Size = 11
    end
    object ProtocolosCPF_ESCREVENTE_PG: TStringField
      FieldName = 'CPF_ESCREVENTE_PG'
      Origin = 'CPF_ESCREVENTE_PG'
      Size = 11
    end
    object ProtocolosOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      BlobType = ftMemo
    end
    object ProtocolosDT_SUSTADO: TDateField
      FieldName = 'DT_SUSTADO'
      Origin = 'DT_SUSTADO'
    end
    object ProtocolosDT_RETIRADO: TDateField
      FieldName = 'DT_RETIRADO'
      Origin = 'DT_RETIRADO'
    end
    object ProtocolosSTATUS: TStringField
      FieldName = 'STATUS'
      Origin = 'STATUS'
    end
    object ProtocolosPROTESTADO: TStringField
      FieldName = 'PROTESTADO'
      Origin = 'PROTESTADO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosENVIADO_APONTAMENTO: TStringField
      FieldName = 'ENVIADO_APONTAMENTO'
      Origin = 'ENVIADO_APONTAMENTO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosENVIADO_PROTESTO: TStringField
      FieldName = 'ENVIADO_PROTESTO'
      Origin = 'ENVIADO_PROTESTO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosENVIADO_PAGAMENTO: TStringField
      FieldName = 'ENVIADO_PAGAMENTO'
      Origin = 'ENVIADO_PAGAMENTO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosENVIADO_RETIRADO: TStringField
      FieldName = 'ENVIADO_RETIRADO'
      Origin = 'ENVIADO_RETIRADO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosENVIADO_SUSTADO: TStringField
      FieldName = 'ENVIADO_SUSTADO'
      Origin = 'ENVIADO_SUSTADO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosENVIADO_DEVOLVIDO: TStringField
      FieldName = 'ENVIADO_DEVOLVIDO'
      Origin = 'ENVIADO_DEVOLVIDO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosEXPORTADO: TStringField
      FieldName = 'EXPORTADO'
      Origin = 'EXPORTADO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosAVALISTA_DEVEDOR: TStringField
      FieldName = 'AVALISTA_DEVEDOR'
      Origin = 'AVALISTA_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object ProtocolosFINS_FALIMENTARES: TStringField
      FieldName = 'FINS_FALIMENTARES'
      Origin = 'FINS_FALIMENTARES'
      FixedChar = True
      Size = 1
    end
    object ProtocolosTARIFA_BANCARIA: TFloatField
      FieldName = 'TARIFA_BANCARIA'
      Origin = 'TARIFA_BANCARIA'
    end
    object ProtocolosFORMA_PAGAMENTO: TStringField
      FieldName = 'FORMA_PAGAMENTO'
      Origin = 'FORMA_PAGAMENTO'
      FixedChar = True
      Size = 2
    end
    object ProtocolosNUMERO_PAGAMENTO: TStringField
      FieldName = 'NUMERO_PAGAMENTO'
      Origin = 'NUMERO_PAGAMENTO'
      Size = 40
    end
    object ProtocolosARQUIVO: TStringField
      FieldName = 'ARQUIVO'
      Origin = 'ARQUIVO'
    end
    object ProtocolosRETORNO: TStringField
      FieldName = 'RETORNO'
      Origin = 'RETORNO'
    end
    object ProtocolosDT_DEVOLVIDO: TDateField
      FieldName = 'DT_DEVOLVIDO'
      Origin = 'DT_DEVOLVIDO'
    end
    object ProtocolosCPF_CNPJ_SACADOR: TStringField
      FieldName = 'CPF_CNPJ_SACADOR'
      Origin = 'CPF_CNPJ_SACADOR'
      Size = 14
    end
    object ProtocolosID_MSG: TIntegerField
      FieldName = 'ID_MSG'
      Origin = 'ID_MSG'
    end
    object ProtocolosVALOR_AR: TFloatField
      FieldName = 'VALOR_AR'
      Origin = 'VALOR_AR'
    end
    object ProtocolosELETRONICO: TStringField
      FieldName = 'ELETRONICO'
      Origin = 'ELETRONICO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosIRREGULARIDADE: TIntegerField
      FieldName = 'IRREGULARIDADE'
      Origin = 'IRREGULARIDADE'
    end
    object ProtocolosAVISTA: TStringField
      FieldName = 'AVISTA'
      Origin = 'AVISTA'
      FixedChar = True
      Size = 1
    end
    object ProtocolosSALDO_TITULO: TFloatField
      FieldName = 'SALDO_TITULO'
      Origin = 'SALDO_TITULO'
    end
    object ProtocolosTIPO_SUSTACAO: TStringField
      FieldName = 'TIPO_SUSTACAO'
      Origin = 'TIPO_SUSTACAO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosRETORNO_PROTESTO: TStringField
      FieldName = 'RETORNO_PROTESTO'
      Origin = 'RETORNO_PROTESTO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosDT_RETORNO_PROTESTO: TDateField
      FieldName = 'DT_RETORNO_PROTESTO'
      Origin = 'DT_RETORNO_PROTESTO'
    end
    object ProtocolosDT_DEFINITIVA: TDateField
      FieldName = 'DT_DEFINITIVA'
      Origin = 'DT_DEFINITIVA'
    end
    object ProtocolosJUDICIAL: TStringField
      FieldName = 'JUDICIAL'
      Origin = 'JUDICIAL'
      FixedChar = True
      Size = 1
    end
    object ProtocolosALEATORIO_PROTESTO: TStringField
      FieldName = 'ALEATORIO_PROTESTO'
      Origin = 'ALEATORIO_PROTESTO'
      Size = 3
    end
    object ProtocolosALEATORIO_SOLUCAO: TStringField
      FieldName = 'ALEATORIO_SOLUCAO'
      Origin = 'ALEATORIO_SOLUCAO'
      Size = 3
    end
    object ProtocolosCCT: TStringField
      FieldName = 'CCT'
      Origin = 'CCT'
      Size = 9
    end
    object ProtocolosDETERMINACAO: TStringField
      FieldName = 'DETERMINACAO'
      Origin = 'DETERMINACAO'
      Size = 100
    end
    object ProtocolosANTIGO: TStringField
      FieldName = 'ANTIGO'
      Origin = 'ANTIGO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosLETRA: TStringField
      FieldName = 'LETRA'
      Origin = 'LETRA'
      Size = 1
    end
    object ProtocolosPROTOCOLO_CARTORIO: TIntegerField
      FieldName = 'PROTOCOLO_CARTORIO'
      Origin = 'PROTOCOLO_CARTORIO'
    end
    object ProtocolosARQUIVO_CARTORIO: TStringField
      FieldName = 'ARQUIVO_CARTORIO'
      Origin = 'ARQUIVO_CARTORIO'
    end
    object ProtocolosRETORNO_CARTORIO: TStringField
      FieldName = 'RETORNO_CARTORIO'
      Origin = 'RETORNO_CARTORIO'
    end
    object ProtocolosDT_DEVOLVIDO_CARTORIO: TDateField
      FieldName = 'DT_DEVOLVIDO_CARTORIO'
      Origin = 'DT_DEVOLVIDO_CARTORIO'
    end
    object ProtocolosCARTORIO: TIntegerField
      FieldName = 'CARTORIO'
      Origin = 'CARTORIO'
    end
    object ProtocolosDT_PROTOCOLO_CARTORIO: TDateField
      FieldName = 'DT_PROTOCOLO_CARTORIO'
      Origin = 'DT_PROTOCOLO_CARTORIO'
    end
    object ProtocolosDIAS_AVISTA: TIntegerField
      FieldName = 'DIAS_AVISTA'
      Origin = 'DIAS_AVISTA'
    end
    object ProtocolosFLG_SALDO: TStringField
      FieldName = 'FLG_SALDO'
      Origin = 'FLG_SALDO'
      FixedChar = True
      Size = 1
    end
    object ProtocolosNOME_APRESENTANTE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOME_APRESENTANTE'
      Origin = 'NOME_APRESENTANTE'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object ProtocolosPOSTECIPADO: TStringField
      FieldName = 'POSTECIPADO'
      Origin = 'POSTECIPADO'
      FixedChar = True
      Size = 1
    end
  end
end
