object FMovimento: TFMovimento
  Left = 360
  Top = 206
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'MOVIMENTO'
  ClientHeight = 178
  ClientWidth = 233
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 8
    Top = 8
    Width = 217
    Height = 121
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object edData: TsDBDateEdit
      Left = 120
      Top = 14
      Width = 86
      Height = 21
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 0
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.Caption = 'Data do Movimento'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      DataField = 'DATA'
      DataSource = dsMovimento
    end
    object edHora: TsDBEdit
      Left = 120
      Top = 48
      Width = 86
      Height = 23
      Color = clWhite
      DataField = 'HORA'
      DataSource = dsMovimento
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Hora do Movimento'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sDBDateEdit1: TsDBDateEdit
      Left = 120
      Top = 86
      Width = 86
      Height = 21
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.Caption = 'Data da Baixa'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      DataField = 'BAIXA'
      DataSource = dsMovimento
    end
  end
  object btSalvar: TsBitBtn
    Left = 52
    Top = 138
    Width = 85
    Height = 31
    Cursor = crHandPoint
    Caption = 'Salvar'
    TabOrder = 1
    OnClick = btSalvarClick
    SkinData.SkinSection = 'BUTTON'
    ImageIndex = 0
    Images = Gdm.Im16
  end
  object btCancelar: TsBitBtn
    Left = 140
    Top = 138
    Width = 85
    Height = 31
    Cursor = crHandPoint
    Caption = 'Cancelar'
    TabOrder = 2
    OnClick = btCancelarClick
    SkinData.SkinSection = 'BUTTON'
    ImageIndex = 1
    Images = Gdm.Im16
  end
  object dsMovimento: TDataSource
    DataSet = dm.Movimento
    Left = 296
    Top = 32
  end
end
