unit UQuickReciboPagamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, DB, DBClient, SimpleDS, StdCtrls,
  ComCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFQuickReciboPagamento = class(TForm)
    Desistencia: TQuickRep;
    QRBand1: TQRBand;
    lbCidade: TQRLabel;
    lbTitular: TQRLabel;
    lbCartorio: TQRLabel;
    QRLabel2: TQRLabel;
    QRRichText1: TQRRichText;
    qrProtocolo: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel33: TQRLabel;
    lbProtocolo: TQRLabel;
    lbDevedor: TQRLabel;
    lbDocumento: TQRLabel;
    lbTitulo: TQRLabel;
    lbSacador: TQRLabel;
    lbCedente: TQRLabel;
    QRLabel5: TQRLabel;
    lbValor: TQRLabel;
    QRLabel12: TQRLabel;
    lbVencimento: TQRLabel;
    QRLabel19: TQRLabel;
    lbApresentante: TQRLabel;
    lbRecibo: TQRLabel;
    qrRecibo: TQRLabel;
    ChildBand1: TQRChildBand;
    lbData: TQRLabel;
    QRShape2: TQRShape;
    QRLabel3: TQRLabel;
    M: TQRMemo;
    QRLabel1: TQRLabel;
    RE: TRichEdit;
    Protocolo: TFDQuery;
    ProtocoloID_ATO: TIntegerField;
    ProtocoloPROTOCOLO: TIntegerField;
    ProtocoloTIPO_DEVEDOR: TStringField;
    ProtocoloDEVEDOR: TStringField;
    ProtocoloCPF_CNPJ_DEVEDOR: TStringField;
    ProtocoloNUMERO_TITULO: TStringField;
    ProtocoloSACADOR: TStringField;
    ProtocoloCEDENTE: TStringField;
    ProtocoloVALOR_TITULO: TFloatField;
    ProtocoloDT_VENCIMENTO: TDateField;
    ProtocoloAPRESENTANTE: TStringField;
    ProtocoloRECIBO_PAGAMENTO: TIntegerField;
    ProtocoloEMOLUMENTOS: TFloatField;
    ProtocoloFETJ: TFloatField;
    ProtocoloFUNDPERJ: TFloatField;
    ProtocoloFUNPERJ: TFloatField;
    ProtocoloFUNARPEN: TFloatField;
    ProtocoloPMCMV: TFloatField;
    ProtocoloISS: TFloatField;
    ProtocoloMUTUA: TFloatField;
    ProtocoloACOTERJ: TFloatField;
    ProtocoloTARIFA_BANCARIA: TFloatField;
    ProtocoloTOTAL: TFloatField;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickReciboPagamento: TFQuickReciboPagamento;

implementation

uses UDM;

{$R *.dfm}

procedure TFQuickReciboPagamento.FormCreate(Sender: TObject);
begin
  lbCartorio.Caption  :=dm.ServentiaDESCRICAO.AsString;
  lbCidade.Caption    :=dm.ServentiaCIDADE.AsString+' - Rio de Janeiro';
  lbTitular.Caption   :=dm.ServentiaTABELIAO.AsString+' - '+dm.ServentiaTITULO.AsString;
  lbData.Caption      :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',Now);
end;

end.
