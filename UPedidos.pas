unit UPedidos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FMTBcd, Grids, Wwdbigrd, Wwdbgrid, StdCtrls, sEdit, sComboBox,
  ExtCtrls, sPanel, Menus, DB, DBClient, Provider, SqlExpr, Mask,
  sMaskEdit, sCustomComboEdit, sTooledit, sRadioButton, Buttons, sBitBtn,
  sMonthCalendar, jpeg, Math;

type
  TFPedidos = class(TForm)
    dtsConsulta: TSQLDataSet;
    dtsConsultaDT_PEDIDO: TDateField;
    dtsConsultaRECIBO: TIntegerField;
    dtsConsultaREQUERIDO: TStringField;
    dtsConsultaSELO: TStringField;
    dtsConsultaCPF_CNPJ_REQUERIDO: TStringField;
    dtsConsultaTIPO_REQUERIDO: TStringField;
    dtsConsultaID_CERTIDAO: TIntegerField;
    dspConsulta: TDataSetProvider;
    Consulta: TClientDataSet;
    ConsultaDT_PEDIDO: TDateField;
    ConsultaRECIBO: TIntegerField;
    ConsultaREQUERIDO: TStringField;
    ConsultaSELO: TStringField;
    ConsultaCPF_CNPJ_REQUERIDO: TStringField;
    ConsultaDocumento: TStringField;
    ConsultaTIPO_REQUERIDO: TStringField;
    ConsultaID_CERTIDAO: TIntegerField;
    dsConsulta: TDataSource;
    PM: TPopupMenu;
    Excluir: TMenuItem;
    GridAtos: TwwDBGrid;
    ConsultaStatus: TStringField;
    P1: TsPanel;
    Rb1: TsRadioButton;
    Rb2: TsRadioButton;
    Rb0: TsRadioButton;
    sPanel1: TsPanel;
    btDetalhes: TsBitBtn;
    btPedido: TsBitBtn;
    btCertidao: TsBitBtn;
    cbBuscar: TsComboBox;
    edPesquisar: TsEdit;
    edData: TsDateEdit;
    dtsConsultaTIPO_CERTIDAO: TStringField;
    ConsultaTIPO_CERTIDAO: TStringField;
    ConsultaNomeCertidao: TStringField;
    dtsConsultaID_ATO: TIntegerField;
    ConsultaID_ATO: TIntegerField;
    btTitulo: TsBitBtn;
    btCancelamento: TsBitBtn;
    PA: TsPanel;
    Image1: TImage;
    M1: TMemo;
    M2: TMemo;
    dtsConsultaALEATORIO: TStringField;
    dtsConsultaCCT: TStringField;
    ConsultaALEATORIO: TStringField;
    ConsultaCCT: TStringField;
    ConsultaSeloEletronico: TStringField;
    L1: TMenuItem;
    N1: TMenuItem;
    procedure ConsultaCalcFields(DataSet: TDataSet);
    procedure edPesquisarKeyPress(Sender: TObject; var Key: Char);
    procedure edDataKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Rb0Click(Sender: TObject);
    procedure Rb1Click(Sender: TObject);
    procedure Rb2Click(Sender: TObject);
    procedure btDetalhesClick(Sender: TObject);
    procedure GridAtosDblClick(Sender: TObject);
    procedure btPedidoClick(Sender: TObject);
    procedure btCertidaoClick(Sender: TObject);
    procedure cbBuscarChange(Sender: TObject);
    procedure dsConsultaDataChange(Sender: TObject; Field: TField);
    procedure btTituloClick(Sender: TObject);
    procedure btCancelamentoClick(Sender: TObject);
    procedure GridAtosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ExcluirClick(Sender: TObject);
    procedure L1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPedidos: TFPedidos;

implementation

uses UPF, UDM, UCertidao, UQuickPedido1, UQuickCertidao1, UPedidoCertidao,
     UCancelado, UTitulo, UQuickCancelado2, UQuickCancelado1,
     UPrincipal, UGeral, UQuickCertidaoCancelamento1, UGDM, UCertidao2Via,
  StrUtils;

{$R *.dfm}

procedure TFPedidos.ConsultaCalcFields(DataSet: TDataSet);
begin
  if ConsultaSELO.AsString<>'' then ConsultaSeloEletronico.AsString:=GR.SeloFormatado(ConsultaSELO.AsString,ConsultaALEATORIO.AsString);

  if ConsultaSELO.AsString='' then
    ConsultaStatus.AsString:='Andamento'
      else ConsultaStatus.AsString:='Conclu�do';

  if ConsultaTIPO_REQUERIDO.AsString='F' then ConsultaDocumento.AsString:=PF.FormatarCPF(ConsultaCPF_CNPJ_REQUERIDO.AsString);
  if ConsultaTIPO_REQUERIDO.AsString='J' then ConsultaDocumento.AsString:=PF.FormatarCNPJ(ConsultaCPF_CNPJ_REQUERIDO.AsString);

  if ConsultaTIPO_CERTIDAO.AsString='N' then ConsultaNomeCertidao.AsString:='NORMAL';
  if ConsultaTIPO_CERTIDAO.AsString='E' then ConsultaNomeCertidao.AsString:='ESPECIAL';
  if ConsultaTIPO_CERTIDAO.AsString='C' then ConsultaNomeCertidao.AsString:='CADASTRO';
  if ConsultaTIPO_CERTIDAO.AsString='I' then ConsultaNomeCertidao.AsString:='INTEIRO TEOR';
  if ConsultaTIPO_CERTIDAO.AsString='R' then ConsultaNomeCertidao.AsString:='REVALIDA��O (VISTO)';
  if ConsultaTIPO_CERTIDAO.AsString='X' then ConsultaNomeCertidao.AsString:='CANCELAMENTO';
  if ConsultaTIPO_CERTIDAO.AsString='F' then ConsultaNomeCertidao.AsString:='2� VIA ('+dm.CampoTitulos('STATUS',ConsultaID_ATO.AsInteger)+')';
end;

procedure TFPedidos.edPesquisarKeyPress(Sender: TObject; var Key: Char);
var
  Query,SubQuery: String;
begin
  if dm.vCancelamento then
    SubQuery:=' TIPO_CERTIDAO='''+'X'+''' AND '
      else SubQuery:=' TIPO_CERTIDAO<>'''+'X'+''' AND ';
  if key=#13 then
  begin
      if edPesquisar.Text='' then Exit;

      if Rb0.Checked then
      Query:='SELECT ID_CERTIDAO,ID_ATO,DT_PEDIDO,RECIBO,SELO,ALEATORIO,CCT,REQUERIDO,CPF_CNPJ_REQUERIDO,TIPO_REQUERIDO,TIPO_CERTIDAO FROM CERTIDOES WHERE '+SubQuery;

      if Rb1.Checked then
      Query:='SELECT ID_CERTIDAO,ID_ATO,DT_PEDIDO,RECIBO,SELO,ALEATORIO,CCT,REQUERIDO,CPF_CNPJ_REQUERIDO,TIPO_REQUERIDO,TIPO_CERTIDAO FROM CERTIDOES WHERE SELO IS NULL AND '+SubQuery;

      if Rb2.Checked then
      Query:='SELECT ID_CERTIDAO,ID_ATO,DT_PEDIDO,RECIBO,SELO,ALEATORIO,CCT,REQUERIDO,CPF_CNPJ_REQUERIDO,TIPO_REQUERIDO,TIPO_CERTIDAO FROM CERTIDOES WHERE NOT (SELO IS NULL) AND '+SubQuery;

      if dm.vCancelamento then
        SubQuery:=' TIPO_CERTIDAO='''+'X'+''' AND '
          else SubQuery:=' (TIPO_CERTIDAO='''+'N'+''' OR TIPO_CERTIDAO='''+'I'+''' OR TIPO_CERTIDAO='''+'F'+''') AND ';

      Consulta.Close;

      {REQUERENTE}
      if cbBuscar.ItemIndex=0 then
      begin
          Consulta.CommandText:=Query+SubQuery+'REQUERENTE LIKE:P ORDER BY RECIBO';
          Consulta.Params.ParamByName('P').AsString:=edPesquisar.Text+'%';
      end;

      {REQUERIDO}
      if cbBuscar.ItemIndex=1 then
      begin
          Consulta.CommandText:=Query+SubQuery+'REQUERIDO LIKE:P ORDER BY RECIBO';
          Consulta.Params.ParamByName('P').AsString:=edPesquisar.Text+'%';
      end;

      {DOCUMENTO}
      if cbBuscar.ItemIndex=2 then
      begin
          Consulta.CommandText:=Query+SubQuery+'CPF_CNPJ_REQUERIDO=:P ORDER BY REQUERIDO';
          Consulta.Params.ParamByName('P').AsString:=edPesquisar.Text;
      end;

      {PROTOCOLO}
      if cbBuscar.ItemIndex=3 then
      begin
          Consulta.CommandText:=Query+SubQuery+'ID_ATO=:P ORDER BY RECIBO';
          Consulta.Params.ParamByName('P').AsInteger:=dm.fIdAto(edPesquisar.Text);
      end;

      {RECIBO}
      if cbBuscar.ItemIndex=4 then
      begin
          Consulta.CommandText:=Query+SubQuery+'RECIBO=:P ORDER BY RECIBO';
          Consulta.Params.ParamByName('P').AsInteger:=StrToInt(edPesquisar.Text);
      end;

      {SELO}
      if cbBuscar.ItemIndex=5 then
      begin
          Consulta.CommandText:=Query+SubQuery+'SELO=:P ORDER BY RECIBO';
          Consulta.Params.ParamByName('P').AsString:=edPesquisar.Text;
      end;

      Consulta.Open;
  end;
end;

procedure TFPedidos.edDataKeyPress(Sender: TObject; var Key: Char);
var
  vQuery,vSubQuery: String;
begin
  if key=#13 then
  begin
      vQuery:='SELECT ID_CERTIDAO,ID_ATO,DT_PEDIDO,RECIBO,SELO,ALEATORIO,CCT,REQUERIDO,CPF_CNPJ_REQUERIDO,TIPO_REQUERIDO,TIPO_CERTIDAO FROM CERTIDOES ';

      {TODOS}
      if Rb0.Checked then
      vQuery:=vQuery+' WHERE ';

      {ANDAMENTOS}
      if Rb1.Checked then
      vQuery:=vQuery+' WHERE SELO IS NULL AND ';

      {CONCLU�DOS}
      if Rb2.Checked then
      vQuery:=vQuery+' WHERE NOT (SELO IS NULL) AND ';

      if dm.vCancelamento then
        vSubQuery:=' TIPO_CERTIDAO='''+'X'+''' AND '
          else vSubQuery:=' (TIPO_CERTIDAO='''+'N'+''' OR TIPO_CERTIDAO='''+'I'+''' OR TIPO_CERTIDAO='''+'F'+''') AND ';

      Consulta.Close;
      Consulta.CommandText:=vQuery+vSubQuery+IfThen(cbBuscar.ItemIndex=6,'DT_PEDIDO','DT_CERTIDAO')+'='+
                            QuotedStr(FormatDateTime('mm/dd/yyyy',edData.Date))+' ORDER BY RECIBO';
      Consulta.Open;
  end;
end;

procedure TFPedidos.FormCreate(Sender: TObject);
var
  Query,SubQuery: String;
begin
  Query:='SELECT ID_CERTIDAO,ID_ATO,DT_PEDIDO,RECIBO,SELO,ALEATORIO,CCT,REQUERIDO,CPF_CNPJ_REQUERIDO,TIPO_REQUERIDO,TIPO_CERTIDAO FROM CERTIDOES WHERE ';

  if dm.vCancelamento then
  begin
      Self.Caption:='Pedidos de Cancelamento';
      SubQuery:=' TIPO_CERTIDAO='''+'X'+''' AND ';
      btCertidao.Caption    :='Efetuar Cancelamento';
      btCertidao.ImageIndex :=9;
      btCancelamento.Visible:=True;
      M1.Visible            :=False;
      M2.Visible            :=True;
  end
  else
  begin
      Self.Caption:='Pedidos de Certid�o';
      SubQuery:='(TIPO_CERTIDAO='''+'N'+''' OR TIPO_CERTIDAO='''+'I'+''' OR TIPO_CERTIDAO='''+'F'+''') AND ';
      btCertidao.Caption    :='Lan�ar Certid�o';
      btCertidao.ImageIndex :=10;
      M1.Visible            :=True;
      M2.Visible            :=False;
  end;

  PA.Visible:=FPrincipal.ckAssistente.Checked;

  Consulta.Close;
  Consulta.CommandText:=Query+SubQuery+'DT_PEDIDO = :D ORDER BY RECIBO';
  Consulta.Params.ParamByName('D').AsDate:=Now;
  Consulta.Open;
end;

procedure TFPedidos.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFPedidos.Rb0Click(Sender: TObject);
var
  Query,SubQuery: String;
begin
  Consulta.Filtered:=False;
  Query:='SELECT ID_CERTIDAO,ID_ATO,DT_PEDIDO,RECIBO,SELO,ALEATORIO,CCT,REQUERIDO,CPF_CNPJ_REQUERIDO,TIPO_REQUERIDO,TIPO_CERTIDAO FROM CERTIDOES WHERE ';

  if dm.vCancelamento then
  begin
      Self.Caption:='Pedidos de Cancelamento';
      SubQuery:=' TIPO_CERTIDAO='''+'X'+''' AND ';
  end
  else
  begin
      Self.Caption:='Pedidos de Certid�o';
      SubQuery:='(TIPO_CERTIDAO='''+'N'+''' OR TIPO_CERTIDAO='''+'I'+''') AND ';
  end;

  Consulta.Close;
  Consulta.CommandText:=Query+SubQuery+'DT_PEDIDO = :D ORDER BY RECIBO';
  Consulta.Params.ParamByName('D').AsDate:=Now;
  Consulta.Open;
end;

procedure TFPedidos.Rb1Click(Sender: TObject);
begin
  Consulta.Filtered:=False;
  Consulta.Filter:='SELO IS NULL';
  Consulta.Filtered:=True;
end;

procedure TFPedidos.Rb2Click(Sender: TObject);
begin
  Consulta.Filtered:=False;
  Consulta.Filter:='SELO IS NOT NULL';
  Consulta.Filtered:=True;
end;

procedure TFPedidos.btDetalhesClick(Sender: TObject);
begin
  if Consulta.IsEmpty then Exit;

  dm.Certidoes.Close;
  dm.Certidoes.Params[0].AsInteger:=ConsultaID_CERTIDAO.AsInteger;
  dm.Certidoes.Open;
  dm.Certidoes.Edit;

  GR.CriarForm(TFPedidoCertidao,FPedidoCertidao);

  Consulta.RefreshRecord;
end;

procedure TFPedidos.GridAtosDblClick(Sender: TObject);
begin
  btDetalhesClick(Sender);
end;

procedure TFPedidos.btPedidoClick(Sender: TObject);
begin
  if Consulta.IsEmpty then Exit;
  dm.Certidoes.Close;
  dm.Certidoes.Params[0].AsInteger:=ConsultaID_CERTIDAO.AsInteger;
  dm.Certidoes.Open;
  PF.CarregarCustas(dm.CertidoesID_CERTIDAO.AsInteger);

  dm.vEmolumentos :=dm.CertidoesEMOLUMENTOS.AsFloat;
  dm.vFetj        :=dm.CertidoesFETJ.AsFloat;
  dm.vFundperj    :=dm.CertidoesFUNDPERJ.AsFloat;
  dm.vFunperj     :=dm.CertidoesFUNPERJ.AsFloat;
  dm.vFunarpen    :=dm.CertidoesFUNARPEN.AsFloat;
  dm.vPmcmv       :=dm.CertidoesPMCMV.AsFloat;
  dm.vIss         :=dm.CertidoesISS.AsFloat;
  dm.vApontamento :=dm.CertidoesAPONTAMENTO.AsFloat;
  dm.vMutua       :=dm.CertidoesMUTUA.AsFloat;
  dm.vAcoterj     :=dm.CertidoesACOTERJ.AsFloat;
  dm.vTotal       :=dm.CertidoesTOTAL.AsFloat;

  if dm.ValorParametro(42)='S' then
    dm.ReciboMatricial(ConsultaID_CERTIDAO.AsInteger,ConsultaTIPO_CERTIDAO.AsString)
      else Application.CreateForm(TFQuickPedido1,FQuickPedido1);
  dm.Certidoes.Close;
end;

procedure TFPedidos.btCertidaoClick(Sender: TObject);
var
  vTipo: String;
begin
  (**if Consulta.IsEmpty then Exit;

  if dm.ValorParametro(18)='' then
  begin
      raise Exception.Create('INFORMAR O PAR�METRO N� 18!'+#13#13+'N� DE REGISTROS POR FOLHA NA CERTID�O.');
      Exit;
  end;

  if not Gdm.QtdSeloDisponivel(StrToInt(dm.ValorParametro(82)),1) then
  begin
      GR.Aviso('NENHUM SELO DISPON�VEL!');
      Exit;
  end;

  {CANCELAMENTO}
  if ConsultaTIPO_CERTIDAO.AsString='X' then
  begin
      if ConsultaSELO.AsString<>'' then
      begin
          GR.Aviso('ATO J� SELADO!');
          Exit;
      end;

      try
        dm.vOkGeral:=False;
        dm.Certidoes.Close;
        dm.Certidoes.Params[0].AsInteger:=ConsultaID_CERTIDAO.AsInteger;
        dm.Certidoes.Open;
        dm.Certidoes.Edit;

        if dm.CertidoesFOLHAS.AsInteger=0 then dm.CertidoesFOLHAS.AsInteger:=1;

        dm.Titulos.Close;
        dm.Titulos.Params[0].AsInteger:=dm.CertidoesID_ATO.AsInteger;
        dm.Titulos.Open;

        PF.CarregarCustas(dm.CertidoesID_CERTIDAO.AsInteger);

        dm.vEmolumentos   :=dm.CertidoesEMOLUMENTOS.AsFloat;
        dm.vFetj          :=dm.CertidoesFETJ.AsFloat;
        dm.vFundperj      :=dm.CertidoesFUNDPERJ.AsFloat;
        dm.vFunperj       :=dm.CertidoesFUNPERJ.AsFloat;
        dm.vFunarpen      :=dm.CertidoesFUNARPEN.AsFloat;
        dm.vPmcmv         :=dm.CertidoesPMCMV.AsFloat;
        dm.vIss           :=dm.CertidoesISS.AsFloat;
        dm.vMutua         :=dm.CertidoesMUTUA.AsFloat;
        dm.vAcoterj       :=dm.CertidoesACOTERJ.AsFloat;
        dm.vDistribuicao  :=dm.CertidoesDISTRIBUICAO.AsFloat;
        dm.vApontamento   :=dm.CertidoesAPONTAMENTO.AsFloat;
        dm.vTotal         :=dm.CertidoesTOTAL.AsFloat;

        Application.CreateForm(TFCancelado,FCancelado);
        FCancelado.edRecibo.Text:=dm.CertidoesRECIBO.AsString;
        FCancelado.lbTotal.Caption:='Total: R$ '+FloatToStrF(dm.CertidoesTOTAL.AsFloat,ffNumber,7,2);
        FCancelado.lkEscreventes.KeyValue:=dm.vCPF;
        FCancelado.ShowModal;

        if dm.vOkGeral then
        begin
            dm.Titulos.Edit;
            dm.TitulosSTATUS.AsString             :='CANCELADO';
            dm.TitulosDT_PAGAMENTO.AsDateTime     :=FCancelado.edData.Date;
            dm.TitulosSELO_PAGAMENTO.AsString     :=FCancelado.edSelo.Text;
            dm.TitulosALEATORIO_SOLUCAO.AsString  :=FCancelado.edAleatorio.Text;
            dm.TitulosVALOR_PAGAMENTO.AsFloat     :=dm.TitulosSALDO_PROTESTO.AsFloat;
            dm.TitulosRECIBO_PAGAMENTO.AsInteger  :=StrToInt(FCancelado.edRecibo.Text);
            dm.TitulosCPF_ESCREVENTE_PG.AsString  :=dm.EscreventesCPF.AsString;

            dm.TitulosDT_SUSTADO.Clear;
            dm.TitulosDT_RETIRADO.Clear;

            if (dm.vCobranca='JG') or (dm.vCobranca='SC') or (dm.vCobranca='FL') then
            begin
                dm.TitulosEMOLUMENTOS.AsFloat:=0;
                dm.TitulosFETJ.AsFloat:=0;
                dm.TitulosFUNDPERJ.AsFloat:=0;
                dm.TitulosFUNPERJ.AsFloat:=0;
                dm.TitulosFUNARPEN.AsFloat:=0;
                dm.TitulosPMCMV.AsFloat:=0;
                dm.TitulosISS.AsFloat:=0;
                dm.TitulosMUTUA.AsFloat:=0;
                dm.TitulosACOTERJ.AsFloat:=0;
                dm.TitulosDISTRIBUICAO.AsFloat:=0;
                dm.TitulosTARIFA_BANCARIA.AsFloat:=0;
                dm.TitulosVALOR_AR.AsFloat:=0;
                dm.TitulosTOTAL.AsFloat:=0;
                dm.TitulosCOBRANCA.AsString:=dm.vCobranca;

                GR.ExecutarSQLFD('DELETE FROM CUSTAS WHERE ID_ATO='+dm.CertidoesID_CERTIDAO.AsString,dm.conSISTEMA);
                GR.ExecutarSQLFD('DELETE FROM CUSTAS WHERE ID_ATO='+dm.TitulosID_ATO.AsString,dm.conSISTEMA);
            end;

            dm.Titulos.Post;
            dm.Titulos.ApplyUpdates(0);

            dm.CertidoesCOBRANCA.AsString       :=dm.vCobranca;
            dm.CertidoesEMOLUMENTOS.AsFloat     :=dm.vEmolumentos;
            dm.CertidoesFETJ.AsFloat            :=dm.vFetj;
            dm.CertidoesFUNDPERJ.AsFloat        :=dm.vFundperj;
            dm.CertidoesFUNPERJ.AsFloat         :=dm.vFunperj;
            dm.CertidoesFUNARPEN.AsFloat        :=dm.vFunarpen;
            dm.CertidoesPMCMV.AsFloat           :=dm.vPmcmv;
            dm.CertidoesISS.AsFloat             :=dm.vIss;
            dm.CertidoesMUTUA.AsFloat           :=dm.vMutua;
            dm.CertidoesACOTERJ.AsFloat         :=dm.vAcoterj;
            dm.CertidoesDISTRIBUICAO.AsFloat    :=dm.vDistribuicao;
            dm.CertidoesAPONTAMENTO.AsFloat     :=dm.vApontamento;
            dm.CertidoesTOTAL.AsFloat           :=dm.vTotal;
            dm.CertidoesSELO.AsString           :=dm.TitulosSELO_PAGAMENTO.AsString;
            dm.CertidoesALEATORIO.AsString      :=dm.TitulosALEATORIO_SOLUCAO.AsString;
            dm.CertidoesDT_CERTIDAO.AsDateTime  :=dm.TitulosDT_PAGAMENTO.AsDateTime;
            dm.CertidoesRESULTADO.AsString      :='P';
            dm.CertidoesREGISTROS.AsInteger     :=1;
            dm.CertidoesFOLHAS.AsInteger        :=1;
            dm.CertidoesESCREVENTE.AsString     :=dm.TitulosCPF_ESCREVENTE_PG.AsString;
            dm.Certidoes.Post;
            dm.Certidoes.ApplyUpdates(0);

            Gdm.BaixarSelo(dm.CertidoesSELO.AsString,
                           dm.vIdReservado,
                           'PROTESTO',
                           Self.Name,
                           dm.CertidoesID_CERTIDAO.AsInteger,
                           dm.TitulosID_ATO.AsInteger,
                           4,
                           dm.CertidoesDT_CERTIDAO.AsDateTime,
                           dm.TitulosLIVRO_REGISTRO.AsString,
                           GR.PegarNumero(dm.TitulosFOLHA_REGISTRO.AsString),
                           GR.PegarNumero(dm.TitulosFOLHA_REGISTRO.AsString),
                           0,dm.TitulosPROTOCOLO.AsInteger,0,
                           dm.CertidoesCODIGO.AsInteger,
                           'CANCELAMENTO',
                           dm.vNome,
                           dm.CertidoesCONVENIO.AsString,
                           dm.CertidoesCOBRANCA.AsString,
                           dm.CertidoesEMOLUMENTOS.AsFloat,
                           dm.CertidoesFETJ.AsFloat,
                           dm.CertidoesFUNDPERJ.AsFloat,
                           dm.CertidoesFUNPERJ.AsFloat,
                           dm.CertidoesFUNARPEN.AsFloat,
                           dm.CertidoesPMCMV.AsFloat,
                           dm.CertidoesMUTUA.AsFloat,
                           dm.CertidoesACOTERJ.AsFloat,
                           dm.CertidoesDISTRIBUICAO.AsFloat,
                           0,
                           IfThen(dm.TitulosCONVENIO.AsString='S',dm.TitulosEMOLUMENTOS.AsFloat,0),
                           0,
                           0,
                           dm.CertidoesTOTAL.AsFloat,
                           dm.CertidoesISS.AsFloat);

            dm.vIdReservado:=0;

            dm.Movimento.Close;
            dm.Movimento.Open;
            dm.Movimento.Append;
            dm.MovimentoID_MOVIMENTO.AsInteger  :=dm.IdAtual('ID_MOVIMENTO','S');
            dm.MovimentoID_ATO.AsInteger        :=dm.TitulosID_ATO.AsInteger;
            dm.MovimentoDATA.AsDateTime         :=Now;
            dm.MovimentoHORA.AsDateTime         :=Time;
            dm.MovimentoBAIXA.AsDateTime        :=dm.TitulosDT_PAGAMENTO.AsDateTime;
            dm.MovimentoESCREVENTE.AsString     :=dm.EscreventesNOME.AsString;
            dm.MovimentoSTATUS.AsString         :=dm.TitulosSTATUS.AsString;
            dm.Movimento.Post;
            dm.Movimento.ApplyUpdates(0);

            if dm.ValorParametro(51)='S' then
            begin
                Application.CreateForm(TFQuickCancelado1,FQuickCancelado1);
                FQuickCancelado1.Free;
            end;

            Application.CreateForm(TFQuickCancelado2,FQuickCancelado2);
            FQuickCancelado2.Free;
        end;
        FCancelado.Free;
      except
        on E: Exception do
        begin
            GR.Aviso('Erro: '+E.Message);
        end;
      end;
  end
  else
  begin
      dm.Certidoes.Close;
      dm.Certidoes.Params[0].AsInteger:=ConsultaID_CERTIDAO.AsInteger;
      dm.Certidoes.Open;
      dm.Certidoes.Edit;

      if dm.CertidoesDT_CERTIDAO.IsNull then dm.CertidoesDT_CERTIDAO.AsDateTime:=Now;
      if dm.CertidoesESCREVENTE.IsNull  then dm.CertidoesESCREVENTE.AsString:=dm.vCPF;
      if dm.CertidoesFOLHAS.AsInteger=0 then dm.CertidoesFOLHAS.AsInteger:=1;

      if dm.CertidoesTIPO_CERTIDAO.AsString='N' then vTipo:=' NORMAL';
      if dm.CertidoesTIPO_CERTIDAO.AsString='I' then vTipo:=' DE INTEIRO TEOR';

      if dm.CertidoesSELO.IsNull then
      begin
          dm.vBuscar:=True;
          dm.vIdReservado:=GR.ValorGeneratorDBX('ID_RESERVADO',Gdm.BDGerencial);
          dm.CertidoesSELO.AsString:=Gdm.ProximoSelo(dm.SerieAtual,'PROTESTO',Self.Name,'N','',
                                                     0,0,0,'CERTID�O'+vTipo,dm.vNome,Now,dm.vIdReservado);
          dm.CertidoesALEATORIO.AsString:=Gdm.vAleatorio;
          Gdm.vAleatorio:='*';
      end
      else
      begin
          dm.vIdReservado:=0;
          dm.vBuscar:=False;
      end;

      if dm.CertidoesTIPO_CERTIDAO.AsString='F' then
        GR.CriarForm(TFCertidao2Via,FCertidao2Via)
          else GR.CriarForm(TFCertidao,FCertidao);
  end;

  Consulta.RefreshRecord;  **)
end;

procedure TFPedidos.cbBuscarChange(Sender: TObject);
begin
  if (cbBuscar.ItemIndex=6) or (cbBuscar.ItemIndex=7) then
  begin
      edData.Enabled            :=True;
      edPesquisar.Enabled       :=False;
      edData.BoundLabel.Caption :=IfThen(cbBuscar.ItemIndex=6,'Data do Pedido','Cancelamento');
      edPesquisar.Clear;
  end
  else
  begin
      edData.Clear;
      edData.Enabled            :=False;
      edPesquisar.Enabled       :=True;
  end;

  if cbBuscar.ItemIndex=0 then edPesquisar.BoundLabel.Caption:='Localizar Requerente';
  if cbBuscar.ItemIndex=1 then edPesquisar.BoundLabel.Caption:='Localizar Requerido';
  if cbBuscar.ItemIndex=2 then edPesquisar.BoundLabel.Caption:='Localizar Documento';
  if cbBuscar.ItemIndex=3 then edPesquisar.BoundLabel.Caption:='Localizar Protocolo';
  if cbBuscar.ItemIndex=4 then edPesquisar.BoundLabel.Caption:='Localizar Recibo';
  if cbBuscar.ItemIndex=5 then edPesquisar.BoundLabel.Caption:='Localizar Selo';
end;

procedure TFPedidos.dsConsultaDataChange(Sender: TObject; Field: TField);
begin
  btTitulo.Visible:=(ConsultaTIPO_CERTIDAO.AsString='X') or (ConsultaTIPO_CERTIDAO.AsString='I');
end;

procedure TFPedidos.btTituloClick(Sender: TObject);
begin
  if Consulta.IsEmpty then Exit;

  dm.vTipo:='A';

  Application.CreateForm(TFTitulo,FTitulo);
  FTitulo.CarregarTitulo(ConsultaID_ATO.AsInteger);
  FTitulo.Visualizacao;
  FTitulo.ShowModal;
  FTitulo.Free;
end;

procedure TFPedidos.btCancelamentoClick(Sender: TObject);
begin
  if ConsultaSELO.AsString='' then
  begin
      GR.Aviso('Cancelamento ainda n�o efetuado.');
      Exit;
  end;

  dm.Certidoes.Close;
  dm.Certidoes.Params[0].AsInteger:=ConsultaID_CERTIDAO.AsInteger;
  dm.Certidoes.Open;

  dm.Titulos.Close;
  dm.Titulos.Params[0].AsInteger:=dm.CertidoesID_ATO.AsInteger;
  dm.Titulos.Open;

  if dm.ValorParametro(51)='S' then
  begin
      try
        Application.CreateForm(TFQuickCancelado1,FQuickCancelado1);
      finally
        FQuickCancelado1.Free;
      end;
  end;

  try
    Application.CreateForm(TFQuickCancelado2,FQuickCancelado2);
  finally
    FQuickCancelado2.Free;
  end;

  dm.Certidoes.Close;
  dm.Titulos.Close;
end;

procedure TFPedidos.GridAtosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_DELETE then
  begin
      if Consulta.IsEmpty then Exit;

      if GR.Pergunta('CONFIRMA A EXCLUS�O') then
      begin
          Consulta.Delete;
          Consulta.ApplyUpdates(0);
      end;
  end;
end;

procedure TFPedidos.ExcluirClick(Sender: TObject);
begin
  if Consulta.IsEmpty then Exit;

  if GR.Pergunta('CONFIRMA A EXCLUS�O') then
  begin
      Gdm.LiberarSelo('PROTESTO',ConsultaSELO.AsString,False);
      Gdm.LiberarSelo('PROTESTO',ConsultaCCT.AsString,False);
      Consulta.Delete;
      Consulta.ApplyUpdates(0);
  end;
end;

procedure TFPedidos.L1Click(Sender: TObject);
begin
  if ConsultaSELO.AsString='' then Exit;

  if GR.PasswordInputBox('SEGURAN�A','SENHA')=dm.ServentiaCODIGO.AsString then
    if GR.Pergunta('LIMPAR SELO "'+GR.SeloFormatado(ConsultaSELO.AsString,ConsultaALEATORIO.AsString)+'"') then
    begin
        if GR.Pergunta('DESEJA LIBERAR O SELO NO GERENCIAL') then
        Gdm.LiberarSelo('PROTESTO',ConsultaSELO.AsString,False);
        Consulta.Edit;
        ConsultaSELO.Clear;
        ConsultaALEATORIO.Clear;
        Consulta.Post;
        Consulta.ApplyUpdates(0);
        GR.Aviso('INFORMA��ES ATUALIZADAS COM SUCESSO!');
    end;
end;

end.
