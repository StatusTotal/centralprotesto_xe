unit UDevedores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, ExtCtrls, sPanel, Grids, Wwdbigrd,
  Wwdbgrid, DB;

type
  TFDevedores = class(TForm)
    sPanel1: TsPanel;
    P1: TsPanel;
    Grid: TwwDBGrid;
    btIncluir: TsBitBtn;
    btAlterar: TsBitBtn;
    btExcluir: TsBitBtn;
    dsDevedores: TDataSource;
    btFechar: TsBitBtn;
    procedure btIncluirClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btAlterarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDevedores: TFDevedores;

implementation

uses UDM, UCadastro, UGeral, UGDM;

{$R *.dfm}

procedure TFDevedores.btIncluirClick(Sender: TObject);
begin
  dm.RxDevedor.Append;
  dm.RxDevedorORDEM.AsInteger     :=dm.RxDevedor.RecordCount+1;
  dm.RxDevedorTIPO.AsString       :='F';
  dm.RxDevedorIGNORADO.AsString   :='N';

  GR.CriarForm(TFCadastro,FCadastro);
end;

procedure TFDevedores.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFDevedores.btAlterarClick(Sender: TObject);
begin
  if dm.RxDevedor.IsEmpty then Exit;
  
  dm.RxDevedor.Edit;

  GR.CriarForm(TFCadastro,FCadastro);
end;

procedure TFDevedores.btExcluirClick(Sender: TObject);
begin
  if dm.RxDevedor.IsEmpty then Exit;

  if GR.Pergunta('CONFIRMA A EXCLUS�O') then
  begin
      if not dm.RxDevedorID_DEVEDOR.IsNull then
      GR.ExecutarSQLDAC('DELETE FROM DEVEDORES WHERE ID_DEVEDOR='+dm.RxDevedorID_DEVEDOR.AsString,dm.conSISTEMA);
      dm.RxDevedor.Delete;
      dm.OrdenarRXDevedores;
  end;
end;

procedure TFDevedores.GridDblClick(Sender: TObject);
begin
  btAlterar.Click;
end;

procedure TFDevedores.btFecharClick(Sender: TObject);
begin
  Close;
end;

end.
