{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   URelImportadosExportados.pas
  Descricao:   Tela de filtro para o Relatorio de Arquivos Importados e Exportados
  Author   :   Cristina
  Date:        21-jun-2017
  Last Update: 05-jul-2017  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit URelImportadosExportados;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, sDBLookupComboBox, StdCtrls, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, sGroupBox, ExtCtrls, sPanel, Buttons,
  sBitBtn, DB, DBClient, SimpleDS, sCheckBox, System.DateUtils,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFRelImportadosExportados = class(TForm)
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    P1: TsPanel;
    sGroupBox1: TsGroupBox;
    dteInicio: TsDateEdit;
    dteFim: TsDateEdit;
    rgServentiasAgregadas: TsRadioGroup;
    chbExibeTitulos: TsCheckBox;
    lcbServentiaAgregada: TsDBLookupComboBox;
    dsSA: TDataSource;
    qrySA: TFDQuery;
    qrySAID_SERVENTIA_AGREGADA: TIntegerField;
    qrySACODIGO: TIntegerField;
    qrySADESCRICAO: TStringField;
    qrySACODIGO_DISTRIBUICAO: TIntegerField;
    qrySAVALOR_ULTIMO_SALDO: TFloatField;
    qrySAQTD_ULTIMO_SALDO: TIntegerField;
    qrySADATA_ULTIMO_SALDO: TDateField;
    qrySAFLG_ATIVA: TStringField;
    qrySADATA_INATIVACAO: TDateField;
    qrySAFLG_USAPROTESTO: TStringField;
    procedure btSairClick(Sender: TObject);
    procedure btVisualizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure rgServentiasAgregadasChange(Sender: TObject);
    procedure chbExibeTitulosClick(Sender: TObject);
    procedure chbExibeTitulosExit(Sender: TObject);
  private
    { Private declarations }
    function VerificarFiltro: Boolean;
  public
    { Public declarations }
  end;

var
  FRelImportadosExportados: TFRelImportadosExportados;

implementation

uses UDM, UPF, UGDM, UGeral;

{$R *.dfm}

procedure TFRelImportadosExportados.btSairClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TFRelImportadosExportados.btVisualizarClick(Sender: TObject);
var
  qryAux: TFDQuery;
begin
  if not VerificarFiltro then
    Exit;

  PF.Aguarde(True);

  //Detalhe
  dm.lRelExibeDetalhe := chbExibeTitulos.Checked;

  { ARQUIVOS }
  dm.qryRel01_ArqIE.Close;
  dm.qryRel01_ArqIE.Params.ParamByName('DATA_INI').Value := dteInicio.Date;
  dm.qryRel01_ArqIE.Params.ParamByName('DATA_FIM').Value := dteFim.Date;
  dm.qryRel01_ArqIE.Open;

  if dm.qryRel01_ArqIE.RecordCount = 0 then
  begin
    Application.MessageBox('N�o h� arquivos para o per�odo informado.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
    Exit;
  end;

  dm.qryRel01_ArqIE.First;

  while not dm.qryRel01_ArqIE.Eof do
  begin
    dm.qryRel01_ArqIE.Edit;

    GR.CriarFDQuery(qryAux,'', dm.conSISTEMAAux);

    { Quantidade e Valor de Titulos aceitos na importacao do arquivo de
      Remessa B do CRA }
    qryAux.Close;
    qryAux.SQL.Clear;
    qryAux.SQL.Text := 'SELECT COUNT(TT.ID_ATO) AS QTD_TITULO, ' +
                       '       SUM(TT.VALOR_TITULO) AS VR_TITULO ' +
                       '  FROM TITULOS TT ' +
                       ' WHERE TT.ARQUIVO = :ARQUIVO_IMPORTADO_CRA ' +
                       '   AND TT.DT_ENTRADA = :DT_ENTRADA';
    qryAux.Params.ParamByName('ARQUIVO_IMPORTADO_CRA').Value := QuotedStr(dm.qryRel01_ArqIE.FieldByName('ARQUIVO_IMPORTADO_CRA').AsString);
    qryAux.Params.ParamByName('DT_ENTRADA').Value            := dm.qryRel01_ArqIE.FieldByName('DT_ENTRADA').AsDateTime;
    qryAux.Open;

    if qryAux.RecordCount > 0 then
    begin
      dm.qryRel01_ArqIE.FieldByName('QTD_ACEITO_CRA').AsInteger := qryAux.FieldByName('QTD_TITULO').AsInteger;
      dm.qryRel01_ArqIE.FieldByName('VR_ACEITO_CRA').AsFloat    := qryAux.FieldByName('VR_TITULO').AsFloat;
    end
    else
    begin
      dm.qryRel01_ArqIE.FieldByName('QTD_ACEITO_CRA').AsInteger := 0;
      dm.qryRel01_ArqIE.FieldByName('VR_ACEITO_CRA').AsFloat    := 0;
    end;

    { Quantidade, Total de Titulos e Total de Custas na exportacao do arquivo de
      Remessa D para as Serventias Agregadas }
    if not dm.qryRel01_ArqIE.FieldByName('ARQUIVO_EXPORTADO_SA').IsNull then
    begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Text := 'SELECT COUNT(TT.ID_ATO) AS QTD_TITULO, ' +
                         '       SUM(TT.VALOR_TITULO) AS VR_TITULO, ' +
                         '       SUM(TT.TOTAL) AS VR_CUSTAS_TITULO ' +
                         '  FROM TITULOS TT ' +
                         ' WHERE TT.ARQUIVO_CARTORIO = :ARQUIVO_EXPORTADO_SA ' +
                         '   AND TT.DT_ENTRADA = :DT_ENTRADA';
      qryAux.Params.ParamByName('ARQUIVO_EXPORTADO_SA').Value  := QuotedStr(dm.qryRel01_ArqIE.FieldByName('ARQUIVO_EXPORTADO_SA').AsString);
      qryAux.Params.ParamByName('DT_ENTRADA').Value            := dm.qryRel01_ArqIE.FieldByName('DT_ENTRADA').AsDateTime;
      qryAux.Open;

      if qryAux.RecordCount > 0 then
      begin
        dm.qryRel01_ArqIE.FieldByName('QTD_EXPORTADO_SA').AsInteger := qryAux.FieldByName('QTD_TITULO').AsInteger;
        dm.qryRel01_ArqIE.FieldByName('VR_EXPORTADO_SA').AsFloat    := qryAux.FieldByName('VR_TITULO').AsFloat;
        dm.qryRel01_ArqIE.FieldByName('VR_CUSTAS_EXP_SA').AsFloat   := qryAux.FieldByName('VR_CUSTAS_TITULO').AsFloat;
      end
      else
      begin
        dm.qryRel01_ArqIE.FieldByName('QTD_EXPORTADO_SA').AsInteger := 0;
        dm.qryRel01_ArqIE.FieldByName('VR_EXPORTADO_SA').AsFloat    := 0;
        dm.qryRel01_ArqIE.FieldByName('VR_CUSTAS_EXP_SA').AsFloat   := 0;
      end;
    end;

    { Quantidade, Total de Titulos e Total de Custas no retorno do arquivo de
      Retorno das Serventias Agregadas }
    if not dm.qryRel01_ArqIE.FieldByName('ARQUIVO_RET_SA').IsNull then
    begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Text := 'SELECT COUNT(TT.ID_ATO) AS QTD_TITULO, ' +
                         '       SUM(TT.VALOR_TITULO) AS VR_TITULO, ' +
                         '       SUM(TT.TOTAL) AS VR_CUSTAS_TITULO ' +
                         '  FROM TITULOS TT ' +
                         ' WHERE TT.RETORNO_CARTORIO = :ARQUIVO_RET_SA ' +
                         '   AND TT.DT_ENTRADA = :DT_ENTRADA';
      qryAux.Params.ParamByName('ARQUIVO_RET_SA').Value        := QuotedStr(dm.qryRel01_ArqIE.FieldByName('ARQUIVO_RET_SA').AsString);
      qryAux.Params.ParamByName('DT_ENTRADA').Value            := dm.qryRel01_ArqIE.FieldByName('DT_ENTRADA').AsDateTime;
      qryAux.Open;

      if qryAux.RecordCount > 0 then
      begin
        dm.qryRel01_ArqIE.FieldByName('QTD_RETORNO_SA').AsInteger := qryAux.FieldByName('QTD_TITULO').AsInteger;
        dm.qryRel01_ArqIE.FieldByName('VR_RETORNO_SA').AsFloat    := qryAux.FieldByName('VR_TITULO').AsFloat;
        dm.qryRel01_ArqIE.FieldByName('VR_CUSTAS_RET_SA').AsFloat := qryAux.FieldByName('VR_CUSTAS_TITULO').AsFloat;
      end
      else
      begin
        dm.qryRel01_ArqIE.FieldByName('QTD_RETORNO_SA').AsInteger := 0;
        dm.qryRel01_ArqIE.FieldByName('VR_RETORNO_SA').AsFloat    := 0;
        dm.qryRel01_ArqIE.FieldByName('VR_CUSTAS_RET_SA').AsFloat := 0;
      end;
    end;

    { Quantidade, Total de Titulos e Total de Custas no retorno do arquivo de
      Retorno para o CRA }
    if not dm.qryRel01_ArqIE.FieldByName('ARQUIVO_RET_CRA').IsNull then
    begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Text := 'SELECT COUNT(TT.ID_ATO) AS QTD_TITULO, ' +
                         '       SUM(TT.VALOR_TITULO) AS VR_TITULO, ' +
                         '       SUM(TT.TOTAL) AS VR_CUSTAS_TITULO ' +
                         '  FROM TITULOS TT ' +
                         ' WHERE TT.RETORNO = :ARQUIVO_RET_CRA ' +
                         '   AND TT.DT_ENTRADA = :DT_ENTRADA';
      qryAux.Params.ParamByName('ARQUIVO_RET_CRA').Value       := QuotedStr(dm.qryRel01_ArqIE.FieldByName('ARQUIVO_RET_CRA').AsString);
      qryAux.Params.ParamByName('DT_ENTRADA').Value            := dm.qryRel01_ArqIE.FieldByName('DT_ENTRADA').AsDateTime;
      qryAux.Open;

      if qryAux.RecordCount > 0 then
      begin
        dm.qryRel01_ArqIE.FieldByName('QTD_RETORNO_CRA').AsInteger := qryAux.FieldByName('QTD_TITULO').AsInteger;
        dm.qryRel01_ArqIE.FieldByName('VR_RETORNO_CRA').AsFloat    := qryAux.FieldByName('VR_TITULO').AsFloat;
        dm.qryRel01_ArqIE.FieldByName('VR_CUSTAS_RET_CRA').AsFloat := qryAux.FieldByName('VR_CUSTAS_TITULO').AsFloat;
      end
      else
      begin
        dm.qryRel01_ArqIE.FieldByName('QTD_RETORNO_CRA').AsInteger := 0;
        dm.qryRel01_ArqIE.FieldByName('VR_RETORNO_CRA').AsFloat    := 0;
        dm.qryRel01_ArqIE.FieldByName('VR_CUSTAS_RET_CRA').AsFloat := 0;
      end;
    end;

    FreeAndNil(qryAux);

    dm.qryRel01_ArqIE.Post;

    dm.qryRel01_ArqIE.Next;
  end;

  dm.sRelNomeArquivoRelatorio := 'frRelatorio_ImportadosExportados.fr3';

  //Titulo
  dm.sRelTituloRelatorio := 'Rela��o de Arquivos Importados e Exportados';

  //Subtitulo (Periodo)
  dm.sRelSubtituloRelatorio := 'Per�odo de ' + DateToStr(dteInicio.Date) + ' a ' + DateToStr(dteFim.Date);

  { TITULOS }
  if chbExibeTitulos.Checked then
  begin
    dm.qryRel01_Tit.Close;

    dm.qryRel01_Tit.Params.ParamByName('DATA_INI').Value := dteInicio.Date;
    dm.qryRel01_Tit.Params.ParamByName('DATA_FIM').Value := dteFim.Date;

    if rgServentiasAgregadas.ItemIndex = 0 then
    begin
      dm.qryRel01_Tit.Params.ParamByName('COD_D1').Value := 0;
      dm.qryRel01_Tit.Params.ParamByName('COD_D2').Value := 0;
    end
    else if rgServentiasAgregadas.ItemIndex = 1 then
    begin
      dm.qryRel01_Tit.Params.ParamByName('COD_D1').Value := qrySA.FieldByName('CODIGO_DISTRIBUICAO').AsInteger;
      dm.qryRel01_Tit.Params.ParamByName('COD_D2').Value := qrySA.FieldByName('CODIGO_DISTRIBUICAO').AsInteger;
    end;

    dm.qryRel01_Tit.Open;
  end;

  PF.Aguarde(False);

  dm.ImprimirRelatorio;
end;

procedure TFRelImportadosExportados.chbExibeTitulosClick(Sender: TObject);
begin
  if chbExibeTitulos.Checked then
  begin
    rgServentiasAgregadas.Enabled   := True;
    rgServentiasAgregadas.ItemIndex := 0;
  end
  else
  begin
    rgServentiasAgregadas.Enabled   := False;
    rgServentiasAgregadas.ItemIndex := 1;
  end;
end;

procedure TFRelImportadosExportados.chbExibeTitulosExit(Sender: TObject);
begin
  if chbExibeTitulos.Checked then
  begin
    rgServentiasAgregadas.Enabled   := True;
    rgServentiasAgregadas.ItemIndex := 0;
  end
  else
  begin
    rgServentiasAgregadas.Enabled   := False;
    rgServentiasAgregadas.ItemIndex := 1;
  end;
end;

procedure TFRelImportadosExportados.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key = VK_ESCAPE then
    Close;
end;

procedure TFRelImportadosExportados.FormShow(Sender: TObject);
begin
  dteInicio.Date := StartOfTheMonth(Date);
  dteFim.Date    := Date;

  rgServentiasAgregadas.Enabled := True;
  lcbServentiaAgregada.Enabled  := False;

  chbExibeTitulos.Checked := True;
end;

procedure TFRelImportadosExportados.rgServentiasAgregadasChange(
  Sender: TObject);
begin
  if rgServentiasAgregadas.ItemIndex = 0 then  //Todas
  begin
    qrySA.Close;
    lcbServentiaAgregada.Enabled := False;
  end
  else if rgServentiasAgregadas.ItemIndex = 1 then  //Especifica
  begin
    qrySA.Close;
    qrySA.Open;

    lcbServentiaAgregada.Enabled := True;
  end;
end;

function TFRelImportadosExportados.VerificarFiltro: Boolean;
var
  Msg: String;
begin
  Result := True;

  if dteInicio.Date = 0 then
  begin
    Msg := '- Informe a DATA DE IN�CIO do Per�odo.';
    Result := False;
  end;

  if dteFim.Date = 0 then
  begin
    if Trim(Msg) = '' then
      Msg := '- Informe a DATA DE FIM do Per�odo.'
    else
      Msg := #13#10 + '- Informe a DATA DE FIM do Per�odo.';

    Result := False;
  end;

  if dteFim.Date < dteInicio.Date then
  begin
    if Trim(Msg) = '' then
      Msg := '- A DATA DE FIM do Per�odo n�o pode inferior � DATA DE IN�CIO do mesmo.'
    else
      Msg := #13#10 + '- A DATA DE FIM do Per�odo n�o pode ser inferior � DATA DE IN�CIO do mesmo.';

    Result := False;
  end;

  if lcbServentiaAgregada.Enabled then
  begin
    if lcbServentiaAgregada.KeyValue = Null then
    begin
      if Trim(Msg) = '' then
        Msg := '- Informe a SERVENTIA AGREGADA.'
      else
        Msg := #13#10 + '- Informe a SERVENTIA AGREGADA.';

      Result := False;
    end;
  end;

  if not Result then
    Application.MessageBox(PChar(':: ATEN��O ::' + #13#10 + #13#10 + Msg),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
end;

end.
