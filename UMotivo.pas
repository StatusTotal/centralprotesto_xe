unit UMotivo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, sMemo, sLabel;

type
  TFMotivo = class(TForm)
    MMotivo: TsMemo;
    btOk: TsBitBtn;
    btCancelar: TsBitBtn;
    lbCaracteres: TsLabel;
    procedure FormCreate(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure MMotivoChange(Sender: TObject);
    function Caracteres: Integer;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMotivo: TFMotivo;

implementation

uses UDM;

{$R *.dfm}

procedure TFMotivo.FormCreate(Sender: TObject);
begin
  MMotivo.Text:=dm.vMotivo;
  MMotivoChange(Sender);
end;

procedure TFMotivo.btOkClick(Sender: TObject);
begin
  dm.vOkMotivo:=True;
  dm.vMotivo:=MMotivo.Text;
  Close;
end;

procedure TFMotivo.btCancelarClick(Sender: TObject);
begin
  Close;
end;

function TFMotivo.Caracteres: Integer;
begin
  Result:=400-Length(MMotivo.Text);
end;

procedure TFMotivo.MMotivoChange(Sender: TObject);
begin
  lbCaracteres.Caption:=IntToStr(Caracteres)+' restantes';
end;

end.
