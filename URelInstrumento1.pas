unit URelInstrumento1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, ExtCtrls, sPanel, sEdit, sGroupBox,
  QuickRpt, QRCtrls, DB, RxMemDS, FMTBcd, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, sRadioButton, SqlExpr, Grids, Wwdbigrd,
  Wwdbgrid, DBClient, SimpleDS, DBCtrls, sDBLookupComboBox, Menus,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFRelInstrumento = class(TForm)
    P: TsPanel;
    GbProtocolo: TsGroupBox;
    edProtFinal: TsEdit;
    edProtInicial: TsEdit;
    Rb1: TsRadioButton;
    GbData: TsGroupBox;
    Rb2: TsRadioButton;
    edDataInicial: TsDateEdit;
    edDataFinal: TsDateEdit;
    dsProtocolo: TDataSource;
    GridAtos: TwwDBGrid;
    btFiltrar: TsBitBtn;
    PM: TPopupMenu;
    Marcartodos1: TMenuItem;
    Desmarcartodos1: TMenuItem;
    btVisualizar: TsBitBtn;
    edHora: TsMaskEdit;
    qryProtocolo: TFDQuery;
    qryProtocoloCheck: TBooleanField;
    qryProtocoloID_ATO: TIntegerField;
    qryProtocoloPROTOCOLO: TIntegerField;
    qryProtocoloDT_PROTOCOLO: TDateField;
    qryProtocoloAPRESENTANTE: TStringField;
    qryProtocoloDEVEDOR: TStringField;
    qryProtocoloSTATUS: TStringField;
    procedure edProtInicialKeyPress(Sender: TObject; var Key: Char);
    procedure edProtFinalKeyPress(Sender: TObject; var Key: Char);
    procedure Rb1Click(Sender: TObject);
    procedure Rb2Click(Sender: TObject);
    procedure btFiltrarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btVisualizarClick(Sender: TObject);
    procedure edDataInicialExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Marcartodos1Click(Sender: TObject);
    procedure Desmarcartodos1Click(Sender: TObject);
    procedure edProtInicialChange(Sender: TObject);
    function Marcados: Integer;
    procedure GridAtosMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure sPanel1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure GridAtosMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure qryProtocoloAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelInstrumento: TFRelInstrumento;

implementation

uses UDM, UQuickIntimacao1, UPF, UQuickInstrumento1, ComObj, UGeral,
  UAssinatura;

{$R *.dfm}

function TFRelInstrumento.Marcados: Integer;
var
  Posicao: Integer;
begin
  Result:=0;
  qryProtocolo.DisableControls;
  Posicao:=qryProtocolo.RecNo;
  qryProtocolo.First;
  while not qryProtocolo.Eof do
  begin
      if qryProtocoloCheck.AsBoolean then Result:=Result+1;
      if Result>1 then Break;
      qryProtocolo.Next;
  end;
  qryProtocolo.RecNo:=Posicao;
  qryProtocolo.EnableControls;
end;

procedure TFRelInstrumento.edProtInicialKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFRelInstrumento.edProtFinalKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFRelInstrumento.Rb1Click(Sender: TObject);
begin
  GbData.Enabled:=False;
  GbProtocolo.Enabled:=True;
  edDataInicial.Clear;
  edDataFinal.Clear;
  edProtInicial.SetFocus;
end;

procedure TFRelInstrumento.Rb2Click(Sender: TObject);
begin
  GbData.Enabled:=True;
  GbProtocolo.Enabled:=False;
  edProtInicial.Clear;
  edProtFinal.Clear;
  edDataInicial.SetFocus;
end;

procedure TFRelInstrumento.btFiltrarClick(Sender: TObject);
begin
  if Rb1.Checked then
    if edProtInicial.Text='' then
    begin
        edProtInicial.SetFocus;
        Exit;
    end;

  if edProtFinal.Text='' then edProtFinal.Text:=edProtInicial.Text;

  PF.Aguarde(True);
  qryProtocolo.Close;
  if Rb1.Checked then
  begin
      qryProtocolo.SQL.Text:='SELECT ID_ATO,PROTOCOLO,DT_PROTOCOLO,APRESENTANTE,DEVEDOR,STATUS FROM TITULOS '+
                             'WHERE PROTOCOLO BETWEEN :P1 AND :P2 AND PROTESTADO='+QuotedStr('S')+
                             ' ORDER BY PROTOCOLO';
      qryProtocolo.ParamByName('P1').AsInteger:=StrToInt(edProtInicial.Text);
      qryProtocolo.ParamByName('P2').AsInteger:=StrToInt(edProtFinal.Text);
  end
  else
  begin
      qryProtocolo.SQL.Text:='SELECT ID_ATO,PROTOCOLO,DT_PROTOCOLO,APRESENTANTE,DEVEDOR,STATUS FROM TITULOS '+
                             'WHERE DT_REGISTRO BETWEEN :D1 AND :D2 AND PROTESTADO='+QuotedStr('S')+
                             ' ORDER BY PROTOCOLO';
      qryProtocolo.ParamByName('D1').AsDate:=edDataInicial.Date;
      qryProtocolo.ParamByName('D2').AsDate:=edDataFinal.Date;
  end;
  qryProtocolo.Open;
  PF.Aguarde(False);
end;

procedure TFRelInstrumento.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFRelInstrumento.btVisualizarClick(Sender: TObject);
var
  Praca: String;
  Posicao: Integer;
begin
  if qryProtocolo.IsEmpty then Exit;
  if Marcados=0        then Exit;

  try
    dm.vHora:=StrToTime(edHora.Text);
    GR.CriarForm(TFAssinatura,FAssinatura);
    PF.Aguarde(True);
    Application.CreateForm(TFQuickInstrumento1,FQuickInstrumento1);

    with FQuickInstrumento1 do
    begin
        RX.Close;
        RX.Open;

        Posicao:=qryProtocolo.RecNo;
        qryProtocolo.DisableControls;
        qryProtocolo.First;
        while not qryProtocolo.Eof do
        begin
            if qryProtocoloCheck.Value=True then
            begin
                dm.Titulos.Close;
                dm.Titulos.Params[0].AsInteger:=qryProtocoloID_ATO.AsInteger;
                dm.Titulos.Open;

                RX.Append;
                RXIdAto.AsInteger         :=dm.TitulosID_ATO.AsInteger;
                RXTitulo.AsString         :=PF.RetornarTitulo(dm.TitulosTIPO_TITULO.AsInteger);
                RXApresentante.AsString   :=dm.TitulosAPRESENTANTE.AsString;
                RXCredor.AsString         :=dm.TitulosCEDENTE.AsString;
                RXSacador.AsString        :=dm.TitulosSACADOR.AsString;
                RXDevedor.AsString        :=dm.TitulosDEVEDOR.AsString;
                RXValor.AsFloat           :=dm.TitulosVALOR_TITULO.AsFloat;
                RXCustas.AsFloat          :=dm.TitulosTOTAL.AsFloat;
                RXSaldoProtesto.AsFloat   :=dm.TitulosSALDO_PROTESTO.AsFloat;

                if dm.TitulosSALDO_TITULO.AsFloat<>0 then
                  RXSaldoTitulo.AsFloat:=dm.TitulosSALDO_TITULO.AsFloat
                    else RXSaldoTitulo.AsFloat:=dm.TitulosVALOR_TITULO.AsFloat;

                RXVencimento.AsDateTime   :=dm.TitulosDT_VENCIMENTO.AsDateTime;
                RXIntimacao.AsDateTime    :=dm.TitulosDT_INTIMACAO.AsDateTime;
                RXProtocolo.AsInteger     :=dm.TitulosPROTOCOLO.AsInteger;
                RXDt_Protocolo.AsDateTime :=dm.TitulosDT_PROTOCOLO.AsDateTime;
                RXNumero.AsString         :=dm.TitulosNUMERO_TITULO.AsString;
                RXLivro.AsString          :=dm.TitulosLIVRO_REGISTRO.AsString;
                RXFolha.AsString          :=dm.TitulosFOLHA_REGISTRO.AsString;
                RXDt_Registro.AsDateTime  :=dm.TitulosDT_REGISTRO.AsDateTime;
                RXNossoNumero.AsString    :=dm.TitulosNOSSO_NUMERO.AsString;
                RXTipo.AsString           :=dm.TitulosTIPO_DEVEDOR.AsString;
                RXMotivo.AsString         :=dm.TitulosMOTIVO_INTIMACAO.AsString;
                RXCedente.AsString        :=dm.TitulosCEDENTE.AsString;
                RXDt_Titulo.AsDateTime    :=dm.TitulosDT_TITULO.AsDateTime;
                RXEmolumentos.AsFloat     :=dm.TitulosEMOLUMENTOS.AsFloat;
                RXFetj.AsFloat            :=dm.TitulosFETJ.AsFloat;
                RXFundperj.AsFloat        :=dm.TitulosFUNDPERJ.AsFloat;
                RXFunperj.AsFloat         :=dm.TitulosFUNPERJ.AsFloat;
                RXFunarpen.AsFloat        :=dm.TitulosFUNARPEN.AsFloat;
                RXPmcmv.AsFloat           :=dm.TitulosPMCMV.AsFloat;
                RXIss.AsFloat             :=dm.TitulosISS.AsFloat;
                RXMutua.AsFloat           :=dm.TitulosMUTUA.AsFloat;
                RXAcoterj.AsFloat         :=dm.TitulosACOTERJ.AsFloat;
                RXDistribuicao.AsFloat    :=dm.TitulosDISTRIBUICAO.AsFloat;
                RXTarifa.AsFloat          :=dm.TitulosTARIFA_BANCARIA.AsFloat;
                RXAR.AsFloat              :=dm.TitulosVALOR_AR.AsFloat;
                RXTotal.AsFloat           :=dm.TitulosTOTAL.AsFloat;
                RXSelo.AsString           :=dm.TitulosSELO_REGISTRO.AsString;
                RXAleatorio.AsString      :=dm.TitulosALEATORIO_PROTESTO.AsString;
                RXTipoProtesto.AsInteger  :=dm.TitulosTIPO_PROTESTO.AsInteger;
                RXObservacao.AsString     :=dm.TitulosOBSERVACAO.AsString;
                RXTermo.AsInteger         :=dm.TitulosREGISTRO.AsInteger;
                RXDt_Publicacao.AsDateTime:=dm.TitulosDT_PUBLICACAO.AsDateTime;
                RXConvenio.AsString       :=dm.TitulosCONVENIO.AsString;
                RXMensagem.AsString       :=PF.RetornarMensagem(dm.TitulosID_MSG.AsInteger);
                RXFins.AsString           :=GR.iif(dm.TitulosFINS_FALIMENTARES.AsString='S','SIM','N�O');
                if dm.TitulosPRACA_PROTESTO.AsString='' then
                begin
                    InputQuery('Atualiza��o de dados','Pra�a de Pagamento',Praca);
                    dm.Titulos.Edit;
                    dm.TitulosPRACA_PROTESTO.AsString:=Praca;
                    dm.Titulos.Post;
                    dm.Titulos.ApplyUpdates(0);
                end;
                RXPraca.AsString:=dm.TitulosPRACA_PROTESTO.AsString;

                if RXTipo.AsString='F' then
                  RXDocumento.AsString:=PF.FormatarCPF(dm.TitulosCPF_CNPJ_DEVEDOR.AsString)
                    else RXDocumento.AsString:=PF.FormatarCNPJ(dm.TitulosCPF_CNPJ_DEVEDOR.AsString);

                if dm.TitulosTIPO_INTIMACAO.AsString='P' then RXIntimado.AsString:='PESSOAL';
                if dm.TitulosTIPO_INTIMACAO.AsString='E' then RXIntimado.AsString:='EDITAL';
                if dm.TitulosTIPO_INTIMACAO.AsString='C' then RXIntimado.AsString:='CARTA';

                if dm.TitulosTIPO_ENDOSSO.AsString='M' then RXEndosso.AsString:='MANDATO';
                if dm.TitulosTIPO_ENDOSSO.AsString='T' then RXEndosso.AsString:='TRANSLATIVO';
                if dm.TitulosTIPO_ENDOSSO.AsString='S' then RXEndosso.AsString:='SEM ENDOSSO, PARA CONTRATOS NO PR�PRIO BANCO';
                if dm.TitulosTIPO_ENDOSSO.AsString=''  then RXEndosso.AsString:='SEM ENDOSSO, PARA CONTRATOS NO PR�PRIO BANCO';

                PF.CarregarDevedor(dm.TitulosID_ATO.AsInteger);

                RXEndereco.AsString:=dm.RxDevedorENDERECO.AsString+' '+dm.RxDevedorBAIRRO.AsString+' '+
                                     dm.RxDevedorMUNICIPIO.AsString+' '+dm.RxDevedorUF.AsString;

                if dm.RxDevedorCEP.AsString<>'' then
                RXEndereco.AsString:=RXEndereco.AsString+' - CEP: '+PF.FormatarCEP(dm.RxDevedorCEP.AsString);

                PF.CarregarSacador(dm.TitulosID_ATO.AsInteger);

                RXEnderecoSa.AsString:=dm.RxSacadorENDERECO.AsString+' '+dm.RxSacadorBAIRRO.AsString+' '+
                                       dm.RxSacadorMUNICIPIO.AsString+' '+dm.RxSacadorUF.AsString;

                if dm.TitulosCODIGO_APRESENTANTE.AsString<>'' then
                  PF.CarregarPortador('C',dm.TitulosCODIGO_APRESENTANTE.AsString,dm.TitulosAPRESENTANTE.AsString,
                                          dm.TitulosTIPO_APRESENTANTE.AsString,dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosCODIGO_APRESENTANTE.AsString)
                    else
                      if dm.TitulosCPF_CNPJ_APRESENTANTE.AsString<>'' then
                        PF.CarregarPortador('D',dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosAPRESENTANTE.AsString,
                                                dm.TitulosTIPO_APRESENTANTE.AsString,dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosCODIGO_APRESENTANTE.AsString)
                        else
                          PF.CarregarPortador('N',dm.TitulosAPRESENTANTE.AsString,dm.TitulosAPRESENTANTE.AsString,
                                                  dm.TitulosTIPO_APRESENTANTE.AsString,dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosCODIGO_APRESENTANTE.AsString);

                RXEnderecoAp.AsString:=dm.RxPortadorENDERECO.AsString;
                RXDt_Intimacao.AsDateTime:=dm.TitulosDT_INTIMACAO.AsDateTime;
                RX.Post;
            end;
            qryProtocolo.Next;
        end;
        qryProtocolo.RecNo:=Posicao;
        qryProtocolo.EnableControls;

        qrFuncionario.Caption   :=dm.vAssinatura;
        qrQualificacao.Caption  :=dm.vMatricula;

        qkInstrumento.Preview;
        Free;
    end;
  except
    on E:Exception do
    begin
        PF.Aguarde(False);
        GR.Aviso('Erro: '+E.Message);
    end;
  end;
end;

procedure TFRelInstrumento.edDataInicialExit(Sender: TObject);
begin
  if edDataFinal.Date<edDataInicial.Date then
    edDataFinal.Date:=edDataInicial.Date;
end;

procedure TFRelInstrumento.FormCreate(Sender: TObject);
begin
  edHora.Text:=FormatDateTime('HH:MM',Now);
  edHora.Visible:=dm.ValorParametro(53)='S';
  dm.Escreventes.Close;
  dm.Escreventes.Open;
end;

procedure TFRelInstrumento.Marcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=qryProtocolo.RecNo;
  qryProtocolo.DisableControls;
  qryProtocolo.First;
  while not qryProtocolo.Eof do
  begin
      qryProtocolo.Edit;
      qryProtocoloCheck.AsBoolean:=True;
      qryProtocolo.Post;
      qryProtocolo.Next;
  end;
  qryProtocolo.RecNo:=Posicao;
  qryProtocolo.EnableControls;
end;

procedure TFRelInstrumento.Desmarcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=qryProtocolo.RecNo;
  qryProtocolo.DisableControls;
  qryProtocolo.First;
  while not qryProtocolo.Eof do
  begin
      qryProtocolo.Edit;
      qryProtocoloCheck.AsBoolean:=False;
      qryProtocolo.Post;
      qryProtocolo.Next;
  end;
  qryProtocolo.RecNo:=Posicao;
  qryProtocolo.EnableControls;
end;

procedure TFRelInstrumento.edProtInicialChange(Sender: TObject);
begin
  edProtFinal.Text:=edProtInicial.Text;
end;

procedure TFRelInstrumento.GridAtosMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  Screen.Cursor:=GR.iif(X in [11..51],crHandPoint,crDefault);
end;

procedure TFRelInstrumento.PMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  Screen.Cursor:=crDefault;
end;

procedure TFRelInstrumento.sPanel1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  Screen.Cursor:=crDefault;
end;

procedure TFRelInstrumento.GridAtosMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Screen.Cursor=crHandPoint then
    if not qryProtocolo.IsEmpty then
    begin
        qryProtocolo.Edit;
        qryProtocoloCheck.AsBoolean:=not qryProtocoloCheck.AsBoolean;
        qryProtocolo.Post;
    end;
end;

procedure TFRelInstrumento.qryProtocoloAfterOpen(DataSet: TDataSet);
begin
  Self.Caption:='INSTRUMENTO DE PROTESTO: '+IntToStr(qryProtocolo.RecordCount);
end;

end.
