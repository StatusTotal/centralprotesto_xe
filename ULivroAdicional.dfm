object FLivroAdicional: TFLivroAdicional
  Left = 239
  Top = 122
  BorderStyle = bsDialog
  Caption = 'Exportar Livro Adicional'
  ClientHeight = 552
  ClientWidth = 884
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 16
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 884
    Height = 552
    Align = alClient
    TabOrder = 0
    OnMouseMove = sPanel1MouseMove
    SkinData.SkinSection = 'SELECTION'
    object sPanel2: TsPanel
      Left = 5
      Top = 6
      Width = 872
      Height = 52
      TabOrder = 0
      SkinData.SkinSection = 'BARTITLE'
      object lbEtapa: TsLabel
        Left = 447
        Top = 6
        Width = 29
        Height = 14
        Caption = 'Etapa'
        ParentFont = False
        Visible = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 5059883
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      object edData: TsDateEdit
        Left = 47
        Top = 19
        Width = 93
        Height = 22
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
        Text = '  /  /    '
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Data'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -13
        BoundLabel.Font.Name = 'Arial'
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
      end
      object btVisualizar: TsBitBtn
        Left = 152
        Top = 16
        Width = 76
        Height = 27
        Cursor = crHandPoint
        Caption = 'Visualizar'
        TabOrder = 1
        OnClick = btVisualizarClick
        SkinData.SkinSection = 'BUTTON'
      end
      object btExportar: TsBitBtn
        Left = 232
        Top = 16
        Width = 76
        Height = 27
        Cursor = crHandPoint
        Caption = 'Exportar'
        TabOrder = 2
        OnClick = btExportarClick
        SkinData.SkinSection = 'BUTTON'
      end
      object P: TsProgressBar
        Left = 447
        Top = 20
        Width = 406
        Height = 23
        TabOrder = 3
        Visible = False
        SkinData.SkinSection = 'GAUGE'
      end
    end
    object Grid: TwwDBGrid
      Left = 5
      Top = 66
      Width = 872
      Height = 436
      ControlType.Strings = (
        'CHECK;CheckBox;True;False')
      Selected.Strings = (
        'CHECK'#9'2'#9'#'#9'F'
        'DATA'#9'9'#9'Data'#9'F'
        'PROTOCOLO'#9'9'#9'Protocolo'#9'F'
        'CODIGO'#9'6'#9'C'#243'digo'#9'F'
        'GRATUITO'#9'2'#9'G.'#9'F'
        'CONVENIO'#9'2'#9'C.'#9'F'
        'LIVRO'#9'4'#9'Livro'#9'F'
        'FOLHA'#9'4'#9'Folha'#9'F'
        'RECIBO'#9'6'#9'Recibo'#9'F'
        'EMOL'#9'8'#9'Emol.'#9'F'
        'FETJ'#9'6'#9'20%'#9'F'
        'FUND'#9'5'#9'5%'#9'F'
        'FUNP'#9'5'#9'5%'#9'F'
        'FUNA'#9'5'#9'4%'#9'F'
        'MUTUA'#9'6'#9'M'#250'tua'#9'F'
        'ACOTERJ'#9'5'#9'Acot.'#9'F'
        'DIST'#9'5'#9'Dist.'#9'F'
        'TOTAL'#9'6'#9'Total'#9'F'
        'SELO'#9'12'#9'Selo'#9'F'
        'STATUS'#9'16'#9'Status'#9'F')
      IniAttributes.Delimiter = ';;'
      IniAttributes.UnicodeIniFile = False
      TitleColor = clBtnFace
      FixedCols = 1
      ShowHorzScrollBar = True
      DataSource = dsRX
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
      ParentFont = False
      PopupMenu = PM
      TabOrder = 1
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Arial'
      TitleFont.Style = []
      TitleLines = 1
      TitleButtons = False
      UseTFields = False
      OnCalcCellColors = GridCalcCellColors
      OnMouseMove = GridMouseMove
      OnMouseUp = GridMouseUp
      PaintOptions.ActiveRecordColor = 16777158
    end
    object sPanel3: TsPanel
      Left = 5
      Top = 508
      Width = 872
      Height = 37
      TabOrder = 2
      SkinData.SkinSection = 'BARTITLE'
      object sLabel1: TsLabel
        Left = 27
        Top = 11
        Width = 67
        Height = 16
        Caption = 'Apontados'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 5059883
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      object sLabel2: TsLabel
        Left = 136
        Top = 11
        Width = 39
        Height = 16
        Caption = 'Pagos'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 5059883
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      object sLabel3: TsLabel
        Left = 334
        Top = 11
        Width = 74
        Height = 16
        Caption = 'Cancelados'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 5059883
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      object Shape1: TShape
        Left = 8
        Top = 12
        Width = 13
        Height = 13
        Brush.Color = clBlue
      end
      object Shape2: TShape
        Left = 117
        Top = 12
        Width = 13
        Height = 13
        Brush.Color = clGreen
      end
      object sLabel4: TsLabel
        Left = 218
        Top = 11
        Width = 74
        Height = 16
        Caption = 'Protestados'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 5059883
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      object Shape3: TShape
        Left = 200
        Top = 12
        Width = 13
        Height = 13
        Brush.Color = clPurple
      end
      object Shape4: TShape
        Left = 316
        Top = 12
        Width = 13
        Height = 13
        Brush.Color = clRed
      end
      object Shape5: TShape
        Left = 540
        Top = 12
        Width = 13
        Height = 13
        Brush.Color = 4227327
      end
      object sLabel5: TsLabel
        Left = 558
        Top = 11
        Width = 60
        Height = 16
        Caption = 'Certid'#245'es'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 5059883
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      object Shape6: TShape
        Left = 428
        Top = 12
        Width = 13
        Height = 13
        Brush.Color = clMaroon
      end
      object sLabel6: TsLabel
        Left = 446
        Top = 11
        Width = 72
        Height = 16
        Caption = 'Desist'#234'ncia'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = 5059883
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
    end
  end
  object dsXml: TDataSource
    DataSet = qryXml
    Left = 176
    Top = 200
  end
  object RX: TRxMemoryData
    FieldDefs = <
      item
        Name = 'CHECK'
        DataType = ftBoolean
      end
      item
        Name = 'DATA'
        DataType = ftDate
      end
      item
        Name = 'CODIGO'
        DataType = ftInteger
      end
      item
        Name = 'GRATUITO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LIVRO'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'FOLHA'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'RECIBO'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'EMOL'
        DataType = ftFloat
      end
      item
        Name = 'FETJ'
        DataType = ftFloat
      end
      item
        Name = 'FUND'
        DataType = ftFloat
      end
      item
        Name = 'FUNP'
        DataType = ftFloat
      end
      item
        Name = 'MUTUA'
        DataType = ftFloat
      end
      item
        Name = 'ACOTERJ'
        DataType = ftFloat
      end
      item
        Name = 'DIST'
        DataType = ftFloat
      end
      item
        Name = 'TOTAL'
        DataType = ftFloat
      end
      item
        Name = 'SELO'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'ID_ATO'
        DataType = ftInteger
      end
      item
        Name = 'DESCRICAO'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'PROTOCOLO'
        DataType = ftInteger
      end
      item
        Name = 'STATUS'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'FUNA'
        DataType = ftFloat
      end>
    Left = 224
    Top = 200
    object RXCHECK: TBooleanField
      DisplayLabel = '#'
      DisplayWidth = 5
      FieldName = 'CHECK'
    end
    object RXDATA: TDateField
      Alignment = taCenter
      DisplayLabel = 'Data'
      DisplayWidth = 10
      FieldName = 'DATA'
    end
    object RXCODIGO: TIntegerField
      Alignment = taCenter
      DisplayLabel = 'C'#243'digo'
      DisplayWidth = 10
      FieldName = 'CODIGO'
    end
    object RXGRATUITO: TStringField
      Alignment = taCenter
      DisplayLabel = 'Grat.'
      DisplayWidth = 4
      FieldName = 'GRATUITO'
      Size = 1
    end
    object RXCONVENIO: TStringField
      Alignment = taCenter
      DisplayLabel = 'Conv.'
      DisplayWidth = 4
      FieldName = 'CONVENIO'
      Size = 1
    end
    object RXLIVRO: TStringField
      Alignment = taCenter
      DisplayLabel = 'Livro'
      DisplayWidth = 15
      FieldName = 'LIVRO'
      Size = 15
    end
    object RXFOLHA: TStringField
      Alignment = taCenter
      DisplayLabel = 'Folha'
      DisplayWidth = 15
      FieldName = 'FOLHA'
      Size = 15
    end
    object RXRECIBO: TStringField
      Alignment = taCenter
      DisplayLabel = 'Recibo'
      DisplayWidth = 15
      FieldName = 'RECIBO'
      Size = 15
    end
    object RXEMOL: TFloatField
      DisplayLabel = 'Emol.'
      DisplayWidth = 10
      FieldName = 'EMOL'
      DisplayFormat = '#####0.00'
      EditFormat = '#####0.00'
    end
    object RXFETJ: TFloatField
      DisplayLabel = '20%'
      DisplayWidth = 10
      FieldName = 'FETJ'
      DisplayFormat = '#####0.00'
      EditFormat = '#####0.00'
    end
    object RXFUND: TFloatField
      DisplayLabel = '5%'
      DisplayWidth = 10
      FieldName = 'FUND'
      DisplayFormat = '#####0.00'
      EditFormat = '#####0.00'
    end
    object RXFUNP: TFloatField
      DisplayLabel = '5%'
      DisplayWidth = 10
      FieldName = 'FUNP'
      DisplayFormat = '#####0.00'
      EditFormat = '#####0.00'
    end
    object RXMUTUA: TFloatField
      DisplayLabel = 'M'#250'tua'
      DisplayWidth = 10
      FieldName = 'MUTUA'
      DisplayFormat = '#####0.00'
      EditFormat = '#####0.00'
    end
    object RXACOTERJ: TFloatField
      DisplayLabel = 'Acot.'
      DisplayWidth = 10
      FieldName = 'ACOTERJ'
      DisplayFormat = '#####0.00'
      EditFormat = '#####0.00'
    end
    object RXDIST: TFloatField
      DisplayLabel = 'Dist.'
      DisplayWidth = 10
      FieldName = 'DIST'
      DisplayFormat = '#####0.00'
      EditFormat = '#####0.00'
    end
    object RXTOTAL: TFloatField
      DisplayLabel = 'Total'
      DisplayWidth = 10
      FieldName = 'TOTAL'
      DisplayFormat = '#####0.00'
      EditFormat = '#####0.00'
    end
    object RXSELO: TStringField
      Alignment = taCenter
      DisplayLabel = 'Selo'
      DisplayWidth = 8
      FieldName = 'SELO'
      Size = 8
    end
    object RXID_ATO: TIntegerField
      DisplayWidth = 10
      FieldName = 'ID_ATO'
      Visible = False
    end
    object RXDESCRICAO: TStringField
      DisplayWidth = 100
      FieldName = 'DESCRICAO'
      Visible = False
      Size = 100
    end
    object RXPROTOCOLO: TIntegerField
      Alignment = taCenter
      FieldName = 'PROTOCOLO'
    end
    object RXSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 30
    end
    object RXFUNA: TFloatField
      DisplayLabel = '4%'
      FieldName = 'FUNA'
      DisplayFormat = '#####0.00'
      EditFormat = '#####0.00'
    end
  end
  object dsRX: TDataSource
    DataSet = RX
    Left = 272
    Top = 200
  end
  object PM: TPopupMenu
    Left = 80
    Top = 200
    object Marcartodos1: TMenuItem
      Caption = 'Marcar todos'
      OnClick = Marcartodos1Click
    end
    object Desmarcartodos1: TMenuItem
      Caption = 'Desmarcar todos'
      OnClick = Desmarcartodos1Click
    end
  end
  object qryXml: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT * FROM TITULOS'
      ''
      'WHERE DT_PROTOCOLO BETWEEN :D1 AND :D2')
    Left = 128
    Top = 200
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end>
    object qryXmlID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryXmlCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
    end
    object qryXmlRECIBO: TIntegerField
      FieldName = 'RECIBO'
      Origin = 'RECIBO'
    end
    object qryXmlDT_ENTRADA: TDateField
      FieldName = 'DT_ENTRADA'
      Origin = 'DT_ENTRADA'
    end
    object qryXmlDT_PROTOCOLO: TDateField
      FieldName = 'DT_PROTOCOLO'
      Origin = 'DT_PROTOCOLO'
    end
    object qryXmlPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object qryXmlLIVRO_PROTOCOLO: TIntegerField
      FieldName = 'LIVRO_PROTOCOLO'
      Origin = 'LIVRO_PROTOCOLO'
    end
    object qryXmlFOLHA_PROTOCOLO: TStringField
      FieldName = 'FOLHA_PROTOCOLO'
      Origin = 'FOLHA_PROTOCOLO'
      Size = 10
    end
    object qryXmlDT_PRAZO: TDateField
      FieldName = 'DT_PRAZO'
      Origin = 'DT_PRAZO'
    end
    object qryXmlDT_REGISTRO: TDateField
      FieldName = 'DT_REGISTRO'
      Origin = 'DT_REGISTRO'
    end
    object qryXmlREGISTRO: TIntegerField
      FieldName = 'REGISTRO'
      Origin = 'REGISTRO'
    end
    object qryXmlLIVRO_REGISTRO: TIntegerField
      FieldName = 'LIVRO_REGISTRO'
      Origin = 'LIVRO_REGISTRO'
    end
    object qryXmlFOLHA_REGISTRO: TStringField
      FieldName = 'FOLHA_REGISTRO'
      Origin = 'FOLHA_REGISTRO'
      Size = 10
    end
    object qryXmlSELO_REGISTRO: TStringField
      FieldName = 'SELO_REGISTRO'
      Origin = 'SELO_REGISTRO'
      Size = 9
    end
    object qryXmlDT_PAGAMENTO: TDateField
      FieldName = 'DT_PAGAMENTO'
      Origin = 'DT_PAGAMENTO'
    end
    object qryXmlSELO_PAGAMENTO: TStringField
      FieldName = 'SELO_PAGAMENTO'
      Origin = 'SELO_PAGAMENTO'
      Size = 9
    end
    object qryXmlRECIBO_PAGAMENTO: TIntegerField
      FieldName = 'RECIBO_PAGAMENTO'
      Origin = 'RECIBO_PAGAMENTO'
    end
    object qryXmlCOBRANCA: TStringField
      FieldName = 'COBRANCA'
      Origin = 'COBRANCA'
      FixedChar = True
      Size = 2
    end
    object qryXmlEMOLUMENTOS: TFloatField
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
    end
    object qryXmlFETJ: TFloatField
      FieldName = 'FETJ'
      Origin = 'FETJ'
    end
    object qryXmlFUNDPERJ: TFloatField
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
    end
    object qryXmlFUNPERJ: TFloatField
      FieldName = 'FUNPERJ'
      Origin = 'FUNPERJ'
    end
    object qryXmlFUNARPEN: TFloatField
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
    end
    object qryXmlPMCMV: TFloatField
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
    end
    object qryXmlISS: TFloatField
      FieldName = 'ISS'
      Origin = 'ISS'
    end
    object qryXmlMUTUA: TFloatField
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
    end
    object qryXmlDISTRIBUICAO: TFloatField
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
    end
    object qryXmlACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
    end
    object qryXmlTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
    end
    object qryXmlTIPO_PROTESTO: TIntegerField
      FieldName = 'TIPO_PROTESTO'
      Origin = 'TIPO_PROTESTO'
    end
    object qryXmlTIPO_TITULO: TIntegerField
      FieldName = 'TIPO_TITULO'
      Origin = 'TIPO_TITULO'
    end
    object qryXmlNUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
      Origin = 'NUMERO_TITULO'
    end
    object qryXmlDT_TITULO: TDateField
      FieldName = 'DT_TITULO'
      Origin = 'DT_TITULO'
    end
    object qryXmlBANCO: TStringField
      FieldName = 'BANCO'
      Origin = 'BANCO'
    end
    object qryXmlAGENCIA: TStringField
      FieldName = 'AGENCIA'
      Origin = 'AGENCIA'
      Size = 10
    end
    object qryXmlCONTA: TStringField
      FieldName = 'CONTA'
      Origin = 'CONTA'
      Size = 10
    end
    object qryXmlVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
      Origin = 'VALOR_TITULO'
    end
    object qryXmlSALDO_PROTESTO: TFloatField
      FieldName = 'SALDO_PROTESTO'
      Origin = 'SALDO_PROTESTO'
    end
    object qryXmlDT_VENCIMENTO: TDateField
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
    end
    object qryXmlCONVENIO: TStringField
      FieldName = 'CONVENIO'
      Origin = 'CONVENIO'
      FixedChar = True
      Size = 1
    end
    object qryXmlTIPO_APRESENTACAO: TStringField
      FieldName = 'TIPO_APRESENTACAO'
      Origin = 'TIPO_APRESENTACAO'
      FixedChar = True
      Size = 1
    end
    object qryXmlDT_ENVIO: TDateField
      FieldName = 'DT_ENVIO'
      Origin = 'DT_ENVIO'
    end
    object qryXmlCODIGO_APRESENTANTE: TStringField
      FieldName = 'CODIGO_APRESENTANTE'
      Origin = 'CODIGO_APRESENTANTE'
      Size = 10
    end
    object qryXmlTIPO_INTIMACAO: TStringField
      FieldName = 'TIPO_INTIMACAO'
      Origin = 'TIPO_INTIMACAO'
      FixedChar = True
      Size = 1
    end
    object qryXmlMOTIVO_INTIMACAO: TMemoField
      FieldName = 'MOTIVO_INTIMACAO'
      Origin = 'MOTIVO_INTIMACAO'
      BlobType = ftMemo
    end
    object qryXmlDT_INTIMACAO: TDateField
      FieldName = 'DT_INTIMACAO'
      Origin = 'DT_INTIMACAO'
    end
    object qryXmlDT_PUBLICACAO: TDateField
      FieldName = 'DT_PUBLICACAO'
      Origin = 'DT_PUBLICACAO'
    end
    object qryXmlVALOR_PAGAMENTO: TFloatField
      FieldName = 'VALOR_PAGAMENTO'
      Origin = 'VALOR_PAGAMENTO'
    end
    object qryXmlAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Origin = 'APRESENTANTE'
      Size = 100
    end
    object qryXmlCPF_CNPJ_APRESENTANTE: TStringField
      FieldName = 'CPF_CNPJ_APRESENTANTE'
      Origin = 'CPF_CNPJ_APRESENTANTE'
      Size = 15
    end
    object qryXmlTIPO_APRESENTANTE: TStringField
      FieldName = 'TIPO_APRESENTANTE'
      Origin = 'TIPO_APRESENTANTE'
      FixedChar = True
      Size = 1
    end
    object qryXmlCEDENTE: TStringField
      FieldName = 'CEDENTE'
      Origin = 'CEDENTE'
      Size = 100
    end
    object qryXmlNOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Origin = 'NOSSO_NUMERO'
      Size = 15
    end
    object qryXmlSACADOR: TStringField
      FieldName = 'SACADOR'
      Origin = 'SACADOR'
      Size = 100
    end
    object qryXmlDEVEDOR: TStringField
      FieldName = 'DEVEDOR'
      Origin = 'DEVEDOR'
      Size = 100
    end
    object qryXmlCPF_CNPJ_DEVEDOR: TStringField
      FieldName = 'CPF_CNPJ_DEVEDOR'
      Origin = 'CPF_CNPJ_DEVEDOR'
      Size = 15
    end
    object qryXmlTIPO_DEVEDOR: TStringField
      FieldName = 'TIPO_DEVEDOR'
      Origin = 'TIPO_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object qryXmlAGENCIA_CEDENTE: TStringField
      FieldName = 'AGENCIA_CEDENTE'
      Origin = 'AGENCIA_CEDENTE'
      Size = 15
    end
    object qryXmlPRACA_PROTESTO: TStringField
      FieldName = 'PRACA_PROTESTO'
      Origin = 'PRACA_PROTESTO'
    end
    object qryXmlTIPO_ENDOSSO: TStringField
      FieldName = 'TIPO_ENDOSSO'
      Origin = 'TIPO_ENDOSSO'
      FixedChar = True
      Size = 1
    end
    object qryXmlACEITE: TStringField
      FieldName = 'ACEITE'
      Origin = 'ACEITE'
      FixedChar = True
      Size = 1
    end
    object qryXmlCPF_ESCREVENTE: TStringField
      FieldName = 'CPF_ESCREVENTE'
      Origin = 'CPF_ESCREVENTE'
      Size = 11
    end
    object qryXmlCPF_ESCREVENTE_PG: TStringField
      FieldName = 'CPF_ESCREVENTE_PG'
      Origin = 'CPF_ESCREVENTE_PG'
      Size = 11
    end
    object qryXmlOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      BlobType = ftMemo
    end
    object qryXmlDT_SUSTADO: TDateField
      FieldName = 'DT_SUSTADO'
      Origin = 'DT_SUSTADO'
    end
    object qryXmlDT_RETIRADO: TDateField
      FieldName = 'DT_RETIRADO'
      Origin = 'DT_RETIRADO'
    end
    object qryXmlSTATUS: TStringField
      FieldName = 'STATUS'
      Origin = 'STATUS'
    end
    object qryXmlPROTESTADO: TStringField
      FieldName = 'PROTESTADO'
      Origin = 'PROTESTADO'
      FixedChar = True
      Size = 1
    end
    object qryXmlENVIADO_APONTAMENTO: TStringField
      FieldName = 'ENVIADO_APONTAMENTO'
      Origin = 'ENVIADO_APONTAMENTO'
      FixedChar = True
      Size = 1
    end
    object qryXmlENVIADO_PROTESTO: TStringField
      FieldName = 'ENVIADO_PROTESTO'
      Origin = 'ENVIADO_PROTESTO'
      FixedChar = True
      Size = 1
    end
    object qryXmlENVIADO_PAGAMENTO: TStringField
      FieldName = 'ENVIADO_PAGAMENTO'
      Origin = 'ENVIADO_PAGAMENTO'
      FixedChar = True
      Size = 1
    end
    object qryXmlENVIADO_RETIRADO: TStringField
      FieldName = 'ENVIADO_RETIRADO'
      Origin = 'ENVIADO_RETIRADO'
      FixedChar = True
      Size = 1
    end
    object qryXmlENVIADO_SUSTADO: TStringField
      FieldName = 'ENVIADO_SUSTADO'
      Origin = 'ENVIADO_SUSTADO'
      FixedChar = True
      Size = 1
    end
    object qryXmlENVIADO_DEVOLVIDO: TStringField
      FieldName = 'ENVIADO_DEVOLVIDO'
      Origin = 'ENVIADO_DEVOLVIDO'
      FixedChar = True
      Size = 1
    end
    object qryXmlEXPORTADO: TStringField
      FieldName = 'EXPORTADO'
      Origin = 'EXPORTADO'
      FixedChar = True
      Size = 1
    end
    object qryXmlAVALISTA_DEVEDOR: TStringField
      FieldName = 'AVALISTA_DEVEDOR'
      Origin = 'AVALISTA_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object qryXmlFINS_FALIMENTARES: TStringField
      FieldName = 'FINS_FALIMENTARES'
      Origin = 'FINS_FALIMENTARES'
      FixedChar = True
      Size = 1
    end
    object qryXmlTARIFA_BANCARIA: TFloatField
      FieldName = 'TARIFA_BANCARIA'
      Origin = 'TARIFA_BANCARIA'
    end
    object qryXmlFORMA_PAGAMENTO: TStringField
      FieldName = 'FORMA_PAGAMENTO'
      Origin = 'FORMA_PAGAMENTO'
      FixedChar = True
      Size = 2
    end
    object qryXmlNUMERO_PAGAMENTO: TStringField
      FieldName = 'NUMERO_PAGAMENTO'
      Origin = 'NUMERO_PAGAMENTO'
      Size = 40
    end
    object qryXmlARQUIVO: TStringField
      FieldName = 'ARQUIVO'
      Origin = 'ARQUIVO'
    end
    object qryXmlRETORNO: TStringField
      FieldName = 'RETORNO'
      Origin = 'RETORNO'
    end
    object qryXmlDT_DEVOLVIDO: TDateField
      FieldName = 'DT_DEVOLVIDO'
      Origin = 'DT_DEVOLVIDO'
    end
    object qryXmlCPF_CNPJ_SACADOR: TStringField
      FieldName = 'CPF_CNPJ_SACADOR'
      Origin = 'CPF_CNPJ_SACADOR'
      Size = 14
    end
    object qryXmlID_MSG: TIntegerField
      FieldName = 'ID_MSG'
      Origin = 'ID_MSG'
    end
    object qryXmlVALOR_AR: TFloatField
      FieldName = 'VALOR_AR'
      Origin = 'VALOR_AR'
    end
    object qryXmlELETRONICO: TStringField
      FieldName = 'ELETRONICO'
      Origin = 'ELETRONICO'
      FixedChar = True
      Size = 1
    end
    object qryXmlIRREGULARIDADE: TIntegerField
      FieldName = 'IRREGULARIDADE'
      Origin = 'IRREGULARIDADE'
    end
    object qryXmlAVISTA: TStringField
      FieldName = 'AVISTA'
      Origin = 'AVISTA'
      FixedChar = True
      Size = 1
    end
    object qryXmlSALDO_TITULO: TFloatField
      FieldName = 'SALDO_TITULO'
      Origin = 'SALDO_TITULO'
    end
    object qryXmlTIPO_SUSTACAO: TStringField
      FieldName = 'TIPO_SUSTACAO'
      Origin = 'TIPO_SUSTACAO'
      FixedChar = True
      Size = 1
    end
    object qryXmlRETORNO_PROTESTO: TStringField
      FieldName = 'RETORNO_PROTESTO'
      Origin = 'RETORNO_PROTESTO'
      FixedChar = True
      Size = 1
    end
    object qryXmlDT_RETORNO_PROTESTO: TDateField
      FieldName = 'DT_RETORNO_PROTESTO'
      Origin = 'DT_RETORNO_PROTESTO'
    end
    object qryXmlDT_DEFINITIVA: TDateField
      FieldName = 'DT_DEFINITIVA'
      Origin = 'DT_DEFINITIVA'
    end
    object qryXmlJUDICIAL: TStringField
      FieldName = 'JUDICIAL'
      Origin = 'JUDICIAL'
      FixedChar = True
      Size = 1
    end
    object qryXmlALEATORIO_PROTESTO: TStringField
      FieldName = 'ALEATORIO_PROTESTO'
      Origin = 'ALEATORIO_PROTESTO'
      Size = 3
    end
    object qryXmlALEATORIO_SOLUCAO: TStringField
      FieldName = 'ALEATORIO_SOLUCAO'
      Origin = 'ALEATORIO_SOLUCAO'
      Size = 3
    end
    object qryXmlCCT: TStringField
      FieldName = 'CCT'
      Origin = 'CCT'
      Size = 9
    end
    object qryXmlDETERMINACAO: TStringField
      FieldName = 'DETERMINACAO'
      Origin = 'DETERMINACAO'
      Size = 100
    end
    object qryXmlANTIGO: TStringField
      FieldName = 'ANTIGO'
      Origin = 'ANTIGO'
      FixedChar = True
      Size = 1
    end
    object qryXmlLETRA: TStringField
      FieldName = 'LETRA'
      Origin = 'LETRA'
      Size = 1
    end
    object qryXmlPROTOCOLO_CARTORIO: TIntegerField
      FieldName = 'PROTOCOLO_CARTORIO'
      Origin = 'PROTOCOLO_CARTORIO'
    end
    object qryXmlARQUIVO_CARTORIO: TStringField
      FieldName = 'ARQUIVO_CARTORIO'
      Origin = 'ARQUIVO_CARTORIO'
    end
    object qryXmlRETORNO_CARTORIO: TStringField
      FieldName = 'RETORNO_CARTORIO'
      Origin = 'RETORNO_CARTORIO'
    end
    object qryXmlDT_DEVOLVIDO_CARTORIO: TDateField
      FieldName = 'DT_DEVOLVIDO_CARTORIO'
      Origin = 'DT_DEVOLVIDO_CARTORIO'
    end
    object qryXmlCARTORIO: TIntegerField
      FieldName = 'CARTORIO'
      Origin = 'CARTORIO'
    end
    object qryXmlDT_PROTOCOLO_CARTORIO: TDateField
      FieldName = 'DT_PROTOCOLO_CARTORIO'
      Origin = 'DT_PROTOCOLO_CARTORIO'
    end
  end
  object qryCertidao: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT * FROM CERTIDOES'
      ''
      'WHERE DT_CERTIDAO =:D1')
    Left = 112
    Top = 264
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end>
    object qryCertidaoID_CERTIDAO: TIntegerField
      FieldName = 'ID_CERTIDAO'
      Origin = 'ID_CERTIDAO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryCertidaoID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
    end
    object qryCertidaoORDEM: TIntegerField
      FieldName = 'ORDEM'
      Origin = 'ORDEM'
    end
    object qryCertidaoDT_PEDIDO: TDateField
      FieldName = 'DT_PEDIDO'
      Origin = 'DT_PEDIDO'
    end
    object qryCertidaoDT_ENTREGA: TDateField
      FieldName = 'DT_ENTREGA'
      Origin = 'DT_ENTREGA'
    end
    object qryCertidaoDT_CERTIDAO: TDateField
      FieldName = 'DT_CERTIDAO'
      Origin = 'DT_CERTIDAO'
    end
    object qryCertidaoTIPO_CERTIDAO: TStringField
      FieldName = 'TIPO_CERTIDAO'
      Origin = 'TIPO_CERTIDAO'
      FixedChar = True
      Size = 1
    end
    object qryCertidaoNUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
      Origin = 'NUMERO_TITULO'
    end
    object qryCertidaoANOS: TIntegerField
      FieldName = 'ANOS'
      Origin = 'ANOS'
    end
    object qryCertidaoCONVENIO: TStringField
      FieldName = 'CONVENIO'
      Origin = 'CONVENIO'
      FixedChar = True
      Size = 1
    end
    object qryCertidaoCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
    end
    object qryCertidaoSELO: TStringField
      FieldName = 'SELO'
      Origin = 'SELO'
      Size = 9
    end
    object qryCertidaoALEATORIO: TStringField
      FieldName = 'ALEATORIO'
      Origin = 'ALEATORIO'
      Size = 3
    end
    object qryCertidaoRECIBO: TIntegerField
      FieldName = 'RECIBO'
      Origin = 'RECIBO'
    end
    object qryCertidaoREQUERIDO: TStringField
      FieldName = 'REQUERIDO'
      Origin = 'REQUERIDO'
      Size = 100
    end
    object qryCertidaoTIPO_REQUERIDO: TStringField
      FieldName = 'TIPO_REQUERIDO'
      Origin = 'TIPO_REQUERIDO'
      FixedChar = True
      Size = 1
    end
    object qryCertidaoCPF_CNPJ_REQUERIDO: TStringField
      FieldName = 'CPF_CNPJ_REQUERIDO'
      Origin = 'CPF_CNPJ_REQUERIDO'
      Size = 15
    end
    object qryCertidaoREQUERENTE: TStringField
      FieldName = 'REQUERENTE'
      Origin = 'REQUERENTE'
      Size = 100
    end
    object qryCertidaoCPF_CNPJ_REQUERENTE: TStringField
      FieldName = 'CPF_CNPJ_REQUERENTE'
      Origin = 'CPF_CNPJ_REQUERENTE'
      Size = 14
    end
    object qryCertidaoTELEFONE_REQUERENTE: TStringField
      FieldName = 'TELEFONE_REQUERENTE'
      Origin = 'TELEFONE_REQUERENTE'
      Size = 10
    end
    object qryCertidaoRESULTADO: TStringField
      FieldName = 'RESULTADO'
      Origin = 'RESULTADO'
      FixedChar = True
      Size = 1
    end
    object qryCertidaoFOLHAS: TIntegerField
      FieldName = 'FOLHAS'
      Origin = 'FOLHAS'
    end
    object qryCertidaoREGISTROS: TIntegerField
      FieldName = 'REGISTROS'
      Origin = 'REGISTROS'
    end
    object qryCertidaoCOBRANCA: TStringField
      FieldName = 'COBRANCA'
      Origin = 'COBRANCA'
      FixedChar = True
      Size = 2
    end
    object qryCertidaoEMOLUMENTOS: TFloatField
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
    end
    object qryCertidaoFETJ: TFloatField
      FieldName = 'FETJ'
      Origin = 'FETJ'
    end
    object qryCertidaoFUNDPERJ: TFloatField
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
    end
    object qryCertidaoFUNPERJ: TFloatField
      FieldName = 'FUNPERJ'
      Origin = 'FUNPERJ'
    end
    object qryCertidaoFUNARPEN: TFloatField
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
    end
    object qryCertidaoPMCMV: TFloatField
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
    end
    object qryCertidaoISS: TFloatField
      FieldName = 'ISS'
      Origin = 'ISS'
    end
    object qryCertidaoMUTUA: TFloatField
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
    end
    object qryCertidaoACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
    end
    object qryCertidaoDISTRIBUICAO: TFloatField
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
    end
    object qryCertidaoAPONTAMENTO: TFloatField
      FieldName = 'APONTAMENTO'
      Origin = 'APONTAMENTO'
    end
    object qryCertidaoCAPA: TFloatField
      FieldName = 'CAPA'
      Origin = 'CAPA'
    end
    object qryCertidaoTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
    end
    object qryCertidaoEXRECIBO: TIntegerField
      FieldName = 'EXRECIBO'
      Origin = 'EXRECIBO'
    end
    object qryCertidaoEXCOBRANCA: TStringField
      FieldName = 'EXCOBRANCA'
      Origin = 'EXCOBRANCA'
      FixedChar = True
      Size = 2
    end
    object qryCertidaoEXEMOLUMENTOS: TFloatField
      FieldName = 'EXEMOLUMENTOS'
      Origin = 'EXEMOLUMENTOS'
    end
    object qryCertidaoEXFETJ: TFloatField
      FieldName = 'EXFETJ'
      Origin = 'EXFETJ'
    end
    object qryCertidaoEXFUNDPERJ: TFloatField
      FieldName = 'EXFUNDPERJ'
      Origin = 'EXFUNDPERJ'
    end
    object qryCertidaoEXFUNPERJ: TFloatField
      FieldName = 'EXFUNPERJ'
      Origin = 'EXFUNPERJ'
    end
    object qryCertidaoEXISS: TFloatField
      FieldName = 'EXISS'
      Origin = 'EXISS'
    end
    object qryCertidaoEXFUNARPEN: TFloatField
      FieldName = 'EXFUNARPEN'
      Origin = 'EXFUNARPEN'
    end
    object qryCertidaoEXTOTAL: TFloatField
      FieldName = 'EXTOTAL'
      Origin = 'EXTOTAL'
    end
    object qryCertidaoCCT: TStringField
      FieldName = 'CCT'
      Origin = 'CCT'
      Size = 9
    end
    object qryCertidaoESCREVENTE: TStringField
      FieldName = 'ESCREVENTE'
      Origin = 'ESCREVENTE'
      Size = 11
    end
    object qryCertidaoRESPONSAVEL: TStringField
      FieldName = 'RESPONSAVEL'
      Origin = 'RESPONSAVEL'
      Size = 11
    end
    object qryCertidaoOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      BlobType = ftMemo
    end
    object qryCertidaoENVIADO: TStringField
      FieldName = 'ENVIADO'
      Origin = 'ENVIADO'
      FixedChar = True
      Size = 1
    end
    object qryCertidaoENVIADO_FLS: TStringField
      FieldName = 'ENVIADO_FLS'
      Origin = 'ENVIADO_FLS'
      FixedChar = True
      Size = 1
    end
    object qryCertidaoQTD: TIntegerField
      FieldName = 'QTD'
      Origin = 'QTD'
    end
  end
  object qryBoletos: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select * from BOLETOS'
      ''
      'where DATA_EMISSAO=:D1')
    Left = 176
    Top = 264
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end>
    object qryBoletosID_BOLETO: TIntegerField
      FieldName = 'ID_BOLETO'
      Origin = 'ID_BOLETO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryBoletosID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
    end
    object qryBoletosID_DEVEDOR: TIntegerField
      FieldName = 'ID_DEVEDOR'
      Origin = 'ID_DEVEDOR'
    end
    object qryBoletosESPECIE_DOCUMENTO: TStringField
      FieldName = 'ESPECIE_DOCUMENTO'
      Origin = 'ESPECIE_DOCUMENTO'
      Size = 5
    end
    object qryBoletosMOEDA: TStringField
      FieldName = 'MOEDA'
      Origin = 'MOEDA'
      Size = 5
    end
    object qryBoletosACEITE: TStringField
      FieldName = 'ACEITE'
      Origin = 'ACEITE'
      FixedChar = True
      Size = 1
    end
    object qryBoletosCARTEIRA: TStringField
      FieldName = 'CARTEIRA'
      Origin = 'CARTEIRA'
      Size = 5
    end
    object qryBoletosNOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Origin = 'NOSSO_NUMERO'
      Size = 8
    end
    object qryBoletosVALOR_MORA_JUROS: TFloatField
      FieldName = 'VALOR_MORA_JUROS'
      Origin = 'VALOR_MORA_JUROS'
    end
    object qryBoletosVALOR_DESCONTO: TFloatField
      FieldName = 'VALOR_DESCONTO'
      Origin = 'VALOR_DESCONTO'
    end
    object qryBoletosVALOR_ABATIMENTO: TFloatField
      FieldName = 'VALOR_ABATIMENTO'
      Origin = 'VALOR_ABATIMENTO'
    end
    object qryBoletosPORCENTO_MULTA: TFloatField
      FieldName = 'PORCENTO_MULTA'
      Origin = 'PORCENTO_MULTA'
    end
    object qryBoletosDATA_MULTA_JUROS: TDateField
      FieldName = 'DATA_MULTA_JUROS'
      Origin = 'DATA_MULTA_JUROS'
    end
    object qryBoletosDATA_DESCONTO: TDateField
      FieldName = 'DATA_DESCONTO'
      Origin = 'DATA_DESCONTO'
    end
    object qryBoletosDATA_ABATIMENTO: TDateField
      FieldName = 'DATA_ABATIMENTO'
      Origin = 'DATA_ABATIMENTO'
    end
    object qryBoletosDATA_PROTESTO: TDateField
      FieldName = 'DATA_PROTESTO'
      Origin = 'DATA_PROTESTO'
    end
    object qryBoletosNUMERO_DOCUMENTO: TStringField
      FieldName = 'NUMERO_DOCUMENTO'
      Origin = 'NUMERO_DOCUMENTO'
    end
    object qryBoletosVALOR: TFloatField
      FieldName = 'VALOR'
      Origin = 'VALOR'
    end
    object qryBoletosDATA_EMISSAO: TDateField
      FieldName = 'DATA_EMISSAO'
      Origin = 'DATA_EMISSAO'
    end
    object qryBoletosDATA_VENCIMENTO: TDateField
      FieldName = 'DATA_VENCIMENTO'
      Origin = 'DATA_VENCIMENTO'
    end
    object qryBoletosINSTRUCAO1: TStringField
      FieldName = 'INSTRUCAO1'
      Origin = 'INSTRUCAO1'
      Size = 100
    end
    object qryBoletosINSTRUCAO2: TStringField
      FieldName = 'INSTRUCAO2'
      Origin = 'INSTRUCAO2'
      Size = 100
    end
    object qryBoletosINSTRUCAO3: TStringField
      FieldName = 'INSTRUCAO3'
      Origin = 'INSTRUCAO3'
      Size = 100
    end
    object qryBoletosINSTRUCAO4: TStringField
      FieldName = 'INSTRUCAO4'
      Origin = 'INSTRUCAO4'
      Size = 100
    end
    object qryBoletosINSTRUCAO5: TStringField
      FieldName = 'INSTRUCAO5'
      Origin = 'INSTRUCAO5'
      Size = 100
    end
    object qryBoletosDATA_PROTOCOLO: TDateField
      FieldName = 'DATA_PROTOCOLO'
      Origin = 'DATA_PROTOCOLO'
    end
    object qryBoletosPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object qryBoletosHORA: TTimeField
      FieldName = 'HORA'
      Origin = 'HORA'
    end
    object qryBoletosEMISSOR: TStringField
      FieldName = 'EMISSOR'
      Origin = 'EMISSOR'
    end
    object qryBoletosPAGO: TStringField
      FieldName = 'PAGO'
      Origin = 'PAGO'
      FixedChar = True
      Size = 1
    end
    object qryBoletosEMOLUMENTOS: TFloatField
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
    end
    object qryBoletosFETJ: TFloatField
      FieldName = 'FETJ'
      Origin = 'FETJ'
    end
    object qryBoletosFUNDPERJ: TFloatField
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
    end
    object qryBoletosFUNPERJ: TFloatField
      FieldName = 'FUNPERJ'
      Origin = 'FUNPERJ'
    end
    object qryBoletosFUNARPEN: TFloatField
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
    end
    object qryBoletosPMCMV: TFloatField
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
    end
    object qryBoletosTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
    end
    object qryBoletosCCT: TStringField
      FieldName = 'CCT'
      Origin = 'CCT'
      Size = 9
    end
    object qryBoletosENVIADO: TStringField
      FieldName = 'ENVIADO'
      Origin = 'ENVIADO'
      FixedChar = True
      Size = 1
    end
    object qryBoletosDATA_BAIXA: TDateField
      FieldName = 'DATA_BAIXA'
      Origin = 'DATA_BAIXA'
    end
  end
  object qryCancelado: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select * from CERTIDOES'
      ''
      'where ID_ATO=:ID_ATO'
      '')
    Left = 240
    Top = 264
    ParamData = <
      item
        Name = 'ID_ATO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryCanceladoID_CERTIDAO: TIntegerField
      FieldName = 'ID_CERTIDAO'
      Origin = 'ID_CERTIDAO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryCanceladoID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
    end
    object qryCanceladoORDEM: TIntegerField
      FieldName = 'ORDEM'
      Origin = 'ORDEM'
    end
    object qryCanceladoDT_PEDIDO: TDateField
      FieldName = 'DT_PEDIDO'
      Origin = 'DT_PEDIDO'
    end
    object qryCanceladoDT_ENTREGA: TDateField
      FieldName = 'DT_ENTREGA'
      Origin = 'DT_ENTREGA'
    end
    object qryCanceladoDT_CERTIDAO: TDateField
      FieldName = 'DT_CERTIDAO'
      Origin = 'DT_CERTIDAO'
    end
    object qryCanceladoTIPO_CERTIDAO: TStringField
      FieldName = 'TIPO_CERTIDAO'
      Origin = 'TIPO_CERTIDAO'
      FixedChar = True
      Size = 1
    end
    object qryCanceladoNUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
      Origin = 'NUMERO_TITULO'
    end
    object qryCanceladoANOS: TIntegerField
      FieldName = 'ANOS'
      Origin = 'ANOS'
    end
    object qryCanceladoCONVENIO: TStringField
      FieldName = 'CONVENIO'
      Origin = 'CONVENIO'
      FixedChar = True
      Size = 1
    end
    object qryCanceladoCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
    end
    object qryCanceladoSELO: TStringField
      FieldName = 'SELO'
      Origin = 'SELO'
      Size = 9
    end
    object qryCanceladoALEATORIO: TStringField
      FieldName = 'ALEATORIO'
      Origin = 'ALEATORIO'
      Size = 3
    end
    object qryCanceladoRECIBO: TIntegerField
      FieldName = 'RECIBO'
      Origin = 'RECIBO'
    end
    object qryCanceladoREQUERIDO: TStringField
      FieldName = 'REQUERIDO'
      Origin = 'REQUERIDO'
      Size = 100
    end
    object qryCanceladoTIPO_REQUERIDO: TStringField
      FieldName = 'TIPO_REQUERIDO'
      Origin = 'TIPO_REQUERIDO'
      FixedChar = True
      Size = 1
    end
    object qryCanceladoCPF_CNPJ_REQUERIDO: TStringField
      FieldName = 'CPF_CNPJ_REQUERIDO'
      Origin = 'CPF_CNPJ_REQUERIDO'
      Size = 15
    end
    object qryCanceladoREQUERENTE: TStringField
      FieldName = 'REQUERENTE'
      Origin = 'REQUERENTE'
      Size = 100
    end
    object qryCanceladoCPF_CNPJ_REQUERENTE: TStringField
      FieldName = 'CPF_CNPJ_REQUERENTE'
      Origin = 'CPF_CNPJ_REQUERENTE'
      Size = 14
    end
    object qryCanceladoTELEFONE_REQUERENTE: TStringField
      FieldName = 'TELEFONE_REQUERENTE'
      Origin = 'TELEFONE_REQUERENTE'
      Size = 10
    end
    object qryCanceladoRESULTADO: TStringField
      FieldName = 'RESULTADO'
      Origin = 'RESULTADO'
      FixedChar = True
      Size = 1
    end
    object qryCanceladoFOLHAS: TIntegerField
      FieldName = 'FOLHAS'
      Origin = 'FOLHAS'
    end
    object qryCanceladoREGISTROS: TIntegerField
      FieldName = 'REGISTROS'
      Origin = 'REGISTROS'
    end
    object qryCanceladoCOBRANCA: TStringField
      FieldName = 'COBRANCA'
      Origin = 'COBRANCA'
      FixedChar = True
      Size = 2
    end
    object qryCanceladoEMOLUMENTOS: TFloatField
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
    end
    object qryCanceladoFETJ: TFloatField
      FieldName = 'FETJ'
      Origin = 'FETJ'
    end
    object qryCanceladoFUNDPERJ: TFloatField
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
    end
    object qryCanceladoFUNPERJ: TFloatField
      FieldName = 'FUNPERJ'
      Origin = 'FUNPERJ'
    end
    object qryCanceladoFUNARPEN: TFloatField
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
    end
    object qryCanceladoPMCMV: TFloatField
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
    end
    object qryCanceladoISS: TFloatField
      FieldName = 'ISS'
      Origin = 'ISS'
    end
    object qryCanceladoMUTUA: TFloatField
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
    end
    object qryCanceladoACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
    end
    object qryCanceladoDISTRIBUICAO: TFloatField
      FieldName = 'DISTRIBUICAO'
      Origin = 'DISTRIBUICAO'
    end
    object qryCanceladoAPONTAMENTO: TFloatField
      FieldName = 'APONTAMENTO'
      Origin = 'APONTAMENTO'
    end
    object qryCanceladoCAPA: TFloatField
      FieldName = 'CAPA'
      Origin = 'CAPA'
    end
    object qryCanceladoTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
    end
    object qryCanceladoEXRECIBO: TIntegerField
      FieldName = 'EXRECIBO'
      Origin = 'EXRECIBO'
    end
    object qryCanceladoEXCOBRANCA: TStringField
      FieldName = 'EXCOBRANCA'
      Origin = 'EXCOBRANCA'
      FixedChar = True
      Size = 2
    end
    object qryCanceladoEXEMOLUMENTOS: TFloatField
      FieldName = 'EXEMOLUMENTOS'
      Origin = 'EXEMOLUMENTOS'
    end
    object qryCanceladoEXFETJ: TFloatField
      FieldName = 'EXFETJ'
      Origin = 'EXFETJ'
    end
    object qryCanceladoEXFUNDPERJ: TFloatField
      FieldName = 'EXFUNDPERJ'
      Origin = 'EXFUNDPERJ'
    end
    object qryCanceladoEXFUNPERJ: TFloatField
      FieldName = 'EXFUNPERJ'
      Origin = 'EXFUNPERJ'
    end
    object qryCanceladoEXISS: TFloatField
      FieldName = 'EXISS'
      Origin = 'EXISS'
    end
    object qryCanceladoEXFUNARPEN: TFloatField
      FieldName = 'EXFUNARPEN'
      Origin = 'EXFUNARPEN'
    end
    object qryCanceladoEXTOTAL: TFloatField
      FieldName = 'EXTOTAL'
      Origin = 'EXTOTAL'
    end
    object qryCanceladoCCT: TStringField
      FieldName = 'CCT'
      Origin = 'CCT'
      Size = 9
    end
    object qryCanceladoESCREVENTE: TStringField
      FieldName = 'ESCREVENTE'
      Origin = 'ESCREVENTE'
      Size = 11
    end
    object qryCanceladoRESPONSAVEL: TStringField
      FieldName = 'RESPONSAVEL'
      Origin = 'RESPONSAVEL'
      Size = 11
    end
    object qryCanceladoOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      Origin = 'OBSERVACAO'
      BlobType = ftMemo
    end
    object qryCanceladoENVIADO: TStringField
      FieldName = 'ENVIADO'
      Origin = 'ENVIADO'
      FixedChar = True
      Size = 1
    end
    object qryCanceladoENVIADO_FLS: TStringField
      FieldName = 'ENVIADO_FLS'
      Origin = 'ENVIADO_FLS'
      FixedChar = True
      Size = 1
    end
    object qryCanceladoQTD: TIntegerField
      FieldName = 'QTD'
      Origin = 'QTD'
    end
  end
end
