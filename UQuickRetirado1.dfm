�
 TFQUICKRETIRADO1 0�{  TPF0TFQuickRetirado1FQuickRetirado1Left� TopvCaptionDesistencia1ClientHeightsClientWidthAColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderScaledOnCreate
FormCreatePixelsPerInch`
TextHeight 	TQuickRepDesistenciaLeftTop(WidthHeightcShowingPreviewFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.ContinuousPage.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPreviewWidth�PreviewHeight�PrevInitialZoomqrZoomToWidthPreviewDefaultSaveTypestQRPPreviewLeft 
PreviewTop  TQRBandQRBand1Left9Top9Width�HeightWFrame.DrawTop	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.ValuesTUUUUU��@      :�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbTitle TQRLabellbCidadeLeft-Top'WidthNHeightSize.Values�������@UUUUUU�@      `�@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionCidade - EstadoColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabel	lbTitularLeft,Top6WidthPHeightSize.Values�������@      p�@      ��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionNome do TitularColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabel
lbCartorioLeft Top
Width� HeightSize.Values��������@UUUUUUU�@��������@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   NOME DO CARTÓRIOColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel2Left� TopNWidth)HeightSize.Values��������@       �@      `�@      t�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption!   RECIBO DE DESISTÊNCIA DE TÍTULOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRRichTextQRRichText1LeftTopbWidth�Height'Size.Values      `�@UUUUUUU�@UUUUUU��@UUUUUU��	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAutoStretchColorclWindowFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style HiresExportTransparent
YIncrement2ParentRichEditRE  TQRLabelqrProtocoloLeft1Top� Width1HeightSize.Values�������@UUUUUU��@��������@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption
Protocolo:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel6Left(Top� Width:HeightSize.Values�������@��������@������R�@UUUUUUu�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption
Documento:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel7LeftETop� WidthHeightSize.Values�������@      ��@UUUUUU��@UUUUUUu�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption   Título:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel11Left5TopWidth-HeightSize.Values�������@������:�@      ��@       �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionSacador:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel10Left5Top� Width-HeightSize.Values�������@������:�@      (�@       �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionDevedor:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel33Left6Top(Width,HeightSize.Values�������@      ��@��������@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionCedente:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbProtocoloLeftiTop� Width1HeightSize.Values�������@      �@��������@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption
Protocolo:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	lbDevedorLeftiTop� Width*HeightSize.Values�������@      �@      (�@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionDevedorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbDocumentoLeftiTop� Width7HeightSize.Values�������@      �@������R�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption	DocumentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbTituloLeftiTop� WidthHeightSize.Values�������@      �@UUUUUU��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   TítuloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	lbSacadorLeftiTopWidth*HeightSize.Values�������@      �@      ��@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionSacadorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	lbCedenteLeftiTop(Width)HeightSize.Values�������@      �@��������@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionCedenteColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel5LeftTop� WidthIHeightSize.Values�������@������J�@      Ж@UUUUUU%�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption   Valor do Título:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbValorLeftiTop� WidthHeightSize.Values�������@      �@      Ж@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption0.00ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel12Left%Top� Width=HeightSize.Values�������@��������@�������@UUUUUUe�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionVencimento:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbVencimentoLeftiTop� WidthEHeightSize.Values�������@      �@�������@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption
DD/MM/YYYYColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel19Left5TopWidth-HeightSize.Values�������@������:�@UUUUUUM�@       �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption	Portador:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbApresentanteLeftiTopWidth*HeightSize.Values�������@      �@UUUUUUM�@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionPortadorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbReciboLeftiTop� WidthHeightSize.Values�������@      �@UUUUUU-�@������
�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption00000ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrReciboLeft+Top� Width7HeightSize.Values�������@��������@UUUUUU-�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption   Nº Recibo:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ1LeftTop� WidthhHeightSize.Values�������@UUUUUUS�	@      �@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption   Poder Judiciário - TJERJColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ2Left�Top� Width}HeightSize.Values�������@      ��	@������N�@UUUUUU]�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption   Corregedoria Geral da JustiçaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ3Left�Top� Width� HeightSize.Values�������@�������	@UUUUUU��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption"   Selo de Fiscalização EletrônicoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbSeloEletronicoLeftTop� WidthVHeightSize.Values�������@UUUUUUM�	@������.�@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaptionLLLL 00000 XXXColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ4Left�Top� Width� HeightSize.Values�������@�������	@      Ț@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaptionConsulte a validade do selo em:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ5Left�Top� Width� HeightSize.Values�������@UUUUUU_�	@�������@UUUUUU5�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption$https://www3.tjrj.jus.br/sitepublicoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel14Left?Top9Width#HeightSize.Values�������@      ��@UUUUUU	�@UUUUUU5�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionMotivo:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbMotivoLeftiTop9Width9HeightSize.Values�������@      �@UUUUUU	�@UUUUUU/�	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionMotivoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   TQRChildBand
ChildBand1Left9Top�Width�Height� Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values������R�@      :�	@ PreCaluculateBandHeightKeepOnOnePage
ParentBandQRBand1
PrintOrdercboAfterParent TQRLabellbDataLeftfTopWidth�HeightSize.Values ������@      ��@ XUUUUU�@ ������	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaptionCidade, DataColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRShapeQRShape2Left� Top@WidthRHeightSize.Values XUUUUU� @ �������@ XUUUUU�@ �������@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabellbAssinaturaLeftDTopDWidth!HeightSize.Values�������@      P�@�������@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption	lbTitularColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbMatriculaLeftBTopOWidth&HeightSize.Values�������@TUUUUU��@UUUUUU�@UUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption	MatriculaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel3LeftBToplWidth$HeightSize.Values������
�@      ��@      ��@UUUUUU=�	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	AutoSizeAutoStretch	Caption�   Este Recibo é gerado de forma eletrônica, qualquer forma de rasura ou complementação a tornará como inválida, ou como princípio de fraude.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRMemoMLeftTop� Width�HeightSize.Values �����*�@ XUUUUU�@ XUUUU��@ �������	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretch	ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentFullJustifyMaxBreakChars FontSize  TQRLabelQRLabel1Left8Top� WidthhHeightSize.Values XUUUU��@ �����ڻ	@     `�@ XUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionTaxas e Emolumentos:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   TQRBandQRBand2Left9TopQWidth�Height&Frame.DrawTop	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values      x�@      :�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageFooter TQRLabelQRLabel4LeftTopWidth� HeightSize.Values��������@��������@UUUUUUU�@������F�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   DESISTÊNCIA DE TÍTULOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbPortadorLeftTop Width-HeightSize.Values�������@��������@UUUUUUU�@       �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption	Portador:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel8LeftTop1Width�HeightSize.Values      ��@��������@UUUUUU��@TUUUUU��	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretch	Caption�   Declaro que me foi entregue o DOCUMENTO DE DÍVIDA abaixo discriminado, e o respectivo RECIBO DE DESISTÊNCIA, conforme requerido.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelQRLabel9LeftTop`Width1HeightSize.Values�������@������*�@       �@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption
Protocolo:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbProtocolo2LeftTTop`Width1HeightSize.Values�������@     @�@       �@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption
Protocolo:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel13Left TopqWidth-HeightSize.Values�������@UUUUUUU�@UUUUUU}�@       �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionDevedor:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbDevedor2LeftTTopqWidth*HeightSize.Values�������@     @�@UUUUUU}�@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionDevedorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel15LeftTop� Width:HeightSize.Values�������@UUUUUU�@��������@UUUUUUu�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption
Documento:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel16Left!Top� Width,HeightSize.Values�������@      ��@      x�@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption   Nº Título:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel17Left Top� Width-HeightSize.Values�������@UUUUUUU�@������r�@       �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionSacador:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel18Left!Top� Width,HeightSize.Values�������@      ��@      ��@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionCedente:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbDocumento2LeftTTop� Width7HeightSize.Values�������@     @�@��������@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption	DocumentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	lbTitulo2LeftTTop� WidthHeightSize.Values�������@     @�@      x�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption   TítuloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbSacador2LeftTTop� Width*HeightSize.Values�������@     @�@������r�@      @�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionSacadorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
lbCedente2LeftTTop� Width)HeightSize.Values�������@     @�@      ��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionCedenteColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbData2Left1Top� WidthHHeightSize.Values�������@��������@�������@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaptionCidade, DataColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRShapeQRShape1Left� TopWidthRHeightSize.ValuesUUUUUUU� @��������@������޹@��������@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabel	lbRecibo2LeftTTopOWidthHeightSize.Values�������@     @�@UUUUUU�@������
�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaption00000ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	qrRecibo2LeftTopOWidth7HeightSize.Values�������@VUUUUU��@UUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption   Nº Recibo:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel20LeftTop� WidthIHeightSize.Values�������@UUUUUUU�@UUUUUU��@UUUUUU%�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption   Valor do Título:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbValor2LeftTTop� WidthHeightSize.Values�������@     @�@UUUUUU��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionValorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel21Left4Top� WidthHeightSize.Values�������@UUUUUU��@������6�@������J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionSelo:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbSeloLeftTTop� WidthHeightSize.Values�������@     @�@������6�@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionSeloColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize    	TRichEditRELeftTopWidth�HeightFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTabOrderZoomd  TFDQueryqryProtocolo
Connectiondm.conSISTEMASQL.Stringsselect     ID_ATO,
PROTOCOLO,TIPO_DEVEDOR,DEVEDOR,CPF_CNPJ_DEVEDOR,NUMERO_TITULO,SACADOR,CEDENTE,VALOR_TITULO,DT_VENCIMENTO,APRESENTANTE,RECIBO_PAGAMENTO,SELO_PAGAMENTO,ALEATORIO_SOLUCAO,EMOLUMENTOS,FETJ,	FUNDPERJ,FUNPERJ,	FUNARPEN,PMCMV,ISS,MUTUA,ACOTERJ,TOTAL,IRREGULARIDADE    from titulos     where PROTOCOLO=:PROTOCOLO and     STATUS='RETIRADO'    ORDER BY DT_RETIRADO Left Top8	ParamDataName	PROTOCOLODataType	ftInteger	ParamTypeptInput   TIntegerFieldqryProtocoloID_ATO	FieldNameID_ATOOriginID_ATOProviderFlags
pfInUpdate	pfInWherepfInKey Required	  TIntegerFieldqryProtocoloPROTOCOLO	FieldName	PROTOCOLOOrigin	PROTOCOLO  TStringFieldqryProtocoloTIPO_DEVEDOR	FieldNameTIPO_DEVEDOROriginTIPO_DEVEDOR	FixedChar	Size  TStringFieldqryProtocoloDEVEDOR	FieldNameDEVEDOROriginDEVEDORSized  TStringFieldqryProtocoloCPF_CNPJ_DEVEDOR	FieldNameCPF_CNPJ_DEVEDOROriginCPF_CNPJ_DEVEDORSize  TStringFieldqryProtocoloNUMERO_TITULO	FieldNameNUMERO_TITULOOriginNUMERO_TITULO  TStringFieldqryProtocoloSACADOR	FieldNameSACADOROriginSACADORSized  TStringFieldqryProtocoloCEDENTE	FieldNameCEDENTEOriginCEDENTESized  TFloatFieldqryProtocoloVALOR_TITULO	FieldNameVALOR_TITULOOriginVALOR_TITULO  
TDateFieldqryProtocoloDT_VENCIMENTO	FieldNameDT_VENCIMENTOOriginDT_VENCIMENTO  TStringFieldqryProtocoloAPRESENTANTE	FieldNameAPRESENTANTEOriginAPRESENTANTESized  TIntegerFieldqryProtocoloRECIBO_PAGAMENTO	FieldNameRECIBO_PAGAMENTOOriginRECIBO_PAGAMENTO  TStringFieldqryProtocoloSELO_PAGAMENTO	FieldNameSELO_PAGAMENTOOriginSELO_PAGAMENTOSize	  TStringFieldqryProtocoloALEATORIO_SOLUCAO	FieldNameALEATORIO_SOLUCAOOriginALEATORIO_SOLUCAOSize  TFloatFieldqryProtocoloEMOLUMENTOS	FieldNameEMOLUMENTOSOriginEMOLUMENTOS  TFloatFieldqryProtocoloFETJ	FieldNameFETJOriginFETJ  TFloatFieldqryProtocoloFUNDPERJ	FieldNameFUNDPERJOriginFUNDPERJ  TFloatFieldqryProtocoloFUNPERJ	FieldNameFUNPERJOriginFUNPERJ  TFloatFieldqryProtocoloFUNARPEN	FieldNameFUNARPENOriginFUNARPEN  TFloatFieldqryProtocoloPMCMV	FieldNamePMCMVOriginPMCMV  TFloatFieldqryProtocoloISS	FieldNameISSOriginISS  TFloatFieldqryProtocoloMUTUA	FieldNameMUTUAOriginMUTUA  TFloatFieldqryProtocoloACOTERJ	FieldNameACOTERJOriginACOTERJ  TFloatFieldqryProtocoloTOTAL	FieldNameTOTALOriginTOTAL  TIntegerFieldqryProtocoloIRREGULARIDADE	FieldNameIRREGULARIDADEOriginIRREGULARIDADE    