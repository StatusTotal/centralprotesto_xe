unit UDetalhes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, ExtCtrls, sPanel, DB, DBCtrls,
  sDBMemo, Mask, sMaskEdit, sCustomComboEdit, sCurrEdit, sDBCalcEdit,
  sDBEdit;

type
  TFDetalhes = class(TForm)
    sPanel1: TsPanel;
    btOk: TsBitBtn;
    btCancelar: TsBitBtn;
    MDescricao: TsDBMemo;
    dsCodigos: TDataSource;
    edTitulo: TsDBEdit;
    sDBCalcEdit1: TsDBCalcEdit;
    sDBCalcEdit2: TsDBCalcEdit;
    procedure btCancelarClick(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDetalhes: TFDetalhes;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFDetalhes.btCancelarClick(Sender: TObject);
begin
  dm.Codigos.Cancel;
  Close;
end;

procedure TFDetalhes.btOkClick(Sender: TObject);
begin
  dm.Codigos.ApplyUpdates(0);
  Close;
end;

procedure TFDetalhes.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then btCancelarClick(Sender);  
end;

end.
