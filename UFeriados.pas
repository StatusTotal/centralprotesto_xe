{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UFeriados.pas
  Descricao:   Tela de cadastro de Feriados
  Author   :
  Date:        24-ago-2016
  Last Update: 02-jan-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UFeriados;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, Buttons, sBitBtn, DB, Grids,
  Wwdbigrd, Wwdbgrid, DBCtrls, sDBEdit, Mask, sMaskEdit, sCustomComboEdit,
  sTooledit, sDBDateEdit, sCheckBox, sDBCheckBox, sComboBox, sDBComboBox,
  System.DateUtils;

type
  TFFeriados = class(TForm)
    P1: TsPanel;
    btIncluir: TsBitBtn;
    btAlterar: TsBitBtn;
    btExcluir: TsBitBtn;
    btSalvar: TsBitBtn;
    btCancelar: TsBitBtn;
    dsFeriados: TDataSource;
    P2: TsPanel;
    Grid: TwwDBGrid;
    edDescricao: TsDBEdit;
    cbMes: TsComboBox;
    cbDia: TsDBComboBox;
    cbAno: TsDBComboBox;
    cbFixo: TsDBComboBox;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dsFeriadosStateChange(Sender: TObject);
    procedure dsFeriadosDataChange(Sender: TObject; Field: TField);
    procedure cbMesChange(Sender: TObject);
    procedure cbFixoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFeriados: TFFeriados;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFFeriados.FormCreate(Sender: TObject);
begin
  dm.Feriados.Close;
  dm.Feriados.Params.ParamByName('ANO').Value := YearOf(Date);
  dm.Feriados.Open;
end;

procedure TFFeriados.btIncluirClick(Sender: TObject);
begin
  dm.Feriados.Append;
  dm.Feriados.FieldByName('ATIVO').AsString    := 'S';
  dm.Feriados.FieldByName('FLG_FIXO').AsString := 'S';
  dm.Feriados.FieldByName('ANO').AsInteger     := YearOf(Date);

  cbAno.SetFocus;
end;

procedure TFFeriados.btAlterarClick(Sender: TObject);
begin
  if dm.Feriados.IsEmpty then
    Exit;

  dm.Feriados.Edit;

  cbMes.ItemIndex := dm.Feriados.FieldByName('MES').AsInteger - 1;

  cbAno.SetFocus;
end;

procedure TFFeriados.btExcluirClick(Sender: TObject);
begin
  if dm.Feriados.IsEmpty then
    Exit;

  if GR.Pergunta('Confirma a exclus�o') then
  begin
    dm.Feriados.Delete;
    dm.Feriados.ApplyUpdates(0);
  end;
end;

procedure TFFeriados.btSalvarClick(Sender: TObject);
begin
  try
    if dm.conSISTEMA.Connected then
      dm.conSISTEMA.StartTransaction;

    if dm.Feriados.State = dsInsert then
      dm.Feriados.FieldByName('ID_FERIADO').AsInteger := dm.IdAtual('ID_FERIADO','S');

    dm.Feriados.Post;
    dm.Feriados.ApplyUpdates(0);

    if dm.conSISTEMA.InTransaction then
      dm.conSISTEMA.Commit;
  except
    on E:Exception do
    begin
      if dm.conSISTEMA.InTransaction then
        dm.conSISTEMA.Rollback;
    end;
  end;

  btIncluir.SetFocus;
end;

procedure TFFeriados.btCancelarClick(Sender: TObject);
begin
  dm.Feriados.Cancel;
  dm.Feriados.Close;
end;

procedure TFFeriados.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_ESCAPE then
    Self.Close;

  if key = VK_RETURN then
    Perform(WM_NEXTDLGCTL, 0, 0);
end;

procedure TFFeriados.dsFeriadosStateChange(Sender: TObject);
begin
  if dm.Feriados.State = dsBrowse then
  begin
    P2.Enabled         := False;
    btIncluir.Enabled  := True;
    btAlterar.Enabled  := True;
    btExcluir.Enabled  := True;
    btSalvar.Enabled   := False;
    btCancelar.Enabled := False;
  end;

  if dm.Feriados.State = dsEdit then
  begin
    P2.Enabled         := True;
    btIncluir.Enabled  := False;
    btAlterar.Enabled  := False;
    btExcluir.Enabled  := False;
    btSalvar.Enabled   := True;
    btCancelar.Enabled := True;
  end;

  if dm.Feriados.State = dsInsert then
  begin
    P2.Enabled         := True;
    btIncluir.Enabled  := False;
    btAlterar.Enabled  := False;
    btExcluir.Enabled  := False;
    btSalvar.Enabled   := True;
    btCancelar.Enabled := True;
  end;
end;

procedure TFFeriados.dsFeriadosDataChange(Sender: TObject; Field: TField);
begin
  cbMes.ItemIndex := dm.Feriados.FieldByName('MES').AsInteger - 1;
end;

procedure TFFeriados.cbFixoExit(Sender: TObject);
begin
  btSalvar.SetFocus;
end;

procedure TFFeriados.cbMesChange(Sender: TObject);
begin
  if dm.Feriados.State in [dsEdit,dsInsert] then
    dm.Feriados.FieldByName('MES').AsInteger := cbMes.ItemIndex + 1;
end;

end.
