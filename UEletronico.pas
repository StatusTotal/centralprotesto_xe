unit UEletronico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, sMemo, DBCtrls, sDBLookupComboBox,
  DB, Mask, sMaskEdit, sCustomComboEdit, sTooledit, Buttons, sBitBtn,
  sGroupBox, FMTBcd, Grids, Wwdbigrd, Wwdbgrid, DBClient, Provider, SqlExpr,
  sDBEdit, sEdit, DateUtils, Menus, ComCtrls, sStatusBar, sDialogs, sLabel;

type
  TFEletronico = class(TForm)
    P1: TsPanel;
    M: TsMemo;
    dsPortadores: TDataSource;
    btVisualizar: TsBitBtn;
    btExportar: TsBitBtn;
    dtsTitulos: TSQLDataSet;
    dspTitulos: TDataSetProvider;
    Titulos: TClientDataSet;
    TitulosID_ATO: TIntegerField;
    TitulosCODIGO: TIntegerField;
    TitulosRECIBO: TIntegerField;
    TitulosDT_PROTOCOLO: TDateField;
    TitulosPROTOCOLO: TIntegerField;
    TitulosLIVRO_PROTOCOLO: TIntegerField;
    TitulosFOLHA_PROTOCOLO: TStringField;
    TitulosDT_PRAZO: TDateField;
    TitulosDT_REGISTRO: TDateField;
    TitulosREGISTRO: TIntegerField;
    TitulosLIVRO_REGISTRO: TIntegerField;
    TitulosFOLHA_REGISTRO: TStringField;
    TitulosSELO_REGISTRO: TStringField;
    TitulosDT_PAGAMENTO: TDateField;
    TitulosSELO_PAGAMENTO: TStringField;
    TitulosRECIBO_PAGAMENTO: TIntegerField;
    TitulosCOBRANCA: TStringField;
    TitulosEMOLUMENTOS: TFloatField;
    TitulosFETJ: TFloatField;
    TitulosFUNDPERJ: TFloatField;
    TitulosFUNPERJ: TFloatField;
    TitulosMUTUA: TFloatField;
    TitulosDISTRIBUICAO: TFloatField;
    TitulosACOTERJ: TFloatField;
    TitulosTOTAL: TFloatField;
    TitulosTIPO_PROTESTO: TIntegerField;
    TitulosTIPO_TITULO: TIntegerField;
    TitulosNUMERO_TITULO: TStringField;
    TitulosDT_TITULO: TDateField;
    TitulosBANCO: TStringField;
    TitulosVALOR_TITULO: TFloatField;
    TitulosSALDO_PROTESTO: TFloatField;
    TitulosDT_VENCIMENTO: TDateField;
    TitulosCONVENIO: TStringField;
    TitulosTIPO_APRESENTACAO: TStringField;
    TitulosDT_ENVIO: TDateField;
    TitulosTIPO_INTIMACAO: TStringField;
    TitulosMOTIVO_INTIMACAO: TMemoField;
    TitulosDT_INTIMACAO: TDateField;
    TitulosDT_PUBLICACAO: TDateField;
    TitulosVALOR_PAGAMENTO: TFloatField;
    TitulosAPRESENTANTE: TStringField;
    TitulosCPF_CNPJ_APRESENTANTE: TStringField;
    TitulosTIPO_APRESENTANTE: TStringField;
    TitulosCEDENTE: TStringField;
    TitulosNOSSO_NUMERO: TStringField;
    TitulosSACADOR: TStringField;
    TitulosDEVEDOR: TStringField;
    TitulosCPF_CNPJ_DEVEDOR: TStringField;
    TitulosTIPO_DEVEDOR: TStringField;
    TitulosAGENCIA_CEDENTE: TStringField;
    TitulosPRACA_PROTESTO: TStringField;
    TitulosTIPO_ENDOSSO: TStringField;
    TitulosACEITE: TStringField;
    TitulosCPF_ESCREVENTE: TStringField;
    TitulosCPF_ESCREVENTE_PG: TStringField;
    TitulosOBSERVACAO: TMemoField;
    TitulosDT_SUSTADO: TDateField;
    TitulosDT_RETIRADO: TDateField;
    TitulosSTATUS: TStringField;
    TitulosPROTESTADO: TStringField;
    TitulosENVIADO_PROTESTO: TStringField;
    TitulosENVIADO_PAGAMENTO: TStringField;
    TitulosEXPORTADO: TStringField;
    TitulosDocDevedor: TStringField;
    TitulosDocApresentante: TStringField;
    dsTitulos: TDataSource;
    GridAtos: TwwDBGrid;
    TitulosCheck: TStringField;
    dsParametros: TDataSource;
    TitulosDT_ENTRADA: TDateField;
    TitulosAGENCIA: TStringField;
    TitulosCONTA: TStringField;
    TitulosAVALISTA_DEVEDOR: TStringField;
    TitulosFINS_FALIMENTARES: TStringField;
    TitulosTARIFA_BANCARIA: TFloatField;
    TitulosFORMA_PAGAMENTO: TStringField;
    TitulosNUMERO_PAGAMENTO: TStringField;
    TitulosARQUIVO: TStringField;
    TitulosRETORNO: TStringField;
    PM: TPopupMenu;
    MarcarTodos1: TMenuItem;
    DesmarcarTodos1: TMenuItem;
    Barra: TsStatusBar;
    dsRemessas: TDataSource;
    N1: TMenuItem;
    Parmetros1: TMenuItem;
    opd: TsOpenDialog;
    TitulosDT_DEVOLVIDO: TDateField;
    TitulosCPF_CNPJ_SACADOR: TStringField;
    TitulosID_MSG: TIntegerField;
    TitulosVALOR_AR: TFloatField;
    TitulosELETRONICO: TStringField;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    TitulosCODIGO_APRESENTANTE: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edRemessaKeyPress(Sender: TObject; var Key: Char);
    procedure TitulosCalcFields(DataSet: TDataSet);
    procedure GridAtosColEnter(Sender: TObject);
    procedure MarcarTodos1Click(Sender: TObject);
    procedure DesmarcarTodos1Click(Sender: TObject);
    procedure Parmetros1Click(Sender: TObject);
    procedure btVisualizarClick(Sender: TObject);
    procedure Custas(Codigo: Integer; Eletronico: Boolean);
    function Texto(Linha, Inicio, Fim: Integer): String;
    function Vazio: Boolean;
    procedure btExportarClick(Sender: TObject);
    procedure TitulosAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    vTipo: String;
  end;

var
  FEletronico: TFEletronico;
  CalcFields: Boolean;
  Posicao: Integer;
  Periodo: Integer;

implementation

uses UDM, UPF, UParametros, UGeral;

{$R *.dfm}

procedure TFEletronico.Custas(Codigo: Integer; Eletronico: Boolean);
{var
  Soma: Double;}
begin
  {PEGAR AS CUSTAS DA TABELA GERAL}
  {N�O TEM CUSTAS DIFERENCIADAS PARA ELETR�NICO, AT� ENT�O}
  (*dm.RxCustas.Close;
  dm.RxCustas.Open;
  Soma:=0;

  while not dm.Eletronico.Eof do
  begin
      dm.RxCustas.Append;
      dm.RxCustasTABELA.AsString    :=dm.EletronicoTABELA.AsString;
      dm.RxCustasITEM.AsString      :=dm.EletronicoITEM.AsString;
      dm.RxCustasSUBITEM.AsString   :=dm.EletronicoSUBITEM.AsString;
      dm.RxCustasVALOR.AsFloat      :=dm.EletronicoVALOR.AsFloat;
      dm.RxCustasQTD.AsInteger      :=dm.EletronicoQTD.AsInteger;
      dm.RxCustasTOTAL.AsFloat      :=dm.EletronicoTOTAL.AsFloat;
      dm.RxCustasDESCRICAO.AsString :=dm.EletronicoDESCRICAO.AsString;
      dm.RxCustas.Post;
      dm.Eletronico.Next;
  end;
  {VALOR FIXO DE CERTID�O = N�O}
  if dm.ValorParametro(29)='N' then
  begin
      dm.Tabela.Close;
      dm.Tabela.Open;
      if Periodo>5 then
        dm.Tabela.Locate('TAB;ITEM;SUB',VarArrayOf(['1','4','B']),[loCaseInsensitive, loPartialKey])
          else dm.Tabela.Locate('TAB;ITEM;SUB',VarArrayOf(['1','4','A']),[loCaseInsensitive, loPartialKey]);
      dm.RxCustas.Append;
      dm.RxCustasTABELA.AsString    :=dm.TabelaTAB.AsString;
      dm.RxCustasITEM.AsString      :=dm.TabelaITEM.AsString;
      dm.RxCustasSUBITEM.AsString   :=dm.TabelaSUB.AsString;
      dm.RxCustasVALOR.AsFloat      :=dm.TabelaVALOR.AsFloat;
      dm.RxCustasQTD.AsInteger      :=Periodo Div 5;
      dm.RxCustasTOTAL.AsFloat      :=dm.TabelaVALOR.AsFloat;
      dm.RxCustasDESCRICAO.AsString :=dm.TabelaDESCR.AsString;
      dm.RxCustas.Post;
  end;
  dm.RxCustas.First;
  while not dm.RxCustas.Eof do
  begin
      Soma:=Soma+dm.RxCustasTOTAL.AsFloat;
      dm.RxCustas.Next;
  end;
  dm.Fundos.Close;
  dm.Fundos.Params[0].AsString:=dm.EletronicoTIPO.AsString;
  dm.Fundos.Open;
  dm.vEmolumentos   :=Soma;
  dm.vFetj          :=GR.NoRound(Soma*0.2,2);
  dm.vFundperj      :=GR.NoRound(Soma*0.05,2);
  dm.vFunperj       :=GR.NoRound(Soma*0.05,2);
  dm.vFunarpen      :=GR.NoRound(Soma*0.04,2);
  dm.vMutua         :=dm.FundosMUTUA.AsFloat;
  dm.vAcoterj       :=dm.FundosACOTERJ.AsFloat;
  dm.vDistribuicao  :=dm.FundosDISTRIBUICAO.AsFloat;
  dm.vTotal         :=dm.vEmolumentos+dm.vFetj+dm.vFundperj+dm.vFunperj+dm.vFunarpen+
                      dm.vMutua+dm.vAcoterj+dm.vDistribuicao;

  dm.RxCustas.First;*)
end;

function TFEletronico.Texto(Linha, Inicio, Fim: Integer): String;
begin
  Result:=Trim(Copy(M.Lines[Linha],Inicio,Fim));
end;

procedure TFEletronico.FormCreate(Sender: TObject);
begin
  dm.Portadores.Close;
  dm.qryPortadores.SQL.Text:='select * from PORTADORES order by NOME';
  dm.Portadores.Open;
  dm.Portadores.Filtered:=False;

  dm.Parametros.Close;
  dm.Parametros.Params[0].AsInteger:=10;
  dm.Parametros.Open;
  dm.Parametros.Close;
end;

procedure TFEletronico.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm.Portadores.Close;
end;

procedure TFEletronico.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFEletronico.edRemessaKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFEletronico.TitulosCalcFields(DataSet: TDataSet);
begin
  if CalcFields then
    if TitulosEXPORTADO.AsString='N' then TitulosCheck.AsString:='S';
end;

procedure TFEletronico.GridAtosColEnter(Sender: TObject);
begin
  CalcFields:=False;
end;

procedure TFEletronico.MarcarTodos1Click(Sender: TObject);
begin
  Posicao:=Titulos.RecNo;
  Titulos.DisableControls;
  Titulos.First;
  while not Titulos.Eof do
  begin
      Titulos.Edit;
      TitulosCheck.AsString:='S';
      Titulos.Next;
  end;
  Titulos.RecNo:=Posicao;
  Titulos.EnableControls;
end;

procedure TFEletronico.DesmarcarTodos1Click(Sender: TObject);
begin
  Posicao:=Titulos.RecNo;
  Titulos.DisableControls;
  Titulos.First;
  while not Titulos.Eof do
  begin
      Titulos.Edit;
      TitulosCheck.AsString:='N';
      Titulos.Next;
  end;
  Titulos.RecNo:=Posicao;
  Titulos.EnableControls;
end;

function TFEletronico.Vazio: Boolean;
var
  Posicao: Integer;
begin
  Result :=True;
  Posicao:=Titulos.RecNo;
  Titulos.DisableControls;
  Titulos.First;
  while not Titulos.Eof do
  begin
      if TitulosCheck.AsString='S' then
      begin
          Result:=False;
          Break;
      end;
      Titulos.Next;
  end;
  Titulos.RecNo:=Posicao;
  Titulos.EnableControls;
end;

procedure TFEletronico.Parmetros1Click(Sender: TObject);
begin
  GR.CriarForm(TFParametros,FParametros);
end;

procedure TFEletronico.btVisualizarClick(Sender: TObject);
var
  I: Integer;
  Protocolos: String;
begin
  if opd.Execute then
  begin
      M.Lines.LoadFromFile(opd.FileName);
      if Copy(ExtractFileName(opd.FileName),1,2)='CP' then
        vTipo:='C'
          else vTipo:='D';
      Protocolos:='';
      for I := 0 to M.Lines.Count -1 do
      begin
          if Texto(I,1,1)='2' then
          Protocolos:=Protocolos+IntToStr(StrToInt(Texto(I,2,10)))+',';
      end;

      GridAtos.Columns[6].DisplayLabel:='';

      Protocolos[Length(Protocolos)]:=' ';
      Protocolos:=Trim(Protocolos);
      Titulos.Close;
      Titulos.CommandText:='SELECT * FROM TITULOS WHERE PROTOCOLO IN ('+Protocolos+') ORDER BY PROTOCOLO';
      Titulos.Open;
      Posicao:=Titulos.RecNo;
      Titulos.DisableControls;
      Titulos.First;
      while not Titulos.Eof do
      begin
          Titulos.Edit;
          if vTipo='C' then
          TitulosVALOR_PAGAMENTO.AsFloat:=TitulosSALDO_PROTESTO.AsFloat;

          Titulos.Post;
          Titulos.Next;
      end;
      Titulos.RecNo:=Posicao;
      Titulos.EnableControls;
  end;
end;

procedure TFEletronico.btExportarClick(Sender: TObject);
var
  Ano1,Ano2: Integer;
begin
  Posicao:=Titulos.RecNo;
  Titulos.DisableControls;
  Titulos.First;
  while not Titulos.Eof do
  begin
      Application.ProcessMessages;
      if TitulosCheck.AsString='S' then
      begin
          if TitulosDT_PAGAMENTO.AsDateTime=0 then
          begin
              if vTipo='C' then
                GR.Aviso('Protocolo: '+TitulosPROTOCOLO.AsString+' * DATA DO CANCELAMENTO N�O INFORMADA *')
                  else GR.Aviso('Protocolo: '+TitulosPROTOCOLO.AsString+' * DATA DA RETIRADA N�O INFORMADA *');
              Titulos.RecNo:=Posicao;
              Titulos.EnableControls;
              Exit;
          end;
          if vTipo='C' then
          begin
              if TitulosSELO_PAGAMENTO.AsString='' then
              begin
                  GR.Aviso('Protocolo: '+TitulosPROTOCOLO.AsString+' * SELO N�O INFORMADO *');
                  Titulos.RecNo:=Posicao;
                  Titulos.EnableControls;
                  Exit;
              end;

              if TitulosVALOR_PAGAMENTO.AsFloat=0 then
              begin
                  GR.Aviso('Protocolo: '+TitulosPROTOCOLO.AsString+' * VALOR PAGO N�O INFORMADO *');
                  Titulos.RecNo:=Posicao;
                  Titulos.EnableControls;
                  Exit;
              end;

              if TitulosRECIBO_PAGAMENTO.AsString='' then
              begin
                  GR.Aviso('Protocolo: '+TitulosPROTOCOLO.AsString+' * RECIBO N�O INFORMADO *');
                  Titulos.RecNo:=Posicao;
                  Titulos.EnableControls;
                  Exit;
              end;
          end;
      end;
      Titulos.Next;
  end;

  Ano1    :=YearOf(TitulosDT_REGISTRO.AsDateTime);
  Ano2    :=YearOf(Now);
  Periodo :=Ano2-Ano1;
  if Periodo<5 then Periodo:=5;
  Custas(4010,True);
  Titulos.ApplyUpdates(0);
  Titulos.First;
  while not Titulos.Eof do
  begin
      if TitulosCheck.AsString='S' then
      begin
          Titulos.Edit;
          if vTipo='C' then
            TitulosSTATUS.AsString:='CANCELADO'
              else
                TitulosSTATUS.AsString:='RETIRADO';
          TitulosELETRONICO.AsString:='S';
          TitulosCPF_ESCREVENTE_PG.AsString:=dm.vCPF;
          Titulos.Post;
          Titulos.ApplyUpdates(0);
          if vTipo='C' then
          begin
              dm.Certidoes.Close;
              dm.Certidoes.Params[0].AsInteger:=-1;
              dm.Certidoes.Open;
              dm.Certidoes.Append;
              dm.CertidoesID_CERTIDAO.AsInteger       :=dm.IdAtual('ID_ATO','S');
              dm.CertidoesCONVENIO.AsString           :='N';
              dm.CertidoesCODIGO.AsInteger            :=4010;
              dm.CertidoesSELO.AsString               :=TitulosSELO_PAGAMENTO.AsString;
              dm.CertidoesRECIBO.AsInteger            :=TitulosRECIBO_PAGAMENTO.AsInteger;
              dm.CertidoesREQUERIDO.AsString          :=TitulosDEVEDOR.AsString;
              dm.CertidoesTIPO_REQUERIDO.AsString     :=TitulosTIPO_DEVEDOR.AsString;
              dm.CertidoesCPF_CNPJ_REQUERIDO.AsString :=TitulosCPF_CNPJ_DEVEDOR.AsString;
              dm.CertidoesREQUERENTE.AsString         :=TitulosDEVEDOR.AsString;
              dm.CertidoesCPF_CNPJ_REQUERENTE.AsString:=TitulosCPF_CNPJ_DEVEDOR.AsString;
              dm.CertidoesDT_PEDIDO.AsDateTime        :=TitulosDT_PAGAMENTO.AsDateTime;
              dm.CertidoesANOS.AsInteger              :=Periodo;
              dm.CertidoesDT_ENTREGA.AsDateTime       :=TitulosDT_PAGAMENTO.AsDateTime;
              dm.CertidoesTIPO_CERTIDAO.AsString      :='X';
              dm.CertidoesFOLHAS.AsString             :='1';
              dm.CertidoesRESULTADO.AsString          :='P';
              dm.CertidoesREGISTROS.AsInteger         :=1;
              dm.CertidoesCOBRANCA.AsString           :='CC';
              dm.CertidoesEMOLUMENTOS.AsFloat         :=dm.vEmolumentos;
              dm.CertidoesFETJ.AsFloat                :=dm.vFetj;
              dm.CertidoesFUNDPERJ.AsFloat            :=dm.vFundperj;
              dm.CertidoesFUNPERJ.AsFloat             :=dm.vFunperj;
              dm.CertidoesMUTUA.AsFloat               :=dm.vMutua;
              dm.CertidoesACOTERJ.AsFloat             :=dm.vAcoterj;
              dm.CertidoesDISTRIBUICAO.AsFloat        :=dm.vDistribuicao;
              dm.CertidoesTOTAL.AsFloat               :=dm.vTotal;
              dm.CertidoesESCREVENTE.AsString         :=TitulosCPF_ESCREVENTE_PG.AsString;
              dm.CertidoesID_ATO.AsInteger            :=TitulosID_ATO.AsInteger;
              dm.CertidoesENVIADO.AsString            :='N';
              dm.Certidoes.Post;
              dm.Certidoes.ApplyUpdates(0);
              {CUSTAS}
              dm.RxCustas.First;
              dm.Custas.Close;
              dm.Custas.Params[0].AsInteger:=-1;
              dm.Custas.Open;
              while not dm.RxCustas.Eof do
              begin
                  dm.Custas.Append;
                  dm.CustasID_CUSTA.AsInteger :=dm.IdAtual('ID_CUSTA','S');
                  dm.CustasID_ATO.AsInteger   :=dm.CertidoesID_CERTIDAO.AsInteger;
                  dm.CustasTABELA.AsString    :=dm.RxCustasTABELA.AsString;
                  dm.CustasITEM.AsString      :=dm.RxCustasITEM.AsString;
                  dm.CustasSUBITEM.AsString   :=dm.RxCustasSUBITEM.AsString;
                  dm.CustasVALOR.AsFloat      :=dm.RxCustasVALOR.AsFloat;
                  dm.CustasQTD.AsInteger      :=dm.RxCustasQTD.AsInteger;
                  dm.CustasTOTAL.AsFloat      :=dm.RxCustasTOTAL.AsFloat;
                  dm.Custas.Post;
                  dm.Custas.ApplyUpdates(0);
                  dm.RxCustas.Next;
              end;
          end;
      end;
      Titulos.Next;
  end;
  Titulos.RecNo:=Posicao;
  Titulos.EnableControls;
  Titulos.Close;
end;

procedure TFEletronico.TitulosAfterPost(DataSet: TDataSet);
begin
  Titulos.ApplyUpdates(0);
end;

end.
