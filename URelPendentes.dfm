�
 TFRELPENDENTES 0�  TPF0TFRelPendentesFRelPendentesLeftFTop� BorderIconsbiSystemMenu BorderStylebsDialogCaption   Relatório de TítulosClientHeight� ClientWidth�Color	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
KeyPreview	OldCreateOrderPositionpoMainFormCenterOnCreate
FormCreate	OnKeyDownFormKeyDownPixelsPerInch`
TextHeight TsBitBtnbtVisualizarLeft+Top� Width[HeightCursorcrHandPointCaption
VisualizarFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrder OnClickbtVisualizarClick
ImageIndexImages
dm.ImagensSkinData.SkinSectionBUTTON  TsBitBtnbtSairLeft�Top� WidthKHeightCursorcrHandPointCaptionSairFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderOnClickbtSairClick
ImageIndexImages
dm.ImagensSkinData.SkinSectionBUTTON  TsPanelP1Left Top Width�Height� AlignalTopTabOrderSkinData.SkinSectionPANEL 
TsGroupBox
sGroupBox1Left
Top6Width�HeightICaption   PeríodoFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style 
ParentFontTabOrderSkinData.SkinSectionGROUPBOX 
TsDateEditedInicioLeftYTopWidthoHeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontTabOrder BoundLabel.Active	BoundLabel.ParentFontBoundLabel.Caption   InícioBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style SkinData.SkinSectionEDITGlyphMode.Blend GlyphMode.GrayedDefaultToday	  
TsDateEditedFimLeft)TopWidthpHeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontTabOrderBoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionFimBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style SkinData.SkinSectionEDITGlyphMode.Blend GlyphMode.GrayedDefaultToday	   TsDBLookupComboBox
lkPortadorLeft
TopWidth�HeightCursorcrHandPointColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTahoma
Font.Style KeyFieldID_PORTADOR	ListFieldNOME
ListSource
dsPortador
ParentFontTabOrder BoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionPortadorBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style BoundLabel.Layout
sclTopLeftSkinData.SkinSectionCOMBOBOX  
TsCheckBoxckTodosLeftTop� Width� HeightCaption Todos PortadoresChecked	Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold 
ParentFontState	cbCheckedTabOrderSkinData.SkinSectionCHECKBOX
ImgChecked ImgUnchecked    TDataSource
dsPortadorDataSetdm.PortadoresLeftPTop�    