�
 TFQUICKCERTIDAOCADASTRO1 0�;  TPF0TFQuickCertidaoCadastro1FQuickCertidaoCadastro1Left�TopVVertScrollBar.PositionCaption	   CertidãoClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderScaledOnCreate
FormCreatePixelsPerInch`
TextHeight 	TQuickRep	Certidao1Left;ToprWidthHeightcFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	ShowingPreviewDataSetRXFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' 	OnEndPageCertidao1EndPageOptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeCustomPage.ContinuousPage.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCode	PrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsNormalPreviewInitialStatewsMaximizedPreviewWidth�PreviewHeight�PrevInitialZoomqrZoomToWidthPreviewDefaultSaveTypestQRPPreviewLeft 
PreviewTop  TQRBand	CabecalhoLeft9Top9Width�Height� AlignToBottomBeforePrintCabecalhoBeforePrintTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUUa�@     :�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbTitle 	TQRDBTexttxNomeServentiaLeft�TopWidthNHeightSize.Values�������@UUUUUU�@������*�@      `�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataSetdm.Serventia	DataField	DESCRICAOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize
  	TQRDBText
txEnderecoLeft�TopWidth7HeightSize.Values�������@UUUUUU	�@������j�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataSetdm.Serventia	DataFieldENDERECOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRLabel
lbCertidaoLeft�TopmWidthUHeight!Size.Values      ��@UUUUUU�@������2�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   C E R T I D Ã OColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTimes New Roman
Font.StylefsUnderline 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabel	lbTitularLeft<Top� Width1HeightSize.Values�������@UUUUUU-�	@      p�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionTITULARColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbTexto1Left<Top� Width1Height!Size.Values      ��@      ��@      8�@      ��	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionX   [Título] do [Cartório], Estado do Rio de Janeiro, por nomeação na forma da Lei, etc.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  	TQRDBText
QRDBText14Left�Top&Width4HeightSize.Values�������@��������@UUUUUU�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataSetdm.Serventia	DataFieldTELEFONEFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText15Left�Top6WidthHeightSize.Values�������@UUUUUU��@      ��@������
�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataSetdm.Serventia	DataFieldEMAILFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  	TQRDBText
QRDBText16Left�TopFWidthHeightSize.Values�������@������K�@UUUUUU5�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataSetdm.Serventia	DataFieldCNPJFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize  TQRRichTextQRELeft<Top� Width1HeightSize.Values �����J�@      ��@      ��@      ��	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAutoStretch	ColorclWindowFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style HiresExportTransparent
YIncrement2ParentRichEditRE  	TQRDBText
QRDBText17Left�TopVWidthHeightSize.Values�������@��������@��������@VUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	ColorclWhiteDataSetdm.Serventia	DataFieldSITEFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars VerticalAlignmenttlTopFontSize   TQRBandSummaryLeft9Top0Width�HeightAlignToBottomTransparentBandForceNewColumnForceNewPageSize.Values������ֽ@     :�	@ PreCaluculateBandHeightKeepOnOnePageBandType	rbSummary TQRLabellbEscreventeLeft��TopWidthrHeight0Size.Values       �@      ��@������j�@UUUUUU	�	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	AutoSizeCaption5Eu,______________________Escrevente, efetuei a busca.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRLabel	QRLabel17Left�TopXWidth� HeightSize.Values�������@      ��@VUUUUU��@      �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   O referido é verdade e dou féColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbCidadeLeft�TophWidth3HeightSize.Values�������@     \�@UUUUUU��@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	CaptionlbCidadeColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize
  TQRLabellbAssinaturaLeft3Top� WidthBHeightSize.Values XUUUUU�@UUUUUU�@UUUUUU%�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption   FuncionárioColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRShapeQRShape5Left� Top� Width,HeightSize.ValuesUUUUUUU� @UUUUUU��@      ��@      p�@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRMemoM1LeftTopWidth�HeightSize.Values      ��@UUUUUUU�@UUUUUUE�@      N�	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretch	ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentFullJustifyMaxBreakChars FontSize  TQRLabellbReciboLeftoTop� Width"HeightSize.Values�������@UUUUUU�	@      Ԕ@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionReciboColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	ExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
qrTabeliaoLeft<Top� Width1HeightEnabledSize.Values XUUUUU�@UUUUUU�@UUUUUU��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBand	Caption	   TabeliãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize	  TQRShapeqrAssinaturaLeft� Top� Width,HeightEnabledSize.ValuesUUUUUUU� @UUUUUU��@������J�@      p�@ XLColumn XLNumFormat	nfGeneralBrush.ColorclBlackShapeqrsRectangle
VertAdjust   TQRLabelQRLabel1Left-Top� WidtheHeightSize.ValuesUUUUUU��@UUUUUU7�	@�������@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaptionTabela de EmolumentosColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ1LeftTopbWidthhHeightSize.Values�������@UUUUUUE�	@UUUUUU��@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption   Poder Judiciário - TJERJColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ2LeftTopmWidth}HeightSize.Values�������@      ��	@������2�@UUUUUU]�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption   Corregedoria Geral da JustiçaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ3LeftTopxWidth� HeightSize.Values�������@��������	@      ��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption"   Selo de Fiscalização EletrônicoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbSeloEletronicoLeft!Top� WidthVHeightSize.Values�������@UUUUUU?�	@�������@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaptionLLLL 00000 XXXColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ4LeftTop� Width� HeightSize.Values�������@��������	@UUUUUU%�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaptionConsulte a validade do selo em:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabellbCGJ5LeftTop� Width� HeightSize.Values�������@UUUUUUQ�	@��������@UUUUUU5�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandCaption$https://www3.tjrj.jus.br/sitepublicoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   TQRBandQRBand1Left9TopOWidth�HeightLFrame.DrawTop	AlignToBottomTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUU�@     :�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageFooter 
TQRSysData	sysPaginaLeftFTop7WidthJHeightSize.Values      ��@      |�	@UUUUUU��@��������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandColorclWhiteDataqrsPageNumberText   Página TransparentExportAsexptTextFontSize  TQRLabellbAvisoLeftTopWidth�HeightSize.Values������
�@UUUUUUU�@������*�@��������	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeAutoStretch	Caption�   Esta Certidão é gerada de forma eletrônica, qualquer forma de rasura ou complementação a tornará como inválida, ou como princípio de fraude. (Art. 559 - Resolução 01/2000- CGJERJ) - Válida somente se aposta de selo de fiscalização.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize    	TRichEditRELeftTop��Width�HeightFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTabOrderZoomd  TRxMemoryDataRX	FieldDefs LeftTop� TStringFieldRXTitulo	FieldNameTitulo  TStringFieldRXApresentante	FieldNameApresentanteSized  TStringField	RXDevedor	FieldNameDevedorSized  TFloatFieldRXValorDisplayLabel	#####0.00	FieldNameValorDisplayFormat	#####0.00  
TDateFieldRXVencimento	FieldName
VencimentoDisplayFormat
DD/MM/YYYY  TStringFieldRXProtocolo	FieldName	Protocolo  
TDateFieldRXDt_Titulo	FieldName	Dt_TituloDisplayFormat
DD/MM/YYYY  TStringField	RXEspecie	FieldNameEspecieSized  TStringField	RXSacador	FieldNameSacadorSized  TStringField	RXCedente	FieldNameCedenteSized  
TDateFieldRXDt_Protesto	FieldNameDt_ProtestoDisplayFormat
DD/MM/YYYY  TStringField	RXEndosso	FieldNameEndossoSize  TStringFieldRXLivro	FieldNameLivro  TStringFieldRXFolha	FieldNameFolha    