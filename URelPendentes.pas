unit URelPendentes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, sDBLookupComboBox, StdCtrls, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, sGroupBox, ExtCtrls, sPanel, Buttons,
  sBitBtn, DB, DBClient, SimpleDS, sCheckBox;

type
  TFRelPendentes = class(TForm)
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    P1: TsPanel;
    sGroupBox1: TsGroupBox;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    lkPortador: TsDBLookupComboBox;
    dsPortador: TDataSource;
    ckTodos: TsCheckBox;
    procedure btSairClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btVisualizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelPendentes: TFRelPendentes;

implementation

uses UDM, UPF, UQuickPendentes;

{$R *.dfm}

procedure TFRelPendentes.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFRelPendentes.FormCreate(Sender: TObject);
begin
  dm.Portadores.Close;
  dm.Portadores.Open;
end;

procedure TFRelPendentes.btVisualizarClick(Sender: TObject);
begin
  if (not ckTodos.Checked) and (lkPortador.Text='') then Exit;
  PF.Aguarde(True);
  Application.CreateForm(TFQuickPendentes,FQuickPendentes);
  with FQuickPendentes do
  begin
      if ckTodos.Checked then
      begin
          TodosPortadores.Close;
          TodosPortadores.Params[0].AsDate:=edInicio.Date;
          TodosPortadores.Params[1].AsDate:=edFim.Date;
          TodosPortadores.Open;

          lbTabeliao.Caption   :=dm.ServentiaTABELIAO.AsString;
          lbCartorio2.Caption  :=dm.ServentiaDESCRICAO.AsString;
          lbEndereco2.Caption  :=dm.ServentiaENDERECO.AsString+' '+dm.ServentiaBAIRRO.AsString+' '+dm.ServentiaCIDADE.AsString+' - RJ ';
          lbTitulo2.Caption    :='Relat�rio de T�tulos - Per�odo: '+edInicio.Text+' at� '+edFim.Text;
          Todos.Preview;
      end
      else
      begin
          Titulos.Close;
          if dm.PortadoresCODIGO.AsInteger<>0 then
          begin
              Titulos.SQL.Text:='SELECT PROTOCOLO, DT_PROTOCOLO, TOTAL, NUMERO_TITULO, VALOR_TITULO, NOSSO_NUMERO, SACADOR, DEVEDOR,'+
                                           'STATUS FROM TITULOS WHERE CODIGO_APRESENTANTE=:CODIGO AND DT_PROTOCOLO BETWEEN :D1 AND :D2 '+
                                           'AND EXPORTADO='''+'N'+'''ORDER BY 2,1';
              Titulos.Params.ParamByName('CODIGO').AsInteger:=dm.PortadoresCODIGO.AsInteger;
          end
          else
          begin
              Titulos.SQL.Text:='SELECT PROTOCOLO, DT_PROTOCOLO, TOTAL, NUMERO_TITULO, VALOR_TITULO, NOSSO_NUMERO, SACADOR, DEVEDOR,'+
                                           'STATUS from TITULOS WHERE CPF_CNPJ_APRESENTANTE=:CPF AND DT_PROTOCOLO BETWEEN :D1 AND :D2 '+
                                           'AND EXPORTADO='''+'N'+'''ORDER BY 2,1';

              Titulos.Params.ParamByName('CPF').AsString:=dm.PortadoresDOCUMENTO.AsString;
          end;

          Titulos.Params.ParamByName('D1').AsDate:=edInicio.Date;
          Titulos.Params.ParamByName('D2').AsDate:=edFim.Date;

          Titulos.Open;

          Titulos.Filtered:=False;

          lbCartorio.Caption  :=dm.ServentiaDESCRICAO.AsString;
          lbEndereco.Caption  :=dm.ServentiaENDERECO.AsString+' '+dm.ServentiaBAIRRO.AsString+' '+dm.ServentiaCIDADE.AsString+' - RJ ';
          lbTitulo.Caption    :='Relat�rio de T�tulos - Per�odo: '+edInicio.Text+' at� '+edFim.Text;
          lbPortador.Caption  :=dm.PortadoresCODIGO.AsString+'  '+dm.PortadoresNOME.AsString;
          QuickRep1.Preview;
      end;
      Free;
  end;
end;

procedure TFRelPendentes.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

end.
