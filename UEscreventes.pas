unit UEscreventes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, sDBMemo, sDBComboBox, Mask, sDBEdit,
  sCheckBox, sDBCheckBox, Grids, Wwdbigrd, Wwdbgrid, Buttons, sBitBtn,
  ExtCtrls, sPanel, DB, sGroupBox, sDBRadioGroup, acPNG;

type
  TFEscreventes = class(TForm)
    P1: TsPanel;
    btIncluir: TsBitBtn;
    btAlterar: TsBitBtn;
    btExcluir: TsBitBtn;
    btSalvar: TsBitBtn;
    btCancelar: TsBitBtn;
    GridPorta: TwwDBGrid;
    P2: TsPanel;
    edLogin: TsDBEdit;
    edNome: TsDBEdit;
    edDocumento: TsDBEdit;
    edQualificacao: TsDBEdit;
    edSenha: TsDBEdit;
    dsEscreventes: TDataSource;
    RgTransmite: TsDBRadioGroup;
    ImAviso1: TImage;
    ImOk1: TImage;
    edMatricula: TsDBEdit;
    procedure dsEscreventesStateChange(Sender: TObject);
    procedure dsEscreventesDataChange(Sender: TObject; Field: TField);
    procedure btIncluirClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edDocumentoChange(Sender: TObject);
    procedure edDocumentoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEscreventes: TFEscreventes;

implementation

uses UDM, UPF, UGeral;

{$R *.dfm}

procedure TFEscreventes.dsEscreventesStateChange(Sender: TObject);
begin
  if dm.Escreventes.State=dsInsert then
  begin
      btIncluir.Enabled :=False;
      btAlterar.Enabled :=False;
      btExcluir.Enabled :=False;
      btSalvar.Enabled  :=True;
      btCancelar.Enabled:=True;
      GridPorta.Enabled :=False;
      P2.Enabled        :=True;
      edNome.SetFocus;
  end;

  if dm.Escreventes.State=dsEdit then
  begin
      btIncluir.Enabled :=False;
      btAlterar.Enabled :=False;
      btExcluir.Enabled :=False;
      GridPorta.Enabled :=False;
      btSalvar.Enabled  :=True;
      btCancelar.Enabled:=True;
      P2.Enabled        :=True;
      edNome.SetFocus;
  end;

  if dm.Escreventes.State=dsBrowse then
  begin
      btIncluir.Enabled :=True;
      btAlterar.Enabled :=True;
      btExcluir.Enabled :=True;
      GridPorta.Enabled :=True;
      btSalvar.Enabled  :=False;
      btCancelar.Enabled:=False;
      P2.Enabled        :=False;
  end;
end;

procedure TFEscreventes.dsEscreventesDataChange(Sender: TObject;
  Field: TField);
begin
  btAlterar.Enabled:=not dm.Escreventes.IsEmpty;
  btExcluir.Enabled:=not dm.Escreventes.IsEmpty;
end;

procedure TFEscreventes.btIncluirClick(Sender: TObject);
begin
  dm.Escreventes.Append;
  dm.EscreventesAUTORIZADO.AsString:='N';
end;

procedure TFEscreventes.btAlterarClick(Sender: TObject);
begin
  if dm.Escreventes.IsEmpty then Exit;

  dm.Escreventes.Edit;
  dm.EscreventesSENHA.AsString:=PF.Cript(edSenha.Text,False);
end;

procedure TFEscreventes.btExcluirClick(Sender: TObject);
begin
  if dm.Escreventes.IsEmpty then Exit;

  if GR.Pergunta('Confirma a exclus�o?') then
  begin
      dm.Escreventes.Delete;
      dm.Escreventes.ApplyUpdates(0);
  end;
end;

procedure TFEscreventes.btSalvarClick(Sender: TObject);
begin
  if edNome.Text='TOTAL' then
  begin
      GR.Aviso('Nome inv�lido.');
      edNome.SetFocus;
      Exit;
  end;

  if edLogin.Text='TOTAL' then
  begin
      GR.Aviso('Login inv�lido.');
      edLogin.SetFocus;
      Exit;
  end;

  if edNome.Text='' then
  begin
      GR.Aviso('Preencha o Nome.');
      edNome.SetFocus;
      Exit;
  end;

  if GR.PegarNumeroTexto(edDocumento.Text)='' then
  begin
      GR.Aviso('Preencha o Documento.');
      edDocumento.SetFocus;
      Exit;
  end;

  if ImAviso1.Visible then
  begin
      GR.Aviso('Documento Inv�lido.');
      edDocumento.SetFocus;
      Exit;
  end;

  if edQualificacao.Text='' then
  begin
      GR.Aviso('Preencha a Qualifica��o.');
      edQualificacao.SetFocus;
      Exit;
  end;

  if edLogin.Text='' then
  begin
      GR.Aviso('Preencha o Login.');
      edLogin.SetFocus;
      Exit;
  end;

  if edSenha.Text='' then
  begin
      GR.Aviso('Preencha a Senha.');
      edSenha.SetFocus;
      Exit;
  end;

  dm.EscreventesSENHA.AsString:=PF.Cript(edSenha.Text,True);
  dm.Escreventes.Post;
  dm.Escreventes.ApplyUpdates(0);
end;

procedure TFEscreventes.btCancelarClick(Sender: TObject);
begin
  dm.Escreventes.Cancel;
end;

procedure TFEscreventes.FormCreate(Sender: TObject);
begin
  dm.Escreventes.Close;
  dm.Escreventes.Open;
end;

procedure TFEscreventes.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFEscreventes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm.Escreventes.Close;
end;

procedure TFEscreventes.edDocumentoChange(Sender: TObject);
begin
  if GR.PegarNumeroTexto(edDocumento.Text)='' then
  begin
      ImAviso1.Visible:=True;
      ImOk1.Visible   :=False;
      Exit;
  end;

  ImAviso1.Visible:= not PF.VerificarCPF(GR.PegarNumeroTexto(edDocumento.Text));

  ImOk1.Visible:=not ImAviso1.Visible;
end;

procedure TFEscreventes.edDocumentoClick(Sender: TObject);
begin
  edDocumento.SelectAll;
end;

end.
