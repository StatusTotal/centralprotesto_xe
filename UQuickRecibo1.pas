unit UQuickRecibo1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, QuickRpt, QRCtrls, ExtCtrls;

type
  TFQuickRecibo1 = class(TForm)
    Recibo1: TQuickRep;
    Cabecalho: TQRBand;
    txNomeServentia: TQRDBText;
    txEndereco: TQRDBText;
    qrTitular1: TQRLabel;
    QRLabel1: TQRLabel;
    lbRecibo1: TQRLabel;
    Detalhe: TQRBand;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    qrDoc1: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    qrDoc2: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText6: TQRDBText;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    SubDetailCustas: TQRSubDetail;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    lbTabela: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel9: TQRLabel;
    QRBand1: TQRBand;
    lbCidade1: TQRLabel;
    ChildBand1: TQRChildBand;
    lbProtocolo1: TQRLabel;
    QRLabel32: TQRLabel;
    QRDBText31: TQRDBText;
    QRLabel22: TQRLabel;
    QRImage1: TQRImage;
    qrCobranca1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel7: TQRLabel;
    QRDBText16: TQRDBText;
    QRLabel20: TQRLabel;
    QRDBText33: TQRDBText;
    QRLabel30: TQRLabel;
    lbRecibo2: TQRLabel;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    qrTitular2: TQRLabel;
    lbProtocolo2: TQRLabel;
    QRDBText32: TQRDBText;
    QRShape9: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRDBText27: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRLabel16: TQRLabel;
    qrDoc3: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    qrDoc4: TQRLabel;
    QRLabel21: TQRLabel;
    qrCobranca2: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel15: TQRLabel;
    QRDBText28: TQRDBText;
    QRDBText34: TQRDBText;
    QRLabel23: TQRLabel;
    lbCidade2: TQRLabel;
    QRLabel31: TQRLabel;
    M: TQRMemo;
    QRLabel29: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRDBText35: TQRDBText;
    QRDBText36: TQRDBText;
    QRDBText37: TQRDBText;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRDBText38: TQRDBText;
    QRDBText39: TQRDBText;
    QRDBText40: TQRDBText;
    procedure FormCreate(Sender: TObject);
    procedure SubDetailCustasBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure Recibo1EndPage(Sender: TCustomQuickRep);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickRecibo1: TFQuickRecibo1;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFQuickRecibo1.FormCreate(Sender: TObject);
begin
  if dm.ServentiaCODIGO.AsInteger=1208 then
    lbRecibo1.Caption:='PEDIDO N� '+dm.TitulosRECIBO.AsString
      else lbRecibo1.Caption:='RECIBO N� '+dm.TitulosRECIBO.AsString;
  lbCidade1.Caption     :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',Now);
  lbCidade2.Caption     :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy',Now)+', __________________________ (Apresentante).';

  if dm.ValorParametro(16)='S' then
  begin
    lbProtocolo1.Enabled:= True;
    lbProtocolo2.Enabled:= True;
  end
  else
  begin
    lbProtocolo1.Enabled:= False;
    lbProtocolo2.Enabled:= False;
  end;

  lbProtocolo1.Caption  :='PROTOCOLO N� '+dm.TitulosPROTOCOLO.AsString;
  lbRecibo2.Caption     :=lbRecibo1.Caption;
  lbProtocolo2.Caption  :=lbProtocolo1.Caption;

  if dm.TitulosTIPO_APRESENTANTE.AsString='J' then qrDoc1.Caption:='CNPJ do Apresentante';
  if dm.TitulosTIPO_DEVEDOR.AsString='J'      then qrDoc2.Caption:='CNPJ do Devedor';

  qrDoc3.Caption:=qrDoc1.Caption;
  qrDoc4.Caption:=qrDoc2.Caption;

  qrTitular1.Caption:=dm.ValorParametro(7)+': '+dm.ServentiaTABELIAO.AsString;
  qrTitular2.Caption:=qrTitular1.Caption;

  if dm.TitulosCOBRANCA.AsString='CC' then qrCobranca1.Caption:='Com Cobran�a';
  if dm.TitulosCOBRANCA.AsString='JG' then qrCobranca1.Caption:='Justi�a Gratuita';
  if dm.TitulosCOBRANCA.AsString='SC' then qrCobranca1.Caption:='Sem Cobran�a';
  if dm.TitulosCOBRANCA.AsString='NH' then qrCobranca1.Caption:='NIHIL';

  qrCobranca2.Caption:=qrCobranca1.Caption;

  if (dm.TitulosCOBRANCA.AsString='JG') or (dm.TitulosCOBRANCA.AsString='SC') then SubDetailCustas.Enabled:=False;

  Recibo1.Preview;
end;

procedure TFQuickRecibo1.SubDetailCustasBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  lbTabela.Caption:=dm.RxCustasTABELA.AsString+' '+PF.Espaco(dm.RxCustasITEM.AsString,'D',2)+' '+dm.RxCustasSUBITEM.AsString;
end;

procedure TFQuickRecibo1.Recibo1EndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickRecibo1.QRBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  M.Lines.Clear;
  dm.RxCustas.First;
  while not dm.RxCustas.Eof do
  begin
      M.Lines.Add(dm.RxCustasTABELA.AsString+'.'+dm.RxCustasITEM.AsString+'.'+dm.RxCustasSUBITEM.AsString+'   '+dm.RxCustasDESCRICAO.AsString+' x '+dm.RxCustasQTD.AsString+Format('%-1s %7.2f',[' =',dm.RxCustasTOTAL.AsFloat]));
      dm.RxCustas.Next;
  end;
end;

end.
