unit UIrregularidades;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, Wwdbigrd, Wwdbgrid, DB;

type
  TFIrregularidades = class(TForm)
    dsIrregularidades: TDataSource;
    Grid: TwwDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
    procedure GridKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FIrregularidades: TFIrregularidades;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFIrregularidades.FormCreate(Sender: TObject);
begin
  dm.Irregularidades.Close;
  dm.Irregularidades.Open;
  dm.Irregularidades.Filtered:=False;
  dm.Irregularidades.Filter:='CODIGO>0';
  dm.Irregularidades.Filtered:=True;
end;

procedure TFIrregularidades.GridDblClick(Sender: TObject);
begin
  dm.vOkGeral             :=True;
  dm.vIrregularidade      :=dm.IrregularidadesCODIGO.AsString;
  dm.vMotivo              :=dm.IrregularidadesMOTIVO.AsString;
  Close;
end;

procedure TFIrregularidades.GridKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
  begin
      dm.vOkGeral         :=True;
      dm.vIrregularidade  :=dm.IrregularidadesCODIGO.AsString;
      dm.vMotivo          :=dm.IrregularidadesMOTIVO.AsString;
      Close;
  end;
end;

procedure TFIrregularidades.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFIrregularidades.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm.Irregularidades.Filtered:=False;
end;

end.
