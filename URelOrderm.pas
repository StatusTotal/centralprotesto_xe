{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   URelOrderm.pas
  Descricao:   Filtro para impressao de Ordem de Protesto
  Author:      -
  Date:        -
  Last Update: 28-ago-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit URelOrderm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, ExtCtrls, sPanel, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, sGroupBox, sEdit, FMTBcd, DB, SqlExpr,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  sCheckBox;

type
  TFRelOrdem = class(TForm)
    P1: TsPanel;
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    gbPeriodoProtocolo: TsGroupBox;
    Protocolos: TFDQuery;
    ProtocolosID_ATO: TIntegerField;
    ProtocolosCODIGO: TIntegerField;
    ProtocolosRECIBO: TIntegerField;
    ProtocolosDT_ENTRADA: TDateField;
    ProtocolosDT_PROTOCOLO: TDateField;
    ProtocolosPROTOCOLO: TIntegerField;
    ProtocolosLIVRO_PROTOCOLO: TIntegerField;
    ProtocolosFOLHA_PROTOCOLO: TStringField;
    ProtocolosDT_PRAZO: TDateField;
    ProtocolosDT_REGISTRO: TDateField;
    ProtocolosREGISTRO: TIntegerField;
    ProtocolosLIVRO_REGISTRO: TIntegerField;
    ProtocolosFOLHA_REGISTRO: TStringField;
    ProtocolosSELO_REGISTRO: TStringField;
    ProtocolosDT_PAGAMENTO: TDateField;
    ProtocolosSELO_PAGAMENTO: TStringField;
    ProtocolosRECIBO_PAGAMENTO: TIntegerField;
    ProtocolosCOBRANCA: TStringField;
    ProtocolosEMOLUMENTOS: TFloatField;
    ProtocolosFETJ: TFloatField;
    ProtocolosFUNDPERJ: TFloatField;
    ProtocolosFUNPERJ: TFloatField;
    ProtocolosFUNARPEN: TFloatField;
    ProtocolosPMCMV: TFloatField;
    ProtocolosISS: TFloatField;
    ProtocolosMUTUA: TFloatField;
    ProtocolosDISTRIBUICAO: TFloatField;
    ProtocolosACOTERJ: TFloatField;
    ProtocolosTOTAL: TFloatField;
    ProtocolosTIPO_PROTESTO: TIntegerField;
    ProtocolosTIPO_TITULO: TIntegerField;
    ProtocolosNUMERO_TITULO: TStringField;
    ProtocolosDT_TITULO: TDateField;
    ProtocolosBANCO: TStringField;
    ProtocolosAGENCIA: TStringField;
    ProtocolosCONTA: TStringField;
    ProtocolosVALOR_TITULO: TFloatField;
    ProtocolosSALDO_PROTESTO: TFloatField;
    ProtocolosDT_VENCIMENTO: TDateField;
    ProtocolosCONVENIO: TStringField;
    ProtocolosTIPO_APRESENTACAO: TStringField;
    ProtocolosDT_ENVIO: TDateField;
    ProtocolosCODIGO_APRESENTANTE: TStringField;
    ProtocolosTIPO_INTIMACAO: TStringField;
    ProtocolosMOTIVO_INTIMACAO: TMemoField;
    ProtocolosDT_INTIMACAO: TDateField;
    ProtocolosDT_PUBLICACAO: TDateField;
    ProtocolosVALOR_PAGAMENTO: TFloatField;
    ProtocolosAPRESENTANTE: TStringField;
    ProtocolosCPF_CNPJ_APRESENTANTE: TStringField;
    ProtocolosTIPO_APRESENTANTE: TStringField;
    ProtocolosCEDENTE: TStringField;
    ProtocolosNOSSO_NUMERO: TStringField;
    ProtocolosSACADOR: TStringField;
    ProtocolosDEVEDOR: TStringField;
    ProtocolosCPF_CNPJ_DEVEDOR: TStringField;
    ProtocolosTIPO_DEVEDOR: TStringField;
    ProtocolosAGENCIA_CEDENTE: TStringField;
    ProtocolosPRACA_PROTESTO: TStringField;
    ProtocolosTIPO_ENDOSSO: TStringField;
    ProtocolosACEITE: TStringField;
    ProtocolosCPF_ESCREVENTE: TStringField;
    ProtocolosCPF_ESCREVENTE_PG: TStringField;
    ProtocolosOBSERVACAO: TMemoField;
    ProtocolosDT_SUSTADO: TDateField;
    ProtocolosDT_RETIRADO: TDateField;
    ProtocolosSTATUS: TStringField;
    ProtocolosPROTESTADO: TStringField;
    ProtocolosENVIADO_APONTAMENTO: TStringField;
    ProtocolosENVIADO_PROTESTO: TStringField;
    ProtocolosENVIADO_PAGAMENTO: TStringField;
    ProtocolosENVIADO_RETIRADO: TStringField;
    ProtocolosENVIADO_SUSTADO: TStringField;
    ProtocolosENVIADO_DEVOLVIDO: TStringField;
    ProtocolosEXPORTADO: TStringField;
    ProtocolosAVALISTA_DEVEDOR: TStringField;
    ProtocolosFINS_FALIMENTARES: TStringField;
    ProtocolosTARIFA_BANCARIA: TFloatField;
    ProtocolosFORMA_PAGAMENTO: TStringField;
    ProtocolosNUMERO_PAGAMENTO: TStringField;
    ProtocolosARQUIVO: TStringField;
    ProtocolosRETORNO: TStringField;
    ProtocolosDT_DEVOLVIDO: TDateField;
    ProtocolosCPF_CNPJ_SACADOR: TStringField;
    ProtocolosID_MSG: TIntegerField;
    ProtocolosVALOR_AR: TFloatField;
    ProtocolosELETRONICO: TStringField;
    ProtocolosIRREGULARIDADE: TIntegerField;
    ProtocolosAVISTA: TStringField;
    ProtocolosSALDO_TITULO: TFloatField;
    ProtocolosTIPO_SUSTACAO: TStringField;
    ProtocolosRETORNO_PROTESTO: TStringField;
    ProtocolosDT_RETORNO_PROTESTO: TDateField;
    ProtocolosDT_DEFINITIVA: TDateField;
    ProtocolosJUDICIAL: TStringField;
    ProtocolosALEATORIO_PROTESTO: TStringField;
    ProtocolosALEATORIO_SOLUCAO: TStringField;
    ProtocolosCCT: TStringField;
    ProtocolosDETERMINACAO: TStringField;
    ProtocolosANTIGO: TStringField;
    ProtocolosLETRA: TStringField;
    ProtocolosPROTOCOLO_CARTORIO: TIntegerField;
    ProtocolosARQUIVO_CARTORIO: TStringField;
    ProtocolosRETORNO_CARTORIO: TStringField;
    ProtocolosDT_DEVOLVIDO_CARTORIO: TDateField;
    ProtocolosCARTORIO: TIntegerField;
    ProtocolosDT_PROTOCOLO_CARTORIO: TDateField;
    gbFaixaProtocolo: TsGroupBox;
    edtNumProtocoloInicial: TsEdit;
    edtNumProtocoloFinal: TsEdit;
    gbPortador: TsGroupBox;
    edtCodigoPortador: TsEdit;
    edtNomePortador: TsEdit;
    gbDevedor: TsGroupBox;
    edtNomeDevedor: TsEdit;
    dteDataInicial: TsDateEdit;
    dteDataFinal: TsDateEdit;
    ProtocolosDIAS_AVISTA: TIntegerField;
    chbTrocarPortadorCedente: TsCheckBox;
    ProtocolosFLG_SALDO: TStringField;
    ProtocolosNOME_APRESENTANTE: TStringField;
    ProtocolosPOSTECIPADO: TStringField;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSairClick(Sender: TObject);
    procedure btVisualizarClick(Sender: TObject);
    procedure dteDataInicialExit(Sender: TObject);
    procedure edtNumProtocoloInicialExit(Sender: TObject);
  private
    { Private declarations }

    function VerificarFiltro: Boolean;
  public
    { Public declarations }
  end;

var
  FRelOrdem: TFRelOrdem;

implementation

uses UDM, UPF, UQuickOrdem1, UGeral;

{$R *.dfm}

procedure TFRelOrdem.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Close;

  if Key = VK_RETURN then
    Perform(WM_NEXTDLGCTL, 0, 0);
end;

function TFRelOrdem.VerificarFiltro: Boolean;
begin
  Result := True;

  if (dteDataInicial.Date = 0) and
    (dteDataFinal.Date = 0) and
    (Trim(edtNumProtocoloInicial.Text) = '') and
    (Trim(edtNumProtocoloFinal.Text) = '') and
    (Trim(edtCodigoPortador.Text) = '') and
    (Trim(edtNomePortador.Text) = '') and
    (Trim(edtNomeDevedor.Text) = '') then
  begin
    Application.MessageBox('� preciso informar algum par�metro para a pesquisa.',
                           'Aviso',
                           MB_OK + MB_ICONWARNING);
    Result := False;
  end
  else
  begin
    if ((dteDataInicial.Date > 0) and
      (dteDataFinal.Date = 0)) or
      ((dteDataInicial.Date = 0) and
      (dteDataFinal.Date > 0)) then
    begin
      Application.MessageBox('Informe corretamente o Per�odo do Protocolo.',
                             'Aviso',
                             MB_OK + MB_ICONWARNING);
      Result := False;
    end
    else  if ((dteDataInicial.Date > 0) and
      (dteDataFinal.Date > 0)) then
    begin
      if (dteDataInicial.Date > dteDataFinal.Date) then
      begin
        Application.MessageBox('A Data Inicial n�o pode ser superior � Data Final.',
                               'Aviso',
                               MB_OK + MB_ICONWARNING);
        Result := False;
      end;
    end
    else if ((Trim(edtNumProtocoloInicial.Text) <> '') and
      (Trim(edtNumProtocoloFinal.Text) = '')) or
      ((Trim(edtNumProtocoloInicial.Text) = '') and
      (Trim(edtNumProtocoloFinal.Text) <> '')) then
    begin
      Application.MessageBox('Informe corretamente a Faixa do Protocolo.',
                             'Aviso',
                             MB_OK + MB_ICONWARNING);
      Result := False;
    end
    else if ((Trim(edtNumProtocoloInicial.Text) <> '') and
      (Trim(edtNumProtocoloFinal.Text) <> '')) then
    begin
      if(StrToInt(edtNumProtocoloInicial.Text) > StrToInt(edtNumProtocoloFinal.Text)) then
      begin
        Application.MessageBox('O N� do Protocolo Inicial n�o pode ser superior ao N� do Protocolo Final.',
                               'Aviso',
                               MB_OK + MB_ICONWARNING);
        Result := False;
      end;
    end;
  end;
end;

procedure TFRelOrdem.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFRelOrdem.btVisualizarClick(Sender: TObject);
begin
  if not VerificarFiltro then
    Exit;

  Protocolos.Close;
  Protocolos.SQL.Clear;

  Protocolos.SQL.Text := 'SELECT T.*, ' +
                         '       (CASE WHEN (T.CODIGO_APRESENTANTE = ' + QuotedStr('E19') + ') ' +
                         '             THEN T.CEDENTE ' +
                         '             ELSE T.APRESENTANTE ' +
                         '        END) AS NOME_APRESENTANTE ' +
                         '  FROM TITULOS T ' +
                         ' WHERE T.ID_ATO > 0 ';

  //Periodo do Protocolo
  if (dteDataInicial.Date > 0) and
    (dteDataFinal.Date > 0) then
  begin
    Protocolos.SQL.Add(' AND T.DT_PROTOCOLO BETWEEN :DT_INI AND :DT_FIM ');

    Protocolos.Params.ParamByName('DT_INI').Value := dteDataInicial.Date;
    Protocolos.Params.ParamByName('DT_FIM').Value := dteDataFinal.Date;
  end;

  //Faixa do Protocolo
  if (Trim(edtNumProtocoloInicial.Text) <> '') and
    (Trim(edtNumProtocoloFinal.Text) <> '') then
  begin
    Protocolos.SQL.Add(' AND T.PROTOCOLO BETWEEN :PROT_INI AND :PROT_FIM ');

    Protocolos.Params.ParamByName('PROT_INI').Value := StrToInt(edtNumProtocoloInicial.Text);
    Protocolos.Params.ParamByName('PROT_FIM').Value := StrToInt(edtNumProtocoloFinal.Text);
  end;

  //Portador - Codigo
  if (Trim(edtCodigoPortador.Text) <> '') then
  begin
    Protocolos.SQL.Add(' AND T.CODIGO_APRESENTANTE = :COD_PORT ');

    Protocolos.Params.ParamByName('COD_PORT').Value := Trim(edtCodigoPortador.Text);
  end;

  //Portador - Nome
  if (Trim(edtNomePortador.Text) <> '') then
  begin
    Protocolos.SQL.Add(' AND T.APRESENTANTE LIKE :NM_PORT ');

    Protocolos.Params.ParamByName('NM_PORT').Value := '%' + Trim(edtNomePortador.Text) + '%';
  end;

  //Devedor - Nome
  if (Trim(edtNomeDevedor.Text) <> '') then
  begin
    Protocolos.SQL.Add(' AND T.DEVEDOR LIKE :NM_DEV ');

    Protocolos.Params.ParamByName('NM_DEV').Value := '%' + Trim(edtNomeDevedor.Text) + '%';
  end;

  Protocolos.SQL.Add('ORDER BY T.PROTOCOLO');

  Protocolos.Open;

  if Protocolos.RecordCount = 0 then
  begin
    GR.Aviso('Nenhum Protocolo encontrado.');
    Exit;
  end;

  PF.Aguarde(True);

  Application.CreateForm(TFQuickOrdem1,FQuickOrdem1);

{  if Application.MessageBox('Incluir 2� via?',
                            'Impress�o',
                            MB_YESNO + MB_ICONQUESTION) = ID_YES then
    FQuickOrdem1.lSegundaVia := True
  else
    FQuickOrdem1.lSegundaVia := False;

  FQuickOrdem1.lTrocarPortadorCedente := chbTrocarPortadorCedente.Checked;  }

  FQuickOrdem1.lSegundaVia := False;

  with FQuickOrdem1 do
  begin
    RXOrdem.Close;
    RXOrdem.Open;

    iTotOrdems := 0;

    Protocolos.First;

    while not Protocolos.Eof do
    begin
      RXOrdem.Append;
      RXOrdemDevedor.AsString           := ProtocolosDEVEDOR.AsString;
      RXOrdemTipoDevedor.AsString       := ProtocolosTIPO_DEVEDOR.AsString;

      if ProtocolosTIPO_DEVEDOR.AsString = 'F' then
        RXOrdemCPF_CNPJ_Devedor.AsString := PF.FormatarCPF(ProtocolosCPF_CNPJ_DEVEDOR.AsString)
      else
        RXOrdemCPF_CNPJ_Devedor.AsString := PF.FormatarCNPJ(ProtocolosCPF_CNPJ_DEVEDOR.AsString);

      RXOrdemPraca.AsString             :=ProtocolosPRACA_PROTESTO.AsString;

      PF.CarregarDevedor(ProtocolosID_ATO.AsInteger);

      RXOrdemEnderecoDevedor.AsString   := dm.RxDevedorENDERECO.AsString + ' ' +
                                           dm.RxDevedorBAIRRO.AsString + ' ' +
                                           dm.RxDevedorMUNICIPIO.AsString + '-' +
                                           dm.RxDevedorUF.AsString;

      RXOrdemSacador.AsString           := ProtocolosSACADOR.AsString;
      RXOrdemCedente.AsString           := ProtocolosCEDENTE.AsString;
      RXOrdemCodApresentante.AsString   := ProtocolosCODIGO_APRESENTANTE.AsString;
      RXOrdemApresentante.AsString      := ProtocolosAPRESENTANTE.AsString;
      RXOrdemNomeApresentante.AsString  := ProtocolosNOME_APRESENTANTE.AsString;
      RXOrdemEmissao.AsDateTime         := ProtocolosDT_TITULO.AsDateTime;

      if not ProtocolosDIAS_AVISTA.IsNull then
        RXOrdemVencimento.AsString := '� VISTA'
      else
        RXOrdemVencimento.AsDateTime := ProtocolosDT_VENCIMENTO.AsDateTime;

{      if ProtocolosSALDO_TITULO.AsFloat <> 0 then
        RXOrdemValor.AsFloat := ProtocolosSALDO_TITULO.AsFloat
      else
        RXOrdemValor.AsFloat := ProtocolosVALOR_TITULO.AsFloat;  }

      RXOrdemValor.AsFloat              := ProtocolosVALOR_TITULO.AsFloat;
      RXOrdemSaldo.AsFloat              := ProtocolosSALDO_TITULO.AsFloat;
      RXOrdemAgencia.AsString           := ProtocolosAGENCIA.AsString;
      RXOrdemEspecie.AsString           := PF.RetornarSigla(ProtocolosTIPO_TITULO.AsInteger);
      RXOrdemNumeroTitulo.AsString      := ProtocolosNUMERO_TITULO.AsString;
      RXOrdemAgencia.AsString           := ProtocolosAGENCIA_CEDENTE.AsString;
      RXOrdemLetra.AsString             := ProtocolosLETRA.AsString;

      if ProtocolosTIPO_ENDOSSO.AsString = 'M' then
        RXOrdemEndosso.AsString := 'Mandato';

      if ProtocolosTIPO_ENDOSSO.AsString = 'T' then
        RXOrdemEndosso.AsString := 'Translativo';

      if ProtocolosTIPO_ENDOSSO.AsString = 'S' then
        RXOrdemEndosso.AsString := 'Sem endosso';

      RXOrdemAceite.AsString            := ProtocolosACEITE.AsString;
      RXOrdemNossoNumero.AsString       := ProtocolosNOSSO_NUMERO.AsString;
      RXOrdemProtocolo.AsInteger        := ProtocolosProtocolo.AsInteger;
      RXOrdemDataProtocolo.AsDateTime   := ProtocolosDT_PROTOCOLO.AsDateTime;
      RXOrdemConvenio.AsBoolean         := (ProtocolosCONVENIO.AsString = 'S');
      RXOrdemCustas.AsFloat             := (ProtocolosTOTAL.AsFloat - ProtocolosTARIFA_BANCARIA.AsFloat);
      RXOrdemCartorio.AsString          := ProtocolosCARTORIO.AsString;

      if ProtocolosPOSTECIPADO.AsString.Trim='P' then
       RXOrdemPOSTECIPADO.AsString       := 'Sim'
         else
           RXOrdemPOSTECIPADO.AsString       := 'N�o';

      RXOrdem.Post;

      Inc(iTotOrdems);

      Protocolos.Next;
    end;

    Ordem.Preview;
    Free;
  end;
end;

procedure TFRelOrdem.dteDataInicialExit(Sender: TObject);
begin
  if (dteDataInicial.Date > 0) and
    (dteDataFinal.Date = 0) then
    dteDataFinal.Date := dteDataInicial.Date;
end;

procedure TFRelOrdem.edtNumProtocoloInicialExit(Sender: TObject);
begin
  if (Trim(edtNumProtocoloInicial.Text) <> '') and
    (Trim(edtNumProtocoloFinal.Text) = '') then
    edtNumProtocoloFinal.Text := edtNumProtocoloInicial.Text;
end;

end.
