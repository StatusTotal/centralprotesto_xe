unit UConsultaCompleta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, Grids, Wwdbigrd, Wwdbgrid, FMTBcd, DBClient,
  Provider, DB, SqlExpr, StdCtrls, Buttons, sBitBtn, acPNG, sEdit, IniFiles,
  sComboBox, Menus, sLabel, sDBText, DBCtrls, sRadioButton, sMemo, Mask,
  sMaskEdit, sCustomComboEdit, sTooledit, SimpleDS, sDBMemo, sDBNavigator,
  sSkinProvider, Data.DBXFirebird, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, System.StrUtils, sGroupBox;

type
  TFConsultaCompleta = class(TForm)
    P1: TsPanel;
    dspConsulta: TDataSetProvider;
    Consulta: TClientDataSet;
    dsConsulta: TDataSource;
    cbBuscar: TsComboBox;
    edPesquisar: TsEdit;
    PM: TPopupMenu;
    Detalhes: TMenuItem;
    Excluir: TMenuItem;
    P3: TsPanel;
    dsTitulos: TDataSource;
    lbEspecie: TLabel;
    lbTitulo: TLabel;
    lbPrazo: TLabel;
    lbEmissao: TLabel;
    lbVencimento: TLabel;
    lbValor: TLabel;
    lbCustas: TLabel;
    lbSacador: TLabel;
    lbCedente: TLabel;
    lbStatus: TLabel;
    lbData: TLabel;
    edData: TsDateEdit;
    dsObs: TDataSource;
    N1: TMenuItem;
    lbTotal: TLabel;
    lbTipo: TLabel;
    RecibodeBaixa1: TMenuItem;
    R2: TMenuItem;
    M2: TMenuItem;
    J1: TMenuItem;
    DBNavigator1: TDBNavigator;
    lbSaldo: TLabel;
    sSkinProvider: TsSkinProvider;
    MenuEtiqueta: TMenuItem;
    GridAtos: TwwDBGrid;
    lblDataSA: TLabel;
    Obs: TFDQuery;
    ObsID_ATO: TIntegerField;
    ObsOBSERVACAO: TMemoField;
    qryConsulta: TFDQuery;
    qryTitulo: TFDQuery;
    qryTituloSTATUS: TStringField;
    qryTituloDT_PROTOCOLO: TDateField;
    qryTituloDT_INTIMACAO: TDateField;
    qryTituloDT_REGISTRO: TDateField;
    qryTituloDT_PAGAMENTO: TDateField;
    qryTituloDT_RETIRADO: TDateField;
    qryTituloDT_SUSTADO: TDateField;
    qryTituloDT_PRAZO: TDateField;
    qryTituloDT_TITULO: TDateField;
    qryTituloDT_VENCIMENTO: TDateField;
    qryTituloDT_DEVOLVIDO: TDateField;
    qryTituloDT_PUBLICACAO: TDateField;
    qryTituloTIPO_INTIMACAO: TStringField;
    qryTituloMOTIVO_INTIMACAO: TMemoField;
    qryTituloPROTOCOLO: TIntegerField;
    qryTituloTIPO_TITULO: TIntegerField;
    qryTituloNUMERO_TITULO: TStringField;
    qryTituloVALOR_TITULO: TFloatField;
    qryTituloTOTAL: TFloatField;
    qryTituloSALDO_PROTESTO: TFloatField;
    qryTituloSACADOR: TStringField;
    qryTituloCEDENTE: TStringField;
    qryTituloOBSERVACAO: TMemoField;
    qryTituloTARIFA_BANCARIA: TFloatField;
    qryTituloVALOR_AR: TFloatField;
    qryTituloIRREGULARIDADE: TIntegerField;
    qryTituloLIVRO_REGISTRO: TIntegerField;
    qryTituloFOLHA_REGISTRO: TStringField;
    qryTituloTIPO_APRESENTACAO: TStringField;
    qryTituloDISTRIBUICAO: TFloatField;
    qryTituloSALDO_TITULO: TFloatField;
    qryTituloLIVRO_PROTOCOLO: TIntegerField;
    qryTituloFOLHA_PROTOCOLO: TStringField;
    qryTituloTIPO_DEVEDOR: TStringField;
    qryTituloCPF_CNPJ_DEVEDOR: TStringField;
    qryTituloPROTOCOLO_CARTORIO: TIntegerField;
    qryTituloARQUIVO: TStringField;
    qryTituloARQUIVO_CARTORIO: TStringField;
    qryTituloRETORNO_CARTORIO: TStringField;
    qryTituloRETORNO: TStringField;
    qryTituloCARTORIO: TIntegerField;
    qryTituloDT_PROTOCOLO_CARTORIO: TDateField;
    qryTituloDESCRICAO: TStringField;
    qryTituloSERVENTIA_AGREGADA: TStringField;
    ConsultaID_ATO: TIntegerField;
    ConsultaCODIGO: TIntegerField;
    ConsultaRECIBO: TIntegerField;
    ConsultaDT_ENTRADA: TDateField;
    ConsultaDT_PROTOCOLO: TDateField;
    ConsultaPROTOCOLO: TIntegerField;
    ConsultaLIVRO_PROTOCOLO: TIntegerField;
    ConsultaFOLHA_PROTOCOLO: TStringField;
    ConsultaDT_PRAZO: TDateField;
    ConsultaDT_REGISTRO: TDateField;
    ConsultaREGISTRO: TIntegerField;
    ConsultaLIVRO_REGISTRO: TIntegerField;
    ConsultaFOLHA_REGISTRO: TStringField;
    ConsultaSELO_REGISTRO: TStringField;
    ConsultaDT_PAGAMENTO: TDateField;
    ConsultaSELO_PAGAMENTO: TStringField;
    ConsultaRECIBO_PAGAMENTO: TIntegerField;
    ConsultaCOBRANCA: TStringField;
    ConsultaEMOLUMENTOS: TFloatField;
    ConsultaFETJ: TFloatField;
    ConsultaFUNDPERJ: TFloatField;
    ConsultaFUNPERJ: TFloatField;
    ConsultaFUNARPEN: TFloatField;
    ConsultaPMCMV: TFloatField;
    ConsultaISS: TFloatField;
    ConsultaMUTUA: TFloatField;
    ConsultaDISTRIBUICAO: TFloatField;
    ConsultaACOTERJ: TFloatField;
    ConsultaTOTAL: TFloatField;
    ConsultaTIPO_PROTESTO: TIntegerField;
    ConsultaTIPO_TITULO: TIntegerField;
    ConsultaNUMERO_TITULO: TStringField;
    ConsultaDT_TITULO: TDateField;
    ConsultaBANCO: TStringField;
    ConsultaAGENCIA: TStringField;
    ConsultaCONTA: TStringField;
    ConsultaVALOR_TITULO: TFloatField;
    ConsultaSALDO_PROTESTO: TFloatField;
    ConsultaDT_VENCIMENTO: TDateField;
    ConsultaCONVENIO: TStringField;
    ConsultaTIPO_APRESENTACAO: TStringField;
    ConsultaDT_ENVIO: TDateField;
    ConsultaCODIGO_APRESENTANTE: TStringField;
    ConsultaTIPO_INTIMACAO: TStringField;
    ConsultaMOTIVO_INTIMACAO: TMemoField;
    ConsultaDT_INTIMACAO: TDateField;
    ConsultaDT_PUBLICACAO: TDateField;
    ConsultaVALOR_PAGAMENTO: TFloatField;
    ConsultaAPRESENTANTE: TStringField;
    ConsultaCPF_CNPJ_APRESENTANTE: TStringField;
    ConsultaTIPO_APRESENTANTE: TStringField;
    ConsultaCEDENTE: TStringField;
    ConsultaNOSSO_NUMERO: TStringField;
    ConsultaSACADOR: TStringField;
    ConsultaDEVEDOR: TStringField;
    ConsultaCPF_CNPJ_DEVEDOR: TStringField;
    ConsultaTIPO_DEVEDOR: TStringField;
    ConsultaAGENCIA_CEDENTE: TStringField;
    ConsultaPRACA_PROTESTO: TStringField;
    ConsultaTIPO_ENDOSSO: TStringField;
    ConsultaACEITE: TStringField;
    ConsultaCPF_ESCREVENTE: TStringField;
    ConsultaCPF_ESCREVENTE_PG: TStringField;
    ConsultaOBSERVACAO: TMemoField;
    ConsultaDT_SUSTADO: TDateField;
    ConsultaDT_RETIRADO: TDateField;
    ConsultaSTATUS: TStringField;
    ConsultaPROTESTADO: TStringField;
    ConsultaENVIADO_APONTAMENTO: TStringField;
    ConsultaENVIADO_PROTESTO: TStringField;
    ConsultaENVIADO_PAGAMENTO: TStringField;
    ConsultaENVIADO_RETIRADO: TStringField;
    ConsultaENVIADO_SUSTADO: TStringField;
    ConsultaENVIADO_DEVOLVIDO: TStringField;
    ConsultaEXPORTADO: TStringField;
    ConsultaAVALISTA_DEVEDOR: TStringField;
    ConsultaFINS_FALIMENTARES: TStringField;
    ConsultaTARIFA_BANCARIA: TFloatField;
    ConsultaFORMA_PAGAMENTO: TStringField;
    ConsultaNUMERO_PAGAMENTO: TStringField;
    ConsultaARQUIVO: TStringField;
    ConsultaRETORNO: TStringField;
    ConsultaDT_DEVOLVIDO: TDateField;
    ConsultaCPF_CNPJ_SACADOR: TStringField;
    ConsultaID_MSG: TIntegerField;
    ConsultaVALOR_AR: TFloatField;
    ConsultaELETRONICO: TStringField;
    ConsultaIRREGULARIDADE: TIntegerField;
    ConsultaAVISTA: TStringField;
    ConsultaSALDO_TITULO: TFloatField;
    ConsultaTIPO_SUSTACAO: TStringField;
    ConsultaRETORNO_PROTESTO: TStringField;
    ConsultaDT_RETORNO_PROTESTO: TDateField;
    ConsultaDT_DEFINITIVA: TDateField;
    ConsultaJUDICIAL: TStringField;
    ConsultaALEATORIO_PROTESTO: TStringField;
    ConsultaALEATORIO_SOLUCAO: TStringField;
    ConsultaCCT: TStringField;
    ConsultaDETERMINACAO: TStringField;
    ConsultaANTIGO: TStringField;
    ConsultaLETRA: TStringField;
    ConsultaPROTOCOLO_CARTORIO: TIntegerField;
    ConsultaARQUIVO_CARTORIO: TStringField;
    ConsultaRETORNO_CARTORIO: TStringField;
    ConsultaDT_DEVOLVIDO_CARTORIO: TDateField;
    ConsultaCARTORIO: TIntegerField;
    ConsultaDT_PROTOCOLO_CARTORIO: TDateField;
    ConsultaID_SERVENTIA_AGREGADA: TIntegerField;
    ConsultaCODIGO_1: TIntegerField;
    ConsultaDESCRICAO: TStringField;
    ConsultaCODIGO_DISTRIBUICAO: TIntegerField;
    ConsultaVALOR_ULTIMO_SALDO: TFloatField;
    ConsultaQTD_ULTIMO_SALDO: TIntegerField;
    ConsultaDATA_ULTIMO_SALDO: TDateField;
    ConsultaFLG_ATIVA: TStringField;
    ConsultaDATA_INATIVACAO: TDateField;
    ConsultaFLG_USAPROTESTO: TStringField;
    ConsultaLOGRADOURO: TStringField;
    ConsultaCIDADE_LOGRADOURO: TStringField;
    ConsultaUF_LOGRADOURO: TStringField;
    ConsultaCEP: TStringField;
    ConsultaNOME_TITULAR: TStringField;
    ConsultaTELEFONE1: TStringField;
    ConsultaTELEFONE2: TStringField;
    ConsultaEMAIL1: TStringField;
    ConsultaEMAIL2: TStringField;
    qryTituloID_ATO: TIntegerField;
    qryTituloCODIGO: TIntegerField;
    qryTituloRECIBO: TIntegerField;
    qryTituloDT_ENTRADA: TDateField;
    qryTituloREGISTRO: TIntegerField;
    qryTituloSELO_REGISTRO: TStringField;
    qryTituloSELO_PAGAMENTO: TStringField;
    qryTituloRECIBO_PAGAMENTO: TIntegerField;
    qryTituloCOBRANCA: TStringField;
    qryTituloEMOLUMENTOS: TFloatField;
    qryTituloFETJ: TFloatField;
    qryTituloFUNDPERJ: TFloatField;
    qryTituloFUNPERJ: TFloatField;
    qryTituloFUNARPEN: TFloatField;
    qryTituloPMCMV: TFloatField;
    qryTituloISS: TFloatField;
    qryTituloMUTUA: TFloatField;
    qryTituloACOTERJ: TFloatField;
    qryTituloTIPO_PROTESTO: TIntegerField;
    qryTituloBANCO: TStringField;
    qryTituloAGENCIA: TStringField;
    qryTituloCONTA: TStringField;
    qryTituloCONVENIO: TStringField;
    qryTituloDT_ENVIO: TDateField;
    qryTituloCODIGO_APRESENTANTE: TStringField;
    qryTituloVALOR_PAGAMENTO: TFloatField;
    qryTituloAPRESENTANTE: TStringField;
    qryTituloCPF_CNPJ_APRESENTANTE: TStringField;
    qryTituloTIPO_APRESENTANTE: TStringField;
    qryTituloNOSSO_NUMERO: TStringField;
    qryTituloDEVEDOR: TStringField;
    qryTituloAGENCIA_CEDENTE: TStringField;
    qryTituloPRACA_PROTESTO: TStringField;
    qryTituloTIPO_ENDOSSO: TStringField;
    qryTituloACEITE: TStringField;
    qryTituloCPF_ESCREVENTE: TStringField;
    qryTituloCPF_ESCREVENTE_PG: TStringField;
    qryTituloPROTESTADO: TStringField;
    qryTituloENVIADO_APONTAMENTO: TStringField;
    qryTituloENVIADO_PROTESTO: TStringField;
    qryTituloENVIADO_PAGAMENTO: TStringField;
    qryTituloENVIADO_RETIRADO: TStringField;
    qryTituloENVIADO_SUSTADO: TStringField;
    qryTituloENVIADO_DEVOLVIDO: TStringField;
    qryTituloEXPORTADO: TStringField;
    qryTituloAVALISTA_DEVEDOR: TStringField;
    qryTituloFINS_FALIMENTARES: TStringField;
    qryTituloFORMA_PAGAMENTO: TStringField;
    qryTituloNUMERO_PAGAMENTO: TStringField;
    qryTituloCPF_CNPJ_SACADOR: TStringField;
    qryTituloID_MSG: TIntegerField;
    qryTituloELETRONICO: TStringField;
    qryTituloAVISTA: TStringField;
    qryTituloTIPO_SUSTACAO: TStringField;
    qryTituloRETORNO_PROTESTO: TStringField;
    qryTituloDT_RETORNO_PROTESTO: TDateField;
    qryTituloDT_DEFINITIVA: TDateField;
    qryTituloJUDICIAL: TStringField;
    qryTituloALEATORIO_PROTESTO: TStringField;
    qryTituloALEATORIO_SOLUCAO: TStringField;
    qryTituloCCT: TStringField;
    qryTituloDETERMINACAO: TStringField;
    qryTituloANTIGO: TStringField;
    qryTituloLETRA: TStringField;
    qryTituloDT_DEVOLVIDO_CARTORIO: TDateField;
    qryTituloID_SERVENTIA_AGREGADA: TIntegerField;
    qryTituloCODIGO_1: TIntegerField;
    qryTituloCODIGO_DISTRIBUICAO: TIntegerField;
    qryTituloVALOR_ULTIMO_SALDO: TFloatField;
    qryTituloQTD_ULTIMO_SALDO: TIntegerField;
    qryTituloDATA_ULTIMO_SALDO: TDateField;
    qryTituloFLG_ATIVA: TStringField;
    qryTituloDATA_INATIVACAO: TDateField;
    qryTituloFLG_USAPROTESTO: TStringField;
    qryTituloLOGRADOURO: TStringField;
    qryTituloCIDADE_LOGRADOURO: TStringField;
    qryTituloUF_LOGRADOURO: TStringField;
    qryTituloCEP: TStringField;
    qryTituloNOME_TITULAR: TStringField;
    qryTituloTELEFONE1: TStringField;
    qryTituloTELEFONE2: TStringField;
    qryTituloEMAIL1: TStringField;
    qryTituloEMAIL2: TStringField;
    lblPracaPagamento: TLabel;
    lblNomeDevedor: TLabel;
    lblEndDevedor: TLabel;
    lblDocDevedor: TLabel;
    qryTituloEND_DEV: TStringField;
    qryTituloBAIRRO_DEV: TStringField;
    qryTituloMUN_DEV: TStringField;
    qryTituloUF_DEV: TStringField;
    qryTituloCEP_DEV: TStringField;
    gbProtocolo: TsGroupBox;
    lbProtocoloD: TLabel;
    Label1: TLabel;
    lbProtocoloSA: TLabel;
    Label2: TLabel;
    lblNossoNumero: TLabel;
    gbServentiaAgregada: TsGroupBox;
    lblNomeServAgregada: TLabel;
    lblServentiaAgregada: TLabel;
    lblNomePortador: TLabel;
    lblCodigoPortador: TLabel;
    lblDocumentoPortador: TLabel;
    ConsultaDIAS_AVISTA: TIntegerField;
    qryTituloDIAS_AVISTA: TIntegerField;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DetalhesClick(Sender: TObject);
    procedure GridAtosDblClick(Sender: TObject);
    procedure GridAtosTitleButtonClick(Sender: TObject; AFieldName: String);
    procedure edPesquisarKeyPress(Sender: TObject; var Key: Char);
    procedure GridAtosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ExcluirClick(Sender: TObject);
    procedure dsConsultaDataChange(Sender: TObject; Field: TField);
    procedure edDataKeyPress(Sender: TObject; var Key: Char);
    procedure cbBuscarChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MObsExit(Sender: TObject);
    procedure lbObsClick(Sender: TObject);
    procedure lbTriduoClick(Sender: TObject);
    procedure lbContraprotestoClick(Sender: TObject);
    procedure Wrtln(T: String);
    procedure WriteLnEx(Const AMessage: String; AWidth: Integer);
    procedure Ultimos100;
    procedure FormShow(Sender: TObject);
    procedure ConsultaAfterOpen(DataSet: TDataSet);
    procedure RecibodeBaixa1Click(Sender: TObject);
    procedure M2Click(Sender: TObject);
    procedure J1Click(Sender: TObject);
    procedure MenuEtiquetaClick(Sender: TObject);
    procedure PMPopup(Sender: TObject);

    function ListaIds(Campo: String): String;
    procedure lbCustasClick(Sender: TObject);
  private
    { Private declarations }

    F: TextFile;

    sOrigPesquisa: String;

    procedure AbrirConsulta;
  public
    { Public declarations }
  end;

var
  FConsultaCompleta: TFConsultaCompleta;

implementation

uses UDM, UPF, UTitulo, UCertidao, DateUtils, UQuickRecibo1, UGeral, UQuickRecibo2,
  UEtiquetaRetificacao;

{$R *.dfm}

procedure TFConsultaCompleta.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;

  if (ActiveControl<>cbBuscar) then
    if (key=VK_UP) or (key=VK_DOWN) then GridAtos.SetFocus;

  if (ActiveControl<>edPesquisar) then
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFConsultaCompleta.DetalhesClick(Sender: TObject);
begin
  if Consulta.IsEmpty then
    Exit;

  dm.vTipo:='A';

  Application.CreateForm(TFTitulo,FTitulo);
  PF.Aguarde(True);
  FTitulo.CarregarTitulo(ConsultaID_ATO.AsInteger);
  PF.Aguarde(False);
  FTitulo.Visualizacao;
  FTitulo.ShowModal;
  FTitulo.Free;
end;

procedure TFConsultaCompleta.GridAtosDblClick(Sender: TObject);
begin
  DetalhesClick(Sender);
  AbrirConsulta;
end;

procedure TFConsultaCompleta.GridAtosTitleButtonClick(Sender: TObject;
  AFieldName: String);
begin
  Consulta.IndexFieldNames:=AFieldName;
end;

procedure TFConsultaCompleta.edPesquisarKeyPress(Sender: TObject; var Key: Char);
var
  Query, Status, vLista: String;
begin
  if cbBuscar.ItemIndex in [0, 1] then
  begin
    if not (Key in ['0'..'9',#8,#13]) then
      Key := #0;
  end;

  if key = #13 then
  begin
    sOrigPesquisa := 'P';
    AbrirConsulta;
  end;
end;

procedure TFConsultaCompleta.GridAtosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if key=VK_RETURN then DetalhesClick(Sender);
  if key=VK_DELETE then ExcluirClick(Sender);
end;

procedure TFConsultaCompleta.ExcluirClick(Sender: TObject);
var
  Id: String;
begin
  if Consulta.IsEmpty then Exit;
  if PF.PasswordInputBox('Seguran�a','Senha')<>'DELPROT' then Exit;
  if GR.Pergunta('Confirma a exclus�o') then
  begin
      Id:=ConsultaID_ATO.AsString;
      Consulta.Delete;
      Consulta.ApplyUpdates(0);
      PF.ExcluirFD('CUSTAS WHERE ID_ATO='+QuotedStr(Id),dm.conSISTEMA);
  end;
end;

procedure TFConsultaCompleta.dsConsultaDataChange(Sender: TObject;
  Field: TField);
var
  Intimacao, Data, Livro, sCustas: String;
begin
  sCustas := '';

  P3.Visible:=not Consulta.IsEmpty;

  qryTitulo.Close;
  qryTitulo.Params[0].AsInteger := ConsultaID_ATO.AsInteger;
  qryTitulo.Open;

  dm.Movimento.Close;
  dm.Movimento.Params[0].AsInteger:=ConsultaID_ATO.AsInteger;
  dm.Movimento.Open;

  dm.Movimento.First;

  Obs.Close;
  Obs.Params.ParamByName('ID_ATO').Value := ConsultaID_ATO.AsInteger;
  Obs.Open;

  if qryTituloSTATUS.AsString='APONTADO'         then Data := PF.FormatarData(qryTituloDT_PROTOCOLO.AsDateTime,'N');
  if qryTituloSTATUS.AsString='INTIMADO PESSOAL' then Data := PF.FormatarData(qryTituloDT_INTIMACAO.AsDateTime,'N');
  if qryTituloSTATUS.AsString='INTIMADO EDITAL'  then Data := PF.FormatarData(qryTituloDT_PUBLICACAO.AsDateTime,'N');
  if qryTituloSTATUS.AsString='INTIMADO CARTA'   then Data := PF.FormatarData(qryTituloDT_INTIMACAO.AsDateTime,'N');
  if qryTituloSTATUS.AsString='PROTESTADO'       then Data := PF.FormatarData(qryTituloDT_REGISTRO.AsDateTime,'N');
  if qryTituloSTATUS.AsString='PAGO'             then Data := PF.FormatarData(qryTituloDT_PAGAMENTO.AsDateTime,'N');
  if qryTituloSTATUS.AsString='CANCELADO'        then Data := PF.FormatarData(qryTituloDT_PAGAMENTO.AsDateTime,'N');
  if qryTituloSTATUS.AsString='RETIRADO'         then Data := PF.FormatarData(qryTituloDT_RETIRADO.AsDateTime,'N');
  if qryTituloSTATUS.AsString='SUSTADO'          then Data := PF.FormatarData(qryTituloDT_SUSTADO.AsDateTime,'N');
  if qryTituloSTATUS.AsString='DEVOLVIDO'        then Data := PF.FormatarData(qryTituloDT_DEVOLVIDO.AsDateTime,'N');

  if qryTituloTIPO_INTIMACAO.AsString='P' then Intimacao:='Pessoal';
  if qryTituloTIPO_INTIMACAO.AsString='E' then Intimacao:='Edital ('+Trim(qryTituloMOTIVO_INTIMACAO.AsString)+')';
  if qryTituloTIPO_INTIMACAO.AsString='C' then Intimacao:='Carta';

  if qryTituloSTATUS.AsString='PROTESTADO' then
    Livro:=' (LIVRO: '+qryTituloLIVRO_REGISTRO.AsString+'/FOLHA: '+qryTituloFOLHA_REGISTRO.AsString+')'
      else Livro:='';

  if Trim(qryTituloSTATUS.AsString) = 'Aceito' then
    lbStatus.Caption           := ''
  else
  begin
    if Trim(Data) <> '' then
      lbStatus.Caption         := qryTituloSTATUS.AsString + #13#10 +
                                  ' EM ' + Data
    else
      lbStatus.Caption         := qryTituloSTATUS.AsString;
  end;

  lbProtocoloD.Caption         := qryTituloPROTOCOLO.AsString;

  if Trim(qryTituloPROTOCOLO_CARTORIO.AsString) = '' then
    lbProtocoloSA.Caption      := '__________'
  else
    lbProtocoloSA.Caption      := qryTituloPROTOCOLO_CARTORIO.AsString;

  lblNomeDevedor.Caption       := 'Devedor: ' + qryTituloDEVEDOR.AsString;
  lblDocDevedor.Caption        := 'Documento: ' + qryTituloCPF_CNPJ_DEVEDOR.AsString;
  lblEndDevedor.Caption        := 'Endere�o: ' + qryTituloEND_DEV.AsString +
                                                 IfThen(qryTituloBAIRRO_DEV.IsNull or
                                                        (Trim(qryTituloBAIRRO_DEV.AsString) = ''), '', ', ' + qryTituloBAIRRO_DEV.AsString) +
                                                 IfThen(qryTituloMUN_DEV.IsNull or
                                                        (Trim(qryTituloMUN_DEV.AsString) = ''), '', ', ' + qryTituloMUN_DEV.AsString) +
                                                 IfThen(qryTituloUF_DEV.IsNull or
                                                        (Trim(qryTituloUF_DEV.AsString) = ''), '', ', ' + qryTituloUF_DEV.AsString) +
                                                 IfThen(qryTituloCEP_DEV.IsNull or
                                                        (Trim(qryTituloCEP_DEV.AsString) = ''), '', ' - ' + qryTituloCEP_DEV.AsString);
  lblPracaPagamento.Caption    := 'Pra�a: ' + qryTituloPRACA_PROTESTO.AsString;
  lbData.Caption               := 'Data (D.): '+ PF.FormatarData(qryTituloDT_PROTOCOLO.AsDateTime, 'N');
  lblDataSA.Caption            := 'Data (S.Ag.): ' + PF.FormatarData(qryTituloDT_PROTOCOLO_CARTORIO.AsDateTime, 'N');
  lbPrazo.Caption              := 'Prazo: ' + PF.FormatarData(qryTituloDT_PRAZO.AsDateTime, 'N');
  lbEspecie.Caption            := 'Esp�cie: ' + PF.RetornarTitulo(qryTituloTIPO_TITULO.AsInteger);
  lbTitulo.Caption             := 'T�tulo: ' + qryTituloNUMERO_TITULO.AsString;
  lblNossoNumero.Caption       := 'N. N�mero: ' + qryTituloNOSSO_NUMERO.AsString;
  lbEmissao.Caption            := 'Emiss�o: ' + PF.FormatarData(qryTituloDT_TITULO.AsDateTime, 'N');
  lbTipo.Caption               := 'Tipo: ' + qryTituloTIPO_APRESENTACAO.AsString;

  if not qryTituloDIAS_AVISTA.IsNull then
    lbVencimento.Caption     := 'Vencimento: � Vista'
  else
  begin
    if qryTituloDT_VENCIMENTO.AsDateTime <> 0 then
      lbVencimento.Caption     := 'Vencimento: ' + PF.FormatarData(qryTituloDT_VENCIMENTO.AsDateTime, 'N')
    else
      lbVencimento.Caption     := 'Vencimento: ';
  end;

  lbVencimento.Caption         := 'Vencimento: ' + PF.FormatarData(qryTituloDT_VENCIMENTO.AsDateTime, 'N');

  lbValor.Caption              := 'Valor: ' + FloatToStrF(GR.NoRound(qryTituloVALOR_TITULO.AsFloat, 2), ffFixed, 16, 2);

  lbSaldo.Caption              := '(+) Saldo: ' + FloatToStrF(GR.NoRound(qryTituloSALDO_TITULO.AsFloat, 2), ffFixed, 16, 2);

  sCustas := FloatToStrF(GR.NoRound(qryTituloTOTAL.AsFloat-
                                    qryTituloTARIFA_BANCARIA.AsFloat-
                                    qryTituloVALOR_AR.AsFloat-
                                    qryTituloDISTRIBUICAO.AsFloat, 2),
                         ffFixed, 16, 2);
  lbCustas.Caption             := '(+) Custas: ' + sCustas;

  lbTotal.Caption              := '(=) Total: ' + FloatToStrF(GR.NoRound(qryTituloSALDO_PROTESTO.AsFloat, 2), ffFixed, 16, 2);

  lblNomePortador.Caption      := 'Portador: ' + qryTituloAPRESENTANTE.AsString;
  lblCodigoPortador.Caption    := 'C�d. Portador: ' + qryTituloCODIGO_APRESENTANTE.AsString;
  lblDocumentoPortador.Caption := 'Doc. Portador: ' + qryTituloCPF_CNPJ_APRESENTANTE.AsString;
  lbSacador.Caption            := 'Sacador: ' + qryTituloSACADOR.AsString;
  lbCedente.Caption            := 'Cedente: ' + qryTituloCEDENTE.AsString;
  lblNomeServAgregada.Caption  := qryTituloDESCRICAO.AsString;
  lblServentiaAgregada.Caption := qryTituloCARTORIO.AsString;

  gbServentiaAgregada.Visible := qryTituloCARTORIO.AsInteger > 0;
end;

procedure TFConsultaCompleta.edDataKeyPress(Sender: TObject; var Key: Char);
var
  Query: String;
begin
  if key = #13 then
  begin
    sOrigPesquisa := 'D';
    AbrirConsulta;
  end;
end;

procedure TFConsultaCompleta.AbrirConsulta;
var
  Query, Status, vLista: String;
begin
  PF.Aguarde(True);

  case AnsiIndexStr(UpperCase(sOrigPesquisa), ['U', 'P', 'D']) of
    0:  //Ultimos100
      Ultimos100;
    1:  //Pesquisa
    begin
      {NOME, DOCUMENTO OU DEVEDOR OU ENDERECO}
      if (cbBuscar.ItemIndex = 3) or (cbBuscar.ItemIndex = 4) or (cbBuscar.ItemIndex = 5) then
      begin
        if cbBuscar.ItemIndex = 3 then
          vLista:='(ID_ATO IN (SELECT ID_ATO FROM DEVEDORES WHERE NOME LIKE '+QuotedStr(edPesquisar.Text+'%')+'))';
        //vLista := ListaIds('NOME');

        if cbBuscar.ItemIndex = 4 then
          vLista:='(ID_ATO IN (SELECT ID_ATO FROM DEVEDORES WHERE DOCUMENTO='+QuotedStr(GR.PegarNumeroTexto(edPesquisar.Text))+'))';
        //vLista := ListaIds('DOCUMENTO');

        if cbBuscar.ItemIndex = 5 then
          vLista:='(ID_ATO IN (SELECT ID_ATO FROM DEVEDORES WHERE ENDERECO LIKE '+QuotedStr(edPesquisar.Text+'%')+'))';
        //vLista := ListaIds('ENDERECO');

        {if vLista = '*' then
        begin
          PF.Aguarde(False);
          GR.Aviso('PREENCHA MAIS INFORMA��ES DO ENDERE�O!');
          edPesquisar.SetFocus;
          Exit;
        end;

        if Trim(vLista) = ')' then
        vLista := ' ID_ATO IN (-1) ';  }
      end;

      if edPesquisar.Text = '' then
      begin
        Query := 'SELECT T.*, ' +
                 '       SA.* ' +
                 '  FROM TITULOS T ' +
                 '  LEFT JOIN SERVENTIA_AGREGADA SA ' +
                 '    ON T.CARTORIO = SA.CODIGO_DISTRIBUICAO ' +
                 ' WHERE NOT PROTOCOLO IS NULL ' +
                 'ORDER BY DT_PROTOCOLO, PROTOCOLO';

        Consulta.Close;
        Consulta.CommandText := Query;
        Consulta.Open;
        PF.Aguarde(False);

        Exit;
      end;

      Query := 'SELECT T.*, ' +
               '       SA.* ' +
               '  FROM TITULOS T ' +
               '  LEFT JOIN SERVENTIA_AGREGADA SA ' +
               '    ON T.CARTORIO = SA.CODIGO_DISTRIBUICAO ' +
               ' WHERE ';

      Consulta.Close;

      { PROTOCOLO (DISTRIBUIDOR) }
      if cbBuscar.ItemIndex = 0 then
      begin
        Consulta.CommandText := Query + 'PROTOCOLO = :P ' + Status + ' AND NOT PROTOCOLO IS NULL ORDER BY DT_PROTOCOLO, PROTOCOLO';
        Consulta.Params[0].AsInteger := StrToInt(edPesquisar.Text);
      end;

      { PROTOCOLO (SERVENTIA AGREGADA) }
      if cbBuscar.ItemIndex = 1 then
      begin
        Consulta.CommandText := Query + 'PROTOCOLO_CARTORIO = :P ' + Status + ' AND NOT PROTOCOLO IS NULL ORDER BY DT_PROTOCOLO, PROTOCOLO';
        Consulta.Params[0].AsInteger := StrToInt(edPesquisar.Text);
      end;

      { PORTADOR }
      if cbBuscar.ItemIndex = 2 then
      begin
        Consulta.CommandText := Query + 'APRESENTANTE LIKE :P ' + Status + ' AND NOT PROTOCOLO IS NULL ORDER BY DT_PROTOCOLO, PROTOCOLO';
        Consulta.Params[0].AsString := edPesquisar.Text + '%';
      end;

      { DEVEDOR }
      if cbBuscar.ItemIndex = 3 then
      begin
        Consulta.CommandText := Query + vLista + Status + ' AND NOT PROTOCOLO IS NULL ORDER BY DT_PROTOCOLO, PROTOCOLO';
        //Consulta.Params[0].AsString:=edPesquisar.Text+'%';
      end;

      { DOCUMENTO DO DEVEDOR }
      if cbBuscar.ItemIndex = 4 then
      begin
        Consulta.CommandText := Query + vLista + Status + ' AND NOT PROTOCOLO IS NULL ORDER BY DT_PROTOCOLO, PROTOCOLO';
        //Consulta.Params[0].AsString:=edPesquisar.Text;
      end;

      { ENDERE�O DO DEVEDOR }
      if cbBuscar.ItemIndex = 5 then
        Consulta.CommandText := Query + vLista + Status + ' AND NOT PROTOCOLO IS NULL ORDER BY DT_PROTOCOLO, PROTOCOLO';

      {SACADOR}
      if cbBuscar.ItemIndex = 6 then
      begin
        Consulta.CommandText := Query + 'SACADOR LIKE :P ' + Status + ' AND NOT PROTOCOLO IS NULL ORDER BY DT_PROTOCOLO, PROTOCOLO';
        Consulta.Params[0].AsString := edPesquisar.Text + '%';
      end;

      { CEDENTE }
      if cbBuscar.ItemIndex = 7 then
      begin
        Consulta.CommandText := Query + 'CEDENTE LIKE :P ' + Status + ' AND NOT PROTOCOLO IS NULL ORDER BY DT_PROTOCOLO, PROTOCOLO';
        Consulta.Params[0].AsString := edPesquisar.Text + '%';
      end;

      { NUMERO DO T�TULO }
      if cbBuscar.ItemIndex=8 then
      begin
        Consulta.CommandText := Query + 'NUMERO_TITULO = :P ' + Status + ' AND NOT PROTOCOLO IS NULL ORDER BY DT_PROTOCOLO, PROTOCOLO';
        Consulta.Params[0].AsString := edPesquisar.Text;
      end;

      Consulta.Open;
      Consulta.IndexFieldNames := 'PROTOCOLO';
    end;
    2:  //Data
    begin
      Query := 'SELECT T.*, ' +
               '       SA.* ' +
               '  FROM TITULOS T ' +
               '  LEFT JOIN SERVENTIA_AGREGADA SA ' +
               '    ON T.CARTORIO = SA.CODIGO_DISTRIBUICAO ' +
               ' WHERE ';

      Consulta.Close;

      if cbBuscar.ItemIndex = 9 then
        Consulta.CommandText := Query + 'T.DT_PROTOCOLO = :P ORDER BY T.DT_PROTOCOLO, T.PROTOCOLO'
      else
        Consulta.CommandText := Query + 'T.DT_REGISTRO = :P OR T.DT_PAGAMENTO = :P OR T.DT_SUSTADO = :P OR T.DT_RETIRADO = :P OR T.DT_DEVOLVIDO = :P ORDER BY T.DT_PROTOCOLO, T.PROTOCOLO';

      Consulta.Params.ParamByName('P').AsDate := edData.Date;
      Consulta.Open;
    end;
  end;

  Consulta.Last;

  PF.Aguarde(False);
end;

procedure TFConsultaCompleta.cbBuscarChange(Sender: TObject);
begin
  if (cbBuscar.ItemIndex = 9) or (cbBuscar.ItemIndex = 10) then
  begin
    edPesquisar.Clear;
    edPesquisar.Enabled := False;
    edData.Enabled := True;
    edData.SetFocus;
  end
  else
  begin
    edPesquisar.Enabled := True;
    edData.Enabled := False;
    edPesquisar.SetFocus;
  end;
end;

procedure TFConsultaCompleta.FormCreate(Sender: TObject);
begin
  edData.Date:=Now;
  Ultimos100;
  Consulta.Last;
  Self.Caption:='Consulta Completa';
end;

procedure TFConsultaCompleta.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ActiveControl:=Nil;
  qryTitulo.Close;
end;

procedure TFConsultaCompleta.MObsExit(Sender: TObject);
begin
  if Obs.State in [dsEdit,dsInsert] then
  begin
      Obs.Post;
      Obs.ApplyUpdates(0);
  end;
end;

procedure TFConsultaCompleta.lbObsClick(Sender: TObject);
begin
  Obs.Edit;
end;

procedure TFConsultaCompleta.lbTriduoClick(Sender: TObject);
begin
  Obs.Edit;
end;

procedure TFConsultaCompleta.lbContraprotestoClick(Sender: TObject);
begin
  Obs.Edit;
end;

procedure TFConsultaCompleta.lbCustasClick(Sender: TObject);
begin
  dm.AtualizaCustasTitulo(ConsultaID_ATO.AsInteger);
  Consulta.Refresh;
end;

procedure TFConsultaCompleta.WriteLnEx(Const AMessage: String; AWidth: Integer);
begin
  if Trim(AMessage)='' then Exit;
  WriteLn(F,Copy(AMessage, 1, AWidth));
  WriteLnEx(Copy(AMessage, AWidth + 1, Length(AMessage)), AWidth);
end;

procedure TFConsultaCompleta.Wrtln(T: String);
begin
  Writeln(F,Copy(PF.RemoverAcento(T),1,55));
end;

procedure TFConsultaCompleta.Ultimos100;
var
  Query: String;
begin
  Query:='SELECT FIRST 100 T.*, ' +
         '       SA.* ' +
         '  FROM TITULOS T ' +
         '  LEFT JOIN SERVENTIA_AGREGADA SA ' +
         '    ON T.CARTORIO = SA.CODIGO_DISTRIBUICAO ' +
         ' WHERE NOT T.PROTOCOLO IS NULL ' +
         'ORDER BY T.DT_PROTOCOLO DESCENDING';
  Consulta.Close;
  Consulta.CommandText := Query;
  Consulta.Open;
  Consulta.IndexFieldNames:='DT_PROTOCOLO;PROTOCOLO';

  sOrigPesquisa := 'U';
end;

procedure TFConsultaCompleta.FormShow(Sender: TObject);
begin
  PF.Aguarde(False);

  //Protocolo
  gbProtocolo.Font.Color   := clNavy;
  lbProtocoloD.Font.Color  := clNavy;
  lbProtocoloSA.Font.Color := clNavy;
  Label1.Font.Color        := clNavy;
  Label2.Font.Color        := clNavy;

  //Status
  lbStatus.Font.Color := clNavy;

  //Serventia Agregada
  gbServentiaAgregada.Font.Color  := clNavy;
  lblNomeServAgregada.Font.Color  := clNavy;
  lblServentiaAgregada.Font.Color := clNavy;

  //Devedor
  lblNomeDevedor.Font.Color := clRed;
  lblDocDevedor.Font.Color  := clRed;
  lblEndDevedor.Font.Color  := clRed;

  //Demais Dados do Titulo
  lbData.Font.Color    := clNavy;
  lblDataSA.Font.Color := clNavy;
  lbPrazo.Font.Color   := clNavy;

  lbTitulo.Font.Color          := clNavy;
  lblNossoNumero.Font.Color    := clNavy;
  lbEmissao.Font.Color         := clNavy;
  lblPracaPagamento.Font.Color := clNavy;
  lbVencimento.Font.Color      := clNavy;

  lbEspecie.Font.Color := clNavy;
  lbTipo.Font.Color    := clNavy;

  lbValor.Font.Color  := clNavy;
  lbSaldo.Font.Color  := clNavy;
  lbCustas.Font.Color := clNavy;
  lbTotal.Font.Color  := clNavy;

  //Portador
  lblNomePortador.Font.Color      := clNavy;
  lblCodigoPortador.Font.Color    := clNavy;
  lblDocumentoPortador.Font.Color := clNavy;

  //Sacador
  lbsacador.Font.Color := clNavy;

  //Cedente
  lbCedente.Font.Color := clNavy;
end;

procedure TFConsultaCompleta.ConsultaAfterOpen(DataSet: TDataSet);
begin
  Self.Caption := 'Consulta Completa: ' + IntToStr(Consulta.RecordCount) + ' encontrados';
end;

procedure TFConsultaCompleta.RecibodeBaixa1Click(Sender: TObject);
begin
  Application.CreateForm(TFQuickRecibo2,FQuickRecibo2);
  with FQuickRecibo2 do
  begin
      RX.Close;
      RX.Open;
      RX.Append;
      RXLivroProtocolo.AsInteger  :=qryTituloLIVRO_PROTOCOLO.AsInteger;
      RXFolhaProtocolo.AsString   :=qryTituloFOLHA_PROTOCOLO.AsString;
      RXDevedor.AsString          :=ConsultaDEVEDOR.AsString;
      if qryTituloTIPO_DEVEDOR.AsString='J' then
        RXDocumento.AsString:=GR.FormatarCNPJ(qryTituloCPF_CNPJ_DEVEDOR.AsString)
          else RXDocumento.AsString:=GR.FormatarCPF(qryTituloCPF_CNPJ_DEVEDOR.AsString);
      RXCedente.AsString          :=qryTituloCEDENTE.AsString;
      RXPortador.AsString         :=ConsultaAPRESENTANTE.AsString;
      RXSacador.AsString          :=qryTituloSACADOR.AsString;
      RXProtocolo.AsInteger       :=ConsultaPROTOCOLO.AsInteger;
      RXValorTitulo.AsFloat       :=qryTituloVALOR_TITULO.AsFloat;
      RXDataVencimento.AsDateTime :=qryTituloDT_VENCIMENTO.AsDateTime;
      RXNumeroTitulo.AsString     :=qryTituloNUMERO_TITULO.AsString;
      RXStatus.AsString           :=qryTituloSTATUS.AsString;

      if qryTituloSTATUS.AsString='APONTADO'         then RXDataStatus.AsDateTime:=qryTituloDT_PROTOCOLO.AsDateTime;
      if qryTituloSTATUS.AsString='INTIMADO PESSOAL' then RXDataStatus.AsDateTime:=qryTituloDT_INTIMACAO.AsDateTime;
      if qryTituloSTATUS.AsString='INTIMADO EDITAL'  then RXDataStatus.AsDateTime:=qryTituloDT_PUBLICACAO.AsDateTime;
      if qryTituloSTATUS.AsString='INTIMADO CARTA'   then RXDataStatus.AsDateTime:=qryTituloDT_INTIMACAO.AsDateTime;
      if qryTituloSTATUS.AsString='PROTESTADO'       then RXDataStatus.AsDateTime:=qryTituloDT_REGISTRO.AsDateTime;
      if qryTituloSTATUS.AsString='PAGO'             then RXDataStatus.AsDateTime:=qryTituloDT_PAGAMENTO.AsDateTime;
      if qryTituloSTATUS.AsString='CANCELADO'        then RXDataStatus.AsDateTime:=qryTituloDT_PAGAMENTO.AsDateTime;
      if qryTituloSTATUS.AsString='RETIRADO'         then RXDataStatus.AsDateTime:=qryTituloDT_RETIRADO.AsDateTime;
      if qryTituloSTATUS.AsString='SUSTADO'          then RXDataStatus.AsDateTime:=qryTituloDT_SUSTADO.AsDateTime;
      if qryTituloSTATUS.AsString='DEVOLVIDO'        then RXDataStatus.AsDateTime:=qryTituloDT_DEVOLVIDO.AsDateTime;

      RX.Post;
      Recibo.Preview;
      Free;
  end;
end;

procedure TFConsultaCompleta.M2Click(Sender: TObject);
begin
  try
    PF.Aguarde(True);
    dm.ReciboMatricial(ConsultaID_ATO.AsInteger,'P');
    PF.Aguarde(False);
  except
    on E: Exception do
    begin
        PF.Aguarde(False);
        GR.Aviso('Erro: '+E.Message);
    end;
  end;
end;

procedure TFConsultaCompleta.J1Click(Sender: TObject);
begin
  if Consulta.IsEmpty then Exit;

  PF.Aguarde(True);

  dm.Titulos.Close;
  dm.Titulos.Params[0].AsInteger:=ConsultaID_ATO.AsInteger;
  dm.Titulos.Open;

  PF.CarregarCustas(dm.TitulosID_ATO.AsInteger);

  Application.CreateForm(TFQuickRecibo1,FQuickRecibo1);

  dm.Titulos.Close;
end;

function TFConsultaCompleta.ListaIds(Campo: String): String;
var
  I: Integer;
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT ID_ATO FROM DEVEDORES WHERE ' + Campo + ' LIKE ' + QuotedStr(edPesquisar.Text + '%'));
  Q.Open;
  Result := '';
  I := 1;
  while not Q.Eof do
  begin
      Inc(I);

      {if Result='' then
        Result:=' ID_ATO IN ('+Q.FieldByName('ID_ATO').AsString
          else Result:=Result+','+Q.FieldByName('ID_ATO').AsString;}

      if Result='' then
        Result:='(ID_ATO='+Q.FieldByName('ID_ATO').AsString
          else Result:=Result+' OR ID_ATO='+Q.FieldByName('ID_ATO').AsString;

      Q.Next;
  end;
  Result:=Result+') ';

  {if I>1500 then
  Result:='*';}

  Q.Free;
end;

procedure TFConsultaCompleta.MenuEtiquetaClick(Sender: TObject);
begin
  dm.vDataProtocolo:=GR.FormatarData(ConsultaDT_PROTOCOLO.AsDateTime);
  dm.vProtocolo:=ConsultaPROTOCOLO.AsString;
  GR.CriarForm(TFEtiquetaRetificacao,FEtiquetaRetificacao);
end;

procedure TFConsultaCompleta.PMPopup(Sender: TObject);
begin
  MenuEtiqueta.Visible:=dm.ServentiaCODIGO.AsInteger=2652;
end;

end.
