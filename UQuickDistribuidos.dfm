object FQuickDistribuidos: TFQuickDistribuidos
  Left = 253
  Top = 116
  Caption = 'Distribu'#237'dos'
  ClientHeight = 651
  ClientWidth = 822
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Relatorio: TQuickRep
    Left = 8
    Top = 8
    Width = 794
    Height = 1123
    ShowingPreview = False
    DataSet = Titulos
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Continuous = False
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = False
    PrinterSettings.CustomBinCode = 0
    PrinterSettings.ExtendedDuplex = 0
    PrinterSettings.UseCustomPaperCode = False
    PrinterSettings.CustomPaperCode = 0
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.MemoryLimit = 1000000
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 0
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 100
    PrevFormStyle = fsNormal
    PreviewInitialState = wsMaximized
    PreviewWidth = 500
    PreviewHeight = 500
    PrevInitialZoom = qrZoomToFit
    PreviewDefaultSaveType = stPDF
    PreviewLeft = 0
    PreviewTop = 0
    object QRChildBand2: TQRChildBand
      Left = 38
      Top = 217
      Width = 718
      Height = 10
      AlignToBottom = False
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        26.458333333333330000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      ParentBand = pghTitulosDistribuidos
      PrintOrder = cboAfterParent
    end
    object QRChildBand1: TQRChildBand
      Left = 38
      Top = 324
      Width = 718
      Height = 20
      Frame.DrawTop = True
      AlignToBottom = False
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        52.916666666666670000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = True
      ParentBand = gfServentiaA
      PrintOrder = cboAfterParent
    end
    object pghTitulosDistribuidos: TQRBand
      Left = 38
      Top = 38
      Width = 718
      Height = 179
      Frame.DrawTop = True
      Frame.DrawBottom = True
      Frame.DrawLeft = True
      Frame.DrawRight = True
      AlignToBottom = False
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        473.604166666666700000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbPageHeader
      object lbCartorio: TQRLabel
        Left = 283
        Top = 7
        Width = 151
        Height = 23
        Size.Values = (
          60.854166666666670000
          748.770833333333300000
          18.520833333333330000
          399.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'Nome do Cart'#243'rio'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 14
      end
      object lbEndereco: TQRLabel
        Left = 335
        Top = 72
        Width = 47
        Height = 17
        Size.Values = (
          44.979166666666670000
          886.354166666666700000
          190.500000000000000000
          124.354166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'Endere'#231'o'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbPeriodo: TQRLabel
        Left = 336
        Top = 129
        Width = 45
        Height = 19
        Size.Values = (
          50.270833333333330000
          889.000000000000000000
          341.312500000000000000
          119.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'Periodo'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRSysData2: TQRSysData
        Left = 627
        Top = 160
        Width = 84
        Height = 17
        Size.Values = (
          44.979166666666670000
          1658.937500000000000000
          423.333333333333300000
          222.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Color = clWhite
        Data = qrsPageNumber
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Text = 'P'#225'gina: '
        Transparent = True
        ExportAs = exptText
        FontSize = 8
      end
      object QRLabel7: TQRLabel
        Left = 209
        Top = 29
        Width = 300
        Height = 17
        Size.Values = (
          44.979166666666670000
          552.979166666666700000
          76.729166666666670000
          793.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'PROTESTO DE T'#205'TULOS E DOCUMENTOS DE D'#205'VIDA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object lbTabeliao: TQRLabel
        Left = 338
        Top = 52
        Width = 41
        Height = 17
        Size.Values = (
          44.979166666666670000
          894.291666666666700000
          137.583333333333300000
          108.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'Tabeli'#227'o'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel8: TQRLabel
        Left = 201
        Top = 100
        Width = 315
        Height = 20
        Size.Values = (
          52.916666666666670000
          531.812500000000000000
          264.583333333333300000
          833.437500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'RELAT'#211'RIO DE T'#205'TULOS DISTRIBU'#205'DOS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 12
      end
    end
    object hdServentiaA: TQRChildBand
      Left = 38
      Top = 252
      Width = 718
      Height = 24
      Frame.DrawTop = True
      Frame.DrawBottom = True
      Frame.DrawLeft = True
      Frame.DrawRight = True
      AlignToBottom = False
      Color = cl3DLight
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        63.500000000000000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = True
      ParentBand = ghServentiaA
      PrintOrder = cboAfterParent
      object QRLabel1: TQRLabel
        Left = 284
        Top = 4
        Width = 62
        Height = 16
        Size.Values = (
          42.333333333333330000
          751.416666666666700000
          10.583333333333330000
          164.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Protocolo'
        Color = cl3DLight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel2: TQRLabel
        Left = 6
        Top = 4
        Width = 109
        Height = 16
        Size.Values = (
          42.333333333333330000
          15.875000000000000000
          10.583333333333330000
          288.395833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome do Sacado'
        Color = cl3DLight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel3: TQRLabel
        Left = 392
        Top = 6
        Width = 70
        Height = 16
        Size.Values = (
          42.333333333333330000
          1037.166666666667000000
          15.875000000000000000
          185.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor T'#237'tulo'
        Color = cl3DLight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel5: TQRLabel
        Left = 478
        Top = 6
        Width = 70
        Height = 16
        Size.Values = (
          42.333333333333330000
          1264.708333333333000000
          15.875000000000000000
          185.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor Custa'
        Color = cl3DLight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel4: TQRLabel
        Left = 631
        Top = 4
        Width = 80
        Height = 16
        Size.Values = (
          42.333333333333330000
          1669.520833333333000000
          10.583333333333330000
          211.666666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'M'#250'tua/Acoterj'
        Color = cl3DLight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel6: TQRLabel
        Left = 598
        Top = 4
        Width = 28
        Height = 16
        Size.Values = (
          42.333333333333330000
          1582.208333333333000000
          10.583333333333330000
          74.083333333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'FETJ'
        Color = cl3DLight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
    end
    object gfServentiaA: TQRBand
      Left = 38
      Top = 298
      Width = 718
      Height = 24
      Frame.DrawTop = True
      Frame.DrawLeft = True
      Frame.DrawRight = True
      AlignToBottom = False
      BeforePrint = gfServentiaABeforePrint
      Color = cl3DLight
      TransparentBand = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ForceNewColumn = False
      ForceNewPage = False
      ParentFont = False
      Size.Values = (
        63.500000000000000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = True
      BandType = rbGroupFooter
      object lblTotQuantidade: TQRLabel
        Left = 6
        Top = 4
        Width = 200
        Height = 16
        Size.Values = (
          42.333333333333330000
          15.875000000000000000
          10.583333333333330000
          529.166666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Quantidade de T'#237'tulos:'
        Color = cl3DLight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lblTotVlrFETJ: TQRLabel
        Left = 562
        Top = 4
        Width = 64
        Height = 16
        Size.Values = (
          42.333333333333330000
          1486.958333333333000000
          10.583333333333330000
          169.333333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor T'#237'tulo'
        Color = cl3DLight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lblTotVlrMuAc: TQRLabel
        Left = 648
        Top = 4
        Width = 64
        Height = 16
        Size.Values = (
          42.333333333333330000
          1714.500000000000000000
          10.583333333333330000
          169.333333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor Custa'
        Color = cl3DLight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lblTotVlrTitulos: TQRLabel
        Left = 392
        Top = 4
        Width = 70
        Height = 16
        Size.Values = (
          42.333333333333330000
          1037.166666666667000000
          10.583333333333330000
          185.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor T'#237'tulo'
        Color = cl3DLight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lblTotVlrCustas: TQRLabel
        Left = 478
        Top = 4
        Width = 70
        Height = 16
        Size.Values = (
          42.333333333333330000
          1264.708333333333000000
          10.583333333333330000
          185.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor Custa'
        Color = cl3DLight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    object dtServentiaA: TQRBand
      Left = 38
      Top = 276
      Width = 718
      Height = 22
      AlignToBottom = False
      BeforePrint = dtServentiaABeforePrint
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        58.208333333333330000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = True
      BandType = rbDetail
      object QRDBText2: TQRDBText
        Left = 6
        Top = 4
        Width = 271
        Height = 15
        Size.Values = (
          39.687500000000000000
          15.875000000000000000
          10.583333333333330000
          717.020833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = Titulos
        DataField = 'DEVEDOR'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText1: TQRDBText
        Left = 284
        Top = 4
        Width = 97
        Height = 15
        Size.Values = (
          39.687500000000000000
          751.416666666666700000
          10.583333333333330000
          256.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = Titulos
        DataField = 'PROTOCOLO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText3: TQRDBText
        Left = 392
        Top = 4
        Width = 70
        Height = 15
        Size.Values = (
          39.687500000000000000
          1037.166666666667000000
          10.583333333333330000
          185.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = Titulos
        DataField = 'VALOR_TITULO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText4: TQRDBText
        Left = 478
        Top = 4
        Width = 70
        Height = 15
        Size.Values = (
          39.687500000000000000
          1264.708333333333000000
          10.583333333333330000
          185.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = Titulos
        DataField = 'TOTAL'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText6: TQRDBText
        Left = 562
        Top = 4
        Width = 64
        Height = 15
        Size.Values = (
          39.687500000000000000
          1486.958333333333000000
          10.583333333333330000
          169.333333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = Titulos
        DataField = 'FETJ'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRDBText5: TQRDBText
        Left = 647
        Top = 4
        Width = 64
        Height = 15
        Size.Values = (
          39.687500000000000000
          1711.854166666667000000
          10.583333333333330000
          169.333333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = Titulos
        DataField = 'MUAC'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    object ghServentiaA: TQRGroup
      Left = 38
      Top = 227
      Width = 718
      Height = 25
      AlignToBottom = False
      BeforePrint = ghServentiaABeforePrint
      Color = clScrollBar
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        66.145833333333330000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = True
      Expression = 'Titulos.DESCRICAO'
      FooterBand = gfServentiaA
      Master = Relatorio
      ReprintOnNewPage = False
      object QRDBText13: TQRDBText
        Left = 0
        Top = 4
        Width = 718
        Height = 19
        Size.Values = (
          50.270833333333330000
          0.000000000000000000
          10.583333333333330000
          1899.708333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clScrollBar
        DataSet = Titulos
        DataField = 'DESCRICAO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        VerticalAlignment = tlTop
        FontSize = 10
      end
    end
  end
  object Titulos: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT T.ID_ATO,'
      '       T.PROTOCOLO,'
      '       T.TOTAL,'
      '       T.VALOR_TITULO,'
      '       T.DEVEDOR,'
      '       T.STATUS,'
      '       T.FETJ,'
      '       (T.MUTUA + T.ACOTERJ) AS MUAC,'
      '       T.CARTORIO,'
      '       UPPER(SA.DESCRICAO) AS DESCRICAO'
      '  FROM TITULOS T'
      ' INNER JOIN EXPORTADO E'
      '    ON T.ARQUIVO_CARTORIO = E.NOME_ARQUIVO_EXP'
      ' INNER JOIN SERVENTIA_AGREGADA SA'
      '    ON T.CARTORIO = SA.CODIGO_DISTRIBUICAO'
      ' WHERE E.DATA_EXPORTACAO BETWEEN :DATA1 AND :DATA2'
      
        '   AND T.CARTORIO = (CASE WHEN (:CART1 = 0) THEN T.CARTORIO ELSE' +
        ' :CART2 END)'
      'GROUP BY SA.DESCRICAO,'
      '         T.CARTORIO,'
      '         T.PROTOCOLO,'
      '         T.ID_ATO,'
      '         T.PROTOCOLO,'
      '         T.TOTAL,'
      '         T.VALOR_TITULO,'
      '         T.DEVEDOR,'
      '         T.STATUS,'
      '         T.FETJ,'
      '         T.MUTUA,'
      '         T.ACOTERJ'
      'ORDER BY SA.DESCRICAO,'
      '         T.PROTOCOLO')
    Left = 32
    Top = 34
    ParamData = <
      item
        Name = 'DATA1'
        DataType = ftDateTime
        ParamType = ptInput
      end
      item
        Name = 'DATA2'
        DataType = ftDateTime
        ParamType = ptInput
      end
      item
        Name = 'CART1'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'CART2'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object TitulosID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object TitulosPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object TitulosTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      OnGetText = TitulosTOTALGetText
    end
    object TitulosVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
      Origin = 'VALOR_TITULO'
      OnGetText = TitulosVALOR_TITULOGetText
    end
    object TitulosDEVEDOR: TStringField
      FieldName = 'DEVEDOR'
      Origin = 'DEVEDOR'
      Size = 100
    end
    object TitulosSTATUS: TStringField
      FieldName = 'STATUS'
      Origin = 'STATUS'
    end
    object TitulosFETJ: TFloatField
      FieldName = 'FETJ'
      Origin = 'FETJ'
      OnGetText = TitulosFETJGetText
    end
    object TitulosMUAC: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'MUAC'
      Origin = 'MUAC'
      ProviderFlags = []
      ReadOnly = True
      OnGetText = TitulosMUACGetText
    end
    object TitulosCARTORIO: TIntegerField
      FieldName = 'CARTORIO'
      Origin = 'CARTORIO'
    end
    object TitulosDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
  end
end
