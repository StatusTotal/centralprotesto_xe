unit UQuickMovimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, DB, DBClient, SimpleDS,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFQuickMovimento = class(TForm)
    Relatorio: TQuickRep;
    QRBand1: TQRBand;
    lbCartorio: TQRLabel;
    lbEndereco: TQRLabel;
    lbPeriodo: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel2: TQRLabel;
    QRBand3: TQRBand;
    QRBand2: TQRBand;
    QRDBText2: TQRDBText;
    QRLabel7: TQRLabel;
    lbTabeliao: TQRLabel;
    qrRelacao: TQRLabel;
    QRDBText7: TQRDBText;
    QRLabel1: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRDBText3: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText5: TQRDBText;
    QRLabel9: TQRLabel;
    qrData: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText6: TQRDBText;
    QRLabel11: TQRLabel;
    qrSelo: TQRDBText;
    qrPortador: TQRLabel;
    QRDBText8: TQRDBText;
    QRLabel8: TQRLabel;
    QRSysData2: TQRSysData;
    QRExpr4: TQRExpr;
    QRSysData1: TQRSysData;
    QRLabel12: TQRLabel;
    QRDBText9: TQRDBText;
    QRExpr1: TQRExpr;
    QRLabel13: TQRLabel;
    QRDBText10: TQRDBText;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    qrAleatorio: TQRDBText;
    lbConvenio: TQRLabel;
    ChildBand2: TQRChildBand;
    QRLabel14: TQRLabel;
    lbAssinatura: TQRLabel;
    lbData: TQRLabel;
    lbRG: TQRLabel;
    lbCheque: TQRLabel;
    Titulos: TFDQuery;
    TitulosAPRESENTANTE: TStringField;
    TitulosPROTOCOLO: TIntegerField;
    TitulosDEVEDOR: TStringField;
    TitulosCPF_CNPJ_DEVEDOR: TStringField;
    TitulosTIPO_TITULO: TIntegerField;
    TitulosNUMERO_TITULO: TStringField;
    TitulosSACADOR: TStringField;
    TitulosDT_PROTOCOLO: TDateField;
    TitulosDT_REGISTRO: TDateField;
    TitulosDT_PAGAMENTO: TDateField;
    TitulosVALOR_TITULO: TFloatField;
    TitulosTARIFA_BANCARIA: TFloatField;
    TitulosDT_SUSTADO: TDateField;
    TitulosDT_RETIRADO: TDateField;
    TitulosDT_DEVOLVIDO: TDateField;
    TitulosTOTAL: TFloatField;
    TitulosSELO_REGISTRO: TStringField;
    TitulosSELO_PAGAMENTO: TStringField;
    TitulosSTATUS: TStringField;
    TitulosSALDO_PROTESTO: TFloatField;
    TitulosCONVENIO: TStringField;
    TitulosDISTRIBUICAO: TFloatField;
    TitulosSALDO_TITULO: TFloatField;
    TitulosALEATORIO_PROTESTO: TStringField;
    TitulosALEATORIO_SOLUCAO: TStringField;
    TitulosDocumento: TStringField;
    TitulosNatureza: TStringField;
    TitulosCustas: TFloatField;
    TitulosSomaCustas: TFloatField;
    procedure FormCreate(Sender: TObject);
    procedure RelatorioEndPage(Sender: TCustomQuickRep);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FDQuery1CalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FQuickMovimento: TFQuickMovimento;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFQuickMovimento.FDQuery1CalcFields(DataSet: TDataSet);
begin
  if Length(TitulosCPF_CNPJ_DEVEDOR.AsString)=11 then
    TitulosDocumento.AsString:=PF.FormatarCPF(TitulosCPF_CNPJ_DEVEDOR.AsString)
      else
        if Length(TitulosCPF_CNPJ_DEVEDOR.AsString)=14 then
          TitulosDocumento.AsString:=PF.FormatarCNPJ(TitulosCPF_CNPJ_DEVEDOR.AsString)
            else TitulosDocumento.AsString:='';

  TitulosNatureza.AsString:=PF.RetornarSigla(TitulosTIPO_TITULO.AsInteger)+'-'+TitulosNUMERO_TITULO.AsString;

  TitulosCustas.AsFloat:=TitulosTOTAL.AsFloat-TitulosDISTRIBUICAO.AsFloat-TitulosTARIFA_BANCARIA.AsFloat;

  if TitulosCONVENIO.AsString='N' then TitulosSomaCustas.AsFloat:=TitulosSomaCustas.AsFloat+TitulosCustas.AsFloat;
end;

procedure TFQuickMovimento.FormCreate(Sender: TObject);
begin
  lbCartorio.Caption:=dm.ServentiaDESCRICAO.AsString;
  lbTabeliao.Caption:=dm.ServentiaTABELIAO.AsString;
  lbEndereco.Caption:=dm.ServentiaENDERECO.AsString;

  lbAssinatura.Enabled  :=dm.ServentiaCODIGO.AsInteger=1726;
  lbData.Enabled        :=dm.ServentiaCODIGO.AsInteger=1726;
  lbRG.Enabled          :=dm.ServentiaCODIGO.AsInteger=1726;
  lbCheque.Enabled      :=dm.ServentiaCODIGO.AsInteger=1726;
end;

procedure TFQuickMovimento.RelatorioEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickMovimento.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbConvenio.Enabled:=TitulosCONVENIO.AsString='S';
end;

end.
