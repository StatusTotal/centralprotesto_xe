unit UImportarDistribuidor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, Mask, sMaskEdit, sCustomComboEdit,
  sTooledit, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, sPanel, DB, ComCtrls,
  acProgressBar, sDBDateEdit, DBCtrls, sDBEdit, RxMemDS, sDialogs, sEdit,
  sCurrEdit, sCurrencyEdit, FMTBcd, SqlExpr, sDBText, sLabel,
  Menus, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFImportarDistribuidor = class(TForm)
    P1: TsPanel;
    P4: TsPanel;
    Grid: TwwDBGrid;
    edData: TsDateEdit;
    btArquivo: TsBitBtn;
    btProcessar: TsBitBtn;
    Opd: TsOpenDialog;
    RX: TRxMemoryData;
    P2: TsPanel;
    edDevedor: TsDBEdit;
    edDataTitulo: TsDBDateEdit;
    edValor: TsDBEdit;
    edEspecie: TsDBEdit;
    edDataVencimento: TsDBDateEdit;
    edDocumento: TsDBEdit;
    edEndereco: TsDBEdit;
    edAgencia: TsDBEdit;
    edUF: TsDBEdit;
    edCidade: TsDBEdit;
    edPraca: TsDBEdit;
    PB: TsProgressBar;
    dsRX: TDataSource;
    RXCodigoPortador: TIntegerField;
    RXPortador: TStringField;
    RXAgenciaCedente: TStringField;
    RXNossoNumero: TStringField;
    RXSacador: TStringField;
    RXCedente: TStringField;
    RXNatureza: TStringField;
    RXNumeroTitulo: TStringField;
    RXDataEmissao: TDateField;
    RXDataVencimento: TDateField;
    RXValorTitulo: TFloatField;
    RXSaldoTitulo: TFloatField;
    RXPracaPagamento: TStringField;
    RXEndosso: TStringField;
    RXAceite: TStringField;
    RXDevedor: TStringField;
    RXTipoDevedor: TStringField;
    RXDocDevedor: TStringField;
    RXIdentidadeDevedor: TStringField;
    RXEndereco: TStringField;
    RXCidade: TStringField;
    RXUF: TStringField;
    RXProtocolo: TStringField;
    RXNumeroDistribuicao: TStringField;
    M: TMemo;
    edQtd: TsCurrencyEdit;
    edNossoNumero: TsDBEdit;
    RXIrregularidade: TStringField;
    RXMotivoRejeicao: TStringField;
    P3: TsPanel;
    sLabel1: TsLabel;
    txMotivo: TsDBText;
    btIrregularidades: TsBitBtn;
    RXRetirado: TBooleanField;
    PM: TPopupMenu;
    Aceitar1: TMenuItem;
    Rejeitar1: TMenuItem;
    RXSelo: TStringField;
    btSelo: TsBitBtn;
    RXAVista: TStringField;
    RXValorDistribuicao: TFloatField;
    edDistribuicao: TsDBEdit;
    edSaldo: TsDBEdit;
    RXCEPDevedor: TStringField;
    edCEP: TsDBEdit;
    Duplicado: TFDQuery;
    Faixa: TFDQuery;
    DuplicadoID_ATO: TIntegerField;
    DuplicadoCODIGO: TIntegerField;
    DuplicadoRECIBO: TIntegerField;
    DuplicadoDT_ENTRADA: TDateField;
    DuplicadoDT_PROTOCOLO: TDateField;
    DuplicadoPROTOCOLO: TIntegerField;
    DuplicadoLIVRO_PROTOCOLO: TIntegerField;
    DuplicadoFOLHA_PROTOCOLO: TStringField;
    DuplicadoDT_PRAZO: TDateField;
    DuplicadoDT_REGISTRO: TDateField;
    DuplicadoREGISTRO: TIntegerField;
    DuplicadoLIVRO_REGISTRO: TIntegerField;
    DuplicadoFOLHA_REGISTRO: TStringField;
    DuplicadoSELO_REGISTRO: TStringField;
    DuplicadoDT_PAGAMENTO: TDateField;
    DuplicadoSELO_PAGAMENTO: TStringField;
    DuplicadoRECIBO_PAGAMENTO: TIntegerField;
    DuplicadoCOBRANCA: TStringField;
    DuplicadoEMOLUMENTOS: TFloatField;
    DuplicadoFETJ: TFloatField;
    DuplicadoFUNDPERJ: TFloatField;
    DuplicadoFUNPERJ: TFloatField;
    DuplicadoFUNARPEN: TFloatField;
    DuplicadoPMCMV: TFloatField;
    DuplicadoISS: TFloatField;
    DuplicadoMUTUA: TFloatField;
    DuplicadoDISTRIBUICAO: TFloatField;
    DuplicadoACOTERJ: TFloatField;
    DuplicadoTOTAL: TFloatField;
    DuplicadoTIPO_PROTESTO: TIntegerField;
    DuplicadoTIPO_TITULO: TIntegerField;
    DuplicadoNUMERO_TITULO: TStringField;
    DuplicadoDT_TITULO: TDateField;
    DuplicadoBANCO: TStringField;
    DuplicadoAGENCIA: TStringField;
    DuplicadoCONTA: TStringField;
    DuplicadoVALOR_TITULO: TFloatField;
    DuplicadoSALDO_PROTESTO: TFloatField;
    DuplicadoDT_VENCIMENTO: TDateField;
    DuplicadoCONVENIO: TStringField;
    DuplicadoTIPO_APRESENTACAO: TStringField;
    DuplicadoDT_ENVIO: TDateField;
    DuplicadoCODIGO_APRESENTANTE: TStringField;
    DuplicadoTIPO_INTIMACAO: TStringField;
    DuplicadoMOTIVO_INTIMACAO: TMemoField;
    DuplicadoDT_INTIMACAO: TDateField;
    DuplicadoDT_PUBLICACAO: TDateField;
    DuplicadoVALOR_PAGAMENTO: TFloatField;
    DuplicadoAPRESENTANTE: TStringField;
    DuplicadoCPF_CNPJ_APRESENTANTE: TStringField;
    DuplicadoTIPO_APRESENTANTE: TStringField;
    DuplicadoCEDENTE: TStringField;
    DuplicadoNOSSO_NUMERO: TStringField;
    DuplicadoSACADOR: TStringField;
    DuplicadoDEVEDOR: TStringField;
    DuplicadoCPF_CNPJ_DEVEDOR: TStringField;
    DuplicadoTIPO_DEVEDOR: TStringField;
    DuplicadoAGENCIA_CEDENTE: TStringField;
    DuplicadoPRACA_PROTESTO: TStringField;
    DuplicadoTIPO_ENDOSSO: TStringField;
    DuplicadoACEITE: TStringField;
    DuplicadoCPF_ESCREVENTE: TStringField;
    DuplicadoCPF_ESCREVENTE_PG: TStringField;
    DuplicadoOBSERVACAO: TMemoField;
    DuplicadoDT_SUSTADO: TDateField;
    DuplicadoDT_RETIRADO: TDateField;
    DuplicadoSTATUS: TStringField;
    DuplicadoPROTESTADO: TStringField;
    DuplicadoENVIADO_APONTAMENTO: TStringField;
    DuplicadoENVIADO_PROTESTO: TStringField;
    DuplicadoENVIADO_PAGAMENTO: TStringField;
    DuplicadoENVIADO_RETIRADO: TStringField;
    DuplicadoENVIADO_SUSTADO: TStringField;
    DuplicadoENVIADO_DEVOLVIDO: TStringField;
    DuplicadoEXPORTADO: TStringField;
    DuplicadoAVALISTA_DEVEDOR: TStringField;
    DuplicadoFINS_FALIMENTARES: TStringField;
    DuplicadoTARIFA_BANCARIA: TFloatField;
    DuplicadoFORMA_PAGAMENTO: TStringField;
    DuplicadoNUMERO_PAGAMENTO: TStringField;
    DuplicadoARQUIVO: TStringField;
    DuplicadoRETORNO: TStringField;
    DuplicadoDT_DEVOLVIDO: TDateField;
    DuplicadoCPF_CNPJ_SACADOR: TStringField;
    DuplicadoID_MSG: TIntegerField;
    DuplicadoVALOR_AR: TFloatField;
    DuplicadoELETRONICO: TStringField;
    DuplicadoIRREGULARIDADE: TIntegerField;
    DuplicadoAVISTA: TStringField;
    DuplicadoSALDO_TITULO: TFloatField;
    DuplicadoTIPO_SUSTACAO: TStringField;
    DuplicadoRETORNO_PROTESTO: TStringField;
    DuplicadoDT_RETORNO_PROTESTO: TDateField;
    DuplicadoDT_DEFINITIVA: TDateField;
    DuplicadoJUDICIAL: TStringField;
    DuplicadoALEATORIO_PROTESTO: TStringField;
    DuplicadoALEATORIO_SOLUCAO: TStringField;
    DuplicadoCCT: TStringField;
    DuplicadoDETERMINACAO: TStringField;
    DuplicadoANTIGO: TStringField;
    DuplicadoLETRA: TStringField;
    DuplicadoPROTOCOLO_CARTORIO: TIntegerField;
    DuplicadoARQUIVO_CARTORIO: TStringField;
    DuplicadoRETORNO_CARTORIO: TStringField;
    DuplicadoDT_DEVOLVIDO_CARTORIO: TDateField;
    DuplicadoCARTORIO: TIntegerField;
    DuplicadoDT_PROTOCOLO_CARTORIO: TDateField;
    FaixaCOD: TIntegerField;
    FaixaEMOL: TFloatField;
    FaixaFETJ: TFloatField;
    FaixaFUND: TFloatField;
    FaixaFUNP: TFloatField;
    FaixaMUTUA: TFloatField;
    FaixaACOTERJ: TFloatField;
    FaixaDISTRIB: TFloatField;
    FaixaTOT: TFloatField;
    FaixaTITULO: TStringField;
    function Texto(Linha, Inicio, Fim: Integer): String;
    procedure btArquivoClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Criticas;
    procedure btProcessarClick(Sender: TObject);
    procedure btIrregularidadesClick(Sender: TObject);
    procedure dsRXDataChange(Sender: TObject; Field: TField);
    procedure GridMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure P1MouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure GridMouseUp(Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Integer);
    procedure RXRetiradoChange(Sender: TField);
    procedure RXBeforePost(DataSet: TDataSet);
    procedure MarcarTodos;
    procedure DesmarcarTodos;
    procedure Aceitar1Click(Sender: TObject);
    procedure Rejeitar1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btSeloClick(Sender: TObject);
    procedure GridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FImportarDistribuidor: TFImportarDistribuidor;

implementation

uses UDM, UPF, UCriticas, UIrregularidades, USelo, UGeral;

{$R *.dfm}

procedure TFImportarDistribuidor.DesmarcarTodos;
var
  Posicao: Integer;
begin
  RX.DisableControls;
  Posicao:=RX.RecNo;
  RX.First;
  while not RX.Eof do
  begin
      RX.Edit;
      RXRetirado.AsBoolean:=False;
      RX.Next;
  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;
end;

procedure TFImportarDistribuidor.MarcarTodos;
var
  Posicao: Integer;
begin
  RX.DisableControls;
  Posicao:=RX.RecNo;
  RX.First;
  while not RX.Eof do
  begin
      RX.Edit;
      RXRetirado.AsBoolean:=True;
      RX.Next;
  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;
end;

function TFImportarDistribuidor.Texto(Linha, Inicio, Fim: Integer): String;
begin
  Result:=Trim(Copy(M.Lines[Linha],Inicio,Fim));
end;

procedure TFImportarDistribuidor.btArquivoClick(Sender: TObject);
var
  I: Integer;
  Vencimento: String;
begin
  Opd.InitialDir:=dm.ValorParametro(2);
  if Opd.Execute then
  begin
      try
          M.Lines.Clear;
          M.Lines.LoadFromFile(Opd.FileName);
          {APAGANDO AS LINHAS EM BRANCO}
          for I := 0 to M.Lines.Count-1 do
            if M.Lines[I]='' then
              M.Lines.Delete(I);
          edData.Date:=StrToDate(Texto(0,2,2)+'/'+Texto(0,4,2)+'/20'+Texto(0,6,2));
          edQtd.Value:=StrToInt(Texto(0,27,4));
          {CARREGANDO O RX}
          RX.Close;
          RX.Open;
          for I := 0 to M.Lines.Count-1 do
          begin
              if Texto(I,1,1)='1' then
              begin
                  RX.Append;
                  if Texto(I,2,3)<>'' then
                  RXCodigoPortador.AsInteger    :=StrToInt(Texto(I,2,3));
                  RXPortador.AsString           :=Texto(I,362,45);
                  RXAgenciaCedente.AsString     :=Texto(I,5,15);
                  RXNossoNumero.AsString        :=Texto(I,20,15);
                  RXSacador.AsString            :=Texto(I,35,45);
                  RXCedente.AsString            :=Texto(I,80,45);
                  RXNatureza.AsString           :=Texto(I,125,3);
                  RXNumeroTitulo.AsString       :=Texto(I,128,11);
                  RXDataEmissao.AsDateTime      :=StrToDate(Texto(I,139,2)+'/'+Texto(I,141,2)+'/20'+Texto(I,143,2));
                  Vencimento                    :=Texto(I,145,2)+'/'+Texto(I,147,2)+'/20'+Texto(I,149,2);
                  if Vencimento='99/99/2099' then
                  begin
                      RXAVista.AsString:='S';
                      RXDataVencimento.AsDateTime:=RXDataEmissao.AsDateTime;
                  end
                  else
                  begin
                      RXAVista.AsString:='N';
                      RXDataVencimento.AsDateTime:=StrToDate(Texto(I,145,2)+'/'+Texto(I,147,2)+'/20'+Texto(I,149,2));
                  end;
                  if Texto(I,154,14)<>'' then RXValorTitulo.AsFloat:=PF.TextoParaFloat(Texto(I,154,14));
                  if Texto(I,168,14)<>'' then RXSaldoTitulo.AsFloat:=PF.TextoParaFloat(Texto(I,168,14));
                  RXPracaPagamento.AsString     :=Texto(I,182,20);
                  RXEndosso.AsString            :=Texto(I,202,1);
                  RXAceite.AsString             :=Texto(I,203,1);
                  RXDevedor.AsString            :=Texto(I,205,45);
                  if Texto(I,250,3)='CGC' then RXTipoDevedor.AsString:='J' else RXTipoDevedor.AsString:='F';
                  RXDocDevedor.AsString         :=Texto(I,253,14);
                  if RXTipoDevedor.AsString='F' then RXDocDevedor.AsString:=Copy(RXDocDevedor.AsString,4,11);
                  RXIdentidadeDevedor.AsString  :=Texto(I,267,11);
                  RXEndereco.AsString           :=Texto(I,278,45);
                  RXCEPDevedor.AsString         :=Texto(I,323,8);
                  RXCidade.AsString             :=Texto(I,331,20);
                  RXUF.AsString                 :=Texto(I,351,2);
                  RXProtocolo.AsString          :=Texto(I,355,7);
                  if Texto(I,407,10)<>'' then RXValorDistribuicao.AsFloat:=PF.TextoParaFloat(Texto(I,407,10));
                  RXNumeroDistribuicao.AsString :=Texto(I,417,7);

                  if RXCodigoPortador.AsString<>'' then
                  begin
                      dm.Portadores.Close;
                      dm.Portadores.CommandText:='SELECT * FROM PORTADORES WHERE CODIGO ='+QuotedStr(RXCodigoPortador.AsString);
                      dm.Portadores.Open;
                      if dm.Portadores.IsEmpty then
                      begin
                          dm.Portadores.Append;
                          dm.PortadoresCODIGO.AsInteger   :=RXCodigoPortador.AsInteger;
                          dm.PortadoresNOME.AsString      :=RXPortador.AsString;
                          dm.PortadoresTIPO.AsString      :='J';
                          dm.PortadoresBANCO.AsString     :=UpperCase(InputBox(dm.PortadoresNOME.AsString,'Banco? (S/N)','S'));;
                          dm.PortadoresCONVENIO.AsString  :=UpperCase(InputBox(dm.PortadoresNOME.AsString,'Conv�nio? (S/N)','N'));
                          if (dm.PortadoresCodigo.AsInteger=237) or (dm.PortadoresCodigo.AsInteger=341) then
                            dm.PortadoresCRA.AsString:='S'
                              else dm.PortadoresCRA.AsString:='N';
                          dm.Portadores.Post;
                          dm.Portadores.ApplyUpdates(0);
                          RXCodigoPortador.AsInteger:=dm.PortadoresCODIGO.AsInteger;
                      end;
                  end;
                  RX.Post;
              end;
          end;
          RX.First;
          RX.SortOnFields('PROTOCOLO');
          Criticas;
      except
        on E: Exception do
        begin
            PF.Aguarde(False);
            GR.Aviso('Erro: '+E.Message+#13#13+'Protocolo: '+RXProtocolo.AsString);
        end;
      end;
  end;
end;

procedure TFImportarDistribuidor.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFImportarDistribuidor.Criticas;
var
  Posicao: Integer;
begin
  Screen.Cursor:=crHourGlass;

  dm.Criticas.Close;
  dm.Criticas.Open;

  RX.DisableControls;
  Posicao:=RX.RecNo;
  RX.First;
  while not RX.Eof do
  begin
      PF.Aguarde(True,'Verificando irregularidades...');
      if edData.Date<RXDataVencimento.AsDateTime then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXProtocolo.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=1;
          dm.CriticasDescricao.AsString        :='Data da apresenta��o inferior � data de vencimento';
          dm.Criticas.Post;

          RX.Edit;
          RXIrregularidade.AsString:='1';
          RXMotivoRejeicao.AsString:='(1) Data da apresenta��o inferior � data de vencimento';
          RXRetirado.AsBoolean     :=True;
          RX.Post;
      end;

      if RXDocDevedor.AsString='' then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXProtocolo.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=7;
          dm.CriticasDescricao.AsString        :='CNPJ/CPF do sacado inv�lido/incorreto';
          dm.Criticas.Post;

          RX.Edit;
          RXIrregularidade.AsString:='7';
          RXMotivoRejeicao.AsString:='(7) CNPJ/CPF do sacado inv�lido/incorreto';
          RXRetirado.AsBoolean     :=True;
          RX.Post;
      end;

      if Trim(GR.PegarNumeroTexto(RXEndereco.AsString))='' then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXProtocolo.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=40;
          dm.CriticasDescricao.AsString        :='Rua/N�mero inexistente no endere�o';
          dm.Criticas.Post;

          RX.Edit;
          RXIrregularidade.AsString:='40';
          RXMotivoRejeicao.AsString:='(40) Rua/N�mero inexistente no endere�o';
          RXRetirado.AsBoolean     :=True;
          RX.Post;
      end;

      if Trim(GR.PegarNumeroTexto(RXCedente.AsString))<>'' then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXProtocolo.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=4;
          dm.CriticasDescricao.AsString        :='Nome do Cedente incompleto/incorreto';
          dm.Criticas.Post;

          RX.Edit;
          RXIrregularidade.AsString:='4';
          RXMotivoRejeicao.AsString:='(4) Nome do Cedente incompleto/incorreto';
          RXRetirado.AsBoolean     :=True;
          RX.Post;
      end;

      if Trim(GR.PegarNumeroTexto(RXSacador.AsString))<>'' then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXProtocolo.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=5;
          dm.CriticasDescricao.AsString        :='Nome do Sacador incompleto/incorreto';
          dm.Criticas.Post;

          RX.Edit;
          RXIrregularidade.AsString:='5';
          RXMotivoRejeicao.AsString:='(5) Nome do Sacador incompleto/incorreto';
          RXRetirado.AsBoolean     :=True;
          RX.Post;
      end;

      if RXTipoDevedor.AsString='F' then
        if not PF.VerificarCPF(RXDocDevedor.AsString) then
        begin
            dm.Criticas.Append;
            dm.CriticasID.AsInteger              :=RX.RecNo;
            dm.CriticasNumero_Titulo.AsString    :=RXProtocolo.AsString;
            dm.CriticasCodigo_Critica.AsInteger  :=7;
            dm.CriticasDescricao.AsString        :='CNPJ/CPF do sacado inv�lido/incorreto';
            dm.Criticas.Post;

            RX.Edit;
            RXIrregularidade.AsString:='7';
            RXMotivoRejeicao.AsString:='(7) CNPJ/CPF do sacado inv�lido/incorreto';
            RXRetirado.AsBoolean     :=True;
            RX.Post;
        end;

      if RXTipoDevedor.AsString='J' then
        if not PF.VerificarCNPJ(RXDocDevedor.AsString) then
        begin
            dm.Criticas.Append;
            dm.CriticasID.AsInteger              :=RX.RecNo;
            dm.CriticasNumero_Titulo.AsString    :=RXProtocolo.AsString;
            dm.CriticasCodigo_Critica.AsInteger  :=7;
            dm.CriticasDescricao.AsString        :='CNPJ/CPF do sacado inv�lido/incorreto';
            dm.Criticas.Post;

            RX.Edit;
            RXIrregularidade.AsString:='7';
            RXMotivoRejeicao.AsString:='(7) CNPJ/CPF do sacado inv�lido/incorreto';
            RXRetirado.AsBoolean     :=True;
            RX.Post;
        end;

      if RXPracaPagamento.AsString<>Copy(dm.ValorParametro(6),1,20) then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXProtocolo.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=15;
          dm.CriticasDescricao.AsString        :='Pra�a de pagamento incompat�vel ('+RXPracaPagamento.AsString+')';
          dm.Criticas.Post;

          RX.Edit;
          RXIrregularidade.AsString:='15';
          RXMotivoRejeicao.AsString:='(15) Pra�a de pagamento incompat�vel ('+RXPracaPagamento.AsString+')';
          RXRetirado.AsBoolean     :=True;
          RX.Post;
      end;

      if RXNumeroTitulo.AsString='' then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXProtocolo.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=16;
          dm.CriticasDescricao.AsString        :='Falta n�mero do t�tulo';
          dm.Criticas.Post;

          RX.Edit;
          RXIrregularidade.AsString:='16';
          RXMotivoRejeicao.AsString:='(16) Falta n�mero do t�tulo';
          RXRetirado.AsBoolean     :=True;
          RX.Post;
      end;

      if RXDataEmissao.AsDateTime=0 then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXProtocolo.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=18;
          dm.CriticasDescricao.AsString        :='Falta data de emiss�o do t�tulo';
          dm.Criticas.Post;

          RX.Edit;
          RXIrregularidade.AsString:='18';
          RXMotivoRejeicao.AsString:='(18) Falta data de emiss�o do t�tulo';
          RXRetirado.AsBoolean     :=True;
          RX.Post;
      end;

      if RXDataEmissao.AsDateTime>RXDataVencimento.AsDateTime then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXProtocolo.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=20;
          dm.CriticasDescricao.AsString        :='Data de emiss�o posterior ao vencimento';
          dm.Criticas.Post;

          RX.Edit;
          RXIrregularidade.AsString:='20';
          RXMotivoRejeicao.AsString:='(20) Data de emiss�o posterior ao vencimento';
          RXRetirado.AsBoolean     :=True;
          RX.Post;
      end;

      if RXDEVEDOR.AsString=RXSACADOR.AsString then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXProtocolo.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=28;
          dm.CriticasDescricao.AsString        :='Sacado e Sacador/Avalista s�o a mesma pessoa';
          dm.Criticas.Post;

          RX.Edit;
          RXIrregularidade.AsString:='28';
          RXMotivoRejeicao.AsString:='(28) Sacado e Sacador/Avalista s�o a mesma pessoa';
          RXRetirado.AsBoolean     :=True;
          RX.Post;
      end;

      if RXDataVencimento.AsDateTime=0 then
      begin
          dm.Criticas.Append;
          dm.CriticasID.AsInteger              :=RX.RecNo;
          dm.CriticasNumero_Titulo.AsString    :=RXProtocolo.AsString;
          dm.CriticasCodigo_Critica.AsInteger  :=33;
          dm.CriticasDescricao.AsString        :='Falta data de vencimento do t�tulo';
          dm.Criticas.Post;

          RX.Edit;
          RXIrregularidade.AsString:='33';
          RXMotivoRejeicao.AsString:='(33) Falta data de vencimento do t�tulo';
          RXRetirado.AsBoolean     :=True;
          RX.Post;
      end;

      {VERIFICANDO SE N�MERO DO T�TULO J� FOI APONTADO PARA ESSE PORTADOR}
      if RXCodigoPortador.AsInteger<>0 then
      begin
          Duplicado.Close;
          Duplicado.Params[0].AsString  :=RXNumeroTitulo.AsString;
          Duplicado.Params[1].AsInteger :=RXCodigoPortador.AsInteger;
          Duplicado.Params[2].AsDate    :=edData.Date;
          Duplicado.Open;
          if not Duplicado.IsEmpty then
          begin
              dm.Criticas.Append;
              dm.CriticasID.AsInteger              :=RX.RecNo;
              dm.CriticasNumero_Titulo.AsString    :=RXProtocolo.AsString;
              dm.CriticasCodigo_Critica.AsInteger  :=52;
              dm.CriticasDescricao.AsString        :='T�tulo apresentado em duplicidade';
              dm.Criticas.Post;

              RX.Edit;
              RXIrregularidade.AsString:='52';
              RXMotivoRejeicao.AsString:='(52) T�tulo apresentado em duplicidade';
              RXRetirado.AsBoolean     :=True;
              RX.Post;
          end;
      end;

      RX.Next;
  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;

  PF.Aguarde(False);
  Screen.Cursor:=crDefault;

  dm.Criticas.First;
  if not dm.Criticas.IsEmpty then
    GR.CriarForm(TFCriticas,FCriticas)
      else GR.Aviso('Nenhuma irregularidade encontrada.');
end;

procedure TFImportarDistribuidor.btProcessarClick(Sender: TObject);
var
  Posicao,Marcados: Integer;
begin
  if RX.IsEmpty then Exit;
  if not GR.Pergunta('Confirma��o:'#13#13+'Total de T�tulos: '+IntToStr(RX.RecordCount)+#13#13+'Processar?') then Exit;

  dm.vCalc := False;

  try
    if dm.conSISTEMA.Connected then
      dm.conSISTEMA.StartTransaction;

    dm.Custas.Close;
    dm.Custas.Params[0].AsInteger:=-1;
    dm.Custas.Open;

    dm.Movimento.Close;
    dm.Movimento.Params[0].AsInteger:=-1;
    dm.Movimento.Open;

    dm.Devedores.Close;
    dm.Devedores.Params[0].AsInteger:=-1;
    dm.Devedores.Open;

    dm.Sacadores.Close;
    dm.Sacadores.Params[0].AsInteger:=-1;
    dm.Sacadores.Open;

    PF.Aguarde(True);

    RX.DisableControls;
    Posicao:=RX.RecNo;
    RX.First;

    dm.Titulos.Close;
    dm.Titulos.Open;

    PB.Max:=RX.RecordCount;
    PB.Position:=0;
    PB.Visible:=True;
    while not RX.Eof do
    begin
        Application.ProcessMessages;
        PB.Position:=PB.Position+1;

        dm.Titulos.Append;
        dm.TitulosID_ATO.AsInteger                :=dm.IdAtual('ID_ATO','S');
        dm.TitulosDT_PROTOCOLO.AsDateTime         :=edData.Date;
        dm.TitulosDT_ENTRADA.AsDateTime           :=edData.Date;
        dm.TitulosDT_PRAZO.AsDateTime             :=PF.DataFinalFeriados(dm.TitulosDT_PROTOCOLO.AsDateTime,StrToInt(dm.ValorParametro(5)));
        dm.TitulosPROTOCOLO.AsInteger             :=RXProtocolo.AsInteger;
        dm.TitulosLIVRO_PROTOCOLO.AsInteger       :=dm.ValorAtual('LPR','N');
        dm.TitulosFOLHA_PROTOCOLO.AsString        :=IntToStr(dm.ValorAtual('FPR','S'));
        dm.TitulosCOBRANCA.AsString               :='CC';
        dm.TitulosTIPO_PROTESTO.AsInteger         :=1;
        dm.TitulosTIPO_TITULO.AsInteger           :=PF.RetornarEspecie(RXNatureza.AsString);
        dm.TitulosNUMERO_TITULO.AsString          :=RXNumeroTitulo.AsString;
        dm.TitulosDT_TITULO.AsDateTime            :=RXDataEmissao.AsDateTime;
        dm.TitulosVALOR_TITULO.AsFloat            :=RXSaldoTitulo.AsFloat; //RXValorTitulo.AsFloat;
        dm.TitulosDT_VENCIMENTO.AsDateTime        :=RXDataVencimento.AsDateTime;
        dm.TitulosSELO_PAGAMENTO.AsString         :=RXSelo.AsString;
        dm.TitulosAVISTA.AsString                 :=RXAVista.AsString;
        dm.TitulosSALDO_TITULO.AsFloat            :=RXSaldoTitulo.AsFloat;

        if not RXCodigoPortador.IsNull then
        begin
            dm.Portadores.Close;
            dm.Portadores.CommandText:='SELECT * FROM PORTADORES WHERE CODIGO='+QuotedStr(RXCodigoPortador.AsString);
            dm.Portadores.Open;
            dm.TitulosAPRESENTANTE.AsString           :=dm.PortadoresNOME.AsString;
            dm.TitulosCPF_CNPJ_APRESENTANTE.AsString  :=dm.PortadoresDOCUMENTO.AsString;
            dm.TitulosTIPO_APRESENTANTE.AsString      :=dm.PortadoresTIPO.AsString;
            if dm.PortadoresCODIGO.AsInteger<>0 then  dm.TitulosCODIGO_APRESENTANTE.AsInteger:=dm.PortadoresCODIGO.AsInteger;
            dm.TitulosCONVENIO.AsString               :=dm.PortadoresCONVENIO.AsString;
            dm.Portadores.Close;
        end
        else
        begin
            dm.TitulosCONVENIO.AsString    :='N'; 
            dm.TitulosAPRESENTANTE.AsString:=RXPortador.AsString;
        end;

        dm.TitulosCEDENTE.AsString                :=RXCedente.AsString;
        dm.TitulosNOSSO_NUMERO.AsString           :=RXNossoNumero.AsString;
        dm.TitulosSACADOR.AsString                :=RXSacador.AsString;
        dm.TitulosDEVEDOR.AsString                :=RXDevedor.AsString;
        dm.TitulosTIPO_DEVEDOR.AsString           :=RXTipoDevedor.AsString;
        dm.TitulosCPF_CNPJ_DEVEDOR.AsString       :=RXDocDevedor.AsString;
        dm.TitulosAGENCIA_CEDENTE.AsString        :=RXAgenciaCedente.AsString;
        dm.TitulosPRACA_PROTESTO.AsString         :=RXPracaPagamento.AsString;
        dm.TitulosTIPO_ENDOSSO.AsString           :=RXEndosso.AsString;
        dm.TitulosACEITE.AsString                 :=RXAceite.AsString;

        if RXRetirado.AsBoolean then
        begin
            if Trim(RXIrregularidade.AsString)<>'' then
            begin
                dm.TitulosSTATUS.AsString:='RETIRADO';
                dm.TitulosDT_RETIRADO.AsDateTime:=edData.Date;
                dm.TitulosIRREGULARIDADE.AsInteger:=RXIrregularidade.AsInteger;
            end
            else
                Raise Exception.Create('Faltou informar a irregularidade do t�tulo n� '+RXNumeroTitulo.AsString);
        end
        else dm.TitulosSTATUS.AsString:='APONTADO';

        dm.TitulosARQUIVO.AsString                :=ExtractFileName(Opd.FileName);
        dm.TitulosTIPO_APRESENTACAO.AsString      :='I';

        {BUSCANDO A FAIXA DA TABELA DE CUSTAS}
        Faixa.Close;
        Faixa.Params[0].AsString:=dm.TitulosCONVENIO.AsString;
        Faixa.Params[1].AsString:='N';
        Faixa.Params[2].AsString:='N';
        Faixa.Params[3].AsFloat :=dm.TitulosSALDO_TITULO.AsFloat;
        Faixa.Params[4].AsFloat :=dm.TitulosSALDO_TITULO.AsFloat;
        Faixa.Open;

        dm.Itens.Close;
        dm.Itens.Params[0].AsInteger:=FaixaCOD.AsInteger;
        dm.Itens.Open;
        dm.Itens.First;

        while not dm.Itens.Eof do
        begin
            dm.Custas.Append;
            dm.CustasID_CUSTA.AsInteger :=dm.IdAtual('ID_CUSTA','S');
            dm.CustasID_ATO.AsInteger   :=dm.TitulosID_ATO.AsInteger;
            dm.CustasTABELA.AsString    :=dm.ItensTAB.AsString;
            dm.CustasITEM.AsString      :=dm.ItensITEM.AsString;
            dm.CustasSUBITEM.AsString   :=dm.ItensSUB.AsString;
            dm.CustasVALOR.AsFloat      :=dm.ItensVALOR.AsFloat;
            dm.CustasQTD.AsString       :=dm.ItensQTD.AsString;
            dm.CustasTOTAL.AsFloat      :=dm.CustasVALOR.AsFloat*dm.CustasQTD.AsFloat;
            dm.Custas.Post;
            if dm.Custas.ApplyUpdates(0)>0 then
            Raise Exception.Create('ApplyUpdates dm.Custas');

            dm.TitulosEMOLUMENTOS.AsFloat :=dm.TitulosEMOLUMENTOS.AsFloat+dm.CustasTOTAL.AsFloat;
            dm.Itens.Next;
        end;

        dm.TitulosCODIGO.AsInteger        :=FaixaCOD.AsInteger;
        dm.TitulosFETJ.AsFloat            :=GR.NoRound(dm.TitulosEMOLUMENTOS.AsFloat*0.2,2);
        dm.TitulosFUNDPERJ.AsFloat        :=GR.NoRound(dm.TitulosEMOLUMENTOS.AsFloat*0.05,2);
        dm.TitulosFUNPERJ.AsFloat         :=GR.NoRound(dm.TitulosEMOLUMENTOS.AsFloat*0.05,2);
        dm.TitulosFUNARPEN.AsFloat        :=GR.NoRound(dm.TitulosEMOLUMENTOS.AsFloat*0.04,2);
        dm.TitulosPMCMV.AsFloat           :=GR.NoRound(dm.TitulosEMOLUMENTOS.AsFloat*0.02,2);
        dm.TitulosMUTUA.AsFloat           :=FaixaMUTUA.AsFloat;
        dm.TitulosACOTERJ.AsFloat         :=FaixaACOTERJ.AsFloat;
        dm.TitulosDISTRIBUICAO.AsFloat    :=RXValorDistribuicao.AsFloat;
        dm.TitulosVALOR_AR.AsFloat        :=StrToFloat(dm.ValorParametro(35));
        dm.TitulosTOTAL.AsFloat           :=dm.TitulosEMOLUMENTOS.AsFloat+
                                            dm.TitulosFETJ.AsFloat+
                                            dm.TitulosFUNDPERJ.AsFloat+
                                            dm.TitulosFUNPERJ.AsFloat+
                                            dm.TitulosFUNARPEN.AsFloat+
                                            dm.TitulosPMCMV.AsFloat+
                                            dm.TitulosMUTUA.AsFloat+
                                            dm.TitulosACOTERJ.AsFloat+
                                            dm.TitulosDISTRIBUICAO.AsFloat+
                                            StrToFloat(dm.ValorParametro(35));
        dm.TitulosSALDO_PROTESTO.AsFloat  :=dm.TitulosSALDO_TITULO.AsFloat+dm.TitulosTOTAL.AsFloat;
        dm.TitulosCPF_ESCREVENTE.AsString :=dm.vCPF;

        dm.Titulos.Post;
        if dm.Titulos.ApplyUpdates(0)>0 then
        Raise Exception.Create('ApplyUpdates dm.Titulos');

        dm.Movimento.Append;
        dm.MovimentoID_MOVIMENTO.AsInteger  :=dm.IdAtual('ID_MOVIMENTO','S');
        dm.MovimentoID_ATO.AsInteger        :=dm.TitulosID_ATO.AsInteger;
        dm.MovimentoDATA.AsDateTime         :=Now;
        dm.MovimentoHORA.AsDateTime         :=Time;
        dm.MovimentoBAIXA.AsDateTime        :=dm.TitulosDT_PROTOCOLO.AsDateTime;
        dm.MovimentoESCREVENTE.AsString     :=dm.vNome;
        dm.MovimentoSTATUS.AsString         :=dm.TitulosSTATUS.AsString;
        dm.Movimento.Post;
        if dm.Movimento.ApplyUpdates(0)>0 then
        Raise Exception.Create('ApplyUpdates dm.Movimento');

        {GRAVANDO O DEVEDOR}
        dm.Devedores.Append;
        dm.DevedoresID_DEVEDOR.AsInteger      :=dm.IdAtual('ID_DEVEDOR','S');
        dm.DevedoresID_ATO.AsInteger          :=dm.TitulosID_ATO.AsInteger;
        dm.DevedoresNOME.AsString             :=RXDEVEDOR.AsString;
        dm.DevedoresTIPO.AsString             :=RXTipoDevedor.AsString;
        dm.DevedoresDOCUMENTO.AsString        :=RXDocDevedor.AsString;
        dm.DevedoresIFP_DETRAN.AsString       :='O';
        dm.DevedoresIDENTIDADE.AsString       :=RXIdentidadeDevedor.AsString;
        dm.DevedoresORGAO.AsString            :='';
        dm.DevedoresENDERECO.AsString         :=RXEndereco.AsString;
        dm.DevedoresMUNICIPIO.AsString        :=RXCidade.AsString;
        dm.DevedoresUF.AsString               :=RXUF.AsString;
        dm.DevedoresJUSTIFICATIVA.AsString    :='';
        dm.DevedoresIGNORADO.AsString         :='N';
        dm.DevedoresCEP.AsString              :=RXCEPDevedor.AsString;

        dm.Devedores.Post;
        if dm.Devedores.ApplyUpdates(0)>0 then
        Raise Exception.Create('ApplyUpdates dm.Devedores');

        {GRAVANDO O SACADOR}
        dm.Sacadores.Append;
        dm.SacadoresID_SACADOR.AsInteger      :=dm.IdAtual('ID_SACADOR','S');
        dm.SacadoresID_ATO.AsInteger          :=dm.TitulosID_ATO.AsInteger;
        dm.SacadoresNOME.AsString             :=RXSacador.AsString;
        dm.Sacadores.Post;
        if dm.Sacadores.ApplyUpdates(0)>0 then
        Raise Exception.Create('ApplyUpdates dm.Sacadores');

        RX.Next;
    end;

    RX.RecNo:=Posicao;
    RX.EnableControls;

    dm.Importados.Close;
    dm.Importados.Open;
    dm.Importados.Append;
    dm.ImportadosID_IMPORTADO.AsInteger :=dm.IdAtual('ID_IMPORTADO','S');
    dm.ImportadosARQUIVO.AsString       :=ExtractFileName(opd.FileName);
    dm.ImportadosDATA.AsDateTime        :=Now;
    dm.ImportadosTITULOS.AsInteger      :=RX.RecordCount;
    dm.ImportadosACEITOS.AsInteger      :=Marcados;
    dm.ImportadosREJEITADOS.AsInteger   :=Rx.RecordCount-Marcados;
    dm.Importados.Post;
    dm.Importados.ApplyUpdates(0);

    PF.Aguarde(False);

    RX.Close;

    if dm.conSISTEMA.InTransaction then
      dm.conSISTEMA.Commit;

    GR.Aviso('Arquivo processado com sucesso.');
    PB.Visible:=False;
  except
      on E:Exception do
      begin
          PB.Visible:=False;

          if dm.conSISTEMA.InTransaction then
            dm.conSISTEMA.Rollback;

          PF.Aguarde(False);
          GR.Aviso('Erro: '+E.Message);
      end;
  end;
end;

procedure TFImportarDistribuidor.btIrregularidadesClick(Sender: TObject);
begin
  if RX.IsEmpty then Exit;
  dm.vOkGeral:=False;
  GR.CriarForm(TFIrregularidades,FIrregularidades);

  if dm.vOkGeral then
  begin
      RX.Edit;
      RXIrregularidade.AsString:=dm.IrregularidadesCODIGO.AsString;
      RXMotivoRejeicao.AsString:='('+dm.IrregularidadesCODIGO.AsString+') '+dm.IrregularidadesMOTIVO.AsString;
  end;

  dm.Irregularidades.Close;
end;

procedure TFImportarDistribuidor.dsRXDataChange(Sender: TObject;
  Field: TField);
begin
  P3.Visible:=RXRetirado.AsBoolean;
end;

procedure TFImportarDistribuidor.GridMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if X in [11..86] then
    Screen.Cursor:=crHandPoint
      else Screen.Cursor:=crDefault;
end;

procedure TFImportarDistribuidor.P1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  Screen.Cursor:=crDefault;
end;

procedure TFImportarDistribuidor.GridMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if X in [11..86] then
    if not RX.IsEmpty then
    begin
        RX.Edit;
        RXRetirado.AsBoolean:=not RXRetirado.AsBoolean;
        RX.Post;
    end;
end;

procedure TFImportarDistribuidor.RXRetiradoChange(Sender: TField);
begin
  if not RXRetirado.AsBoolean then
  begin
      RX.Edit;
      RXIrregularidade.Clear;
      RXMotivoRejeicao.Clear;
  end;
end;

procedure TFImportarDistribuidor.RXBeforePost(DataSet: TDataSet);
begin
  if (RXRetirado.AsBoolean) and (RXIrregularidade.AsString='') then
  RXMotivoRejeicao.AsString:='N�O INFORMADA!';
end;

procedure TFImportarDistribuidor.Aceitar1Click(Sender: TObject);
begin
  MarcarTodos;
end;

procedure TFImportarDistribuidor.Rejeitar1Click(Sender: TObject);
begin
  DesmarcarTodos;
end;

procedure TFImportarDistribuidor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm.vCalc:=True;
end;

procedure TFImportarDistribuidor.FormCreate(Sender: TObject);
begin
  edData.Date:=Now;
end;

procedure TFImportarDistribuidor.btSeloClick(Sender: TObject);
begin
  Application.CreateForm(TFSelo,FSelo);
  FSelo.edSelo.Text:=RXSelo.AsString;
  FSelo.ShowModal;
  RX.Edit;
  RXSelo.AsString:=FSelo.edSelo.Text;
  RX.Post;
  FSelo.Free;
end;

procedure TFImportarDistribuidor.GridKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if key=VK_DELETE then
    if GR.Pergunta('Confirma a exclus�o?') then
      RX.Delete;
end;

end.
