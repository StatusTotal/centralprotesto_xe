{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   USplash.pas
  Descricao:   Tela de splash com o versionamento do sistema
  Author:      -
  Date:        -
  Last Update: 12-out-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit USplash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Gauges, jpeg, ExtCtrls, sPanel, StdCtrls, acPNG, sSkinProvider,
  dxGDIPlusClasses;

type
  TFSplash = class(TForm)
    P1: TsPanel;
    Image2: TImage;
    G: TGauge;
    lbTotalProtesto: TLabel;
    lbVersao: TLabel;
    lbAtualizacao: TLabel;
    lbMsn: TLabel;
    lbCallCenter: TLabel;
    Label1: TLabel;
    sSkinProvider: TsSkinProvider;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSplash: TFSplash;

implementation

uses UDM;

{$R *.dfm}

end.
