object FRelConferencia: TFRelConferencia
  Left = 357
  Top = 258
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Confer'#234'ncia de Digita'#231#227'o'
  ClientHeight = 166
  ClientWidth = 213
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 15
  object P1: TsPanel
    Left = 0
    Top = 0
    Width = 213
    Height = 166
    Align = alClient
    TabOrder = 0
    SkinData.SkinSection = 'SELECTION'
    ExplicitWidth = 1015
    ExplicitHeight = 601
    object edInicio: TsDateEdit
      Left = 72
      Top = 18
      Width = 111
      Height = 26
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 0
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'In'#237'cio'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -16
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
    end
    object edFim: TsDateEdit
      Left = 72
      Top = 61
      Width = 111
      Height = 26
      AutoSize = False
      Color = clWhite
      EditMask = '!99/99/9999;1; '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      Text = '  /  /    '
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Fim'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -16
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
    end
    object btVisualizar: TsBitBtn
      Left = 28
      Top = 127
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Caption = 'Visualizar'
      TabOrder = 2
      OnClick = btVisualizarClick
      SkinData.SkinSection = 'BUTTON'
    end
    object btSair: TsBitBtn
      Left = 108
      Top = 127
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Caption = 'Sair'
      TabOrder = 3
      OnClick = btSairClick
      SkinData.SkinSection = 'BUTTON'
    end
    object Relatorio: TQuickRep
      Left = 56
      Top = 232
      Width = 794
      Height = 1123
      ShowingPreview = False
      DataSet = RX
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      Functions.Strings = (
        'PAGENUMBER'
        'COLUMNNUMBER'
        'REPORTTITLE')
      Functions.DATA = (
        '0'
        '0'
        #39#39)
      OnEndPage = RelatorioEndPage
      Options = [FirstPageHeader, LastPageFooter]
      Page.Columns = 1
      Page.Orientation = poPortrait
      Page.PaperSize = A4
      Page.Continuous = False
      Page.Values = (
        100.000000000000000000
        2970.000000000000000000
        100.000000000000000000
        2100.000000000000000000
        100.000000000000000000
        100.000000000000000000
        0.000000000000000000)
      PrinterSettings.Copies = 1
      PrinterSettings.OutputBin = Auto
      PrinterSettings.Duplex = False
      PrinterSettings.FirstPage = 0
      PrinterSettings.LastPage = 0
      PrinterSettings.UseStandardprinter = False
      PrinterSettings.UseCustomBinCode = False
      PrinterSettings.CustomBinCode = 0
      PrinterSettings.ExtendedDuplex = 0
      PrinterSettings.UseCustomPaperCode = False
      PrinterSettings.CustomPaperCode = 0
      PrinterSettings.PrintMetaFile = False
      PrinterSettings.MemoryLimit = 1000000
      PrinterSettings.PrintQuality = 0
      PrinterSettings.Collate = 0
      PrinterSettings.ColorOption = 0
      PrintIfEmpty = True
      SnapToGrid = True
      Units = MM
      Zoom = 100
      PrevFormStyle = fsNormal
      PreviewInitialState = wsMaximized
      PreviewWidth = 500
      PreviewHeight = 500
      PrevInitialZoom = qrZoomToFit
      PreviewDefaultSaveType = stPDF
      PreviewLeft = 0
      PreviewTop = 0
      object Cabecalho: TQRBand
        Left = 38
        Top = 38
        Width = 718
        Height = 75
        Frame.DrawTop = True
        Frame.DrawBottom = True
        Frame.DrawLeft = True
        Frame.DrawRight = True
        AlignToBottom = False
        TransparentBand = False
        ForceNewColumn = False
        ForceNewPage = False
        Size.Values = (
          198.437500000000000000
          1899.708333333333000000)
        PreCaluculateBandHeight = False
        KeepOnOnePage = False
        BandType = rbPageHeader
        object lbRelatorio: TQRLabel
          Left = 205
          Top = 52
          Width = 308
          Height = 20
          Size.Values = (
            52.916666666666670000
            542.395833333333300000
            137.583333333333300000
            814.916666666666700000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = True
          Caption = 'RELAT'#211'RIO DE CONFER'#202'NCIA: 00/00/0000 - 00/00/0000'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 9
        end
        object lbCartorio: TQRLabel
          Left = 320
          Top = 4
          Width = 78
          Height = 24
          Size.Values = (
            63.500000000000000000
            846.666666666666700000
            10.583333333333330000
            206.375000000000000000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = True
          Caption = 'Cartorio'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsItalic]
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 14
        end
        object QRLabel16: TQRLabel
          Left = 215
          Top = 30
          Width = 288
          Height = 18
          Size.Values = (
            47.625000000000000000
            568.854166666666700000
            79.375000000000000000
            762.000000000000000000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taCenter
          AlignToBand = True
          Caption = 'PROTESTO DE T'#205'TULOS E DOCUMENTOS DE D'#205'VIDA'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          VerticalAlignment = tlTop
          FontSize = 9
        end
      end
      object Detail: TQRBand
        Left = 38
        Top = 113
        Width = 718
        Height = 32
        Frame.DrawBottom = True
        Frame.DrawLeft = True
        Frame.DrawRight = True
        AlignToBottom = False
        BeforePrint = DetailBeforePrint
        Color = clWindow
        TransparentBand = False
        ForceNewColumn = False
        ForceNewPage = False
        Size.Values = (
          84.666666666666670000
          1899.708333333333000000)
        PreCaluculateBandHeight = False
        KeepOnOnePage = False
        BandType = rbDetail
        object QRDBText1: TQRDBText
          Left = 4
          Top = 2
          Width = 63
          Height = 15
          Size.Values = (
            39.687500000000000000
            10.583333333333330000
            5.291666666666667000
            166.687500000000000000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taLeftJustify
          AlignToBand = False
          Color = clWhite
          DataSet = RX
          DataField = 'DESCRICAO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          FullJustify = False
          MaxBreakChars = 0
          VerticalAlignment = tlTop
          FontSize = 8
        end
        object QRDBText3: TQRDBText
          Left = 4
          Top = 16
          Width = 707
          Height = 15
          Size.Values = (
            39.687500000000000000
            10.583333333333330000
            42.333333333333330000
            1870.604166666667000000)
          XLColumn = 0
          XLNumFormat = nfGeneral
          Alignment = taLeftJustify
          AlignToBand = False
          AutoSize = False
          AutoStretch = True
          Color = clWhite
          DataSet = RX
          DataField = 'PROTOCOLOS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
          ExportAs = exptText
          WrapStyle = BreakOnSpaces
          FullJustify = False
          MaxBreakChars = 0
          VerticalAlignment = tlTop
          FontSize = 8
        end
      end
    end
    object ckSintetico: TsCheckBox
      Left = 70
      Top = 94
      Width = 72
      Height = 19
      Cursor = crHandPoint
      Caption = 'Sint'#233'tico'
      TabOrder = 5
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
  end
  object RX: TRxMemoryData
    FieldDefs = <>
    Left = 240
    Top = 88
    object RXCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
    object RXDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 200
    end
    object RXPROTOCOLOS: TStringField
      FieldName = 'PROTOCOLOS'
      Size = 1000
    end
    object RXQTD: TIntegerField
      FieldName = 'QTD'
    end
  end
  object qryApontados: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select PROTOCOLO from titulos where '
      ''
      'CODIGO=:CODIGO and'
      ''
      'DT_PROTOCOLO between :D1 and :D2'
      ''
      'order by PROTOCOLO')
    Left = 336
    Top = 88
    ParamData = <
      item
        Name = 'CODIGO'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end>
    object qryApontadosPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
  end
  object qryCancelados: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select PROTOCOLO from titulos where '
      ''
      'DT_PAGAMENTO between :D1 and :D2 and'
      'STATUS='#39'CANCELADO'#39' and CONVENIO=:Convenio'
      ''
      'order by PROTOCOLO')
    Left = 400
    Top = 72
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object qryCanceladosPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
  end
  object qryEspecial: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select RECIBO from CERTIDOES where'
      ''
      'DT_CERTIDAO between :D1 and :D2 and CODIGO=4011'
      ''
      'order by RECIBO')
    Left = 464
    Top = 88
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end>
    object qryEspecialRECIBO: TIntegerField
      FieldName = 'RECIBO'
      Origin = 'RECIBO'
    end
  end
  object qryDesistencias: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select PROTOCOLO from titulos where '
      ''
      'DT_RETIRADO between :D1 and :D2 and'
      'STATUS='#39'RETIRADO'#39' and CONVENIO=:Convenio'
      ''
      'order by PROTOCOLO')
    Left = 528
    Top = 72
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object qryDesistenciasPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
  end
  object qryCertidoes: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select RECIBO from CERTIDOES where'
      ''
      'DT_CERTIDAO between :D1 and :D2 and CODIGO=4022'
      ''
      'order by RECIBO')
    Left = 592
    Top = 88
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end>
    object qryCertidoesRECIBO: TIntegerField
      FieldName = 'RECIBO'
      Origin = 'RECIBO'
    end
  end
  object qryPagos: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select PROTOCOLO from titulos where '
      ''
      'DT_PAGAMENTO between :D1 and :D2 and'
      'STATUS='#39'PAGO'#39' and CONVENIO=:Convenio'
      ''
      'order by PROTOCOLO')
    Left = 656
    Top = 72
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object qryPagosPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
  end
  object qrySustados: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select PROTOCOLO from titulos where '
      ''
      'DT_SUSTADO between :D1 and :D2 and'
      'STATUS='#39'SUSTADO'#39' and CONVENIO=:Convenio'
      ''
      'order by PROTOCOLO')
    Left = 720
    Top = 88
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object qrySustadosPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
  end
  object qryProtestados: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select PROTOCOLO from titulos where '
      ''
      'DT_REGISTRO between :D1 and :D2 and'
      'STATUS='#39'PROTESTADO'#39' and CONVENIO=:Convenio'
      ''
      'order by PROTOCOLO')
    Left = 784
    Top = 72
    ParamData = <
      item
        Name = 'D1'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'D2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object qryProtestadosPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
  end
  object qryCodigos: TFDQuery
    Connection = dm.conCENTRAL
    SQL.Strings = (
      'select '
      ''
      'cod,titulo from cod where '
      ''
      'atrib=4 and '
      'cod between :codigo1 and :codigo2 and '
      'oculto=:oculto and '
      'convenio=:convenio'
      ''
      'order by cod')
    Left = 848
    Top = 88
    ParamData = <
      item
        Name = 'CODIGO1'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'CODIGO2'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'OCULTO'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'CONVENIO'
        DataType = ftString
        ParamType = ptInput
      end>
    object qryCodigosCOD: TIntegerField
      FieldName = 'COD'
      Origin = 'COD'
    end
    object qryCodigosTITULO: TStringField
      FieldName = 'TITULO'
      Origin = 'TITULO'
      Size = 50
    end
  end
end
