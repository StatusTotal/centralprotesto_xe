unit URelConferencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, FMTBcd, DB, SqlExpr, RxMemDS, QuickRpt, QRCtrls,
  StdCtrls, Buttons, sBitBtn, Mask, sMaskEdit, sCustomComboEdit, sTooledit,
  sPanel, sCheckBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFRelConferencia = class(TForm)
    P1: TsPanel;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    Relatorio: TQuickRep;
    Cabecalho: TQRBand;
    lbRelatorio: TQRLabel;
    lbCartorio: TQRLabel;
    QRLabel16: TQRLabel;
    Detail: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    RX: TRxMemoryData;
    RXCODIGO: TIntegerField;
    RXDESCRICAO: TStringField;
    RXPROTOCOLOS: TStringField;
    ckSintetico: TsCheckBox;
    RXQTD: TIntegerField;
    qryApontados: TFDQuery;
    qryCancelados: TFDQuery;
    qryEspecial: TFDQuery;
    qryDesistencias: TFDQuery;
    qryCertidoes: TFDQuery;
    qryPagos: TFDQuery;
    qrySustados: TFDQuery;
    qryProtestados: TFDQuery;
    qryCodigos: TFDQuery;
    qryCodigosCOD: TIntegerField;
    qryCodigosTITULO: TStringField;
    qryProtestadosPROTOCOLO: TIntegerField;
    qrySustadosPROTOCOLO: TIntegerField;
    qryPagosPROTOCOLO: TIntegerField;
    qryCertidoesRECIBO: TIntegerField;
    qryDesistenciasPROTOCOLO: TIntegerField;
    qryEspecialRECIBO: TIntegerField;
    qryCanceladosPROTOCOLO: TIntegerField;
    qryApontadosPROTOCOLO: TIntegerField;
    procedure btVisualizarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btSairClick(Sender: TObject);
    procedure RelatorioEndPage(Sender: TCustomQuickRep);
    procedure DetailBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelConferencia: TFRelConferencia;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFRelConferencia.btVisualizarClick(Sender: TObject);
var
  vQtd: Integer;
  vVazio: Boolean;
begin
  PF.Aguarde(True);
  lbCartorio.Caption  :=dm.ServentiaDESCRICAO.AsString;
  lbRelatorio.Caption :='RELAT�RIO DE CONFER�NCIA: '+edInicio.Text+' - '+edFim.Text;
  RX.Close;
  RX.Open;

  {APONTAMENTOS}
  vVazio:=True;
  RX.Append;
  RXCODIGO.AsInteger  :=0;
  RXDESCRICAO.AsString:='T�tulos Protocolizados no per�odo de '+edInicio.Text+' � '+edFim.Text;
  RX.Post;
  qryCodigos.Close;
  qryCodigos.Params[0].AsInteger :=4049;
  qryCodigos.Params[1].AsInteger :=4074;
  qryCodigos.Params[2].AsString  :='N';
  qryCodigos.Params[3].AsString  :='N';
  qryCodigos.Open;
  while not qryCodigos.Eof do
  begin
      RX.Append;
      RXCODIGO.AsInteger    :=qryCodigosCOD.AsInteger;
      RXDESCRICAO.AsString  :=qryCodigosTITULO.AsString+' ('+qryCodigosCOD.AsString+')';

      vQtd:=0;
      qryApontados.Close;
      qryApontados.Params[0].AsInteger :=qryCodigosCOD.AsInteger;
      qryApontados.Params[1].AsDate    :=edInicio.Date;
      qryApontados.Params[2].AsDate    :=edFim.Date;
      qryApontados.Open;
      while not qryApontados.Eof do
      begin
          vVazio:=False;
          Inc(vQtd);
          if RXPROTOCOLOS.AsString='' then
            RXPROTOCOLOS.AsString:=qryApontadosPROTOCOLO.AsString
              else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' - '+qryApontadosPROTOCOLO.AsString;
          qryApontados.Next;
      end;
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:='Total: 0'
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' = Total: '+IntToStr(vQtd);

      RXQTD.AsInteger:=vQtd;
      if ckSintetico.Checked then
      begin
          if RXQTD.AsInteger=0 then
            RX.Cancel
              else RX.Post;
      end
      else
        RX.Post;
      qryCodigos.Next;
  end;

  if vVazio then RX.Delete;

  {CANCELAMENTOS}
  vVazio:=True;
  RX.Append;
  RXDESCRICAO.AsString:='T�tulos com Movimento no per�odo de '+edInicio.Text+' � '+edFim.Text;
  RX.Post;
  vQtd:=0;
  qryCancelados.Close;
  qryCancelados.Params[0].AsDate   :=edInicio.Date;
  qryCancelados.Params[1].AsDate   :=edFim.Date;
  qryCancelados.Params[2].AsString :='N';
  qryCancelados.Open;
  RX.Append;
  RXCODIGO.AsInteger    :=4010;
  RXDESCRICAO.AsString  :='Cancelamentos (4010)';
  while not qryCancelados.Eof do
  begin
      vVazio:=False;
      Inc(vQtd);
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:=qryCanceladosPROTOCOLO.AsString
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' - '+qryCanceladosPROTOCOLO.AsString;
      qryCancelados.Next;
  end;

  if not vVazio then
  begin
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:='Total: 0'
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' = Total: '+IntToStr(vQtd);

      RXQTD.AsInteger:=vQtd;
      if ckSintetico.Checked then
      begin
          if RXQTD.AsInteger=0 then
            RX.Cancel
              else RX.Post;
      end
      else
        RX.Post;
  end
  else
    RX.Cancel;

  {CERTID�O ESPECIAL DE CADASTRO}
  vQtd:=0;
  qryEspecial.Close;
  qryEspecial.Params[0].AsDate:=edInicio.Date;
  qryEspecial.Params[1].AsDate:=edFim.Date;
  qryEspecial.Open;
  RX.Append;
  RXCODIGO.AsInteger    :=4011;
  RXDESCRICAO.AsString  :='Certid�o Especial de Cadastro (4011)';
  while not qryEspecial.Eof do
  begin
      vVazio:=False;
      Inc(vQtd);
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:=qryEspecialRECIBO.AsString
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' - '+qryEspecialRECIBO.AsString;
      qryEspecial.Next;
  end;

  if not vVazio then
  begin
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:='Total: 0'
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' = Total: '+IntToStr(vQtd);

      RXQTD.AsInteger:=vQtd;
      if ckSintetico.Checked then
      begin
          if RXQTD.AsInteger=0 then
            RX.Cancel
              else RX.Post;
      end
      else
        RX.Post;
  end
  else
    RX.Cancel;

  {DESIST�NCIAS}
  vQtd:=0;
  qryDesistencias.Close;
  qryDesistencias.Params[0].AsDate   :=edInicio.Date;
  qryDesistencias.Params[1].AsDate   :=edFim.Date;
  qryDesistencias.Params[2].AsString :='N';
  qryDesistencias.Open;
  RX.Append;
  RXCODIGO.AsInteger    :=4021;
  RXDESCRICAO.AsString  :='Desist�ncias (4021)';
  while not qryDesistencias.Eof do
  begin
      vVazio:=False;
      Inc(vQtd);
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:=qryDesistenciasPROTOCOLO.AsString
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' - '+qryDesistenciasPROTOCOLO.AsString;
      qryDesistencias.Next;
  end;

  if not vVazio then
  begin
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:='Total: 0'
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' = Total: '+IntToStr(vQtd);

      RXQTD.AsInteger:=vQtd;
      if ckSintetico.Checked then
      begin
          if RXQTD.AsInteger=0 then
            RX.Cancel
              else RX.Post;
      end
      else
        RX.Post;
  end
  else
    RX.Cancel;

  {CERTID�O}
  vQtd:=0;
  qryCertidoes.Close;
  qryCertidoes.Params[0].AsDate:=edInicio.Date;
  qryCertidoes.Params[1].AsDate:=edFim.Date;
  qryCertidoes.Open;
  RX.Append;
  RXCODIGO.AsInteger    :=4022;
  RXDESCRICAO.AsString  :='Certid�o (4022)';
  while not qryCertidoes.Eof do
  begin
      vVazio:=False;
      Inc(vQtd);
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:=qryCertidoesRECIBO.AsString
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' - '+qryCertidoesRECIBO.AsString;
      qryCertidoes.Next;
  end;

  if not vVazio then
  begin
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:='Total: 0'
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' = Total: '+IntToStr(vQtd);

      RXQTD.AsInteger:=vQtd;
      if ckSintetico.Checked then
      begin
          if RXQTD.AsInteger=0 then
            RX.Cancel
              else RX.Post;
      end
      else
        RX.Post;
  end
  else
    RX.Cancel;

  {PAGOS}
  vQtd:=0;
  qryPagos.Close;
  qryPagos.Params[0].AsDate    :=edInicio.Date;
  qryPagos.Params[1].AsDate    :=edFim.Date;
  qryPagos.Params[2].AsString  :='N';
  qryPagos.Open;
  RX.Append;
  RXCODIGO.AsInteger    :=4026;
  RXDESCRICAO.AsString  :='Quita��o de T�tulo (4026)';
  while not qryPagos.Eof do
  begin
      vVazio:=False;
      Inc(vQtd);
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:=qryPagosPROTOCOLO.AsString
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' - '+qryPagosPROTOCOLO.AsString;
      qryPagos.Next;
  end;

  if not vVazio then
  begin
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:='Total: 0'
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' = Total: '+IntToStr(vQtd);

      RXQTD.AsInteger:=vQtd;
      if ckSintetico.Checked then
      begin
          if RXQTD.AsInteger=0 then
            RX.Cancel
              else RX.Post;
      end
      else
        RX.Post;
  end
  else
    RX.Cancel;

  {SUSTADOS}
  vQtd:=0;
  qrySustados.Close;
  qrySustados.Params[0].AsDate   :=edInicio.Date;
  qrySustados.Params[1].AsDate   :=edFim.Date;
  qrySustados.Params[2].AsString :='N';
  qrySustados.Open;
  RX.Append;
  RXCODIGO.AsInteger    :=4027;
  RXDESCRICAO.AsString  :='Susta��o Definitiva (4027)';
  while not qrySustados.Eof do
  begin
      Inc(vQtd);
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:=qrySustadosPROTOCOLO.AsString
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' - '+qrySustadosPROTOCOLO.AsString;
      qrySustados.Next;
  end;

  if not vVazio then
  begin
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:='Total: 0'
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' = Total: '+IntToStr(vQtd);

      RXQTD.AsInteger:=vQtd;
      if ckSintetico.Checked then
      begin
          if RXQTD.AsInteger=0 then
            RX.Cancel
              else RX.Post;
      end
      else
        RX.Post;
  end
  else
    RX.Cancel;

  {PROTESTADOS}
  vQtd:=0;
  qryProtestados.Close;
  qryProtestados.Params[0].AsDate    :=edInicio.Date;
  qryProtestados.Params[1].AsDate    :=edFim.Date;
  qryProtestados.Params[2].AsString  :='N';
  qryProtestados.Open;
  RX.Append;
  RXCODIGO.AsInteger    :=4029;
  RXDESCRICAO.AsString  :='T�tulos Protestados (4029)';
  while not qryProtestados.Eof do
  begin
      Inc(vQtd);
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:=qryProtestadosPROTOCOLO.AsString
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' - '+qryProtestadosPROTOCOLO.AsString;
      qryProtestados.Next;
  end;

  if not vVazio then
  begin
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:='Total: 0'
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' = Total: '+IntToStr(vQtd);

      RXQTD.AsInteger:=vQtd;
      if ckSintetico.Checked then
      begin
          if RXQTD.AsInteger=0 then
            RX.Cancel
              else RX.Post;
      end
      else
        RX.Post;
  end
  else
    RX.Cancel;

  if vVazio then RX.Delete;

  vVazio:=True;

  {CONV�NIOS}
  {APONTAMENTOS}
  RX.Append;
  RXCODIGO.AsInteger  :=0;
  RXDESCRICAO.AsString:='T�tulos Protocolizados no per�odo de '+edInicio.Text+' � '+edFim.Text+' - ATO NORMATIVO CONJUNTO N� 05/05';
  RX.Post;
  qryCodigos.Close;
  qryCodigos.Params[0].AsInteger :=4075;
  qryCodigos.Params[1].AsInteger :=4100;
  qryCodigos.Params[2].AsString  :='N';
  qryCodigos.Params[3].AsString  :='S';
  qryCodigos.Open;
  while not qryCodigos.Eof do
  begin
      RX.Append;
      RXCODIGO.AsInteger    :=qryCodigosCOD.AsInteger;
      RXDESCRICAO.AsString  :=qryCodigosTITULO.AsString+' ('+qryCodigosCOD.AsString+')';

      vQtd:=0;
      qryApontados.Close;
      qryApontados.Params[0].AsInteger :=qryCodigosCOD.AsInteger;
      qryApontados.Params[1].AsDate    :=edInicio.Date;
      qryApontados.Params[2].AsDate    :=edFim.Date;
      qryApontados.Open;
      while not qryApontados.Eof do
      begin
          vVazio:=False;
          Inc(vQtd);
          if RXPROTOCOLOS.AsString='' then
            RXPROTOCOLOS.AsString:=qryApontadosPROTOCOLO.AsString
              else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' - '+qryApontadosPROTOCOLO.AsString;
          qryApontados.Next;
      end;
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:='Total: 0'
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' = Total: '+IntToStr(vQtd);

      RXQTD.AsInteger:=vQtd;
      if ckSintetico.Checked then
      begin
          if RXQTD.AsInteger=0 then
            RX.Cancel
              else RX.Post;
      end
      else
        RX.Post;
      qryCodigos.Next;
  end;

  if vVazio then RX.Delete;

  {CANCELAMENTOS}
  vVazio:=True;
  RX.Append;
  RXDESCRICAO.AsString:='T�tulos com Movimento no per�odo de '+edInicio.Text+' � '+edFim.Text+' - ATO NORMATIVO CONJUNTO N� 05/05';
  RX.Post;
  vQtd:=0;
  qryCancelados.Close;
  qryCancelados.Params[0].AsDate   :=edInicio.Date;
  qryCancelados.Params[1].AsDate   :=edFim.Date;
  qryCancelados.Params[2].AsString :='S';
  qryCancelados.Open;
  RX.Append;
  RXCODIGO.AsInteger    :=4038;
  RXDESCRICAO.AsString  :='Cancelamentos (4038)';
  while not qryCancelados.Eof do
  begin
      vVazio:=False;
      Inc(vQtd);
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:=qryCanceladosPROTOCOLO.AsString
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' - '+qryCanceladosPROTOCOLO.AsString;
      qryCancelados.Next;
  end;

  if not vVazio then
  begin
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:='Total: 0'
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' = Total: '+IntToStr(vQtd);

      RXQTD.AsInteger:=vQtd;
      if ckSintetico.Checked then
      begin
          if RXQTD.AsInteger=0 then
            RX.Cancel
              else RX.Post;
      end
      else
        RX.Post;
  end
  else
    RX.Cancel;

  {DESIST�NCIAS}
  vQtd:=0;
  qryDesistencias.Close;
  qryDesistencias.Params[0].AsDate   :=edInicio.Date;
  qryDesistencias.Params[1].AsDate   :=edFim.Date;
  qryDesistencias.Params[2].AsString :='S';
  qryDesistencias.Open;
  RX.Append;
  RXCODIGO.AsInteger    :=4039;
  RXDESCRICAO.AsString  :='Desist�ncias (4039)';
  while not qryDesistencias.Eof do
  begin
      vVazio:=False;
      Inc(vQtd);
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:=qryDesistenciasPROTOCOLO.AsString
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' - '+qryDesistenciasPROTOCOLO.AsString;
      qryDesistencias.Next;
  end;

  if not vVazio then
  begin
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:='Total: 0'
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' = Total: '+IntToStr(vQtd);

      RXQTD.AsInteger:=vQtd;
      if ckSintetico.Checked then
      begin
          if RXQTD.AsInteger=0 then
            RX.Cancel
              else RX.Post;
      end
      else
        RX.Post;
  end
  else
    RX.Cancel;

  {PAGOS}
  vQtd:=0;
  qryPagos.Close;
  qryPagos.Params[0].AsDate    :=edInicio.Date;
  qryPagos.Params[1].AsDate    :=edFim.Date;
  qryPagos.Params[2].AsString  :='S';
  qryPagos.Open;
  RX.Append;
  RXCODIGO.AsInteger    :=4041;
  RXDESCRICAO.AsString  :='Quita��o de T�tulo (4041)';
  while not qryPagos.Eof do
  begin
      vVazio:=False;
      Inc(vQtd);
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:=qryPagosPROTOCOLO.AsString
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' - '+qryPagosPROTOCOLO.AsString;
      qryPagos.Next;
  end;

  if not vVazio then
  begin
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:='Total: 0'
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' = Total: '+IntToStr(vQtd);

      RXQTD.AsInteger:=vQtd;
      if ckSintetico.Checked then
      begin
          if RXQTD.AsInteger=0 then
            RX.Cancel
              else RX.Post;
      end
      else
        RX.Post;
  end
  else
    RX.Cancel;

  {SUSTADOS}
  vQtd:=0;
  qrySustados.Close;
  qrySustados.Params[0].AsDate   :=edInicio.Date;
  qrySustados.Params[1].AsDate   :=edFim.Date;
  qrySustados.Params[2].AsString :='S';
  qrySustados.Open;
  RX.Append;
  RXCODIGO.AsInteger    :=4042;
  RXDESCRICAO.AsString  :='Susta��o Definitiva (4042)';
  while not qrySustados.Eof do
  begin
      vVazio:=False;
      Inc(vQtd);
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:=qrySustadosPROTOCOLO.AsString
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' - '+qrySustadosPROTOCOLO.AsString;
      qrySustados.Next;
  end;

  if not vVazio then
  begin
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:='Total: 0'
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' = Total: '+IntToStr(vQtd);

      RXQTD.AsInteger:=vQtd;
      if ckSintetico.Checked then
      begin
          if RXQTD.AsInteger=0 then
            RX.Cancel
              else RX.Post;
      end
      else
        RX.Post;
  end
  else
    RX.Cancel;

  {PROTESTADOS}
  vQtd:=0;
  qryProtestados.Close;
  qryProtestados.Params[0].AsDate    :=edInicio.Date;
  qryProtestados.Params[1].AsDate    :=edFim.Date;
  qryProtestados.Params[2].AsString  :='S';
  qryProtestados.Open;
  RX.Append;
  RXCODIGO.AsInteger    :=4043;
  RXDESCRICAO.AsString  :='T�tulos Protestados (4043)';
  while not qryProtestados.Eof do
  begin
      vVazio:=False;
      Inc(vQtd);
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:=qryProtestadosPROTOCOLO.AsString
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' - '+qryProtestadosPROTOCOLO.AsString;
      qryProtestados.Next;
  end;

  if not vVazio then
  begin
      if RXPROTOCOLOS.AsString='' then
        RXPROTOCOLOS.AsString:='Total: 0'
          else RXPROTOCOLOS.AsString:=RXPROTOCOLOS.AsString+' = Total: '+IntToStr(vQtd);

      RXQTD.AsInteger:=vQtd;
      if ckSintetico.Checked then
      begin
          if RXQTD.AsInteger=0 then
            RX.Cancel
              else RX.Post;
      end
      else
        RX.Post;
  end
  else
    RX.Cancel;

  Relatorio.Preview;
end;

procedure TFRelConferencia.FormCreate(Sender: TObject);
begin
  edInicio.Date :=Now;
  edFim.Date    :=Now;
end;

procedure TFRelConferencia.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFRelConferencia.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFRelConferencia.RelatorioEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFRelConferencia.DetailBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if RXCODIGO.AsInteger=0 then
    Detail.Color:=$00EBEBEB
      else Detail.Color:=clWindow;
end;

end.
