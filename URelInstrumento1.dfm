object FRelInstrumento: TFRelInstrumento
  Left = 192
  Top = 242
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'INSTRUMENTO DE PROTESTO'
  ClientHeight = 466
  ClientWidth = 742
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 14
  object P: TsPanel
    Left = 0
    Top = 0
    Width = 742
    Height = 84
    Align = alTop
    TabOrder = 0
    OnMouseMove = PMouseMove
    SkinData.SkinSection = 'PANEL'
    object GbProtocolo: TsGroupBox
      Left = 17
      Top = 14
      Width = 235
      Height = 58
      Caption = 'Protocolo'
      TabOrder = 0
      SkinData.SkinSection = 'MAINMENU'
      CaptionSkin = 'COMBOBOX'
      object edProtFinal: TsEdit
        Left = 149
        Top = 25
        Width = 75
        Height = 22
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnKeyPress = edProtFinalKeyPress
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Final'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
      end
      object edProtInicial: TsEdit
        Left = 42
        Top = 25
        Width = 75
        Height = 22
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = edProtInicialChange
        OnKeyPress = edProtInicialKeyPress
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Inicial'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
      end
    end
    object Rb1: TsRadioButton
      Left = 6
      Top = 6
      Width = 17
      Height = 13
      Cursor = crHandPoint
      Checked = True
      TabOrder = 3
      TabStop = True
      OnClick = Rb1Click
      SkinData.SkinSection = 'TRANSPARENT'
    end
    object GbData: TsGroupBox
      Left = 267
      Top = 14
      Width = 267
      Height = 58
      Caption = 'Data'
      Enabled = False
      TabOrder = 1
      SkinData.SkinSection = 'MAINMENU'
      CaptionSkin = 'COMBOBOX'
      object edDataInicial: TsDateEdit
        Left = 42
        Top = 25
        Width = 92
        Height = 22
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
        OnExit = edDataInicialExit
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Inicial'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        DefaultToday = True
      end
      object edDataFinal: TsDateEdit
        Left = 164
        Top = 25
        Width = 92
        Height = 22
        AutoSize = False
        Color = clWhite
        EditMask = '!99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'Final'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 5059883
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = 'Tahoma'
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
        GlyphMode.Blend = 0
        GlyphMode.Grayed = False
        DefaultToday = True
      end
    end
    object Rb2: TsRadioButton
      Left = 256
      Top = 6
      Width = 17
      Height = 13
      Cursor = crHandPoint
      TabOrder = 4
      OnClick = Rb2Click
      SkinData.SkinSection = 'TRANSPARENT'
    end
    object btFiltrar: TsBitBtn
      Left = 597
      Top = 22
      Width = 60
      Height = 50
      Cursor = crHandPoint
      Caption = 'Filtrar'
      Layout = blGlyphTop
      TabOrder = 2
      OnClick = btFiltrarClick
      ImageIndex = 0
      Images = dm.Imagens
      Reflected = True
      SkinData.SkinSection = 'MAINMENU'
    end
    object btVisualizar: TsBitBtn
      Left = 664
      Top = 22
      Width = 60
      Height = 50
      Cursor = crHandPoint
      Caption = 'Visualizar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      Layout = blGlyphTop
      ParentFont = False
      TabOrder = 5
      OnClick = btVisualizarClick
      ImageIndex = 4
      Images = dm.Imagens
      SkinData.SkinSection = 'MAINMENU'
    end
    object edHora: TsMaskEdit
      Left = 541
      Top = 50
      Width = 49
      Height = 22
      AutoSize = False
      Color = 16184300
      EditMask = '!90:00;1;_'
      Font.Charset = ANSI_CHARSET
      Font.Color = 5059883
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      MaxLength = 5
      ParentFont = False
      TabOrder = 6
      Text = '  :  '
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Hora'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Arial'
      BoundLabel.Font.Style = [fsBold]
      BoundLabel.Layout = sclTopLeft
      SkinData.SkinSection = 'PAGECONTROL'
    end
  end
  object GridAtos: TwwDBGrid
    Left = 0
    Top = 84
    Width = 742
    Height = 382
    ControlType.Strings = (
      'Check;CheckBox;True;False')
    Selected.Strings = (
      'Check'#9'5'#9'*'#9'F'
      'PROTOCOLO'#9'10'#9'Protocolo'#9'T'
      'DT_PROTOCOLO'#9'10'#9'Data'#9'T'
      'APRESENTANTE'#9'26'#9'Apresentante'#9'T'
      'DEVEDOR'#9'28'#9'Devedor'#9'T'
      'STATUS'#9'18'#9'Status'#9'F')
    IniAttributes.Delimiter = ';;'
    IniAttributes.UnicodeIniFile = False
    TitleColor = clBtnFace
    FixedCols = 1
    ShowHorzScrollBar = False
    EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm, ecoDisableDateTimePicker]
    Align = alClient
    BorderStyle = bsNone
    DataSource = dsProtocolo
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
    ParentFont = False
    PopupMenu = PM
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = False
    UseTFields = False
    OnMouseDown = GridAtosMouseDown
    OnMouseMove = GridAtosMouseMove
  end
  object dsProtocolo: TDataSource
    DataSet = qryProtocolo
    Left = 64
    Top = 304
  end
  object PM: TPopupMenu
    Left = 64
    Top = 192
    object Marcartodos1: TMenuItem
      Caption = 'Marcar todos'
      OnClick = Marcartodos1Click
    end
    object Desmarcartodos1: TMenuItem
      Caption = 'Desmarcar todos'
      OnClick = Desmarcartodos1Click
    end
  end
  object qryProtocolo: TFDQuery
    AfterOpen = qryProtocoloAfterOpen
    Connection = dm.conSISTEMA
    SQL.Strings = (
      
        'select ID_ATO,PROTOCOLO,DT_PROTOCOLO,APRESENTANTE,DEVEDOR,STATUS' +
        ' from TITULOS')
    Left = 64
    Top = 248
    object qryProtocoloCheck: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'Check'
    end
    object qryProtocoloID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryProtocoloPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object qryProtocoloDT_PROTOCOLO: TDateField
      FieldName = 'DT_PROTOCOLO'
      Origin = 'DT_PROTOCOLO'
    end
    object qryProtocoloAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Origin = 'APRESENTANTE'
      Size = 100
    end
    object qryProtocoloDEVEDOR: TStringField
      FieldName = 'DEVEDOR'
      Origin = 'DEVEDOR'
      Size = 100
    end
    object qryProtocoloSTATUS: TStringField
      FieldName = 'STATUS'
      Origin = 'STATUS'
    end
  end
end
