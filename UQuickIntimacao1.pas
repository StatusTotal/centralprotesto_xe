unit UQuickIntimacao1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, RxMemDS, QRCtrls, QuickRpt, ExtCtrls, Grids, DBGrids, Math;

type
  TFQuickIntimacao1 = class(TForm)
    QkIntimacao: TQuickRep;
    Cabecalho: TQRBand;
    QRLabel1: TQRLabel;
    lbTexto1: TQRLabel;
    lbTexto2: TQRLabel;
    Detalhe: TQRBand;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    lbTexto3: TQRLabel;
    lbTexto4: TQRLabel;
    Sh14: TQRShape;
    lbAssinatura: TQRLabel;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    qrCustas: TQRLabel;
    lbChequeAdm: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    qrValorAdm: TQRDBText;
    QRDBText12: TQRDBText;
    qrVencimento: TQRDBText;
    qrrLinha2: TQRLabel;
    qrrTesoura2: TQRImage;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    lbComprovante: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape11: TQRShape;
    QRLabel25: TQRLabel;
    QRShape12: TQRShape;
    QRLabel26: TQRLabel;
    QRShape13: TQRShape;
    QRLabel27: TQRLabel;
    QRShape14: TQRShape;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText19: TQRDBText;
    QRLabel3: TQRLabel;
    QRDBText20: TQRDBText;
    QRLabel12: TQRLabel;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRLabel13: TQRLabel;
    QRDBText23: TQRDBText;
    QRShape17: TQRShape;
    QRLabel34: TQRLabel;
    lbTarifa: TQRLabel;
    qrTarifa: TQRDBText;
    QRDBText18: TQRDBText;
    lbObs: TQRLabel;
    lbHora: TQRLabel;
    MMensagem: TQRMemo;
    qrSaldo: TQRLabel;
    qrValorDeposito: TQRDBText;
    qrDeposito: TQRLabel;
    qrCheque: TQRLabel;
    qrValorCheque: TQRDBText;
    RX: TRxMemoryData;
    RXTitulo: TStringField;
    RXApresentante: TStringField;
    RXCredor: TStringField;
    RXSacador: TStringField;
    RXDevedor: TStringField;
    RXValor: TFloatField;
    RXVencimento: TDateField;
    RXProtocolo: TStringField;
    RXIntimacao: TDateField;
    RXValorDeposito: TFloatField;
    RXCustas: TFloatField;
    RXEndereco: TStringField;
    RXCPF_CNPJ_Devedor: TStringField;
    RXNumeroTitulo: TStringField;
    RXEmissao: TDateField;
    RXDataProtocolo: TDateField;
    RXTarifa: TFloatField;
    RXObservacao: TStringField;
    RXDataIntimacao: TDateField;
    RXTipoIntimacao: TStringField;
    RXMotivo: TMemoField;
    RXValorCheque: TFloatField;
    lbDatas: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText13: TQRDBText;
    RXPraca: TStringField;
    QRLabel35: TQRLabel;
    QRDBText27: TQRDBText;
    RXDistribuicao: TFloatField;
    lbBoleto: TQRLabel;
    qrBoleto: TQRDBText;
    RXSaldo: TFloatField;
    sh1: TQRShape;
    qrTesoura2: TQRImage;
    qrLinha2: TQRLabel;
    sh2: TQRShape;
    qrGuia: TQRLabel;
    sh12: TQRShape;
    sh3: TQRShape;
    qrFavorecido: TQRLabel;
    qrServentia: TQRDBText;
    sh4: TQRShape;
    qrValido: TQRLabel;
    qrAgencia: TQRLabel;
    qrConta: TQRLabel;
    qrData: TQRLabel;
    qrTipo: TQRLabel;
    sh13: TQRShape;
    qrDepositado: TQRLabel;
    qrDevedor: TQRDBText;
    sh5: TQRShape;
    sh7: TQRShape;
    sh6: TQRShape;
    qrDocumento: TQRDBText;
    sh9: TQRShape;
    sh8: TQRShape;
    qrEmissao: TQRLabel;
    sh11: TQRShape;
    qrProtocolo: TQRLabel;
    sh10: TQRShape;
    qrNumProtocolo: TQRDBText;
    qrNumConta: TQRLabel;
    qrNumAgencia: TQRLabel;
    qrDinheiro: TQRLabel;
    qrValor: TQRDBText;
    qrBanco: TQRLabel;
    QRDBText29: TQRDBText;
    RXCEP: TStringField;
    RXConvenio: TStringField;
    lbBanco: TQRLabel;
    lbAgencia: TQRLabel;
    lbConta: TQRLabel;
    lbAviso: TQRLabel;
    lbConvenio: TQRLabel;
    lbPrazo: TQRLabel;
    qrPrazo: TQRDBText;
    RXDataPrazo: TDateField;
    lbLimite: TQRLabel;
    lbMatricula: TQRLabel;
    txNomeServentia: TQRLabel;
    ssh1: TQRShape;
    ssh2: TQRShape;
    qrrGuia: TQRLabel;
    ssh12: TQRShape;
    ssh3: TQRShape;
    qrrFavorecido: TQRLabel;
    qrrServentia: TQRDBText;
    ssh4: TQRShape;
    qrrValido: TQRLabel;
    qrrAgencia: TQRLabel;
    qrrConta: TQRLabel;
    qrrData: TQRLabel;
    qrrTipo: TQRLabel;
    ssh13: TQRShape;
    qrrDepositado: TQRLabel;
    qrrDevedor: TQRDBText;
    ssh5: TQRShape;
    ssh7: TQRShape;
    ssh6: TQRShape;
    qrrDocumento: TQRDBText;
    ssh9: TQRShape;
    ssh8: TQRShape;
    qrrEmissao: TQRLabel;
    ssh11: TQRShape;
    qrrProtocolo: TQRLabel;
    ssh10: TQRShape;
    qrrNumProtocolo: TQRDBText;
    qrrNumConta: TQRLabel;
    qrrNumAgencia: TQRLabel;
    qrrDinheiro: TQRLabel;
    qrrValor: TQRDBText;
    qrrBanco: TQRLabel;
    lbTabeliao: TQRLabel;
    lbCNPJ: TQRLabel;
    RXCPF_CNPJ_Apresentante: TStringField;
    QRLabel2: TQRLabel;
    QRShape18: TQRShape;
    QRLabel16: TQRLabel;
    QRDBText11: TQRDBText;
    RXFins: TStringField;
    QRLabel22: TQRLabel;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRShape21: TQRShape;
    RXNominal: TStringField;
    RXCodigoApresentante: TStringField;
    lbAssociado: TQRLabel;
    lbPGNF: TQRLabel;
    ShFins: TQRShape;
    qrCNPJ: TQRLabel;
    procedure DetalheBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QkIntimacaoEndPage(Sender: TCustomQuickRep);
    procedure FormCreate(Sender: TObject);
    procedure CarregarRX(IdAto: Integer);
    procedure CabecalhoBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
  private
    { Private declarations }
    vGuia   : Boolean;
    vNominal: Boolean;
  public
    { Public declarations }
  end;

var
  FQuickIntimacao1: TFQuickIntimacao1;

implementation

uses UPF, UDM, UAssinatura, UGeral;

{$R *.dfm}

procedure TFQuickIntimacao1.DetalheBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
var
  vTexto: String;
begin
  if dm.ServentiaCODIGO.AsInteger=1622 then
  begin
      if RXCodigoApresentante.AsString='582' then
        lbPGNF.Enabled:=True
          else lbPGNF.Enabled:=False;
  end;

  {SJVRP}
  lbConvenio.Enabled:=RXConvenio.AsString='S';

  if dm.ServentiaCODIGO.AsInteger=2366 then
  begin
      lbTexto3.Caption:='Buscando evitar futuros transtornos, assim como o protesto dos t�tulos listados acima, '+
                        'solicitamos que seja providenciado o pagamento atrav�s de dep�sito banc�rio em conta do Cart�rio '+
                        'no Banco do Brasil, Ag�ncia 3470-3, Conta corrente n�7883-2, no valor de R$ '+FloatToStrF(RXValorDeposito.AsFloat,ffNumber,7,2)+'.';
      lbTexto4.Caption:='ATEN��O: � imprescind�vel o retorno ao Cart�rio com o comprovante do dep�sito banc�rio, '+
                        'dentro do prazo acima mencionado, para evitar o protesto do t�tulo.';
  end
  else{BOM JARDIM}
    if dm.ServentiaCODIGO.AsInteger=1166 then
    begin
        lbTexto3.Caption:='Buscando evitar futuros transtornos, assim como o protesto dos t�tulos listados acima, '+
                          'solicitamos que seja providenciado o pagamento atrav�s de boleto banc�rio em dinheiro ou '+
                          'atrav�s de cheque administrativo (de qualquer banco), no valor de R$ '+FloatToStrF(RXValorDeposito.AsFloat,ffNumber,7,2)+'.';
    end
    else{ARRAIAL DO CABO}
      if dm.ServentiaCODIGO.AsInteger=2353 then
      begin
          lbTexto4.Enabled:=False;
          lbTexto3.Caption:='Buscando evitar futuros transtornos, assim como o protesto do t�tulo listado acima, '+
                            'solicitamos que seja providenciado o pagamento atrav�s de boleto banc�rio em dinheiro, no valor de R$ '+FloatToStrF(RXValor.AsFloat+RXCustas.AsFloat+RXTarifa.AsFloat,ffNumber,7,2)+' '+
                            'ou atrav�s de cheque administrativo (de qualquer banco), no valor de R$ '+FloatToStrF(RXValor.AsFloat+RXCustas.AsFloat,ffNumber,7,2)+' '+
                            'nominal a '+RXNominal.AsString+'. AP�S O PAGAMENTO, RETORNAR AO CART�RIO NO MESMO DIA PARA A QUITA��O DO T�TULO. '+
                            '(CONTA CORRENTE: 00578-9, AG�NCIA: 6894, BANCO BRADESCO).';
      end
      else{PAULO DE FRONTIN}
        if dm.ServentiaCODIGO.AsInteger=1237 then
        begin
            lbTexto3.Caption:='Buscando evitar futuros transtornos, assim como o protesto dos t�tulos listados acima, '+
                              'solicitamos que seja providenciado o pagamento atrav�s de cheque administrativo (de qualquer banco), '+
                              'no valor de R$ '+FloatToStrF(RXValorDeposito.AsFloat,ffNumber,7,2)+' nominal a '+RXNominal.AsString+'.';
            lbTexto4.Caption:='';
        end
        else{TANGU�}
          if dm.ServentiaCODIGO.AsInteger=2652 then
          begin
              lbTexto3.Caption:='Buscando evitar futuros transtornos, assim como o protesto dos t�tulos listados acima, '+
                                'solicitamos que seja providenciado o pagamento atrav�s de dep�sito banc�rio (preferencialmente '+
                                'direto no caixa) no valor de R$ '+FloatToStrF(RXValorDeposito.AsFloat,ffNumber,7,2)+'.';
              lbTexto4.Caption:='ATEN��O: � imprescind�vel o retorno ao Cart�rio com o comprovante do dep�sito banc�rio, '+
                                'dentro do prazo acima mencionado, para evitar o protesto do t�tulo.';
              lbTexto4.Font.Size:=9;
              lbTexto4.Font.Style:=[fsBold];
          end
          else{CARMO}
            if dm.ServentiaCODIGO.AsInteger=1208 then
            begin
                MMensagem.Top   :=430;
                lbHora.Caption  :='Hor�rio de funcionamento do setor de protesto: '+dm.ValorParametro(25);
                lbTexto2.Caption:='Intimo-o(s) por este meio � vir(em) pag�-lo(s), ou dar-me raz�es por que n�o o faz(em), ficando desde j� notificado(s) '+
                                  'do respectivo protesto DENTRO DO PRAZO DE 24 HORAS CONTADOS A PARTIR DO RECEBIMENTO DESSA INTIMA��O conforme '+
                                  'lei n� 9.492 de 10 de Setembro de 1997.';
                lbComprovante.Caption:='Intima��o por A.R.';
                lbTexto3.Caption:='Buscando evitar futuros transtornos, assim como o protesto dos t�tulos listados acima, '+
                                  'solicitamos que seja providenciado o pagamento atrav�s de boleto banc�rio em dinheiro no valor de R$ '+
                                  FloatToStrF(RXValorDeposito.AsFloat,ffNumber,7,2)+' ou atrav�s de cheque administrativo (de qualquer banco), no valor de R$ '+
                                  FloatToStrF(RXValorDeposito.AsFloat-RXTarifa.AsFloat,ffNumber,7,2)+' '+
                                  'nominal a '+RXNominal.AsString+'.';
                lbTexto4.Caption:='ATEN��O: Para pagamento em dinheiro, � necess�rio o pr�vio comparecimento ao cart�rio para a retirada da '+
                                  'guia de pagamento. Ap�s o pagamento, retornar ao cart�rio no mesmo dia para a quita��o do t�tulo, a fim de '+
                                  'que se evite o protesto do mesmo.';
            end {CAMPOS}
            else
              if dm.ServentiaCODIGO.AsInteger=8251 then
              begin
                  lbTexto4.Enabled    :=False;
                  lbBoleto.Enabled    :=False;
                  qrBoleto.Enabled    :=False;
                  lbChequeAdm.Caption :='Valor para pagamento em Cheque Administrativo/Moeda Corrente R$:';
                  lbTexto3.Caption    :='Buscando evitar futuros transtornos, assim como o protesto dos t�tulos listados acima, '+
                                        'solicitamos que seja providenciado o pagamento atrav�s de Cheque Administrativo ou Dinheiro '+
                                        'no valor de R$ '+FloatToStrF(RXValorCheque.AsFloat,ffNumber,7,2)+'.';
              end
                else
                  if (dm.ServentiaCODIGO.AsInteger=1227) or (dm.ServentiaCODIGO.AsInteger=6918) then
                  begin
                      lbChequeAdm.Enabled         :=False;
                      qrValorAdm.Enabled          :=False;
                      lbTexto3.Enabled            :=False;
                      qrCheque.Enabled            :=False;
                      qrValorCheque.Enabled       :=False;
                      qrDeposito.Caption          :='R$';
                      qrValorDeposito.Left        :=565;
                      qrValorDeposito.Font.Style  :=[fsBold];
                      lbTexto4.Caption            :='ATEN��O: Para pagamento � necess�rio o pr�vio comparecimento ao cart�rio.';
                      lbBoleto.Caption            :='Saldo Devedor R$:';
                      qrBoleto.DataField          :='Saldo';
                  end
                    else {1� RESENDE}
                      if dm.ServentiaCODIGO.AsInteger=1726 then
                      begin
                          lbTexto2.Caption:='Intimo-o(s) por este meio � vir(em) pag�-lo(s), ou dar-me raz�es por que n�o o faz(em), '+
                                            'ficando desde j� notificado(s) do respectivo protesto DENTRO DO PRAZO DE 03 (TR�S) DIAS '+
                                            '�TEIS conforme lei n� 9.492 de 10 de Setembro de 1997.';
                          lbTexto3.Enabled            :=False;
                          lbTexto4.Top                :=330;
                          lbTexto4.Height             :=80;
                          lbTexto4.Caption            :='COMUNICADOS:'+#13+
                                                        'Para pagamento em dinheiro, � necess�rio o pr�vio comparecimento ao cart�rio '+
                                                        'para a retirada da guia de pagamento. Ap�s o pagamento, retornar ao cart�rio '+
                                                        'no mesmo dia para a quita��o do t�tulo.'+#13+
                                                        'Para pagamento atrav�s de cheque administrativo, estar nominal ao Cedente/Portador.';
                      end
                      else
                        {RIO DAS OSTRAS}
                        if dm.ServentiaCODIGO.AsInteger=2354 then
                        begin
                            //if RXConvenio.AsString='S' then
                           // begin
                                if dm.Especie(RXCodigoApresentante.AsString) then
                                begin
                                    lbTexto3.Caption:='Buscando evitar futuros transtornos, assim como o protesto dos t�tulos listados acima, '+
                                                      'solicitamos que seja providenciado o pagamento atrav�s de cheque administrativo '+
                                                      '(de qualquer banco), no valor de R$ '+FloatToStrF(RXValor.AsFloat,ffNumber,7,2)+' '+
                                                      'nominal a '+RXNominal.AsString+'. As custas, no valor de R$ '+
                                                      FloatToStrF(RXCustas.AsFloat,ffNumber,7,2)+' ser�o cobradas em esp�cie no cart�rio.';
                                    qrSaldo.Caption   :='Esp�cie (No Cart�rio): R$ '+FloatToStrF(RXCustas.AsFloat,ffNumber,7,2);
                                    qrCheque.Caption  :='Cheque Administrativo: R$ '+FloatToStrF(RXValor.AsFloat,ffNumber,7,2);
                                end
                                else
                                begin
                                    lbTexto3.Caption:='Buscando evitar futuros transtornos, assim como o protesto dos t�tulos listados acima, '+
                                                      'solicitamos que seja providenciado o pagamento atrav�s de cheque administrativo '+
                                                      '(de qualquer banco), no valor de R$ '+FloatToStrF(RXValorCheque.AsFloat,ffNumber,7,2)+' '+
                                                      'nominal a '+RXNominal.AsString+'.';
                                    qrSaldo.Caption   :='Cheque Administrativo: R$ '+FloatToStrF(RXValorCheque.AsFloat,ffNumber,7,2);
                                    qrCheque.Caption  :='';
                                    {lbTexto3.Caption:='Buscando evitar futuros transtornos, assim como o protesto dos t�tulos listados acima, '+
                                                      'solicitamos que seja providenciado o pagamento atrav�s de cheque administrativo '+
                                                      '(de qualquer banco), no valor de R$ '+FloatToStrF(RXValor.AsFloat,ffNumber,7,2)+' '+
                                                      'nominal a '+RXNominal.AsString+'. As custas, no valor de R$ '+
                                                      FloatToStrF(RXCustas.AsFloat,ffNumber,7,2)+' ser�o cobradas em cheque administrativo no cart�rio.';
                                    qrSaldo.Caption   :='Esp�cie (No Cart�rio): R$ '+FloatToStrF(RXCustas.AsFloat,ffNumber,7,2);
                                    qrCheque.Caption  :='Cheque Administrativo: R$ '+FloatToStrF(RXValor.AsFloat,ffNumber,7,2); }
                                end;
                           { end
                            else
                            begin
                                lbTexto3.Caption:='Buscando evitar futuros transtornos, assim como o protesto dos t�tulos listados acima, '+
                                                  'solicitamos que seja providenciado o pagamento atrav�s de cheque administrativo '+
                                                  '(de qualquer banco), no valor de R$ '+FloatToStrF(RXValorCheque.AsFloat,ffNumber,7,2)+' '+
                                                  'nominal a '+RXNominal.AsString+'.';
                                qrSaldo.Caption   :='Cheque Administrativo: R$ '+FloatToStrF(RXValorCheque.AsFloat,ffNumber,7,2);
                                qrCheque.Caption  :='';
                            end;  }

                            lbTexto4.Enabled        :=False;
                            qrDeposito.Enabled      :=False;
                            lbBoleto.Enabled        :=False;
                            qrBoleto.Enabled        :=False;
                            qrValorDeposito.Enabled :=False;
                            qrTarifa.Enabled        :=False;
                            lbTarifa.Enabled        :=False;
                            qrValorCheque.Enabled   :=False;
                        end
                        else {2� CACHOEIRAS DE MACACU}
                          if dm.ServentiaCODIGO.AsInteger=1182 then
                          begin{SEM CHEQUE ADMINISTRATIVO}
                               lbTexto3.Caption:='Buscando evitar futuros transtornos, assim como o protesto dos t�tulos listados acima, '+
                                                 'solicitamos que seja providenciado o pagamento atrav�s de guia banc�ria ou dinheiro, no '+
                                                 'valor de R$ '+FloatToStrF(RXValorCheque.AsFloat,ffNumber,7,2)+'.';
                               qrCheque.Enabled:=False;
                               lbChequeAdm.Enabled:=False;
                               qrValorAdm.Enabled:=False;
                               qrValorCheque.Enabled:=False;
                          end
                          else{JAPERI}
                            if dm.ServentiaCODIGO.AsInteger=6926 then
                            begin
                                 lbTexto3.Caption:='Buscando evitar futuros transtornos, assim como o protesto dos t�tulos listados acima, '+
                                                   'solicitamos que seja providenciado o pagamento atrav�s de dep�sito banc�rio, no '+
                                                   'valor de R$ '+FloatToStrF(RXValorCheque.AsFloat,ffNumber,7,2)+'.';
                                 lbBoleto.Caption:='Valor para pagamento R$:';
                                 qrCheque.Enabled:=False;
                                 lbChequeAdm.Enabled:=False;
                                 qrValorAdm.Enabled:=False;
                                 qrValorCheque.Enabled:=False;
                                 lbTexto4.Enabled:=False;
                            end
                            else
                              if dm.ServentiaCODIGO.AsInteger=4473 then
                              begin
                                  lbTexto3.Caption:='Buscando evitar futuros transtornos, assim como o protesto dos t�tulos listados acima, '+
                                                    'solicitamos que seja providenciado o pagamento no Cart�rio, no '+
                                                    'valor de R$ '+FloatToStrF(RXValorCheque.AsFloat,ffNumber,7,2)+'.';
                                  lbTexto4.Enabled:=False;
                                  lbBoleto.Enabled:=False;
                                  qrBoleto.Enabled:=False;
                              end
                              else{PORTO REAL}
                                if dm.ServentiaCODIGO.AsInteger=2323 then
                                begin
                                    lbBanco.Enabled   :=False;
                                    lbAgencia.Enabled :=False;
                                    lbConta.Enabled   :=False;
                                    lbTexto3.Caption  :='Buscando evitar futuros transtornos, assim como o protesto dos t�tulos listados acima, '+
                                                        'solicitamos que seja providenciado o pagamento atrav�s de dep�sito, '+
                                                        'no valor de R$ '+FloatToStrF(RXValorDeposito.AsFloat,ffNumber,7,2)+
                                                        '. Banco: '+dm.ValorParametro(44)+
                                                        ' - Ag�ncia: '+dm.ValorParametro(45)+'/'+dm.ValorParametro(64)+' - '+
                                                        'C/C: '+dm.ValorParametro(46)+'/'+dm.ValorParametro(65)+
                                                        '. Favorecido: '+dm.ServentiaDESCRICAO.AsString+
                                                        ' - CNPJ: '+GR.FormatarCPFCNPJ(dm.ServentiaCNPJ.AsString)+'.';
                                    lbTexto4.Caption  :='ATEN��O: Ap�s o pagamento, retornar ao cart�rio no mesmo dia para a quita��o do t�tulo.'; 
                                end
                                else
                                  if dm.ServentiaCODIGO.AsInteger=1329 then
                                  begin
                                      lbChequeAdm.Enabled         :=False;
                                      qrValorAdm.Enabled          :=False;
                                      lbTexto3.Enabled            :=False;
                                      qrCheque.Enabled            :=False;
                                      qrValorCheque.Enabled       :=False;
                                      qrDeposito.Caption          :='R$';
                                      qrValorDeposito.Left        :=565;
                                      qrValorDeposito.Font.Style  :=[fsBold];
                                      lbTexto4.Caption            :='ATEN��O: O PAGAMENTO DEVE SER FEITO NO CART�RIO!';
                                      lbBoleto.Caption            :='Saldo Devedor R$:';
                                      qrBoleto.DataField          :='Saldo';
                                  end
                                  else
                                    begin{GERAL}
                                        lbTexto3.Caption:='Buscando evitar futuros transtornos, assim como o protesto dos t�tulos listados acima, '+
                                                          'solicitamos que seja providenciado o pagamento atrav�s de guia banc�ria em dinheiro ou '+
                                                          'atrav�s de cheque administrativo (de qualquer banco), no valor de R$ '+FloatToStrF(RXValorCheque.AsFloat,ffNumber,7,2)+' '+
                                                          'nominal a '+RXNominal.AsString+'.';
                                    end;

  qrCNPJ.Caption:=GR.FormatarCPFCNPJ(RXCPF_CNPJ_Devedor.AsString,'S');

  if RXCPF_CNPJ_Apresentante.AsString<>'' then
    lbCNPJ.Caption:='CNPJ: '+GR.FormatarCNPJ(RXCPF_CNPJ_Apresentante.AsString)
      else lbCNPJ.Caption:=''; 

  if dm.ServentiaCODIGO.AsInteger=1430 then
  lbTarifa.Caption:='Certid�o de Quita��o R$:';

  if Trim(RXObservacao.AsString)='' then
    lbObs.Enabled:=False
      else lbObs.Enabled:=True;

  ShFins.Enabled:=RXFins.AsString='SIM';

  MMensagem.Lines.Add(dm.ValorParametro(24));
  lbDatas.Caption :='Data de Entrada: '+PF.FormatarData(RXDataProtocolo.AsDateTime,'N')+
                    '  Data da Intima��o: '+PF.FormatarData(RXDataIntimacao.AsDateTime,'N');

  if RXTipoIntimacao.AsString='Carta' then
  begin
      vTexto:='Intimo-o(s) por este meio � vir(em) pag�-lo(s), ou dar-me raz�es por que n�o o faz(em), '+
              'ficando desde j� notificado(s) do respectivo protesto DENTRO DO PRAZO DE 03 (TR�S) DIAS �TEIS AP�S O RECEBIMENTO, '+
              'conforme lei n� 9.492 de 10 de Setembro de 1997.';
      lbPrazo.Enabled:=False;
      qrPrazo.Enabled:=False;
  end
    else
      if dm.ServentiaCODIGO.AsInteger<>1726 then
        vTexto:='Intimo-o(s) por este meio � vir(em) pag�-lo(s), ou dar-me raz�es por que n�o o faz(em), '+
                'ficando desde j� notificado(s) do respectivo protesto DENTRO DO PRAZO DE 03 (TR�S) DIAS �TEIS CONTADOS DE [DATA] '+
                'conforme lei n� 9.492 de 10 de Setembro de 1997.';

  if dm.ServentiaCODIGO.AsInteger<>1726 then
  lbTexto2.Caption:=StringReplace(vTexto,'[DATA]',FormatDateTime('dd/mm/yyyy',RXDataProtocolo.AsDateTime),[rfReplaceAll,rfIgnoreCase]);

  sh1.Enabled              :=vGuia;
  sh2.Enabled              :=vGuia;
  sh3.Enabled              :=vGuia;
  sh4.Enabled              :=vGuia;
  sh5.Enabled              :=vGuia;
  sh6.Enabled              :=vGuia;
  sh7.Enabled              :=vGuia;
  sh8.Enabled              :=vGuia;
  sh9.Enabled              :=vGuia;
  sh10.Enabled             :=vGuia;
  sh11.Enabled             :=vGuia;
  sh12.Enabled             :=vGuia;
  sh13.Enabled             :=vGuia;
  qrBanco.Enabled          :=vGuia;
  qrGuia.Enabled           :=vGuia;
  qrFavorecido.Enabled     :=vGuia;
  qrServentia.Enabled      :=vGuia;
  qrValido.Enabled         :=vGuia;
  qrAgencia.Enabled        :=vGuia;
  qrNumAgencia.Enabled     :=vGuia;
  qrConta.Enabled          :=vGuia;
  qrNumConta.Enabled       :=vGuia;
  qrData.Enabled           :=vGuia;
  qrEmissao.Enabled        :=vGuia;
  qrProtocolo.Enabled      :=vGuia;
  qrNumProtocolo.Enabled   :=vGuia;
  qrDepositado.Enabled     :=vGuia;
  qrDevedor.Enabled        :=vGuia;
  qrTipo.Enabled           :=vGuia;
  qrDocumento.Enabled      :=vGuia;
  qrDinheiro.Enabled       :=vGuia;
  qrValor.Enabled          :=vGuia;
  qrTesoura2.Enabled       :=dm.ValorParametro(47)='S';
  qrLinha2.Enabled         :=dm.ValorParametro(47)='S';
  qrBanco.Caption          :=dm.ValorParametro(44);
  qrNumAgencia.Caption     :=dm.ValorParametro(45);
  qrNumConta.Caption       :=dm.ValorParametro(46)+'-'+dm.ValorParametro(65);
  qrEmissao.Caption        :=FormatDateTime('dd/mm/yyyy',Now);

  ssh1.Enabled             :=dm.ValorParametro(47)='S';
  ssh2.Enabled             :=dm.ValorParametro(47)='S';
  ssh3.Enabled             :=dm.ValorParametro(47)='S';
  ssh4.Enabled             :=dm.ValorParametro(47)='S';
  ssh5.Enabled             :=dm.ValorParametro(47)='S';
  ssh6.Enabled             :=dm.ValorParametro(47)='S';
  ssh7.Enabled             :=dm.ValorParametro(47)='S';
  ssh8.Enabled             :=dm.ValorParametro(47)='S';
  ssh9.Enabled             :=dm.ValorParametro(47)='S';
  ssh10.Enabled            :=dm.ValorParametro(47)='S';
  ssh11.Enabled            :=dm.ValorParametro(47)='S';
  ssh12.Enabled            :=dm.ValorParametro(47)='S';
  ssh13.Enabled            :=dm.ValorParametro(47)='S';
  qrrBanco.Enabled         :=dm.ValorParametro(47)='S';
  qrrGuia.Enabled          :=dm.ValorParametro(47)='S';
  qrrFavorecido.Enabled    :=dm.ValorParametro(47)='S';
  qrrServentia.Enabled     :=dm.ValorParametro(47)='S';
  qrrValido.Enabled        :=dm.ValorParametro(47)='S';
  qrrAgencia.Enabled       :=dm.ValorParametro(47)='S';
  qrrNumAgencia.Enabled    :=dm.ValorParametro(47)='S';
  qrrConta.Enabled         :=dm.ValorParametro(47)='S';
  qrrNumConta.Enabled      :=dm.ValorParametro(47)='S';
  qrrData.Enabled          :=dm.ValorParametro(47)='S';
  qrrEmissao.Enabled       :=dm.ValorParametro(47)='S';
  qrrProtocolo.Enabled     :=dm.ValorParametro(47)='S';
  qrrNumProtocolo.Enabled  :=dm.ValorParametro(47)='S';
  qrrDepositado.Enabled    :=dm.ValorParametro(47)='S';
  qrrDevedor.Enabled       :=dm.ValorParametro(47)='S';
  qrrTipo.Enabled          :=dm.ValorParametro(47)='S';
  qrrDocumento.Enabled     :=dm.ValorParametro(47)='S';
  qrrDinheiro.Enabled      :=dm.ValorParametro(47)='S';
  qrrValor.Enabled         :=dm.ValorParametro(47)='S';
  qrrTesoura2.Enabled      :=dm.ValorParametro(47)='S';
  qrrLinha2.Enabled        :=dm.ValorParametro(47)='S';
  qrrBanco.Caption         :=dm.ValorParametro(44);
  qrrNumAgencia.Caption    :=dm.ValorParametro(45);
  qrrNumConta.Caption      :=dm.ValorParametro(46)+'-'+dm.ValorParametro(65);
  qrrEmissao.Caption       :=FormatDateTime('dd/mm/yyyy',Now);

  if dm.ServentiaCODIGO.AsInteger=2366 then
  begin
      lbChequeAdm.Enabled :=False;
      qrValorAdm.Enabled  :=False;
      lbTarifa.Enabled    :=False;
      qrTarifa.Enabled    :=False;
      lbBoleto.Enabled    :=False;
      qrBoleto.Enabled    :=False;
      lbBanco.Enabled     :=False;
      lbAgencia.Enabled   :=False;
      lbConta.Enabled     :=False;
  end;
end;

procedure TFQuickIntimacao1.QkIntimacaoEndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFQuickIntimacao1.FormCreate(Sender: TObject);
begin
  RX.Close;
  RX.Open;

  lbAssociado.Enabled:=dm.ServentiaCODIGO.AsInteger=1622;

  if dm.ServentiaCODIGO.AsInteger=2369 then
  begin
      qrSaldo.Enabled         :=False;
      qrDeposito.Enabled      :=False;
      qrValorDeposito.Enabled :=False;
      qrCheque.Enabled        :=False;
      qrValor.Enabled         :=False;
      qrValorCheque.Enabled   :=False;
  end;

  if dm.ServentiaCODIGO.AsInteger=4448 then
  begin
      lbBoleto.Caption:='Informa��es para dep�sito banc�rio:';
      lbBoleto.Left   :=467;
      qrBoleto.Enabled:=False;
  end;

  if dm.ServentiaCODIGO.AsInteger=1336 then
  begin
      Sh14.Enabled          :=False;
      lbAssinatura.Enabled  :=False;
      lbMatricula.Enabled   :=False;
  end;

  txNomeServentia.Caption:=dm.ServentiaDESCRICAO.AsString+' - '+dm.ServentiaENDERECO.AsString+' '+PF.FormatarTelefone(dm.ServentiaTELEFONE.AsString);
  lbTabeliao.Caption:=dm.ServentiaTABELIAO.AsString+' '+dm.ValorParametro(7)+' '+dm.ServentiaMATRICULA.AsString;
  {if dm.ServentiaCODIGO.AsInteger=1430 then
    if GR.Pergunta('Utilizar informa��es do 4� OFicio') then
      txNomeServentia.Caption:='VASSOURAS - 4� OF�CIO';}

  if dm.ValorParametro(44)<>'' then
  begin
      lbBanco.Caption:='Banco: '+dm.ValorParametro(44);
      lbBanco.Enabled:=True;
  end;

  if dm.ValorParametro(45)<>'' then
  begin
      lbAgencia.Caption:='Ag�ncia: '+dm.ValorParametro(45);
      lbAgencia.Enabled:=True;
  end;

  if dm.ValorParametro(46)<>'' then
  begin
      lbConta.Caption:='Conta: '+dm.ValorParametro(46)+'-'+dm.ValorParametro(65);
      lbConta.Enabled:=True;
  end;

  if dm.ServentiaCODIGO.AsInteger=1726 then
  begin
      lbBanco.Enabled   :=False;
      lbAgencia.Enabled :=False;
      lbConta.Enabled   :=False;
      lbBoleto.Caption  :='Valor para pagamento em Esp�cie R$:';
  end;

  if dm.ServentiaCODIGO.AsInteger=1554 then
    lbHora.Caption:='Hor�rio de Pagamento: '+dm.ValorParametro(25)
      else lbHora.Caption:='Hor�rio de funcionamento do Cart�rio: '+dm.ValorParametro(25);

  if dm.ServentiaCODIGO.AsInteger=2369 then
  begin
      Cabecalho.Frame.DrawTop    :=False;
      Cabecalho.Frame.DrawLeft   :=False;
      Cabecalho.Frame.DrawRight  :=False;
      Cabecalho.Frame.DrawBottom :=False;
      Detalhe.Frame.DrawLeft     :=False;
      Detalhe.Frame.DrawRight    :=False;
      Detalhe.Frame.DrawBottom   :=False;
  end;

  if dm.ServentiaCODIGO.AsInteger=4473 then
  begin
      dm.Codigos.Close;
      dm.Codigos.Params.ParamByName('OCULTO1').AsString:='N';
      dm.Codigos.Params.ParamByName('OCULTO2').AsString:='N';
      dm.Codigos.Open;
      dm.Codigos.Locate('COD',4022,[]);
      qrCustas.Caption:='*Custas R$:';
      lbAviso.Enabled :=True;
      lbAviso.Caption :='* J� inclu�do o valor de R$'+FloatToStrF(dm.CodigosTOT.AsFloat,ffNumber,7,2)+' referente a Certid�o.';
  end;

  if dm.ServentiaCODIGO.AsInteger=1237 then
  begin
      qrBoleto.Enabled:=False;
      lbBoleto.Enabled:=False;
  end;

  lbLimite.Enabled:=dm.ServentiaCODIGO.AsInteger=1866;

  if GR.Pergunta('DESEJA IMPRIMIR NA FOLHA DE SEGURAN�A')  then
  begin
      {if dm.vFlTiracabecalho = 'S' then
      begin
        Titulo.Enabled := False;
      end
      else
      begin
        Titulo.Enabled := True;
      end;}

      Cabecalho.Frame.DrawTop   := False;
      Cabecalho.Frame.DrawBottom:= False;
      Cabecalho.Frame.DrawLeft  := False;
      Cabecalho.Frame.DrawRight := False;

      Detalhe.Frame.DrawTop     := False;
      Detalhe.Frame.DrawBottom  := False;
      Detalhe.Frame.DrawLeft    := False;
      Detalhe.Frame.DrawRight   := False;

      QkIntimacao.Page.BottomMargin := StrToFloat(dm.vFlBottom);
      QkIntimacao.Page.TopMargin    := StrToFloat(dm.vFlTop);
      QkIntimacao.Page.LeftMargin   := StrToFloat(dm.vFlLeft);
      QkIntimacao.Page.RightMargin  := StrToFloat(dm.vFlRight);

      if dm.vFlFolha ='A4' then
      begin
        QkIntimacao.Page.Width := 210.0;
        QkIntimacao.Page.Length:= 297.0;
        Detalhe.Width := 160;
      end;

      if dm.vFlFolha ='Legal' then
      begin
        QkIntimacao.Page.Width := 215.9;
        QkIntimacao.Page.Length:= 355.6;
      end;
  end;

  GR.CriarForm(TFAssinatura,FAssinatura);
  lbAssinatura.Caption:=dm.vAssinatura;
  lbMatricula.Caption :=dm.vMatricula;

  if dm.ServentiaCODIGO.AsInteger=1513 then
  begin
      lbAssinatura.Caption:='O Oficial';
      lbMatricula.Caption:='';
  end;

  if dm.ServentiaCODIGO.AsInteger=1554 then
  begin
      lbBoleto.Caption    :='Valor para pagamento com Guia Banc�ria R$:';
      lbAssinatura.Caption:='Tabeli�o de Protesto';
      lbMatricula.Caption :='';
  end;

  if dm.ServentiaCODIGO.AsInteger=1726 then
    vGuia:=GR.Pergunta('IMPRIMIR GUIA PARA DEP�SITO')
      else vGuia:=dm.ValorParametro(47)='S';
end;

procedure TFQuickIntimacao1.CarregarRX(IdAto: Integer);
begin
  dm.Titulos.Close;
  dm.Titulos.Params[0].AsInteger:=IdAto;
  dm.Titulos.Open;

  PF.CarregarDevedor(dm.TitulosID_ATO.AsInteger);

  while not dm.RXDevedor.Eof do
  begin
      RX.Append;
      RXTitulo.AsString               :=PF.RetornarTitulo(dm.TitulosTIPO_TITULO.AsInteger);
      RXApresentante.AsString         :=dm.TitulosAPRESENTANTE.AsString;
      RXCredor.AsString               :=dm.TitulosCEDENTE.AsString;

      if PF.PortadorNominal(dm.TitulosCODIGO_APRESENTANTE.AsString,
                            dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,
                            dm.TitulosAPRESENTANTE.AsString) then
        begin
            RXNominal.AsString:=dm.ServentiaDESCRICAO.AsString;
            vNominal := true;
        end
        else
        begin
            RXNominal.AsString:=dm.TitulosAPRESENTANTE.AsString;
            vNominal := False;
        end;

      if dm.TitulosCPF_CNPJ_SACADOR.AsString<>'' then
        RXSacador.AsString:=dm.TitulosSACADOR.AsString+' - '+GR.FormatarCPFCNPJ(dm.TitulosCPF_CNPJ_SACADOR.AsString,'N')
          else RXSacador.AsString:=dm.TitulosSACADOR.AsString;

      if dm.TitulosSALDO_TITULO.AsFloat<>0 then
        RXValor.AsFloat:=dm.TitulosSALDO_TITULO.AsFloat
          else RXValor.AsFloat:=dm.TitulosVALOR_TITULO.AsFloat;

      RXCustas.AsFloat                :=dm.TitulosTOTAL.AsFloat-dm.TitulosTARIFA_BANCARIA.AsFloat;
      RXTarifa.AsFloat                :=StrToFloat(dm.ValorParametro(19));
      RXDistribuicao.AsFloat          :=dm.TitulosDISTRIBUICAO.AsFloat;
      RXSaldo.AsFloat                 :=dm.TitulosSALDO_PROTESTO.AsFloat;
      RXValorDeposito.AsFloat         :=RXValor.AsFloat+RXCustas.AsFloat+RXTarifa.AsFloat;
      RXValorCheque.AsFloat           :=IfThen((dm.ServentiaCODIGO.AsInteger=2354) and
                                               (dm.TitulosCODIGO_APRESENTANTE.AsString='901'),
                                               RXValor.AsFloat,RXValor.AsFloat+RXCustas.AsFloat);

      RXDevedor.AsString              :=dm.RxDevedorNOME.AsString;
      RXCPF_CNPJ_Apresentante.AsString:=dm.TitulosCPF_CNPJ_APRESENTANTE.AsString;
      RXCodigoApresentante.AsString   :=dm.TitulosCODIGO_APRESENTANTE.AsString;

      if (dm.TitulosDT_VENCIMENTO.AsDateTime=30/12/1899) or
         (dm.TitulosDT_VENCIMENTO.AsDateTime=0) or
         (dm.TitulosAVISTA.AsString='S') then
        RXVencimento.AsDateTime:=dm.TitulosDT_TITULO.AsDateTime
          else RXVencimento.AsDateTime:=dm.TitulosDT_VENCIMENTO.AsDateTime;
          
      RXConvenio.AsString             :=dm.TitulosCONVENIO.AsString;
      RXIntimacao.AsDateTime          :=dm.TitulosDT_INTIMACAO.AsDateTime;
      RXProtocolo.AsInteger           :=dm.TitulosPROTOCOLO.AsInteger;
      RXPraca.AsString                :=dm.TitulosPRACA_PROTESTO.AsString;

      if dm.TitulosDT_PRAZO.AsDateTime<>0 then
        RXDataPrazo.AsDateTime:=dm.TitulosDT_PRAZO.AsDateTime
          else
          begin
              dm.Titulos.Edit;
              dm.TitulosDT_PRAZO.AsDateTime:=PF.DataFinalFeriados(dm.TitulosDT_INTIMACAO.AsDateTime,StrToInt(dm.ValorParametro(5)));
              dm.Titulos.Post;
              dm.Titulos.ApplyUpdates(0);
              RXDataPrazo.AsDateTime:=dm.TitulosDT_PRAZO.AsDateTime;
          end;

      if dm.TitulosCPF_CNPJ_DEVEDOR.AsString<>'' then
        if dm.TitulosTIPO_DEVEDOR.AsString='F' then
          RXCPF_CNPJ_Devedor.AsString:=PF.FormatarCPF(dm.TitulosCPF_CNPJ_DEVEDOR.AsString)
            else
              if dm.TitulosTIPO_DEVEDOR.AsString='J' then
                RXCPF_CNPJ_Devedor.AsString:=PF.FormatarCNPJ(dm.TitulosCPF_CNPJ_DEVEDOR.AsString);

      RXFins.AsString             :=GR.iif(dm.TitulosFINS_FALIMENTARES.AsString='S','SIM','N�O');
      RXNumeroTitulo.AsString     :=dm.TitulosNUMERO_TITULO.AsString;
      RXDataProtocolo.AsDateTime  :=dm.TitulosDT_PROTOCOLO.AsDateTime;
      RXObservacao.AsString       :=dm.TitulosOBSERVACAO.AsString;
      RXDataIntimacao.AsDateTime  :=dm.TitulosDT_INTIMACAO.AsDateTime;
      RXMotivo.AsString           :=dm.TitulosMOTIVO_INTIMACAO.AsString;

      if dm.TitulosDT_TITULO.AsDateTime<>0 then
      RXEmissao.AsDateTime:=dm.TitulosDT_TITULO.AsDateTime;

      RXEndereco.AsString:=dm.RxDevedorENDERECO.AsString+' '+dm.RxDevedorBAIRRO.AsString+' '+
                           dm.RxDevedorMUNICIPIO.AsString+' '+dm.RxDevedorUF.AsString;

      if dm.RxDevedorCEP.AsString<>'' then
      RXCEP.AsString:='CEP: '+PF.FormatarCEP(dm.RxDevedorCEP.AsString);

      if dm.TitulosTIPO_INTIMACAO.AsString='P' then RXTipoIntimacao.AsString:='Pessoal';
      if dm.TitulosTIPO_INTIMACAO.AsString='E' then RXTipoIntimacao.AsString:='Edital';
      if dm.TitulosTIPO_INTIMACAO.AsString='C' then RXTipoIntimacao.AsString:='Carta';

      if dm.ServentiaCODIGO.AsInteger=4473 then
      begin
          dm.Codigos.Close;
          dm.Codigos.Params.ParamByName('OCULTO1').AsString:='N';
          dm.Codigos.Params.ParamByName('OCULTO2').AsString:='N';
          dm.Codigos.Open;
          dm.Codigos.Locate('COD',4022,[]);
          RXCustas.AsFloat        :=RXCustas.AsFloat+dm.CodigosTOT.AsFloat;
          RXValorCheque.AsFloat   :=RXValorCheque.AsFloat+dm.CodigosTOT.AsFloat;
          RXValorDeposito.AsFloat :=RXValorDeposito.AsFloat+dm.CodigosTOT.AsFloat;
      end;

      RX.Post;
      dm.RxDevedor.Next;
  end;
  RX.First;

  PF.Aguarde(False);
end;

procedure TFQuickIntimacao1.CabecalhoBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbTexto2.Caption:=StringReplace(lbTexto2.Caption,'[DATA]',
                    FormatDateTime('dd/mm/yyyy',RXDataProtocolo.AsDateTime),
                    [rfReplaceAll,rfIgnoreCase]);
end;

end.
