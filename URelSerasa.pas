unit URelSerasa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, sGroupBox, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, ExtCtrls, sPanel;

type
  TFRelSerasa = class(TForm)
    P1: TsPanel;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    RgTipo: TsRadioGroup;
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRelSerasa: TFRelSerasa;

implementation

{$R *.dfm}

end.
