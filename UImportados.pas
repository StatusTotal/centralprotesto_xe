unit UImportados;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FMTBcd, DB, DBClient, Provider, SqlExpr, Grids, Wwdbigrd,
  Wwdbgrid, ExtCtrls, sPanel, sDBNavigator, StdCtrls, Buttons, sBitBtn,
  sEdit, SimpleDS, Data.DBXFirebird, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFImportados = class(TForm)
    dsImportados: TDataSource;
    dspImportados: TDataSetProvider;
    Importados: TClientDataSet;
    ImportadosID_IMPORTADO: TIntegerField;
    ImportadosARQUIVO: TStringField;
    ImportadosDATA: TDateField;
    ImportadosTITULOS: TIntegerField;
    ImportadosACEITOS: TIntegerField;
    ImportadosREJEITADOS: TIntegerField;
    P: TsPanel;
    Grid: TwwDBGrid;
    edLocalizar: TsEdit;
    btSair: TsBitBtn;
    sDBNavigator1: TsDBNavigator;
    qrySelos_CCT: TFDQuery;
    qrySelos_CCTSELO_REGISTRO: TStringField;
    qrySelos_CCTSELO_PAGAMENTO: TStringField;
    qrySelos_CCTCCT: TStringField;
    qryImportados: TFDQuery;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btSairClick(Sender: TObject);
    procedure edLocalizarChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FImportados: TFImportados;

implementation

uses UDM, UPF, UGeral, UGDM;

{$R *.dfm}

procedure TFImportados.FormCreate(Sender: TObject);
begin
  Importados.Close;
  Importados.Open;
  Importados.Last;
end;

procedure TFImportados.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_ESCAPE then
    Close;
end;

procedure TFImportados.GridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  QryAux: TFDQuery;
  sNomeArquivo: String;
begin
  sNomeArquivo := '';

  if Importados.IsEmpty then
    Exit;

  if key = VK_DELETE then
  begin
    if not GR.Pergunta(PChar('CONFIRMA A EXCLUS�O DO ARQUIVO?' + #13#10 +
                             '** Essa a��o ir� excluir os T�tulos do banco de dados e ' +
                             'o aquivo de Confirma��o (se encontrado) e, por fim, ir� recalcular ' +
                             'o saldo de T�tulos enviado para cada Serventia Agregada ' +
                             '(caso o arquivo de Remessa D j� tenha sido gerado) **')) then
      Exit;

    sNomeArquivo := ImportadosARQUIVO.AsString;

    GR.CriarFDQuery(QryAux,'', dm.conSISTEMA);

//    if GR.Pergunta('EXCLUIR TAMB�M OS T�TULOS QUE VIERAM NESTE ARQUIVO') then
//    begin
      {if GR.Pergunta('DESEJA LIBERAR OS SEUS RESPECTIVOS SELOS E CCTs NO GERENCIAL') then
      begin
          PF.Aguarde(True);
          Selos_CCT.Close;
          Selos_CCT.DataSet.Params[0].AsString:=ImportadosARQUIVO.AsString;
          Selos_CCT.Open;
          Selos_CCT.Last;
          Selos_CCT.First;
          while not Selos_CCT.Eof do
          begin
              Application.ProcessMessages;
              PF.Aguarde(True,'Liberando: '+Selos_CCTCCT.AsString+' - '+IntToStr(Selos_CCT.RecNo)+'/'+IntToStr(Selos_CCT.RecordCount));
              Gdm.LiberarSelo('PROTESTO',Selos_CCTCCT.AsString,False);
              PF.Aguarde(True,'Liberando: '+Selos_CCTSELO_REGISTRO.AsString+' - '+IntToStr(Selos_CCT.RecNo)+'/'+IntToStr(Selos_CCT.RecordCount));
              Gdm.LiberarSelo('PROTESTO',Selos_CCTSELO_REGISTRO.AsString,False);
              PF.Aguarde(True,'Liberando: '+Selos_CCTSELO_PAGAMENTO.AsString+' - '+IntToStr(Selos_CCT.RecNo)+'/'+IntToStr(Selos_CCT.RecordCount));
              Gdm.LiberarSelo('PROTESTO',Selos_CCTSELO_PAGAMENTO.AsString,False);
              Selos_CCT.Next;
          end;
          Selos_CCT.Close;
      end; }

    PF.Aguarde(True,'Finalizando...');

    Application.ProcessMessages;

    GR.ExecutarSQLDAC('DELETE FROM EXPORTADO ' +
                     ' WHERE NOME_ARQUIVO_IMP = ' + QuotedStr(sNomeArquivo), dm.conSISTEMA);

    QryAux.Close;
    QryAux.SQL.Clear;
    QryAux.SQL.Text := 'SELECT ID_ATO ' +
                       '  FROM TITULOS ' +
                       ' WHERE ARQUIVO = :ARQUIVO';
    QryAux.Params.ParamByName('ARQUIVO').Value  := sNomeArquivo;
    QryAux.Open;

    QryAux.First;

    while not QryAux.Eof do
    begin
      GR.ExecutarSQLDAC('DELETE FROM DEVEDORES ' +
                       ' WHERE ID_ATO = ' + QryAux.FieldByName('ID_ATO').AsString, dm.conSISTEMA);

      GR.ExecutarSQLDAC('DELETE FROM CUSTAS ' +
                       ' WHERE ID_ATO = ' + QryAux.FieldByName('ID_ATO').AsString, dm.conSISTEMA);

      GR.ExecutarSQLDAC('DELETE FROM SACADORES ' +
                       ' WHERE ID_ATO = ' + QryAux.FieldByName('ID_ATO').AsString, dm.conSISTEMA);

      QryAux.Next;
    end;

//    end;

    dm.ServentiaAgregada.Close;
    dm.ServentiaAgregada.Open;

    dm.ServentiaAgregada.First;

    while not dm.ServentiaAgregada.Eof do
    begin
      QryAux.Close;
      QryAux.SQL.Clear;
      QryAux.SQL.Text := 'SELECT SUM(TOTAL) AS VR_TOTAL, ' +
                         '       COUNT(ID_ATO) AS QTD_TOTAL ' +
                         '  FROM TITULOS ' +
                         ' WHERE FLG_SALDO = ' + QuotedStr('S') +
                         '   AND ARQUIVO = :ARQUIVO ' +
                         '   AND CARTORIO = :CARTORIO';
      QryAux.Params.ParamByName('ARQUIVO').Value  := sNomeArquivo;
      QryAux.Params.ParamByName('CARTORIO').Value := dm.ServentiaAgregada.FieldByName('CODIGO_DISTRIBUICAO').AsInteger;
      QryAux.Open;

      if QryAux.RecordCount > 0 then
      begin
        dm.ServentiaAgregada.Edit;
        dm.ServentiaAgregada.FieldByName('QTD_ULTIMO_SALDO').AsInteger   := (dm.ServentiaAgregada.FieldByName('QTD_ULTIMO_SALDO').AsInteger -
                                                                             QryAux.FieldByName('QTD_TOTAL').AsInteger);
        dm.ServentiaAgregada.FieldByName('VALOR_ULTIMO_SALDO').AsFloat   := (dm.ServentiaAgregada.FieldByName('VALOR_ULTIMO_SALDO').AsFloat -
                                                                             QryAux.FieldByName('VR_TOTAL').AsFloat);
        dm.ServentiaAgregada.FieldByName('DATA_ULTIMO_SALDO').AsDateTime := Date;
        dm.ServentiaAgregada.Post;
      end;

      dm.ServentiaAgregada.Next;
    end;

    dm.ServentiaAgregada.ApplyUpdates(0);

    GR.ExecutarSQLDAC('DELETE FROM TITULOS ' +
                     ' WHERE ARQUIVO = ' + QuotedStr(sNomeArquivo), dm.conSISTEMA);

    FreeAndNil(QryAux);

{    if Copy(sNomeArquivo, 1, 1) = 'B' then
    begin
      if FileExists(dm.ValorParametro(3) + 'C' + Copy(sNomeArquivo, 2, (Length(sNomeArquivo) - 1))) then
        DeleteFile(dm.ValorParametro(3) + 'C' + Copy(sNomeArquivo, 2, (Length(sNomeArquivo) - 1)));
    end;  }

    Importados.Delete;
    Importados.ApplyUpdates(0);

    PF.Aguarde(False);
    GR.Aviso('EXCLUS�O EFETUADA COM SUCESSO!');
  end;
end;

procedure TFImportados.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFImportados.edLocalizarChange(Sender: TObject);
begin
  if Importados.IsEmpty then Exit;

  Importados.Locate('ARQUIVO',edLocalizar.Text,[loCaseInsensitive,loPartialKey]);
end;

end.
