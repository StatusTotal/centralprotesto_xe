unit UQuickDistribuidos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, DB, DBClient, SimpleDS,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFQuickDistribuidos = class(TForm)
    Titulos: TFDQuery;
    TitulosPROTOCOLO: TIntegerField;
    TitulosTOTAL: TFloatField;
    TitulosVALOR_TITULO: TFloatField;
    TitulosDEVEDOR: TStringField;
    TitulosSTATUS: TStringField;
    TitulosFETJ: TFloatField;
    TitulosMUAC: TFloatField;
    TitulosCARTORIO: TIntegerField;
    TitulosDESCRICAO: TStringField;
    Relatorio: TQuickRep;
    pghTitulosDistribuidos: TQRBand;
    lbCartorio: TQRLabel;
    lbEndereco: TQRLabel;
    lbPeriodo: TQRLabel;
    QRSysData2: TQRSysData;
    QRLabel7: TQRLabel;
    lbTabeliao: TQRLabel;
    QRLabel8: TQRLabel;
    hdServentiaA: TQRChildBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    gfServentiaA: TQRBand;
    dtServentiaA: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText5: TQRDBText;
    ghServentiaA: TQRGroup;
    QRDBText13: TQRDBText;
    TitulosID_ATO: TIntegerField;
    QRChildBand1: TQRChildBand;
    lblTotQuantidade: TQRLabel;
    lblTotVlrFETJ: TQRLabel;
    lblTotVlrMuAc: TQRLabel;
    lblTotVlrTitulos: TQRLabel;
    lblTotVlrCustas: TQRLabel;
    QRChildBand2: TQRChildBand;
    procedure FormCreate(Sender: TObject);
    procedure ghServentiaABeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure dtServentiaABeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure gfServentiaABeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure TitulosTOTALGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure TitulosVALOR_TITULOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure TitulosFETJGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure TitulosMUACGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    { Private declarations }

    iCodCartorio,
    iTotQuantidade: Integer;

    cTotVlrTitulos,
    cTotVlrCustas,
    cTotVlrFETJ,
    cTotVlrMuAc: Currency;
  public
    { Public declarations }
  end;

var
  FQuickDistribuidos: TFQuickDistribuidos;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFQuickDistribuidos.dtServentiaABeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if TitulosCARTORIO.AsInteger = iCodCartorio then
  begin
    Inc(iTotQuantidade);

    cTotVlrTitulos := (cTotVlrTitulos + TitulosVALOR_TITULO.AsCurrency);
    cTotVlrCustas  := (cTotVlrCustas + TitulosTOTAL.AsCurrency);
    cTotVlrFETJ    := (cTotVlrFETJ + TitulosFETJ.AsCurrency);
    cTotVlrMuAc    := (cTotVlrMuAc + TitulosMUAC.AsCurrency);
  end;
end;

procedure TFQuickDistribuidos.FormCreate(Sender: TObject);
begin
  lbCartorio.Caption := dm.ServentiaDESCRICAO.AsString;
  lbTabeliao.Caption := dm.ServentiaTABELIAO.AsString;
  lbEndereco.Caption := dm.ServentiaENDERECO.AsString;
end;

procedure TFQuickDistribuidos.gfServentiaABeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lblTotQuantidade.Caption := 'Quantidade de T�tulos: ' + IntToStr(iTotQuantidade);

  lblTotVlrTitulos.Caption := FormatCurr('#0.00', cTotVlrTitulos);
  lblTotVlrCustas.Caption  := FormatCurr('#0.00', cTotVlrCustas);
  lblTotVlrFETJ.Caption    := FormatCurr('#0.00', cTotVlrFETJ);
  lblTotVlrMuAc.Caption    := FormatCurr('#0.00', cTotVlrMuAc);
end;

procedure TFQuickDistribuidos.ghServentiaABeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  iCodCartorio := TitulosCARTORIO.AsInteger;

  iTotQuantidade := 0;

  cTotVlrTitulos := 0;
  cTotVlrCustas  := 0;
  cTotVlrFETJ    := 0;
  cTotVlrMuAc    := 0;
end;

procedure TFQuickDistribuidos.TitulosFETJGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickDistribuidos.TitulosMUACGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickDistribuidos.TitulosTOTALGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

procedure TFQuickDistribuidos.TitulosVALOR_TITULOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := FormatFloat('#0.00', Sender.AsFloat);
end;

end.
