unit USerasa;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Forms,
  Dialogs, Wwdbgrid, sBitBtn, Mask, Math,
  sTooledit, sRadioButton, sEdit, sGroupBox,
  sPanel, DB, SimpleDS, sMemo, Menus, QRCtrls, QuickRpt,
  ComCtrls, sStatusBar, sSpeedButton, sCurrEdit, sCurrencyEdit, sCheckBox, DateUtils,
  RxMemDS, DBClient, Grids, Wwdbigrd, sMaskEdit, sCustomComboEdit,
  StdCtrls, Buttons, ExtCtrls, jpeg, sSkinProvider, Data.DBXFirebird,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFSerasa = class(TForm)
    dsRX: TDataSource;
    P1: TsPanel;
    btFiltrar: TsBitBtn;
    GridAtos: TwwDBGrid;
    GbProtocolo: TsGroupBox;
    edProtFinal: TsEdit;
    edProtInicial: TsEdit;
    Rb1: TsRadioButton;
    GbData: TsGroupBox;
    edDataInicial: TsDateEdit;
    edDataFinal: TsDateEdit;
    Rb2: TsRadioButton;
    M: TsMemo;
    PM: TPopupMenu;
    MarcarTodos: TMenuItem;
    DesmarcarTodos: TMenuItem;
    Svd: TSaveDialog;
    qkModelo1: TQuickRep;
    Titulo: TQRBand;
    lbTabeliao: TQRLabel;
    lbCartorio: TQRLabel;
    QRLabel1: TQRLabel;
    lbEndereco: TQRLabel;
    Detalhe: TQRBand;
    ChildBand1: TQRChildBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    lbData: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    qrData: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    lbTelefone: TQRLabel;
    lbRelacao: TQRLabel;
    lbPeriodo: TQRLabel;
    Rodape: TQRBand;
    lbCidade: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText8: TQRDBText;
    lbSite: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    btCertidao: TsBitBtn;
    B: TsStatusBar;
    btExportar: TsSpeedButton;
    PM2: TPopupMenu;
    Serasa1: TMenuItem;
    PopEquifax: TMenuItem;
    edSerasa: TsCurrencyEdit;
    qrAssinatura: TQRShape;
    lbAssinatura: TQRLabel;
    lbDataVencimento: TQRLabel;
    QRDBText9: TQRDBText;
    ckFiltro: TsCheckBox;
    QRSysData1: TQRSysData;
    lbNatureza: TQRLabel;
    edBoaVista: TsCurrencyEdit;
    RX: TRxMemoryData;
    RXID_ATO: TIntegerField;
    RXProtocolo: TIntegerField;
    RXDevedor: TStringField;
    RXTipo_Devedor: TStringField;
    RXCPF_CNPJ_Devedor: TStringField;
    RXTipo_Titulo: TIntegerField;
    RXValor_Titulo: TFloatField;
    RXDt_Registro: TDateField;
    RXStatus: TStringField;
    RXAceite: TStringField;
    RXDt_Vencimento: TDateField;
    RXDt_Titulo: TDateField;
    RXMotivo: TStringField;
    RXCheck: TBooleanField;
    RXDt_Pagamento: TDateField;
    RXFins_Falimentares: TStringField;
    RXLivro_Protocolo: TIntegerField;
    RXFolha_Protocolo: TStringField;
    RXData: TDateField;
    RXSituacao: TStringField;
    RXTipo_Protesto: TIntegerField;
    lbQtd: TQRLabel;
    ckProtestados: TsCheckBox;
    RXLivro_Registro: TIntegerField;
    RXFolha_Registro: TStringField;
    btImprimir: TsSpeedButton;
    PM3: TPopupMenu;
    M1: TMenuItem;
    M2: TMenuItem;
    qkModelo2: TQuickRep;
    QRBand1: TQRBand;
    QRLabel18: TQRLabel;
    QRBand2: TQRBand;
    QRDBText4: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText17: TQRDBText;
    lbNatureza2: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRLabel22: TQRLabel;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRLabel23: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    lbEspecie: TQRLabel;
    lbTipoDoc: TQRLabel;
    QRDBText15: TQRDBText;
    QRLabel21: TQRLabel;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRLabel25: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRDBText16: TQRDBText;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    ChildBand2: TQRChildBand;
    RXEndereco_Devedor: TStringField;
    RXCidade_Devedor: TStringField;
    RXUF_Devedor: TStringField;
    RXCEP_Devedor: TStringField;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRLabel13: TQRLabel;
    lbProtesto: TQRLabel;
    lbCancelado: TQRLabel;
    QRImage1: TQRImage;
    lbInfo1: TQRLabel;
    lbInfo3: TQRLabel;
    lbInfo2: TQRLabel;
    RXApresentante: TStringField;
    QRDBText23: TQRDBText;
    lbTipoDoc2: TQRLabel;
    QRDBText24: TQRDBText;
    RXCPF_CNPJ_Apresentante: TStringField;
    RXTipo_Apresentante: TStringField;
    N1: TMenuItem;
    MarcarQuantidade: TMenuItem;
    RXSaldo_Titulo: TFloatField;
    ckAviso042015: TsCheckBox;
    sSpeedButton1: TsSpeedButton;
    sSkinProvider: TsSkinProvider;
    qryConsulta: TFDQuery;
    qryConsultaCheck: TBooleanField;
    qryConsultaID_ATO: TIntegerField;
    qryConsultaPROTOCOLO: TIntegerField;
    qryConsultaDT_PROTOCOLO: TDateField;
    qryConsultaDEVEDOR: TStringField;
    qryConsultaTIPO_DEVEDOR: TStringField;
    qryConsultaCPF_CNPJ_DEVEDOR: TStringField;
    qryConsultaTIPO_APRESENTANTE: TStringField;
    qryConsultaCPF_CNPJ_APRESENTANTE: TStringField;
    qryConsultaTIPO_TITULO: TIntegerField;
    qryConsultaVALOR_TITULO: TFloatField;
    qryConsultaSALDO_TITULO: TFloatField;
    qryConsultaDT_REGISTRO: TDateField;
    qryConsultaDT_PAGAMENTO: TDateField;
    qryConsultaSTATUS: TStringField;
    qryConsultaTIPO_PROTESTO: TIntegerField;
    qryConsultaACEITE: TStringField;
    qryConsultaDT_VENCIMENTO: TDateField;
    qryConsultaDT_TITULO: TDateField;
    qryConsultaFINS_FALIMENTARES: TStringField;
    qryConsultaLIVRO_PROTOCOLO: TIntegerField;
    qryConsultaFOLHA_PROTOCOLO: TStringField;
    qryConsultaLIVRO_REGISTRO: TIntegerField;
    qryConsultaFOLHA_REGISTRO: TStringField;
    qryConsultaCEDENTE: TStringField;
    qryConsultaAPRESENTANTE: TStringField;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btFiltrarClick(Sender: TObject);
    procedure edProtInicialExit(Sender: TObject);
    procedure GridAtosMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure FormClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Rb2Click(Sender: TObject);
    procedure Rb1Click(Sender: TObject);
    procedure MarcarTodosClick(Sender: TObject);
    procedure DesmarcarTodosClick(Sender: TObject);
    procedure TituloBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qkModelo1EndPage(Sender: TCustomQuickRep);
    procedure RodapeBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure btCertidaoClick(Sender: TObject);
    procedure SalvarCustas;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ckFiltroClick(Sender: TObject);
    procedure GerarArquivo(Orgao: String);
    procedure DetalheBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure Serasa1Click(Sender: TObject);
    procedure PopEquifaxClick(Sender: TObject);
    procedure RXFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    function Marcados: Integer;
    function Lavrados: Integer;
    function LavradosMarcados: Integer;
    function Cancelados: Integer;
    function CanceladosMarcados: Integer;
    procedure M1Click(Sender: TObject);
    procedure M2Click(Sender: TObject);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qkModelo2EndPage(Sender: TCustomQuickRep);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure FormShow(Sender: TObject);
    procedure MarcarQuantidadeClick(Sender: TObject);
    procedure ckAviso042015Click(Sender: TObject);
    procedure sSpeedButton1Click(Sender: TObject);
    procedure SvdShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSerasa    : TFSerasa;
  vPosicao   : Integer;
  vQuantidade: Integer;
  vStatus    : String;

implementation

uses UDM, UPF, UQuickLegendas, UQuickCertidaoCadastro1,
  UQuickReciboSerasaBV, UGeral, UAssinatura, UGDM, StrUtils;

{$R *.dfm}

function TFSerasa.CanceladosMarcados: Integer;
var
  X: Integer;
begin
  vPosicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  X:=0;
  while not RX.Eof do
  begin
      if (RXStatus.AsString='CANCELADO') and (RXCheck.AsBoolean) then
        Inc(X);
      RX.Next;
  end;
  RX.RecNo:=vPosicao;
  RX.EnableControls;
  Result:=X;
end;

function TFSerasa.LavradosMarcados: Integer;
var
  X: Integer;
begin
  vPosicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  X:=0;
  while not RX.Eof do
  begin
      if (RXStatus.AsString='LAVRADO') and (RXCheck.AsBoolean) then
        Inc(X);
      RX.Next;
  end;
  RX.RecNo:=vPosicao;
  RX.EnableControls;
  Result:=X;
end;

procedure TFSerasa.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
end;

procedure TFSerasa.btFiltrarClick(Sender: TObject);
var
  SQL,Query: String;
begin
  PF.Aguarde(True);

  RX.Close;
  RX.Open;

  SQL:='SELECT ID_ATO,PROTOCOLO,DT_PROTOCOLO,DEVEDOR,TIPO_DEVEDOR,CPF_CNPJ_DEVEDOR,TIPO_TITULO,'+
       'VALOR_TITULO,SALDO_TITULO,DT_REGISTRO,DT_PAGAMENTO,STATUS,TIPO_PROTESTO,ACEITE,DT_VENCIMENTO,'+
       'DT_TITULO,FINS_FALIMENTARES,LIVRO_PROTOCOLO,FOLHA_PROTOCOLO,LIVRO_REGISTRO,FOLHA_REGISTRO,'+
       'CEDENTE,APRESENTANTE,TIPO_APRESENTANTE,CPF_CNPJ_APRESENTANTE FROM TITULOS';

  if Rb1.Checked then
  begin
      if edProtInicial.Text='' then
      begin
          PF.Aguarde(False);
          GR.Aviso('Informe o Protocolo!');
          edProtInicial.SetFocus;
          Exit;
      end;

      {BUSCANDO SOMENTE OS PROTESTADOS}
      if ckFiltro.Checked then
        Query:=' WHERE (PROTOCOLO BETWEEN :P1 AND :P2) AND PROTESTADO='''+'S'+''' AND (TIPO_TITULO='''+'10'+''' OR TIPO_TITULO='''+'27'+''') ORDER BY PROTOCOLO'
          else Query:=' WHERE (PROTOCOLO BETWEEN :P1 AND :P2) AND PROTESTADO='''+'S'+''' ORDER BY PROTOCOLO';

      qryConsulta.Filtered:=False;
      if ckProtestados.Checked then
      begin
          qryConsulta.Filter:='STATUS='+QuotedStr('PROTESTADO');
          qryConsulta.Filtered:=True;
      end;

      dm.vDescontarMarcados:=0;
      dm.vDescontarProtestados:=0;
      dm.vDescontarCancelados:=0;
      qryConsulta.Close;
      qryConsulta.SQL.Text:=SQL+Query;
      qryConsulta.ParamByName('P1').AsInteger:=StrToInt(edProtInicial.Text);
      qryConsulta.ParamByName('P2').AsInteger:=StrToInt(edProtFinal.Text);
      qryConsulta.Open;
      qryConsulta.First;
      while not qryConsulta.Eof do
      begin
          PF.CarregarDevedor(qryConsultaID_ATO.AsInteger);
          dm.vDescontarMarcados:=dm.vDescontarMarcados+(dm.RxDevedor.RecordCount-1);
          dm.vDescontarProtestados:=dm.vDescontarProtestados+(dm.RxDevedor.RecordCount-1);
          while not dm.RxDevedor.Eof do
          begin
              RX.Append;
              RXID_ATO.AsInteger            :=qryConsultaID_ATO.AsInteger;
              RXProtocolo.AsInteger         :=qryConsultaPROTOCOLO.AsInteger;
              RXDevedor.AsString            :=dm.RxDevedorNOME.AsString;
              RXTipo_Devedor.AsString       :=dm.RxDevedorTIPO.AsString;
              RXEndereco_Devedor.AsString   :=dm.RxDevedorENDERECO.AsString;
              RXCidade_Devedor.AsString     :=dm.RxDevedorMUNICIPIO.AsString;
              RXUF_Devedor.AsString         :=dm.RxDevedorUF.AsString;
              RXCEP_Devedor.AsString        :=dm.RxDevedorCEP.AsString;
              RXData.AsDateTime             :=qryConsultaDT_REGISTRO.AsDateTime;
              RXAceite.AsString             :=qryConsultaACEITE.AsString;
              RXFins_Falimentares.AsString  :=qryConsultaFINS_FALIMENTARES.AsString;
              RXLivro_Protocolo.AsInteger   :=qryConsultaLIVRO_PROTOCOLO.AsInteger;
              RXLivro_Registro.AsInteger    :=qryConsultaLIVRO_REGISTRO.AsInteger;
              RXFolha_Registro.AsString     :=qryConsultaFOLHA_REGISTRO.AsString;
              RXFolha_Protocolo.AsString    :=qryConsultaFOLHA_PROTOCOLO.AsString;
              RXTipo_Titulo.AsInteger       :=qryConsultaTIPO_TITULO.AsInteger;
              RXTipo_Protesto.AsInteger     :=qryConsultaTIPO_PROTESTO.AsInteger;
              RXTipo_Apresentante.AsString  :=qryConsultaTIPO_APRESENTANTE.AsString;
              RXApresentante.AsString       :=qryConsultaAPRESENTANTE.AsString;

              if qryConsultaTIPO_APRESENTANTE.AsString='F' then
                RXCPF_CNPJ_Apresentante.AsString:=PF.FormatarCPF(qryConsultaCPF_CNPJ_APRESENTANTE.AsString)
                  else RXCPF_CNPJ_Apresentante.AsString:=PF.FormatarCNPJ(qryConsultaCPF_CNPJ_APRESENTANTE.AsString);

              if dm.RxDevedorTIPO.AsString<>'' then
                if dm.RxDevedorTIPO.AsString='F' then
                  RXCPF_CNPJ_Devedor.AsString:=PF.FormatarCPF(dm.RxDevedorDOCUMENTO.AsString)
                    else RXCPF_CNPJ_Devedor.AsString:=PF.FormatarCNPJ(dm.RxDevedorDOCUMENTO.AsString);

              if (qryConsultaTIPO_TITULO.AsInteger=10) or (qryConsultaTIPO_TITULO.AsInteger=27) then
              begin
                  if qryConsultaDT_TITULO.AsDateTime<>0 then
                    RXDt_Vencimento.AsDateTime:=qryConsultaDT_TITULO.AsDateTime
                      else
                        if qryConsultaDT_VENCIMENTO.AsDateTime<>0 then
                          RXDt_Vencimento.AsDateTime:=qryConsultaDT_VENCIMENTO.AsDateTime;
              end
              else
              begin
                  if qryConsultaDT_VENCIMENTO.AsDateTime<>0 then
                    RXDt_Vencimento.AsDateTime:=qryConsultaDT_VENCIMENTO.AsDateTime;
              end;

              RXValor_Titulo.AsFloat  :=qryConsultaVALOR_TITULO.AsFloat;
              RXSaldo_Titulo.AsFloat  :=qryConsultaSALDO_TITULO.AsFloat;
              RXDt_Registro.AsDateTime:=qryConsultaDT_REGISTRO.AsDateTime;
              RXStatus.AsString       :='LAVRADO';
              RXDt_Titulo.AsDateTime  :=qryConsultaDT_TITULO.AsDateTime;

              case qryConsultaTIPO_PROTESTO.AsInteger of
                1: RXSituacao.AsString:='� Pagamento';
                2: RXSituacao.AsString:='� Aceite';
                3: RXSituacao.AsString:='� Devolu��o';
              end;

              case qryConsultaTIPO_PROTESTO.AsInteger of
                1: RXMotivo.AsString:='PFP'; {N�O PAGAMENTO}
                2: RXMotivo.AsString:='PFA'; {N�O ACEITE}
                3: RXMotivo.AsString:='PFD'; {N�O DEVOLU��O}
              end;
              dm.RxDevedor.Next;
          end;
          qryConsulta.Next;
      end;

      {BUSCANDO SOMENTE OS CANCELADOS}
      if not ckFiltro.Checked then
      begin
          Query:=' WHERE (PROTOCOLO BETWEEN :P1 AND :P2) AND STATUS='''+'CANCELADO'+''' ORDER BY PROTOCOLO';
          qryConsulta.Close;
          qryConsulta.SQL.Text:=SQL+Query;
          qryConsulta.ParamByName('P1').AsInteger:=StrToInt(edProtInicial.Text);
          qryConsulta.ParamByName('P2').AsInteger:=StrToInt(edProtFinal.Text);
          qryConsulta.Open;
          qryConsulta.First;
          while not qryConsulta.Eof do
          begin
              PF.CarregarDevedor(qryConsultaID_ATO.AsInteger);
              dm.vDescontarMarcados:=dm.vDescontarMarcados+(dm.RxDevedor.RecordCount-1);
              dm.vDescontarProtestados:=dm.vDescontarProtestados+(dm.RxDevedor.RecordCount-1);
              while not dm.RxDevedor.Eof do
              begin
                  RX.Append;
                  RXID_ATO.AsInteger            :=qryConsultaID_ATO.AsInteger;
                  RXProtocolo.AsInteger         :=qryConsultaPROTOCOLO.AsInteger;
                  RXDevedor.AsString            :=dm.RxDevedorNOME.AsString;
                  RXTipo_Devedor.AsString       :=dm.RxDevedorTIPO.AsString;
                  RXEndereco_Devedor.AsString   :=dm.RxDevedorENDERECO.AsString;
                  RXCidade_Devedor.AsString     :=dm.RxDevedorMUNICIPIO.AsString;
                  RXUF_Devedor.AsString         :=dm.RxDevedorUF.AsString;
                  RXCEP_Devedor.AsString        :=dm.RxDevedorCEP.AsString;
                  RXData.AsDateTime             :=qryConsultaDT_PAGAMENTO.AsDateTime;
                  RXAceite.AsString             :=qryConsultaACEITE.AsString;
                  RXFins_Falimentares.AsString  :=qryConsultaFINS_FALIMENTARES.AsString;
                  RXLivro_Protocolo.AsInteger   :=qryConsultaLIVRO_PROTOCOLO.AsInteger;
                  RXFolha_Protocolo.AsString    :=qryConsultaFOLHA_PROTOCOLO.AsString;
                  RXTipo_Titulo.AsInteger       :=qryConsultaTIPO_TITULO.AsInteger;
                  RXTipo_Protesto.AsInteger     :=qryConsultaTIPO_PROTESTO.AsInteger;
                  RXTipo_Apresentante.AsString  :=qryConsultaTIPO_APRESENTANTE.AsString;
                  RXApresentante.AsString       :=qryConsultaAPRESENTANTE.AsString;

                  if qryConsultaTIPO_APRESENTANTE.AsString<>'' then
                    if qryConsultaTIPO_APRESENTANTE.AsString='F' then
                      RXCPF_CNPJ_Apresentante.AsString:=PF.FormatarCPF(qryConsultaCPF_CNPJ_APRESENTANTE.AsString)
                        else RXCPF_CNPJ_Apresentante.AsString:=PF.FormatarCNPJ(qryConsultaCPF_CNPJ_APRESENTANTE.AsString);

                  if dm.RxDevedorTIPO.AsString<>'' then
                    if dm.RxDevedorTIPO.AsString='F' then
                      RXCPF_CNPJ_Devedor.AsString:=PF.FormatarCPF(dm.RxDevedorDOCUMENTO.AsString)
                        else RXCPF_CNPJ_Devedor.AsString:=PF.FormatarCNPJ(dm.RxDevedorDOCUMENTO.AsString);

                  if (qryConsultaTIPO_TITULO.AsInteger=10) or (qryConsultaTIPO_TITULO.AsInteger=27) then
                  begin
                      if qryConsultaDT_TITULO.AsDateTime<>0 then
                        RXDt_Vencimento.AsDateTime:=qryConsultaDT_TITULO.AsDateTime
                          else
                            if qryConsultaDT_VENCIMENTO.AsDateTime<>0 then
                              RXDt_Vencimento.AsDateTime:=qryConsultaDT_VENCIMENTO.AsDateTime;
                  end
                  else
                  begin
                      if qryConsultaDT_VENCIMENTO.AsDateTime<>0 then
                        RXDt_Vencimento.AsDateTime:=qryConsultaDT_VENCIMENTO.AsDateTime;
                  end;

                  RXValor_Titulo.AsFloat    :=qryConsultaVALOR_TITULO.AsFloat;
                  RXSaldo_Titulo.AsFloat    :=qryConsultaSALDO_TITULO.AsFloat;
                  RXDt_Registro.AsDateTime  :=qryConsultaDT_REGISTRO.AsDateTime;
                  RXDt_Pagamento.AsDateTime :=qryConsultaDT_PAGAMENTO.AsDateTime;
                  RXStatus.AsString         :='CANCELADO';
                  RXDt_Titulo.AsDateTime    :=qryConsultaDT_TITULO.AsDateTime;

                  case qryConsultaTIPO_PROTESTO.AsInteger of
                    1: RXSituacao.AsString:='� Pagamento';
                    2: RXSituacao.AsString:='� Aceite';
                    3: RXSituacao.AsString:='� Devolu��o';
                  end;

                  case qryConsultaTIPO_PROTESTO.AsInteger of
                    1: RXMotivo.AsString:='PFP'; {N�O PAGAMENTO}
                    2: RXMotivo.AsString:='PFA'; {N�O ACEITE}
                    3: RXMotivo.AsString:='PFD'; {N�O DEVOLU��O}
                  end;
                  dm.RxDevedor.Next;
              end;
              qryConsulta.Next;
          end;
      end;
  end;

  if Rb2.Checked then
  begin
      if ckAviso042015.Checked then
        Query:=' WHERE (DT_REGISTRO BETWEEN :D1 AND :D2) AND PROTESTADO='''+'S'+''' AND ('+
               'TIPO_TITULO=''18'' OR TIPO_TITULO=''25'' OR TIPO_TITULO=''41'' OR '+
               'TIPO_TITULO=''42'' OR TIPO_TITULO=''44'' OR TIPO_TITULO=''46'' OR '+
               'TIPO_TITULO=''47'' OR TIPO_TITULO=''48'' OR TIPO_TITULO=''49'' OR '+
               'TIPO_TITULO=''50'' OR TIPO_TITULO=''51'' OR TIPO_TITULO=''52'' OR '+
               'TIPO_TITULO=''53'' OR TIPO_TITULO=''54'' OR TIPO_TITULO=''55'' OR '+
               'TIPO_TITULO=''56'' OR TIPO_TITULO=''58'' OR TIPO_TITULO=''59'' OR '+
               'TIPO_TITULO=''60'') AND STATUS=''PROTESTADO'' ORDER BY PROTOCOLO'
          else
            {BUSCANDO SOMENTE OS PROTESTADOS}
            if ckFiltro.Checked then
              Query:=' WHERE (DT_REGISTRO BETWEEN :D1 AND :D2) AND PROTESTADO='''+'S'+''' AND (TIPO_TITULO='''+'10'+''' OR TIPO_TITULO='''+'27'+''') ORDER BY PROTOCOLO'
                else Query:=' WHERE (DT_REGISTRO BETWEEN :D1 AND :D2) AND PROTESTADO='''+'S'+''' ORDER BY PROTOCOLO';

      qryConsulta.Filtered:=False;
      if ckProtestados.Checked then
      begin
          qryConsulta.Filter:='STATUS='+QuotedStr('PROTESTADO');
          qryConsulta.Filtered:=True;
      end;

      dm.vDescontarMarcados:=0;
      dm.vDescontarProtestados:=0;
      dm.vDescontarCancelados:=0;
      qryConsulta.Close;
      qryConsulta.SQL.Text:=SQL+Query;
      qryConsulta.ParamByName('D1').AsDate:=edDataInicial.Date;
      qryConsulta.ParamByName('D2').AsDate:=edDataFinal.Date;
      qryConsulta.Open;
      qryConsulta.First;
      while not qryConsulta.Eof do
      begin
          PF.CarregarDevedor(qryConsultaID_ATO.AsInteger);
          dm.vDescontarMarcados:=dm.vDescontarMarcados+(dm.RxDevedor.RecordCount-1);
          dm.vDescontarProtestados:=dm.vDescontarProtestados+(dm.RxDevedor.RecordCount-1);
          while not dm.RxDevedor.Eof do
          begin
              RX.Append;
              RXID_ATO.AsInteger            :=qryConsultaID_ATO.AsInteger;
              RXProtocolo.AsInteger         :=qryConsultaPROTOCOLO.AsInteger;
              RXDevedor.AsString            :=dm.RxDevedorNOME.AsString;
              RXTipo_Devedor.AsString       :=dm.RxDevedorTIPO.AsString;
              RXEndereco_Devedor.AsString   :=dm.RxDevedorENDERECO.AsString;
              RXCidade_Devedor.AsString     :=dm.RxDevedorMUNICIPIO.AsString;
              RXUF_Devedor.AsString         :=dm.RxDevedorUF.AsString;
              RXCEP_Devedor.AsString        :=dm.RxDevedorCEP.AsString;
              RXData.AsDateTime             :=qryConsultaDT_REGISTRO.AsDateTime;
              RXAceite.AsString             :=qryConsultaACEITE.AsString;
              RXFins_Falimentares.AsString  :=qryConsultaFINS_FALIMENTARES.AsString;
              RXLivro_Protocolo.AsInteger   :=qryConsultaLIVRO_PROTOCOLO.AsInteger;
              RXFolha_Protocolo.AsString    :=qryConsultaFOLHA_PROTOCOLO.AsString;
              RXLivro_Registro.AsInteger    :=qryConsultaLIVRO_REGISTRO.AsInteger;
              RXFolha_Registro.AsString     :=qryConsultaFOLHA_REGISTRO.AsString;
              RXTipo_Protesto.AsInteger     :=qryConsultaTIPO_PROTESTO.AsInteger;
              RXTipo_Apresentante.AsString  :=qryConsultaTIPO_APRESENTANTE.AsString;
              RXApresentante.AsString       :=qryConsultaAPRESENTANTE.AsString;

              if ckAviso042015.Checked then
              begin
                  if (qryConsultaTIPO_TITULO.AsInteger=42) or (qryConsultaTIPO_TITULO.AsInteger=44) or (qryConsultaTIPO_TITULO.AsInteger=46) then
                    RXTipo_Titulo.AsInteger:=-1
                      else RXTipo_Titulo.AsInteger:=qryConsultaTIPO_TITULO.AsInteger;
              end
              else
                RXTipo_Titulo.AsInteger:=qryConsultaTIPO_TITULO.AsInteger;

              if qryConsultaTIPO_APRESENTANTE.AsString<>'' then
                if qryConsultaTIPO_APRESENTANTE.AsString='F' then
                  RXCPF_CNPJ_Apresentante.AsString:=PF.FormatarCPF(qryConsultaCPF_CNPJ_APRESENTANTE.AsString)
                    else RXCPF_CNPJ_Apresentante.AsString:=PF.FormatarCNPJ(qryConsultaCPF_CNPJ_APRESENTANTE.AsString);

              if dm.RxDevedorTIPO.AsString<>'' then
                if dm.RxDevedorTIPO.AsString='F' then
                  RXCPF_CNPJ_Devedor.AsString:=PF.FormatarCPF(dm.RxDevedorDOCUMENTO.AsString)
                    else RXCPF_CNPJ_Devedor.AsString:=PF.FormatarCNPJ(dm.RxDevedorDOCUMENTO.AsString);

              if (qryConsultaTIPO_TITULO.AsInteger=10) or (qryConsultaTIPO_TITULO.AsInteger=27) then
              begin
                  if qryConsultaDT_TITULO.AsDateTime<>0 then
                    RXDt_Vencimento.AsDateTime:=qryConsultaDT_TITULO.AsDateTime
                      else
                        if qryConsultaDT_VENCIMENTO.AsDateTime<>0 then
                          RXDt_Vencimento.AsDateTime:=qryConsultaDT_VENCIMENTO.AsDateTime;
              end
              else
              begin
                  if qryConsultaDT_VENCIMENTO.AsDateTime<>0 then
                    RXDt_Vencimento.AsDateTime:=qryConsultaDT_VENCIMENTO.AsDateTime;
              end;

              RXValor_Titulo.AsFloat  :=qryConsultaVALOR_TITULO.AsFloat;
              RXSaldo_Titulo.AsFloat  :=qryConsultaSALDO_TITULO.AsFloat;//IfThen(ckAviso042015.Checked,0,ConsultaSALDO_TITULO.AsFloat);
              RXDt_Registro.AsDateTime:=qryConsultaDT_REGISTRO.AsDateTime;
              RXStatus.AsString       :='LAVRADO';
              RXDt_Titulo.AsDateTime  :=qryConsultaDT_TITULO.AsDateTime;

              case qryConsultaTIPO_PROTESTO.AsInteger of
                1: RXSituacao.AsString:='� Pagamento';
                2: RXSituacao.AsString:='� Aceite';
                3: RXSituacao.AsString:='� Devolu��o';
              end;

              case qryConsultaTIPO_PROTESTO.AsInteger of
                1: RXMotivo.AsString:='PFP'; {N�O PAGAMENTO}
                2: RXMotivo.AsString:='PFA'; {N�O ACEITE}
                3: RXMotivo.AsString:='PFD'; {N�O DEVOLU��O}
              end;
              dm.RxDevedor.Next;
          end;
          qryConsulta.Next;
      end;

      if ckAviso042015.Checked then MarcarTodosClick(Sender);

      {BUSCANDO SOMENTE OS CANCELADOS}
      if not ckAviso042015.Checked then
        if not ckFiltro.Checked then
        begin
            Query:=' WHERE (DT_PAGAMENTO BETWEEN :D1 AND :D2) AND STATUS='''+'CANCELADO'+''' ORDER BY PROTOCOLO';
            qryConsulta.Close;
            qryConsulta.SQL.Text:=SQL+Query;
            qryConsulta.ParamByName('D1').AsDate:=edDataInicial.Date;
            qryConsulta.ParamByName('D2').AsDate:=edDataFinal.Date;
            qryConsulta.Open;
            qryConsulta.First;
            while not qryConsulta.Eof do
            begin
                PF.CarregarDevedor(qryConsultaID_ATO.AsInteger);
                dm.vDescontarMarcados:=dm.vDescontarMarcados+(dm.RxDevedor.RecordCount-1);
                dm.vDescontarCancelados:=dm.vDescontarCancelados+(dm.RxDevedor.RecordCount-1);
                while not dm.RxDevedor.Eof do
                begin
                    RX.Append;
                    RXID_ATO.AsInteger            :=qryConsultaID_ATO.AsInteger;
                    RXProtocolo.AsInteger         :=qryConsultaPROTOCOLO.AsInteger;
                    RXDevedor.AsString            :=dm.RxDevedorNOME.AsString;
                    RXTipo_Devedor.AsString       :=dm.RxDevedorTIPO.AsString;
                    RXEndereco_Devedor.AsString   :=dm.RxDevedorENDERECO.AsString;
                    RXCidade_Devedor.AsString     :=dm.RxDevedorMUNICIPIO.AsString;
                    RXUF_Devedor.AsString         :=dm.RxDevedorUF.AsString;
                    RXCEP_Devedor.AsString        :=dm.RxDevedorCEP.AsString;
                    RXData.AsDateTime             :=qryConsultaDT_PAGAMENTO.AsDateTime;
                    RXAceite.AsString             :=qryConsultaACEITE.AsString;
                    RXFins_Falimentares.AsString  :=qryConsultaFINS_FALIMENTARES.AsString;
                    RXLivro_Protocolo.AsInteger   :=qryConsultaLIVRO_PROTOCOLO.AsInteger;
                    RXFolha_Protocolo.AsString    :=qryConsultaFOLHA_PROTOCOLO.AsString;
                    RXTipo_Titulo.AsInteger       :=qryConsultaTIPO_TITULO.AsInteger;
                    RXTipo_Protesto.AsInteger     :=qryConsultaTIPO_PROTESTO.AsInteger;
                    RXTipo_Apresentante.AsString  :=qryConsultaTIPO_APRESENTANTE.AsString;
                    RXApresentante.AsString       :=qryConsultaAPRESENTANTE.AsString;

                    if qryConsultaTIPO_APRESENTANTE.AsString<>'' then
                      if qryConsultaTIPO_APRESENTANTE.AsString='F' then
                        RXCPF_CNPJ_Apresentante.AsString:=PF.FormatarCPF(qryConsultaCPF_CNPJ_APRESENTANTE.AsString)
                          else RXCPF_CNPJ_Apresentante.AsString:=PF.FormatarCNPJ(qryConsultaCPF_CNPJ_APRESENTANTE.AsString);

                    if dm.RxDevedorTIPO.AsString<>'' then
                      if dm.RxDevedorTIPO.AsString='F' then
                        RXCPF_CNPJ_Devedor.AsString:=PF.FormatarCPF(dm.RxDevedorDOCUMENTO.AsString)
                          else RXCPF_CNPJ_Devedor.AsString:=PF.FormatarCNPJ(dm.RxDevedorDOCUMENTO.AsString);

                    if (qryConsultaTIPO_TITULO.AsInteger=10) or (qryConsultaTIPO_TITULO.AsInteger=27) then
                    begin
                        if qryConsultaDT_TITULO.AsDateTime<>0 then
                          RXDt_Vencimento.AsDateTime:=qryConsultaDT_TITULO.AsDateTime
                            else
                              if qryConsultaDT_VENCIMENTO.AsDateTime<>0 then
                                RXDt_Vencimento.AsDateTime:=qryConsultaDT_VENCIMENTO.AsDateTime;
                    end
                    else
                    begin
                        if qryConsultaDT_VENCIMENTO.AsDateTime<>0 then
                          RXDt_Vencimento.AsDateTime:=qryConsultaDT_VENCIMENTO.AsDateTime;
                    end;

                    RXValor_Titulo.AsFloat    :=qryConsultaVALOR_TITULO.AsFloat;
                    RXSaldo_Titulo.AsFloat    :=qryConsultaSALDO_TITULO.AsFloat;
                    RXDt_Registro.AsDateTime  :=qryConsultaDT_REGISTRO.AsDateTime;
                    RXDt_Pagamento.AsDateTime :=qryConsultaDT_PAGAMENTO.AsDateTime;
                    RXStatus.AsString         :='CANCELADO';
                    RXDt_Titulo.AsDateTime    :=qryConsultaDT_TITULO.AsDateTime;

                    case qryConsultaTIPO_PROTESTO.AsInteger of
                      1: RXSituacao.AsString:='� Pagamento';
                      2: RXSituacao.AsString:='� Aceite';
                      3: RXSituacao.AsString:='� Devolu��o';
                    end;

                    case qryConsultaTIPO_PROTESTO.AsInteger of
                      1: RXMotivo.AsString:='PFP'; {N�O PAGAMENTO}
                      2: RXMotivo.AsString:='PFA'; {N�O ACEITE}
                      3: RXMotivo.AsString:='PFD'; {N�O DEVOLU��O}
                    end;
                    dm.RxDevedor.Next;
                end;
                qryConsulta.Next;
            end;
        end;
  end;

  btImprimir.Enabled:=not RX.IsEmpty;
  btExportar.Enabled:=not RX.IsEmpty;
  B.Panels[1].Text  :=IntToStr(Lavrados{-dm.vDescontarProtestados});
  B.Panels[3].Text  :=IntToStr(Cancelados{-dm.vDescontarCancelados});

  RX.SortOnFields('PROTOCOLO');
  RX.First;

  PF.Aguarde(False);
end;

procedure TFSerasa.edProtInicialExit(Sender: TObject);
begin
  if edProtFinal.Text='' then edProtFinal.Text:=edProtInicial.Text;
end;

procedure TFSerasa.GridAtosMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  if (X>0) and (X<34) then
    Screen.Cursor:=crHandPoint
      else Screen.Cursor:=crDefault;
end;

procedure TFSerasa.FormClick(Sender: TObject);
begin
  if ActiveControl=GridAtos then
    if Screen.Cursor=-21 then
    begin
        if RX.IsEmpty then Exit;

        RX.Edit;
        if RXCheck.AsString='S' then
          RXCheck.AsString:='N'
            else RXCheck.AsString:='N';
        RX.Post;
    end;
end;

procedure TFSerasa.FormCreate(Sender: TObject);
begin
  if dm.ServentiaCODIGO.AsInteger=2353 then
  begin
      lbAssinatura.Enabled:=True;
      qrAssinatura.Enabled:=True;
      lbAssinatura.Caption:=dm.ServentiaTABELIAO.AsString;
  end;
  edSerasa.Text   :=dm.ValorParametro(37);
  edBoaVista.Text :=dm.ValorParametro(78);
end;

procedure TFSerasa.Rb2Click(Sender: TObject);
begin
  GbData.Enabled:=True;
  GbProtocolo.Enabled:=False;
  edProtInicial.Clear;
  edProtFinal.Clear;
  edDataInicial.SetFocus;
end;

procedure TFSerasa.Rb1Click(Sender: TObject);
begin
  btImprimir.Enabled:=False;
  GbData.Enabled:=False;
  GbProtocolo.Enabled:=True;
  edDataInicial.Clear;
  edDataFinal.Clear;
  edProtInicial.SetFocus;
end;

procedure TFSerasa.MarcarTodosClick(Sender: TObject);
var
  Posicao: Integer;
begin
  PF.Aguarde(True);
  Posicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  while not RX.Eof do
  begin
      Application.ProcessMessages;
      RX.Edit;
      RXCheck.AsBoolean:=True;
      RX.Post;
      RX.Next;
  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;
  PF.Aguarde(False);
end;

procedure TFSerasa.DesmarcarTodosClick(Sender: TObject);
var
  Posicao: Integer;
begin
  PF.Aguarde(True);
  Posicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  while not RX.Eof do
  begin
      Application.ProcessMessages;
      RX.Edit;
      RXCheck.AsBoolean:=False;
      RX.Post;
      RX.Next;
  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;
  PF.Aguarde(False);
end;

procedure TFSerasa.TituloBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  lbCartorio.Caption:=dm.ServentiaDESCRICAO.AsString;
  lbTabeliao.Caption:=dm.ServentiaTABELIAO.AsString+' - '+dm.ServentiaTITULO.AsString;
  lbEndereco.Caption:=dm.ServentiaENDERECO.AsString+' - '+dm.ServentiaBAIRRO.AsString+' - Cep: '+dm.ServentiaCEP.AsString;
  lbTelefone.Caption:=dm.ServentiaEMAIL.AsString+' - '+'('+Copy(dm.ServentiaTELEFONE.AsString,1,2)+')'+Copy(dm.ServentiaTELEFONE.AsString,3,4)+'-'+Copy(dm.ServentiaTELEFONE.AsString,7,4);
  lbPeriodo.Caption :='Per�odo: '+edDataInicial.Text+' a '+edDataFinal.Text;
  lbSite.Caption    :=dm.ServentiaSITE.AsString;
end;

procedure TFSerasa.qkModelo1EndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFSerasa.RodapeBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbCidade.Caption:=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',Now);
  lbQtd.Caption   :='QUANTIDADE DE T�TULOS: '+IntToStr(vQuantidade);
  vQuantidade     :=0;
end;

procedure TFSerasa.btCertidaoClick(Sender: TObject);
var
  vComuns,vEmolGeral,vEmolItem: Double;
begin
  (**if not Gdm.QtdSeloDisponivel(dm.SerieAtual,1) then
  begin
      GR.Aviso('SELO N�O DISPON�VEL!');
      Exit;
  end;

  {if (LavradosMarcados=0) and (CanceladosMarcados=0) then
  begin
      GR.Aviso('NENHUM ATO SELECIONADO!');
      Exit;
  end;}

  PF.Aguarde(True);

  if not RX.Active then
  begin
      RX.Close;
      RX.Open;
  end;

  if StrToInt(dm.ValorParametro(14))<>0 then
    dm.vRecibo:=dm.ValorParametro(14)
      else dm.vRecibo:=dm.ValorParametro(11);

  dm.vDestino     :='S';
  dm.vDataInicial :=edDataInicial.Text;
  dm.vDataFinal   :=edDataFinal.Text;
  dm.vMarcados    :=IntToStr(Marcados);
  dm.vCancelados  :=IntToStr(CanceladosMarcados);
  dm.vProtestados :=IntToStr(LavradosMarcados);

  dm.vDescontarMarcados   :=Marcados-dm.vDescontarMarcados;
  dm.vDescontarProtestados:=IfThen(dm.vDescontarProtestados>0,LavradosMarcados-dm.vDescontarProtestados,LavradosMarcados);
  dm.vDescontarCancelados :=IfThen(dm.vDescontarCancelados>0,CanceladosMarcados-dm.vDescontarCancelados,CanceladosMarcados);

  dm.Itens.Close;
  dm.Itens.Params[0].AsInteger:=4011;
  dm.Itens.Open;
  dm.Itens.First;

  vComuns:=0;
  vEmolGeral:=0;
  dm.RxCustas.Close;
  dm.RxCustas.Open;
  while not dm.Itens.Eof do
  begin
      dm.RxCustas.Append;
      dm.RxCustasTABELA.AsString    :=dm.ItensTAB.AsString;
      dm.RxCustasITEM.AsString      :=dm.ItensITEM.AsString;
      dm.RxCustasSUBITEM.AsString   :=dm.ItensSUB.AsString;
      dm.RxCustasVALOR.AsFloat      :=dm.ItensVALOR.AsFloat;
      dm.RxCustasQTD.AsString       :='1';
      dm.RxCustasDESCRICAO.AsString :=dm.ItensDESCR.AsString;
      dm.RxCustasTOTAL.AsFloat      :=dm.RxCustasVALOR.AsFloat*dm.RxCustasQTD.AsInteger;
      dm.RxCustas.Post;
      vEmolGeral:=vEmolGeral+GR.NoRound(dm.RxCustasTOTAL.AsFloat,2);

      if (dm.ItensTAB.AsString='1') or (dm.ItensTAB.AsString='16') then
      vComuns:=vComuns+dm.RxCustasTOTAL.AsFloat;

      dm.Itens.Next;
  end;

  if dm.vMarcados<>'0' then
  begin
      dm.Subitem.Close;
      dm.Subitem.Params[0].AsString:='24';
      dm.Subitem.Params[1].AsString:='3';
      dm.Subitem.Params[2].AsString:='2';
      dm.Subitem.Params[3].AsInteger:=StrToInt(dm.ValorParametro(77));
      dm.Subitem.Open;

      dm.RxCustas.Append;
      dm.RxCustasTABELA.AsString    :='24';
      dm.RxCustasITEM.AsString      :='3';
      dm.RxCustasSUBITEM.AsString   :='2';
      dm.RxCustasVALOR.AsFloat      :=dm.SubitemVALOR.AsFloat;
      dm.RxCustasQTD.AsString       :=dm.vMarcados;
      dm.RxCustasDESCRICAO.AsString :=dm.SubitemDESCR.AsString;
      dm.RxCustasTOTAL.AsFloat      :=dm.RxCustasVALOR.AsFloat*dm.RxCustasQTD.AsInteger;
      dm.RxCustas.Post;

      vEmolItem:=GR.NoRound(dm.RxCustasVALOR.AsFloat,2);
  end
  else
    vEmolItem:=0;

  dm.vDataPedido:=FormatDateTime('dd/mm/yyyy',Now);
  dm.vDataCertidao:=FormatDateTime('dd/mm/yyyy',Now);

  InputQuery('Certid�o','Data do Pedido',dm.vDataPedido);
  InputQuery('Certid�o','Data da Certid�o',dm.vDataCertidao);
  InputQuery('Certid�o','N� de Folhas',dm.vFolhas);
  InputQuery('Certid�o','Recibo',dm.vRecibo);
  InputQuery('Certid�o','SERASA (S) / BOA VISTA SERVICOS S.A. (B)',dm.vDestino);

  dm.Certidoes.Close;
  dm.Certidoes.Params[0].AsInteger:=-1;
  dm.Certidoes.Open;
  dm.Certidoes.Append;
  dm.CertidoesID_CERTIDAO.AsInteger   :=dm.IdAtual('ID_ATO','S');
  dm.CertidoesCONVENIO.AsString       :='N';
  dm.CertidoesCODIGO.AsInteger        :=4011;
  dm.CertidoesDT_PEDIDO.AsDateTime    :=StrToDate(dm.vDataPedido);
  dm.CertidoesDT_CERTIDAO.AsDateTime  :=StrToDate(dm.vDataCertidao);
  dm.CertidoesTIPO_CERTIDAO.AsString  :='E';
  dm.CertidoesFOLHAS.AsString         :=dm.vFolhas;
  dm.CertidoesRESULTADO.AsString      :='P';
  dm.CertidoesDT_ENTREGA.AsDateTime   :=Now;
  dm.CertidoesANOS.AsInteger          :=1;
  dm.CertidoesTIPO_REQUERIDO.AsString :='J';

  if dm.vDestino='S' then
  begin
      dm.CertidoesREQUERIDO.AsString          :='SERASA';
      dm.CertidoesREQUERENTE.AsString         :='SERASA';
      dm.CertidoesCPF_CNPJ_REQUERIDO.AsString :='62173620006544';
  end
  else
  begin
      dm.CertidoesREQUERIDO.AsString          :='BOA VISTA SERVICOS S.A.';
      dm.CertidoesREQUERENTE.AsString         :='BOA VISTA SERVICOS S.A.';
      dm.CertidoesCPF_CNPJ_REQUERIDO.AsString :='11725176000127';
  end;

  if Trim(dm.vRecibo)<>'' then
  dm.CertidoesRECIBO.AsInteger        :=PF.PegarNumero(dm.vRecibo);
  dm.CertidoesREGISTROS.AsInteger     :=RX.RecordCount;
  dm.CertidoesCOBRANCA.AsString       :='CC';

  dm.CertidoesEMOLUMENTOS.AsFloat :=vEmolGeral+(vEmolItem*StrToInt(dm.vMarcados));
  dm.CertidoesFETJ.AsFloat        :=GR.FETJ(vEmolGeral)+(GR.FETJ(vEmolItem)*StrToInt(dm.vMarcados));
  dm.CertidoesFUNDPERJ.AsFloat    :=GR.FUNDPERJ(vEmolGeral)+(GR.FUNDPERJ(vEmolItem)*StrToInt(dm.vMarcados));
  dm.CertidoesFUNPERJ.AsFloat     :=GR.FUNPERJ(vEmolGeral)+(GR.FUNPERJ(vEmolItem)*StrToInt(dm.vMarcados));
  dm.CertidoesFUNARPEN.AsFloat    :=GR.FUNARPEN(vEmolGeral)+(GR.FUNARPEN(vEmolItem)*StrToInt(dm.vMarcados));
  dm.CertidoesISS.AsFloat         :=GR.ISS(vEmolGeral,Gdm.vAliquotaISS)+(GR.ISS(vEmolItem,Gdm.vAliquotaISS)*StrToInt(dm.vMarcados));
  dm.CertidoesPMCMV.AsFloat       :=GR.PMCMV(vEmolGeral-vComuns)+(GR.PMCMV(vEmolItem)*StrToInt(dm.vMarcados));
  dm.CertidoesTOTAL.AsFloat       :=dm.CertidoesEMOLUMENTOS.AsFloat+
                                    dm.CertidoesFETJ.AsFloat+
                                    dm.CertidoesFUNDPERJ.AsFloat+
                                    dm.CertidoesFUNPERJ.AsFloat+
                                    dm.CertidoesFUNARPEN.AsFloat+
                                    dm.CertidoesPMCMV.AsFloat+
                                    dm.CertidoesISS.AsFloat;

  dm.vIdReservado                 :=GR.ValorGeneratorDBX('ID_RESERVADO',Gdm.BDGerencial);
  dm.CertidoesSELO.AsString       :=Gdm.ProximoSelo(dm.SerieAtual,'PROTESTO',Self.Name,'N','',0,0,0,
                                    'CERTID�O ('+dm.CertidoesREQUERIDO.AsString+')',
                                    dm.vNome,Now,dm.vIdReservado);
  dm.CertidoesALEATORIO.AsString  :=Gdm.vAleatorio;
  Gdm.vAleatorio                  :='*';
  dm.CertidoesESCREVENTE.AsString :=dm.vCPF;
  dm.Certidoes.Post;

  GR.CriarForm(TFAssinatura,FAssinatura);

  Application.CreateForm(TFQuickCertidaoCadastro1,FQuickCertidaoCadastro1);
  FQuickCertidaoCadastro1.Free;

  if GR.Pergunta('CONFIRMA O LAN�AMENTO DA CERTID�O') then
  begin
      dm.Certidoes.ApplyUpdates(0);

      Gdm.BaixarSelo(dm.CertidoesSELO.AsString,
                     dm.vIdReservado,
                     'PROTESTO',
                     Self.Name,
                     dm.CertidoesID_ATO.AsInteger,
                     0,
                     4,
                     dm.CertidoesDT_CERTIDAO.AsDateTime,
                     '',
                     0,0,0,0,
                     dm.CertidoesRECIBO.AsInteger,
                     dm.CertidoesCODIGO.AsInteger,
                     'CERTID�O ('+dm.CertidoesREQUERIDO.AsString+')',
                     dm.vNome,
                     dm.CertidoesCONVENIO.AsString,
                     dm.CertidoesCOBRANCA.AsString,
                     dm.CertidoesEMOLUMENTOS.AsFloat,
                     dm.CertidoesFETJ.AsFloat,
                     dm.CertidoesFUNDPERJ.AsFloat,
                     dm.CertidoesFUNPERJ.AsFloat,
                     dm.CertidoesFUNARPEN.AsFloat,
                     dm.CertidoesPMCMV.AsFloat,
                     dm.CertidoesMUTUA.AsFloat,
                     dm.CertidoesACOTERJ.AsFloat,
                     dm.CertidoesDISTRIBUICAO.AsFloat,
                     0,0,0,0,
                     dm.CertidoesTOTAL.AsFloat,
                     dm.CertidoesISS.AsFloat);

      SalvarCustas;

      if StrToInt(dm.ValorParametro(14))<>0 then
        dm.AtualizarParametro(14,IntToStr(dm.CertidoesRECIBO.AsInteger+1))
          else dm.AtualizarParametro(11,IntToStr(dm.CertidoesRECIBO.AsInteger+1));
  end
  else
  begin
      dm.Certidoes.Cancel;
      Gdm.LiberarSelo('PROTESTO',dm.CertidoesSELO.AsString,False,-1,dm.vIdReservado);
      dm.vIdReservado:=0;
  end;

  if GR.Pergunta('IMPRIMIR RECIBO') then
  begin
      Application.CreateForm(TFQuickSerasaBV,FQuickSerasaBV);
      with FQuickSerasaBV do
      begin
          lbTitular1.Caption     :=dm.ValorParametro(7)+': '+dm.ServentiaTABELIAO.AsString;
          lbEmolumentos1.Caption :='R$ '+FloatToStrF(dm.CertidoesEMOLUMENTOS.AsFloat,ffNumber,7,2);
          lbFetj1.Caption        :='R$ '+FloatToStrF(dm.CertidoesFETJ.AsFloat,ffNumber,7,2);
          lbFundperj1.Caption    :='R$ '+FloatToStrF(dm.CertidoesFUNDPERJ.AsFloat,ffNumber,7,2);
          lbFunperj1.Caption     :='R$ '+FloatToStrF(dm.CertidoesFUNPERJ.AsFloat,ffNumber,7,2);
          lbFunarpen1.Caption    :='R$ '+FloatToStrF(dm.CertidoesFUNARPEN.AsFloat,ffNumber,7,2);
          lbPMCMV1.Caption       :='R$ '+FloatToStrF(dm.CertidoesPMCMV.AsFloat,ffNumber,7,2);
          lbISS1.Caption         :='R$ '+FloatToStrF(dm.CertidoesISS.AsFloat,ffNumber,7,2);
          lbTotal1.Caption       :='R$ '+FloatToStrF(dm.CertidoesTOTAL.AsFloat,ffNumber,7,2);
          if dm.vDestino='B' then
            lbEmpresa1.Caption:=dm.vDestino+' - T�TULOS PROTESTADOS/CANCELADOS'
              else lbEmpresa1.Caption:=dm.vDestino+' - T�TULOS PROTESTADOS/CANCELADOS';

          lbRecibo1.Caption       :='RECIBO N� '+dm.vRecibo;
          lbDataEmissao1.Caption  :=FormatDateTime('dd/mm/yyyy',Now);
          lbQtd1.Caption          :=dm.vMarcados;
          lbCidade1.Caption       :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',Now);
          lbNome1.Caption         :=dm.vNomeCompleto;
          lbQualificacao1.Caption :=PF.QualificacaoEscrevente(dm.vCPF);

          if dm.ServentiaCODIGO.AsInteger=2358 then
          begin
              lbTitular2.Caption      :=lbTitular1.Caption;
              lbEmpresa2.Caption      :=lbEmpresa1.Caption;
              lbRecibo2.Caption       :=lbRecibo1.Caption;
              lbEmolumentos2.Caption  :=lbEmolumentos1.Caption;
              lbFetj2.Caption         :=lbFetj1.Caption;
              lbFundperj2.Caption     :=lbFundperj1.Caption;
              lbFunperj2.Caption      :=lbFunperj1.Caption;
              lbFunarpen2.Caption     :=lbFunarpen1.Caption;
              lbPMCMV2.Caption        :=lbPMCMV1.Caption;
              lbTotal2.Caption        :=lbTotal1.Caption;
              lbCidade2.Caption       :=lbCidade1.Caption;
              lbNome2.Caption         :=lbNome1.Caption;
              lbQualificacao2.Caption :=lbQualificacao1.Caption;
              lbDataEmissao2.Caption  :=lbDataEmissao1.Caption;
              lbQtd2.Caption          :=lbQtd1.Caption;
              Segundo.Enabled         :=True;
          end;

          Recibo.Preview;
          Free;
      end;
  end;

  RX.Close;

  B.Panels[1].Text  :='0';
  B.Panels[3].Text  :='0'; **)
end;

function TFSerasa.Lavrados: Integer;
var
  X: Integer;
begin
  vPosicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  X:=0;
  while not RX.Eof do
  begin
      if RXStatus.AsString='LAVRADO' then
        Inc(X);
      RX.Next;
  end;
  RX.RecNo:=vPosicao;
  RX.EnableControls;
  Result:=X;
end;

function TFSerasa.Cancelados: Integer;
var
  X: Integer;
begin
  vPosicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  X:=0;
  while not RX.Eof do
  begin
      if RXStatus.AsString='CANCELADO' then
        Inc(X);
      RX.Next;
  end;
  RX.RecNo:=vPosicao;
  RX.EnableControls;
  Result:=X;
end;

procedure TFSerasa.SalvarCustas;
begin
  dm.RxCustas.First;
  while not dm.RxCustas.Eof do
  begin
      dm.Custas.Close;
      dm.Custas.Params[0].AsInteger:=dm.RxCustasID_CUSTA.AsInteger;
      dm.Custas.Open;
      dm.Custas.Append;
      dm.CustasID_CUSTA.AsInteger   :=dm.IdAtual('ID_CUSTA','S');
      dm.CustasID_ATO.AsInteger     :=dm.CertidoesID_CERTIDAO.AsInteger;
      dm.CustasTABELA.AsString      :=dm.RxCustasTABELA.AsString;
      dm.CustasITEM.AsString        :=dm.RxCustasITEM.AsString;
      dm.CustasSUBITEM.AsString     :=dm.RxCustasSUBITEM.AsString;
      dm.CustasVALOR.AsFloat        :=dm.RxCustasVALOR.AsFloat;
      dm.CustasQTD.AsInteger        :=dm.RxCustasQTD.AsInteger;
      dm.CustasTOTAL.AsFloat        :=dm.RxCustasTOTAL.AsFloat;
      dm.Custas.Post;
      if dm.Custas.ApplyUpdates(0)>0 then Raise Exception.Create('Erro: ApplyUpdates dm.Custas');
      dm.RxCustas.Next;
  end;
end;

procedure TFSerasa.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.AtualizarParametro(37,edSerasa.Text);
  dm.AtualizarParametro(78,edBoaVista.Text);
end;

procedure TFSerasa.ckFiltroClick(Sender: TObject);
begin
  edDataInicial.Date:=IncYear(Now,-5);
  Rb2.Checked:=True;
  btFiltrarClick(Sender);
end;

function TFSerasa.Marcados: Integer;
var
  X: Integer;
begin
  vPosicao:=RX.RecNo;
  RX.DisableControls;
  RX.First;
  X:=0;
  while not RX.Eof do
  begin
      if RXCheck.AsBoolean then
        Inc(X);
      RX.Next;
  end;
  RX.RecNo:=vPosicao;
  RX.EnableControls;
  Result:=X;
end;

procedure TFSerasa.DetalheBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbNatureza.Caption:=PF.RetornarSigla(RXTIPO_TITULO.AsInteger);
  Inc(vQuantidade);
end;

procedure TFSerasa.GerarArquivo(Orgao: String);
var
  vPosicao,vCount: Integer;
  vLivro,vFolha: String;
  vTipo,vCodigo,vDocumento,vOcorrencia,vBase,vFilial,vDigito,vDestinatario,vMunicipio: String;
  vPessoa,vEspecie,vEmissao,vVencimento,vEnderecoDevedor,vRemessa,vCEP,vUF,vProtocolo: String;
begin
  PF.Aguarde(True);

  if Orgao='S' then
  begin
      vRemessa     :=edSerasa.Text;
      vDestinatario:='SERASA-CONCENTRE-PROTESTO';
  end
  else
  begin
      vRemessa     :=edBoaVista.Text;
      vDestinatario:='BOAVISTASERVICOS-PROTESTO';
  end;

  M.Lines.Clear;
  M.Lines.Add('0'+
              PF.Espaco(' ','E',43)+
              FormatDateTime('ddmmyyyy',Now)+
              PF.Espaco(' ','E',9)+
              PF.Zeros(vRemessa,'D',6)+
              PF.Espaco(' ','E',22)+
              '1'+
              PF.Zeros(Copy(dm.ServentiaCNPJ.AsString,1,8),'D',9)+
              vDestinatario+
              'E'+
              '00'+Copy(dm.ServentiaTELEFONE.AsString,1,2)+
              Copy(dm.ServentiaTELEFONE.AsString,3,10)+
              '0000'+
              PF.Espaco(dm.vNome,'E',70)+
              '0020'+
              'EDI-7'+
              'D'+
              PF.Espaco(' ','E',40)+
              PF.Espaco(' ','E',272)+
              PF.Espaco(' ','E',60)+
              '0000001');

  vCount:=2;

  RX.DisableControls;
  vPosicao:=RX.RecNo;
  RX.First;
  while not RX.Eof do
  begin
      Application.ProcessMessages;
      if RXCheck.AsBoolean then
      begin
          if RXStatus.AsString='CANCELADO' then
          begin
              vTipo        :='C';
              vCodigo      :='E';
              vOcorrencia  :=FormatDateTime('ddmmyyyy',RXDt_Pagamento.AsDateTime);
          end
          else
          begin
              vTipo        :='P';
              vCodigo      :='I';
              vOcorrencia  :='        ';
          end;

          if RXDt_Titulo.AsDateTime<>0 then
            vEmissao:=FormatDateTime('ddmmyyyy',RXDt_Titulo.AsDateTime)
              else vEmissao:='        ';

          if RXDt_Vencimento.AsDateTime<>0 then
            vVencimento:=FormatDateTime('ddmmyyyy',RXDt_Vencimento.AsDateTime)
              else vVencimento:='        ';

          if RXTipo_Devedor.AsString='F' then
          begin
              vPessoa   :='F';
              vDocumento:='2';
              vBase     :=PF.Zeros(Copy(GR.PegarNumeroTexto(RXCPF_CNPJ_Devedor.AsString),1,9),'D',9);
              vFilial   :='0000';
              vDigito   :=PF.Zeros(Copy(GR.PegarNumeroTexto(RXCPF_CNPJ_Devedor.AsString),10,2),'D',2);
          end
          else
          begin
              vPessoa   :='J';
              vDocumento:='1';
              vBase     :=PF.Zeros(Copy(GR.PegarNumeroTexto(RXCPF_CNPJ_Devedor.AsString),1,8),'D',9);
              vFilial   :=PF.Zeros(Copy(GR.PegarNumeroTexto(RXCPF_CNPJ_Devedor.AsString),9,4),'D',4);
              vDigito   :=PF.Zeros(Copy(GR.PegarNumeroTexto(RXCPF_CNPJ_Devedor.AsString),13,2),'D',2);
          end;

          if RXFins_Falimentares.AsString='S' then
            vEspecie:='4'
              else vEspecie:=Copy(RXTipo_Protesto.AsString,1,1);

          PF.CarregarDevedor(RXID_ATO.AsInteger);

          {SPDA ENVIO O MUNIC�PIO NO LUGAR DO ENDERE�O}
          if (dm.ServentiaCODIGO.AsInteger=1866) then
            vEnderecoDevedor:=PF.Espaco(Copy(PF.RemoverEspeciais(dm.RxDevedorMUNICIPIO.AsString),1,45),'E',45)
              else vEnderecoDevedor:=PF.Espaco(Copy(PF.RemoverEspeciais(dm.RxDevedorENDERECO.AsString),1,45),'E',45);

          {NIL�POLIS N�O ENVIO ALGUMAS INFORMA��ES}
          if (dm.ServentiaCODIGO.AsInteger=1622) then
          begin
              vLivro            :='000000';
              vFolha            :='0000';
              vCEP              :='00000000';
              vProtocolo        :='0000000000';
              vEnderecoDevedor  :=GR.Espaco(Copy(PF.RemoverEspeciais(dm.RxDevedorMUNICIPIO.AsString),1,45),'E',45);
              vMunicipio        :='                    ';
              vUF               :='  ';
          end
          else
          begin
              vLivro            :=GR.Zeros(RXLivro_Registro.AsString,'D',6);
              vFolha            :=GR.Zeros(RXFolha_Registro.AsString,'D',4);
              vCEP              :=GR.Zeros(GR.PegarNumeroTexto(dm.RxDevedorCEP.AsString),'D',8);
              vProtocolo        :=GR.Zeros(RXProtocolo.AsString,'D',10);
              vEnderecoDevedor  :=GR.Espaco(Copy(PF.RemoverEspeciais(dm.RxDevedorMUNICIPIO.AsString),1,45),'E',45);
              vMunicipio        :=GR.Espaco(Copy(PF.RemoverEspeciais(dm.RxDevedorMUNICIPIO.AsString),1,20),'E',20);
              vUF               :=GR.Espaco(dm.RxDevedorUF.AsString,'E',2);
          end;

          M.Lines.Add('1'+
                      vTipo+
                      vCodigo+
                      'RJ'+
                      PF.Espaco(dm.ServentiaPRACA.AsString,'E',4)+
                      PF.Espaco(' ','E',55)+
                      PF.Espaco(' ','E',45)+
                      PF.Espaco(' ','E',9)+
                      PF.Espaco(' ','E',4)+
                      PF.Espaco(' ','E',2)+
                      PF.Espaco(' ','E',44)+
                      PF.Espaco(' ','E',8)+
                      PF.Espaco(' ','E',20)+
                      PF.Espaco(' ','E',2)+
                      PF.Espaco(' ','E',25)+
                      PF.Espaco(' ','E',1)+
                      PF.Espaco(' ','E',1)+
                      PF.Espaco(' ','E',7)+
                      vLivro+
                      ' '+
                      vFolha+
                      PF.Espaco(IfThen(RXTipo_Titulo.AsInteger=25,'OT',PF.RetornarSigla(RXTipo_Titulo.AsInteger)),'E',3)+
                      PF.Zeros(GR.PegarNumeroTexto(FloatToStrF(RXSaldo_Titulo.AsFloat,ffNumber,7,2)),'D',14)+
                      FormatDateTime('ddmmyyyy',RXDt_Registro.AsDateTime)+
                      vPessoa+
                      vDocumento+
                      '00'+
                      PF.Espaco(' ','E',25)+
                      PF.Espaco(Copy(PF.RemoverEspeciais(RXDevedor.AsString),1,45),'E',45)+
                      PF.Espaco(' ','E',3)+
                      vBase+
                      vFilial+
                      vDigito+
                      PF.Espaco(' ','E',10)+
                      vEnderecoDevedor+
                      vCEP+
                      vMunicipio+
                      vUF+
                      PF.Zeros(dm.ValorParametro(36),'D',2)+
                      vProtocolo+
                      PF.Espaco(' ','E',20)+
                      vOcorrencia+
                      vEspecie+
                      vEmissao+
                      vVencimento+
                      PF.Espaco(' ','E',31)+
                      PF.Espaco(' ','E',60)+
                      PF.Zeros(IntToStr(vCount),'D',7));
          Inc(vCount);
      end;
      RX.Next;
  end;
  PF.Aguarde(False);
  RX.RecNo:=vPosicao;
  RX.EnableControls;
  M.Lines.Add('9'+
              PF.Espaco(' ','E',592)+
              PF.Zeros(IntToStr(vCount),'D',7));

  PF.Aguarde(True);
  if Orgao='S' then
    Svd.FileName:=dm.ValorParametro(20)+'C'+dm.ValorParametro(36)+FormatDateTime('ddmm',Now)+'.txt'
      else Svd.FileName:=dm.ValorParametro(79)+'C'+dm.ValorParametro(36)+FormatDateTime('ddmm',Now)+'.txt';

  if Svd.Execute then
  M.Lines.SaveToFile(Svd.FileName);

  if GR.Pergunta('CONFIRMA A GERA��O DO ARQUIVO') then
    if Orgao='S' then
      edSerasa.Value:=edSerasa.Value+1
        else edBoaVista.Value:=edBoaVista.Value+1
end;

procedure TFSerasa.Serasa1Click(Sender: TObject);
begin
  GerarArquivo('S');
end;

procedure TFSerasa.PopEquifaxClick(Sender: TObject);
begin
  GerarArquivo('B');
end;

procedure TFSerasa.RXFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept:=RXStatus.AsString=vStatus;
end;

procedure TFSerasa.M1Click(Sender: TObject);
begin
  if RX.IsEmpty then Exit;

  PF.Aguarde(True,'Protestados...');

  RX.DisableControls;
  vQuantidade       :=0;
  vStatus           :='LAVRADO';
  RX.Filtered       :=False;
  RX.Filtered       :=True;
  lbRelacao.Caption :='RELA��O DE T�TULOS PROTESTADOS';
  lbData.Enabled    :=False;
  qrData.Enabled    :=False;

  qkModelo1.Preview;

  PF.Aguarde(True,'Cancelados...');
  vQuantidade       :=0;
  vStatus           :='CANCELADO';
  RX.Filtered       :=False;
  RX.Filtered       :=True;
  lbRelacao.Caption :='RELA��O DE T�TULOS CANCELADOS';
  lbData.Enabled    :=True;
  qrData.Enabled    :=True;

  qkModelo1.Preview;

  RX.EnableControls;
  RX.Filtered:=False;

  Application.CreateForm(TFQuickLegendas,FQuickLegendas);

  FQuickLegendas.Free;
end;

procedure TFSerasa.M2Click(Sender: TObject);
begin
  if RX.IsEmpty then Exit;

  PF.Aguarde(True,'Protestados...');


  lbInfo1.Caption:='Per�odo: '+edDataInicial.Text+' - '+edDataFinal.Text;
  lbInfo2.Caption:='Cart�rio: '+dm.ServentiaCODIGO.AsString+' - '+dm.ServentiaDESCRICAO.AsString;
  lbInfo3.Caption:='Cidade: '+dm.ServentiaCIDADE.AsString+'/'+dm.ServentiaUF.AsString;
  RX.DisableControls;

  vQuantidade         :=0;
  vStatus             :='LAVRADO';
  RX.Filtered         :=False;
  RX.Filtered         :=True;
  lbProtesto.Caption  :='|X| Protesto Lavrado (para Inclus�o)';
  lbCancelado.Caption :='|  | Protesto Cancelado (para Exclus�o)';
  lbData.Enabled      :=False;
  qrData.Enabled      :=False;

  qkModelo2.Preview;

  PF.Aguarde(True,'Cancelados...');
  vQuantidade         :=0;
  vStatus             :='CANCELADO';
  RX.Filtered         :=False;
  RX.Filtered         :=True;
  lbProtesto.Caption  :='|  | Protesto Lavrado (para Inclus�o)';
  lbCancelado.Caption :='|X| Protesto Cancelado (para Exclus�o)';
  lbData.Enabled      :=True;
  qrData.Enabled      :=True;

  qkModelo2.Preview;

  RX.EnableControls;
  RX.Filtered:=False;
end;

procedure TFSerasa.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  lbNatureza2.Caption:=PF.RetornarSigla(RXTIPO_TITULO.AsInteger);

  if RXMotivo.AsString='PFP' then lbEspecie.Caption:='1';
  if RXMotivo.AsString='PFA' then lbEspecie.Caption:='2';
  if RXMotivo.AsString='PFD' then lbEspecie.Caption:='3';
  if RXFins_Falimentares.AsString='S' then lbEspecie.Caption:='4';

  if RXTipo_Devedor.AsString='F' then
    lbTipoDoc.Caption:='2'
      else lbTipoDoc.Caption:='1';

  if RXTipo_Apresentante.AsString='F' then
    lbTipoDoc2.Caption:='2'
      else lbTipoDoc2.Caption:='1';

  Inc(vQuantidade);
end;

procedure TFSerasa.qkModelo2EndPage(Sender: TCustomQuickRep);
begin
  PF.Aguarde(False);
end;

procedure TFSerasa.QRBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  vQuantidade      :=0;
end;

procedure TFSerasa.FormShow(Sender: TObject);
begin
  PF.Aguarde(False);
end;

procedure TFSerasa.MarcarQuantidadeClick(Sender: TObject);
var
  Qtd: String;
  Posicao,I: Integer;
begin
  Qtd:=InputBox('Marcar','Quantidade','');

  PF.Aguarde(True);
  Posicao:=RX.RecNo;
  RX.DisableControls;
  for I := 1 to StrToInt(Qtd) do
  begin
      Application.ProcessMessages;
      RX.Edit;
      RXCheck.AsBoolean:=True;
      RX.Post;
      RX.Next;
  end;
  RX.RecNo:=Posicao;
  RX.EnableControls;
  PF.Aguarde(False);
end;

procedure TFSerasa.ckAviso042015Click(Sender: TObject);
begin
  if ckAviso042015.Checked then
  begin
      edDataInicial.Date:=StrToDate('10/09/2012');
      edDataFinal.Date:=StrToDate('02/06/2015');
      Rb1.Enabled:=False;
      ckProtestados.Enabled:=False;
      PopEquifax.Enabled:=False;
      ckFiltro.Enabled:=False;
      ckFiltro.Checked:=False;
      ckProtestados.Checked:=False;
      Rb2.Checked:=True;
      GbData.Enabled:=False;
      btFiltrar.SetFocus;
  end
  else
  begin
      Rb1.Enabled:=True;
      GbData.Enabled:=True;
      ckProtestados.Enabled:=True;
      ckFiltro.Enabled:=True;
      PopEquifax.Enabled:=True;
  end;
end;

procedure TFSerasa.sSpeedButton1Click(Sender: TObject);
begin
  GR.Processando(True);
  GR.ShellOpen('http:\\www.servidortotal.com.br\cartorios\tabelas\COMUNICADO_05_2015.pdf');
  GR.Processando(False);
end;

procedure TFSerasa.SvdShow(Sender: TObject);
begin
  PF.Aguarde(False);
end;

end.
