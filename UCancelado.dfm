�
 TFCANCELADO 0�  TPF0TFCancelado
FCanceladoLeft�Top#BorderIconsbiSystemMenu BorderStylebsDialogCaption	CanceladoClientHeight� ClientWidthxColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
KeyPreview	OldCreateOrderPositionpoMainFormCenterOnClose	FormCloseOnCreate
FormCreatePixelsPerInch`
TextHeight TsLabellbTotalLeftTop� WidthTHeightCursorcrHandPointCaptionTotal: R$ 0,00
ParentFontOnClicklbTotalClickFont.CharsetANSI_CHARSET
Font.Color+5M Font.Height�	Font.NameArial
Font.StylefsBold   TsPanelP1Left Top WidthxHeight� AlignalTopTabOrder SkinData.SkinSectionPANEL 
TsDateEditedDataLeft
TopWidthZHeightAutoSizeColorclWhiteEditMask!99/99/9999;1; Font.CharsetANSI_CHARSET
Font.ColorDDD Font.Height�	Font.NameTahoma
Font.Style 	MaxLength

ParentFontParentShowHintShowHintTabOrder BoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionDataBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style BoundLabel.Layout
sclTopLeftSkinData.SkinSectionEDITGlyphMode.GrayedGlyphMode.Blend DefaultToday	  TsEditedSeloLeftnTopWidthRHeightCharCaseecUpperCaseColor��� Font.CharsetANSI_CHARSET
Font.ColorDDD Font.Height�	Font.NameTahoma
Font.StylefsBold 	MaxLength
ParentFontParentShowHintReadOnly	ShowHintTabOrderSkinData.CustomColor	SkinData.SkinSectionEDITBoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionSeloBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style BoundLabel.Layout
sclTopLeft  TsDBLookupComboBoxlkEscreventesLeft
TopMWidth� HeightColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorDDD Font.Height�	Font.NameArial
Font.Style KeyFieldCPF	ListFieldLOGIN
ListSourcedsEscreventes
ParentFontParentShowHintShowHintTabOrderBoundLabel.Active	BoundLabel.ParentFontBoundLabel.Caption
EscreventeBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style BoundLabel.Layout
sclTopLeftSkinData.SkinSectionCOMBOBOX  TsEditedReciboLeft� TopWidthoHeightCharCaseecUpperCaseColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorDDD Font.Height�	Font.NameTahoma
Font.Style 	MaxLength
ParentFontParentShowHintShowHintTabOrderSkinData.SkinSectionEDITBoundLabel.Active	BoundLabel.ParentFontBoundLabel.CaptionReciboBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameTahomaBoundLabel.Font.Style BoundLabel.Layout
sclTopLeft  
TsCheckBox
ckImprimirLeftToppWidth� HeightCursorcrHandPointCaption$   Imprimir Averbação de CancelamentoChecked	State	cbCheckedTabOrder
ImgChecked ImgUnchecked SkinData.SkinSectionCHECKBOX  
TsComboBox
cbCobrancaLeft� TopMWidthoHeight	AlignmenttaLeftJustifyBoundLabel.Active	BoundLabel.ParentFontBoundLabel.Caption   Tipo de CobrançaBoundLabel.Font.CharsetANSI_CHARSETBoundLabel.Font.Color+5M BoundLabel.Font.Height�BoundLabel.Font.NameArialBoundLabel.Font.Style BoundLabel.Layout
sclTopLeftSkinData.SkinSectionCOMBOBOXVerticalAlignment
taAlignTopStylecsDropDownListColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorDDD Font.Height�	Font.NameArial
Font.Style 	ItemIndex
ParentFontTabOrderText   Com CobrançaOnChangecbCobrancaChangeItems.Strings   Justiça Gratuita   Com Cobrança   Sem CobrançaNIHIL   Força da Lei   TsEditedAleatorioLeft� TopWidth,HeightCharCaseecUpperCaseColor��� Font.CharsetANSI_CHARSET
Font.ColorDDD Font.Height�	Font.NameTahoma
Font.StylefsBold 	MaxLength
ParentFontParentShowHintReadOnly	ShowHintTabOrderSkinData.CustomColor	SkinData.SkinSectionEDITBoundLabel.ParentFontBoundLabel.Font.CharsetDEFAULT_CHARSETBoundLabel.Font.ColorclWindowTextBoundLabel.Font.Height�BoundLabel.Font.NameMS Sans SerifBoundLabel.Font.Style BoundLabel.Layout
sclTopLeft   TsBitBtnbtConfirmarLeft� Top� WidthKHeightCursorcrHandPointCaptionOkFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTabOrderOnClickbtConfirmarClick
ImageIndexImages
dm.ImagensSkinData.SkinSectionBUTTON  TsBitBtn
btCancelarLeftTop� WidthUHeightCursorcrHandPointCaptionCancelarFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTabOrderOnClickbtCancelarClick
ImageIndexImages
dm.ImagensSkinData.SkinSectionBUTTON  TDataSourcedsEscreventesDataSetdm.EscreventesLeft�Top�   TDataSource
dsConsultaDataSetFBaixas2014.ConsultaLeftTop�    