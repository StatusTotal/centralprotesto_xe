unit UServentia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,DB,
  Dialogs, StdCtrls, Mask, DBCtrls, ExtCtrls, Buttons, QuickRpt, QRCtrls,SQLExpr,
  Menus, DBClient, SimpleDS, sDBLookupComboBox, sDBEdit, sPanel,
  sBitBtn, sDBComboBox;

type
  TFServentia = class(TForm)
    P1: TsPanel;
    edCodigo: TsDBEdit;
    dsServentia: TDataSource;
    edDescricao: TsDBEdit;
    edEndereco: TsDBEdit;
    edCep: TsDBEdit;
    edTelefone: TsDBEdit;
    edEmail: TsDBEdit;
    lkCidade: TsDBLookupComboBox;
    btOk: TsBitBtn;
    edTabeliao: TsDBEdit;
    edSubstituto: TsDBEdit;
    edMatricula: TsDBEdit;
    edCNPJ: TsDBEdit;
    btCancelar: TsBitBtn;
    dsMunicipios: TDataSource;
    cbSexo: TsDBComboBox;
    edTitulo: TsDBEdit;
    btAlterar: TsBitBtn;
    edSite: TsDBEdit;
    edPraca: TsDBEdit;
    edCodigoPraca: TsDBEdit;
    edCodigoMunicipio: TsDBEdit;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure lkCidadeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FServentia: TFServentia;

implementation

uses UDM, UPF;

{$R *.dfm}

procedure TFServentia.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL, 0, 0);
end;

procedure TFServentia.btOkClick(Sender: TObject);
begin
  dm.Serventia.Post;
  dm.Serventia.ApplyUpdates(0);
  Close;
end;

procedure TFServentia.FormCreate(Sender: TObject);
begin
  dm.Municipios.Close;
  dm.Municipios.Params[0].AsString:='RJ';
  dm.Municipios.Open;
end;

procedure TFServentia.btCancelarClick(Sender: TObject);
begin
  dm.Serventia.Cancel;
  Close;
end;

procedure TFServentia.btAlterarClick(Sender: TObject);
begin
  if PF.PasswordInputBox('Seguranša','Senha')<>FormatDateTime('HHMM',Now) then Exit;

  P1.Enabled        :=True;
  btOk.Enabled      :=True;
  btCancelar.Enabled:=True;
  edCodigo.SetFocus;
end;

procedure TFServentia.lkCidadeClick(Sender: TObject);
begin
  if dm.Serventia.State in [dsEdit,dsInsert] then
    dm.ServentiaCODIGO_MUNICIPIO.AsString:=dm.MunicipiosCODIGO.AsString;
end;

procedure TFServentia.FormShow(Sender: TObject);
begin
  if FileExists('C:\FNMASTOTAL') then
  begin
      P1.Enabled        :=True;
      btOk.Enabled      :=True;
      btCancelar.Enabled:=True;
      edCodigo.SetFocus;
  end;
end;

end.
