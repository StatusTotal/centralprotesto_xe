unit UPF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, Buttons, Grids, DBGrids, DBClient,
  FMTBcd, DB, Provider, SqlExpr, DBTables, SimpleDS, DBCtrls, RichEdit,
  QuickRpt, QRCtrls, MaskUtils, RXDBCtrl, ShellAPI, Mask, Math,IdSNTP,URLMon,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.Comp.UI,
  FireDAC.Phys.IBBase;

type
  TPF = class(TForm)
      procedure Excluir(SQL: String; Conexao:TSQLConnection);
      procedure ExcluirFD(SQL: String; Conexao: TFDConnection);
      procedure JustificarRichEdit(RichEdit :TRichEdit; AllText :Boolean);
      procedure EspacoLinhaRichEdit(RichEdit :TRichEdit; Espacamento :Integer; AllText :Boolean);
      procedure SetFocusCtrl(Ctrl: TWinControl; ActiveCtrl: TWinControl);
      Procedure FindReplace(Const Enc, subs: String; Var Texto: TQRMemo; T:Boolean);
      procedure CriarSQLQuery(Conexao: TSQLConnection; Query: TSQLQuery);
      procedure CriarFDQuery(Conexao: TFDConnection; Query: TFDQuery);
      procedure LiberarQuery(Query: TSQLQuery);
      procedure LiberarFDQuery(Query: TFDQuery);
      procedure CriarArquivo(DirNome,Texto:String);
      procedure VisualizarArquivo(Arquivo: String);
      procedure DesativarIndices(Tabela: String);
      procedure AtivarIndices(Tabela: String);
      procedure Aguarde(Status: Boolean = True; Msg: String = 'Aguarde...'; Modo: Char = 'P'; MaisMsg: Boolean = False; TextoMsg: String = '');
      procedure CarregarDevedor(ID_ATO: Integer);
      procedure CarregarCustas(ID_ATO: Integer);
      procedure CarregarPortador(Tipo,Valor,Nome,Pessoa,Documento,Codigo: String);
      procedure ExportarParaExcel(D: TDataSet; Dir,Arq: String);
      procedure CarregarSacador(ID_ATO: Integer);
      procedure BuscarApresentante(Tipo,Valor: String);
      function FormatarCEP(Cep: String): String;
      function VerificarCPF(numero: string): Boolean;
      function VerificarCNPJ(CNPJ: String):Boolean;
      function FormatarCPF(CPF: string): string;
      function FormatarCNPJ(CNPJ: String): string;
      function JstParagrafo (Paragrafo : string; Largura : word) : string;
      function JstParagrafo2 (Paragrafo : string; Largura : word) : string;
      function VerificarData(Texto: String; Campo: TField): boolean;
      function MaskString(Valor: String) : String;
      function RemoverCaracter(Texto,Excluir: String): String;
      function FormatarData(Data: TDate; Tipo: String): String;
      function Espaco(Campo,Alinha:String; Tam:Integer):String;
      function Zeros(Campo,Alinha: String; Tam: Integer): String;
      function BuscaTroca(Text,Busca,Troca : string) : string;
      function RemoverAcento(Texto: String): String;
      function RemoverEspeciais(Texto: String): String;
      function Imprime(Linha: Integer): String;
      function DataExtenso(Data: TDate): String;
      function PasswordInputBox(const ACaption, APrompt:string): string;
      function TextoMaiusculo(Texto: String):String;
      function ID_PESSOA():Integer;
      function RemoverMascara(CPF_CNPJ: String): String;
      function PegarNumero(Texto: String):Integer;
      function Truncar(Valor: Real; Decimais: Integer): Real;
      function Sobrenome(S: String): String;
      function UltimoNome(Name: String): String;
      function FormatarSelo(Selo: String): String;
      function DescricaoTabela(Tabela,Item,Subitem: String): String;
      function RetornarAdicional(Codigo: Integer): String;
      function RetornarTitulo(Codigo: Integer): String;
      function RetornarSigla(Codigo: Integer):String;
      function RetornarEspecie(Sigla: String): Integer;
      function Troca(Texto,Busca,Troca : string) : string;
      function FimNome(Nome: String) : String;
      function DataEscrita(Data: TDate): String;
      function IDEscrevente(CPF: String): Integer;
      function NomeEscrevente(CPF: String): String;
      function QualificacaoEscrevente(CPF: String): String;
      function MatriculaEscrevente(CPF: String): String;
      function DataFinalFeriados(Data:TDateTime; Dias:Integer):TDateTime;
      function DataAnterior(Data: TDateTime; Dias: Integer): TDateTime;
      function TextoParaFloat(Texto: String): Double;
      function RetornarMensagem(ID: Integer): String;
      function RetornarIrregularidade(Codigo: Integer): String;
      function FormatarTelefone(Telefone: String): String;
      function VirgulaDecimal(Valor: Double): String;
      function FimDeSemana(Data: TDate): Boolean;
      function HoraInternet: TDateTime;
      function LerArquivo(Arquivo: String): String;
      function FindForm(F: String): Boolean;
      function Cript(Texto: String; P_Cript: Boolean; Cod: Integer = 50): String;
      function DownloadFile(Origem, Destino: String): Boolean;
      function RetirarEnter(Str: String): String;
      function PortadorNominal(Codigo,Documento,Nome: String): Boolean;
    private
      { Private declarations }
    public
      { Public declarations }
    end;

var
  PF: TPF;

implementation

uses UDM, UPrincipal, UAguarde, UTitulo, DateUtils, UGeral;

function TPF.RetirarEnter(Str: String): String;
var
  I: Integer;
  Temp : String;
begin
  Temp := Str;
  for I := 1 to Length(Temp) do
  begin
      if (Temp[I]=#13) or (Temp[I]=#10) then
      Temp[I]:=' ';
  end;
  Result:=Temp;
end;

function TPF.DownloadFile(Origem,Destino: String): Boolean;
begin
  try
    Result:=UrlDownloadToFile(Nil,PChar(Origem),PChar(Destino),0,Nil)=0;
  except
    Result:=False;
  end;
end;

function TPF.Cript(Texto: String; P_Cript: Boolean; Cod: Integer = 50): String;
var
   X: String;
   I: Integer;
begin
    X:='';
    for I:=1 to Length(Texto) do
    begin
        if P_Cript then
          X:=X+Chr(Ord(Texto[I])+Cod)
            else
              X:=X+Chr(Ord(Texto[I])-Cod);
         Inc(cod);
    end;
    Result:=X;
end;

function TPF.FindForm(F: String): Boolean;
var
   I: Integer;
   R: Boolean;
begin
    R:=False;
    for I:=0 to Screen.FormCount -1 do
      if UpperCase(Screen.Forms[I].Name)=UpperCase(F) then
        R:=True;
    Result:=R;
end;

function TPF.LerArquivo(Arquivo: String): String;
var
  Arq,R,S: String;
  F: TextFile;
begin
  R:='';
  Arq:=Arquivo;
  if FileExists(Arq) then
  begin
      AssignFile(F,Arq);
      Reset(F);
      while not Eof(F) do
      begin
          ReadLn(F,S);
          R:=S;
      end;
      CloseFile(F);
  end;
  Result:=R;
end;

function TPF.FimDeSemana(Data: TDate): Boolean;
begin
  if(DayOfWeek(Data)=1) or (DayOfWeek(Data)=7) then
    Result:=True
      else Result:=False;
end;

function TPF.FormatarTelefone(Telefone: String): String;
begin
  if Telefone='' then
    Result:=''
      else Result:='('+Copy(Telefone,1,2)+')'+Copy(Telefone,3,4)+'-'+Copy(Telefone,7,4);
end;

procedure TPF.CarregarSacador(ID_ATO: Integer);
begin
  dm.Sacadores.Close;
  dm.Sacadores.CommandText:='SELECT * FROM SACADORES WHERE ID_ATO=:ID_ATO';
  dm.Sacadores.Params[0].AsInteger:=ID_ATO;
  dm.Sacadores.Open;

  dm.RxSacador.Close;
  dm.RxSacador.Open;
  dm.RxSacador.Append;
  dm.RxSacadorID_SACADOR.AsInteger    :=dm.SacadoresID_SACADOR.AsInteger;
  dm.RxSacadorNOME.AsString           :=dm.SacadoresNOME.AsString;
  dm.RxSacadorTIPO.AsString           :=dm.SacadoresTIPO.AsString;
  dm.RxSacadorDOCUMENTO.AsString      :=dm.SacadoresDOCUMENTO.AsString;
  dm.RxSacadorENDERECO.AsString       :=dm.SacadoresENDERECO.AsString;
  dm.RxSacadorBAIRRO.AsString         :=dm.SacadoresBAIRRO.AsString;
  dm.RxSacadorMUNICIPIO.AsString      :=dm.SacadoresMUNICIPIO.AsString;
  dm.RxSacadorUF.AsString             :=dm.SacadoresUF.AsString;
end;

procedure TPF.ExportarParaExcel(D: TDataSet; Dir,Arq: String);
var
  F: TextFile;
  H,R: String;
  I: Integer;
begin
  AssignFile(F,Dir+Arq);
  ReWrite(F);
  H:='';
  for i:=0 to D.FieldDefs.Count -1 do H:=H+D.FieldDefs[i].Name+';';
  writeln(F,H);
  D.First;
  while not D.Eof do begin
       R:='';
       for i:=0 to D.FieldDefs.Count -1 do R:=R+D.FieldByName(D.FieldDefs[i].Name).AsString+';';
       writeln(f,r);
       D.next;
  end;
  CloseFile(F);
  MessageBox(0,PChar('Arquivo '+Dir+Arq+' foi criado com sucesso!'),'TOTAL CENTRAL PROTESTO',0+48+8192);
end;

procedure TPF.CarregarPortador(Tipo,Valor,Nome,Pessoa,Documento,Codigo: String);
begin
  //C=C�DIGO
  //D=DOCUMENTO
  //N=NOME

  dm.Portadores.Close;

  if Tipo='C' then dm.Portadores.CommandText:='SELECT * FROM PORTADORES WHERE CODIGO=:P';
  if Tipo='D' then dm.Portadores.CommandText:='SELECT * FROM PORTADORES WHERE DOCUMENTO=:P';
  if Tipo='N' then dm.Portadores.CommandText:='SELECT * FROM PORTADORES WHERE NOME=:P';

  if (Tipo='N') or (Tipo='C') then
    dm.Portadores.Params.ParamByName('P').AsString:=Valor
      else
        dm.Portadores.Params.ParamByName('P').AsString:=GR.PegarNumeroTexto(Valor);
  dm.Portadores.Open;

  dm.RxPortador.Close;
  dm.RxPortador.Open;
  dm.RxPortador.Append;

  if dm.Portadores.IsEmpty then
  begin
      dm.Portadores.Append;
      dm.PortadoresID_PORTADOR.AsInteger  :=dm.IdAtual('ID_PORTADOR','S');
      dm.PortadoresNOME.AsString          :=Nome;
      dm.PortadoresTIPO.AsString          :=Pessoa;
      dm.PortadoresDOCUMENTO.AsString     :=Documento;
      dm.PortadoresCODIGO.AsString        :=Codigo;
      dm.Portadores.Post;
      dm.Portadores.ApplyUpdates(0);
  end;

  dm.RxPortadorID_PORTADOR.AsInteger  := dm.PortadoresID_PORTADOR.AsInteger;
  dm.RxPortadorNOME.AsString          := dm.PortadoresNOME.AsString;
  dm.RxPortadorTIPO.AsString          := dm.PortadoresTIPO.AsString;

  if dm.PortadoresDOCUMENTO.AsString <> '' then
    dm.RxPortadorDOCUMENTO.AsString   := dm.PortadoresDOCUMENTO.AsString;

  if dm.PortadoresCODIGO.AsString <> '' then
    dm.RxPortadorCODIGO.AsString      := dm.PortadoresCODIGO.AsString;

  dm.RxPortadorENDERECO.AsString           := dm.PortadoresENDERECO.AsString;
  dm.RxPortadorBANCO.AsString              := dm.PortadoresBANCO.AsString;
  dm.RxPortadorCONVENIO.AsString           := dm.PortadoresCONVENIO.AsString;
  dm.RxPortadorCONTA.AsString              := dm.PortadoresCONTA.AsString;
  dm.RxPortadorOBSERVACAO.AsString         := dm.PortadoresOBSERVACAO.AsString;
  dm.RxPortadorFLG_PAGAANTECIPADO.AsString := dm.PortadoresFLG_PAGAANTECIPADO.AsString;
end;

function TPF.FimNome(Nome: String) : String;
var
  PNome : String;
begin
  PNome := '';
  if pos ('  ', Nome) <> 0 then
  PNome := copy (Nome, 1, pos ('  ', Nome) - 1);
  Result := PNome;
end;

function TPF.Troca(Texto,Busca,Troca: String) : String;
var
  n: integer;
begin
  for n := 1 to length(Texto) do
    if Copy(Texto,n,1) = Busca then
      begin
        Delete(Texto,n,1);
        Insert(Troca,Texto,n);
      end;
  Result := Texto;

  if Result='0.00' then
    Result:='0';
end;

function TPF.RetornarEspecie(Sigla: String): Integer;
begin
  if not dm.Tipos.Active then
  begin
      dm.Tipos.Close;
      dm.Tipos.Open;
  end;
  if dm.Tipos.Locate('SIGLA',Sigla,[]) then
    Result:=dm.TiposCODIGO.AsInteger
      else Result:=25;
end;

procedure TPF.Aguarde(Status: Boolean = True; Msg: String = 'Aguarde...'; Modo: Char = 'P'; MaisMsg: Boolean = False; TextoMsg: String = '');
begin
  if Status then
  begin
      if not FindForm('FAguarde') then
      Application.CreateForm(TFAguarde,FAguarde);

      FAguarde.P.Caption:='  '+Msg;
      FAguarde.Show;
      FAguarde.UpDate;
  end
  else
    if FindForm('FAguarde') then
    begin
        FAguarde.Free;
    end;
end;

function TPF.FormatarSelo(Selo: String): String;
var
  Letras : String;
  Numeros: String;
  X      : Integer;
begin
  Letras :=Copy(Selo,1,3);
  Numeros:=Copy(Selo,4,5);

  if Selo='' then
  begin
      Result:='';
      Exit;
  end;

  if Length(Numeros)=5 then
    Result:=Selo
  else
  begin
      for X := 1 to 5-Length(Numeros) do
        Numeros:='0'+Numeros;

      Result:=Letras+Numeros
  end;
end;

function TPF.UltimoNome(Name: string): string;
begin
  Result := Name;
  repeat
    Delete(Result,1,Pos(' ',Result));
  until Pos(' ',Result)=0;
end;

function TPF.Sobrenome(S: String): String;
var
  i, Size: Integer;
begin
  i := Pos(#32, S);
  if i = 0 then
     begin
     Result := S;
     Exit;
     end
  else
     begin
     Size := (Length(S) - i);
     Move(S[i + 1], S[1], Size);
     SetLength(S, Size);
     Result := S;
     end;
end;

function TPF.Truncar(Valor: Real; Decimais: Integer): Real;
var
  I       :Integer;
  Virgula :Boolean;
  Texto   :String;
begin
  Virgula :=False;
  Texto   :=FloatToStr(Valor); //Tranforma o par�metro em String

  for i:=0 to Length(Texto) do //Verifica se o Texto possui virgula
    if Texto[I]=',' then
      begin
          Virgula:=True;
          Break;
      end
    else Virgula:=False;

  if Virgula=False then //Se houver virgula
    Texto:=Texto+',00';

  Result:=StrToFloat(Copy(Texto,1,Pos(',',Texto) - 1) + ',' + Copy(Texto,Pos(',',Texto) + 1,Decimais));
end;

function TPF.RemoverMascara(CPF_CNPJ: String): String;
begin
  while Pos('.', CPF_CNPJ) > 0 do
     Delete(CPF_CNPJ, Pos('.', CPF_CNPJ), 1);

  while Pos('-', CPF_CNPJ) > 0 do
     Delete(CPF_CNPJ, Pos('-', CPF_CNPJ), 1);

  while Pos('/', CPF_CNPJ) > 0 do
     Delete(CPF_CNPJ, Pos('/', CPF_CNPJ), 1);

  Result:=CPF_CNPJ;
end;

function TPF.PasswordInputBox(const ACaption, APrompt:string): string;
var
 Form: TForm;
 Prompt: TLabel;
 Edit: TEdit;
 DialogUnits: TPoint;
 ButtonTop, ButtonWidth, ButtonHeight: Integer;
 Value: string;
 I: Integer;
 Buffer: array[0..51] of Char;
begin
 Result := '';
 Form := TForm.Create(Application);
 with Form do
 try
   Font.Name:='Arial';
   Canvas.Font:=Font;
   for I := 0 to 25 do Buffer[I] := Chr(I + Ord('A'));
   for I := 0 to 25 do Buffer[I + 26] := Chr(I + Ord('a'));
   GetTextExtentPoint(Canvas.Handle, Buffer, 52, TSize(DialogUnits));
   DialogUnits.X := DialogUnits.X div 52;
   BorderStyle := bsDialog;
   Caption := ACaption;
   ClientWidth := MulDiv(180, DialogUnits.X, 4);
   ClientHeight := MulDiv(63, DialogUnits.Y, 8);
   Position := poScreenCenter;
   Prompt := TLabel.Create(Form);
   with Prompt do
   begin
     Parent := Form;
     AutoSize := True;
     Left := MulDiv(8, DialogUnits.X, 4);
     Top := MulDiv(8, DialogUnits.Y, 8);
     Caption := APrompt;
   end;
   Edit := TEdit.Create(Form);
   with Edit do
   begin
     Parent := Form;
     Left := Prompt.Left;
     Top := MulDiv(19, DialogUnits.Y, 8);
     Width := MulDiv(164, DialogUnits.X, 4);
     MaxLength := 255;
     PasswordChar := '*';
     SelectAll;
   end;
   ButtonTop := MulDiv(41, DialogUnits.Y, 8);
   ButtonWidth := MulDiv(50, DialogUnits.X, 4);
   ButtonHeight := MulDiv(14, DialogUnits.Y, 8);
   with TButton.Create(Form) do
   begin
     Parent := Form;
     Caption := 'OK';
     ModalResult := mrOk;
     Default := True;
     SetBounds(MulDiv(38, DialogUnits.X, 4),ButtonTop, ButtonWidth,ButtonHeight);
   end;
   with TButton.Create(Form) do
   begin
     Parent := Form;
     Caption := 'Cancel';
     ModalResult := mrCancel;
     Cancel := True;
     SetBounds(MulDiv(92, DialogUnits.X, 4),ButtonTop, ButtonWidth,ButtonHeight);
   end;
   if ShowModal = mrOk then
   begin
     Value := Edit.Text;
     Result := Value;
   end;
 finally
   Form.Free;
 end;
end;

function SubstituiCaractere (vString, vStr1, vStr2 : string) : string;
begin
  while Pos(vStr1, vString) <> 0 do
    vString := Copy(vString, 1, Pos(vStr1, vString)-1)+vStr2+
               Copy(vString, Pos(vStr1, vString)+Length(vStr1), Length(vString)-(Pos(vStr1, vString)+Length(vStr1)-1));
  Result := vString
end; { SubstituiCaractere }

function Alltrim(Cadeia : string) : string;
  var Inicio, Fim : integer;
begin
  Inicio := 1;
  while (Copy(Cadeia, Inicio, 1) = ' ') and (Inicio < Length(Cadeia)) do
    Inc(Inicio);
  Fim := Length(Cadeia);
  while (Copy(Cadeia, Fim, 1) = ' ') and (Fim > Inicio) do
    Dec(Fim);
  Result := Copy(Cadeia, Inicio, Fim-Inicio+1)
end; { Alltrim }

Procedure TPF.FindReplace (const Enc, subs: String; Var Texto: TQRMemo; T:Boolean);
var
  i, Posicao: Integer;
  Linha: string;
Begin
  for i:= 0 to Texto.Lines.count - 1 do
  begin
      Linha := Texto.Lines[i];
      Repeat
          Posicao:=Pos(Enc,Linha);
          if Posicao > 0 then
          begin
              Delete(Linha,Posicao,Length(Enc));
              Insert(Subs,Linha,Posicao);
              if T then Texto.Lines[i]:=Trim(Linha) else Texto.Lines[i]:=Linha;
          end;
      until Posicao=0;
  end;
end;

function TPF.Imprime(Linha: Integer): String;
var
   Arq,R,S: String;
   F: TextFile;
   I: Integer;
begin
     R:='';
     Arq:='C:\TOTAL\NOTAS\IMP.TXT';
     if FileExists(Arq) then begin
        AssignFile(F,Arq);
        Reset(F);
        I:=0;
        while not Eof(F) do begin
              ReadLn(F,S);
              if I=Linha then R:=S;
              Inc(I);
        end;
        CloseFile(F);
     end;
     Result:=R;
end;

function TPF.RemoverEspeciais(Texto: String): String;
var
  i: Integer;
begin
  for i := 1 to Length (Texto) do
  begin
    case Texto[i] of
      '*': Texto[i] := ' ';
      '-': Texto[i] := ' ';
      '/': Texto[i] := ' ';
      '\': Texto[i] := ' ';
      '.': Texto[i] := ' ';
      ':': Texto[i] := ' ';
      '�': Texto[i] := ' ';
      '"': Texto[i] := ' ';
      '�': Texto[i] := ' ';
      '�': Texto[i] := ' ';
      '�': Texto[i] := '2';
      '�': Texto[i] := 'a';
      '�': Texto[i] := 'e';
      '�': Texto[i] := 'i';
      '�': Texto[i] := 'o';
      '�': Texto[i] := 'u';
      '�': Texto[i] := 'a';
      '�': Texto[i] := 'e';
      '�': Texto[i] := 'i';
      '�': Texto[i] := 'o';
      '�': Texto[i] := 'u';
      '�': Texto[i] := 'a';
      '�': Texto[i] := 'e';
      '�': Texto[i] := 'i';
      '�': Texto[i] := 'o';
      '�': Texto[i] := 'u';
      '�': Texto[i] := 'a';
      '�': Texto[i] := 'e';
      '�': Texto[i] := 'i';
      '�': Texto[i] := 'o';
      '�': Texto[i] := 'u';
      '�': Texto[i] := 'a';
      '�': Texto[i] := 'o';
      '�': Texto[i] := 'n';
      '�': Texto[i] := 'c';
      '�': Texto[i] := 'A';
      '�': Texto[i] := 'E';
      '�': Texto[i] := 'I';
      '�': Texto[i] := 'O';
      '�': Texto[i] := 'U';
      '�': Texto[i] := 'A';
      '�': Texto[i] := 'E';
      '�': Texto[i] := 'I';
      '�': Texto[i] := 'O';
      '�': Texto[i] := 'U';
      '�': Texto[i] := 'A';
      '�': Texto[i] := 'E';
      '�': Texto[i] := 'I';
      '�': Texto[i] := 'O';
      '�': Texto[i] := 'U';
      '�': Texto[i] := 'A';
      '�': Texto[i] := 'E';
      '�': Texto[i] := 'I';
      '�': Texto[i] := 'O';
      '�': Texto[i] := 'U';
      '�': Texto[i] := 'A';
      '�': Texto[i] := 'O';
      '�': Texto[i] := 'N';
      '�': Texto[i] := 'C';
      '�': Texto[i] := 'o';
      '�': Texto[i] := 'a';
      '�': Texto[i] := ' ';
      '&': Texto[i] := 'E';
      '�': Texto[i] := 'o';
  end;
end;
  Result := Texto;
end;

function TPF.RemoverAcento(Texto: String): String;
var
  i: Integer;
begin
  for i := 1 to Length (Texto) do
  begin
    case Texto[i] of
      '�': Texto[i] := ' ';
      '"': Texto[i] := ' ';
      '�': Texto[i] := ' ';
      '�': Texto[i] := ' ';
      '�': Texto[i] := '2';
      '�': Texto[i] := 'a';
      '�': Texto[i] := 'e';
      '�': Texto[i] := 'i';
      '�': Texto[i] := 'o';
      '�': Texto[i] := 'u';
      '�': Texto[i] := 'a';
      '�': Texto[i] := 'e';
      '�': Texto[i] := 'i';
      '�': Texto[i] := 'o';
      '�': Texto[i] := 'u';
      '�': Texto[i] := 'a';
      '�': Texto[i] := 'e';
      '�': Texto[i] := 'i';
      '�': Texto[i] := 'o';
      '�': Texto[i] := 'u';
      '�': Texto[i] := 'a';
      '�': Texto[i] := 'e';
      '�': Texto[i] := 'i';
      '�': Texto[i] := 'o';
      '�': Texto[i] := 'u';
      '�': Texto[i] := 'a';
      '�': Texto[i] := 'o';
      '�': Texto[i] := 'n';
      '�': Texto[i] := 'c';
      '�': Texto[i] := 'A';
      '�': Texto[i] := 'E';
      '�': Texto[i] := 'I';
      '�': Texto[i] := 'O';
      '�': Texto[i] := 'U';
      '�': Texto[i] := 'A';
      '�': Texto[i] := 'E';
      '�': Texto[i] := 'I';
      '�': Texto[i] := 'O';
      '�': Texto[i] := 'U';
      '�': Texto[i] := 'A';
      '�': Texto[i] := 'E';
      '�': Texto[i] := 'I';
      '�': Texto[i] := 'O';
      '�': Texto[i] := 'U';
      '�': Texto[i] := 'A';
      '�': Texto[i] := 'E';
      '�': Texto[i] := 'I';
      '�': Texto[i] := 'O';
      '�': Texto[i] := 'U';
      '�': Texto[i] := 'A';
      '�': Texto[i] := 'O';
      '�': Texto[i] := 'N';
      '�': Texto[i] := 'C';
      '�': Texto[i] := 'o';
      '�': Texto[i] := 'a';
      '�': Texto[i] := ' ';
      '&': Texto[i] := 'E';
      '�': Texto[i] := 'o';
  end;
end;
  Result := Texto;
end;

procedure TPF.SetFocusCtrl(Ctrl: TWinControl; ActiveCtrl: TWinControl);
begin
  Ctrl        := ActiveCtrl;
  //ActiveCtrl  := Nil;
  PostMessage(TWinControl(Ctrl).Handle, WM_SETFOCUS, 0, 0);
  TWinControl(Ctrl).SetFocus;
end;

function TPF.BuscaTroca(Text,Busca,Troca : string) : string;
var
  n: integer;
  valor: String;
begin
  for n := 1 to length(Text) do
    begin
      if Text[n]='.' then
        Text[n]:=' ';

      if Text[n]=',' then
        Text[n]:='.';
      //
      valor:=valor+Text[n];
    end;

  for n := 1 to length(Valor) do
    if Copy(Valor,n,1) = Busca then
      begin
        Delete(Valor,n,1);
        Insert(Troca,Valor,n);
      end;
  Result := Valor;
end;

function TPF.Espaco(Campo,Alinha:String; Tam:Integer):String;
var
  i,j: integer;
  s: String;
begin
  s:='';
  j:=Tam-Length(Trim(Campo));
  for i := 1 to j do
    s:=s+' ';
  if Alinha='D' then Result:=s+Trim(Campo) else Result:=Trim(Campo)+s;
end;

function TPF.FormatarData(Data: TDate; Tipo: String): String;
begin
  if Data=0 then
    Result:=''
      else
        if Tipo='I' then {INVERSO}
          Result:=FormatDateTime('mm/dd/yyyy',Data)
            else Result:=FormatDateTime('dd/mm/yyyy',Data);
end;

procedure TPF.EspacoLinhaRichEdit(RichEdit :TRichEdit; Espacamento :Integer; AllText :Boolean);
var
  ParaFormat :TParaFormat2;
  SelStart,
  SelLength :Integer;
begin
  SelStart  := RichEdit.SelStart;
  SelLength := RichEdit.SelLength;
  if AllText then
    RichEdit.SelectAll;
  ParaFormat.cbSize := SizeOf(ParaFormat);
  ParaFormat.dwMask := PFM_LINESPACING or PFM_SPACEAFTER;
  ParaFormat.dyLineSpacing := Espacamento;
  ParaFormat.bLineSpacingRule := Espacamento;
  SendMessage(RichEdit.handle, EM_SETPARAFORMAT, 0, LongInt(@ParaFormat));
// Restaura sele��o caso tenhamos mudado para All
  RichEdit.SelStart := SelStart;
  RichEdit.SelLength := SelLength;
end;


procedure TPF.JustificarRichEdit(RichEdit :TRichEdit; AllText :Boolean);
const
  TO_ADVANCEDTYPOGRAPHY   = $1;
  EM_SETTYPOGRAPHYOPTIONS = (WM_USER + 202);
  EM_GETTYPOGRAPHYOPTIONS = (WM_USER + 203);
var
  ParaFormat :TParaFormat;
  SelStart,
  SelLength :Integer;
begin
  ParaFormat.cbSize := SizeOf(ParaFormat);
  if SendMessage(RichEdit.handle,
              EM_SETTYPOGRAPHYOPTIONS, 
              TO_ADVANCEDTYPOGRAPHY, 
              TO_ADVANCEDTYPOGRAPHY) = 1 then
  begin
    SelStart := RichEdit.SelStart;
    SelLength := RichEdit.SelLength;
    if AllText then
      RichEdit.SelectAll;
    ParaFormat.dwMask := PFM_ALIGNMENT;
    ParaFormat.wAlignment := PFA_JUSTIFY;
    SendMessage(RichEdit.handle, EM_SETPARAFORMAT, 0, LongInt(@ParaFormat));
// Restaura sele��o caso tenhamos mudado para All
    RichEdit.SelStart := SelStart;
    RichEdit.SelLength := SelLength;
  end;
end;

function TPF.MaskString(Valor: String) : String;
begin
  Result := FormatMaskText('!;0; ',(FormatFloat('#,##0.00',StrToFloat(valor))));
end;

function TPF.RemoverCaracter(Texto,Excluir: String): String;
var
  i: integer;
begin
  for i:=0 to Length(Excluir) do
    while Pos(Excluir[i],Texto) > 0 do
      Delete(Texto, Pos(Excluir[i],Texto),1);
   Result := Texto;
end;

function TPF.VerificarData(Texto: String; Campo: TField{; Componente: TDBEdit}): boolean;
var
  Data: TDate;
begin
  try
    if Trim(Texto) <> '/  /' then
      begin
        Data := StrToDate(Texto);
        Campo.AsDateTime := Data;
      end
    else
      begin
        Campo.Clear;
        //Componente.Clear;
      end;
  except
    on econverterror do
      begin
        MessageBox(0,'Data Inv�lida!','TOTAL NOTAS',0+48+8192);
        Campo.Clear;
        //Componente.Clear;
        //Componente.SetFocus;
        Abort;
      end;
  end;
  Result:=False;
end;

function TPF.FormatarCPF(CPF: string): string;
begin
  if (CPF <> '') then
    Result:=Copy(CPF,1,3)+'.'+Copy(CPF,4,3)+'.'+Copy(CPF,7,3)+'-'+Copy(CPF,10,2);
end;

function TPF.VerificarCNPJ(CNPJ: String):Boolean;
var
  d1,d4,xx,nCount,fator,resto,digito1,digito2 : Integer;
  Check : String;
begin
  d1 := 0;
  d4 := 0;
  xx := 1;
  for nCount := 1 to Length( CNPJ )-2 do begin
    if Pos( Copy( CNPJ, nCount, 1 ), '/-.' ) = 0 then begin
      if xx < 5 then
        fator := 6 - xx
      else
        fator := 14 - xx;
      d1 := d1 + StrToInt( Copy( CNPJ, nCount, 1 ) ) * fator;
      if xx < 6 then
        fator := 7 - xx
      else
      fator := 15 - xx;
      d4 := d4 + StrToInt( Copy( CNPJ, nCount, 1 ) ) * fator;
      xx := xx+1;
    end;{IF}
  end;{FOR}
  resto := (d1 mod 11);
  if resto < 2 then
    digito1 := 0
  else
   digito1 := 11 - resto;
  d4 := d4 + 2 * digito1;
  resto := (d4 mod 11);
  if resto < 2 then
    digito2 := 0
  else
    digito2 := 11 - resto;
  Check := IntToStr(Digito1) + IntToStr(Digito2);
  if Check <> copy(CNPJ,succ(length(CNPJ)-2),2) then  
    Result := False // CNPJ Incorreto.
  else
    Result := True; // CNPJ Correto.
end;

function TPF.VerificarCPF(numero: string): Boolean;
var
  n1, n2, n3, n4, n5, n6, n7, n8, n9: integer;
  d1, d2: integer;
  digitado, calculado: string;

begin
  Result := True;
  if (Numero<>'') then
    begin
      if (Numero='00000000000') or
         (Numero='11111111111') or
         (Numero='22222222222') or
         (Numero='33333333333') or
         (Numero='44444444444') or
         (Numero='55555555555') or
         (Numero='66666666666') or
         (Numero='77777777777') or
         (Numero='88888888888') or
         (Numero='99999999999') then
        begin
          Result:=False; // CPF Incorreto.
          Exit;
        end;

      if Length(Numero) = 11 then
        begin
          n1 := StrToInt(numero[1]);
          n2 := StrToInt(numero[2]); 
          n3 := StrToInt(numero[3]); 
          n4 := StrToInt(numero[4]); 
          n5 := StrToInt(numero[5]); 
          n6 := StrToInt(numero[6]); 
          n7 := StrToInt(numero[7]); 
          n8 := StrToInt(numero[8]); 
          n9 := StrToInt(numero[9]); 

          d1 := n9 * 2 + n8 * 3 + n7 * 4 + n6 * 5 + n5 * 6 + n4 * 7 + n3 * 8 + n2 * 9 + n1 * 10;
          d1 := 11 - (d1 mod 11);
          if d1 >= 10 then d1 := 0;

          d2 := d1 * 2 + n9 * 3 + n8 * 4 + n7 * 5 + n6 * 6 + n5 * 7 + n4 * 8 + n3 * 9 + n2 * 10 + n1 * 11;
          d2 := 11 - (d2 mod 11);
          if d2>=10 then d2:=0;

          calculado := inttostr(d1) + inttostr(d2);
          digitado  := numero[10] + numero[11];

          if calculado = digitado then
              Result  := True
          else
            begin
              Result:=False; // CPF Incorreto.
              Exit;
            end;
        end
      else
        begin
          Result:=False; // CPF Incorreto.
          Exit;
        end;
    end;
end;

function TPF.FormatarCNPJ(CNPJ: String): string;
begin
    Result:=Copy(CNPJ,1,2)+'.'+Copy(CNPJ,3,3)+'.'+Copy(CNPJ,6,3)+'/'+Copy(CNPJ,9,4)+'-'+Copy(CNPJ,13,2);
end;

function PadR(Cadeia : string; Tamanho : integer; Caractere : string) : string;
begin
  while Length(Cadeia) < Tamanho do
    Cadeia := Cadeia + Caractere;
  Result := Copy(Cadeia, 1, Tamanho)
end; { PadR }

function JstString(StringInicial: string; TamanhoFinal : byte) : string;
var
  AUXString: string;
  TamanhoAUXString , i: byte;
begin
  AUXString := Alltrim(StringInicial);
  AUXString := SubstituiCaractere(AUXString, '  ', ' ');
  TamanhoAUXString := Length(AUXString);
  i := TamanhoAUXString;
  while (TamanhoAUXString < TamanhoFinal) and (Pos(' ', AUXString)<>0) do
    begin
      while (Copy(AUXString, i, 1) <> ' ') and (i > 0) do
        Dec(i);
      if i > 0 then
        begin
          AUXString := Copy(AUXString, 1, i-1)+' '+Copy(AUXString, i, TamanhoAUXString+1);
          Inc(TamanhoAUXString)
        end; { if }
      while (Copy(AUXString, i, 1) = ' ') and (i > 0) do
        Dec(i);
      if i = 0 then
        i := TamanhoAUXString
    end; { while }
  Result := AUXString
end; { JstString }


function TPF.JstParagrafo (Paragrafo : string; Largura : word) : string;
var
  Inicio, Fim : word;
begin
  if Largura = 0 then
    Largura := 1;
  Paragrafo := Alltrim(Paragrafo);
  Paragrafo := SubstituiCaractere(Paragrafo, #10, '');
  Paragrafo := SubstituiCaractere(Paragrafo, #13, '');
  Paragrafo := SubstituiCaractere(Paragrafo, '  ', ' ');
  Inicio:=1;
  Result:='';
  while Inicio <= Length(Paragrafo) do begin
    while (Inicio <= Length(Paragrafo)) and (Copy(Paragrafo, Inicio, 1) = ' ') do
      Inc(Inicio);
    Fim := Inicio + Largura; // -1;
    if Fim <= Length(Paragrafo) then
      begin
        while (Fim > Inicio) and (Copy(Paragrafo, Fim, 1) <> ' ') do
          Dec(Fim);
        while (Fim > Inicio) and (Copy(Paragrafo, Fim, 1) = ' ') do
          Dec(Fim);
        if Fim = Inicio then
          Fim := Inicio + Largura-1;
      end; { if }
    if Fim >= Length(Paragrafo) then
      Result := Result + PadR(Alltrim(Copy(Paragrafo, Inicio, Fim-Inicio+1)), Largura, ' ')
    else
      Result := Result + Alltrim(JstString(Copy(Paragrafo, Inicio, Fim-Inicio+1), Largura)){+#13+#13+}+#10;
    Inicio := Fim+1;
  end { while }
end; { JstParagrafo }

procedure TPF.Excluir(SQL: String; Conexao:TSQLConnection);
var
  Q:TSQLQuery;
  Query:String;
begin
  Query:='DELETE FROM '+SQL;
  Q:=TSQLQuery.Create(Nil);
  Q.SQLConnection:=Conexao;
  Q.Close;
  Q.SQL.Clear;
  Q.SQL.Add(Query);
  Q.ExecSQL(True);
  Q.Close;
  Q.Free;
end;

procedure TPF.ExcluirFD(SQL: String; Conexao: TFDConnection);
var
  Q: TFDQuery;
  Query: String;
begin
  Query := 'DELETE FROM ' + SQL;

  Q := TFDQuery.Create(nil);
  Q.Connection := Conexao;

  Q.Close;
  Q.SQL.Clear;
  Q.SQL.Add(Query);
  Q.ExecSQL(True);
  Q.Close;

  FreeAndNil(Q);
end;

function TPF.DataExtenso(Data: TDate): String;
var
  A,M,D   :Word;
  Ano,Mes :String;
begin
  DecodeDate(Data,A,M,D);
  if Copy(IntToStr(A),1,1)='1' then Ano:='mil';
  if Copy(IntToStr(A),1,1)='2' then Ano:='dois mil';
  if Copy(IntToStr(A),1,1)='3' then Ano:='tr�s mil';
  if Copy(IntToStr(A),1,1)='4' then Ano:='quatro mil';
  if Copy(IntToStr(A),1,1)='5' then Ano:='cinco mil';
  if Copy(IntToStr(A),1,1)='6' then Ano:='seis mil';
  if Copy(IntToStr(A),1,1)='7' then Ano:='sete mil';
  if Copy(IntToStr(A),1,1)='8' then Ano:='oito mil';
  if Copy(IntToStr(A),1,1)='9' then Ano:='nove mil';

  if Copy(IntToStr(A),2,1)='0' then Ano:=Ano;
  if Copy(IntToStr(A),2,1)='1' then Ano:=Ano+' cento';
  if Copy(IntToStr(A),2,1)='2' then Ano:=Ano+' duzentos';
  if Copy(IntToStr(A),2,1)='3' then Ano:=Ano+' trezentos';
  if Copy(IntToStr(A),2,1)='4' then Ano:=Ano+' quatrocentos';
  if Copy(IntToStr(A),2,1)='5' then Ano:=Ano+' quinhentos';
  if Copy(IntToStr(A),2,1)='6' then Ano:=Ano+' seiscentos';
  if Copy(IntToStr(A),2,1)='7' then Ano:=Ano+' setecentos';
  if Copy(IntToStr(A),2,1)='8' then Ano:=Ano+' oitocentos';
  if Copy(IntToStr(A),2,1)='9' then Ano:=Ano+' novecentos';

  if Copy(IntToStr(A),3,1)='0' then
  begin
      if Copy(IntToStr(A),4,1)='0' then Ano:=Ano;
      if Copy(IntToStr(A),4,1)='1' then Ano:=Ano+' e um';
      if Copy(IntToStr(A),4,1)='2' then Ano:=Ano+' e dois';
      if Copy(IntToStr(A),4,1)='3' then Ano:=Ano+' e tr�s';
      if Copy(IntToStr(A),4,1)='4' then Ano:=Ano+' e quatro';
      if Copy(IntToStr(A),4,1)='5' then Ano:=Ano+' e cinco';
      if Copy(IntToStr(A),4,1)='6' then Ano:=Ano+' e seis';
      if Copy(IntToStr(A),4,1)='7' then Ano:=Ano+' e sete';
      if Copy(IntToStr(A),4,1)='8' then Ano:=Ano+' e oito';
      if Copy(IntToStr(A),4,1)='9' then Ano:=Ano+' e nove';
  end;

  if Copy(IntToStr(A),3,1)='1' then
  begin
      if Copy(IntToStr(A),4,1)='0' then Ano:=Ano+' e dez';
      if Copy(IntToStr(A),4,1)='1' then Ano:=Ano+' e onze';
      if Copy(IntToStr(A),4,1)='2' then Ano:=Ano+' e doze';
      if Copy(IntToStr(A),4,1)='3' then Ano:=Ano+' e treze';
      if Copy(IntToStr(A),4,1)='4' then Ano:=Ano+' e quatorze';
      if Copy(IntToStr(A),4,1)='5' then Ano:=Ano+' e quinze';
      if Copy(IntToStr(A),4,1)='6' then Ano:=Ano+' e dezesseis';
      if Copy(IntToStr(A),4,1)='7' then Ano:=Ano+' e dezessete';
      if Copy(IntToStr(A),4,1)='8' then Ano:=Ano+' e dezoito';
      if Copy(IntToStr(A),4,1)='9' then Ano:=Ano+' e dezenove';
  end;

  if Copy(IntToStr(A),3,1)='2' then
  begin
      if Copy(IntToStr(A),4,1)='0' then Ano:=Ano+' e vinte';
      if Copy(IntToStr(A),4,1)='1' then Ano:=Ano+' e vinte e um';
      if Copy(IntToStr(A),4,1)='2' then Ano:=Ano+' e vinte e dois';
      if Copy(IntToStr(A),4,1)='3' then Ano:=Ano+' e vinte e tr�s';
      if Copy(IntToStr(A),4,1)='4' then Ano:=Ano+' e vinte e quatro';
      if Copy(IntToStr(A),4,1)='5' then Ano:=Ano+' e vinte e cinco';
      if Copy(IntToStr(A),4,1)='6' then Ano:=Ano+' e vinte e seis';
      if Copy(IntToStr(A),4,1)='7' then Ano:=Ano+' e vinte e sete';
      if Copy(IntToStr(A),4,1)='8' then Ano:=Ano+' e vinte e oito';
      if Copy(IntToStr(A),4,1)='9' then Ano:=Ano+' e vinte e nove';
  end;

  if Copy(IntToStr(A),3,1)='3' then
  begin
      if Copy(IntToStr(A),4,1)='0' then Ano:=Ano+' e trinta';
      if Copy(IntToStr(A),4,1)='1' then Ano:=Ano+' e trinta e um';
      if Copy(IntToStr(A),4,1)='2' then Ano:=Ano+' e trinta e dois';
      if Copy(IntToStr(A),4,1)='3' then Ano:=Ano+' e trinta e tr�s';
      if Copy(IntToStr(A),4,1)='4' then Ano:=Ano+' e trinta e quatro';
      if Copy(IntToStr(A),4,1)='5' then Ano:=Ano+' e trinta e cinco';
      if Copy(IntToStr(A),4,1)='6' then Ano:=Ano+' e trinta e seis';
      if Copy(IntToStr(A),4,1)='7' then Ano:=Ano+' e trinta e sete';
      if Copy(IntToStr(A),4,1)='8' then Ano:=Ano+' e trinta e oito';
      if Copy(IntToStr(A),4,1)='9' then Ano:=Ano+' e trinta e nove';
  end;

  if Copy(IntToStr(A),3,1)='4' then
  begin
      if Copy(IntToStr(A),4,1)='0' then Ano:=Ano+' e quarenta';
      if Copy(IntToStr(A),4,1)='1' then Ano:=Ano+' e quarenta e um';
      if Copy(IntToStr(A),4,1)='2' then Ano:=Ano+' e quarenta e dois';
      if Copy(IntToStr(A),4,1)='3' then Ano:=Ano+' e quarenta e tr�s';
      if Copy(IntToStr(A),4,1)='4' then Ano:=Ano+' e quarenta e quatro';
      if Copy(IntToStr(A),4,1)='5' then Ano:=Ano+' e quarenta e cinco';
      if Copy(IntToStr(A),4,1)='6' then Ano:=Ano+' e quarenta e seis';
      if Copy(IntToStr(A),4,1)='7' then Ano:=Ano+' e quarenta e sete';
      if Copy(IntToStr(A),4,1)='8' then Ano:=Ano+' e quarenta e oito';
      if Copy(IntToStr(A),4,1)='9' then Ano:=Ano+' e quarenta e nove';
  end;

  if Copy(IntToStr(A),3,1)='5' then
  begin
      if Copy(IntToStr(A),4,1)='0' then Ano:=Ano+' e cinquenta';
      if Copy(IntToStr(A),4,1)='1' then Ano:=Ano+' e cinquenta e um';
      if Copy(IntToStr(A),4,1)='2' then Ano:=Ano+' e cinquenta e dois';
      if Copy(IntToStr(A),4,1)='3' then Ano:=Ano+' e cinquenta e tr�s';
      if Copy(IntToStr(A),4,1)='4' then Ano:=Ano+' e cinquenta e quatro';
      if Copy(IntToStr(A),4,1)='5' then Ano:=Ano+' e cinquenta e cinco';
      if Copy(IntToStr(A),4,1)='6' then Ano:=Ano+' e cinquenta e seis';
      if Copy(IntToStr(A),4,1)='7' then Ano:=Ano+' e cinquenta e sete';
      if Copy(IntToStr(A),4,1)='8' then Ano:=Ano+' e cinquenta e oito';
      if Copy(IntToStr(A),4,1)='9' then Ano:=Ano+' e cinquenta e nove';
  end;

  if Copy(IntToStr(A),3,1)='6' then
  begin
      if Copy(IntToStr(A),4,1)='0' then Ano:=Ano+' e sessenta';
      if Copy(IntToStr(A),4,1)='1' then Ano:=Ano+' e sessenta e um';
      if Copy(IntToStr(A),4,1)='2' then Ano:=Ano+' e sessenta e dois';
      if Copy(IntToStr(A),4,1)='3' then Ano:=Ano+' e sessenta e tr�s';
      if Copy(IntToStr(A),4,1)='4' then Ano:=Ano+' e sessenta e quatro';
      if Copy(IntToStr(A),4,1)='5' then Ano:=Ano+' e sessenta e cinco';
      if Copy(IntToStr(A),4,1)='6' then Ano:=Ano+' e sessenta e seis';
      if Copy(IntToStr(A),4,1)='7' then Ano:=Ano+' e sessenta e sete';
      if Copy(IntToStr(A),4,1)='8' then Ano:=Ano+' e sessenta e oito';
      if Copy(IntToStr(A),4,1)='9' then Ano:=Ano+' e sessenta e nove';
  end;

  if Copy(IntToStr(A),3,1)='7' then
  begin
      if Copy(IntToStr(A),4,1)='0' then Ano:=Ano+' e setenta';
      if Copy(IntToStr(A),4,1)='1' then Ano:=Ano+' e setenta e um';
      if Copy(IntToStr(A),4,1)='2' then Ano:=Ano+' e setenta e dois';
      if Copy(IntToStr(A),4,1)='3' then Ano:=Ano+' e setenta e tr�s';
      if Copy(IntToStr(A),4,1)='4' then Ano:=Ano+' e setenta e quatro';
      if Copy(IntToStr(A),4,1)='5' then Ano:=Ano+' e setenta e cinco';
      if Copy(IntToStr(A),4,1)='6' then Ano:=Ano+' e setenta e seis';
      if Copy(IntToStr(A),4,1)='7' then Ano:=Ano+' e setenta e sete';
      if Copy(IntToStr(A),4,1)='8' then Ano:=Ano+' e setenta e oito';
      if Copy(IntToStr(A),4,1)='9' then Ano:=Ano+' e setenta e nove';
  end;

  if Copy(IntToStr(A),3,1)='8' then
  begin
      if Copy(IntToStr(A),4,1)='0' then Ano:=Ano+' e oitenta';
      if Copy(IntToStr(A),4,1)='1' then Ano:=Ano+' e oitenta e um';
      if Copy(IntToStr(A),4,1)='2' then Ano:=Ano+' e oitenta e dois';
      if Copy(IntToStr(A),4,1)='3' then Ano:=Ano+' e oitenta e tr�s';
      if Copy(IntToStr(A),4,1)='4' then Ano:=Ano+' e oitenta e quatro';
      if Copy(IntToStr(A),4,1)='5' then Ano:=Ano+' e oitenta e cinco';
      if Copy(IntToStr(A),4,1)='6' then Ano:=Ano+' e oitenta e seis';
      if Copy(IntToStr(A),4,1)='7' then Ano:=Ano+' e oitenta e sete';
      if Copy(IntToStr(A),4,1)='8' then Ano:=Ano+' e oitenta e oito';
      if Copy(IntToStr(A),4,1)='9' then Ano:=Ano+' e oitenta e nove';
  end;

  if Copy(IntToStr(A),3,1)='9' then
  begin
      if Copy(IntToStr(A),4,1)='0' then Ano:=Ano+' e noventa';
      if Copy(IntToStr(A),4,1)='1' then Ano:=Ano+' e noventa e um';
      if Copy(IntToStr(A),4,1)='2' then Ano:=Ano+' e noventa e dois';
      if Copy(IntToStr(A),4,1)='3' then Ano:=Ano+' e noventa e tr�s';
      if Copy(IntToStr(A),4,1)='4' then Ano:=Ano+' e noventa e quatro';
      if Copy(IntToStr(A),4,1)='5' then Ano:=Ano+' e noventa e cinco';
      if Copy(IntToStr(A),4,1)='6' then Ano:=Ano+' e noventa e seis';
      if Copy(IntToStr(A),4,1)='7' then Ano:=Ano+' e noventa e sete';
      if Copy(IntToStr(A),4,1)='8' then Ano:=Ano+' e noventa e oito';
      if Copy(IntToStr(A),4,1)='9' then Ano:=Ano+' e noventa e nove';
  end;

  if M=1  then Mes:='Janeiro';
  if M=2  then Mes:='Fevereiro';
  if M=3  then Mes:='Mar�o';
  if M=4  then Mes:='Abril';
  if M=5  then Mes:='Maio';
  if M=6  then Mes:='Junho';
  if M=7  then Mes:='Julho';
  if M=8  then Mes:='Agosto';
  if M=9  then Mes:='Setembro';
  if M=10 then Mes:='Outubro';
  if M=11 then Mes:='Novembro';
  if M=12 then Mes:='Dezembro';

  Result:=Ano+' ('+IntToStr(A)+'), no '+IntToStr(D)+'� dia do m�s de '+Mes;
end;

{JUSTIFICA SEM ESPA�O ENTRE LINHAS}
function TPF.JstParagrafo2 (Paragrafo : string; Largura : word) : string;
var
  Inicio, Fim : word;
begin
  if Largura = 0 then
    Largura := 1;
  Paragrafo := Alltrim(Paragrafo);
  Paragrafo := SubstituiCaractere(Paragrafo, #10, '');
  Paragrafo := SubstituiCaractere(Paragrafo, #13, '');
  Paragrafo := SubstituiCaractere(Paragrafo, '  ', ' ');
  Inicio:=1;
  Result:='';
  while Inicio <= Length(Paragrafo) do begin
    while (Inicio <= Length(Paragrafo)) and (Copy(Paragrafo, Inicio, 1) = ' ') do
      Inc(Inicio);
    Fim := Inicio + Largura; // -1;
    if Fim <= Length(Paragrafo) then
      begin
        while (Fim > Inicio) and (Copy(Paragrafo, Fim, 1) <> ' ') do
          Dec(Fim);
        while (Fim > Inicio) and (Copy(Paragrafo, Fim, 1) = ' ') do
          Dec(Fim);
        if Fim = Inicio then
          Fim := Inicio + Largura-1;
      end; { if }
    if Fim >= Length(Paragrafo) then
      Result := Result + PadR(Alltrim(Copy(Paragrafo, Inicio, Fim-Inicio+1)), Largura, ' ')
    else
      Result := Result + Alltrim(JstString(Copy(Paragrafo, Inicio, Fim-Inicio+1), Largura))+#13+#13+#10;
    Inicio := Fim+1;
  end { while }
end; { JstParagrafo }

function TPF.TextoMaiusculo(Texto: String): String;
var
  i: Integer;
begin
  for i := 1 to Length (Texto) do
  begin
    case Texto[i] of
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
      '�': Texto[i] := '�';
  end;
end;
  Result := UpperCase(Texto);
end;

function TPF.ID_PESSOA: Integer;
begin
  CriarFDQuery(dm.conCENTRAL, dm.Q1);

  dm.Q1.SQL.Add('SELECT GEN_ID(ID_PESSOAS,1) AS ID_ATUAL FROM RDB$DATABASE');
  dm.Q1.Open;

  Result := dm.Q1.FieldByName('ID_ATUAL').AsInteger;

  LiberarFDQuery(dm.Q1);
end;

procedure TPF.CriarSQLQuery(Conexao: TSQLConnection; Query: TSQLQuery);
begin
  Query:=TSQLQuery.Create(Nil);
  Query.SQLConnection:=Conexao;
  Query.Close;
  Query.SQL.Clear;
end;

procedure TPF.CriarArquivo(DirNome,Texto: String);
var
  F: TextFile;
begin
  if not FileExists(DirNome) then
  begin
      AssignFile(F,DirNome);
      Rewrite(F);
      Writeln(F,Texto);
      CloseFile(F);
  end;
end;

procedure TPF.CriarFDQuery(Conexao: TFDConnection; Query: TFDQuery);
begin
  Query := TFDQuery.Create(Nil);
  Query.Connection := Conexao;
  Query.Close;
  Query.SQL.Clear;
end;

procedure TPF.LiberarFDQuery(Query: TFDQuery);
begin
  Query.Close;
  FreeAndNil(Query);
end;

procedure TPF.LiberarQuery(Query: TSQLQuery);
begin
  Query.Close;
  Query.Free;
end;

procedure TPF.VisualizarArquivo(Arquivo: String);
begin
  ShellExecute(Handle,'open',PChar(Arquivo),nil,nil,SW_SHOWMAXIMIZED);
end;

function TPF.PegarNumero(Texto: String): Integer;
var
  I: Integer;
  N: String;
begin
  for I := 0 to Length(Texto) do
    if Texto[I] in ['0'..'9'] then N:=N+Texto[I];

  if N<>'' then
    Result:=StrToInt(N)
      else Result:=0;
end;

procedure TPF.DesativarIndices(Tabela:String);
var
  SQL,Nome: String;
begin
  try
    PF.CriarFDQuery(dm.conSISTEMA, dm.Q2);

    dm.Q2.Close;
    dm.Q2.Connection := dm.conSISTEMA;
    dm.Q2.SQL.Clear;
    SQL:='select rdb$index_name as indice from rdb$indices where rdb$relation_name='+QuotedStr(Tabela);
    dm.Q2.SQL.Add(SQL);
    dm.Q2.Open;
    dm.Q2.First;

    PF.CriarFDQuery(dm.conSISTEMA, dm.Q1);

    while not dm.Q2.Eof do
    begin
        Application.ProcessMessages;
        Nome:=Trim(dm.Q2.FieldByName('indice').AsString);
        if (Copy(Nome,1,2)<>'PK') then
        begin
            dm.Q1.SQL.Clear;
            dm.Q1.SQL.Add('alter index '+Nome+' inactive');
            dm.Q1.ExecSQL();
        end;
        dm.Q2.Next;
    end;
    PF.LiberarFDQuery(dm.Q1);
    PF.LiberarFDQuery(dm.Q2);
  except
  end;
end;

procedure TPF.AtivarIndices(Tabela: String);
var
  SQL,Nome: String;
begin
  try
    PF.CriarFDQuery(dm.conSISTEMA,dm.Q2);
    dm.Q2.Close;
    dm.Q2.Connection:=dm.conSISTEMA;
    dm.Q2.SQL.Clear;
    SQL:='select rdb$index_name as indice from rdb$indices where rdb$relation_name='+QuotedStr(Tabela);
    dm.Q2.SQL.Add(SQL);
    dm.Q2.Open;
    dm.Q2.First;

    PF.CriarFDQuery(dm.conSISTEMA,dm.Q1);
    while not dm.Q2.Eof do
    begin
        Application.ProcessMessages;
        Nome:=Trim(dm.Q2.FieldByName('indice').AsString);
        if (Copy(Nome,1,2)<>'PK') then
        begin
            dm.Q1.SQL.Clear;
            dm.Q1.SQL.Add('alter index '+Nome+' active');
            dm.Q1.ExecSQL();
        end;
        dm.Q2.Next;
    end;
    PF.LiberarFDQuery(dm.Q1);
    PF.LiberarFDQuery(dm.Q2);
  except
  end;
end;

procedure TPF.CarregarDevedor(ID_ATO: Integer);
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Close;
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT * FROM DEVEDORES WHERE ID_ATO='+IntToStr(ID_ATO)+' ORDER BY ORDEM');
  Q.Open;

  dm.RxDevedor.Close;
  dm.RxDevedor.Open;

  while not Q.Eof do
  begin
      dm.RxDevedor.Append;
      dm.RxDevedorID_DEVEDOR.AsInteger    :=Q.FieldByName('ID_DEVEDOR').AsInteger;
      dm.RxDevedorORDEM.AsInteger         :=Q.FieldByName('ORDEM').AsInteger;
      dm.RxDevedorNOME.AsString           :=Q.FieldByName('NOME').AsString;
      dm.RxDevedorTIPO.AsString           :=Q.FieldByName('TIPO').AsString;
      dm.RxDevedorDOCUMENTO.AsString      :=Q.FieldByName('DOCUMENTO').AsString;
      dm.RxDevedorIFP_DETRAN.AsString     :=Q.FieldByName('IFP_DETRAN').AsString;
      dm.RxDevedorIDENTIDADE.AsString     :=Q.FieldByName('IDENTIDADE').AsString;
      dm.RxDevedorORGAO.AsString          :=Q.FieldByName('ORGAO').AsString;
      dm.RxDevedorENDERECO.AsString       :=Q.FieldByName('ENDERECO').AsString;
      dm.RxDevedorBAIRRO.AsString         :=Q.FieldByName('BAIRRO').AsString;
      dm.RxDevedorMUNICIPIO.AsString      :=Q.FieldByName('MUNICIPIO').AsString;
      dm.RxDevedorUF.AsString             :=Q.FieldByName('UF').AsString;
      dm.RxDevedorIGNORADO.AsString       :=Q.FieldByName('IGNORADO').AsString;
      dm.RxDevedorJUSTIFICATIVA.AsString  :=Q.FieldByName('JUSTIFICATIVA').AsString;
      dm.RxDevedorCEP.AsString            :=Q.FieldByName('CEP').AsString;
      dm.RxDevedorDT_EMISSAO.AsDateTime   :=Q.FieldByName('DT_EMISSAO').AsDateTime;
      dm.RxDevedor.Post;
      Q.Next;
  end;

  dm.RxDevedor.SortOnFields('ORDEM');
  dm.RxDevedor.First;
  Q.Close;
  Q.Free;
end;

procedure TPF.CarregarCustas(ID_ATO: Integer);
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('select * from CUSTAS where ID_ATO=:ID');
  Q.ParamByName('ID').AsInteger:=ID_ATO;
  Q.Open;
  Q.First;
  dm.RxCustas.Close;
  dm.RxCustas.Open;
  while not Q.Eof do
  begin
      dm.RxCustas.Append;
      dm.RxCustasID_CUSTA.AsInteger   :=Q.FieldByName('ID_CUSTA').AsInteger;
      dm.RxCustasID_ATO.AsInteger     :=Q.FieldByName('ID_ATO').AsInteger;
      dm.RxCustasTABELA.AsString      :=Q.FieldByName('TABELA').AsString;
      dm.RxCustasITEM.AsString        :=Q.FieldByName('ITEM').AsString;
      dm.RxCustasSUBITEM.AsString     :=Q.FieldByName('SUBITEM').AsString;
      dm.RxCustasVALOR.AsFloat        :=Q.FieldByName('VALOR').AsFloat;
      dm.RxCustasQTD.AsInteger        :=Q.FieldByName('QTD').AsInteger;
      dm.RxCustasTOTAL.AsFloat        :=Q.FieldByName('TOTAL').AsFloat;
      dm.RxCustasDESCRICAO.AsString   :=DescricaoTabela(dm.RxCustasTABELA.AsString,
                                                        dm.RxCustasITEM.AsString,
                                                        dm.RxCustasSUBITEM.AsString);
      dm.RxCustas.Post;
      Q.Next;
  end;
  Q.Close;
  Q.Free;
end;

function TPF.DescricaoTabela(Tabela, Item, Subitem: String): String;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conCENTRAL;
  Q.SQL.Add('SELECT DESCR FROM CONCUS WHERE TAB=:T AND ITEM=:I AND SUB=:S');
  Q.ParamByName('T').AsString:=Tabela;
  Q.ParamByName('I').AsString:=Item;
  Q.ParamByName('S').AsString:=Subitem;
  Q.Open;
  Result:=Q.FieldByName('DESCR').AsString;
  Q.Close;
  Q.Free;
end;

function TPF.RetornarTitulo(Codigo: Integer): String;
begin
  dm.Tipos.Close;
  dm.Tipos.Open;
  if dm.Tipos.Locate('CODIGO',Codigo,[]) then
    Result:=dm.TiposDESCRICAO.AsString
      else Result:='N�O INFORMADO';
end;

function TPF.DataEscrita(Data: TDate): String;
var
  D,M,A: String;
begin
  D:=FormatDateTime('DD',Data);
  M:=FormatDateTime('MMMM',Data);
  A:=FormatDateTime('YYYY',Data);

  if StrToInt(D)>1 then
    Result:='aos '+D+' dias do m�s de '+M+' do ano de '+A
      else Result:='ao '+D+'� dia do m�s de '+M+' do ano de '+A;
end;

function TPF.NomeEscrevente(CPF: String): String;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT NOME FROM ESCREVENTES WHERE CPF=:CPF');
  Q.ParamByName('CPF').AsString:=CPF;
  Q.Open;
  Result:=Q.FieldByName('NOME').AsString;
  Q.Close;
  Q.Free;
end;

function TPF.QualificacaoEscrevente(CPF: String): String;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT QUALIFICACAO FROM ESCREVENTES WHERE CPF=:CPF');
  Q.ParamByName('CPF').AsString:=CPF;
  Q.Open;
  Result:=Q.FieldByName('QUALIFICACAO').AsString;
  Q.Close;
  Q.Free;
end;

function TPF.MatriculaEscrevente(CPF: String): String;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT MATRICULA FROM ESCREVENTES WHERE CPF=:CPF');
  Q.ParamByName('CPF').AsString:=CPF;
  Q.Open;
  Result:=Q.FieldByName('MATRICULA').AsString;
  Q.Close;
  Q.Free;
end;

function TPF.Zeros(Campo, Alinha: String; Tam: Integer): String;
var
  i,j: integer;
  s: String;
begin
  s:='';
  j:=Tam-Length(Trim(Campo));
  for i := 1 to j do
    s:=s+'0';
  if Alinha='D' then Result:=s+Trim(Campo) else Result:=Trim(Campo)+s;
end;

function TPF.RetornarSigla(Codigo: Integer): String;
begin
  dm.Tipos.Close;
  dm.Tipos.Open;
  if dm.Tipos.Locate('CODIGO',Codigo,[]) then
    Result:=dm.TiposSIGLA.AsString
      else Result:='';

  if Codigo=-1 then
  Result:='OT';
end;

function TPF.TextoParaFloat(Texto: String): Double;
var
  dValor: Double;
  sValor: String;
begin
  dValor:=StrToFloat(Texto);
  sValor:=FloatToStr(dValor);
  Result:=StrToFloat(Copy(sValor,1,Length(sValor)-2)+','+Copy(sValor,Length(sValor)-1,Length(sValor)));
end;

function TPF.RetornarMensagem(ID: Integer): String;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT MENSAGEM FROM MENSAGENS WHERE ID_MSG=:ID');
  Q.ParamByName('ID').AsInteger:=ID;
  Q.Open;
  Result:=Q.FieldByName('MENSAGEM').AsString;
  Q.Close;
  Q.Free;
end;

function TPF.VirgulaDecimal(Valor: Double): String;
var
  Numeros: String;
begin
  Numeros :=GR.PegarNumeroTexto(FloatToStrF(Valor,ffNumber,7,2));
  Result  :=Copy(Numeros,1,Length(Numeros)-2)+'.'+Copy(Numeros,Length(Numeros)-1,Length(Numeros));
end;

function TPF.HoraInternet: TDateTime;
var
  IDSntp: TIDSntp;
begin
  IDSntp:=TIdSNTP.Create(Nil);
  try
    IDSntp.Host:='pool.ntp.br';
    Result:=IDSntp.DateTime;
  finally
    IDSntp.Free;
  end;
end;

function TPF.RetornarAdicional(Codigo: Integer): String;
begin
  dm.Codigos.Close;
  dm.Codigos.Params.ParamByName('OCULTO1').AsString:='N';
  dm.Codigos.Params.ParamByName('OCULTO2').AsString:='N';
  dm.Codigos.Open;
  if dm.Codigos.Locate('COD',Codigo,[]) then
    Result:=dm.CodigosATOS.AsString
      else Result:='PROTOCOLIZACAO DE TITULOS OU QUALQUER OUTRO DOCUMENTO DE DIVIDA';
end;

function TPF.RetornarIrregularidade(Codigo: Integer): String;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT MOTIVO FROM IRREGULARIDADES WHERE CODIGO=:CODIGO');
  Q.Close;
  Q.ParamByName('CODIGO').AsInteger:=Codigo;
  Q.Open;
  Result:=Q.FieldByName('MOTIVO').AsString;
  Q.Close;
  Q.Free;
end;

function TPF.FormatarCEP(Cep: String): String;
begin
  Result:=Copy(Cep,1,2)+'.'+Copy(Cep,3,3)+'-'+Copy(Cep,6,3);
end;

procedure TPF.BuscarApresentante(Tipo, Valor: String);
begin
  //C=C�DIGO
  //D=DOCUMENTO
  //N=NOME

  {O APRESENTANTE TEM QUE EXISTIR SEN�O N�O CADASTRO}

  dm.Portadores.Close;

  if Tipo='C' then dm.Portadores.CommandText:='SELECT * FROM PORTADORES WHERE CODIGO=:P';
  if Tipo='D' then dm.Portadores.CommandText:='SELECT * FROM PORTADORES WHERE DOCUMENTO=:P';
  if Tipo='N' then dm.Portadores.CommandText:='SELECT * FROM PORTADORES WHERE NOME=:P';

  if (Tipo='N') or (Tipo='C') then
    dm.Portadores.Params.ParamByName('P').AsString:=Valor
      else
        dm.Portadores.Params.ParamByName('P').AsString:=GR.PegarNumeroTexto(Valor);
  dm.Portadores.Open;
  if dm.Portadores.IsEmpty then
  MessageBox(0,PChar('Portador n�o cadastrado.'),'TOTAL CENTRAL PROTESTO',0+48+8192);
end;

function TPF.IDEscrevente(CPF: String): Integer;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT ID_ESCREVENTE FROM ESCREVENTES WHERE CPF=:CPF');
  Q.ParamByName('CPF').AsString:=CPF;
  Q.Open;
  Result:=Q.FieldByName('ID_ESCREVENTE').AsInteger;
  Q.Close;
  Q.Free;
end;

function TPF.DataAnterior(Data: TDateTime; Dias: Integer): TDateTime;
var
  W,D,M,A: Integer;
  Q: TFDQuery;
begin
  Data:=Data-Dias;
  W:=DayOfWeek(Data);
  if (W=1) or (W=7) then
    repeat
      Data:=Data-1;
    until
      (DayOfWeek(Data)<>1) and (DayOfWeek(Data)<>7);

  Result:=Data;

  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT * FROM FERIADOS WHERE DIA=:D AND MES=:M');
  Q.ParamByName('D').AsInteger:=D;
  Q.ParamByName('M').AsInteger:=M;
  Q.Open;
  if not Q.IsEmpty then
  begin
      repeat
          Dec(D);
          if D>31 then
          begin
              D:=1;
              Dec(M);
              if M>12 then
              begin
                  D:=1;
                  M:=1;
              end;
          end;

          W:=DayOfWeek(StrToDate(IntToStr(D)+'/'+IntToStr(M)+'/'+IntToStr(A)))-1;
          Result:=StrToDate(IntToStr(D)+'/'+IntToStr(M)+'/'+IntToStr(A))+1+((1-1+W) div 5)*2;

          D:=DayOf(Result);
          M:=MonthOf(Result);
          A:=YearOf(Result);

          Q.Close;
          Q.ParamByName('D').AsInteger:=D;
          Q.ParamByName('M').AsInteger:=M;
          Q.Open;
      until Q.IsEmpty;
  end;
  Q.Close;
  Q.Free;

  //Result:=StrToDate(IntToStr(D)+'/'+IntToStr(M)+'/'+IntToStr(A));
end;

function TPF.DataFinalFeriados(Data: TDateTime; Dias: Integer): TDateTime;
var
  I: Integer;
  Q: TFDQuery;
  Proximo: TDate;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT * FROM FERIADOS WHERE DIA=:DIA AND MES=:MES');

  I:=1;
  Proximo:=Data;
  while I<=Dias do
  begin
      Proximo:=Proximo+1;
      Q.Close;
      Q.ParamByName('DIA').AsInteger:=DayOf(Proximo);
      Q.ParamByName('MES').AsInteger:=MonthOf(Proximo);
      Q.Open;

      if (DayOfTheWeek(Proximo) in [1..5]) then
        if (Q.IsEmpty) then
          Inc(I);
  end;
  Result:=Proximo;
  Q.Close;
  Q.Free;
end;

function TPF.PortadorNominal(Codigo,Documento,Nome: String): Boolean;
var
  Q: TFDQuery;
begin
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  if Codigo<>'' then
    Q.SQL.Add('SELECT NOMINAL FROM PORTADORES WHERE CODIGO='+QuotedStr(Codigo))
      else
        if GR.PegarNumeroTexto(Documento)<>'' then
          Q.SQL.Add('SELECT NOMINAL FROM PORTADORES WHERE DOCUMENTO='+QuotedStr(Documento))
            else
              Q.SQL.Add('SELECT NOMINAL FROM PORTADORES WHERE NOME='+QuotedStr(Nome));
  Q.Open;
  if not Q.IsEmpty then
    Result:=Q.FieldByName('NOMINAL').AsString='S'
      else Result:=False;

  Q.Close;
  Q.Free;
end;

end.
