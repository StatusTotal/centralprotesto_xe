unit UExportarTxt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, sMemo, DBCtrls, sDBLookupComboBox,
  DB, Mask, sMaskEdit, sCustomComboEdit, sTooledit, Buttons, sBitBtn,
  sGroupBox, FMTBcd, Grids, Wwdbigrd, Wwdbgrid, DBClient, Provider, SqlExpr,
  sDBEdit, sEdit, DateUtils, Menus, ComCtrls, sStatusBar, QuickRpt, QRCtrls,
  SimpleDS, sPageControl, sComboBox, sLabel, RxMemDS, Data.DBXInterBase,
  Data.DBXFirebird, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFExportarTxt = class(TForm)
    P1: TsPanel;
    M: TsMemo;
    RgTipo: TsRadioGroup;
    btVisualizar: TsBitBtn;
    btExportar: TsBitBtn;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    dspTitulos: TDataSetProvider;
    Titulos: TClientDataSet;
    dsTitulos: TDataSource;
    edData: TsDateEdit;
    PM: TPopupMenu;
    MarcarTodos1: TMenuItem;
    DesmarcarTodos1: TMenuItem;
    Barra: TsStatusBar;
    edRemessa: TsDBEdit;
    dsRemessas: TDataSource;
    btRejeitar: TsBitBtn;
    N1: TMenuItem;
    Imprimir1: TMenuItem;
    qkRelatorio: TQuickRep;
    QRBand1: TQRBand;
    lbCidade: TQRLabel;
    lbCartorio: TQRLabel;
    lbRecibo: TQRLabel;
    lbPeriodo: TQRLabel;
    QRBand2: TQRBand;
    ChildBand1: TQRChildBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRBand3: TQRBand;
    QRSysData1: TQRSysData;
    TitulosID_ATO: TIntegerField;
    TitulosDT_PROTOCOLO: TDateField;
    TitulosPROTOCOLO: TIntegerField;
    TitulosDT_REGISTRO: TDateField;
    TitulosDT_PAGAMENTO: TDateField;
    TitulosDISTRIBUICAO: TFloatField;
    TitulosTOTAL: TFloatField;
    TitulosNUMERO_TITULO: TStringField;
    TitulosVALOR_TITULO: TFloatField;
    TitulosSALDO_PROTESTO: TFloatField;
    TitulosCONVENIO: TStringField;
    TitulosAPRESENTANTE: TStringField;
    TitulosNOSSO_NUMERO: TStringField;
    TitulosAGENCIA_CEDENTE: TStringField;
    TitulosDT_SUSTADO: TDateField;
    TitulosDT_RETIRADO: TDateField;
    TitulosSTATUS: TStringField;
    TitulosEXPORTADO: TStringField;
    TitulosCheck: TStringField;
    TitulosTARIFA_BANCARIA: TFloatField;
    TitulosRETORNO: TStringField;
    TitulosDT_DEVOLVIDO: TDateField;
    TitulosVALOR_AR: TFloatField;
    TitulosIRREGULARIDADE: TIntegerField;
    TitulosSALDO_TITULO: TFloatField;
    TitulosCODIGO: TStringField;
    TitulosSEQUENCIA: TIntegerField;
    TitulosAGENCIA: TStringField;
    TitulosVALOR_TED: TFloatField;
    N2: TMenuItem;
    MenuPortador: TMenuItem;
    Panel1: TPanel;
    G1: TwwDBGrid;
    dsOutros: TDataSource;
    RXOutros: TRxMemoryData;
    RXOutrosID_PORTADOR: TIntegerField;
    RXOutrosNOME: TStringField;
    RXOutrosCODIGO: TStringField;
    lkOutros: TsDBLookupComboBox;
    TitulosID_PORTADOR: TIntegerField;
    TitulosELETRONICO: TStringField;
    TitulosCODIGO_APRESENTANTE: TStringField;
    qryOutros: TFDQuery;
    qryTitulos: TFDQuery;
    qryOutrosID_PORTADOR: TIntegerField;
    qryOutrosCODIGO: TStringField;
    qryOutrosNOME: TStringField;
    qryOutrosTIPO: TStringField;
    qryOutrosDOCUMENTO: TStringField;
    qryOutrosENDERECO: TStringField;
    qryOutrosBANCO: TStringField;
    qryOutrosCONVENIO: TStringField;
    qryOutrosCONTA: TStringField;
    qryOutrosOBSERVACAO: TMemoField;
    qryOutrosAGENCIA: TStringField;
    qryOutrosPRACA: TStringField;
    qryOutrosCRA: TStringField;
    qryOutrosSEQUENCIA: TIntegerField;
    qryOutrosVALOR_DOC: TFloatField;
    qryOutrosVALOR_TED: TFloatField;
    qryOutrosNOMINAL: TStringField;
    qryOutrosFORCA_LEI: TStringField;
    qryOutrosESPECIE: TStringField;
    qryTitulosID_ATO: TIntegerField;
    qryTitulosAPRESENTANTE: TStringField;
    qryTitulosCODIGO_APRESENTANTE: TStringField;
    qryTitulosPROTOCOLO: TIntegerField;
    qryTitulosNUMERO_TITULO: TStringField;
    qryTitulosSTATUS: TStringField;
    qryTitulosAGENCIA_CEDENTE: TStringField;
    qryTitulosNOSSO_NUMERO: TStringField;
    qryTitulosEXPORTADO: TStringField;
    qryTitulosRETORNO: TStringField;
    qryTitulosVALOR_TITULO: TFloatField;
    qryTitulosSALDO_TITULO: TFloatField;
    qryTitulosSALDO_PROTESTO: TFloatField;
    qryTitulosDT_DEVOLVIDO: TDateField;
    qryTitulosDT_PROTOCOLO: TDateField;
    qryTitulosDT_PAGAMENTO: TDateField;
    qryTitulosDT_RETIRADO: TDateField;
    qryTitulosDT_SUSTADO: TDateField;
    qryTitulosDISTRIBUICAO: TFloatField;
    qryTitulosTARIFA_BANCARIA: TFloatField;
    qryTitulosVALOR_AR: TFloatField;
    qryTitulosTOTAL: TFloatField;
    qryTitulosDT_REGISTRO: TDateField;
    qryTitulosIRREGULARIDADE: TIntegerField;
    qryTitulosCONVENIO: TStringField;
    qryTitulosELETRONICO: TStringField;
    qryTitulosID_PORTADOR: TIntegerField;
    qryTitulosCODIGO: TStringField;
    qryTitulosSEQUENCIA: TIntegerField;
    qryTitulosAGENCIA: TStringField;
    qryTitulosVALOR_TED: TFloatField;
    lcbServentiaAgregada: TsDBLookupComboBox;
    dsServentiaAgregada: TDataSource;
    qryTitulosCARTORIO: TIntegerField;
    TitulosCARTORIO: TIntegerField;
    qryTitulosDT_ENTRADA: TDateField;
    TitulosDT_ENTRADA: TDateField;
    qryTitulosPROTOCOLO_CARTORIO: TIntegerField;
    TitulosPROTOCOLO_CARTORIO: TIntegerField;
    pmExportar: TPopupMenu;
    MenuItem1: TMenuItem;
    qryTitulosPOSTECIPADO: TStringField;
    TitulosxDIA28: TBooleanField;
    TitulosPOSTECIPADO: TStringField;
    procedure btVisualizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GerarTxt;
    procedure btExportarClick(Sender: TObject);
    procedure edRemessaKeyPress(Sender: TObject; var Key: Char);
    procedure TitulosCalcFields(DataSet: TDataSet);
    procedure G1ColEnter(Sender: TObject);
    procedure MarcarTodos1Click(Sender: TObject);
    procedure DesmarcarTodos1Click(Sender: TObject);
    procedure edRemessaExit(Sender: TObject);
    procedure btRejeitarClick(Sender: TObject);
    procedure Imprimir1Click(Sender: TObject);
    procedure TitulosAfterPost(DataSet: TDataSet);
    procedure G1KeyPress(Sender: TObject; var Key: Char);
    procedure edDataChange(Sender: TObject);
    procedure PMPopup(Sender: TObject);
    procedure MenuPortadorClick(Sender: TObject);
    procedure edInicioKeyPress(Sender: TObject; var Key: Char);
    procedure edFimKeyPress(Sender: TObject; var Key: Char);
    procedure G1MouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure P1MouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure BarraMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure lkOutrosClick(Sender: TObject);

    function Marcados: Integer;
    function Vazio: Boolean;
    function PagosMarcados(IdAto,IdPortador: Integer): Integer;
    procedure lcbServentiaAgregadaKeyPress(Sender: TObject; var Key: Char);
    procedure CriarArquivoRetornoCartorio(pDiretorio,pArquivo:String);
    procedure MenuItem1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Arquivo: String;
    vApresentante: String;
    Dia, Mes, Ano: Word;
    vDiretorio:String;
  end;

var
  FExportarTxt: TFExportarTxt;
  CalcFields1,CalcFields2: Boolean;
  Posicao: Integer;

implementation

uses UDM, UPF, UIrregularidades, UGeral, StrUtils, UGDM, Math;

{$R *.dfm}

function TFExportarTxt.Vazio: Boolean;
var
  Posicao: Integer;
begin
  Result  := True;
  Posicao := Titulos.RecNo;

  Titulos.DisableControls;

  Titulos.First;

  while not Titulos.Eof do
  begin
    if TitulosCheck.AsString = 'S' then
    begin
      Result := False;
      Break;
    end;

    Titulos.Next;
  end;

  Titulos.RecNo := Posicao;
  Titulos.EnableControls;
end;

procedure TFExportarTxt.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFExportarTxt.GerarTxt;
var
  P: Pointer;
  vInicio: Boolean;
  vSoma,vSaldo,vDocTed: Double;
  vQtd,vLinha,vIdAtual,vIdAto,vPagos: Integer;
  Ocorrencia, Data, Irregularidade, vValorDocTed,
  NomeCredor, vCodigo, vCustasC, vCustasD: String;
begin
  P:=Titulos.GetBookmark;
  Titulos.DisableControls;

  Titulos.Filtered:=False;
  Titulos.Filter:='Check='+QuotedStr('S');
  Titulos.Filtered:=True;

  Titulos.First;

  vLinha    :=1;
  vInicio   :=True;
  vIdAtual  :=TitulosID_PORTADOR.AsInteger;
  vCodigo   :=TitulosCODIGO.AsString;

  M.Lines.Clear;

  while not Titulos.Eof do
  begin
    Application.ProcessMessages;
    if vInicio then
    begin
        vQtd   :=0;
        vSoma  :=0;
        vSaldo :=0;

        if dm.ServentiaCODIGO.AsInteger=1866 then
          NomeCredor:='CREDOR'
            else NomeCredor:=Copy(TitulosAPRESENTANTE.AsString,1,40);

        vIdAto:=TitulosID_ATO.AsInteger;

        repeat
          Inc(vQtd);
          Titulos.Next;
        until (TitulosID_PORTADOR.AsInteger<>vIdAtual) or (Titulos.Eof);

        Titulos.Locate('ID_ATO',vIdAto,[]);

        M.Lines.Add('0'+GR.Zeros(Copy(TitulosCODIGO.AsString,1,3),'D',3)+
                        GR.Espaco(NomeCredor,'E',40)+
                        FormatDateTime('ddmmyyyy',edData.Date)+'SDTBFORTP'+
                        GR.Zeros(TitulosSEQUENCIA.AsString,'D',6)+
                        GR.Zeros(IntToStr(vQtd),'D',4)+
                        '000000000000'+
                        GR.Zeros(TitulosAGENCIA.AsString,'E',6)+
                        '043'+
                        GR.Zeros(dm.ServentiaCODIGO_PRACA.AsString, 'E', 7) +
                        GR.Espaco(' ','E',497)+
                        GR.Zeros(IntToStr(vLinha),'D',4));

        Inc(vLinha);
    end;

    vInicio:=False;

    vPagos:=PagosMarcados(TitulosID_ATO.AsInteger,vIdAtual);
    if (TitulosVALOR_TED.AsFloat<>0) and (vPagos<>0) then
      vDocTed:=GR.NoRound(TitulosVALOR_TED.AsFloat/vPagos,2)
        else vDocTed:=0;

    Titulos.Edit;
    TitulosEXPORTADO.AsString :='S';
    TitulosRETORNO.AsString   :=Arquivo;

    if TitulosSALDO_TITULO.AsFloat=0 then
    TitulosSALDO_TITULO.AsFloat:=TitulosVALOR_TITULO.AsFloat;

    Titulos.Post;
    if Titulos.ApplyUpdates(0)>0 then raise Exception.Create('Error Titulos.ApplyUpdates');

    vValorDocTed := '';

    vCustasC := '0000000000'; {S� ALTERO ESSE CAMPO SE FOR CANCELAMENTO ELETR�NICO DE ITA�}

    if TitulosDT_ENTRADA.AsDateTime > StrToDate('25/10/2017') then
    begin
      if Trim(TitulosCONVENIO.AsString) = 'S' then
      begin
        if dm.PagamentoAntecipadoConvenio(TitulosCODIGO_APRESENTANTE.AsString) then
          vCustasD := '0000000000'
        else
          vCustasD := GR.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(TitulosDISTRIBUICAO.AsFloat, 2), ffNumber, 7, 2)), 'D', 10);
      end
      else
        vCustasD := GR.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(TitulosDISTRIBUICAO.AsFloat, 2), ffNumber, 7, 2)), 'D', 10);
    end
    else
      vCustasD := GR.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(TitulosDISTRIBUICAO.AsFloat, 2), ffNumber, 7, 2)), 'D', 10);

    if TitulosSTATUS.AsString='PAGO' then
    begin
      Ocorrencia  :='1';
      Data        :=FormatDateTime('ddmmyyyy',TitulosDT_PAGAMENTO.AsDateTime);
      {vSaldo      :=IfThen(TitulosCONVENIO.AsString='S',
                           TitulosSALDO_TITULO.AsFloat,
                           TitulosSALDO_TITULO.AsFloat+(TitulosTOTAL.AsFloat-TitulosTARIFA_BANCARIA.AsFloat)); }
      vValorDocTed:=GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(vDocTed,2),ffNumber,7,2));

      if dm.ServentiaCODIGO.AsInteger = 4473 then
        vSaldo := vSaldo - TitulosVALOR_AR.AsFloat;

      if not TitulosxDIA28.AsBoolean then
      begin
        vSaldo      :=IfThen((TitulosCONVENIO.AsString='S') AND (NOT dm.PagamentoAntecipadoConvenio(TitulosCODIGO_APRESENTANTE.AsString)),
                             TitulosSALDO_TITULO.AsFloat,
                             TitulosSALDO_TITULO.AsFloat+(TitulosTOTAL.AsFloat-TitulosTARIFA_BANCARIA.AsFloat));

      end
      else
      begin
        if TitulosPOSTECIPADO.AsString='P' then
        begin
          vSaldo:=TitulosSALDO_TITULO.AsFloat;
          //vCustasC:=GR.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(TitulosTOTAL.AsFloat,2),ffNumber,7,2)),'D',10);
        end
        else
        begin
          vSaldo:=TitulosSALDO_TITULO.AsFloat+(TitulosTOTAL.AsFloat-TitulosTARIFA_BANCARIA.AsFloat);
        end;
      end;

    end;
          
    if TitulosSTATUS.AsString='PROTESTADO' then
    begin
      Ocorrencia := '2';
      Data       := FormatDateTime('ddmmyyyy', TitulosDT_REGISTRO.AsDateTime);
      vSaldo     := TitulosSALDO_TITULO.AsFloat;
    end;

    if TitulosSTATUS.AsString='RETIRADO' then
    begin
      if TitulosIRREGULARIDADE.AsInteger=98 then
      begin
        Ocorrencia:='5'{PGNF};
        vSaldo:=TitulosSALDO_PROTESTO.AsFloat;
      end
      else
      begin
        Ocorrencia:='3';
        vSaldo:=TitulosSALDO_TITULO.AsFloat;
      end;

      Data:=FormatDateTime('ddmmyyyy',TitulosDT_RETIRADO.AsDateTime);

      if TitulosPOSTECIPADO.AsString='P' then
        vCustasC:=GR.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(TitulosTOTAL.AsFloat,2),ffNumber,7,2)),'D',10);
    end;

    {JOGO 3 COMO RETIRADO POIS S� VAI PRO RETORNO SUSTA��O EXTRAJUDICIAL}
    {22/03/2016 - MUDEI PARA 4 DEPOIS DE CONSULTAR A LARISSA NO INSTITUTO}
    if TitulosSTATUS.AsString='SUSTADO' then
    begin
      Ocorrencia:='4';
      Data      :=FormatDateTime('ddmmyyyy',TitulosDT_SUSTADO.AsDateTime);
      vSaldo    :=TitulosSALDO_TITULO.AsFloat;
    end;

    if TitulosSTATUS.AsString='CANCELADO' then
    begin
      Ocorrencia:='A';
      Data      :=FormatDateTime('ddmmyyyy',TitulosDT_PAGAMENTO.AsDateTime);
      vSaldo    :=TitulosSALDO_TITULO.AsFloat;

      if not TitulosxDIA28.AsBoolean then
      begin
          if (TitulosCODIGO_APRESENTANTE.AsString='341') and (TitulosELETRONICO.AsString='S') then
            vCustasC:=GR.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(GR.ValorCampoDAC('TITULOS',
                                                                                          'TOTAL','F',dm.conSISTEMA,
                                                                                          ' ID_ATO='+TitulosID_ATO.AsString),
                                                                                          2),ffNumber,7,2)),'D',10)
          else
            if not dm.PagamentoAntecipadoConvenio(TitulosCODIGO_APRESENTANTE.AsString) then
              vCustasC:= GR.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(GR.ValorCampoDAC('TITULOS',
                                                                                          'TOTAL','F',dm.conSISTEMA,
                                                                                          ' ID_ATO='+TitulosID_ATO.AsString),
                                                                                          2),ffNumber,7,2)),'D',10);
      end
      else
      begin
          if TitulosPOSTECIPADO.AsString='P' then
            vCustasC:=GR.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(GR.ValorCampoDAC('TITULOS',
                                                                                          'TOTAL','F',dm.conSISTEMA,
                                                                                          ' ID_ATO='+TitulosID_ATO.AsString),
                                                                                          2),ffNumber,7,2)),'D',10);
      end;
    end;

    if (TitulosSTATUS.AsString='DEVOLVIDO') then
    begin
      if TitulosIRREGULARIDADE.AsString='' then
      begin
        GR.Aviso('Informe o motivo da devolu��o do protocolo n� '+TitulosPROTOCOLO.AsString+'.');
        Titulos.Locate('PROTOCOLO',TitulosPROTOCOLO.AsInteger,[]);
        btRejeitarClick(Self);
      end;

      {SEM CUSTAS}
      if TitulosCONVENIO.AsString='S' then
      begin
        Ocorrencia:='5';
        Data      :=FormatDateTime('ddmmyyyy',TitulosDT_DEVOLVIDO.AsDateTime);
        vSaldo    :=TitulosSALDO_TITULO.AsFloat;
      end
      else
      begin
        Ocorrencia:='6';
        Data      :=FormatDateTime('ddmmyyyy',TitulosDT_DEVOLVIDO.AsDateTime);
        vSaldo    :=TitulosSALDO_TITULO.AsFloat;
      end;
    end;

    vSaldo:=GR.NoRound(vSaldo,2);
    vSoma :=vSoma+vSaldo;

    {COMO CRIEI UMA TABELA COM C�DIGO NEGATIVO "RETIRADO PELO CLIENTE" (N�O PODERIA CRIAR)}
    {SE FOR ESSE ITEM EU COLOCO 00, SENDO QUE S� RETIRADO TEM ESSA OP��O}
    if TitulosIRREGULARIDADE.AsInteger > 0 then
      Irregularidade := PF.Zeros(TitulosIRREGULARIDADE.AsString, 'D', 2)
    else
      Irregularidade := '00';

    M.Lines.Add('1'+
                GR.Zeros(TitulosCODIGO.AsString,'D',3)+
                GR.Espaco(TitulosAGENCIA_CEDENTE.AsString,'E',15)+
                GR.Espaco(' ','E',45)+
                GR.Espaco(' ','E',45)+
                GR.Espaco(' ','E',14)+
                GR.Espaco(' ','E',45)+
                '00000000'+
                GR.Espaco(' ','E',20)+
                GR.Espaco(' ','E',2)+
                GR.Espaco(TitulosNOSSO_NUMERO.AsString,'E',15)+
                GR.Espaco(' ','E',3)+
                GR.Espaco(' ','E',11)+
                '00000000'+
                '00000000'+
                '001'+
                GR.Zeros(GR.PegarNumeroTexto(GR.FormatarFloat(TitulosVALOR_TITULO.AsFloat)),'D',14)+
                GR.Zeros(GR.PegarNumeroTexto(GR.FormatarFloat(vSaldo)),'D',14)+
                GR.Espaco(' ','E',20)+
                GR.Espaco(' ','E',1)+
                GR.Espaco(' ','E',1)+
                GR.Espaco(' ','E',1)+
                GR.Espaco(' ','E',45)+
                '000'+
                '00000000000000'+
                GR.Espaco(' ','E',11)+
                GR.Espaco(' ','E',45)+
                '00000000'+
                GR.Espaco(' ','E',20)+
                GR.Espaco(' ','E',2)+
                GR.Zeros(Trim(TitulosCARTORIO.AsString), 'D', 2) +  //dm.ValorParametro(23)+
                GR.Zeros(TitulosPROTOCOLO.AsString,'D',10)+
                Ocorrencia+
                FormatDateTime('ddmmyyyy',TitulosDT_PROTOCOLO.AsDateTime)+
                vCustasC+
                GR.Espaco(' ','E',1)+
                Data+
                Irregularidade+
                GR.Espaco(' ','E',20)+
                GR.Zeros(GR.PegarNumeroTexto(FloatToStrF(GR.NoRound(TitulosDISTRIBUICAO.AsFloat,2),ffNumber,7,2)),'D',10) +
                '000000'+
                '0000000000'+
                '00000'+
                '000000000000000'+
                '000'+
                '0'+
                GR.Espaco(' ','E',8)+
                GR.Espaco(' ','E',1)+
                GR.iif(TitulosPOSTECIPADO.AsString.Trim='P','P',' ')+
                //GR.Espaco(' ','E',1)+
                GR.Zeros(vValorDocTed,'D',10)+
                GR.Espaco(' ','E',19)+
                GR.Zeros(IntToStr(vLinha),'D',4));

    Inc(vLinha);

    Titulos.Next;

    if (TitulosID_PORTADOR.AsInteger<>vIdAtual) or (Titulos.Eof) then
    begin
      M.Lines.Add('9'+
                  GR.Zeros(Copy(vCodigo,1,3),'D',3)+
                  GR.Espaco(NomeCredor,'E',40)+
                  FormatDateTime('ddmmyyyy',edData.Date)+
                  GR.Zeros(IntToStr(vQtd),'D',5)+
                  GR.Zeros(GR.PegarNumeroTexto(GR.FormatarFloat(vSoma)),'D',18)+
                  GR.Espaco(' ','E',521)+
                  GR.Zeros(IntToStr(vLinha),'D',4));

      GR.ExecutarSQLDAC('UPDATE PORTADORES SET SEQUENCIA=SEQUENCIA+1 WHERE ID_PORTADOR='+IntToStr(vIdAtual),dm.conSISTEMA);

      vIdAtual:=TitulosID_PORTADOR.AsInteger;
      vCodigo :=TitulosCODIGO.AsString;
      vInicio :=True;
      vLinha  :=1;
    end;
  end;

  Titulos.Filtered:=False;

  if P<>Nil then
    if Titulos.BookmarkValid(P) then
      Titulos.GotoBookmark(P);
  Titulos.EnableControls;
end;

procedure TFExportarTxt.btVisualizarClick(Sender: TObject);
begin
  PF.Aguarde(True);
  CalcFields1:=True;
  CalcFields2:=True;

  Titulos.Close;
  Titulos.Params.ParamByName('CR1').AsString     := IfThen(RXOutrosID_PORTADOR.AsInteger = -1, 'S', '*');
  Titulos.Params.ParamByName('CR2').AsString     := IfThen(RXOutrosID_PORTADOR.AsInteger = -1, 'S', '*');
  Titulos.Params.ParamByName('BC1').AsString     := IfThen(RXOutrosID_PORTADOR.AsInteger = -1, '*', '*');
  Titulos.Params.ParamByName('BC2').AsString     := IfThen(RXOutrosID_PORTADOR.AsInteger = -1, '*', '*');
  Titulos.Params.ParamByName('CV1').AsString     := IfThen(RXOutrosID_PORTADOR.AsInteger = -1, '*', '*');
  Titulos.Params.ParamByName('CV2').AsString     := IfThen(RXOutrosID_PORTADOR.AsInteger = -1, '*', '*');
  Titulos.Params.ParamByName('ID1').AsInteger    := RXOutrosID_PORTADOR.AsInteger;
  Titulos.Params.ParamByName('ID2').AsInteger    := RXOutrosID_PORTADOR.AsInteger;
  Titulos.Params.ParamByName('D11').AsDate       := edInicio.Date;
  Titulos.Params.ParamByName('D12').AsDate       := edFim.Date;
  Titulos.Params.ParamByName('D21').AsDate       := edInicio.Date;
  Titulos.Params.ParamByName('D22').AsDate       := edFim.Date;
  Titulos.Params.ParamByName('D31').AsDate       := edInicio.Date;
  Titulos.Params.ParamByName('D32').AsDate       := edFim.Date;
  Titulos.Params.ParamByName('D41').AsDate       := edInicio.Date;
  Titulos.Params.ParamByName('D42').AsDate       := edFim.Date;
  Titulos.Params.ParamByName('D51').AsDate       := edInicio.Date;
  Titulos.Params.ParamByName('D52').AsDate       := edFim.Date;

  if Trim(lcbServentiaAgregada.Text) = '' then
  begin
    Titulos.Params.ParamByName('CART01').AsInteger := 0;
    Titulos.Params.ParamByName('CART02').AsInteger := 0;
  end
  else
  begin
    Titulos.Params.ParamByName('CART01').AsInteger := lcbServentiaAgregada.KeyValue;
    Titulos.Params.ParamByName('CART02').AsInteger := lcbServentiaAgregada.KeyValue;
  end;

  Titulos.Open;

  Barra.Panels[1].Text:=IntToStr(Titulos.RecordCount);

  MarcarTodos1.Click;

  PF.Aguarde(False);
end;

procedure TFExportarTxt.CriarArquivoRetornoCartorio(pDiretorio,
  pArquivo: String);
var
  P: Pointer;
  vInicio: Boolean;
  vSoma,vSaldo,vDocTed: Double;
  vQtd,vLinha,vIdAtual,vIdAto,vPagos: Integer;
  Ocorrencia, Data, Irregularidade, vValorDocTed,
  NomeCredor, vCodigo, vCustasC, vCustasD: String;
begin
  P:=Titulos.GetBookmark;
  Titulos.DisableControls;

  Titulos.Filtered:=False;
  Titulos.Filter:='Check='+QuotedStr('S');
  Titulos.Filtered:=True;

  Titulos.First;

  if not Titulos.IsEmpty then
  begin
      M.Lines.Clear;

      while not Titulos.Eof do
      begin
        Application.ProcessMessages;

        M.Lines.Add(GR.Zeros(TitulosCARTORIO.AsString,'D',2)+'|'+
                    FormatDateTime('dd/mm/yyyy',TitulosDT_PROTOCOLO.AsDateTime)+'|'+
                    GR.Zeros(TitulosPROTOCOLO.AsString,'D',20)+'|'+
                    TitulosRETORNO.AsString);

        Titulos.Next;
      end;

      M.Lines.SaveToFile(pDiretorio+'\'+FormatDateTime('DDMMYYYY_hhmmsszzz',Now)+'.txt');
  end;
  Titulos.Filtered:=False;

  GR.Aviso('ARQUIVO GERADO COM SUCESSO!');

  if P<>Nil then
    if Titulos.BookmarkValid(P) then
      Titulos.GotoBookmark(P);
  Titulos.EnableControls;
end;

procedure TFExportarTxt.btExportarClick(Sender: TObject);
begin
  try
    if Vazio then
      Exit;

    if Titulos.IsEmpty then
      Exit;

    {if RXOutrosID_PORTADOR.AsInteger = -1 then
    begin
      if dm.PortadoresDuplicados(False) then
      begin
        if GR.Pergunta('N�O FOI POSS�VEL GERAR O ARQUIVO!'+#13#13+
                       'EXISTEM PORTADORES CADASTRADOS EM DUPLICIDADE NO SISTEMA.'+#13#13+
                       'DESEJA VISUALIZAR O RELAT�RIO') then
          GR.Imprimir(dm.RXDuplicados,'Rela��o de Portadores em Duplicidade',8,True);

        Exit;
      end;
    end;}

    if dm.RemessasNUMERO.AsInteger<1 then
    begin
      GR.Aviso('O N�MERO DE REMESSAS N�O PODE SER INFERIOR A 1!'+#13#13+'ARQUIVO N�O GERADO!!');
      Exit;
    end;

    if dm.RemessasNUMERO.AsInteger>9 then
    begin
      GR.Aviso('O N�MERO DE REMESSAS N�O PODE SER SUPERIOR A 9!'+#13#13+'ARQUIVO N�O GERADO!!');
      Exit;
    end;

    if GR.Pergunta(edRemessa.Text+'� REMESSA'+#13+#13+'Data: '+PF.FormatarData(edData.Date,'N')+#13#13+'CONFIRMA') then
    begin
      PF.Aguarde(True);
      Arquivo:=IfThen(lkOutros.Text='','R000','R'+GR.Zeros(RXOutrosCODIGO.AsString,'D',3))+FormatDateTime('ddmm.yy',edData.Date)+edRemessa.Text;

      GerarTxt;

      dm.VerificarExistenciaPastasSistema;

      DecodeDate(Date, Ano, Mes, Dia);

      M.Lines.SaveToFile(dm.ValorParametro(4) +
                         IntToStr(Ano) + '\' +
                         GR.Zeros(IntToStr(Mes), 'D', 2) + '\' +
                         GR.Zeros(IntToStr(Dia), 'D', 2) + '\' +
                         Arquivo);


      M.Lines.Clear;

      {CONFIRMANDO A REMESSA}
      dm.Remessas.Edit;
      dm.RemessasNUMERO.AsInteger:=dm.RemessasNUMERO.AsInteger+1;
      dm.Remessas.Post;
      dm.Remessas.ApplyUpdates(0);
      PF.Aguarde(False);
      GR.Aviso('REMESSA DE RETORNO GERADA COM SUCESSO!');
    end;

    Titulos.Close;
  except
    on E: Exception do
    GR.Aviso('N�O FOI POSS�VEL GERAR A REMESSA DE RETORNO.'+#13#13+'Erro: '+E.Message);
  end;
end;

procedure TFExportarTxt.edRemessaKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8,#13]) then key:=#0;
end;

procedure TFExportarTxt.TitulosCalcFields(DataSet: TDataSet);
begin
  if CalcFields1 then
  if TitulosEXPORTADO.AsString='N' then TitulosCheck.AsString:='S';

  TitulosxDIA28.AsBoolean             :=GR.iif(TitulosDT_PROTOCOLO.AsDateTime>=StrToDate('28/11/2019'),True,False);
end;

procedure TFExportarTxt.G1ColEnter(Sender: TObject);
begin
  CalcFields1:=False;
end;

procedure TFExportarTxt.MarcarTodos1Click(Sender: TObject);
begin
  Posicao := Titulos.RecNo;
  Titulos.DisableControls;
  Titulos.First;

  while not Titulos.Eof do
  begin
    Titulos.Edit;
    TitulosCheck.AsString := 'S';
    Titulos.Post;
    Titulos.Next;
  end;

  Titulos.RecNo := Posicao;
  Titulos.EnableControls;
end;

procedure TFExportarTxt.DesmarcarTodos1Click(Sender: TObject);
begin
  Posicao:=Titulos.RecNo;
  Titulos.DisableControls;
  Titulos.First;
  while not Titulos.Eof do
  begin
      Titulos.Edit;
      TitulosCheck.AsString:='N';
      Titulos.Post;
      Titulos.Next;
  end;
  Titulos.RecNo:=Posicao;
  Titulos.EnableControls;
end;

procedure TFExportarTxt.edRemessaExit(Sender: TObject);
begin
  if dm.Remessas.State=dsEdit then
  begin
      dm.Remessas.Post;
      dm.Remessas.ApplyUpdates(0);
  end;
end;

function TFExportarTxt.Marcados: Integer;
begin
  Posicao:=Titulos.RecNo;
  Titulos.DisableControls;
  Titulos.First;
  Result:=0;
  while not Titulos.Eof do
  begin
      if TitulosCheck.AsString='S' then
      Result:=Result+1;
      Titulos.Next;
  end;
  Titulos.RecNo:=Posicao;
  Titulos.EnableControls;
end;

procedure TFExportarTxt.btRejeitarClick(Sender: TObject);
begin
  dm.vOkGeral:=False;
  GR.CriarForm(TFIrregularidades,FIrregularidades);

  if dm.vOkGeral then
  begin
      Titulos.Edit;
      TitulosIRREGULARIDADE.AsInteger:=dm.IrregularidadesCODIGO.AsInteger;
      Titulos.Post;
      Titulos.ApplyUpdates(0);
  end;
  dm.Irregularidades.Close;
end;

function TFExportarTxt.PagosMarcados(IdAto,IdPortador: Integer): Integer;
begin
  Result:=0;
  Titulos.First;
  while not Titulos.Eof do
  begin
      if (TitulosCheck.AsString='S') and (TitulosSTATUS.AsString='PAGO') and (TitulosID_PORTADOR.AsInteger=IdPortador) then
        Result:=Result+1;
      Titulos.Next;
  end;
  Titulos.Locate('ID_ATO',IdAto,[]);
end;

procedure TFExportarTxt.Imprimir1Click(Sender: TObject);
begin
  lbCartorio.Caption:=dm.ServentiaDESCRICAO.AsString;
  lbCidade.Caption  :=dm.ServentiaCIDADE.AsString+' - '+dm.ServentiaUF.AsString;
  lbPeriodo.Caption :='Data: '+edData.Text+' - Remessa: '+edRemessa.Text;
  qkRelatorio.Preview;
end;

procedure TFExportarTxt.TitulosAfterPost(DataSet: TDataSet);
begin
  Titulos.ApplyUpdates(0);
end;

procedure TFExportarTxt.G1KeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
    if not Titulos.IsEmpty then
      Titulos.ApplyUpdates(0);
end;

procedure TFExportarTxt.edDataChange(Sender: TObject);
begin
  dm.Remessas.Close;
  dm.Remessas.Params[0].AsDate:=edData.Date;
  dm.Remessas.Params[1].AsInteger:=-1;
  dm.Remessas.Open;
  if dm.Remessas.IsEmpty then
  begin
      dm.Remessas.Append;
      dm.RemessasID_REMESSA.AsInteger   :=dm.IdAtual('ID_REMESSA','S');
      dm.RemessasID_PORTADOR.AsInteger  :=-1;
      dm.RemessasDATA.AsDateTime        :=edData.Date;
      dm.RemessasNUMERO.AsInteger       :=1;
      dm.Remessas.Post;
      dm.Remessas.ApplyUpdates(0);
  end;
end;

procedure TFExportarTxt.PMPopup(Sender: TObject);
begin
  MenuPortador.Caption:='Selecionar Todos '+TitulosAPRESENTANTE.AsString;
end;

procedure TFExportarTxt.MenuItem1Click(Sender: TObject);
begin
  dm.VerificarExistenciaPastasSistema;

  DecodeDate(Date, Ano, Mes, Dia);

  vDiretorio:=dm.ValorParametro(4) +
              IntToStr(Ano) + '\' +
              GR.Zeros(IntToStr(Mes), 'D', 2) + '\' +
              GR.Zeros(IntToStr(Dia), 'D', 2) + '\RETORNO_INTRUMENTO';

  if not DirectoryExists(vDiretorio) then
    ForceDirectories(vDiretorio);

  CriarArquivoRetornoCartorio(vDiretorio,Arquivo);
end;

procedure TFExportarTxt.MenuPortadorClick(Sender: TObject);
begin
  if Titulos.IsEmpty then Exit;
  
  vApresentante:=TitulosAPRESENTANTE.AsString;

  Posicao:=Titulos.RecNo;
  Titulos.DisableControls;
  Titulos.First;
  while not Titulos.Eof do
  begin
      if TitulosAPRESENTANTE.AsString=vApresentante then
      begin
          Titulos.Edit;
          TitulosCheck.AsString:='S';
          Titulos.Post;
      end;
      Titulos.Next;
  end;
  Titulos.RecNo:=Posicao;
  Titulos.EnableControls;
end;

procedure TFExportarTxt.edInicioKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
    edFim.SetFocus;
end;

procedure TFExportarTxt.edFimKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
    lcbServentiaAgregada.SetFocus;
end;

procedure TFExportarTxt.G1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if X in [0..40] then
    Screen.Cursor:=crHandPoint
      else Screen.Cursor:=crDefault;
end;

procedure TFExportarTxt.P1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  Screen.Cursor:=crDefault;
end;

procedure TFExportarTxt.BarraMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  Screen.Cursor:=crDefault;
end;

procedure TFExportarTxt.FormCreate(Sender: TObject);
begin
  RXOutros.Close;
  RXOutros.Open;
  RXOutros.Append;
  RXOutrosID_PORTADOR.AsInteger :=-1;
  RXOutrosNOME.AsString         :='ARQUIVO �NICO - CRA';
  RXOutros.Post;

  dm.ServentiaAgregada.Close;
  dm.ServentiaAgregada.Open;

  qryOutros.Close;
  qryOutros.Open;

  while not qryOutros.Eof do
  begin
    RXOutros.AppendRecord([qryOutrosID_PORTADOR.AsInteger,qryOutrosNOME.AsString,qryOutrosCODIGO.AsString]);
    qryOutros.Next;
  end;

  RXOutros.First;

  lkOutros.KeyValue:=-1;
end;

procedure TFExportarTxt.lcbServentiaAgregadaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then
    btVisualizar.SetFocus;
end;

procedure TFExportarTxt.lkOutrosClick(Sender: TObject);
begin
  dm.Remessas.Close;
  dm.Remessas.Params[0].AsDate:=edData.Date;
  dm.Remessas.Params[1].AsInteger:=RXOutrosID_PORTADOR.AsInteger;
  dm.Remessas.Open;
  if dm.Remessas.IsEmpty then
  begin
      dm.Remessas.Append;
      dm.RemessasID_REMESSA.AsInteger   :=dm.IdAtual('ID_REMESSA','S');
      dm.RemessasID_PORTADOR.AsInteger  :=RXOutrosID_PORTADOR.AsInteger;
      dm.RemessasDATA.AsDateTime        :=edData.Date;
      dm.RemessasNUMERO.AsInteger       :=1;
      dm.Remessas.Post;
      dm.Remessas.ApplyUpdates(0);
  end;
end;

end.
