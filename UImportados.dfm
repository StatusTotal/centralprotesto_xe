object FImportados: TFImportados
  Left = 335
  Top = 198
  BorderStyle = bsDialog
  Caption = 'Importados'
  ClientHeight = 242
  ClientWidth = 521
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 14
  object P: TsPanel
    Left = 0
    Top = 0
    Width = 521
    Height = 242
    Align = alClient
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object Grid: TwwDBGrid
      Left = 8
      Top = 8
      Width = 505
      Height = 193
      Selected.Strings = (
        'ARQUIVO'#9'15'#9'Arquivo'#9'F'
        'DATA'#9'11'#9'Data'#9'F'
        'TITULOS'#9'11'#9'T'#237'tulos'#9'F'
        'ACEITOS'#9'13'#9'Aceitos'#9'F'
        'REJEITADOS'#9'13'#9'Rejeitados'#9'F')
      IniAttributes.Delimiter = ';;'
      IniAttributes.UnicodeIniFile = False
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      DataSource = dsImportados
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgProportionalColResize]
      ReadOnly = True
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      TitleLines = 1
      TitleButtons = False
      UseTFields = False
      OnKeyDown = GridKeyDown
    end
    object edLocalizar: TsEdit
      Left = 56
      Top = 210
      Width = 145
      Height = 22
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = 4473924
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnChange = edLocalizarChange
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Localizar'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 5059883
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = 'Tahoma'
      BoundLabel.Font.Style = []
    end
    object btSair: TsBitBtn
      Left = 437
      Top = 208
      Width = 75
      Height = 25
      Cursor = crHandPoint
      Caption = 'Sair'
      TabOrder = 2
      OnClick = btSairClick
      ImageIndex = 7
      Images = dm.Imagens
      SkinData.SkinSection = 'BUTTON'
    end
    object sDBNavigator1: TsDBNavigator
      Left = 208
      Top = 208
      Width = 113
      Cursor = crHandPoint
      FullRepaint = False
      TabOrder = 3
      SkinData.SkinSection = 'TOOLBUTTON'
      DataSource = dsImportados
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
    end
  end
  object dsImportados: TDataSource
    DataSet = Importados
    Left = 424
    Top = 120
  end
  object dspImportados: TDataSetProvider
    DataSet = qryImportados
    Left = 280
    Top = 120
  end
  object Importados: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspImportados'
    Left = 352
    Top = 120
    object ImportadosID_IMPORTADO: TIntegerField
      Alignment = taCenter
      FieldName = 'ID_IMPORTADO'
      Required = True
    end
    object ImportadosARQUIVO: TStringField
      Alignment = taCenter
      FieldName = 'ARQUIVO'
      Size = 15
    end
    object ImportadosDATA: TDateField
      Alignment = taCenter
      FieldName = 'DATA'
    end
    object ImportadosTITULOS: TIntegerField
      Alignment = taCenter
      FieldName = 'TITULOS'
    end
    object ImportadosACEITOS: TIntegerField
      Alignment = taCenter
      FieldName = 'ACEITOS'
    end
    object ImportadosREJEITADOS: TIntegerField
      Alignment = taCenter
      FieldName = 'REJEITADOS'
    end
  end
  object qrySelos_CCT: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select SELO_REGISTRO, SELO_PAGAMENTO, CCT from TITULOS'
      ''
      'where ARQUIVO=:ARQUIVO')
    Left = 56
    Top = 56
    ParamData = <
      item
        Name = 'ARQUIVO'
        DataType = ftString
        ParamType = ptInput
      end>
    object qrySelos_CCTSELO_REGISTRO: TStringField
      FieldName = 'SELO_REGISTRO'
      Origin = 'SELO_REGISTRO'
      Size = 9
    end
    object qrySelos_CCTSELO_PAGAMENTO: TStringField
      FieldName = 'SELO_PAGAMENTO'
      Origin = 'SELO_PAGAMENTO'
      Size = 9
    end
    object qrySelos_CCTCCT: TStringField
      FieldName = 'CCT'
      Origin = 'CCT'
      Size = 9
    end
  end
  object qryImportados: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM IMPORTADOS'
      ' WHERE SUBSTRING(ARQUIVO FROM 1 FOR 1) = '#39'B'#39
      'ORDER BY DATA ASC')
    Left = 200
    Top = 120
  end
end
