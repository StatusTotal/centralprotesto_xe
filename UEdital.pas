unit UEdital;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, Mask, sMaskEdit, sCustomComboEdit,
  sTooledit, ExtCtrls, sPanel, SQLExpr, DBCtrls, sDBLookupComboBox, DB,
  sGroupBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFEdital = class(TForm)
    P1: TsPanel;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    edPublicacao: TsDateEdit;
    btVisualizar: TsBitBtn;
    btSair: TsBitBtn;
    dsEscreventes: TDataSource;
    lkEscrevente: TsDBLookupComboBox;
    RGTipo: TsRadioGroup;
    procedure btVisualizarClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEdital: TFEdital;

implementation

uses UDM, UPF, UQuickEdital1, UGeral;

{$R *.dfm}

procedure TFEdital.btVisualizarClick(Sender: TObject);
var
  Q: TFDQuery;
  Data: String;
begin
  if lkEscrevente.KeyValue=Null then
  begin
      GR.Aviso('Informe o funcion�rio.');
      lkEscrevente.SetFocus;
      Exit;
  end;

  if RGTipo.ItemIndex=0 then
    Data:='DT_PROTOCOLO'
      else Data:='DT_INTIMACAO';

  PF.Aguarde(True);
  Q:=TFDQuery.Create(Nil);
  Q.Connection:=dm.conSISTEMA;
  Q.SQL.Add('SELECT ID_ATO,DEVEDOR,APRESENTANTE,NUMERO_TITULO,PROTOCOLO,DT_PROTOCOLO,DT_TITULO,DT_VENCIMENTO,DT_PRAZO,TIPO_DEVEDOR,MOTIVO_INTIMACAO,'+
            'VALOR_TITULO,TOTAL,SALDO_PROTESTO,TIPO_TITULO,CPF_CNPJ_DEVEDOR FROM TITULOS WHERE ('+Data+' BETWEEN :D1 AND :D2) AND (DT_PUBLICACAO=:D3)'+
            'AND (TIPO_INTIMACAO='''+'E'+''') ORDER BY PROTOCOLO');
  Q.ParamByName('D1').AsDate:=edInicio.Date;
  Q.ParamByName('D2').AsDate:=edFim.Date;
  Q.ParamByName('D3').AsDate:=edPublicacao.Date;
  Q.Open;
  if not Q.IsEmpty then
  begin
      Application.CreateForm(TFQuickEdital1,FQuickEdital1);
      with FQuickEdital1 do
      begin
          RX.Close;
          RX.Open;
          Q.First;
          while not Q.Eof do
          begin
              PF.CarregarDevedor(Q.FieldByName('ID_ATO').AsInteger);
              while not dm.RxDevedor.Eof do
              begin
                  RX.Append;
                  RXTitulo.AsString         :=Q.FieldByName('NUMERO_TITULO').AsString;
                  RXApresentante.AsString   :=Q.FieldByName('APRESENTANTE').AsString;
                  RXDevedor.AsString        :=Q.FieldByName('DEVEDOR').AsString;
                  RXValor.AsFloat           :=Q.FieldByName('VALOR_TITULO').AsFloat;

                  if Q.FieldByName('DT_VENCIMENTO').AsDateTime<>0 then
                  RXVencimento.AsDateTime   :=Q.FieldByName('DT_VENCIMENTO').AsDateTime;

                  RXProtocolo.AsString      :=Q.FieldByName('PROTOCOLO').AsString;
                  RXSaldo.AsFloat           :=Q.FieldByName('SALDO_PROTESTO').AsFloat;
                  RXCustas.AsFloat          :=Q.FieldByName('TOTAL').AsFloat;

                  if dm.RxDevedorDOCUMENTO.AsString<>'' then
                    if dm.RxDevedorTIPO.AsString='F' then
                      RXDevedor.AsString:=dm.RxDevedorNOME.AsString+'   CPF: '+PF.FormatarCPF(dm.RxDevedorDOCUMENTO.AsString)
                        else
                          if Q.FieldByName('TIPO_DEVEDOR').AsString='J' then
                            RXDevedor.AsString:=dm.RxDevedorNOME.AsString+'   CNPJ: '+PF.FormatarCNPJ(dm.RxDevedorDOCUMENTO.AsString);
                  {if Q.FieldByName('CPF_CNPJ_DEVEDOR').AsString<>'' then
                    if Q.FieldByName('TIPO_DEVEDOR').AsString='F' then
                      RXDevedor.AsString:=RXDevedor.AsString+'   CPF: '+PF.FormatarCPF(Q.FieldByName('CPF_CNPJ_DEVEDOR').AsString)
                        else
                          if Q.FieldByName('TIPO_DEVEDOR').AsString='J' then
                            RXDevedor.AsString:=RXDevedor.AsString+'   CNPJ: '+PF.FormatarCNPJ(Q.FieldByName('CPF_CNPJ_DEVEDOR').AsString); }

                  //PF.CarregarDevedor(Q.FieldByName('ID_ATO').AsInteger);
                  RXEndereco.AsString       :=dm.RxDevedorENDERECO.AsString;
                  RXDt_Titulo.AsDateTime    :=Q.FieldByName('DT_TITULO').AsDateTime;
                  RXDt_Protocolo.AsDateTime :=Q.FieldByName('DT_PROTOCOLO').AsDateTime;
                  RXDt_Prazo.AsDateTime     :=Q.FieldByName('DT_PRAZO').AsDateTime;
                  RXEspecie.AsString        :=PF.RetornarTitulo(Q.FieldByName('TIPO_TITULO').AsInteger);
                  RXMotivo.AsString         :=Q.FieldByName('MOTIVO_INTIMACAO').AsString;
                  RX.Post;
                  dm.RxDevedor.Next;
              end;
              Q.Next;
          end;
          Q.Close;
          Q.Free;
          lbCidade.Caption      :=dm.ServentiaCIDADE.AsString+', '+FormatDateTime('dd "de" mmmm "de" yyyy.',edPublicacao.Date);
          lbFuncionario.Caption :=dm.EscreventesNOME.AsString+#13+dm.EscreventesQUALIFICACAO.AsString;
          Edital1.Preview;
          Free;
      end;
  end
  else
  begin
      PF.Aguarde(False);
      GR.Aviso('Nenhuma intima��o encontrada no per�odo informado.');
  end;
end;

procedure TFEdital.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFEdital.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
end;

procedure TFEdital.FormCreate(Sender: TObject);
begin
  dm.Escreventes.Close;
  dm.Escreventes.Open;
end;

end.
