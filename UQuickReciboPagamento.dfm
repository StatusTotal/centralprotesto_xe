object FQuickReciboPagamento: TFQuickReciboPagamento
  Left = 195
  Top = 131
  Caption = 'Recibo de Pagamento'
  ClientHeight = 620
  ClientWidth = 959
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Desistencia: TQuickRep
    Left = 120
    Top = 50
    Width = 794
    Height = 1123
    ShowingPreview = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Continuous = False
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      150.000000000000000000
      2100.000000000000000000
      150.000000000000000000
      150.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = False
    PrinterSettings.CustomBinCode = 0
    PrinterSettings.ExtendedDuplex = 0
    PrinterSettings.UseCustomPaperCode = False
    PrinterSettings.CustomPaperCode = 0
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.MemoryLimit = 1000000
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 0
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 100
    PrevFormStyle = fsNormal
    PreviewInitialState = wsMaximized
    PreviewWidth = 500
    PreviewHeight = 500
    PrevInitialZoom = qrZoomToWidth
    PreviewDefaultSaveType = stQRP
    PreviewLeft = 0
    PreviewTop = 0
    object QRBand1: TQRBand
      Left = 57
      Top = 57
      Width = 681
      Height = 315
      Frame.DrawTop = True
      Frame.DrawLeft = True
      Frame.DrawRight = True
      AlignToBottom = False
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        833.437500000000000000
        1801.812500000000000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbTitle
      object lbCidade: TQRLabel
        Left = 301
        Top = 39
        Width = 78
        Height = 17
        Size.Values = (
          44.979166666666670000
          796.395833333333300000
          103.187500000000000000
          206.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'Cidade - Estado'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = [fsItalic]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object lbTitular: TQRLabel
        Left = 300
        Top = 54
        Width = 80
        Height = 17
        Size.Values = (
          44.979166666666670000
          793.750000000000000000
          142.875000000000000000
          211.666666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'Nome do Titular'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = [fsItalic]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object lbCartorio: TQRLabel
        Left = 256
        Top = 10
        Width = 169
        Height = 20
        Size.Values = (
          52.916666666666670000
          677.333333333333300000
          26.458333333333330000
          447.145833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'NOME DO CART'#211'RIO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 12
      end
      object QRLabel2: TQRLabel
        Left = 195
        Top = 78
        Width = 291
        Height = 20
        Size.Values = (
          52.916666666666670000
          515.937500000000000000
          206.375000000000000000
          769.937500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        Caption = 'RECIBO DE PAGAMENTO DE T'#205'TULO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 12
      end
      object QRRichText1: TQRRichText
        Left = 8
        Top = 98
        Width = 665
        Height = 39
        Size.Values = (
          103.187500000000000000
          21.166666666666670000
          259.291666666666700000
          1759.479166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AutoStretch = False
        Color = clWindow
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        HiresExport = False
        Transparent = False
        YIncrement = 50
        ParentRichEdit = RE
      end
      object qrProtocolo: TQRLabel
        Left = 49
        Top = 160
        Width = 49
        Height = 17
        Size.Values = (
          44.979166666666670000
          129.645833333333300000
          423.333333333333300000
          129.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Protocolo:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel6: TQRLabel
        Left = 40
        Top = 194
        Width = 58
        Height = 17
        Size.Values = (
          44.979166666666670000
          105.833333333333300000
          513.291666666666700000
          153.458333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Documento:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel7: TQRLabel
        Left = 69
        Top = 211
        Width = 29
        Height = 17
        Size.Values = (
          44.979166666666670000
          182.562500000000000000
          558.270833333333300000
          76.729166666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'T'#237'tulo:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel11: TQRLabel
        Left = 53
        Top = 279
        Width = 45
        Height = 17
        Size.Values = (
          44.979166666666670000
          140.229166666666700000
          738.187500000000000000
          119.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Sacador:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel10: TQRLabel
        Left = 53
        Top = 177
        Width = 45
        Height = 17
        Size.Values = (
          44.979166666666670000
          140.229166666666700000
          468.312500000000000000
          119.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Devedor:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel33: TQRLabel
        Left = 54
        Top = 296
        Width = 44
        Height = 17
        Size.Values = (
          44.979166666666670000
          142.875000000000000000
          783.166666666666700000
          116.416666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Cedente:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbProtocolo: TQRLabel
        Left = 105
        Top = 160
        Width = 49
        Height = 17
        Size.Values = (
          44.979166666666670000
          277.812500000000000000
          423.333333333333300000
          129.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Protocolo:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbDevedor: TQRLabel
        Left = 105
        Top = 177
        Width = 42
        Height = 17
        Size.Values = (
          44.979166666666670000
          277.812500000000000000
          468.312500000000000000
          111.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Devedor'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbDocumento: TQRLabel
        Left = 105
        Top = 194
        Width = 55
        Height = 17
        Size.Values = (
          44.979166666666670000
          277.812500000000000000
          513.291666666666700000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Documento'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbTitulo: TQRLabel
        Left = 105
        Top = 211
        Width = 26
        Height = 17
        Size.Values = (
          44.979166666666670000
          277.812500000000000000
          558.270833333333300000
          68.791666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'T'#237'tulo'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbSacador: TQRLabel
        Left = 105
        Top = 279
        Width = 42
        Height = 17
        Size.Values = (
          44.979166666666670000
          277.812500000000000000
          738.187500000000000000
          111.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Sacador'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbCedente: TQRLabel
        Left = 105
        Top = 296
        Width = 41
        Height = 17
        Size.Values = (
          44.979166666666670000
          277.812500000000000000
          783.166666666666700000
          108.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Cedente'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel5: TQRLabel
        Left = 68
        Top = 228
        Width = 30
        Height = 17
        Size.Values = (
          44.979166666666670000
          179.916666666666700000
          603.250000000000000000
          79.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Valor:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbValor: TQRLabel
        Left = 105
        Top = 228
        Width = 22
        Height = 17
        Size.Values = (
          44.979166666666670000
          277.812500000000000000
          603.250000000000000000
          58.208333333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = '0.00'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel12: TQRLabel
        Left = 37
        Top = 245
        Width = 61
        Height = 17
        Size.Values = (
          44.979166666666670000
          97.895833333333330000
          648.229166666666700000
          161.395833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Vencimento:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbVencimento: TQRLabel
        Left = 105
        Top = 245
        Width = 69
        Height = 17
        Size.Values = (
          44.979166666666670000
          277.812500000000000000
          648.229166666666700000
          182.562500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'DD/MM/YYYY'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel19: TQRLabel
        Left = 53
        Top = 262
        Width = 45
        Height = 17
        Size.Values = (
          44.979166666666670000
          140.229166666666700000
          693.208333333333300000
          119.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'Portador:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbApresentante: TQRLabel
        Left = 105
        Top = 262
        Width = 42
        Height = 17
        Size.Values = (
          44.979166666666670000
          277.812500000000000000
          693.208333333333300000
          111.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Portador'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object lbRecibo: TQRLabel
        Left = 105
        Top = 143
        Width = 31
        Height = 17
        Size.Values = (
          44.979166666666670000
          277.812500000000000000
          378.354166666666700000
          82.020833333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = '00000'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrRecibo: TQRLabel
        Left = 43
        Top = 143
        Width = 55
        Height = 17
        Size.Values = (
          44.979166666666670000
          113.770833333333300000
          378.354166666666700000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        Caption = 'N'#186' Recibo:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    object ChildBand1: TQRChildBand
      Left = 57
      Top = 372
      Width = 681
      Height = 193
      Frame.DrawBottom = True
      Frame.DrawLeft = True
      Frame.DrawRight = True
      AlignToBottom = False
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        510.645833333333300000
        1801.812500000000000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      ParentBand = QRBand1
      PrintOrder = cboAfterParent
      object lbData: TQRLabel
        Left = 102
        Top = 16
        Width = 478
        Height = 17
        Size.Values = (
          44.979166666666670000
          269.875000000000000000
          42.333333333333340000
          1264.708333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Cidade, Data'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRShape2: TQRShape
        Left = 172
        Top = 64
        Width = 338
        Height = 1
        Size.Values = (
          2.645833333333333000
          455.083333333333300000
          169.333333333333300000
          894.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = clBlack
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel3: TQRLabel
        Left = 66
        Top = 108
        Width = 548
        Height = 31
        Size.Values = (
          82.020833333333330000
          174.625000000000000000
          285.750000000000000000
          1449.916666666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = True
        AutoSize = False
        AutoStretch = True
        Caption = 
          'Este Recibo '#233' gerado de forma eletr'#244'nica, qualquer forma de rasu' +
          'ra ou complementa'#231#227'o a tornar'#225' como inv'#225'lida, ou como princ'#237'pio ' +
          'de fraude.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object M: TQRMemo
        Left = 8
        Top = 170
        Width = 664
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          449.791666666666700000
          1756.833333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = True
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 6
      end
      object QRLabel1: TQRLabel
        Left = 568
        Top = 156
        Width = 104
        Height = 13
        Size.Values = (
          34.395833333333340000
          1502.833333333333000000
          412.750000000000100000
          275.166666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Taxas e Emolumentos:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
    end
  end
  object RE: TRichEdit
    Left = 8
    Top = 8
    Width = 665
    Height = 25
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Zoom = 100
  end
  object Protocolo: TFDQuery
    Connection = dm.conSISTEMA
    SQL.Strings = (
      'select '
      ''
      'ID_ATO,'
      'PROTOCOLO,'
      'TIPO_DEVEDOR,'
      'DEVEDOR,'
      'CPF_CNPJ_DEVEDOR,'
      'NUMERO_TITULO,'
      'SACADOR,'
      'CEDENTE,'
      'VALOR_TITULO,'
      'DT_VENCIMENTO,'
      'APRESENTANTE,'
      'RECIBO_PAGAMENTO,'
      'EMOLUMENTOS,'
      'FETJ,'
      'FUNDPERJ,'
      'FUNPERJ,'
      'FUNARPEN,'
      'PMCMV,'
      'ISS,'
      'MUTUA,'
      'ACOTERJ,'
      'TARIFA_BANCARIA,'
      'TOTAL'
      ''
      'from titulos '
      ''
      'where PROTOCOLO=:PROTOCOLO and '
      ''
      'STATUS='#39'PAGO'#39' and CONVENIO='#39'S'#39
      ''
      'ORDER BY DT_PAGAMENTO')
    Left = 40
    Top = 56
    ParamData = <
      item
        Name = 'PROTOCOLO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object ProtocoloID_ATO: TIntegerField
      FieldName = 'ID_ATO'
      Origin = 'ID_ATO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ProtocoloPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Origin = 'PROTOCOLO'
    end
    object ProtocoloTIPO_DEVEDOR: TStringField
      FieldName = 'TIPO_DEVEDOR'
      Origin = 'TIPO_DEVEDOR'
      FixedChar = True
      Size = 1
    end
    object ProtocoloDEVEDOR: TStringField
      FieldName = 'DEVEDOR'
      Origin = 'DEVEDOR'
      Size = 100
    end
    object ProtocoloCPF_CNPJ_DEVEDOR: TStringField
      FieldName = 'CPF_CNPJ_DEVEDOR'
      Origin = 'CPF_CNPJ_DEVEDOR'
      Size = 15
    end
    object ProtocoloNUMERO_TITULO: TStringField
      FieldName = 'NUMERO_TITULO'
      Origin = 'NUMERO_TITULO'
    end
    object ProtocoloSACADOR: TStringField
      FieldName = 'SACADOR'
      Origin = 'SACADOR'
      Size = 100
    end
    object ProtocoloCEDENTE: TStringField
      FieldName = 'CEDENTE'
      Origin = 'CEDENTE'
      Size = 100
    end
    object ProtocoloVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
      Origin = 'VALOR_TITULO'
    end
    object ProtocoloDT_VENCIMENTO: TDateField
      FieldName = 'DT_VENCIMENTO'
      Origin = 'DT_VENCIMENTO'
    end
    object ProtocoloAPRESENTANTE: TStringField
      FieldName = 'APRESENTANTE'
      Origin = 'APRESENTANTE'
      Size = 100
    end
    object ProtocoloRECIBO_PAGAMENTO: TIntegerField
      FieldName = 'RECIBO_PAGAMENTO'
      Origin = 'RECIBO_PAGAMENTO'
    end
    object ProtocoloEMOLUMENTOS: TFloatField
      FieldName = 'EMOLUMENTOS'
      Origin = 'EMOLUMENTOS'
    end
    object ProtocoloFETJ: TFloatField
      FieldName = 'FETJ'
      Origin = 'FETJ'
    end
    object ProtocoloFUNDPERJ: TFloatField
      FieldName = 'FUNDPERJ'
      Origin = 'FUNDPERJ'
    end
    object ProtocoloFUNPERJ: TFloatField
      FieldName = 'FUNPERJ'
      Origin = 'FUNPERJ'
    end
    object ProtocoloFUNARPEN: TFloatField
      FieldName = 'FUNARPEN'
      Origin = 'FUNARPEN'
    end
    object ProtocoloPMCMV: TFloatField
      FieldName = 'PMCMV'
      Origin = 'PMCMV'
    end
    object ProtocoloISS: TFloatField
      FieldName = 'ISS'
      Origin = 'ISS'
    end
    object ProtocoloMUTUA: TFloatField
      FieldName = 'MUTUA'
      Origin = 'MUTUA'
    end
    object ProtocoloACOTERJ: TFloatField
      FieldName = 'ACOTERJ'
      Origin = 'ACOTERJ'
    end
    object ProtocoloTARIFA_BANCARIA: TFloatField
      FieldName = 'TARIFA_BANCARIA'
      Origin = 'TARIFA_BANCARIA'
    end
    object ProtocoloTOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
    end
  end
end
