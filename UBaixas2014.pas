unit UBaixas2014;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FMTBcd, DB, StdCtrls, sEdit, sGroupBox, Buttons, sBitBtn, Grids,
  Wwdbigrd, Wwdbgrid, ExtCtrls, sPanel, SqlExpr, Provider, DBClient, Menus,
  sRadioButton, sLabel, sCheckBox, Mask, sMaskEdit, sCustomComboEdit, DateUtils,
  sTooledit, jpeg, sDialogs, sMemo, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFBaixas2014 = class(TForm)
    dsConsulta: TDataSource;
    Consulta: TClientDataSet;
    dspConsulta: TDataSetProvider;
    P1: TsPanel;
    Grid2: TwwDBGrid;
    GbProtocolo: TsGroupBox;
    edProtFinal: TsEdit;
    edProtInicial: TsEdit;
    ConsultaCheck: TBooleanField;
    ConsultaID_ATO: TIntegerField;
    ConsultaRECIBO: TIntegerField;
    ConsultaDT_PROTOCOLO: TDateField;
    ConsultaPROTOCOLO: TIntegerField;
    ConsultaDT_REGISTRO: TDateField;
    ConsultaREGISTRO: TIntegerField;
    ConsultaLIVRO_REGISTRO: TIntegerField;
    ConsultaFOLHA_REGISTRO: TStringField;
    ConsultaSELO_REGISTRO: TStringField;
    ConsultaDT_PAGAMENTO: TDateField;
    ConsultaSELO_PAGAMENTO: TStringField;
    ConsultaDT_ENVIO: TDateField;
    ConsultaMOTIVO_INTIMACAO: TMemoField;
    ConsultaDT_INTIMACAO: TDateField;
    ConsultaVALOR_PAGAMENTO: TFloatField;
    ConsultaDT_SUSTADO: TDateField;
    ConsultaDT_RETIRADO: TDateField;
    ConsultaAPRESENTANTE: TStringField;
    ConsultaDEVEDOR: TStringField;
    ConsultaSTATUS: TStringField;
    ConsultaPROTESTADO: TStringField;
    dsMovimento: TDataSource;
    ConsultaCPF_ESCREVENTE: TStringField;
    ConsultaDT_PUBLICACAO: TDateField;
    ConsultaCPF_ESCREVENTE_PG: TStringField;
    Grid1: TwwDBGrid;
    btFiltrar: TsBitBtn;
    P3: TsPanel;
    Rb1: TsRadioButton;
    Rb2: TsRadioButton;
    Rb3: TsRadioButton;
    Rb4: TsRadioButton;
    Rb6: TsRadioButton;
    Rb7: TsRadioButton;
    ConsultaTIPO_INTIMACAO: TStringField;
    GbData: TsGroupBox;
    btData: TsBitBtn;
    edInicio: TsDateEdit;
    edFim: TsDateEdit;
    lbTitulo: TsBitBtn;
    PM: TPopupMenu;
    MarcarTodos: TMenuItem;
    Desmarcartodos1: TMenuItem;
    Rb5: TsRadioButton;
    ConsultaRECIBO_PAGAMENTO: TIntegerField;
    ConsultaSALDO_PROTESTO: TFloatField;
    ConsultaTARIFA_BANCARIA: TFloatField;
    ConsultaFORMA_PAGAMENTO: TStringField;
    ConsultaNUMERO_PAGAMENTO: TStringField;
    ConsultaTIPO_DEVEDOR: TStringField;
    ConsultaCPF_CNPJ_DEVEDOR: TStringField;
    PM2: TPopupMenu;
    Excluir1: TMenuItem;
    ConsultaDT_DEVOLVIDO: TDateField;
    ConsultaID_MSG: TIntegerField;
    PA: TsPanel;
    Image1: TImage;
    Memo1: TMemo;
    sGroupBox1: TsGroupBox;
    edNome: TsEdit;
    btNome: TsBitBtn;
    ConsultaVALOR_TITULO: TFloatField;
    ConsultaIRREGULARIDADE: TIntegerField;
    Linha: TMenuItem;
    ImprimirBoletos: TMenuItem;
    ConsultaDT_PRAZO: TDateField;
    ConsultaTOTAL: TFloatField;
    ConsultaTIPO_SUSTACAO: TStringField;
    ConsultaRETORNO_PROTESTO: TStringField;
    ConsultaDT_RETORNO_PROTESTO: TDateField;
    ConsultaDT_DEFINITIVA: TDateField;
    ConsultaJUDICIAL: TStringField;
    ConsultaALEATORIO_PROTESTO: TStringField;
    ConsultaALEATORIO_SOLUCAO: TStringField;
    ConsultaDETERMINACAO: TStringField;
    ConsultaCODIGO: TIntegerField;
    ConsultaCONVENIO: TStringField;
    ConsultaCOBRANCA: TStringField;
    ConsultaEMOLUMENTOS: TFloatField;
    ConsultaFETJ: TFloatField;
    ConsultaFUNDPERJ: TFloatField;
    ConsultaFUNPERJ: TFloatField;
    ConsultaFUNARPEN: TFloatField;
    ConsultaPMCMV: TFloatField;
    ConsultaMUTUA: TFloatField;
    ConsultaACOTERJ: TFloatField;
    ConsultaDISTRIBUICAO: TFloatField;
    ConsultaVALOR_AR: TFloatField;
    ConsultaLIVRO_PROTOCOLO: TIntegerField;
    ConsultaFOLHA_PROTOCOLO: TStringField;
    Rb8: TsRadioButton;
    ConsultaSALDO_TITULO: TFloatField;
    MNCE: TMenuItem;
    Opd: TsOpenDialog;
    M: TsMemo;
    ConsultaELETRONICO: TStringField;
    ConsultaISS: TFloatField;
    qryConsulta: TFDQuery;
    qryConsultaID_ATO: TIntegerField;
    qryConsultaRECIBO: TIntegerField;
    qryConsultaDT_PROTOCOLO: TDateField;
    qryConsultaPROTOCOLO: TIntegerField;
    qryConsultaDT_REGISTRO: TDateField;
    qryConsultaREGISTRO: TIntegerField;
    qryConsultaLIVRO_REGISTRO: TIntegerField;
    qryConsultaFOLHA_REGISTRO: TStringField;
    qryConsultaSELO_REGISTRO: TStringField;
    qryConsultaDT_PAGAMENTO: TDateField;
    qryConsultaSELO_PAGAMENTO: TStringField;
    qryConsultaRECIBO_PAGAMENTO: TIntegerField;
    qryConsultaDT_ENVIO: TDateField;
    qryConsultaMOTIVO_INTIMACAO: TMemoField;
    qryConsultaTIPO_INTIMACAO: TStringField;
    qryConsultaDT_PUBLICACAO: TDateField;
    qryConsultaDT_INTIMACAO: TDateField;
    qryConsultaVALOR_TITULO: TFloatField;
    qryConsultaVALOR_PAGAMENTO: TFloatField;
    qryConsultaSALDO_TITULO: TFloatField;
    qryConsultaSALDO_PROTESTO: TFloatField;
    qryConsultaDT_SUSTADO: TDateField;
    qryConsultaDT_RETIRADO: TDateField;
    qryConsultaAPRESENTANTE: TStringField;
    qryConsultaDEVEDOR: TStringField;
    qryConsultaCPF_ESCREVENTE: TStringField;
    qryConsultaCPF_ESCREVENTE_PG: TStringField;
    qryConsultaSTATUS: TStringField;
    qryConsultaPROTESTADO: TStringField;
    qryConsultaFORMA_PAGAMENTO: TStringField;
    qryConsultaNUMERO_PAGAMENTO: TStringField;
    qryConsultaTIPO_DEVEDOR: TStringField;
    qryConsultaCPF_CNPJ_DEVEDOR: TStringField;
    qryConsultaDT_DEVOLVIDO: TDateField;
    qryConsultaID_MSG: TIntegerField;
    qryConsultaIRREGULARIDADE: TIntegerField;
    qryConsultaDT_PRAZO: TDateField;
    qryConsultaTIPO_SUSTACAO: TStringField;
    qryConsultaRETORNO_PROTESTO: TStringField;
    qryConsultaDT_RETORNO_PROTESTO: TDateField;
    qryConsultaDT_DEFINITIVA: TDateField;
    qryConsultaJUDICIAL: TStringField;
    qryConsultaALEATORIO_PROTESTO: TStringField;
    qryConsultaALEATORIO_SOLUCAO: TStringField;
    qryConsultaDETERMINACAO: TStringField;
    qryConsultaCODIGO: TIntegerField;
    qryConsultaCONVENIO: TStringField;
    qryConsultaCOBRANCA: TStringField;
    qryConsultaEMOLUMENTOS: TFloatField;
    qryConsultaFETJ: TFloatField;
    qryConsultaFUNDPERJ: TFloatField;
    qryConsultaFUNPERJ: TFloatField;
    qryConsultaFUNARPEN: TFloatField;
    qryConsultaPMCMV: TFloatField;
    qryConsultaISS: TFloatField;
    qryConsultaMUTUA: TFloatField;
    qryConsultaACOTERJ: TFloatField;
    qryConsultaDISTRIBUICAO: TFloatField;
    qryConsultaVALOR_AR: TFloatField;
    qryConsultaTARIFA_BANCARIA: TFloatField;
    qryConsultaTOTAL: TFloatField;
    qryConsultaLIVRO_PROTOCOLO: TIntegerField;
    qryConsultaFOLHA_PROTOCOLO: TStringField;
    qryConsultaELETRONICO: TStringField;
    procedure Apontar;
    procedure Protestar;
    procedure Pagar;
    procedure Intimar;
    procedure Cancelar;
    procedure Desistencia;
    procedure Sustar;
    procedure Devolver;
    procedure btFiltrarClick(Sender: TObject);
    procedure btEnviadoClick(Sender: TObject);
    procedure dsConsultaDataChange(Sender: TObject; Field: TField);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure Rb1Click(Sender: TObject);
    procedure Rb2Click(Sender: TObject);
    procedure Rb3Click(Sender: TObject);
    procedure Rb4Click(Sender: TObject);
    procedure Rb6Click(Sender: TObject);
    procedure Rb7Click(Sender: TObject);
    procedure DesmarcarTodos;
    procedure Limpar;
    procedure Grid2MouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure lbTituloClick(Sender: TObject);
    procedure btDataClick(Sender: TObject);
    procedure Grid2DblClick(Sender: TObject);
    procedure MarcarTodosClick(Sender: TObject);
    procedure Desmarcartodos1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ImprimirIntimacao;
    procedure edInicioExit(Sender: TObject);
    procedure Rb5Click(Sender: TObject);
    procedure ImprimirInstrumento;
    procedure Excluir1Click(Sender: TObject);
    procedure btNomeClick(Sender: TObject);
    procedure Grid2MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure edProtInicialEnter(Sender: TObject);
    procedure ImprimirBoletosClick(Sender: TObject);
    procedure PMPopup(Sender: TObject);
    procedure ConsultaAfterOpen(DataSet: TDataSet);
    procedure Rb8Click(Sender: TObject);
    procedure Grid1DblClick(Sender: TObject);
    procedure MNCEClick(Sender: TObject);

    function Selecionados: Integer;
    function Marcados: Integer;
    function QtdMarcados: Integer;
  private
    { Private declarations }
  public
    { Public declarations }
    vEletronico: Boolean;
    vQuery,vTipo: String;
  end;

var
  FBaixas2014: TFBaixas2014;
  Periodo: Integer;

implementation

uses UDM, UPF, UDadosBaixa, UQuickIntimacao1,
     UQuickInstrumento1, UTitulo, UAR, UIntimacao,
     UCancelado, UQuickCancelado2, UQuickPagamento1, QRCtrls,
     UDevolvido, UQuickRetirado1, UPrincipal,
     UQuickCancelado1, UAssinatura, {UBoleto,} UQuickSustacao1, UGeral,
     UProtestar2014, UGDM, UPago2014, UCancelado2014,
     UDesistencia, USustados2014, Math, StrUtils, UMovimento;

{$R *.dfm}

function TFBaixas2014.Selecionados: Integer;
var
  Posicao: Integer;
begin
  Result:=0;
  Consulta.DisableControls;
  Posicao:=Consulta.RecNo;
  Consulta.First;
  while not Consulta.Eof do
  begin
      if ConsultaCheck.AsBoolean then Result:=Result+1;
      Consulta.Next;
  end;
  Consulta.RecNo:=Posicao;
  Consulta.EnableControls;
end;

procedure TFBaixas2014.btFiltrarClick(Sender: TObject);
var
  vWhere: String;
begin
  if (edProtInicial.Text='') then
  begin
      edProtInicial.SetFocus;
      Exit;
  end;

  if edProtFinal.Text='' then
  edProtFinal.Text:=edProtInicial.Text;

  vEletronico:=False;
  vWhere:=' WHERE ';

  if Rb2.Checked then vWhere:=' WHERE (STATUS='''+'APONTADO'+''' OR STATUS='''+'INTIMADO EDITAL'+''' OR STATUS='''+'INTIMADO PESSOAL'+''' OR STATUS='''+'INTIMADO CARTA'+''') AND ';
  if Rb3.Checked then vWhere:=' WHERE (STATUS='''+'INTIMADO EDITAL'+''' OR STATUS='''+'INTIMADO PESSOAL'+''' OR STATUS='''+'INTIMADO CARTA'+''') AND ';
  if Rb4.Checked then vWhere:=' WHERE (STATUS='''+'APONTADO'+''' OR STATUS='''+'INTIMADO EDITAL'+''' OR STATUS='''+'INTIMADO PESSOAL'+''' OR STATUS='''+'INTIMADO CARTA'+''') AND ';
  if Rb5.Checked then vWhere:=' WHERE (STATUS='''+'PROTESTADO'+''' OR STATUS='''+'SUSTADO'+''') AND ';
  if Rb6.Checked then vWhere:=' WHERE (STATUS='''+'APONTADO'+''' OR STATUS='''+'INTIMADO EDITAL'+''' OR STATUS='''+'INTIMADO PESSOAL'+''' OR STATUS='''+'INTIMADO CARTA'+''') AND ';

  PF.Aguarde(True);
  Consulta.Close;
  Consulta.CommandText:=vQuery+vWhere+'(PROTOCOLO BETWEEN'''+edProtInicial.Text+''' AND '''+edProtFinal.Text+''') ORDER BY PROTOCOLO';
  Consulta.Open;

  Desmarcartodos1Click(Sender);
  PF.Aguarde(False);
end;

procedure TFBaixas2014.btEnviadoClick(Sender: TObject);
var
  Posicao: Integer;
  Msg: String;
begin
  if Marcados=0 then Exit;

  if Marcados>1 then
    Msg:='Confirma envio para a Intima��o dos T�tulos Selecionados'
      else Msg:='Confirma envio para a Intima��o do T�tulo Selecionado';

  if GR.Pergunta(Msg) then
  begin
      dm.vOkGeral:=False;
      Application.CreateForm(TFDadosBaixa,FDadosBaixa);
      with FDadosBaixa do
      begin
          edData.Date               :=Now;
          edData.BoundLabel.Caption :='Data do Envio';
          edData.Hint               :='DATA DO ENVIO DA INTIMA��O';
          edSelo.Enabled            :=False;
          edRegistro.Enabled        :=False;
          edLivro.Enabled           :=False;
          edLetra.Enabled           :=False;
          edFolha.Enabled           :=False;
          edRecibo.Enabled          :=False;
          lkEscreventes.Enabled     :=True;
          ckImprime.Visible         :=True;
          edValor.Enabled           :=False;
          cbTipoIntimacao.Enabled   :=False;
          edDataPublicacao.Enabled  :=False;
          lkMotivo.Enabled          :=False;

          ShowModal;

          if dm.vOkGeral then
          begin
              Consulta.DisableControls;
              Posicao:=Consulta.RecNo;
              Consulta.First;
              while not Consulta.Eof do
              begin
                  if ConsultaCheck.AsBoolean then
                  begin
                      Consulta.Edit;
                      ConsultaDT_ENVIO.AsDateTime     :=edData.Date;
                      ConsultaSTATUS.AsString         :='ENVIAR INTIMA��O';
                      ConsultaPROTESTADO.AsString     :='N';
                      ConsultaVALOR_PAGAMENTO.AsFloat :=0;
                      ConsultaCPF_ESCREVENTE.AsString :=dm.EscreventesCPF.AsString;

                      ConsultaDT_REGISTRO.Clear;
                      ConsultaREGISTRO.Clear;
                      ConsultaLIVRO_REGISTRO.Clear;
                      ConsultaFOLHA_REGISTRO.Clear;
                      ConsultaSELO_REGISTRO.Clear;
                      ConsultaDT_PAGAMENTO.Clear;
                      ConsultaSELO_PAGAMENTO.Clear;
                      ConsultaMOTIVO_INTIMACAO.Clear;
                      ConsultaDT_INTIMACAO.Clear;
                      ConsultaDT_SUSTADO.Clear;
                      ConsultaDT_RETIRADO.Clear;

                      Consulta.Post;
                      Consulta.ApplyUpdates(0);

                      dm.Movimento.Append;
                      dm.MovimentoID_MOVIMENTO.AsInteger  :=dm.IdAtual('ID_MOVIMENTO','S');
                      dm.MovimentoID_ATO.AsInteger        :=ConsultaID_ATO.AsInteger;
                      dm.MovimentoDATA.AsDateTime         :=Now;
                      dm.MovimentoHORA.AsDateTime         :=Time;
                      dm.MovimentoBAIXA.AsDateTime        :=ConsultaDT_ENVIO.AsDateTime;
                      dm.MovimentoESCREVENTE.AsString     :=dm.EscreventesNOME.AsString;
                      dm.MovimentoSTATUS.AsString         :=ConsultaSTATUS.AsString;
                      dm.Movimento.Post;
                      dm.Movimento.ApplyUpdates(0);
                  end;
                  Consulta.Next;
              end;
              Consulta.RecNo:=Posicao;
              Consulta.EnableControls;

              if ckImprime.Checked then ImprimirIntimacao;
          end;

          Free;

          Limpar;
      end;
  end;
end;

procedure TFBaixas2014.dsConsultaDataChange(Sender: TObject; Field: TField);
begin
  dm.Movimento.Close;
  dm.Movimento.Params[0].AsInteger:=ConsultaID_ATO.AsInteger;
  dm.Movimento.Open;
end;

procedure TFBaixas2014.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_ESCAPE then Close;
  if key=VK_RETURN then Perform(WM_NEXTDLGCTL,0,0);
  if key=VK_F1     then Grid1.Visible:=not Grid1.Visible;
end;

function TFBaixas2014.Marcados: Integer;
var
  Posicao: Integer;
begin
  Result:=0;
  Consulta.DisableControls;
  Posicao:=Consulta.RecNo;
  Consulta.First;
  while not Consulta.Eof do
  begin
      if ConsultaCheck.AsBoolean then Result:=Result+1;
      if Result>1 then Break;
      Consulta.Next;
  end;
  Consulta.RecNo:=Posicao;
  Consulta.EnableControls;
end;

procedure TFBaixas2014.Rb1Click(Sender: TObject);
begin
  if Rb1.Checked then
  begin
      Consulta.Close;
      dm.Movimento.Close;
      GbData.Enabled:=True;
      edProtFinal.Enabled:=True;
      edProtInicial.Clear;
      edProtFinal.Clear;
      MNCE.Enabled:=False;
  end;

  lbTitulo.Caption:='APONTAR'
end;

procedure TFBaixas2014.Rb2Click(Sender: TObject);
begin
  if Rb2.Checked then
  begin
      Consulta.Close;
      dm.Movimento.Close;
      GbData.Enabled:=True;
      edProtFinal.Enabled:=True;
      edProtInicial.Clear;
      edProtFinal.Clear;
      MNCE.Enabled:=False;
  end;
  
  lbTitulo.Caption:='INTIMAR';
end;

procedure TFBaixas2014.Rb3Click(Sender: TObject);
begin
  if Rb3.Checked then
  begin
      Consulta.Close;
      dm.Movimento.Close;
      GbData.Enabled:=True;
      edProtFinal.Enabled:=True;
      edProtInicial.Clear;
      edProtFinal.Clear;
      MNCE.Enabled:=False;
  end;

  lbTitulo.Caption:='PROTESTAR'
end;

procedure TFBaixas2014.Rb4Click(Sender: TObject);
begin
  if Rb4.Checked then
  begin
      Consulta.Close;
      dm.Movimento.Close;
      GbData.Enabled:=True;
      edProtFinal.Enabled:=True;
      MNCE.Enabled:=False;
      edProtInicial.Clear;
      edProtFinal.Clear;
  end;

  lbTitulo.Caption:='PAGAR';
end;

procedure TFBaixas2014.Rb6Click(Sender: TObject);
begin
  if Rb6.Checked then
  begin
      Consulta.Close;
      dm.Movimento.Close;
      GbData.Enabled:=True;
      edProtFinal.Enabled:=True;
      GbData.Enabled:=False;
      edProtFinal.Enabled:=False;
      MNCE.Enabled:=False;
      edProtInicial.SetFocus;
      edProtInicial.Clear;
      edProtFinal.Clear;
  end;

  lbTitulo.Caption:='DESIST�NCIA';
end;

procedure TFBaixas2014.Rb7Click(Sender: TObject);
begin
  if Rb7.Checked then
  begin
      Consulta.Close;
      dm.Movimento.Close;
      GbData.Enabled:=True;
      edProtFinal.Enabled:=True;
      GbData.Enabled:=False;
      edProtFinal.Enabled:=False;
      MNCE.Enabled:=False;
      edProtInicial.SetFocus;
      edProtInicial.Clear;
      edProtFinal.Clear;
  end;

  lbTitulo.Caption:='SUSTAR';
end;

procedure TFBaixas2014.DesmarcarTodos;
var
  Posicao: Integer;
begin
  Posicao:=Consulta.RecNo;
  Consulta.DisableControls;
  Consulta.First;
  while not Consulta.Eof do
  begin
      Consulta.Edit;
      ConsultaCheck.AsBoolean:=False;
      Consulta.Post;
      Consulta.Next;
  end;
  Consulta.RecNo:=Posicao;
  Consulta.EnableControls;
end;

procedure TFBaixas2014.Limpar;
begin
  Rb1.Checked         :=True;
  Rb2.Checked         :=False;
  Rb3.Checked         :=False;
  Rb4.Checked         :=False;
  Rb5.Checked         :=False;
  Rb6.Checked         :=False;
  Consulta.Close;
end;

procedure TFBaixas2014.Grid2MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if X in[11..36] then
    Screen.Cursor:=crHandPoint
      else Screen.Cursor:=crDefault;
end;

procedure TFBaixas2014.btDataClick(Sender: TObject);
var
  vWhere: String;
begin
  if (edInicio.Date=0) then
  begin
      edInicio.SetFocus;
      Exit;
  end;

  if (edFim.Date=0) then
  begin
      edFim.SetFocus;
      Exit;
  end;

  vEletronico:=False;
  vWhere:=' WHERE ';

  if Rb2.Checked then vWhere:=' WHERE (STATUS='''+'APONTADO'+''' OR STATUS='''+'INTIMADO EDITAL'+''' OR STATUS='''+'INTIMADO PESSOAL'+''' OR STATUS='''+'INTIMADO CARTA'+''') AND ';
  if Rb3.Checked then vWhere:=' WHERE (STATUS='''+'INTIMADO EDITAL'+''' OR STATUS='''+'INTIMADO PESSOAL'+''' OR STATUS='''+'INTIMADO CARTA'+''') AND ';
  if Rb4.Checked then vWhere:=' WHERE (STATUS='''+'APONTADO'+''' OR STATUS='''+'INTIMADO EDITAL'+''' OR STATUS='''+'INTIMADO PESSOAL'+''' OR STATUS='''+'INTIMADO CARTA'+''') AND ';
  if Rb5.Checked then vWhere:=' WHERE (STATUS='''+'PROTESTADO'+''' OR STATUS='''+'SUSTADO'+''') AND ';
  if Rb6.Checked then vWhere:=' WHERE (STATUS='''+'APONTADO'+''' OR STATUS='''+'INTIMADO EDITAL'+''' OR STATUS='''+'INTIMADO PESSOAL'+''' OR STATUS='''+'INTIMADO CARTA'+''') AND ';

  PF.Aguarde(True);
  Consulta.Close;
  Consulta.CommandText:=vQuery+vWhere+'(DT_PROTOCOLO BETWEEN'''+PF.FormatarData(edInicio.Date,'I')+''' AND '''+PF.FormatarData(edFim.Date,'I')+''') ORDER BY PROTOCOLO';
  Consulta.Open;

  Desmarcartodos1Click(Sender);
  PF.Aguarde(False);
end;

procedure TFBaixas2014.Grid2DblClick(Sender: TObject);
begin
  if Screen.Cursor=crHandPoint then Exit;
  if Consulta.IsEmpty then Exit;

  Self.Visible:=False;

  if Consulta.IsEmpty then Exit;

  dm.vTipo:='A';

  Application.CreateForm(TFTitulo,FTitulo);
  FTitulo.CarregarTitulo(ConsultaID_ATO.AsInteger);
  FTitulo.Visualizacao;
  FTitulo.ShowModal;
  FTitulo.Free;

  Consulta.RefreshRecord;
  Self.Visible:=True;
end;

procedure TFBaixas2014.MarcarTodosClick(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=Consulta.RecNo;
  Consulta.DisableControls;
  Consulta.First;
  while not Consulta.Eof do
  begin
      Consulta.Edit;
      ConsultaCheck.AsBoolean:=True;
      Consulta.Post;
      Consulta.Next;
  end;
  Consulta.RecNo:=Posicao;
  Consulta.EnableControls;
end;

procedure TFBaixas2014.Desmarcartodos1Click(Sender: TObject);
var
  Posicao: Integer;
begin
  Posicao:=Consulta.RecNo;
  Consulta.DisableControls;
  Consulta.First;
  while not Consulta.Eof do
  begin
      Consulta.Edit;
      ConsultaCheck.AsBoolean:=False;
      Consulta.Post;
      Consulta.Next;
  end;
  Consulta.RecNo:=Posicao;
  Consulta.EnableControls;
end;

procedure TFBaixas2014.FormCreate(Sender: TObject);
begin
  PA.Visible:=FPrincipal.ckAssistente.Checked;
  Grid1.Visible:=dm.ValorParametro(8)='S';
  dm.Movimento.Close;

  vQuery:='SELECT ID_ATO,RECIBO,DT_PROTOCOLO,PROTOCOLO,DT_REGISTRO,REGISTRO,LIVRO_REGISTRO,TIPO_INTIMACAO,SALDO_PROTESTO,TARIFA_BANCARIA,'+
          'FOLHA_REGISTRO,SELO_REGISTRO,DT_PAGAMENTO,SELO_PAGAMENTO,RECIBO_PAGAMENTO,DT_ENVIO,MOTIVO_INTIMACAO,DT_PUBLICACAO,DT_INTIMACAO,'+
          'VALOR_PAGAMENTO,DT_SUSTADO,DT_RETIRADO,APRESENTANTE,DEVEDOR,CPF_ESCREVENTE,CPF_ESCREVENTE_PG,STATUS,PROTESTADO,FORMA_PAGAMENTO,'+
          'VALOR_TITULO,NUMERO_PAGAMENTO,TIPO_DEVEDOR,CPF_CNPJ_DEVEDOR,DT_DEVOLVIDO,ID_MSG,IRREGULARIDADE,DT_PRAZO,TOTAL,TIPO_SUSTACAO,'+
          'RETORNO_PROTESTO,DT_RETORNO_PROTESTO,DT_DEFINITIVA,JUDICIAL,ALEATORIO_PROTESTO,ALEATORIO_SOLUCAO,DETERMINACAO,CODIGO,SALDO_TITULO,'+
          'CONVENIO,COBRANCA,EMOLUMENTOS,FETJ,FUNDPERJ,FUNPERJ,FUNARPEN,PMCMV,ISS,MUTUA,ACOTERJ,DISTRIBUICAO,VALOR_AR,TARIFA_BANCARIA,TOTAL, '+
          'LIVRO_PROTOCOLO,FOLHA_PROTOCOLO,ELETRONICO FROM TITULOS';
end;

procedure TFBaixas2014.ImprimirIntimacao;
var
  P: Pointer;
begin
  if Marcados=0 then Exit;

  try
    PF.Aguarde(True);

    Application.CreateForm(TFQuickIntimacao1,FQuickIntimacao1);

    P:=Consulta.GetBookmark;
    Consulta.DisableControls;
    Consulta.First;
    while not Consulta.Eof do
    begin
        if ConsultaCheck.Value=True then
        FQuickIntimacao1.CarregarRX(ConsultaID_ATO.AsInteger);
        Consulta.Next;
    end;

    if P<>Nil then
      if Consulta.BookmarkValid(P) then
        Consulta.GotoBookmark(P);

    Consulta.EnableControls;
    FQuickIntimacao1.QkIntimacao.Preview;
    FQuickIntimacao1.Free;
  except
    on E:Exception do
    begin
        PF.Aguarde(False);
        GR.Aviso('Erro: '+E.Message);
    end;
  end;
end;

procedure TFBaixas2014.edInicioExit(Sender: TObject);
begin
  if edFim.Date<edInicio.Date then
    edFim.Date:=edInicio.Date;
end;

procedure TFBaixas2014.Rb5Click(Sender: TObject);
begin
  if Rb5.Checked then
  begin
      Consulta.Close;
      dm.Movimento.Close;
      edProtInicial.SetFocus;
      edProtInicial.Clear;
      edProtFinal.Clear;
      MNCE.Enabled:=True;
  end;

  lbTitulo.Caption:='CANCELAR'
end;

procedure TFBaixas2014.ImprimirInstrumento;
var
  Praca: String;
  Posicao: Integer;
begin
  if Consulta.IsEmpty then Exit;
  if Marcados=0       then Exit;

  try
    PF.Aguarde(True);
    Application.CreateForm(TFQuickInstrumento1,FQuickInstrumento1);

    with FQuickInstrumento1 do
    begin
        RX.Close;
        RX.Open;

        Posicao:=Consulta.RecNo;
        Consulta.DisableControls;
        Consulta.First;
        while not Consulta.Eof do
        begin
            if ConsultaCheck.Value=True then
            begin
                dm.Titulos.Close;
                dm.Titulos.Params[0].AsInteger:=ConsultaID_ATO.AsInteger;
                dm.Titulos.Open;

                RX.Append;
                RXIdAto.AsInteger         :=dm.TitulosID_ATO.AsInteger;
                RXTitulo.AsString         :=PF.RetornarTitulo(dm.TitulosTIPO_TITULO.AsInteger);
                RXApresentante.AsString   :=dm.TitulosAPRESENTANTE.AsString;
                RXCredor.AsString         :=dm.TitulosCEDENTE.AsString;
                RXSacador.AsString        :=dm.TitulosSACADOR.AsString;
                RXDevedor.AsString        :=dm.TitulosDEVEDOR.AsString;
                RXValor.AsFloat           :=dm.TitulosVALOR_TITULO.AsFloat;
                RXCustas.AsFloat          :=dm.TitulosTOTAL.AsFloat;
                RXSaldoProtesto.AsFloat   :=dm.TitulosSALDO_PROTESTO.AsFloat;

                if dm.TitulosSALDO_TITULO.AsFloat<>0 then
                  RXSaldoTitulo.AsFloat:=dm.TitulosSALDO_TITULO.AsFloat
                    else RXSaldoTitulo.AsFloat:=dm.TitulosVALOR_TITULO.AsFloat;

                RXVencimento.AsDateTime   :=dm.TitulosDT_VENCIMENTO.AsDateTime;
                RXIntimacao.AsDateTime    :=dm.TitulosDT_INTIMACAO.AsDateTime;
                RXProtocolo.AsInteger     :=dm.TitulosPROTOCOLO.AsInteger;
                RXDt_Intimacao.AsDateTime :=dm.TitulosDT_INTIMACAO.AsDateTime;
                RXDt_Protocolo.AsDateTime :=dm.TitulosDT_PROTOCOLO.AsDateTime;
                RXNumero.AsString         :=dm.TitulosNUMERO_TITULO.AsString;
                RXLivro.AsString          :=dm.TitulosLIVRO_REGISTRO.AsString;
                RXFolha.AsString          :=dm.TitulosFOLHA_REGISTRO.AsString;
                RXDt_Registro.AsDateTime  :=dm.TitulosDT_REGISTRO.AsDateTime;
                RXNossoNumero.AsString    :=dm.TitulosNOSSO_NUMERO.AsString;
                RXTipo.AsString           :=dm.TitulosTIPO_DEVEDOR.AsString;
                RXMotivo.AsString         :=dm.TitulosMOTIVO_INTIMACAO.AsString;
                RXCedente.AsString        :=dm.TitulosCEDENTE.AsString;
                RXDt_Titulo.AsDateTime    :=dm.TitulosDT_TITULO.AsDateTime;
                RXEmolumentos.AsFloat     :=dm.TitulosEMOLUMENTOS.AsFloat;
                RXFetj.AsFloat            :=dm.TitulosFETJ.AsFloat;
                RXFundperj.AsFloat        :=dm.TitulosFUNDPERJ.AsFloat;
                RXFunperj.AsFloat         :=dm.TitulosFUNPERJ.AsFloat;
                RXFunarpen.AsFloat        :=dm.TitulosFUNARPEN.AsFloat;
                RXPmcmv.AsFloat           :=dm.TitulosPMCMV.AsFloat;
                RXIss.AsFloat             :=dm.TitulosISS.AsFloat;
                RXMutua.AsFloat           :=dm.TitulosMUTUA.AsFloat;
                RXAcoterj.AsFloat         :=dm.TitulosACOTERJ.AsFloat;
                RXDistribuicao.AsFloat    :=dm.TitulosDISTRIBUICAO.AsFloat;
                RXTarifa.AsFloat          :=dm.TitulosTARIFA_BANCARIA.AsFloat;
                RXAR.AsFloat              :=dm.TitulosVALOR_AR.AsFloat;
                RXTotal.AsFloat           :=dm.TitulosTOTAL.AsFloat;
                RXSelo.AsString           :=dm.TitulosSELO_REGISTRO.AsString;
                RXAleatorio.AsString      :=dm.TitulosALEATORIO_PROTESTO.AsString;
                RXTermo.AsInteger         :=dm.TitulosREGISTRO.AsInteger;
                RXTipoProtesto.AsInteger  :=dm.TitulosTIPO_PROTESTO.AsInteger;
                RXObservacao.AsString     :=dm.TitulosOBSERVACAO.AsString;
                RXDt_Publicacao.AsDateTime:=dm.TitulosDT_PUBLICACAO.AsDateTime;
                RXMensagem.AsString       :=PF.RetornarMensagem(dm.TitulosID_MSG.AsInteger);
                RXConvenio.AsString       :=dm.TitulosCONVENIO.AsString;
                RXFins.AsString           :=GR.iif(dm.TitulosFINS_FALIMENTARES.AsString='S','SIM','N�O');

                if dm.TitulosPRACA_PROTESTO.AsString='' then
                begin
                    InputQuery('Atualiza��o de dados','Pra�a de Pagamento',Praca);
                    dm.Titulos.Edit;
                    dm.TitulosPRACA_PROTESTO.AsString:=Praca;
                    dm.Titulos.Post;
                    dm.Titulos.ApplyUpdates(0);
                end;
                RXPraca.AsString:=dm.TitulosPRACA_PROTESTO.AsString;
                if RXTipo.AsString='F' then
                  RXDocumento.AsString:=PF.FormatarCPF(dm.TitulosCPF_CNPJ_DEVEDOR.AsString)
                    else RXDocumento.AsString:=PF.FormatarCNPJ(dm.TitulosCPF_CNPJ_DEVEDOR.AsString);

                if dm.TitulosTIPO_INTIMACAO.AsString='P' then RXIntimado.AsString:='PESSOAL';
                if dm.TitulosTIPO_INTIMACAO.AsString='E' then RXIntimado.AsString:='EDITAL';
                if dm.TitulosTIPO_INTIMACAO.AsString='C' then RXIntimado.AsString:='CARTA';

                if dm.TitulosTIPO_ENDOSSO.AsString='M' then RXEndosso.AsString:='MANDATO';
                if dm.TitulosTIPO_ENDOSSO.AsString='T' then RXEndosso.AsString:='TRANSLATIVO';
                if dm.TitulosTIPO_ENDOSSO.AsString='S' then RXEndosso.AsString:='SEM ENDOSSO, PARA CONTRATOS NO PR�PRIO BANCO';
                if dm.TitulosTIPO_ENDOSSO.AsString=''  then RXEndosso.AsString:='SEM ENDOSSO, PARA CONTRATOS NO PR�PRIO BANCO';

                PF.CarregarDevedor(dm.TitulosID_ATO.AsInteger);

                RXEndereco.AsString:=dm.RxDevedorENDERECO.AsString+' '+dm.RxDevedorBAIRRO.AsString+' '+
                                     dm.RxDevedorMUNICIPIO.AsString+' '+dm.RxDevedorUF.AsString;

                if dm.RxDevedorCEP.AsString<>'' then
                RXEndereco.AsString:=RXEndereco.AsString+' CEP:'+PF.FormatarCEP(dm.RxDevedorCEP.AsString);

                PF.CarregarSacador(dm.TitulosID_ATO.AsInteger);

                RXEnderecoSa.AsString:=dm.RxSacadorENDERECO.AsString+' '+dm.RxSacadorBAIRRO.AsString+' '+
                                       dm.RxSacadorMUNICIPIO.AsString+' '+dm.RxSacadorUF.AsString;

                if dm.TitulosCODIGO_APRESENTANTE.AsString<>'' then
                  PF.CarregarPortador('C',dm.TitulosCODIGO_APRESENTANTE.AsString,dm.TitulosAPRESENTANTE.AsString,
                                          dm.TitulosTIPO_APRESENTANTE.AsString,dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosCODIGO_APRESENTANTE.AsString)
                    else
                      if dm.TitulosCPF_CNPJ_APRESENTANTE.AsString<>'' then
                        PF.CarregarPortador('D',dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosAPRESENTANTE.AsString,
                                                dm.TitulosTIPO_APRESENTANTE.AsString,dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosCODIGO_APRESENTANTE.AsString)
                        else
                          PF.CarregarPortador('N',dm.TitulosAPRESENTANTE.AsString,dm.TitulosAPRESENTANTE.AsString,
                                                  dm.TitulosTIPO_APRESENTANTE.AsString,dm.TitulosCPF_CNPJ_APRESENTANTE.AsString,dm.TitulosCODIGO_APRESENTANTE.AsString);

                RXEnderecoAp.AsString:=dm.RxPortadorENDERECO.AsString;

                RX.Post;
            end;
            Consulta.Next;
        end;
        Consulta.RecNo:=Posicao;
        Consulta.EnableControls;

        qrFuncionario.Caption   :=dm.EscreventesNOME.AsString;
        qrQualificacao.Caption  :=dm.EscreventesQUALIFICACAO.AsString;

        qkInstrumento.Preview;
        Free;
    end;
  except
    on E:Exception do
    begin
        PF.Aguarde(False);
        GR.Aviso('Erro: '+E.Message);
    end;
  end;
end;

procedure TFBaixas2014.Excluir1Click(Sender: TObject);
begin
  if dm.Movimento.IsEmpty then Exit;

  dm.Movimento.Delete;
  dm.Movimento.ApplyUpdates(0);
end;

procedure TFBaixas2014.btNomeClick(Sender: TObject);
var
  vWhere: String;
begin
  if (edNome.Text='') then
  begin
      edNome.SetFocus;
      Exit;
  end;

  vEletronico:=False;
  vWhere:=' WHERE ';

  if Rb2.Checked then vWhere:=' WHERE (STATUS='''+'APONTADO'+''' OR STATUS='''+'INTIMADO EDITAL'+''' OR STATUS='''+'INTIMADO PESSOAL'+''' OR STATUS='''+'INTIMADO CARTA'+''') AND ';
  if Rb3.Checked then vWhere:=' WHERE (STATUS='''+'INTIMADO EDITAL'+''' OR STATUS='''+'INTIMADO PESSOAL'+''' OR STATUS='''+'INTIMADO CARTA'+''') AND ';
  if Rb4.Checked then vWhere:=' WHERE (STATUS='''+'APONTADO'+''' OR STATUS='''+'INTIMADO EDITAL'+''' OR STATUS='''+'INTIMADO PESSOAL'+''' OR STATUS='''+'INTIMADO CARTA'+''') AND ';
  if Rb5.Checked then vWhere:=' WHERE (STATUS='''+'PROTESTADO'+''' OR STATUS='''+'SUSTADO'+''') AND ';
  if Rb6.Checked then vWhere:=' WHERE (STATUS='''+'APONTADO'+''' OR STATUS='''+'INTIMADO EDITAL'+''' OR STATUS='''+'INTIMADO PESSOAL'+''' OR STATUS='''+'INTIMADO CARTA'+''') AND ';

  PF.Aguarde(True);
  Consulta.Close;
  Consulta.CommandText:=vQuery+vWhere+'(DEVEDOR LIKE '''+edNome.Text+'%'+''') ORDER BY PROTOCOLO';
  Consulta.Open;

  Desmarcartodos1Click(Sender);
  PF.Aguarde(False);
end;

procedure TFBaixas2014.Grid2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Screen.Cursor=crHandPoint then
  begin
      if Consulta.IsEmpty then Exit;
      Consulta.Edit;
      if ConsultaCheck.AsBoolean=True then
        ConsultaCheck.AsBoolean:=False
          else ConsultaCheck.AsBoolean:=True;
      Consulta.Post;
  end;
end;

procedure TFBaixas2014.edProtInicialEnter(Sender: TObject);
begin
  edProtFinal.Clear;
end;

procedure TFBaixas2014.ImprimirBoletosClick(Sender: TObject);
var
  Posicao: Integer;
begin
  if Marcados=0 then Exit;
  Posicao:=Consulta.RecNo;
  Consulta.DisableControls;
  Consulta.First;
  while not Consulta.Eof do
  begin
      if ConsultaCheck.AsBoolean then
      begin
          dm.Titulos.Close;
          dm.Titulos.Params[0].AsInteger:=ConsultaID_ATO.AsInteger;
          dm.Titulos.Open;

          dm.Devedores.Close;
          dm.Devedores.Params[0].AsInteger:=dm.TitulosID_ATO.AsInteger;
          dm.Devedores.Open;

          if dm.TitulosTARIFA_BANCARIA.AsFloat=0 then
          begin
              dm.Titulos.Edit;
              dm.TitulosTARIFA_BANCARIA.AsFloat:=StrToFloat(dm.ValorParametro(19));
              dm.TitulosTOTAL.AsFloat:=dm.TitulosTOTAL.AsFloat+dm.TitulosTARIFA_BANCARIA.AsFloat;
              dm.TitulosSALDO_PROTESTO.AsFloat:=dm.TitulosSALDO_PROTESTO.AsFloat+dm.TitulosTARIFA_BANCARIA.AsFloat;
              dm.Titulos.Post;
              dm.Titulos.ApplyUpdates(0);
          end;

//          GR.CriarForm(TFBoleto,FBoleto);
      end;
      Consulta.Next;
  end;
  Consulta.RecNo:=Posicao;
  Consulta.EnableControls;
end;

procedure TFBaixas2014.PMPopup(Sender: TObject);
begin
  Linha.Visible:=dm.ValorParametro(69)='S';
  ImprimirBoletos.Visible:=dm.ValorParametro(69)='S';
end;

procedure TFBaixas2014.ConsultaAfterOpen(DataSet: TDataSet);
begin
  Self.Caption:='Baixa de T�tulos: '+IntToStr(Consulta.RecordCount)+' encontrados';
end;

procedure TFBaixas2014.Protestar;
var
  vPosicao,vIdReservado,vQtdMarcados: Integer;
begin
  (**vQtdMarcados:=QtdMarcados;
  if vQtdMarcados=0 then Exit;

  if not Gdm.QtdSeloDisponivel(dm.SerieAtual,vQtdMarcados) then
  begin
      GR.Aviso('QUANTIDADE DE SELOS ('+IntToStr(vQtdMarcados)+') N�O DISPON�VEIS EM ESTOQUE ('+IntToStr(Gdm.QtdSeloAtual(dm.SerieAtual))+')');
      Exit;
  end;

  PF.Aguarde(True);
  dm.cBaixas.Close;
  dm.cBaixasESCREVENTE.KeyFields:='CPF_ESCREVENTE';
  dm.cBaixas.CreateDataSet;
  dm.cBaixas.Open;
  dm.cBaixas.EmptyDataSet;

  vPosicao:=Consulta.RecNo;
  Consulta.DisableControls;
  Consulta.First;
  vIdReservado:=GR.ValorGeneratorDBX('ID_RESERVADO',Gdm.BDGerencial);
  while not Consulta.Eof do
  begin
      if ConsultaCheck.AsBoolean then
      begin
          dm.cBaixas.Append;
          dm.cBaixasID_ATO.AsInteger        :=ConsultaID_ATO.AsInteger;
          dm.cBaixasPROTOCOLO.AsInteger     :=ConsultaPROTOCOLO.AsInteger;
          dm.cBaixasCPF_ESCREVENTE.AsString :=dm.vCPF;
          dm.cBaixasDATA.AsDateTime         :=Now;
          dm.cBaixasHORA.AsDateTime         :=Now;
          dm.cBaixasLIVRO.AsInteger         :=dm.ValorAtual('LRG','N');
          dm.cBaixasFOLHA.AsInteger         :=dm.ValorAtual('FRG','S');
          dm.cBaixasID_RESERVADO.AsInteger  :=vIdReservado;
          dm.cBaixasSELO.AsString           :=Gdm.ProximoSelo(dm.SerieAtual,'PROTESTO',
                                                              Self.Name,'N',
                                                              dm.cBaixasLIVRO.AsString,
                                                              dm.cBaixasFOLHA.AsInteger,
                                                              dm.cBaixasFOLHA.AsInteger,
                                                              dm.cBaixasREGISTRO.AsInteger,
                                                              'REGISTRO DE PROTESTO',
                                                              dm.vNome,
                                                              Now,
                                                              dm.cBaixasID_RESERVADO.AsInteger);
          dm.cBaixasALEATORIO.AsString      :=Gdm.vAleatorio;
          Gdm.vAleatorio:='*';
          dm.cBaixas.Post;

          if dm.cBaixasFOLHA.AsInteger=300 then
          begin
              dm.GerarValor('LRG');
              GR.ExecutarSQLFD('UPDATE CONTROLE SET VALOR=1 WHERE SIGLA='+QuotedStr('FRG'),dm.conSISTEMA);
          end;
      end;
      Consulta.Next;
  end;

  Consulta.RecNo:=vPosicao;
  Consulta.EnableControls;

  dm.cBaixas.First;
  dm.vOkGeral:=False;
  PF.Aguarde(False);
  Application.CreateForm(TFProtestar2014,FProtestar2014);
  with FProtestar2014 do
  begin
      ShowModal;

      if dm.vOkGeral then
      begin
          Consulta.DisableControls;
          vPosicao:=Consulta.RecNo;
          Consulta.First;
          while not Consulta.Eof do
          begin
              if ConsultaCheck.AsBoolean then
              begin
                  if dm.cBaixas.Locate('ID_ATO',ConsultaID_ATO.AsInteger,[]) then
                  begin
                      Consulta.Edit;
                      if ConsultaSTATUS.AsString='CANCELADO' then
                      PF.ExcluirFD('CERTIDOES WHERE ID_ATO='+ConsultaID_ATO.AsString+' AND TIPO_CERTIDAO='+QuotedStr('X'),dm.conSISTEMA);
                      ConsultaSTATUS.AsString             :='PROTESTADO';
                      ConsultaDT_REGISTRO.AsDateTime      :=dm.cBaixasDATA.AsDateTime;
                      ConsultaPROTESTADO.AsString         :='S';

                      if dm.ValorParametro(33)='S' then
                      ConsultaREGISTRO.AsInteger:=dm.ValorAtual('NRG','S');

                      ConsultaLIVRO_REGISTRO.AsInteger    :=dm.cBaixasLIVRO.AsInteger;
                      ConsultaFOLHA_REGISTRO.AsString     :=dm.cBaixasFOLHA.AsString;
                      ConsultaSELO_REGISTRO.AsString      :=dm.cBaixasSELO.AsString;
                      ConsultaALEATORIO_PROTESTO.AsString :=dm.cBaixasALEATORIO.AsString;
                      ConsultaCPF_ESCREVENTE.AsString     :=dm.cBaixasCPF_ESCREVENTE.AsString;
                      ConsultaVALOR_PAGAMENTO.AsFloat     :=0;
                      ConsultaID_MSG.AsInteger            :=dm.cBaixasID_MSG.AsInteger;
                      ConsultaCPF_ESCREVENTE_PG.Clear;
                      ConsultaDT_PAGAMENTO.Clear;
                      ConsultaSELO_PAGAMENTO.Clear;
                      ConsultaDT_SUSTADO.Clear;
                      ConsultaDT_RETIRADO.Clear;
                      Consulta.Post;
                      Consulta.ApplyUpdates(0);

                      Gdm.BaixarSelo(ConsultaSELO_REGISTRO.AsString,
                                     dm.cBaixasID_RESERVADO.AsInteger,
                                     'PROTESTO',
                                     Self.Name,
                                     ConsultaID_ATO.AsInteger,
                                     0,
                                     4,
                                     ConsultaDT_REGISTRO.AsDateTime,
                                     ConsultaLIVRO_REGISTRO.AsString,
                                     GR.PegarNumero(ConsultaFOLHA_REGISTRO.AsString),
                                     GR.PegarNumero(ConsultaFOLHA_REGISTRO.AsString),
                                     0,
                                     ConsultaPROTOCOLO.AsInteger,0,
                                     IfThen(ConsultaCONVENIO.AsString='S',4043,4029),
                                     'REGISTRO DE PROTESTO',
                                     dm.vNome,
                                     ConsultaCONVENIO.AsString,
                                     IfThen(ConsultaCONVENIO.AsString='S','CV','PZ'),
                                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

                      dm.Movimento.Append;
                      dm.MovimentoID_MOVIMENTO.AsInteger  :=dm.IdAtual('ID_MOVIMENTO','S');
                      dm.MovimentoID_ATO.AsInteger        :=ConsultaID_ATO.AsInteger;
                      dm.MovimentoDATA.AsDateTime         :=Now;
                      dm.MovimentoHORA.AsDateTime         :=Time;
                      dm.MovimentoBAIXA.AsDateTime        :=ConsultaDT_REGISTRO.AsDateTime;
                      dm.MovimentoESCREVENTE.AsString     :=dm.cBaixasESCREVENTE.AsString;
                      dm.MovimentoSTATUS.AsString         :=ConsultaSTATUS.AsString;
                      dm.Movimento.Post;
                      dm.Movimento.ApplyUpdates(0);
                  end;
              end;
              Consulta.Next;
          end;
          Consulta.RecNo:=vPosicao;
          Consulta.EnableControls;
      end;
      if dm.vOkGeral then
        if ckImprime.Checked then
          ImprimirInstrumento;

      Free;
      DesmarcarTodos;
  end;  **)
end;

function TFBaixas2014.QtdMarcados: Integer;
var
  Posicao: Integer;
begin
  Result:=0;
  Consulta.DisableControls;
  Posicao:=Consulta.RecNo;
  Consulta.First;
  while not Consulta.Eof do
  begin
      if ConsultaCheck.AsBoolean then
      Result:=Result+1;
      Consulta.Next;
  end;
  Consulta.RecNo:=Posicao;
  Consulta.EnableControls;
end;

procedure TFBaixas2014.Pagar;
var
  vLista: String;
  vPosicao,vQtdMarcados,vIdReservado: Integer;
begin
  {dm.vPergunta := False;
  vQtdMarcados:=QtdMarcados;
  if vQtdMarcados=0 then Exit;

  if not Gdm.QtdSeloDisponivel(dm.SerieAtual,vQtdMarcados) then
  begin
      GR.Aviso('QUANTIDADE DE SELOS ('+IntToStr(vQtdMarcados)+') N�O DISPON�VEIS EM ESTOQUE ('+IntToStr(Gdm.QtdSeloAtual(dm.SerieAtual))+')');
      Exit;
  end;

  dm.cBaixas.Close;
  dm.cBaixasESCREVENTE.KeyFields:='CPF_ESCREVENTE_PG';
  dm.cBaixas.CreateDataSet;
  dm.cBaixas.Open;
  dm.cBaixas.EmptyDataSet;

  vPosicao:=Consulta.RecNo;
  Consulta.DisableControls;
  Consulta.First;
  vIdReservado:=GR.ValorGeneratorDBX('ID_RESERVADO',Gdm.BDGerencial);
  while not Consulta.Eof do
  begin
      if ConsultaCheck.AsBoolean then
      begin
          dm.cBaixas.Append;
          dm.cBaixasID_ATO.AsInteger        :=ConsultaID_ATO.AsInteger;
          dm.cBaixasPROTOCOLO.AsInteger     :=ConsultaPROTOCOLO.AsInteger;
          dm.cBaixasCPF_ESCREVENTE.AsString :=dm.vCPF;
          dm.cBaixasFORMA_PAGAMENTO.AsString:='Dep�sito';
          dm.cBaixasVALOR_PAGAMENTO.AsFloat :=ConsultaSALDO_PROTESTO.AsFloat;
          dm.cBaixasTARIFA.AsString         :=IfThen(ConsultaTARIFA_BANCARIA.AsFloat<>0,'S','N');

          if dm.ServentiaCODIGO.AsInteger=1823 then
            dm.cBaixasDATA.AsDateTime:=PF.DataAnterior(Now,1)
              else dm.cBaixasDATA.AsDateTime:=Now;

          dm.cBaixasID_RESERVADO.AsInteger  :=vIdReservado;
          dm.cBaixasSELO.AsString           :=Gdm.ProximoSelo(dm.SerieAtual,'PROTESTO',
                                                              Self.Name,'N',
                                                              dm.cBaixasLIVRO.AsString,
                                                              0,
                                                              0,
                                                              dm.cBaixasREGISTRO.AsInteger,
                                                              'PAGAMENTO',
                                                              dm.vNome,
                                                              Now,
                                                              dm.cBaixasID_RESERVADO.AsInteger);

          dm.cBaixasALEATORIO.AsString      :=Gdm.vAleatorio;
          Gdm.vAleatorio:='*';
          dm.cBaixas.Post;
      end;
      Consulta.Next;
  end;

  Consulta.RecNo:=vPosicao;
  Consulta.EnableControls;

  dm.cBaixas.First;

  dm.vOkGeral:=False;
  Application.CreateForm(TFPago2014,FPago2014);
  with FPago2014 do
  begin
      ShowModal;

      if dm.vOkGeral then
      begin
          Consulta.DisableControls;
          vPosicao:=Consulta.RecNo;
          Consulta.First;
          while not Consulta.Eof do
          begin
              if ConsultaCheck.AsBoolean then
              begin
                  if dm.cBaixas.Locate('ID_ATO',ConsultaID_ATO.AsInteger,[]) then
                  begin
                      Consulta.Edit;
                      ConsultaSTATUS.AsString             :='PAGO';
                      ConsultaDT_PAGAMENTO.AsDateTime     :=dm.cBaixasDATA.AsDateTime;
                      ConsultaSELO_PAGAMENTO.AsString     :=dm.cBaixasSELO.AsString;
                      ConsultaALEATORIO_SOLUCAO.AsString  :=dm.cBaixasALEATORIO.AsString;
                      ConsultaCPF_ESCREVENTE_PG.AsString  :=dm.cBaixasCPF_ESCREVENTE_PG.AsString;
                      ConsultaVALOR_PAGAMENTO.AsFloat     :=dm.cBaixasVALOR_PAGAMENTO.AsFloat;
                      ConsultaNUMERO_PAGAMENTO.AsString   :=dm.cBaixasNUMERO_PAGAMENTO.AsString;

                      if dm.cBaixasTARIFA.AsString='S' then
                        ConsultaTARIFA_BANCARIA.AsFloat:=StrToFloat(dm.ValorParametro(19))
                          else ConsultaTARIFA_BANCARIA.AsFloat:=0;

                      ConsultaTOTAL.AsFloat           :=ConsultaEMOLUMENTOS.AsFloat+
                                                        ConsultaFETJ.AsFloat+
                                                        ConsultaFUNDPERJ.AsFloat+
                                                        ConsultaFUNPERJ.AsFloat+
                                                        ConsultaFUNARPEN.AsFloat+
                                                        ConsultaPMCMV.AsFloat+
                                                        ConsultaISS.AsFloat+
                                                        ConsultaMUTUA.AsFloat+
                                                        ConsultaACOTERJ.AsFloat+
                                                        ConsultaDISTRIBUICAO.AsFloat+
                                                        ConsultaVALOR_AR.AsFloat+
                                                        ConsultaTARIFA_BANCARIA.AsFloat;

                      ConsultaSALDO_PROTESTO.AsFloat   :=ConsultaSALDO_TITULO.AsFloat+ConsultaTOTAL.AsFloat;

                      ConsultaDT_SUSTADO.Clear;
                      ConsultaDT_RETIRADO.Clear;

                      if dm.cBaixasFORMA_PAGAMENTO.AsString='Cheque Administrativo' then ConsultaFORMA_PAGAMENTO.AsString:='CH';
                      if dm.cBaixasFORMA_PAGAMENTO.AsString='Dep�sito'              then ConsultaFORMA_PAGAMENTO.AsString:='DP';

                      Consulta.Post;
                      Consulta.ApplyUpdates(0);

                      Gdm.BaixarSelo(ConsultaSELO_PAGAMENTO.AsString,
                                     dm.cBaixasID_RESERVADO.AsInteger,
                                     'PROTESTO',
                                     Self.Name,
                                     ConsultaID_ATO.AsInteger,
                                     0,
                                     4,
                                     ConsultaDT_PAGAMENTO.AsDateTime,
                                     ConsultaLIVRO_PROTOCOLO.AsString,
                                     GR.PegarNumero(ConsultaFOLHA_PROTOCOLO.AsString),
                                     GR.PegarNumero(ConsultaFOLHA_PROTOCOLO.AsString),
                                     0,ConsultaPROTOCOLO.AsInteger,0,
                                     IfThen(ConsultaCONVENIO.AsString='S',4041,4026),
                                     'PAGAMENTO',
                                     dm.vNome,
                                     ConsultaCONVENIO.AsString,
                                     IfThen(ConsultaCONVENIO.AsString='S','PC','PZ'),
                                     IfThen(ConsultaCONVENIO.AsString='S',ConsultaEMOLUMENTOS.AsFloat,0),
                                     IfThen(ConsultaCONVENIO.AsString='S',ConsultaFETJ.AsFloat,0),
                                     IfThen(ConsultaCONVENIO.AsString='S',ConsultaFUNDPERJ.AsFloat,0),
                                     IfThen(ConsultaCONVENIO.AsString='S',ConsultaFUNPERJ.AsFloat,0),
                                     IfThen(ConsultaCONVENIO.AsString='S',ConsultaFUNARPEN.AsFloat,0),
                                     IfThen(ConsultaCONVENIO.AsString='S',ConsultaPMCMV.AsFloat,0),
                                     IfThen(ConsultaCONVENIO.AsString='S',ConsultaMUTUA.AsFloat,0),
                                     IfThen(ConsultaCONVENIO.AsString='S',ConsultaACOTERJ.AsFloat,0),
                                     IfThen(ConsultaCONVENIO.AsString='S',ConsultaDISTRIBUICAO.AsFloat,0),
                                     0,0,
                                     IfThen(ConsultaCONVENIO.AsString='S',ConsultaVALOR_AR.AsFloat,0),
                                     IfThen(ConsultaCONVENIO.AsString='S',ConsultaTARIFA_BANCARIA.AsFloat,0),
                                     IfThen(ConsultaCONVENIO.AsString='S',ConsultaTOTAL.AsFloat,0),
                                     IfThen(ConsultaCONVENIO.AsString='S',ConsultaISS.AsFloat,0));

                      vLista:=vLista+ConsultaID_ATO.AsString+',';

                      dm.Movimento.Append;
                      dm.MovimentoID_MOVIMENTO.AsInteger  :=dm.IdAtual('ID_MOVIMENTO','S');
                      dm.MovimentoID_ATO.AsInteger        :=ConsultaID_ATO.AsInteger;
                      dm.MovimentoDATA.AsDateTime         :=Now;
                      dm.MovimentoHORA.AsDateTime         :=Time;
                      dm.MovimentoBAIXA.AsDateTime        :=ConsultaDT_PAGAMENTO.AsDateTime;
                      dm.MovimentoESCREVENTE.AsString     :=dm.vNome;
                      dm.MovimentoSTATUS.AsString         :=ConsultaSTATUS.AsString;
                      dm.Movimento.Post;
                      dm.Movimento.ApplyUpdates(0);
                  end;
              end;
              Consulta.Next;
          end;

          vLista[Length(vLista)]:=' ';
          vLista:=Trim(vLista);
          dm.v2Via:=False;
          GR.CriarForm(TFAssinatura,FAssinatura);
          Application.CreateForm(TFQuickPagamento1,FQuickPagamento1);
          with FQuickPagamento1 do
          begin
              qryImprime.Close;
              qryImprime.SQL.Text:='SELECT * FROM TITULOS WHERE ID_ATO IN ('+vLista+') ORDER BY PROTOCOLO';
              qryImprime.Open;
              Certidao.Preview;
              if dm.vPergunta then
                  Certidao2.Preview;
              Free;
          end;
          Consulta.RecNo:=vPosicao;
          Consulta.EnableControls;
      end;

      Free;
      DesmarcarTodos;
  end;  }
end;

procedure TFBaixas2014.Intimar;
var
  Posicao: Integer;
begin
  if Marcados=0 then Exit;

  dm.vOkGeral:=False;
  Application.CreateForm(TFIntimacao,FIntimacao);
  with FIntimacao do
  begin
      if dm.ValorParametro(73)='PROTOCOLO' then
        edPrazo.Date:=PF.DataFinalFeriados(ConsultaDT_PROTOCOLO.AsDateTime,StrToInt(dm.ValorParametro(5)))
          else edPrazo.Date:=PF.DataFinalFeriados(Now,StrToInt(dm.ValorParametro(5)));
      ShowModal;

      if dm.vOkGeral then
      begin
          Consulta.DisableControls;
          Posicao:=Consulta.RecNo;
          Consulta.First;
          while not Consulta.Eof do
          begin
              if ConsultaCheck.AsBoolean then
              begin
                  Consulta.Edit;
                  ConsultaDT_INTIMACAO.AsDateTime   :=edData.Date;
                  ConsultaDT_PRAZO.AsDateTime       :=edPrazo.Date;

                  if edDataPublicacao.Date<>0 then
                  ConsultaDT_PUBLICACAO.AsDateTime  :=edDataPublicacao.Date;
                  ConsultaTIPO_INTIMACAO.AsString   :=Copy(cbTipoIntimacao.Text,1,1);
                  ConsultaMOTIVO_INTIMACAO.AsString :=dm.MotivosDESCRICAO.AsString;

                  if FIntimacao.ckBaixar.Checked then
                  begin
                      if ConsultaTIPO_INTIMACAO.AsString='P' then ConsultaSTATUS.AsString:='INTIMADO PESSOAL';
                      if ConsultaTIPO_INTIMACAO.AsString='E' then ConsultaSTATUS.AsString:='INTIMADO EDITAL';
                      if ConsultaTIPO_INTIMACAO.AsString='C' then ConsultaSTATUS.AsString:='INTIMADO CARTA';
                  end;

                  ConsultaPROTESTADO.AsString       :='N';
                  ConsultaVALOR_PAGAMENTO.AsFloat   :=0.00;
                  ConsultaCPF_ESCREVENTE.AsString   :=dm.vCPF;

                  ConsultaDT_REGISTRO.Clear;
                  ConsultaDT_PAGAMENTO.Clear;
                  ConsultaDT_ENVIO.Clear;
                  ConsultaDT_RETIRADO.Clear;
                  ConsultaDT_SUSTADO.Clear;
                  ConsultaREGISTRO.Clear;
                  ConsultaLIVRO_REGISTRO.Clear;
                  ConsultaFOLHA_REGISTRO.Clear;
                  ConsultaSELO_REGISTRO.Clear;
                  ConsultaSELO_PAGAMENTO.Clear;
                  ConsultaRECIBO_PAGAMENTO.Clear;

                  Consulta.Post;
                  Consulta.ApplyUpdates(0);

                  dm.Movimento.Append;
                  dm.MovimentoID_MOVIMENTO.AsInteger  :=dm.IdAtual('ID_MOVIMENTO','S');
                  dm.MovimentoID_ATO.AsInteger        :=ConsultaID_ATO.AsInteger;
                  dm.MovimentoDATA.AsDateTime         :=Now;
                  dm.MovimentoHORA.AsDateTime         :=Time;
                  dm.MovimentoBAIXA.AsDateTime        :=ConsultaDT_INTIMACAO.AsDateTime;
                  dm.MovimentoESCREVENTE.AsString     :=dm.vNome;
                  dm.MovimentoSTATUS.AsString         :=ConsultaSTATUS.AsString;
                  dm.Movimento.Post;
                  dm.Movimento.ApplyUpdates(0);
              end;
              Consulta.Next;
          end;
          Consulta.RecNo:=Posicao;
          Consulta.EnableControls;
      end;
      dm.Motivos.Close;

      if dm.vOkGeral then
        if ckImprime.Checked then
          ImprimirIntimacao;

      Free;
      DesmarcarTodos;
  end;
end;

procedure TFBaixas2014.Cancelar;
var
  Ano1,Ano2: Integer;
  vPosicao,vQtdMarcados,vIdReservado: Integer;
begin
  (**vQtdMarcados:=QtdMarcados;
  if vQtdMarcados=0 then Exit;

  if not Gdm.QtdSeloDisponivel(dm.SerieAtual,vQtdMarcados) then
  begin
      GR.Aviso('QUANTIDADE DE SELOS ('+IntToStr(vQtdMarcados)+') N�O DISPON�VEIS EM ESTOQUE ('+IntToStr(Gdm.QtdSeloAtual(dm.SerieAtual))+')');
      Exit;
  end;

  dm.cBaixas.Close;
  dm.cBaixasESCREVENTE.KeyFields:='CPF_ESCREVENTE_PG';
  dm.cBaixas.CreateDataSet;
  dm.cBaixas.Open;
  dm.cBaixas.EmptyDataSet;

  dm.RxCustas.Close;
  dm.RxCustas.Open;

  vPosicao:=Consulta.RecNo;
  Consulta.DisableControls;
  Consulta.First;
  vIdReservado:=GR.ValorGeneratorDBX('ID_RESERVADO',Gdm.BDGerencial);
  while not Consulta.Eof do
  begin
      if ConsultaCheck.AsBoolean then
      begin
          dm.cBaixas.Append;
          dm.cBaixasID_ATO.AsInteger        :=ConsultaID_ATO.AsInteger;
          dm.cBaixasPROTOCOLO.AsInteger     :=ConsultaPROTOCOLO.AsInteger;
          dm.cBaixasCPF_ESCREVENTE.AsString :=dm.vCPF;
          dm.cBaixasDATA.AsDateTime         :=Now;
          dm.cBaixasLIVRO.AsInteger         :=ConsultaLIVRO_REGISTRO.AsInteger;
          if ConsultaFOLHA_REGISTRO.AsString<>'' then
          dm.cBaixasFOLHA.AsInteger         :=ConsultaFOLHA_REGISTRO.AsInteger;
          dm.cBaixasID_RESERVADO.AsInteger  :=vIdReservado;
          dm.cBaixasTIPO_COBRANCA.AsString  :='COM COBRAN�A';
          dm.cBaixasCONVENIO.AsString       :=ConsultaCONVENIO.AsString;
          dm.cBaixasCODIGO.AsInteger        :=ConsultaCODIGO.AsInteger;
          dm.cBaixasSALDO_TITULO.AsFloat    :=ConsultaSALDO_TITULO.AsFloat;
          dm.cBaixasSELO.AsString           :=Gdm.ProximoSelo(dm.SerieAtual,'PROTESTO',
                                                              Self.Name,'N',
                                                              dm.cBaixasLIVRO.AsString,
                                                              dm.cBaixasFOLHA.AsInteger,
                                                              dm.cBaixasFOLHA.AsInteger,
                                                              dm.cBaixasREGISTRO.AsInteger,
                                                              'CANCELAMENTO',
                                                              dm.vNome,
                                                              Now,
                                                              dm.cBaixasID_RESERVADO.AsInteger);
          dm.cBaixasALEATORIO.AsString      :=Gdm.vAleatorio;
          Gdm.vAleatorio:='*';
          dm.cBaixas.Post;
      end;
      Consulta.Next;
  end;

  Consulta.RecNo:=vPosicao;
  Consulta.EnableControls;

  dm.cBaixas.First;

  Ano1:=YearOf(ConsultaDT_REGISTRO.AsDateTime);
  Ano2:=YearOf(Now);
  Periodo:=Ano2-Ano1;
  if Periodo<5 then Periodo:=5;

  dm.vOkGeral:=False;
  Application.CreateForm(TFCancelado2014,FCancelado2014);

  with FCancelado2014 do
  begin
      vPeriodo:=Periodo;
      ShowModal;

      if dm.vOkGeral then
      begin
          Consulta.DisableControls;
          vPosicao:=Consulta.RecNo;
          Consulta.First;
          while not Consulta.Eof do
          begin
              if ConsultaCheck.AsBoolean then
              begin
                  if dm.cBaixas.Locate('ID_ATO',ConsultaID_ATO.AsInteger,[]) then
                  begin
                      {COLOQUEI ESSA LINHA PQ ESTAVA DANDO DEADLOCK QUANDO ERA CONV�NIO}
                      {POIS EU RECALCULO AS CUSTAS E DOU UM UPDATE EM T�TULOS}
                      {DEIXEI PARA OS N�O CONV�NIO TAMB�M N�O VAI ATRAPALHAR}
                      Consulta.RefreshRecord;
                      Consulta.Edit;
                      ConsultaSTATUS.AsString             :='CANCELADO';
                      ConsultaDT_PAGAMENTO.AsDateTime     :=dm.cBaixasDATA.AsDateTime;
                      ConsultaSELO_PAGAMENTO.AsString     :=dm.cBaixasSELO.AsString;
                      ConsultaALEATORIO_SOLUCAO.AsString  :=dm.cBaixasALEATORIO.AsString;
                      ConsultaVALOR_PAGAMENTO.AsFloat     :=ConsultaSALDO_PROTESTO.AsFloat;
                      ConsultaCPF_ESCREVENTE_PG.AsString  :=dm.cBaixasCPF_ESCREVENTE_PG.AsString;
                      ConsultaRECIBO_PAGAMENTO.AsInteger  :=dm.cBaixasRECIBO.AsInteger;
                      ConsultaELETRONICO.AsString         :=IfThen(vEletronico,'S','N');

                      ConsultaDT_SUSTADO.Clear;
                      ConsultaDT_RETIRADO.Clear;

                      Consulta.Post;
                      Consulta.ApplyUpdates(0);

                      dm.Movimento.Append;
                      dm.MovimentoID_MOVIMENTO.AsInteger  :=dm.IdAtual('ID_MOVIMENTO','S');
                      dm.MovimentoID_ATO.AsInteger        :=ConsultaID_ATO.AsInteger;
                      dm.MovimentoDATA.AsDateTime         :=Now;
                      dm.MovimentoHORA.AsDateTime         :=Time;
                      dm.MovimentoBAIXA.AsDateTime        :=ConsultaDT_PAGAMENTO.AsDateTime;
                      dm.MovimentoESCREVENTE.AsString     :=dm.vNome;
                      dm.MovimentoSTATUS.AsString         :=ConsultaSTATUS.AsString;
                      dm.Movimento.Post;
                      dm.Movimento.ApplyUpdates(0);

                      //GRAVANDO DADOS DO CANCELAMENTO
                      dm.Certidoes.Close;
                      dm.Certidoes.Params[0].AsInteger:=-1;
                      dm.Certidoes.Open;
                      dm.Certidoes.Append;
                      dm.CertidoesID_CERTIDAO.AsInteger       :=dm.IdAtual('ID_ATO','S');
                      dm.CertidoesCONVENIO.AsString           :=ConsultaCONVENIO.AsString;
                      dm.CertidoesCODIGO.AsInteger            :=4010;
                      dm.CertidoesSELO.AsString               :=ConsultaSELO_PAGAMENTO.AsString;
                      dm.CertidoesALEATORIO.AsString          :=ConsultaALEATORIO_SOLUCAO.AsString;
                      dm.CertidoesRECIBO.AsInteger            :=ConsultaRECIBO_PAGAMENTO.AsInteger;
                      dm.CertidoesREQUERIDO.AsString          :=ConsultaDEVEDOR.AsString;
                      dm.CertidoesTIPO_REQUERIDO.AsString     :=ConsultaTIPO_DEVEDOR.AsString;
                      dm.CertidoesCPF_CNPJ_REQUERIDO.AsString :=ConsultaCPF_CNPJ_DEVEDOR.AsString;
                      dm.CertidoesREQUERENTE.AsString         :=ConsultaDEVEDOR.AsString;
                      dm.CertidoesCPF_CNPJ_REQUERENTE.AsString:=ConsultaCPF_CNPJ_DEVEDOR.AsString;
                      dm.CertidoesDT_PEDIDO.AsDateTime        :=ConsultaDT_PAGAMENTO.AsDateTime;
                      dm.CertidoesDT_CERTIDAO.AsDateTime      :=ConsultaDT_PAGAMENTO.AsDateTime;
                      dm.CertidoesANOS.AsInteger              :=Periodo;
                      dm.CertidoesDT_ENTREGA.AsDateTime       :=ConsultaDT_PAGAMENTO.AsDateTime;
                      dm.CertidoesTIPO_CERTIDAO.AsString      :='X';
                      dm.CertidoesFOLHAS.AsString             :='1';
                      dm.CertidoesRESULTADO.AsString          :='P';
                      dm.CertidoesREGISTROS.AsInteger         :=1;

                      if dm.cBaixasTIPO_COBRANCA.AsString='JUSTI�A GRATUITA' then dm.CertidoesCOBRANCA.AsString:='JG';
                      if dm.cBaixasTIPO_COBRANCA.AsString='COM COBRAN�A'     then dm.CertidoesCOBRANCA.AsString:='CC';
                      if dm.cBaixasTIPO_COBRANCA.AsString='SEM COBRAN�A'     then dm.CertidoesCOBRANCA.AsString:='SC';
                      if dm.cBaixasTIPO_COBRANCA.AsString='NIHIL'            then dm.CertidoesCOBRANCA.AsString:='NH';
                      if dm.cBaixasTIPO_COBRANCA.AsString='FOR�A DA LEI'     then dm.CertidoesCOBRANCA.AsString:='FL';

                      dm.CertidoesEMOLUMENTOS.AsFloat         :=dm.cBaixasEMOLUMENTOS.AsFloat;
                      dm.CertidoesFETJ.AsFloat                :=dm.cBaixasFETJ.AsFloat;
                      dm.CertidoesFUNDPERJ.AsFloat            :=dm.cBaixasFUNDPERJ.AsFloat;
                      dm.CertidoesFUNPERJ.AsFloat             :=dm.cBaixasFUNPERJ.AsFloat;
                      dm.CertidoesFUNARPEN.AsFloat            :=dm.cBaixasFUNARPEN.AsFloat;
                      dm.CertidoesPMCMV.AsFloat               :=dm.cBaixasPMCMV.AsFloat;
                      dm.CertidoesISS.AsFloat                 :=dm.cBaixasISS.AsFloat;
                      dm.CertidoesMUTUA.AsFloat               :=dm.cBaixasMUTUA.AsFloat;
                      dm.CertidoesACOTERJ.AsFloat             :=dm.cBaixasACOTERJ.AsFloat;
                      dm.CertidoesDISTRIBUICAO.AsFloat        :=dm.cBaixasDISTRIBUICAO.AsFloat;
                      dm.CertidoesAPONTAMENTO.AsFloat         :=dm.cBaixasAPONTAMENTO.AsFloat;
                      dm.CertidoesTOTAL.AsFloat               :=dm.cBaixasTOTAL.AsFloat;
                      dm.CertidoesESCREVENTE.AsString         :=ConsultaCPF_ESCREVENTE_PG.AsString;
                      dm.CertidoesID_ATO.AsInteger            :=ConsultaID_ATO.AsInteger;
                      dm.CertidoesENVIADO.AsString            :='N';
                      dm.Certidoes.Post;
                      dm.Certidoes.ApplyUpdates(0);

                      Gdm.BaixarSelo(dm.CertidoesSELO.AsString,
                                     dm.cBaixasID_RESERVADO.AsInteger,
                                     'PROTESTO',
                                     Self.Name,
                                     dm.CertidoesID_CERTIDAO.AsInteger,
                                     ConsultaID_ATO.AsInteger,
                                     4,
                                     dm.CertidoesDT_CERTIDAO.AsDateTime,
                                     ConsultaLIVRO_REGISTRO.AsString,
                                     GR.PegarNumero(ConsultaFOLHA_REGISTRO.AsString),
                                     GR.PegarNumero(ConsultaFOLHA_REGISTRO.AsString),
                                     0,ConsultaPROTOCOLO.AsInteger,0,
                                     dm.CertidoesCODIGO.AsInteger,
                                     'CANCELAMENTO',
                                     dm.vNome,
                                     dm.CertidoesCONVENIO.AsString,
                                     IfThen(ConsultaCONVENIO.AsString='S','PC',dm.CertidoesCOBRANCA.AsString),
                                     dm.CertidoesEMOLUMENTOS.AsFloat,
                                     dm.CertidoesFETJ.AsFloat,
                                     dm.CertidoesFUNDPERJ.AsFloat,
                                     dm.CertidoesFUNPERJ.AsFloat,
                                     dm.CertidoesFUNARPEN.AsFloat,
                                     dm.CertidoesPMCMV.AsFloat,
                                     dm.CertidoesMUTUA.AsFloat,
                                     dm.CertidoesACOTERJ.AsFloat,
                                     dm.CertidoesDISTRIBUICAO.AsFloat,
                                     0,
                                     dm.CertidoesAPONTAMENTO.AsFloat,
                                     0,
                                     0,
                                     dm.CertidoesTOTAL.AsFloat,
                                     dm.CertidoesISS.AsFloat);

                      dm.Custas.Close;
                      dm.Custas.Params[0].AsInteger:=-1;
                      dm.Custas.Open;
                      dm.vIdAto:=dm.cBaixasID_ATO.AsInteger;
                      dm.RxCustas.DisableControls;
                      dm.RxCustas.Filtered:=False;
                      dm.RxCustas.Filtered:=True;
                      dm.RxCustas.First;
                      while not dm.RxCustas.Eof do
                      begin
                          dm.Custas.Append;
                          dm.CustasID_CUSTA.AsInteger :=dm.IdAtual('ID_CUSTA','S');
                          dm.CustasID_ATO.AsInteger   :=dm.CertidoesID_CERTIDAO.AsInteger;
                          dm.CustasTABELA.AsString    :=dm.RxCustasTABELA.AsString;
                          dm.CustasITEM.AsString      :=dm.RxCustasITEM.AsString;
                          dm.CustasSUBITEM.AsString   :=dm.RxCustasSUBITEM.AsString;
                          dm.CustasVALOR.AsFloat      :=dm.RxCustasVALOR.AsFloat;
                          dm.CustasQTD.AsInteger      :=dm.RxCustasQTD.AsInteger;
                          dm.CustasTOTAL.AsFloat      :=dm.RxCustasTOTAL.AsFloat;
                          dm.Custas.Post;
                          dm.Custas.ApplyUpdates(0);
                          dm.RxCustas.Next;
                      end;
                      dm.RxCustas.EnableControls;
                      dm.Titulos.Close;
                      dm.Titulos.Params[0].AsInteger:=ConsultaID_ATO.AsInteger;
                      dm.Titulos.Open;
                      if ckImprimir.Checked then
                      begin
                          if dm.ValorParametro(51)='S' then
                          begin
                              Application.CreateForm(TFQuickCancelado1,FQuickCancelado1);
                              FQuickCancelado1.Free;
                          end;

                          Application.CreateForm(TFQuickCancelado2,FQuickCancelado2);
                          FQuickCancelado2.Free;
                      end;
                      dm.Titulos.Close;
                  end;
              end;
              Consulta.Next;
          end;
          Consulta.RecNo:=vPosicao;
          Consulta.EnableControls;
      end;
      Free;
      DesmarcarTodos;
      dm.RxCustas.Filtered:=False;
  end; **)
end;

procedure TFBaixas2014.Desistencia;
var
  Posicao: Integer;
begin
  if Marcados=0 then Exit;

  dm.vOkGeral:=False;
  Application.CreateForm(TFDesistencia,FDesistencia);
  with FDesistencia do
  begin
      ShowModal;

      if dm.vOkGeral then
      begin
          Consulta.DisableControls;
          Posicao:=Consulta.RecNo;
          Consulta.First;
          while not Consulta.Eof do
          begin
              if ConsultaCheck.AsBoolean then
              begin
                  Consulta.Edit;
                  ConsultaDT_RETIRADO.AsDateTime      :=edData.Date;
                  ConsultaSELO_PAGAMENTO.AsString     :=edSelo.Text;
                  ConsultaALEATORIO_SOLUCAO.AsString  :=Gdm.vAleatorio; Gdm.vAleatorio:='*';
                  ConsultaSTATUS.AsString             :='RETIRADO';
                  ConsultaPROTESTADO.AsString         :='N';
                  ConsultaVALOR_PAGAMENTO.AsFloat     :=0;
                  ConsultaCPF_ESCREVENTE_PG.AsString  :=dm.vCPF;
                  dm.Irregularidades.First;
                  while not dm.Irregularidades.Eof do
                  begin
                      if dm.IrregularidadesCheck.AsBoolean then
                      ConsultaIRREGULARIDADE.AsInteger:=dm.IrregularidadesCODIGO.AsInteger;
                      dm.Irregularidades.Next;
                  end;
                  Consulta.Post;
                  Consulta.ApplyUpdates(0);

                  Gdm.BaixarSelo(ConsultaSELO_PAGAMENTO.AsString,
                                 dm.vIdReservado,
                                 'PROTESTO',
                                 Self.Name,
                                 ConsultaID_ATO.AsInteger,
                                 0,
                                 4,
                                 ConsultaDT_RETIRADO.AsDateTime,
                                 ConsultaLIVRO_PROTOCOLO.AsString,
                                 GR.PegarNumero(ConsultaFOLHA_PROTOCOLO.AsString),
                                 GR.PegarNumero(ConsultaFOLHA_PROTOCOLO.AsString),
                                 0,ConsultaPROTOCOLO.AsInteger,0,
                                 IfThen(ConsultaCONVENIO.AsString='S',4039,4021),
                                 'DESIST�NCIA',
                                 dm.vNome,
                                 ConsultaCONVENIO.AsString,
                                 IfThen(ConsultaCONVENIO.AsString='S','CV','PZ'),
                                 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

                  dm.Movimento.Append;
                  dm.MovimentoID_MOVIMENTO.AsInteger  :=dm.IdAtual('ID_MOVIMENTO','S');
                  dm.MovimentoID_ATO.AsInteger        :=ConsultaID_ATO.AsInteger;
                  dm.MovimentoDATA.AsDateTime         :=Now;
                  dm.MovimentoHORA.AsDateTime         :=Time;
                  dm.MovimentoBAIXA.AsDateTime        :=ConsultaDT_RETIRADO.AsDateTime;
                  dm.MovimentoESCREVENTE.AsString     :=dm.vNome;
                  dm.MovimentoSTATUS.AsString         :=ConsultaSTATUS.AsString;
                  dm.Movimento.Post;
                  dm.Movimento.ApplyUpdates(0);
              end;
              Consulta.Next;
          end;
          Consulta.RecNo:=Posicao;
          Consulta.EnableControls;
      end;
      Free;
      DesmarcarTodos;
      dm.vIdReservado:=0;
  end;
end;

procedure TFBaixas2014.Sustar;
var
  Posicao: Integer;
begin
  if Marcados=0 then Exit;

  dm.vOkGeral:=False;
  Application.CreateForm(TFSustado2014,FSustado2014);
  with FSustado2014 do
  begin
      ShowModal;

      if dm.vOkGeral then
      begin
          Consulta.DisableControls;
          Posicao:=Consulta.RecNo;
          Consulta.First;
          while not Consulta.Eof do
          begin
              if ConsultaCheck.AsBoolean then
              begin
                  Consulta.Edit;
                  ConsultaSTATUS.AsString             :='SUSTADO';
                  ConsultaVALOR_PAGAMENTO.AsFloat     :=0;
                  ConsultaCPF_ESCREVENTE_PG.AsString  :=dm.vCPF;

                  {DEFINIMOS JUDICIAL=DEFINITIVA}
                  case RgTipo.ItemIndex of
                    0: begin {LIMINAR}
                           ConsultaTIPO_SUSTACAO.AsString         :='L';
                           ConsultaJUDICIAL.AsString              :='N';
                           ConsultaDT_SUSTADO.AsDateTime          :=edData.Date;
                           ConsultaDETERMINACAO.Clear;
                           ConsultaSELO_PAGAMENTO.Clear;
                           ConsultaALEATORIO_SOLUCAO.Clear;
                       end;
                    1: begin {DEFINITIVA}
                           ConsultaTIPO_SUSTACAO.AsString         :='D';
                           ConsultaDT_DEFINITIVA.AsDateTime       :=edData.Date;
                           ConsultaJUDICIAL.AsString              :='S';
                           ConsultaDETERMINACAO.AsString          :=MJudicial.Text;
                           ConsultaSELO_PAGAMENTO.AsString        :=edSelo.Text;
                           ConsultaALEATORIO_SOLUCAO.AsString     :=Gdm.vAleatorio; Gdm.vAleatorio:='*';
                       end;
                    2: begin {CANCELAMENTO}
                           ConsultaTIPO_SUSTACAO.AsString         :='C';
                           ConsultaDT_RETORNO_PROTESTO.AsDateTime :=edData.Date;
                           ConsultaSTATUS.AsString                :='PROTESTADO';
                           ConsultaJUDICIAL.AsString              :='N';
                           ConsultaDETERMINACAO.Clear;
                       end;
                  end;

                  Consulta.Post;
                  Consulta.ApplyUpdates(0);

                  if RgTipo.ItemIndex=1 then
                    Gdm.BaixarSelo(ConsultaSELO_PAGAMENTO.AsString,
                                   dm.vIdReservado,
                                   'PROTESTO',
                                   Self.Name,
                                   ConsultaID_ATO.AsInteger,
                                   0,
                                   4,
                                   ConsultaDT_SUSTADO.AsDateTime,
                                   ConsultaLIVRO_REGISTRO.AsString,
                                   GR.PegarNumero(ConsultaFOLHA_REGISTRO.AsString),
                                   GR.PegarNumero(ConsultaFOLHA_REGISTRO.AsString),
                                   0,ConsultaPROTOCOLO.AsInteger,0,
                                   ConsultaCODIGO.AsInteger,
                                   'SUSTA��O',
                                   dm.vNome,
                                   ConsultaCONVENIO.AsString,
                                   'SC',
                                   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

                  dm.Movimento.Append;
                  dm.MovimentoID_MOVIMENTO.AsInteger  :=dm.IdAtual('ID_MOVIMENTO','S');
                  dm.MovimentoID_ATO.AsInteger        :=ConsultaID_ATO.AsInteger;
                  dm.MovimentoDATA.AsDateTime         :=Now;
                  dm.MovimentoHORA.AsDateTime         :=Time;
                  dm.MovimentoBAIXA.AsDateTime        :=ConsultaDT_SUSTADO.AsDateTime;
                  dm.MovimentoESCREVENTE.AsString     :=dm.vNome;
                  dm.MovimentoSTATUS.AsString         :=ConsultaSTATUS.AsString;
                  dm.Movimento.Post;
                  dm.Movimento.ApplyUpdates(0);

                  if (ckImprimir.Checked) and (ckImprimir.Visible) then
                  begin
                      Application.CreateForm(TFQuickSustacao1,FQuickSustacao1);
                      with FQuickSustacao1 do
                      begin
                          lbCidade1.Caption:=dm.ServentiaCIDADE.AsString+' - RJ, '+FormatDateTime('dd "de" mmmm "de" yyyy.',edData.Date);
                          lbCidade2.Caption:=lbCidade1.Caption;
                          lbCidade3.Caption:=lbCidade1.Caption;
                          lbSeloEletronico.Caption:=GR.SeloFormatado(ConsultaSELO_PAGAMENTO.AsString,ConsultaALEATORIO_SOLUCAO.AsString);

                          if RgTipo.ItemIndex=0 then
                          begin
                              Liminar.Enabled       :=True;
                              Definitiva.Enabled    :=False;
                              Cancelamento.Enabled  :=False;
                          end;

                          if RgTipo.ItemIndex=1 then
                          begin
                              lbAverbacao1.Enabled  :=False;
                              lbLinha1.Enabled      :=False;
                              lbLinha2.Enabled      :=False;
                              lbLinha3.Enabled      :=False;
                              lbLinha4.Enabled      :=False;
                              lblinha5.Enabled      :=False;
                              lbNomeJuiz1.Enabled   :=False;
                              lbCidade1.Enabled     :=False;
                              Cancelamento.Enabled  :=False;
                          end;
                                  
                          if RgTipo.ItemIndex=2 then
                          begin
                              lbAverbacao1.Enabled  :=False;
                              lbLinha1.Enabled      :=False;
                              lbLinha2.Enabled      :=False;
                              lbLinha3.Enabled      :=False;
                              lbLinha4.Enabled      :=False;
                              lblinha5.Enabled      :=False;
                              lbNomeJuiz1.Enabled   :=False;
                              lbCidade1.Enabled     :=False;
                              Definitiva.Enabled    :=False;
                          end;

                          Relatorio.Preview;
                          Free;
                      end;
                  end;
              end;
              Consulta.Next;
          end;
          Consulta.RecNo:=Posicao;
          Consulta.EnableControls;
      end;
      Free;
      DesmarcarTodos;
  end;
end;

procedure TFBaixas2014.Apontar;
var
  Posicao: Integer;
begin
  if Marcados=0 then Exit;

  dm.vOkGeral:=False;
  Application.CreateForm(TFAR,FAR);
  with FAR do
  begin
      ShowModal;

      if dm.vOkGeral then
      begin
          PF.ExcluirFD('CERTIDOES WHERE ID_ATO='+QuotedStr(ConsultaID_ATO.AsString),dm.conSISTEMA);

          Consulta.DisableControls;
          Posicao:=Consulta.RecNo;
          Consulta.First;
          while not Consulta.Eof do
          begin
              if ConsultaCheck.AsBoolean then
              begin
                  Consulta.Edit;

                  ConsultaDT_PROTOCOLO.AsDateTime :=edData.Date;
                  ConsultaSTATUS.AsString         :='APONTADO';
                  ConsultaPROTESTADO.AsString     :='N';
                  ConsultaVALOR_PAGAMENTO.AsFloat :=0;
                  ConsultaCPF_ESCREVENTE.AsString :=dm.EscreventesCPF.AsString;

                  ConsultaDT_REGISTRO.Clear;
                  ConsultaDT_PAGAMENTO.Clear;
                  ConsultaDT_ENVIO.Clear;
                  ConsultaDT_INTIMACAO.Clear;
                  ConsultaDT_PUBLICACAO.Clear;
                  ConsultaDT_RETIRADO.Clear;
                  ConsultaDT_SUSTADO.Clear;
                  ConsultaREGISTRO.Clear;
                  ConsultaLIVRO_REGISTRO.Clear;
                  ConsultaFOLHA_REGISTRO.Clear;
                  ConsultaSELO_REGISTRO.Clear;
                  ConsultaSELO_PAGAMENTO.Clear;
                  ConsultaMOTIVO_INTIMACAO.Clear;
                  ConsultaRECIBO_PAGAMENTO.Clear;
                  ConsultaTIPO_INTIMACAO.Clear;

                  Consulta.Post;
                  Consulta.ApplyUpdates(0);

                  dm.Movimento.Append;
                  dm.MovimentoID_MOVIMENTO.AsInteger  :=dm.IdAtual('ID_MOVIMENTO','S');
                  dm.MovimentoID_ATO.AsInteger        :=ConsultaID_ATO.AsInteger;
                  dm.MovimentoDATA.AsDateTime         :=Now;
                  dm.MovimentoHORA.AsDateTime         :=Time;
                  dm.MovimentoBAIXA.AsDateTime        :=ConsultaDT_PROTOCOLO.AsDateTime;
                  dm.MovimentoESCREVENTE.AsString     :=dm.EscreventesNOME.AsString;
                  dm.MovimentoSTATUS.AsString         :=ConsultaSTATUS.AsString;
                  dm.Movimento.Post;
                  dm.Movimento.ApplyUpdates(0);
              end;
              Consulta.Next;
          end;
          Consulta.RecNo:=Posicao;
          Consulta.EnableControls;
      end;

      Free;
      DesmarcarTodos;
  end;
end;

procedure TFBaixas2014.Rb8Click(Sender: TObject);
begin
  if Rb8.Checked then
  begin
      Consulta.Close;
      dm.Movimento.Close;
      GbData.Enabled:=True;
      edProtFinal.Enabled:=True;
      MNCE.Enabled:=False;
      edProtInicial.Clear;
      edProtFinal.Clear;
  end;

  lbTitulo.Caption:='DEVOLVER';
end;

procedure TFBaixas2014.Devolver;
var
  Posicao: Integer;
begin
  if Marcados=0 then Exit;

  dm.vOkGeral:=False;
  Application.CreateForm(TFDevolvido,FDevolvido);
  with FDevolvido do
  begin
      ShowModal;

      if dm.vOkGeral then
      begin
          Consulta.DisableControls;
          Posicao:=Consulta.RecNo;
          Consulta.First;
          while not Consulta.Eof do
          begin
              if ConsultaCheck.AsBoolean then
              begin
                  Consulta.Edit;
                  ConsultaDT_DEVOLVIDO.AsDateTime     :=edData.Date;
                  ConsultaSTATUS.AsString             :='DEVOLVIDO';
                  ConsultaPROTESTADO.AsString         :='N';
                  ConsultaVALOR_PAGAMENTO.AsFloat     :=0;
                  ConsultaCPF_ESCREVENTE_PG.AsString  :=dm.vCPF;
                  dm.Irregularidades.First;
                  if not dm.IrregularidadesCheck.AsBoolean then
                    repeat dm.Irregularidades.Next;
                    until dm.IrregularidadesCheck.AsBoolean;
                  ConsultaIRREGULARIDADE.AsInteger    :=dm.IrregularidadesCODIGO.AsInteger;
                  Consulta.Post;
                  Consulta.ApplyUpdates(0);

                  dm.Movimento.Append;
                  dm.MovimentoID_MOVIMENTO.AsInteger  :=dm.IdAtual('ID_MOVIMENTO','S');
                  dm.MovimentoID_ATO.AsInteger        :=ConsultaID_ATO.AsInteger;
                  dm.MovimentoDATA.AsDateTime         :=Now;
                  dm.MovimentoHORA.AsDateTime         :=Time;
                  dm.MovimentoBAIXA.AsDateTime        :=ConsultaDT_SUSTADO.AsDateTime;
                  dm.MovimentoESCREVENTE.AsString     :=dm.vNome;
                  dm.MovimentoSTATUS.AsString         :=ConsultaSTATUS.AsString;
                  dm.Movimento.Post;
                  dm.Movimento.ApplyUpdates(0);
              end;
              Consulta.Next;
          end;
          Consulta.RecNo:=Posicao;
          Consulta.EnableControls;
      end;

      if dm.ServentiaCODIGO.AsInteger<>2353 then
        if dm.vOkGeral then
        begin
            dm.Titulos.Close;
            dm.Titulos.Params[0].AsInteger:=ConsultaID_ATO.AsInteger;
            dm.Titulos.Open;
            Application.CreateForm(TFQuickRetirado1,FQuickRetirado1);
            FQuickRetirado1.Free;
            dm.Titulos.Close;
        end;
      Free;
      DesmarcarTodos;
  end;
end;

procedure TFBaixas2014.lbTituloClick(Sender: TObject);
begin
  if Consulta.IsEmpty then
  begin
      GR.Aviso('Nenhum movimento encontrado.');
      Exit;
  end;

  if Marcados=0 then
  begin
      GR.Aviso('Nenhum t�tulo selecionado.');
      Exit;
  end;

  Self.Caption:='Baixa de T�tulos: '+IntToStr(Consulta.RecordCount)+' encontrados .:. Marcados: '+IntToStr(Selecionados);

  if Rb1.Checked then Apontar;
  if Rb2.Checked then Intimar;
  if Rb3.Checked then Protestar;
  if Rb4.Checked then Pagar;
  if Rb5.Checked then Cancelar;
  if Rb6.Checked then Desistencia;
  if Rb7.Checked then Sustar;
  if Rb8.Checked then Devolver;

  Self.Caption:='Baixa de T�tulos: '+IntToStr(Consulta.RecordCount)+' encontrados';
end;

procedure TFBaixas2014.Grid1DblClick(Sender: TObject);
begin
  if dm.Movimento.IsEmpty then Exit;

  dm.Movimento.Edit;
  GR.CriarForm(TFMovimento,FMovimento);
end;

procedure TFBaixas2014.MNCEClick(Sender: TObject);
  function Texto(Linha, Inicio, Fim: Integer): String;
  begin
    Result:=Trim(Copy(M.Lines[Linha],Inicio,Fim));
  end;
var
  I: Integer;
  Protocolos: String;
begin
  if Opd.Execute then
  begin
      PF.Aguarde(True);
      vEletronico:=True;
      M.Lines.LoadFromFile(opd.FileName);
      if Copy(ExtractFileName(Opd.FileName),1,2)='CP' then
        vTipo:='C'
          else vTipo:='D';
          
      Protocolos:='';
      for I := 0 to M.Lines.Count -1 do
      begin
          if Texto(I,1,1)='2' then
          Protocolos:=Protocolos+IntToStr(StrToInt(Texto(I,2,10)))+',';
      end;

      Protocolos[Length(Protocolos)]:=' ';
      Protocolos:=Trim(Protocolos);

      Consulta.Close;
      Consulta.CommandText:=vQuery+' WHERE PROTOCOLO IN ('+Protocolos+') ORDER BY PROTOCOLO';
      Consulta.Open;
      Desmarcartodos1Click(Sender);
      PF.Aguarde(False);
  end;
end;

end.
